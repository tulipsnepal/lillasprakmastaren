#Specification

You can download a zip with manuscript for several stations here:
```
http://strawberryhill.se/stuff/learninggame/lillamanus.zip
```

##Basic scope
The production consists of 47 exercises, they are all explained in the manuscript
The game takes place in a city. The city is made up of an image that can be explored. There are six different hotspots/places to go. Each place has exercises for a specific part of the swedish language. When you click/touch one of these hotspots on the city screen it opens up the landing page for that specific set of exercises. From this landing page you can select which exercise you want to try.

The production is to be done in html/css/javascript, we are leaning towards using canvas, see more below

Browser Support shall be IE9 and up and firefox, chrome, safari and opera(opera being the less prioritized)
There is an earlier version made ​in flash(se below link), basically, they want the same thing, but they want it work for pad/slates and screens. Not phones.
`http://option.sanomaonline.se/resources/sprakmastaren%20A/sprakmastaren.html`

We need to support resolutions from landscape ipad to computers, I think the way to go is to build it for ipad resolution and if necessary scale it up a bit, but mainly center the game window horizontally on bigger screens.
Graphics

All graphics are made ​in illustrator. We have looked at SVG but find that the performance and browser support seems to be too bad so therefore we are inclined to use canvas.
For canvas support we have looked at the canvas drawing library pixi.js that support canned animation, sprites etc. It is really easy to use, and easier still if you have any flash experience.
`http://www.pixijs.com/`

The animations are mostly quite simple. You can look at the flash game to get a feeling for it.
Sound
The game will also need to support audio for playing instructions and feedback for exercises.
For instance they want to have mouths on certain characters that change randomly while a specific sound is playing.

This library seems nice but we haven’t tested it:
`http://www.createjs.com/#!/SoundJS`

##Other stuff

If this project is successful there may be further work to port two other nearly identical productions. One of them is the seen in the flash link above. So therefore we want the project to be built in a clean modular way so it is easy to change content.
Another need for modularity probably comes from the need to be able to handle all the stations without running out of memory.