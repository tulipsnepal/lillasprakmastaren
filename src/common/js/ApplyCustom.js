/**
 * unload,destroy,clean memory related to exercise during exercise switch
 */
UnloadScene = function() {
	for (var i = this.sceneContainer.children.length - 1; i >= 0; i--) {
		this.sceneContainer.removeChild(this.sceneContainer.children[i]);
	};
	if (main.CurrentExercise) {
		createjs.Sound.stop();
		//Remove all sounds that have been registered with registerSound or registerManifest.
		// createjs.Sound.removeAllSounds();

		createjs.Sound.removeAllEventListeners();

		this.introId = null;
		this.outroId = null;
		this.introInstance = null;

		//destroy all sprite textures from memory
		if (main.texturesToDestroy.length > 0) {
			for (var i = 0; i < main.texturesToDestroy.length; i++) {
				main.texturesToDestroy[i].texture.destroy(true);
			};
			main.texturesToDestroy.length = 0;
		};

		//destroy all animation textures from memory

		if (main.animationTextures.length > 0) {
			for (var i = 0; i < main.animationTextures.length; i++) {
				main.animationTextures[i].destroy(true);
			};
			main.animationTextures.length = 0;
		};

		preload.destroy();
		main.common.init();

		if (uniqueRandoms) uniqueRandoms = [];
		main.page.cleanMemory();
		addedListeners = false;
	};

	sound_help = loadSound('uploads/audios/menu_sounds/helpgirl_level0');

	correct_sound = loadSound('build/common/audios/right');
	correctSoundInstance = createjs.Sound.createInstance(correct_sound);

	wrong_sound = loadSound('build/common/audios/wrong');
	wrongSoundInstance = createjs.Sound.createInstance(wrong_sound);

};

// remove all own properties on obj,
//effectively reverting it to a new object
wipe = function(obj) {
	for (var p in obj) {
		if (obj.hasOwnProperty(p))
			delete obj[p];
	}
};