function ExerciseSpritesPool(jsonArray) {
    this.jsonArray = jsonArray;
    this.createSprites();
}

ExerciseSpritesPool.prototype.borrowSprite = function() {
    return this.sprites.shift();
};

ExerciseSpritesPool.prototype.getBorrowIndex = function() {

    var iterationIndex, poolCategory;

    for (var i = 0; i < this.sprites.length; i++) {

        /* if (this.poolCategoryType === 'category')
             poolCategory = this.sprites[i][0].category[0];
         else
             poolCategory = this.sprites[i][0].word[0];*/

        if (typeof this.poolCategoryType !== "undefined" && this.poolCategoryType !== "word" && this.poolIsNotArray !== true) { //custom main word
            poolCategory = this.sprites[i][0][this.poolCategoryType][0];
        } else {
            if (this.poolIsNotArray == true) {
                poolCategory = this.sprites[i][0][this.poolCategoryType];
            } else {
                poolCategory = this.sprites[i][0].word[0];
            }
        }

        if (poolCategory !== this.lastBorrowSprite && poolCategory === this.borrowCategory) {
            iterationIndex = i;
            break;
        }

    };

    return iterationIndex;
}


ExerciseSpritesPool.prototype.reCreateSprites = function() {

    this.sprites.length = 0;
    this.createSprites();
}

ExerciseSpritesPool.prototype.borrowUniqueSprite = function(borrowCategory, lastBorrowSprite, poolCategoryType, poolIsNotArray) {

    var index;

    if (this.sprites.length < 1)
        this.reCreateSprites();

    this.lastBorrowSprite = lastBorrowSprite;
    this.borrowCategory = borrowCategory;

    this.poolCategoryType = poolCategoryType || 'word';
    this.poolIsNotArray = poolIsNotArray || false;

    index = this.getBorrowIndex();
    console.log("index 1st = " + index);
    if (index === undefined) {
        this.reCreateSprites();
        index = this.getBorrowIndex();
        console.log("index 2nd = " + index);
    }


    return this.sprites.splice(index, 1)[0];

}

ExerciseSpritesPool.prototype.getBorrowIndexMultipleWords = function() {

    var iterationIndex, poolCategory, rawPoolCategory, lastBorrowSprite;

    for (var i = 0; i < this.sprites.length; i++) {

        if (this.isFootball_1_2) {
            rawPoolCategory = this.sprites[i][0][this.poolCategoryType];
            var ucwords = [],
                rawWord;
            for (var j = 0; j < rawPoolCategory.length; j++) {
                rawWord = rawPoolCategory[j].split(" ");
                ucwords.push(rawWord[1].replace(".", ""));
            }
        } else {
            rawPoolCategory = this.sprites[i][0][this.poolCategoryType];
            var ucwords = [];
            for (var j = 0; j < rawPoolCategory.length; j++) {
                ucwords.push(rawPoolCategory[j]);
            }
        }

        poolCategory = ucwords;
        poolCategory = poolCategory.sort();
        // console.log("PC = "+pc);

        if (typeof this.lastBorrowSprite !== "undefined") {
            lastBorrowSprite = this.lastBorrowSprite.filter(onlyUnique).sort();
            if (lastBorrowSprite.compare(this.poolCategory)) {
                this.alreadyPlayedWords = [];
            }

            var testCommon = [];
            testCommon = poolCategory.diff(lastBorrowSprite);
          /*  console.warn("this.lastBorrowSprite = " + lastBorrowSprite);
            console.warn("testCommon = " + testCommon);
             console.warn("poolCategory = " + poolCategory);*/
            /* console.warn("this.borrowCategory = " + borrowCategory);*/
            if (testCommon.length === 0) {
                iterationIndex = i;
                break;
            }
        }
    };

    return iterationIndex;
}

ExerciseSpritesPool.prototype.borrowUniqueSpriteMultipleWords = function(lastBorrowSprite, poolCategory, isFootball_1_2, poolCategoryType) {

    var index;

    if (this.sprites.length < 1)
        this.reCreateSprites();

    this.lastBorrowSprite = lastBorrowSprite;
    this.poolCategory = poolCategory || [];
    this.isFootball_1_2 = isFootball_1_2 || false;
    this.poolCategoryType = poolCategoryType || "word";

    index = this.getBorrowIndexMultipleWords();

    //  console.info("index 1st = " + index);

    if (index === undefined) {
        this.reCreateSprites();
        index = this.getBorrowIndexMultipleWords();
        // console.info("index 2nd = " + index);
    }

    return this.sprites.splice(index, 1)[0];
}

ExerciseSpritesPool.prototype.returnSprite = function(sprite) {
    this.sprites.push(sprite);
};

ExerciseSpritesPool.prototype.createSprites = function() {

    this.sprites = [];

    this.addSpriteSprites();

    // console.log(JSON.stringify(this.sprites));
    this.shuffle(this.sprites);
    // console.log(JSON.stringify(this.sprites));
};

ExerciseSpritesPool.prototype.addSpriteSprites = function() {

    var emptyArray = new Array(),
        data = this.jsonArray;

    for (var i = 0; i < data.length; i++) {
        var obj = data[i];
        this.sprites[i] = [];
        this.sprites[i].push(data[i]);
    };

};

ExerciseSpritesPool.prototype.shuffle = function(array) {
    var len = array.length;
    var shuffles = len * 3;
    for (var i = 0; i < shuffles; i++) {
        var spriteSlice = array.pop();
        var pos = Math.floor(Math.random() * (len - 1));
        array.splice(pos, 0, spriteSlice);
    }
};
