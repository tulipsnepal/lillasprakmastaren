var Menu = Menu || {};

Menu = function(param) {

    PIXI.DisplayObjectContainer.call(this);

    this.activeMenuContainer = new PIXI.DisplayObjectContainer();
    this.activeMenuContainer2 = new PIXI.DisplayObjectContainer();

    this.assetsPath = param.path;
    this.baseBkgSprite = param.background;
    this.menuImages = param.images;
    this.menuPositions = param.positions;
    this.data = param.data;

    this.menuBkg = new PIXI.Sprite.fromImage(this.assetsPath + this.baseBkgSprite);
    this.addChild(this.menuBkg);

    for (var i = 0; i < this.menuImages.length; i++) {

        var menu = PIXI.Sprite.fromFrame(this.menuImages[i]);
        menu.buttonMode = true;
        menu.interactive = true;

        menu.position.x = this.menuPositions[i * 2];
        menu.position.y = this.menuPositions[i * 2 + 1];
        this.addChild(menu);

    }

    this.addChild(this.activeMenuContainer);
    this.addChild(this.activeMenuContainer2);

}

Menu.constructor = Menu;
Menu.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

Menu.prototype.getActiveMenu = function(menuKey) {

    this.subMenus = [];

    this.menuData = this.data[menuKey];

    //clear old container data
    this.activeMenuContainer2.removeChildren();

    this.activeMenuContainer.removeChildren();

    this.menuHover = PIXI.Sprite.fromFrame(this.menuData.hover_image);
    this.menuHover.position = {
        x: this.menuData.hover_position[0],
        y: this.menuData.hover_position[1]
    };
    this.activeMenuContainer.addChild(this.menuHover);

    this.pinkSprite = PIXI.Sprite.fromFrame(this.data.pink_image);
    this.activeMenuContainer2.addChild(this.pinkSprite);

    for (var i = 0; i < this.menuData.submenu_image.length; i++) {

        var subMenu = PIXI.Sprite.fromFrame(this.menuData.submenu_image[i]);

        subMenu.buttonMode = true;
        subMenu.interactive = true;

        subMenu.hitArea = new PIXI.Rectangle(0, 0, 97, 247);

        subMenu.position.x = this.menuData.submenu_position[i * 2];
        subMenu.position.y = this.menuData.submenu_position[i * 2 + 1];
        this.activeMenuContainer2.addChild(subMenu);

        this.subMenus.push(subMenu);

    }

    //when exercise is active prevent hover menu to chop the menu image
    if (this.data.chop) {
        for (var i = 0; i < this.data.chop.images.length; i++) {
            var menu = PIXI.Sprite.fromFrame(this.data.chop.images[i]);

            menu.position.x = this.data.chop.positions[i * 2];
            menu.position.y = this.data.chop.positions[i * 2 + 1];
            this.activeMenuContainer.addChild(menu);
        }
    };

    return this.subMenus;
}


Menu.prototype.moveSelectedExerciseCircle = function(position) {
    this.activeMenuContainer2.removeChildAt(0);
    this.pinkSprite.position = {
        x: position.x + this.data.move_position.x,
        y: position.y + this.data.move_position.y
    };
    this.activeMenuContainer2.addChildAt(this.pinkSprite, 0);
}