var fps = 60;
var now;
var then = Date.now();
var interval = 1000 / fps;
var delta;
var correctSoundInstance = null;
var wrongSoundInstance = null;

var correct_sound, wrong_sound, sound_help;

function Main() {

    cl.show();

    this.CurrentExercise = "";

    this.eventRunning = false;

    // show the canvas loadercl.show();


    /**
     * create an new instance of a pixi stage specifying stage's background color
     * stage represents all your game's graphical content
     */

    this.stage = new PIXI.Stage(0x000000);

    /**
     *  This helper function will automatically detect which renderer you should be using. This function is very similar to the autoDetectRenderer function except that is will return a canvas renderer for android. Even thought both android chrome supports webGL the canvas implementation perform better at the time of writing. This function will likely change and update as webGL performance improves on these devices.
     create a renderer instance and add the renderer view element to the DOM renderer takes the stage and draws it to your HTML page's Canvas so that game scene is visible to user
     */
    if (Device.ieVersion == 11) {
        this.getCanvasRenderer();
    } else if (Device.macOS && Device.safari) {
        this.getCanvasRenderer();
    } else {
        this.renderer = new PIXI.autoDetectRecommendedRenderer(
            2048,
            1496, {
                view: document.getElementById("stage-container"),
                resolution: 1
            }
        );
    }

    // set the canvas width and height to fill the screen
    this.renderer.view.style.display = "block";
    this.renderer.view.style.width = '1024px';
    this.renderer.view.style.height = '748px';

    // store renderer in a global variable
    autoDetectRenderer = this.renderer;

    this.mainContainer = new PIXI.DisplayObjectContainer();

    this.sceneContainer = new PIXI.DisplayObjectContainer();

    this.common = new Common();
    this.common.init();

    this.loadSpriteSheet();

    this.stage.addChild(this.mainContainer);
}

Main.TOTAL_FEEDBACK = 10;

Main.prototype.getCanvasRenderer = function() {
    this.renderer = new PIXI.CanvasRenderer(
        2048,
        1496, {
            view: document.getElementById("stage-container"),
            resolution: 1
        }
    );
}

Main.prototype.loadSpriteSheet = function() {

    this.assetsToLoad = [
        "build/exercises/oversikt_stad/images/sprite_home.json",
        "build/exercises/oversikt_stad/images/sprite_home.png",
        "build/exercises/oversikt_stad/images/overviewcity-40pxcut.png",
        "build/exercises/oversikt_stad/images/recommendation_box.json",
        "build/exercises/oversikt_stad/images/recommendation_box.png",
        "build/exercises/oversikt_stad/images/menu_back.png",
        "build/exercises/oversikt_stad/images/startpage1.png",
        "build/exercises/oversikt_stad/images/startpage2.png"
    ];

    this.pushExerciseAssets();

    loader = new PIXI.AssetLoader(this.assetsToLoad);
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    loader.load();

};

/**
 * if #hash value in a url provided,
 * push neccessary assets for that exercise
 * @return void
 */
Main.prototype.pushExerciseAssets = function() {

    var hash = window.location.hash;

    if (hash) {
        if (hash.indexOf("Tivoli") > -1) {
            this.assetsToLoad.push(Tivoli.COMMONFOLDER + 'sprite_tivoli.json');
            this.assetsToLoad.push(Tivoli.COMMONFOLDER + 'sprite_tivoli.png');
        };

        if (hash.indexOf("Toy") > -1) {
            this.assetsToLoad.push(Toystore.COMMONFOLDER + 'sprite_toystore.json');
            this.assetsToLoad.push(Toystore.COMMONFOLDER + 'sprite_toystore.png');
        }

        if (hash.indexOf("School") > -1) {
            this.assetsToLoad.push(School.COMMONFOLDER + 'sprite_school.json');
            this.assetsToLoad.push(School.COMMONFOLDER + 'sprite_school.png');
        }

        if (hash.indexOf("Football") > -1) {
            this.assetsToLoad.push(Football.COMMONFOLDER + 'sprite_football.json');
            this.assetsToLoad.push(Football.COMMONFOLDER + 'sprite_football.png');
        }

        if (hash.indexOf("House") > -1) {
            this.assetsToLoad.push(House.COMMONFOLDER + 'sprite_house.json');
            this.assetsToLoad.push(House.COMMONFOLDER + 'sprite_house.png');
        }

        if (hash.indexOf("Playground") > -1) {
            this.assetsToLoad.push(Playground.COMMONFOLDER + 'sprite_playground.png');
            this.assetsToLoad.push(Playground.COMMONFOLDER + 'sprite_playground.json');
        }
    }
};

/**
 * load homepage scene as default scene
 */
Main.prototype.spriteSheetLoaded = function() {

    // for IE9 history feature not implemented
    if (Device.ieVersion != 9) {
        var exhistory = new History();
        exhistory.PushState({
            scene: "history_Home"
        });
        exhistory = null;

    };

    this.bottomBackground = createRect({
        "alpha": 0.6,
    });
    this.bottomBackground.height = 748 * 2;
    this.bottomBackground.width = 1024 * 2;
    this.bottomBackground.alpha = 1;
    this.mainContainer.addChild(this.bottomBackground);

    correct_sound = loadSound('build/common/audios/right');
    wrong_sound = loadSound('build/common/audios/wrong');

    this.mainContainer.addChild(this.sceneContainer);

    this.overlay = PIXI.Sprite.fromFrame("overlay.png");
    this.overlay.position = {
        x: 0,
        y: 0
    };
    this.overlay.height = 748 * 2;
    this.overlay.width = 1024 * 2;
    this.overlay.alpha = 0;
    this.overlay.alpha = 0;
    this.overlay.interactive = true;
    this.overlay.buttonMode = true;

    this.overlay.click = this.overlay.tap = function(data) {

        if (main.page.introMode)
            main.page.cancelIntro();
        else
            main.page.cancelOutro();

    }


    //load help master in bottom right corner
    this.girlMenu = new GirlMenu();
    this.mainContainer.addChild(this.girlMenu);

    this.overlay.visible = false;
    this.mainContainer.addChild(this.overlay);

    //make canvas responsive
    this.t = new ScaleCanvas(this.mainContainer);

    this.sceneChange('Home');

    this.checkConnection();

    // javascript function that determines an optimal frame rate of your browser
    // and then calls a specified function when your canvas/stage can next be redrawn
    // call a function named update() the next time your stage’s contents can be redrawn
    //  The update() function that we’ve specified will act as your main loop.
    // bind function guarantee that when update() is called that it’s correctly scoped to our Main class’ instance.
    // If you don’t apply bind() then your update() method won’t be able to access and use any of our class’ member variables.
    requestAnimFrame(this.update.bind(this));


};



/**
 * The update() function that we’ve specified will act as your main loop.
 * Render the stage in canvas for each frame call
 */
Main.prototype.update = function() {

    //call the udpate function at each frame change
    requestAnimFrame(this.update.bind(this));

    now = Date.now();
    delta = now - then;

    if (delta > interval) {
        // update time stuffs

        // Just `then = now` is not enough.
        // Lets say we set fps at 10 which means
        // each frame must take 100ms
        // Now frame executes in 16ms (60fps) so
        // the loop iterates 7 times (16*7 = 112ms) until
        // delta > interval === true
        // Eventually this lowers down the FPS as
        // 112*10 = 1120ms (NOT 1000ms).
        // So we have to get rid of that extra 12ms
        // by subtracting delta (112) % interval (100).
        // Hope that makes sense.

        then = now - (delta % interval);

        // Code for Drawing the Frame
        // render the stage, everything added to stage is rendered
        this.renderer.render(this.stage);
    }


};

/**
 * This function load the new scene to scene container
 */
Main.prototype.sceneChange = function(scene) {
    this.CurrentExercise = "";

    if (Device.safari) {
        if (Device.isLocalStorageWritable)
            localStorage.setItem('exercise', '');
    };


    if (window.location.hash)
        scene = window.location.hash.slice(1);
    else
        scene = scene || 'Home';

    this.page = new window[scene];

    this.page.introMode = true;

    // empty scene container to load new scene
    for (var i = this.sceneContainer.children.length - 1; i >= 0; i--) {
        this.sceneContainer.removeChild(this.sceneContainer.children[i]);
    };

    this.sceneContainer.addChild(this.page);

    //No internet connection stuffs
    var noInternetText = new PIXI.Text("Your device lost its internet connection, attempting to reconnect ...", {
        font: "30px Arial",
        fill: "white"
    });

    var noInternetBoxRaw = createRect({
        w: noInternetText.width + 20,
        h: noInternetText.height + 20,
        color: 0xcc0000
    });
    var noInternetBox = noInternetBoxRaw.generateTexture();

    this.noInternetBoxSprite = new PIXI.Sprite(noInternetBox);
    this.noInternetBoxSprite.addChild(noInternetText);
    noInternetText.position = {
        x: 10,
        y: 10
    };

    this.noInternetBoxSprite.position = {
        x: (2048 - this.noInternetBoxSprite.width) / 2,
        y: 0
    };
    this.sceneContainer.addChild(this.noInternetBoxSprite);
    this.noInternetBoxSprite.visible = false;
};

/**
 * @author Sabin Chhetri and Dharma Thapa
 * @date   1st June,2015
 * This method uses Offline js library
 * Method to Check the internet connection and show the error message
 * @return void
 */
Main.prototype.checkConnection = function() {

    var inactiveMode = PIXI.Sprite.fromFrame("overlay.png");
    inactiveMode.position = {
        x: 0,
        y: 0
    };
    inactiveMode.height = 748 * 2;
    inactiveMode.width = 1024 * 2;
    inactiveMode.alpha = 0.6;
    inactiveMode.visible = false;
    inactiveMode.interactive = true;
    this.mainContainer.addChild(inactiveMode);

    inactiveMode.click = inactiveMode.tap = function(data) {
        return false;
    }

    var run = function() {
        Offline.check();
        if (Offline.state === 'up') {
            main.noInternetBoxSprite.visible = false;
        } else {
            main.noInternetBoxSprite.visible = true;
        }
    }

    setInterval(run, 1000);

    Offline.on('up', function() {
        inactiveMode.visible = false;
    });

    Offline.on('down', function() {
        inactiveMode.visible = true;
    });
};