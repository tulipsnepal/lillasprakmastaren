addedListeners = false;
createjs.Sound.initializeDefaultPlugins();
createjs.Sound.alternateExtensions = ["ogg"];

createjs.Sound.on("fileload", handleLoad);

// create new instance Device Detect constructor function
Device = new DeviceDetect();

// create new instance of CanvasLoader constructor function
cl = new CanvasLoader('canvasloader-container'); //hidden by default
cl.setColor('#18b150'); // default is '#000000'
cl.setDiameter(100); // default is 40

// set approporiate audioFormat to use in device
audioFormat = "mp3";

if (Device.firefox) audioFormat = "ogg";

function loadSound(src) {
    var src = src + '.' + audioFormat;
    if (createjs.Sound._idHash[src]) {
        // createjs.Sound.play(id);
    } else {
        // register and then play sound
        createjs.Sound.registerSound(src, src, 1);
    }

    return src;
}

function handleLoad(event) {

    if (event.id == 'build/common/audios/right.mp3' || event.id == 'build/common/audios/right.ogg') {
        correctSoundInstance = createjs.Sound.createInstance(event.id);
        // console.log('correctSoundInstance')
    }
    if (event.id == 'build/common/audios/wrong.mp3' || event.id == 'build/common/audios/wrong.ogg') {
        wrongSoundInstance = createjs.Sound.createInstance(event.id);
        // console.log('wrongSoundInstance')
    }
}

function handleSuceeded() {
    console.log('The event that is fired when playback has started successfully.');
}

function handleComplete() {
    addedListeners = false;
    console.log('The event that is fired when playback completes.');
}

function handleFailed() {
    addedListeners = false;
    console.log('The event that is fired when playback has failed.');
}

/**
 * this function is used to bring front spirte while dragging
 */
PIXI.Sprite.prototype.bringToFront = function() {
    if (this.parent) {
        var parent = this.parent;
        parent.removeChild(this);
        parent.addChild(this);
    }
}

/**
 * this function is used to bring front container while dragging
 */
PIXI.DisplayObjectContainer.prototype.bringToFront = function() {
    if (this.parent) {
        var parent = this.parent;
        parent.removeChild(this);
        parent.addChild(this);
    }
}

PIXI.Texture.removeTextureFromCache = function(id) {
    var texture = PIXI.TextureCache[id]
    PIXI.TextureCache[id] = null;
    PIXI.BaseTextureCache[id] = null;
    return texture;
}

PIXI.Text.prototype.destroy = function(destroyBaseTexture) {
    this.context = null;
    this.canvas = null;
    this.texture.destroy(destroyBaseTexture);
};

/**
 * splice array on multiple indices (multisplice)
 * @return {array}
 */
/*Array.prototype.multisplice = function() {
    var args = Array.apply(null, arguments);
    args.sort(function(a, b) {
        return a - b;
    });
    for (var i = 0; i < args.length; i++) {
        var index = args[i] - i;
        this.splice(index, 1);
    }
}*/

Array.prototype.remove = function() {
    var what, a = arguments,
        L = a.length,
        ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

/**
 * remove an item – or a group of items – from an array
 * http://ejohn.org/blog/javascript-array-remove/
 * @param  {[type]} from [description]
 * @param  {[type]} to   [description]
 * @return {[type]}      [description]
 */
Array.prototype.removeFromTo = function(from, to) {
    var rest = this.slice((to || from) + 1 || this.length);
    this.length = from < 0 ? this.length + from : from;
    return this.push.apply(this, rest);
};

/**
 * Determine whether an array contains a value
 * arrValues.indexOf('Sam') > -1
 */
if (!Array.indexOf) {
    Array.prototype.indexOf = function(obj) {
        for (var i = 0; i < this.length; i++) {
            if (this[i] == obj) {
                return i;
            }
        }
        return -1;
    }
}

function Common() {}

Common.prototype.showRedOrGreenBox = function(xPos, yPos, obj, duration) {
    obj.position = {
        x: xPos,
        y: yPos
    };

    obj.visible = true;
    obj.bringToFront();

    setTimeout(function() {
        obj.visible = false;
    }, duration || 600);
};

var preload,
    manifest = [];

Common.prototype.init = function() {
    // Create a new queue.
    // preload = new createjs.LoadQueue(true, "uploads/");

    // Use this instead to favor tag loading
    preload = new createjs.LoadQueue(true);

    // createjs.Sound.registerPlugins([createjs.HTMLAudioPlugin]); // need this so it doesn't default to Web Audio
    preload.installPlugin(createjs.Sound);

    preload.on("fileload", function(event) {
        console.log(event)
    });

    preload.on("error", function(event) {
        console.log(event);
    });

    preload.on("complete", function(event) {
        console.log(event);
        main.page.createWallSlices();
    });


};

Common.prototype.startPreload = function(pool) {

    this.pool = pool;

    createjs.Sound.removeAllEventListeners();
    createjs.Sound.removeSounds(manifest);
    manifest.length = 0;

    this.setupManifest();
    this.loadManifest();

    this.pool.length = 0;

};

Common.prototype.setupManifest = function(pool) {

    var images = this.pool.image,
        audios = this.pool.sound,
        len = images.length;

    for (var i = 0; i < len; i++) {
        manifest.push({
            src: "uploads/images/" + images[i],
            id: images[i],
        }, {
            src: "uploads/audios/" + audios[i],
            id: audios[i],
        })
    }

};

Common.prototype.loadManifest = function() {
    preload.loadManifest(manifest);
};

Common.prototype.stop = function() {
    if (preload != null) {
        preload.close();
    }
};

/**
 * shuffle multiple arrays in same way
 */
function shuffle() {
    var length0 = 0,
        length = arguments.length,
        i,
        j,
        rnd,
        tmp;

    for (i = 0; i < length; i += 1) {
        if ({}.toString.call(arguments[i]) !== "[object Array]") {
            throw new TypeError("Argument is not an array.");
        }

        if (i === 0) {
            length0 = arguments[0].length;
        }

        if (length0 !== arguments[i].length) {
            throw new RangeError("Array lengths do not match.");
        }
    }


    for (i = 0; i < length0; i += 1) {
        rnd = Math.floor(Math.random() * i);
        for (j = 0; j < length; j += 1) {
            tmp = arguments[j][i];
            arguments[j][i] = arguments[j][rnd];
            arguments[j][rnd] = tmp;
        }
    }
}

function randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

/**
 * check point exists in rectangle
 * z1,z2 => top left x,y position
 * z3,z4 => bottom right x,y position
 * @return boolean
 */
function isInsideRectangle(x, y, z1, z2, z3, z4) {
    var x1 = Math.min(z1, z3);
    var x2 = Math.max(z1, z3);
    var y1 = Math.min(z2, z4);
    var y2 = Math.max(z2, z4);

    if ((x1 <= x) && (x <= x2) && (y1 <= y) && (y <= y2)) {
        return true;
    } else {
        return false;
    };
};

function createRect(rect) {
    var rect = rect || {},
        graphics = new PIXI.Graphics(),
        x = rect.x || 0,
        y = rect.y || 0,
        w = rect.w || 241,
        h = rect.h || 241,
        borderOnly = rect.borderOnly || false,
        lineWidth = rect.lineWidth || 1,
        lineColor = rect.lineColor || 0x000000,
        alpha = rect.alpha || 1,
        color = rect.color || 0xFFFFFF,
        lineStyle = rect.lineStyle || false;

    if (!borderOnly)
        graphics.beginFill(color);

    if (lineStyle)
        graphics.lineStyle(lineWidth, lineColor);

    graphics.alpha = alpha;
    // draw a rectangle at w/h. The rectangle's top-left corner will
    // be at position x/y
    graphics.drawRect(x, y, w, h);
    graphics.endFill();

    return graphics;

}

function createCircle(circle) {
    var graphics = new PIXI.Graphics(),
        x = circle.x || 0,
        y = circle.y || 0,
        radius = circle.radius || 0,
        color = circle.color || 0xFFFFFF,
        lineStyle = circle.lineStyle || false;

    graphics.beginFill(color);
    graphics.fillAlpha = color.alpha || 1;

    if (lineStyle)
        graphics.lineStyle(1, 0x000000);

    // draw a circle
    graphics.drawCircle(x, y, radius);

    graphics.endFill();

    return graphics;
}

function createLine(line) {

    var graphics = new PIXI.Graphics(),
        lineWidth = line.lineWidth || 1,
        color = line.color || 0x000000,
        alpha = line.alpha || 1,
        x = line.x || 0,
        y = line.y || 0,
        x1 = line.x1 || 50


    // set a fill and line style
    graphics.lineStyle(lineWidth, color, alpha);
    graphics.moveTo(x, y);
    graphics.lineTo(x + x1, y);

    return graphics;
}



/*remove .mp3 from filename. This is temporary, next time we generate JSON we need to send sound filename without extension
 */
function StripExtFromFile(mp3file) {
    var file = mp3file.toString();
    return file.replace(".mp3", "");
}
// color highlight on correct or wrong
var redBox = createRect({
        "alpha": 0.6,
        "color": 0xff0000
    }),
    greenBox = createRect({
        "alpha": 0.6,
        "color": 0x00ff00
    }),
    redSmallBox = createRect({
        "alpha": 0.6,
        "color": 0xff0000,
        "w": 215,
        "h": 98
    }),
    greenSmallBox = createRect({
        "alpha": 0.6,
        "color": 0x00ff00,
        "w": 215,
        "h": 98
    });

function generateUniqueRandomNubers(sets) {
    var arr = [],
        num = sets.total || 11,
        max = sets.max || 100;

    while (arr.length < num) {
        var randomnumber = Math.ceil(Math.random() * max)
        var found = false;
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] == randomnumber) {
                found = true;
                break
            }
        }
        if (!found) arr[arr.length] = randomnumber;
    }
    return arr;
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}


function isPlaying(audelem) {
    return !audelem.paused;
}

var uniqueRandoms = [];
/**
 * Random number out of 5, no repeat until all have been used
 * @param  {integer} numRandoms
 * @return {integer}
 */
function makeUniqueRandom(numRandoms) {
    var numRandoms = numRandoms || 5;
    // refill the array if needed
    if (!uniqueRandoms.length) {
        for (var i = 0; i < numRandoms; i++) {
            uniqueRandoms.push(i);
        }
    }
    var index = Math.floor(Math.random() * uniqueRandoms.length);
    var val = uniqueRandoms[index];

    // now remove that value from the array
    uniqueRandoms.splice(index, 1);

    return val;
}


//Some browsers may not support indexOf. For this following is the workaround
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(searchElement, fromIndex) {
        var k;

        if (this == null) {
            throw new TypeError('"this" is null or not defined');
        }
        var O = Object(this);

        var len = O.length >>> 0;

        if (len === 0) {
            return -1;
        }

        var n = +fromIndex || 0;
        if (Math.abs(n) === Infinity) {
            n = 0;
        }

        if (n >= len) {
            return -1;
        }

        k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

        while (k < len) {
            var kValue;
            if (k in O && O[k] === searchElement) {
                return k;
            }
            k++;
        }
        return -1;
    };
}

//remove duplicates array and return unique
Array.prototype.removeDuplicates = function() {
    var unique = [];

    this.forEach(function(value) {
        if (unique.indexOf(value) === -1) {
            unique.push(value);
        }
    });

    return unique;
}

/**
 * Remove empty elements from an array in Javascript
 * @param  {[type]} deleteValue [description]
 * @return {[type]}             [description]
 */
Array.prototype.clean = function(deleteValue) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};

Array.prototype.randomize = function() { //This is needed because shuffle function in this page cannot randomize array with 2 elements.
    var i = this.length,
        j, temp;
    if (i == 0) return this;
    while (--i) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[i];
        this[i] = this[j];
        this[j] = temp;
    }
    return this;
};

/*Extending javascript array with a method that compares two arrays, if they are identical it returns true*/
Array.prototype.compare = function(testArr) {
    if (this.length != testArr.length) return false;
    for (var i = 0; i < testArr.length; i++) {
        if (this[i].compare) {
            if (!this[i].compare(testArr[i])) return false;
        }
        if (this[i] !== testArr[i]) return false;
    }
    return true;
}

/*Extending javascript array with a method that finds intersecting elements between two arrays, if found then method returns the array of matched elements*/
Array.prototype.diff = function(arr2) {
    var ret = [];
    for (i in this) {
        if (arr2.indexOf(this[i]) > -1) {
            ret.push(this[i]);
        }
    }
    return ret;
};

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

Array.prototype.multisplice = function() {
    var args = Array.apply(null, arguments);
    args.sort(function(a, b) {
        return a - b;
    });
    for (var i = 0; i < args.length; i++) {
        var index = args[i] - i;
        this.splice(index, 1);
    }
}

// generate range of unique random numbers below total value
function uniqueRandomNumbersInRange(total, limit) {

    var total = total || 100;
    var limit = limit || 6;

    var arr = []
    while (arr.length < limit) {
        var randomnumber = Math.ceil(Math.random() * total) - 1
        var found = false;
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] == randomnumber) {
                found = true;
                break
            }
        }
        if (!found) arr[arr.length] = randomnumber;
    }
    return arr;
}

function getNonUnicodeName(name) {
    return name.replace('å', '_ao_').replace('ä', '_ae_').replace('ö', '_oe_');
}