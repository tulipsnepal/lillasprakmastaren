GirlMenu = function() {
    PIXI.DisplayObjectContainer.call(this);
    var self = this;

    sound_help = sound_help || loadSound('uploads/audios/menu_sounds/helpgirl_level0');

    var girlContainer = new PIXI.DisplayObjectContainer();
    girlContainer.position.x = 1330 + 20;
    girlContainer.position.y = 918 - 40;

    //ADDED BY SABIN ON 22ND AUG 2014. FOR HELP BOX, OPENS WHEN LITTLE LANGUAGE MASTER IS CLICKED (IN EXERCISES ONLY). START ---->
    self.helpJSON = {};
    //load json for help text
    var loadJSON = new PIXI.JsonLoader('uploads/exercises/global_help.json?cache=' + (new Date()).getTime());

    loadJSON.on('loaded', function(evt) {
        self.helpJSON = loadJSON.json;

    });

    loadJSON.load();

    var TextContainer = new PIXI.DisplayObjectContainer();
    var HelpContainer = new PIXI.DisplayObjectContainer();
    var HelpImageContainer = new PIXI.DisplayObjectContainer();
    var recommendationMainContainer = new PIXI.DisplayObjectContainer();
    this.addChild(recommendationMainContainer);
    this.showRecommendations(recommendationMainContainer);

    HelpContainer.interactive = true;
    HelpContainer.buttonMode = true;

    HelpContainer.click = HelpContainer.tap = function(data) {
        this.visible = false;
        girlContainer.visible = true;
        self.animateEye.gotoAndStop(0);
        createjs.Sound.stop();
        addedListeners = false;
    }

    var standingGirlContainer = new PIXI.DisplayObjectContainer();
    standingGirlContainer.position.x = 1330 + 20;
    standingGirlContainer.position.y = 918 - 40;

    var standingGirl = PIXI.Sprite.fromFrame("menu_girl_stand.png");
    standingGirl.position = {
        x: 1600 - 1330,
        y: 905 - 918
    };

    var overlay = PIXI.Sprite.fromFrame("overlay.png");
    overlay.position = {
        x: 0,
        y: 0
    };
    overlay.height = 748 * 2;
    overlay.width = 1024 * 2;
    overlay.alpha = 0.8;
    var helpBox = this.drawRectangle(710, 435, 986, 842, 0xFFFFFF, 1, 0X579e35, 25);

    helpBox.interactive = false;

    //STANDING GIRL'S EYE AND MOUTH ANIMATION
    var eyeParams = {
        "image": "pupiller_",
        "length": 2,
        "x": 1768 - 1330,
        "y": 1061 - 918,
        "speed": 0.06,
        "pattern": true
    };
    this.animateEye = new Animation(eyeParams);
    this.animateEye.gotoAndStop(0);

    var mouthParams = {
        "image": "mun_",
        "length": 5,
        "x": 1780 - 1330,
        "y": 1145 - 918,
        "speed": 6,
        "pattern": true,
        "frameId": 5
    };
    this.animateMouth = new Animation(mouthParams);
    this.animateMouth.gotoAndStop(0);
    // <-----  END. ADDED BY SABIN ON 22ND AUG 2014


    var interactiveContainer = new PIXI.DisplayObjectContainer();

    var pixiMenu = PIXI.Sprite.fromFrame("menu_girl_corner.png");
    pixiMenu.buttonMode = true;
    pixiMenu.position = {
        x: 1767 - 1330,
        y: 1200 - 918
    };
    // make the button interactive..
    pixiMenu.interactive = true;

    //ADDED BY SABIN ON 22ND AUG 2014 ---> START
    var title = new PIXI.Text("", {
        font: "50px Arial",
        wordWrap: true,
        wordWrapWidth: 832
    });
    title.position = {
        x: 775,
        y: 525 - 50
    };

    var helpText = new PIXI.Text("", {
        font: "40px Arial",
        wordWrap: true,
        wordWrapWidth: 832
    });

    helpText.position = {
        x: 775,
        y: 615 - 40
    };

    TextContainer.addChild(title);
    TextContainer.addChild(helpText);
    //ADDED BY SABIN ON 22ND AUG 2014. <---- END

    /*THE FOLLOWING CODE WILL BE NEEDED FOR PLAYGROUND MODULE WHEN PLAYER HAS TO DEAL WITH COLORS.
    CLIENT WANTS COLOR NAMES TO COME IN RESPECTIVE COLOR IN HELP TEXT - START--->*/

    var hlpColorCodes = ["#ff0000", "#000000", "#920592", "#0000ff", "#FFFFFF", "#008040", "#ffd405", "#804040", "#ff00ff", "#808080"];
    var hlpColorText = ["röd", "svart", "lila", "blå", "vit", "grön", "gul", "brun", "rosa", "grå"];
    var hlpColorText2 = ["rött", "svart", "lila", "blått", "vitt", "grönt", "gult", "brunt", "rosa", "grått"];
    var bigRectangle = this.drawRectangle(0, 40, 800, 400, 0xffffff, 1, 0xffffff, 0);
    var wordPos = [
        0, 45,
        104, 45,
        0, 90,
        104, 90,
        0, 136,
        104, 136,
        0, 182,
        104, 182,
        0, 224,
        104, 224
    ];
    bigRectangle.position.set(775, helpText.position.y + helpText.height + 10);


    TextContainer.addChild(bigRectangle);
    bigRectangle.visible = false;
    /*<---END*/

    /*LOAD ALPHABETICAL GRID IMAGE & BODY PARTS TO SHOW IN HELP BOX FOR EXERCISE TOYSTORE 4.1 - 4.2 AND PLAYGROUND 4.1 RESPECTIVELY*/
    /*START--->*/
    var alphabetGrid = new PIXI.Sprite.fromImage("build/common/images/toystore/alphabets_grid.png");
    var bodyParts = new PIXI.Sprite.fromImage("build/common/images/playground/body_parts.png");


    /*<---END*/

    pixiMenu.click = pixiMenu.tap = function(data) {
        if (main.CurrentExercise == "") {
            //WE ARE NOT IN EXERCISE SCENE, SO PLAY THE SOUND THAT SAYS THE HELP IS AVAILABLE ONLY WHEN WE'RE IN GAME SCENE
            createjs.Sound.stop();
            createjs.Sound.play(sound_help, {
                interrupt: createjs.Sound.INTERRUPT_NONE
            });
        } else { //WE'RE IN EXERCISE SCENE
            createjs.Sound.stop();
            HelpContainer.visible = true;
            self.animateEye.play();
            girlContainer.visible = false;
            var jsonTitle = self.helpJSON[main.CurrentExercise].title;
            title.setText("\n" + jsonTitle);
            if (main.CurrentExercise == "playground_1_1" || main.CurrentExercise == "playground_1_2") {
                if (bigRectangle.children.length > 0) bigRectangle.removeChildAt(0);
                var arrayColorText = (main.CurrentExercise == "playground_1_1") ? hlpColorText : hlpColorText2;
                for (x = 0; x < hlpColorText.length; x++) {
                    if (x == 4) { //white font can't be seen in white background, so draw black stroke around it
                        var hText = new PIXI.Text(arrayColorText[x], {
                            font: "40px Arial",
                            fill: hlpColorCodes[x],
                            strokeThickness: 6,
                            stroke: "#000000"
                        });
                    } else {
                        var hText = new PIXI.Text(arrayColorText[x], {
                            font: "40px Arial",
                            fill: hlpColorCodes[x]
                        });
                    }


                    hText.position = {
                        x: wordPos[x * 2],
                        y: wordPos[x * 2 + 1]
                    }
                    bigRectangle.addChild(hText);
                    bigRectangle.visible = true;
                }
            } else {
                bigRectangle.removeChildren();
                bigRectangle.visible = false;
            }
            helpText.setText("\n" + self.helpJSON[main.CurrentExercise].help_text);

            //show alphabet image for toy 4.1 and 4.2
            if (main.CurrentExercise == "toy_4_1" || main.CurrentExercise == "toy_4_2" || main.CurrentExercise == "house_1_1" || main.CurrentExercise == "house_1_2" || main.CurrentExercise == "house_1_3" || main.CurrentExercise == "house_2_1" || main.CurrentExercise == "house_2_2" || main.CurrentExercise == "house_2_3") {
                alphabetGrid.position.set(800, 730);
                alphabetGrid.width = 355;
                alphabetGrid.height = 493;
                HelpImageContainer.addChild(alphabetGrid);
            } else if (main.CurrentExercise == "playground_4_1") {
                bodyParts.position.set(800, 610);
                bodyParts.width = 547;
                bodyParts.height = 651;
                HelpImageContainer.addChild(bodyParts);
            } else {
                HelpImageContainer.removeChildren();
            }



            //for house level, we load/play house_1_3_help audios file for all level
            if (main.CurrentExercise.indexOf('house_') > -1) {
                self.animateSound = createjs.Sound.play('build/common/audios/house_1_3_help.' + audioFormat);
            } else {

                if (main.CurrentExercise !== "playground_4_1") { // This exercise doesnot need help audio
                    self.animateSound = createjs.Sound.play('build/common/audios/' + main.CurrentExercise + "_help." + audioFormat);
                }
            }

            if (main.CurrentExercise !== "playground_4_1") {
                if (createjs.Sound.isReady()) {

                    if (main.CurrentExercise === 'toy_1_1') {
                        clearTimeout(main.page.timeoutSittingFigure);
                        main.page.soundPlayed = true;
                    }

                    if (main.CurrentExercise === 'toy_1_2') {
                        clearTimeout(main.page.figureOneSpeaks);
                        clearTimeout(main.page.figureTwoSpeaks);
                        clearTimeout(main.page.figureThreeSpeaks);
                        main.page.enableSpriteInteraction();
                    }

                    self.animateMouth.show();
                }
                self.animateSound.addEventListener("complete", function() {
                    self.animateMouth.hide(0);
                });
            } else {
                self.animateMouth.hide(0);
            }


        }

    }

    girlContainer.addChild(pixiMenu);
    this.addChild(girlContainer);

    //CORNER GIRL'S EYE ANIMATION. ADDED BY SABIN ON 22ND AUG 2014
    var blinkingEyes = {
        "image": "corner_blink_",
        "length": 4,
        "x": 1832 - 1330,
        "y": 1348 - 918,
        "speed": 1,
        "pattern": true
    };
    var eyeClip = new Animation(blinkingEyes);
    eyeClip.show(true);


    girlContainer.addChild(eyeClip);
    this.addChild(HelpContainer);
    HelpContainer.addChild(overlay);
    HelpContainer.addChild(helpBox);
    HelpContainer.addChild(standingGirlContainer);
    HelpContainer.addChild(TextContainer);
    HelpContainer.addChild(HelpImageContainer);

    standingGirlContainer.addChild(standingGirl);
    standingGirlContainer.addChild(this.animateEye);
    standingGirlContainer.addChild(this.animateMouth);
    HelpContainer.visible = false;
}

GirlMenu.constructor = GirlMenu;
GirlMenu.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//ADDED BY SABIN ON 22ND AUG 2014
GirlMenu.prototype.drawRectangle = function(x, y, w, h, color, alpha, lineColor, lineWidth) {
    var graphics = new PIXI.Graphics();

    graphics.beginFill(color || 0xFFFFFF);

    graphics.alpha = alpha || 1;

    if (lineColor && lineWidth) {
        graphics.lineStyle(lineWidth, lineColor);
    }

    graphics.drawRect(x, y, w, h);

    graphics.endFill();

    return graphics;
};


/*
Method to show the recommended System to Run this game application if users' device fails to fulfil the mentioned condition
*/
GirlMenu.prototype.showRecommendations = function(recmmContainer) {
    var recommendation = "";
    var recommendationTitle = "";
    var self = this;
    this.recommendationContainer = new PIXI.DisplayObjectContainer();
    recmmContainer.addChild(this.recommendationContainer);
    //load json for help text
    var loadSysReqJSON = new PIXI.JsonLoader('uploads/exercises/sysreq.json?cache=' + (new Date()).getTime());

    loadSysReqJSON.on('loaded', function(evt) {
        var sysReqJSON = loadSysReqJSON.json;

        recommendationTitle = sysReqJSON.err_title;

        if (Device.localStorage == false) { //Browser doesnot support HTML5 localStorage or user is using private/incognito browsing mode
            recommendation += sysReqJSON.err_localstorage;
        }

        if (Device.iOS == true && Device.iPad == false) { //User using iphone
            recommendation += sysReqJSON.err_no_ipad;
        }

        if (Device.iOS == true && Device.iOSVersion < 7) { //User using device having iOS version less than 7
            var ios_version_error = sysReqJSON.err_ios_version;
            recommendation += ios_version_error.replace("####", Device.iOSVersion);
        }

        if (Device.desktop == true && Device.ie == true && Device.ieVersion < 10) { //Using IE version below IE10
            var ie_version_error = sysReqJSON.err_ie_version;
            recommendation += ie_version_error.replace("####", Device.ieVersion);
        }

        if (Device.desktop == true && Device.ie == true && Device.ieVersion >= 10) { //User using IE recommend to use Chrome or Firefox
            recommendation += sysReqJSON.err_no_ie;
        }

        if (Device.android == true && Device.androidVersion < 4.3 && Device.firefox == false) { //Using Device with Android version less than 4.3
            var android_version_error = sysReqJSON.err_android_version;
            recommendation += android_version_error.replace("####", Device.androidVersion);
        }

        if (Device.android == true && (Device.chrome == false && Device.firefox == false)) { //User using stock browser or other unknown browser on Android
            recommendation += sysReqJSON.err_android_stock_browser;
        }

        if (Device.android == true && Device.firefox == true) { //User using firefox on Android, recommend to make sure their Android version is 4.3 or above
            recommendation += sysReqJSON.err_android_firefox;
        }

        if (Device.windows == true && Device.desktop == true && Device.safari == true) { //User using Safari on windows
            recommendation += sysReqJSON.err_windows_safari;
        }

        if (Device.macOS == true && Device.safari == true && Device.safariVersion < 6) { // User using safari version less than 6
            var error_mac_safari_version = sysReqJSON.err_mac_safari_version;
            recommendation += error_mac_safari_version.replace("####", Device.safariVersion);
        }

        if (Device.isLocalStorageWritable == false && Device.localStorage == true) {
            recommendation += sysReqJSON.err_partial_localstorage;
        }
        /* recommendation = "- Your browser doesnot support HTML5 local storage, or you might be using private or incognito browsing mode. Please use browser that supports local storage. If it does switch to normal browsing mode. \n\n";
         recommendation += "- Safari for windows is not supported properly. Use Google Chrome or Mozilla Firefox for smooth performance. \n";

         recommendationTitle = "System Requirement Recommendation.";*/

        recommendation = recommendation.replace(/^\s+|\s+$/g, ""); //Remove trailing whitespaces and newlines

        if (Device.localStorage == true) {
            var currentStatus = localStorage.getItem("dont_show_recommendation");
        } else {
            var currentStatus = 1;
        }


        if (recommendation != "") { //WE HAVE RECOMMENDATION SHOW REQUIREMENT BOX.

            if (currentStatus == 1) { //if user has selected not to show recommendation again, just hide the container
                self.recommendationContainer.visible = false;
            }

            //Show setting to trigger recommendation box.
            var settingsButton = new PIXI.Sprite.fromFrame("info_icon.png");
            settingsButton.width = settingsButton.height = 48;
            settingsButton.alpha = 0.5;
            settingsButton.interactive = true;
            settingsButton.buttonMode = true;
            settingsButton.position.set(1024 * 2 - settingsButton.width - 20, 20);
            recmmContainer.addChild(settingsButton);
            settingsButton.click = settingsButton.tap = function() {
                self.recommendationContainer.visible = true;
            };


            var titleFontSize = "",
                reFontSize = "",
                wordWrapThing = 0,
                dontShowWidth = 0,
                closeButtonWidth = 0,
                checkboxBottomMargin = 0,
                dontShowFont = "30px",
                dontShowFrontSpace = "",
                dontShowBottomMargin = 0,
                closeButtonMarginRight = 0,
                dontShowHeight = 0;



            if (Device.desktop == false && Device.iPad == false) {
                titleFontSize = "65px";
                reFontSize = "50px";
                wordWrapThing = 1300;
                dontShowWidth = 55;
                dontShowHeight = 49;
                closeButtonWidth = 70;
                checkboxBottomMargin = 70;
                dontShowFont = "40px";
                dontShowBottomMargin = 60;
                dontShowFrontSpace = "        ";
                closeButtonMarginRight = 80;
            } else {
                titleFontSize = "30px";
                reFontSize = "25px";
                wordWrapThing = 1000;
                dontShowWidth = 41;
                dontShowHeight = 37;
                closeButtonWidth = 55;
                checkboxBottomMargin = 50;
                dontShowFont = "25px";
                dontShowBottomMargin = 40;
                dontShowFrontSpace = "        ";
                closeButtonMarginRight = 70;
            }
            //Recommendation box title
            var recomTitle = new PIXI.Text(recommendationTitle, {
                font: "bold " + titleFontSize + " Arial",
                fill: "#9eebfe"
            });

            recomTitle.position = {
                x: 40,
                y: 30
            };

            //Recommendation Notification text
            var recomText = new PIXI.Text(recommendation, {
                font: reFontSize + " Arial",
                fill: "white",
                wordWrap: true,
                wordWrapWidth: wordWrapThing
            });

            recomText.position = {
                x: 40,
                y: recomTitle.height + 60
            };

            //Recommendation box itself
            var recomBox = createRect({
                w: recomText.width + 100,
                h: recomText.height + recomTitle.height + 180,
                color: 0x0281a1
            });

            //Recommendation box arrow for speech bubble effect
            var recomBoxArrow = new PIXI.Sprite.fromFrame("speech_arrow.png");
            recomBoxArrow.position = {
                x: recomBox.width - 80,
                y: recomBox.height
            };

            //Recommendation box close button
            var recomBoxClose = new PIXI.Sprite.fromFrame("close.png");
            recomBoxClose.width = recomBoxClose.height = closeButtonWidth;
            recomBoxClose.position = {
                x: recomBox.width - closeButtonMarginRight,
                y: 10
            };
            recomBoxClose.interactive = true;
            recomBoxClose.buttonMode = true;

            recomBoxClose.click = recomBoxClose.tap = function() {
                self.recommendationContainer.visible = false;
            }

            //line for footer

            var lineBox = createLine({
                x: 0,
                y: recomBox.height - checkboxBottomMargin - 15,
                color: 0xffffff,
                x1: recomBox.width
            });
            lineBox.alpha = 0.8;



            /*dontShowSprite.interactive = true;
            dontShowSprite.buttonMode = true;*/
            //dont show text

            if (Device.localStorage == true && Device.isLocalStorageWritable == true) { //local storage dependent stuffs within the condition
                //Don't show this Button
                var textureUnchecked = new PIXI.Texture.fromFrame("unchecked.png");
                var textureChecked = new PIXI.Texture.fromFrame("checked.png");

                var dontShowSprite = new PIXI.Sprite(currentStatus == 1 ? textureChecked : textureUnchecked);
                dontShowSprite.width = dontShowWidth;
                dontShowSprite.height = dontShowHeight;
                dontShowSprite.position = {
                    x: 40,
                    y: recomBox.height - checkboxBottomMargin
                };

                var dontShowText = new PIXI.Text(dontShowFrontSpace + sysReqJSON.err_dont_show_text, {
                    font: dontShowFont + " Arial",
                    fill: "#9eebfe"
                });
                dontShowText.position.set(40, recomBox.height - dontShowBottomMargin);
                dontShowText.interactive = true;
                dontShowText.buttonMode = true;



                dontShowText.click = dontShowText.tap = function() {
                    if (currentStatus != 1) {
                        localStorage.setItem("dont_show_recommendation", 1);
                        currentStatus = 1;
                        dontShowSprite.setTexture(textureChecked);
                    } else {
                        localStorage.setItem("dont_show_recommendation", 0);
                        currentStatus = 0;
                        dontShowSprite.setTexture(textureUnchecked);
                    }
                }

                recomBox.addChild(dontShowText);
                recomBox.addChild(dontShowSprite);
            }



            //Main recommendation box
            recomBox.alpha = 0.9;
            recomBox.addChild(lineBox);

            recomBox.addChild(recomBoxArrow);
            recomBox.addChild(recomTitle);
            recomBox.addChild(recomText);
            recomBox.addChild(recomBoxClose);
            recomBox.position = {
                x: 1024 * 2 - recomBox.width - 100,
                y: 748 * 2 - 336 - 60 - recomBox.height
            };
            self.recommendationContainer.addChild(recomBox);

        }

    });

    loadSysReqJSON.load();



}