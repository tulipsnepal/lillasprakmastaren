// JavaScript Document
var ScaleCanvas = function(containerObj) {
    this.containerObj = containerObj;
    this.stageRenderer = autoDetectRenderer;
    this.defaultWidth = 1024;
    this.defaultHeight = 748;
    this.defaultScale = 1;
    this.OnOrientationChange();
    window.addEventListener("orientationchange", this.OnOrientationChange.bind(this));
}

ScaleCanvas.prototype.OnOrientationChange = function() {

    if (window.innerHeight > window.innerWidth) { //Device is in portrait mode
        this.screenWidth = window.innerWidth;
        this.screenHeight = (window.innerWidth * this.defaultHeight) / this.defaultWidth;
        this.newScale = this.screenWidth / this.defaultWidth;
    } else { //device is in landscape mode
        this.screenHeight = window.innerHeight;
        this.screenWidth = (window.innerHeight * this.defaultWidth) / this.defaultHeight;
        this.newScale = this.screenHeight / this.defaultHeight;
    }

    //IF BROWSED VIA COMPUTER
    /*if (window.innerWidth >= this.defaultWidth && window.innerHeight >= this.defaultHeight) {
        // this.screenHeight = this.defaultHeight;
        // this.screenWidth = this.defaultWidth;
        // this.newScale = 1;
        this.screenWidth = 1280;
        this.screenHeight = (this.screenWidth * this.defaultHeight) / this.defaultWidth;
        this.newScale = 1 + ((this.screenWidth - this.defaultWidth) * 100 / this.defaultWidth) / 100;
    }*/

    this.containerObj.scale = {
        x: this.newScale,
        y: this.newScale
    };

    //Resize the Canvas according to the size of Display Object Container so that whole things comes horizontally centralized.
    this.stageRenderer.resize(this.screenWidth * 2, this.screenHeight * 2);
    // set the canvas width and height to fill the screen
    this.stageRenderer.view.style.display = "block";
    this.stageRenderer.view.style.width = this.screenWidth + 'px';
    this.stageRenderer.view.style.height = this.screenHeight + 'px';

}
