var History = function() {
    window.addEventListener("popstate", this.PopState.bind(this));
    this.HistorySetForOtherPages = false;
}

History.prototype.PopState = function(e) {
    localStorage.setItem('pushother', 0);
    if (e.state != null) {
        // console.log('History fired back -  ' + e.state.scene);
        var scene = e.state.scene || "";

        if (scene != "" && scene.indexOf("history_other") > -1) {
            this.ShowScene(scene.replace("_other", ""))
        } else {
            this.ShowScene(e.state);
        }
    }
}

History.prototype.ShowScene = function(dataObj) {
    if (dataObj == null) {
        return;
    }
    if (typeof dataObj.scene == "undefined") {
        main.sceneChange(dataObj.replace("history_", ""));
    } else {
        main.sceneChange(dataObj.scene.replace("history_", ""));
    }

}

History.prototype.PushState = function(param) {
    var scene = param.scene || "";

    if (scene != "" && scene.indexOf("history_other") > -1) {
        var pushstorage = localStorage.getItem('pushother');
        if (pushstorage > 0) return false;
        localStorage.setItem('pushother', 1);
        if (this.HistorySetForOtherPages == false) {
            this.HistorySetForOtherPages = true;
            history.pushState({
                scene: param.scene
            }, document.title, document.location.href);
        }
    } else {
        history.pushState(param, document.title, document.location.href);
    }
}

History.prototype.ReplaceState = function(param) {
    history.replaceState(param, document.title, document.location.href);
    // console.log("History Overwritten - " + param.scene);
}
