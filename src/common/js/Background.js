Background = function(image){
// we need to make sure our own class take arguments and uses them to initialize the DisplayObjectContainer functionality,
  // as our class inherits from PIXI.DisplayObjectContainer
  // this keyword lets you refer to a created instance of your class.
  // And through this we can reference all the properties and methods of that instance.
  PIXI.DisplayObjectContainer.call( this );  

  // define member variable
    this.image = image;
     if(!this.image || !this.image.length) {
        throw 'You have to specifie "image" params for S.Background()';
    }
     //create the textures needed
	tmpBkg = new PIXI.Sprite(PIXI.Texture.fromImage(this.image));
	tmpBkg.scale.x = 1;
  tmpBkg.scale.y = 1;
  this.addChild(tmpBkg);
}

// these two lines set the constructor to be our Background function
// and also inherit the feature of PIXI.DisplayObjectContainer class
Background.constructor = Background;
Background.prototype = Object.create( PIXI.DisplayObjectContainer.prototype );