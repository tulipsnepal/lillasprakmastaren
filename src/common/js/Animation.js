Animation = function(param) {

    var tmpTextures = [],
        sequence = null,
        len = null;

    if (param.pattern) { //mouth animation has pattern and loop
        if (param.length == 3) {
            sequence = param.sequence || [1, 2, 3, 1, 3, 2, 3, 2, 1, 3, 1, 2, 1, 2, 3, 1, 2, 1, 3];
        } else if (param.length == 4) {
            //sequence = [1, 2, 3, 4, 3, 1, 3, 2, 3, 4, 3, 2, 3, 3, 3, 1, 3, 2, 3, 2, 3, 4, 3, 2, 3, 1, 4];
            sequence = param.sequence || [1, 2, 3, 4, 3, 1, 3, 2, 3, 4, 3, 2, 3, 3, 3, 1, 3, 2, 3, 2, 3, 4, 3, 2, 3, 1, 4];
        } else if (param.length == 5) {
            sequence = param.sequence || [1, 2, 3, 4, 5, 1, 5, 3, 2, 4, 2, 5, 3, 1, 4, 1, 3, 5, 2, 4, 4];
        } else {
            sequence = param.sequence || [1, 2, 1];
        }
    } else {

        sequence = [];
        for (var i = 1; i <= param.length; i++) {
            sequence.push(i);
        }


    }

    len = sequence.length || 2;

    for (var i = 0; i < len; i++) {
        if (param.fromImage) {
            tmpTextures.push(PIXI.Texture.fromImage(param.imageFolder + param.image + (sequence[i]) + ".png"));
        } else {
            tmpTextures.push(PIXI.Texture.fromFrame(param.image + (sequence[i]) + ".png"));
        }
    }

    this.textures = tmpTextures;
    main.animationTextures = tmpTextures;

    PIXI.MovieClip.call(this, this.textures);

    this.position.x = param.x;
    this.position.y = param.y;

    this.animationSpeed = param.speed || 1;

    this.timeAnim = Date.now();

    this.maxFrame = this.textures.length;

    this.visible = true;
    this.running = false;
    this.frameToStopAt = 0;

    this.frameId = param.frameId || 0;

    this.runonce = false;

    this.counter = 1;

    this.gotoAndStop(this.frameId);

    this.show_reset = false;

}

Animation.constructor = Animation;
Animation.prototype = Object.create(PIXI.MovieClip.prototype);

Animation.prototype.show = function() {

    this.running = true;
    this.visible = true;
}

Animation.prototype.showOnce = function(stopAt) {

    this.show();
    this.runonce = true;

    this.frameToStopAt = stopAt || 0;

}

Animation.prototype.hide = function(frameId) {

    this.running = false;
    this.runonce = false;

    this.counter = 1;

    if (!frameId) frameId = this.frameId;


    if (this.frameToStopAt) {
        var obj = this;
        var a = setTimeout(function() {
            obj.visible = false;
        }, 1000);

        // this.gotoAndStop(this.frameToStopAt);
    } else {
        this.gotoAndStop(frameId);
    }
}

Animation.prototype.showAndReset = function() {
    this.show();

    this.show_reset = true;
}

Animation.prototype.updateTransform = function() {

    PIXI.MovieClip.prototype.updateTransform.call(this);

    if (this.runonce && this.counter >= this.maxFrame)
        this.hide();

    if (this.timeAnim < Date.now() - 1000 / this.animationSpeed) {

        if (!this.running) return;

        if (!this.playing) this.currentFrame++;

        if (this.currentFrame > this.maxFrame) this.currentFrame = 1;

        if (this.show_reset == true) {

            if (this.currentFrame == this.maxFrame) {
                this.hide(0);
                this.visible = false;
                return;
            }
        }

        if (this.running) {
            this.counter++;
            this.gotoAndStop(this.currentFrame);
        } else {
            this.gotoAndStop(this.frameId);
        }

        this.timeAnim = Date.now();

    }

}