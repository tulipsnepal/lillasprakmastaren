function Playground_3_1() {
    cl.show()

    PIXI.DisplayObjectContainer.call(this);

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position.set(77, 88);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();

    localStorage.setItem('playground_3_1', 0);

    main.CurrentExercise = "playground_3_1";

    main.eventRunning = false;

    this.fromOutro = 0;

    main.texturesToDestroy = [];

    this.common = new Common();

    this.loadSpriteSheet();

    // everything connected to this is rendered.
    this.addChild(this.sceneContainer);
}

Playground_3_1.constructor = Playground_3_1;
Playground_3_1.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Playground_3_1.imagePath = 'build/exercises/playground/playground_3_1/images/';
Playground_3_1.audioPath = 'build/exercises/playground/playground_3_1/audios/';
Playground_3_1.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

/**
 * load array of assets of different format
 */
Playground_3_1.prototype.loadSpriteSheet = function() {
    this.introId = loadSound(Playground_3_1.audioPath + 'playpark_3_1_intro');
    this.outroId = loadSound(Playground_3_1.audioPath + 'playpark_3_1_outro');
    // create an array of assets to load
    this.assetsToLoad = [
        Playground_3_1.imagePath + "sprite_3_1.json",
        Playground_3_1.imagePath + "sprite_3_1.png",
        Playground_3_1.imagePath + "wobble_1.png",
        Playground_3_1.imagePath + "wobble_2.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
Playground_3_1.prototype.spriteSheetLoaded = function() {

    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    // fill exercise background
    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0xd6def2);
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xcfc3d5);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1201);
    this.sceneContainer.addChild(this.sceneBackground);

    // fill exercise background layer
    this.grayLayer = new PIXI.Graphics();
    this.grayLayer.beginFill(0x878786);
    // draw a rectangle
    this.grayLayer.drawRect(0, 834, 1843, 367);
    this.sceneContainer.addChild(this.grayLayer);

    // exercise instruction
    this.help = PIXI.Sprite.fromFrame("help.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);
    this.sceneContainer.addChild(this.help);

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.loadScene();
    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.outroContainer);

    this.loadExercise();

}

Playground_3_1.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    };
    if (isNumeric(number))
        this.hands[number].visible = true;
}

/**
 * play intro animation
 */
Playground_3_1.prototype.introAnimation = function() {

    createjs.Sound.stop();
    main.overlay.visible = true;

    if (this.fromOutro == 1) {
        this.fromOutro = 0;
        this.resetExercise();
        this.getNewExerciseSet();
    }

    this.help.interactive = false;
    this.help.buttonMode = false;

    this.mouthParams.show();
    this.ovaigen.visible = false;

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
 0-1 sec arms_0
1-2.5 sec arms_1
2.5-4 sec arms_2
stop at arms_0

 */
Playground_3_1.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    console.log(currentPostion)
    if (currentPostion >= 0 && currentPostion < 2500) {
        this.showHandAt(0);
    } else if (currentPostion >= 2500 && currentPostion < 4000) {
        this.showHandAt(1);
    } else if (currentPostion >= 4000 && currentPostion < 7755) {
        this.showHandAt(2);
    } else {
        this.showHandAt(0);
    }
};

/**
 * cancel running intro animation
 */
Playground_3_1.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);

    this.showHandAt(0);
    this.mouthParams.hide();
    this.help.interactive = true;
    this.help.buttonMode = true;

    this.ovaigen.visible = false;
    main.overlay.visible = false;
}

Playground_3_1.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.mouthParamsOutro.visible = true;
        self.mouthParamsOutro.show();
    }
    this.cancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.cancelHandler);
}


Playground_3_1.prototype.cancelOutro = function() {

    var self = this;

    createjs.Sound.stop();
    self.mouthParamsOutro.hide();
    self.ovaigen.visible = true;
    self.help.interactive = true;
    self.help.buttonMode = true;
    main.overlay.visible = false;

};

/**
 * create sprites,animation,sound related to intro scene
 */
Playground_3_1.prototype.loadScene = function() {
    var self = this;

    this.tyreTexture = new PIXI.Texture.fromFrame('tyre.png');

    this.tyre1 = new PIXI.Sprite(this.tyreTexture);
    this.tyre1.position.set(135, 923);
    main.texturesToDestroy.push(this.tyre1);

    this.tyrebase1 = new PIXI.Sprite.fromFrame('tyrebase1.png');
    this.tyrebase1.position.set(70, 964);
    main.texturesToDestroy.push(this.tyrebase1);

    this.tyre2 = new PIXI.Sprite(this.tyreTexture);
    this.tyre2.position.set(1123, 923);
    main.texturesToDestroy.push(this.tyre2);

    this.tyrebase2 = new PIXI.Sprite.fromFrame('tyrebase2.png');
    this.tyrebase2.position.set(1059, 977);
    main.texturesToDestroy.push(this.tyrebase2);

    this.introContainer.addChild(this.tyre1);
    this.introContainer.addChild(this.tyrebase1);

    this.introContainer.addChild(this.tyre2);
    this.introContainer.addChild(this.tyrebase2);

    this.textureBalance = new PIXI.Texture.fromImage(Playground_3_1.imagePath + 'balanced.png');
    this.textureWobble0 = new PIXI.Texture.fromFrame('introbody.png');
    this.textureBalanceOutro = new PIXI.Texture.fromFrame('outrobody.png');
    this.textureWobble1 = new PIXI.Texture.fromFrame(Playground_3_1.imagePath + 'wobble_1.png');
    this.textureWobble2 = new PIXI.Texture.fromFrame(Playground_3_1.imagePath + 'wobble_2.png');

    this.wobble = new PIXI.Sprite(this.textureWobble0);
    this.wobble.position.set(100, 96);

    this.introContainer.addChild(this.wobble);

    this.hands = [
        new PIXI.Sprite.fromFrame('arms1.png'),
        new PIXI.Sprite.fromFrame('arms2.png'),
        new PIXI.Sprite.fromFrame('arms3.png')
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(417, 359);
        this.introContainer.addChild(this.hands[i]);
        if (i != 0) this.hands[i].visible = false;
    };


    this.mouthParams = new Animation({
        "image": "mouth",
        "length": 5,
        "x": 649,
        "y": 364,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 2, 4, 1, 3],
        "frameId": 0
    });

    this.introContainer.addChild(this.mouthParams);

    this.mouthParamsOutro = new Animation({
        "image": "outro_mouth_",
        "length": 4,
        "x": 281,
        "y": 462,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 2, 4, 1, 3],
        "frameId": 0
    });

    this.introContainer.addChild(this.mouthParamsOutro);
    this.mouthParamsOutro.visible = false;

    this.eyeParams = new Animation({
        "image": "eyes",
        "length": 3,
        "x": 633,
        "y": 320,
        "speed": 2,
        "pattern": true,
        "sequence": [1, 2, 2, 1, 1, 2, 2, 1, 2, 1, 1, 2],
        "frameId": 0
    });
    this.introContainer.addChild(this.eyeParams);

    // back to intro page
    this.ovaigen = PIXI.Sprite.fromFrame('ova_igen.png');
    this.ovaigen.position.set(540, 462);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.ovaigen.visible = false;
    this.introContainer.addChild(this.ovaigen);
}

Playground_3_1.prototype.animateSpriteInitialPosition = function() {
    this.eyeParams.position.set(633, 320);
    this.mouthParams.position.set(649, 364);

    for (var i = 0; i < 3; i++) {
        this.hands[i].position.set(417, 359);
    };
};

Playground_3_1.prototype.animateSpriteBalancePosition = function() {
    this.eyeParams.position.set(692, 274);
    this.mouthParams.position.set(710, 314);

    for (var i = 0; i < 3; i++) {
        this.hands[i].position.set(476, 313);
    };
};

/**
 * load exercise related stuffs
 */
Playground_3_1.prototype.loadExercise = function() {

    this.exerciseBaseContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.addChild(this.exerciseBaseContainer);

    this.textboxTexture = new PIXI.Texture.fromFrame('textbox.png');

    //create 3 textbox
    this.destPosition = [];
    this.tintSpriteBox = [];

    for (var i = 0; i < 3; i++) {

        var textbox = new PIXI.Sprite(this.textboxTexture);
        textbox.position.set(974, 362 + (i * 90));
        main.texturesToDestroy.push(textbox);
        this.exerciseBaseContainer.addChild(textbox);

        this.destPosition.push(textbox.position);

        this.tintSpriteBox.push(textbox);
    };

    //creatre 8 answer white boxes in vertical postion
    this.whiteGraphics = createRect({
        w: 288,
        h: 91
    });
    this.whiteTexture = this.whiteGraphics.generateTexture();

    this.srcPosition = [];
    for (var j = 0; j < 8; j++) {
        var whiteBox = new PIXI.Sprite(this.whiteTexture);
        whiteBox.position.set(1364, 191 + (j * 107));
        main.texturesToDestroy.push(whiteBox);
        this.exerciseBaseContainer.addChild(whiteBox);

        this.srcPosition.push({
            x: whiteBox.position.x,
            y: whiteBox.position.y - 40
        });
    };

    this.categoryPosition = {
        balanced: [10, 358],
        normal: [10, 464]
    }

    /*this.circleSprite = new PIXI.Sprite.fromFrame('circle.png');
    this.circleSprite.position.set(9, 464);
    this.exerciseBaseContainer.addChild(this.circleSprite);*/

    // generate feedback
    this.feedbackContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.feedbackContainer);

    this.generateFeedback();

    // create exercise container to show dynamic sets
    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.addChild(this.exerciseContainer);

    this.answerContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.addChild(this.answerContainer);

    this.initObjectPooling();
}

Playground_3_1.prototype.setBoxPosition = function(yPos, catPos) {
    for (var k = 0; k < 3; k++) {
        //362
        this.tintSpriteBox[k].position.set(974, yPos + (k * 90));

        if (this.selectedAnswers[k] != "") {
            this.selectedAnswers[k].position.x = this.tintSpriteBox[k].position.x + 14;
            this.selectedAnswers[k].position.y = this.tintSpriteBox[k].position.y + 20;
        }
    }

    if (catPos == "normal") {
        this.imageSprite.position.set(this.categoryPosition.normal[0], this.categoryPosition.normal[1]);
    } else {
        this.imageSprite.position.set(this.categoryPosition.balanced[0], this.categoryPosition.balanced[1]);
    }
};

Playground_3_1.prototype.runExercise = function() {

    var self = this;

    this.jsonData = this.wallSlices[0];
    console.info("answer ", this.jsonData.answer);

    this.evaluateAnswer = [];

    this.selectedAnswers = ["", "", ""];

    this.availableIndex = [0, 1, 2];

    this.buttonTextArr = [];
    this.buttonArr = [];
    this.removeArr = [];

    /*this.imagePositionX = [
        63,
        255,
        150,
    ];
    this.imagePositionY = [
        552,
        590,
        685
    ];

    shuffle(this.imagePositionY, this.imagePositionY);*/

    /*for (var i = 0; i < 3; i++) {
        var imageSprite = this.jsonData.image[i];
        imageSprite.position.set(this.imagePositionX[i], this.imagePositionY[i]);
        imageSprite.width = 130;
        imageSprite.height = 146;
        this.exerciseContainer.addChild(imageSprite);
    };*/

    this.imageSprite = this.jsonData.image;
    this.imageSprite.position.set(this.categoryPosition.normal[0], this.categoryPosition.normal[1]);
    this.exerciseContainer.addChild(this.imageSprite);

    this.soundSprite = new PIXI.Sprite.fromFrame('sound_grey.png');
    this.soundSprite.position.set(40, 320);
    this.soundSprite.interactive = true;
    this.soundSprite.buttonMode = true;
    this.imageSprite.addChild(this.soundSprite);



    this.soundSprite.click = this.soundSprite.tap = this.playSoundSprites.bind(this);

    for (var i = 0; i < 8; i++) {

        var wordText = new PIXI.Text(this.jsonData.word[i], {
            'font': '50px Arial'
        });
        wordText.position.x = this.srcPosition[i].x + 30;
        wordText.position.y = this.srcPosition[i].y + 26 + 40;
        wordText.hitOnce = false;
        wordText.interactive = true;
        wordText.buttonMode = true;
        wordText.hitIndex = i;
        this.exerciseContainer.addChild(wordText);

        this.buttonTextArr.push(wordText.text.replace(/(?:\r\n|\r|\n)/g, ''));
        this.buttonArr.push(wordText);

        wordText.click = wordText.tap = function(data) {

            if (addedListeners) return;

            createjs.Sound.stop();

            if (this.hitOnce) {
                this.position.x = self.srcPosition[this.hitIndex].x + 30;
                this.position.y = self.srcPosition[this.hitIndex].y + 26 + 40;
                self.availableIndex.push(this.popIndex);

                self.evaluateAnswer.remove(this.text.replace(/(?:\r\n|\r|\n)/g, ''));

                this.hitOnce = false;

            } else {

                var nextIndex = self.getIndexToInsert();

                this.position.x = self.destPosition[nextIndex].x + 14;
                this.position.y = self.destPosition[nextIndex].y + 20
                this.popIndex = nextIndex;

                self.availableIndex.splice(0, 1);

                self.evaluateAnswer.push(this.text.replace(/(?:\r\n|\r|\n)/g, ''));

                this.hitOnce = true;
            }

            if (self.availableIndex.length < 1) {
                self.evaluateExercise();
            }

        }

    };

};

Playground_3_1.prototype.playSoundSprites = function() {

    if (addedListeners) return;

    var self,
        soundInstance0 = createjs.Sound.createInstance(this.jsonData.sound);
    /*,
        soundInstance1 = createjs.Sound.createInstance(this.jsonData.sound[1]),
        soundInstance2 = createjs.Sound.createInstance(this.jsonData.sound[2]);*/

    soundInstance0.play({
        delay: 0
    });

    /*soundInstance0.addEventListener('complete', function() {

        soundInstance1.play({
            delay: 1000
        });

        soundInstance1.addEventListener('complete', function() {

            soundInstance2.play({
                delay: 1000
            });

        });

    })*/
};

Playground_3_1.prototype.getIndexToInsert = function() {
    this.availableIndex.sort();
    for (var i = 0; i < this.availableIndex.length; i++) {
        if (this.availableIndex[i] >= 0) {
            return this.availableIndex[i];
        };
    };
};

Playground_3_1.prototype.setWordInteractive = function(bool) {

    if (bool) {
        for (var i = 0; i < this.buttonArr.length; i++) {
            this.buttonArr[i].interactive = true;
            this.buttonArr[i].buttonMode = true;
        };

    } else {
        for (var i = 0; i < this.buttonArr.length; i++) {
            this.buttonArr[i].interactive = false;
            this.buttonArr[i].buttonMode = false;
        };
    }
};

Playground_3_1.prototype.evaluateExercise = function() {

    this.setWordInteractive(false);

    var self = this,
        exactAnswer = this.jsonData.answer,
        evaluateAnswer = this.evaluateAnswer,
        correctAnswer = [],
        correctCount = 0;


    for (var i = 0; i < exactAnswer.length; i++) {
        var evaluateIndex = exactAnswer.indexOf(evaluateAnswer[i]),
            matchIndexOf = this.buttonTextArr.indexOf(evaluateAnswer[i]),
            popIndex = this.buttonArr[matchIndexOf].popIndex;

        if (evaluateIndex > -1) { //correct evaluation one by one

            /* this.tintSpriteBox[popIndex].tint = '0x00ff00';
             this.tintSpriteBox[popIndex].alpha = 0.6;*/
            correctAnswer.push(popIndex);
            this.buttonArr[matchIndexOf].interactive = false;
            this.buttonArr[matchIndexOf].buttonMode = false;
            self.selectedAnswers[popIndex] = this.buttonArr[matchIndexOf];
            correctCount++;

        } else { //wrong evaluation one by one

            this.tintSpriteBox[popIndex].tint = '0xff0000';
            this.tintSpriteBox[popIndex].alpha = 0.6;

            this.availableIndex.push(popIndex);
            this.removeArr.push(matchIndexOf);
        }

    };

    if (correctCount >= 3) { // when all 3 are correct

        setTimeout(this.correctFeedback.bind(this), 1000);
        correctCount = 0;
    } else {
        wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
        for (var k = 0; k < correctAnswer.length; k++) {
            this.tintSpriteBox[correctAnswer[k]].tint = '0x00ff00';
            this.tintSpriteBox[correctAnswer[k]].alpha = 0.6;
        }
        setTimeout(this.wrongFeedback.bind(this), 1000);
    }

    setTimeout(this.tintWhite.bind(this), 1000);

};

Playground_3_1.prototype.unsetBalance = function() {
    this.eyeParams.visible = true;
    this.mouthParams.visible = true;
    this.showHandAt(0);
    this.wobble.setTexture(this.textureWobble0);

    this.setBoxPosition(362, "normal");
    this.animateSpriteInitialPosition();

};

Playground_3_1.prototype.setBalance = function() {
    this.eyeParams.visible = true;
    this.mouthParams.visible = true;
    this.showHandAt(0);
    this.wobble.setTexture(this.textureBalance);

    this.setBoxPosition(462, "balanced");
    this.animateSpriteBalancePosition();

};

Playground_3_1.prototype.setBalanceOutro = function() {
    this.eyeParams.visible = false;
    this.mouthParams.visible = false;
    this.showHandAt();
    this.mouthParamsOutro.visible = true;
    this.wobble.setTexture(this.textureBalanceOutro);
};



Playground_3_1.prototype.setWobble1 = function() {
    this.wobble.setTexture(this.textureWobble1);
    this.eyeParams.visible = false;
    this.mouthParams.visible = false;
    this.showHandAt();
};

Playground_3_1.prototype.setWobble2 = function() {
    this.wobble.setTexture(this.textureWobble2);
    this.eyeParams.visible = false;
    this.mouthParams.visible = false;
    this.showHandAt();
};

Playground_3_1.prototype.tintGreen = function() {
    for (var i = 0; i < this.tintGreenArr; i++) {
        this.tintGreenArr[i].tint = '0x00ff00';
        this.tintGreenArr[i].alpha = 0.6;
    };
    setTimeout(this.tintWhite.bind(this), 1000);
}

Playground_3_1.prototype.tintRed = function() {
    for (var i = 0; i < this.tingRedArr; i++) {
        this.tingRedArr[i].tint = '0xff0000';
        this.tingRedArr[i].alpha = 0.6;
    };
    setTimeout(this.tintWhite.bind(this), 1000);
}

Playground_3_1.prototype.tintWhite = function() {
    for (var i = 0; i < 3; i++) {
        this.tintSpriteBox[i].tint = '0xFFFFFF';
        this.tintSpriteBox[i].alpha = 1;
    };
}

/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
Playground_3_1.prototype.correctFeedback = function() {

    var self = this;

    var feedback = localStorage.getItem('playground_3_1');
    this.hiddenFeedback[feedback].alpha = 1;
    feedback++;
    localStorage.setItem('playground_3_1', feedback);

    this.setBalance();
    var c = setTimeout(function() {
        correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
        for (var k = 0; k < 3; k++) {
            self.tintSpriteBox[k].tint = '0x00ff00';
            self.tintSpriteBox[k].alpha = 0.6;
        }
    }, 600);

    var a = setTimeout(this.unsetBalance.bind(this), 1800);
    if (feedback >= Playground_3_1.totalFeedback) {
        var b = setTimeout(this.onComplete.bind(this), 2400);
    } else {
        var b = setTimeout(this.getNewExerciseSet.bind(this), 2000);
    }

}

Playground_3_1.prototype.wrongFeedback = function() {

    var self = this;

    for (var i = 0; i < this.removeArr.length; i++) {
        var removeIndexOf = this.removeArr[i];

        this.buttonArr[removeIndexOf].position.x = this.srcPosition[removeIndexOf].x + 30;
        this.buttonArr[removeIndexOf].position.y = this.srcPosition[removeIndexOf].y + 26 + 40;

        this.buttonArr[removeIndexOf].hitOnce = false;

        this.evaluateAnswer.remove(this.buttonArr[removeIndexOf].text.replace(/(?:\r\n|\r|\n)/g, ''));

    };
    this.removeArr.length = 0;

    var feedback = localStorage.getItem('playground_3_1');
    feedback--;
    if (feedback >= 0) {
        this.hiddenFeedback[feedback].alpha = 0.2;
        localStorage.setItem('playground_3_1', feedback);
    };

    setTimeout(this.setWobble1.bind(this), 400);
    setTimeout(this.setBalance.bind(this), 600);
    setTimeout(this.setWobble2.bind(this), 800);
    setTimeout(this.setBalance.bind(this), 1000);
    setTimeout(this.unsetBalance.bind(this), 1200);

    setTimeout(function() {
        self.setWordInteractive(true);
    }, 1400);
}

Playground_3_1.prototype.getNewExerciseSet = function() {

    this.setWordInteractive(true);

    for (var i = this.exerciseContainer.children.length - 1; i >= 0; i--) {
        this.exerciseContainer.removeChild(this.exerciseContainer.children[i]);
    };

    for (var k = 0; k < 3; k++) {
        this.tintSpriteBox[k].tint = '0xffffff';
        this.tintSpriteBox[k].alpha = 1;
    }
    this.returnWallSprites();
    this.borrowWallSprites(1);
    this.runExercise();
}

/**
 * the outro scene is displayed
 */
Playground_3_1.prototype.onComplete = function() {

    var self = this;

    this.introMode = false;

    this.help.interactive = false;
    this.help.buttonMode = false;
    this.fromOutro = 1;

    this.exerciseBaseContainer.visible = false;
    this.exerciseContainer.visible = false;

    this.setBalanceOutro();

    this.ovaigen.click = this.ovaigen.tap = function(data) {

        self.fromOutro = 0;

        self.cancelOutro();
        self.resetExercise();
        self.getNewExerciseSet();
    }

    this.outroAnimation();

}

Playground_3_1.prototype.resetExercise = function() {

    createjs.Sound.stop();

    this.ovaigen.visible = false;
    this.exerciseBaseContainer.visible = true;
    this.exerciseContainer.visible = true;

    for (var i = this.feedbackContainer.children.length - 1; i >= 0; i--) {
        this.feedbackContainer.removeChild(this.feedbackContainer.children[i]);
    };
    localStorage.setItem('playground_3_1', 0);
    this.generateFeedback();

    this.unsetBalance();

    this.mouthParamsOutro.visible = false;

    this.introMode = true;

};

/**
 * generate output feedback
 */
Playground_3_1.prototype.generateFeedback = function(i) {

    this.hiddenFeedback = [];
    this.feedbackTexture = PIXI.Texture.fromFrame('feedback.png');

    for (var i = 0; i < 10; i++) {
        var feedbackSprite = new PIXI.Sprite(this.feedbackTexture);
        feedbackSprite.position = {
            x: 1724,
            y: 1027 - (i * 108),
        }
        feedbackSprite.alpha = 0.2;
        this.feedbackContainer.addChild(feedbackSprite);
        this.hiddenFeedback.push(feedbackSprite);
    };

}

/**
 * prefetch exercise data and cache number of sets defined only.
 */
Playground_3_1.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/playground/playground_3_1.json?nocache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        self.pool = new ExerciseSpritesPool(loader.json);

        self.generatePoolCategory();
        self.borrowWallSprites(2);
        self.runExercise();

    });
    loader.load();
}

Playground_3_1.prototype.generatePoolCategory = function() {

    var keyArr = [];

    var spriteArr = this.pool.sprites,
        spriteLen = spriteArr.length;

    for (var i = 0; i < spriteLen; i++) {
        keyArr.push(spriteArr[i][0].category[0]);
    };

    this.poolCategory = keyArr.filter(onlyUnique);

    this.poolCategoryType = 'category';

}

Playground_3_1.prototype.borrowWallSprites = function(num) {

    var rand, category, sprite, spriteObj, soundHowl;

    for (var i = 0; i < num; i++) {

        rand = makeUniqueRandom(this.poolCategory.length);
        this.borrowCategory = this.poolCategory[rand];

        console.log('rand', rand)
        console.log('borrow category', this.borrowCategory)

        sprite = this.pool.borrowUniqueSprite(this.borrowCategory, this.lastBorrowSprite, this.poolCategoryType);

        spriteObj = sprite[0];

        this.lastBorrowSprite = spriteObj.word[0];

        var wordArr = [],
            imageArr = [],
            soundArr = [],
            answerArr = [];

        wordArr = spriteObj.word;
        shuffle(wordArr);

        soundHowl = loadSound('uploads/categories/audios/' + StripExtFromFile(spriteObj.category[1]));

        this.wallSlices.push({
            word: wordArr,
            image: PIXI.Sprite.fromImage('uploads/categories/images/' + spriteObj.category[0]),
            sound: soundHowl,
            answer: spriteObj.answer
        });
    }
};

Playground_3_1.prototype.returnWallSprites = function() {
    this.wallSlices.shift();
};

Playground_3_1.prototype.cleanMemory = function() {

    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    this.fromOutro = null;

    if (this.assetsToLoads)
        this.assetsToLoad.length = 0;

    for (var i = this.sceneContainer.children.length - 1; i >= 0; i--) {
        this.sceneContainer.removeChild(this.sceneContainer.children[i]);
    };
    this.sceneContainer = null;
};