function Playground_2_2() {
    cl.show()

    PIXI.DisplayObjectContainer.call(this);

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position.set(77, 88);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();
    this.helpContainer = new PIXI.DisplayObjectContainer();
    this.feedbackContainer = new PIXI.DisplayObjectContainer();
    this.keyboardContainer = new PIXI.DisplayObjectContainer();

    localStorage.setItem('playground_2_2', 0);
    main.CurrentExercise = "playground_2_2";

    main.eventRunning = false;
    this.fromOutro = 0;

    main.texturesToDestroy = [];
    this.arrayClickedKeys = [];
    this.bubblePos = 0;

    this.common = new Common();

    this.loadSpriteSheet();

    // everything connected to this is rendered.
    this.addChild(this.sceneContainer);
}

Playground_2_2.constructor = Playground_2_2;
Playground_2_2.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Playground_2_2.imagePath = 'build/exercises/playground/playground_2_2/images/';
Playground_2_2.audioPath = 'build/exercises/playground/playground_2_2/audios/';
Playground_2_2.totalFeedback = Main.TOTAL_FEEDBACK; // default 10


/**
 * load array of assets of different format
 */
Playground_2_2.prototype.loadSpriteSheet = function() {

    var self = this;

    this.introId = loadSound(Playground_2_2.audioPath + 'intro');
    this.outroId = loadSound(Playground_2_2.audioPath + 'outro');

    // create an array of assets to load
    this.assetsToLoad = [
        Playground_2_2.imagePath + "sprite_playground_2_2.json",
        Playground_2_2.imagePath + "sprite_playground_2_2.png",
        Playground_2_2.imagePath + "climbers_base.png",
        Playground_2_2.imagePath + "base.png",
        Playground_2_2.imagePath + "sprite_outro_1.json",
        Playground_2_2.imagePath + "sprite_outro_1.png",
        Playground_2_2.imagePath + "sprite_outro_2.json",
        Playground_2_2.imagePath + "sprite_outro_2.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
Playground_2_2.prototype.spriteSheetLoaded = function() {

    // hide loader
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    // fill exercise background
    this.sceneBackground = new PIXI.Graphics();
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xCAAABB);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);

    // exercise instruction
    this.loadExercise();
    this.introScene();
    this.outroScene();
    this.help = PIXI.Sprite.fromFrame("help.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);
    this.helpContainer.addChild(this.help);


    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.outroContainer);
    this.sceneContainer.addChild(this.feedbackContainer);
    this.sceneContainer.addChild(this.helpContainer);
    this.sceneContainer.addChild(this.keyboardContainer);
}



/**
 * play intro animation
 */
Playground_2_2.prototype.introAnimation = function() {

    createjs.Sound.stop();
    main.overlay.visible = true;

    if (this.fromOutro == 1) {
        this.fromOutro = 0;
        this.resetExercise();
        this.restartExercise();
    }

    this.help.interactive = false;
    this.help.buttonMode = false;

    this.wallClimbers.setTexture(this.textureOutro1);
    this.introContainer.visible = false;

    this.intro_mouth.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * cancel running intro animation
 */
Playground_2_2.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);

    this.introContainer.visible = true;
    this.intro_mouth.visible = false;
    this.wallClimbers.setTexture(this.climberTexture);
    this.help.interactive = true;
    this.help.buttonMode = true;

    this.ovaigen.visible = false;
    main.overlay.visible = false;
}

Playground_2_2.prototype.outroAnimation = function() {

    var self = this;
    // self.intro_mouth.gotoAndStop(0);
    self.intro_mouth.show();
    self.intro_mouth.bringToFront();

    var outroInstance = createjs.Sound.play(this.outroId);
    //this.wallClimbers.setTexture(this.textureOutro1);
    self.wallClimbers.visible = false;

    if (createjs.Sound.isReady()) {
        self.outro_mouth.visible = false;
        main.overlay.visible = true;
    }

    this.cancelHandler = this.onOutroAnimationComplete.bind(this);

    outroInstance.addEventListener("complete", this.cancelHandler);

}

Playground_2_2.prototype.onOutroAnimationComplete = function() {
    var self = this;

    self.intro_mouth.hide(0);
    createjs.Sound.stop();

    self.help.interactive = true;
    self.help.buttonMode = true;

    // if (data !== 1) {
    self.climbingAnimation();
    //}

    this.ovaigen.visible = true;
    main.overlay.visible = false;

};

Playground_2_2.prototype.cancelOutro = function(data) {

    var self = this;

    self.intro_mouth.gotoAndStop(0);
    self.intro_mouth.visible = false;

    self.outro_mouth.visible = true;
    createjs.Sound.stop();

    self.help.interactive = true;
    self.help.buttonMode = true;

    this.ovaigen.visible = true;
    main.overlay.visible = false;
};


Playground_2_2.prototype.climbingAnimation = function() {
    var self = this;
    self.intro_mouth.visible = false;

    //self.wallClimbers.visible = false;
    this.outro_mouth.visible = false;
    this.outro_mouth.bringToFront();

    self.animFirstKid.showAndReset();
    self.animSecondKid.showAndReset();
    self.animThirdKid.showAndReset();
};

/*
 *
 * create sprites, animation, sound related to intro scene */

Playground_2_2.prototype.introScene = function() {

    var self = this;

    this.intro_mouth = new Animation({
        "image": "mouth",
        "length": 5,
        "x": 313,
        "y": 528,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 5, 3, 2, 4, 1, 5],
        "frameId": 0
    });

    this.sceneContainer.addChild(this.intro_mouth);

    this.intro_mouth.visible = false;
}

/* *
 * create sprites, animation, sound related to outro scene */
Playground_2_2.prototype.outroScene = function() {

    var self = this;
    this.outro_mouth = PIXI.Sprite.fromFrame("mouth1.png");
    main.texturesToDestroy.push(this.outro_mouth);
    this.outro_mouth.position.set(313, 528);
    this.outroContainer.addChild(this.outro_mouth);
    this.outroContainer.visible = false;
    //back to intro page
    this.ovaigen = PIXI.Sprite.fromFrame('ova_igen.png');
    this.ovaigen.position.set(597, 492);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.ovaigen.visible = false;
    this.outroContainer.addChild(this.ovaigen);
}

/*
Method to render Keyboard
*/
Playground_2_2.prototype.renderKeyBoard = function() {

    this.keyboardButtons = [];

    var keyboardBase = PIXI.Sprite.fromFrame("textboard.png");
    keyboardBase.position.set(458, 995);
    main.texturesToDestroy.push(keyboardBase);
    this.keyboardContainer.addChild(keyboardBase);

    this.whiteTextBackground = createRect({
        w: 130,
        h: 100,
        color: 0xffffff
    });
    this.whiteTextRect = this.whiteTextBackground.generateTexture();

    var keyboardKeys,
        keys = ["k", "h", "s", "a", "e", "n", "r", "SUDDA"],
        style;

    for (var k = 0; k < 8; k++) {
        if (k == 7) {
            keyboardKeys = new PIXI.Sprite(this.whiteTextRect);
            keyboardKeys.width = 146;
            keyboardKeys.height = 116;
            keyboardKeys.position = {
                x: 1483,
                y: 1012
            };
            style = "30PX Tahoma";
        } else {
            keyboardKeys = new PIXI.Sprite(this.whiteTextRect);
            keyboardKeys.position = {
                x: 482 + (k * 144),
                y: 1007 + 10
            };
            style = "60px Arial";
        }
        keyboardKeys.interactive = true;
        keyboardKeys.buttonMode = true;
        //render text
        var keyLabel = new PIXI.Text("" + keys[k], {
            font: style
        });

        keyLabel.position = {
            x: (keyboardKeys.width - keyLabel.width) / 2,
            y: (keyboardKeys.height - keyLabel.height) / 2
        };
        keyboardKeys.key = keys[k];
        keyboardKeys.addChild(keyLabel);
        keyboardKeys.hitIndex = k;
        main.texturesToDestroy.push(keyboardKeys);
        this.keyboardButtons.push(keyboardKeys);
        this.keyboardContainer.addChild(keyboardKeys);
    }
}

/**
 * load exercise related stuffs
 */
Playground_2_2.prototype.loadExercise = function() {

    var sandTray = new PIXI.Sprite.fromImage(Playground_2_2.imagePath + 'base.png');
    sandTray.position.set(3, 3);
    main.texturesToDestroy.push(sandTray);
    this.sceneContainer.addChild(sandTray);
    this.createFeedBack();
    this.renderKeyBoard();

    this.animFirstKid = new Animation({
        "image": "first_girl_",
        "length": 5,
        "x": 204,
        "y": -577,
        "speed": 1.5,
        "pattern": false,
        "frameId": 0
    });

    this.animSecondKid = new Animation({
        "image": "second_girl_",
        "length": 4,
        "x": 731,
        "y": -443,
        "speed": 1,
        "pattern": false,
        "frameId": 0
    });

    this.animThirdKid = new Animation({
        "image": "third_boy_",
        "length": 4,
        "x": 1174,
        "y": -541,
        "speed": 0.7,
        "pattern": false,
        "frameId": 0
    });

    this.outroContainer.addChild(this.animFirstKid);
    this.outroContainer.addChild(this.animSecondKid);
    this.outroContainer.addChild(this.animThirdKid);

    var rect = createRect({
        w: 1843,
        h: 200,
        x: 0,
        y: -204,
        color: 0xFFFFFF
    });
    this.outroContainer.addChild(rect);

    this.climberTexture = new PIXI.Texture.fromImage(Playground_2_2.imagePath + 'climbers_base.png');
    this.textureOutro1 = new PIXI.Texture.fromImage(Playground_2_2.imagePath + 'outro1.png');

    this.wallClimbers = new PIXI.Sprite(this.climberTexture);
    this.wallClimbers.position.set(204, 6);
    this.sceneContainer.addChild(this.wallClimbers);

    this.speechBubbleDefault = new PIXI.Texture.fromFrame("speechbubble.png");
    this.speechBubbleLastBoy = new PIXI.Texture.fromFrame("speechbubble_alt.png");
    this.speechBubblePositions = [
        305, 121,
        830, 24,
        1200, 28
    ];

    this.speechBubble = new PIXI.Sprite(this.speechBubbleDefault);
    main.texturesToDestroy.push(this.speechBubble);
    this.speechBubble.position.set(this.speechBubblePositions[this.bubblePos * 2], this.speechBubblePositions[this.bubblePos * 2 + 1]);
    this.introContainer.addChild(this.speechBubble);

    this.Question = new PIXI.Text("", {
        font: "40px Arial"
    });

    //
    this.initObjectPooling();

    this.keyPressHandler = this.onKeyPress.bind(this);
    this.view.addEventListener("keydown", this.keyPressHandler, false);
}

Playground_2_2.prototype.positionSpeechBubbles = function() {
    if (this.bubblePos == 2) {
        this.speechBubble.texture = this.speechBubbleLastBoy;
    } else {
        this.speechBubble.texture = this.speechBubbleDefault;
    }
    this.speechBubble.position.set(this.speechBubblePositions[this.bubblePos * 2], this.speechBubblePositions[this.bubblePos * 2 + 1]);
};

/**
 * Render feedback to the container
 */
Playground_2_2.prototype.createFeedBack = function() {
    var self = this;

    for (var k = 0; k < 10; k++) {
        var feedbackSprite = PIXI.Sprite.fromFrame("feedback.png");

        feedbackSprite.position = {
            x: 50,
            y: 1080 - (k * 116)
        };

        main.texturesToDestroy.push(feedbackSprite);
        this.feedbackContainer.addChild(feedbackSprite);
        feedbackSprite.alpha = 0.4;
    }
};

Playground_2_2.prototype.keyPress = function(obj) {
    obj.tint = 0xffcc00;
    var a = setTimeout(function() {
        obj.tint = 0xffffff;
    }, 200);
};
/**
 * manage exercise with new sets randomized
 */
Playground_2_2.prototype.runExercise = function() {
    var self = this;
    this.jsonData = this.wallSlices[0];

    var text = this.jsonData.sentence;

    text = text.replace("_", " ____ ");

    this.Question.setText(text);

    this.Question.position = {
        x: (this.speechBubble.width - this.Question.width) / 2,
        y: 120
    };

    this.speechBubble.addChild(this.Question);

    this.answerPlaceHolder = new PIXI.Text("", {
        font: "40px Arial"
    })

    this.answerPlaceHolder.position = {
        x: this.Question.position.x + 65 + 25,
        y: 120
    };
    this.speechBubble.addChild(this.answerPlaceHolder);

    for (var k = 0; k < this.keyboardButtons.length; k++) {
        this.keyboardButtons[k].click = this.keyboardButtons[k].tap = function() {
            self.keyHitAction(this.hitIndex);
        }
    }
};

Playground_2_2.prototype.view = window;

Playground_2_2.prototype.onKeyPress = function(e) {
    var self = this;
    if (addedListeners) return false;
    if (e.shiftKey) return false;
    if (self.arrayClickedKeys.length >= 3) return false;



    var code = e.charCode || e.keyCode,
        character = String.fromCharCode(code),
        keyChar = character.toLowerCase();

    if (code === 8) { //Prevent backspace from navigating away from the current exercise.
        e.preventDefault();
    }

    var keys = ["k", "h", "s", "a", "e", "n", "r", "SUDDA"];

    var idx = keys.indexOf(keyChar);
    if (idx > -1) {
        addedListeners = true;
        self.keyHitAction(idx);
    } else {
        if (code === 8) {
            addedListeners = true;
            self.keyHitAction(7);
        }
    }
};

Playground_2_2.prototype.keyHitAction = function(itmIdx) {
    var self = this;

    var obj = self.keyboardButtons[itmIdx];
    self.keyPress(obj);

    if (obj.key != "SUDDA") {
        self.arrayClickedKeys.push(obj.key);
        if (self.arrayClickedKeys.length >= 3) { //evaluate answer
            var userText = self.arrayClickedKeys.join('');
            var correctFeedback = false;
            var fill = "black";
            if (self.jsonData.answers[0] == userText) { //correct answer
                correctFeedback = true;
                fill = "green";

            } else { //incorrect answer
                //check if there is alternate answer and it equals to userText

                if (self.jsonData.answers.length == 2) {
                    if (self.jsonData.answers[1] == userText) //correct answer
                    {
                        correctFeedback = true;
                        fill = "green";
                    } else { //wrong answer
                        correctFeedback = false;
                        fill = "red";
                    }
                } else { //no alternate answer available, so show wrong feedback
                    correctFeedback = false;
                    fill = "red";
                }
            }


            var a = setTimeout(function() {
                if (correctFeedback == true) {
                    correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                } else {
                    wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                }
                self.answerPlaceHolder.setStyle({
                    font: "40px Arial",
                    fill: fill
                });
            }, 1000);

            if (correctFeedback == false) {
                var b = setTimeout(function() {
                    self.arrayClickedKeys = [];
                    self.wrongFeedback();
                    self.answerPlaceHolder.setStyle({
                        font: "40px Arial",
                        fill: "black"
                    });
                    self.answerPlaceHolder.setText("");
                }, 2000);
            } else {
                var b = setTimeout(function() {
                    self.arrayClickedKeys = [];
                    self.correctFeedback();
                    self.answerPlaceHolder.setText("");
                }, 2000);
            }
        }

    } else { //obj.key ="SUDDA", that means user has hit back button to make corrections
        var idx = self.arrayClickedKeys.indexOf(self.arrayClickedKeys[self.arrayClickedKeys.length - 1]);
        self.arrayClickedKeys.splice(idx, 1);
    }

    self.answerPlaceHolder.setText(self.arrayClickedKeys.join(''));
    addedListeners = false;
};


/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
Playground_2_2.prototype.correctFeedback = function() {
    var self = this;
    var feedback = localStorage.getItem('playground_2_2');
    feedback++;

    localStorage.setItem('playground_2_2', feedback);
    this.updateFeedback();

    if (feedback >= Playground_2_2.totalFeedback) {
        var c = setTimeout(this.onComplete.bind(this), 2000);
    } else {
        var c = setTimeout(function() {
            self.getNewExerciseSet();
            main.eventRunning = false;
        }, 400);
    }
}

Playground_2_2.prototype.wrongFeedback = function() {
    var feedback = localStorage.getItem('playground_2_2');
    feedback--;

    if (feedback < 0)
        feedback = 0;

    localStorage.setItem('playground_2_2', feedback);
    this.updateFeedback();

}


Playground_2_2.prototype.updateFeedback = function() {

    var self = this;

    var counter = localStorage.getItem('playground_2_2');

    for (var i = 0; i < self.feedbackContainer.children.length; i++) {
        self.feedbackContainer.children[i].alpha = 0.4;
    }

    for (var j = 0; j < counter; j++) {
        self.feedbackContainer.children[j].alpha = 1;
    }


}


/**
 * prefetch exercise data and cache number of sets defined only.
 */
Playground_2_2.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/playground/playground_2_2.json?nocache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        self.pool = new ExerciseSpritesPool(loader.json);
        //borrow two sprites once
        self.generatePoolCategory();
        self.borrowWallSprites(2);
        self.runExercise();
    });
    loader.load();
}

Playground_2_2.prototype.generatePoolCategory = function() {

    var keyArr = [];

    var spriteArr = this.pool.sprites,
        spriteLen = spriteArr.length;

    for (var i = 0; i < spriteLen; i++) {
        keyArr.push(spriteArr[i][0].exercise_sentences);
    };

    this.poolCategory = keyArr.filter(onlyUnique);

    this.poolCategoryType = "exercise_sentences";

    this.poolIsNotArray = true;

}

Playground_2_2.prototype.borrowWallSprites = function(num) {

    var rand, category, sprite, spriteObj;

    var self = this;

    for (var i = 0; i < num; i++) {

        rand = makeUniqueRandom(this.poolCategory.length);
        this.borrowCategory = this.poolCategory[rand];

        sprite = this.pool.borrowUniqueSprite(this.borrowCategory, this.lastBorrowSprite,this.poolCategoryType,this.poolIsNotArray);
        spriteObj = sprite[0];

        this.poolSlices.push(sprite);
        this.lastBorrowSprite = spriteObj.exercise_sentences;
        this.wallSlices.push({
            sentence: spriteObj.exercise_sentences,
            answers: spriteObj.answers
        });
    }
};

Playground_2_2.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];

    this.pool.returnSprite(sprite);

    this.poolSlices = [];
    this.wallSlices.shift();
};



Playground_2_2.prototype.getNewExerciseSet = function() {
    this.bubblePos++;
    if (this.bubblePos > 2) this.bubblePos = 0;
    this.returnWallSprites();
    this.borrowWallSprites(1);

    this.positionSpeechBubbles();
    this.runExercise();
}

Playground_2_2.prototype.restartExercise = function() {

    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    localStorage.setItem('playground_2_2', 0);

    for (var i = this.feedbackContainer.children.length - 1; i >= 0; i--) {
        this.feedbackContainer.children[i].alpha = 0.4;
    };

    this.getNewExerciseSet();
};

/**
 * the outro scene is displayed
 */
Playground_2_2.prototype.onComplete = function() {
    var self = this;
    this.introMode = false;
    self.wallClimbers.visible = false;
    this.help.interactive = false;
    this.help.buttonMode = false;
    this.introContainer.visible = false;
    this.outroContainer.visible = true;
    this.keyboardContainer.visible = false;
    this.fromOutro = 1;

    this.outro_mouth.visible = true;
    this.animFirstKid.hide(0);
    this.animSecondKid.hide(0);
    this.animThirdKid.hide(0);
    self.animFirstKid.visible = true;
    self.animSecondKid.visible = true;
    self.animThirdKid.visible = true;

    this.ovaigen.click = this.ovaigen.tap = function(data) {

        self.fromOutro = 0;
        self.cancelOutro(1);
        self.resetExercise();
        self.restartExercise();
        self.intro_mouth.visible = false;
        self.wallClimbers.setTexture(self.climberTexture);
    }

    this.outroAnimation();

}

Playground_2_2.prototype.resetExercise = function() {

    this.ovaigen.visible = false;

    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    this.keyboardContainer.visible = true;
    this.wallClimbers.visible = true;

    this.introMode = true;
};

/**
 * clean memory by clearing reference of object
 */
Playground_2_2.prototype.cleanMemory = function() {


    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    if (this.assetsToLoads)
        this.assetsToLoad.length = 0;


    this.help = null;
    this.ovaigen = null;
    this.fromOutro = null;


    this.introContainer = null;
    this.outroContainer = null;
    this.cancelHandler = null;
    this.feedbackContainer = null;


    this.common = null;

    this.introId = null;
    this.outroId = null;

    if (this.arrayClickedKeys)
        this.arrayClickedKeys.length = 0;

    this.helpContainer = null;
    this.keyboardContainer = null;
    this.wallClimbers = null;
    this.intro_mouth = null;

    if (this.outro_mouth) this.outro_mouth = null;

    if (this.keyboardButtons) this.keyboardButtons.length = 0;
    this.whiteTextBackground = null;
    this.whiteTextRect = null;

    this.textureOutro1 = null;
    this.climberTexture = null;
    this.speechBubble = null;
    this.Question = null;
    this.keyPressHandler = null;

    this.answerPlaceHolder = null;


    for (var i = this.sceneContainer.children.length - 1; i >= 0; i--) {
        this.sceneContainer.removeChild(this.sceneContainer.children[i]);
    };

    this.sceneContainer = null;
}
