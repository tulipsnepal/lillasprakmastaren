function Playground_1_2() {
    cl.show()

    PIXI.DisplayObjectContainer.call(this);

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position.set(77, 88);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();
    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.feedbackContainer = new PIXI.DisplayObjectContainer();

    localStorage.setItem('playground_1_2', 0);
    main.CurrentExercise = "playground_1_2";

    main.eventRunning = false;

    main.texturesToDestroy = [];

    this.common = new Common();

    this.colors = ["röd", "lila", "vit", "gul", "rosa", "svart", "blå", "grön", "brun", "grå"];
    this.colorsEnglish = ["red", "purple", "white", "yellow", "pink", "black", "blue", "green", "brown", "grey"];
    this.colorsNoun = ["rött", "lila", "vitt", "gult", "rosa", "svart", "blått", "grönt", "brunt", "grått"];
    this.colorCode = [0xE91E26, 0x662D91, 0xFFFFFF, 0xFFF200, 0xEB018C, 0x000000, 0x0F75BC, 0x3AB34B, 0x8B5E3B, 0x818285];

    //Fetching the 2 combination of indexes (for two colors). For more details see method 'GetCombination()'
    this.getCombination = this.GetCombination([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 2);

    this.currentKidIndex = 1;

    this.pickerButton = [];
    this.questionColors = [];
    this.uniqueRandomFirst = [];
    this.uniqueRandomSecond = [];

    this.fromOutro = 0;

    this.loadSpriteSheet();

    // everything connected to this is rendered.
    this.addChild(this.sceneContainer);
}

Playground_1_2.constructor = Playground_1_2;
Playground_1_2.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Playground_1_2.imagePath = 'build/exercises/playground/playground_1_2/images/';
Playground_1_2.audioPath = 'build/exercises/playground/playground_1_2/audios/';
Playground_1_2.totalFeedback = Main.TOTAL_FEEDBACK; // default 10


/**
 * load array of assets of different format
 */
Playground_1_2.prototype.loadSpriteSheet = function() {

    var self = this;

    this.introId = loadSound(Playground_1_2.audioPath + 'intro');
    this.outroId = loadSound(Playground_1_2.audioPath + 'outro');

    // create an array of assets to load
    this.assetsToLoad = [
        Playground_1_2.imagePath + "sprite_playground_1_2_1.json",
        Playground_1_2.imagePath + "sprite_playground_1_2_1.png",
        Playground_1_2.imagePath + "sprite_playground_1_2_2.json",
        Playground_1_2.imagePath + "sprite_playground_1_2_2.png",
        Playground_1_2.imagePath + "outro.png",
        Playground_1_2.imagePath + "sand_tray.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
Playground_1_2.prototype.spriteSheetLoaded = function() {

    // hide loader
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    // fill exercise background
    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0xB9D3F0);
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xCAAABB);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);

    // exercise instruction
    this.help = PIXI.Sprite.fromFrame("help.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);
    this.sceneContainer.addChild(this.help);

    this.loadExercise();
    this.introScene();
    this.outroScene();

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.outroContainer);
    this.introContainer.addChild(this.exerciseContainer);
    this.introContainer.addChild(this.feedbackContainer);
}

/**
 * play intro animation
 */
Playground_1_2.prototype.introAnimation = function() {

    createjs.Sound.stop();
    main.overlay.visible = true;

    if (this.fromOutro == 1) {
        this.fromOutro = 0;
        this.resetExercise();
        this.restartExercise();
    }

    this.help.interactive = false;
    this.help.buttonMode = false;
    this.exerciseContainer.visible = false;

    this.intro_mouth.show();
    this.introHead.show();
    this.animationContainer.visible = true;

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
0-2 sec arm_0 => 0
2-4 sec arm_pointing at tower =>3
4-5 sec arm_pointing up =>1
5-7 sec arm_pointingat_jars =>2
stop at arm_0 =>0

 */
Playground_1_2.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    console.log(currentPostion)
    if (currentPostion >= 500 && currentPostion < 4000) {
        /*  this.showHandAt(0);
    } else if (currentPostion >= 1500 && currentPostion < 4000) {*/
        this.showHandAt(3);
    } else if (currentPostion >= 4000 && currentPostion < 6000) {
        this.showHandAt(1);
    } else if (currentPostion >= 6000 && currentPostion < 7883) {
        this.showHandAt(2);
    } else {
        this.showHandAt(0);
    }
};

/**
 * cancel running intro animation
 */
Playground_1_2.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);

    this.exerciseContainer.visible = true;
    this.intro_mouth.hide();
    this.introHead.hide();
    this.animationContainer.visible = false;
    this.help.interactive = true;
    this.help.buttonMode = true;

    this.ovaigen.visible = false;
    main.overlay.visible = false;
}

Playground_1_2.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.mouthOutro.show();
    }
    this.outroCancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.outroCancelHandler);

}

Playground_1_2.prototype.cancelOutro = function() {

    var self = this;

    createjs.Sound.stop();
    self.mouthOutro.hide();
    self.ovaigen.visible = true;
    self.help.interactive = true;
    self.help.buttonMode = true;
    main.overlay.visible = false;

};

Playground_1_2.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    };
    if (isNumeric(number))
        this.hands[number].visible = true;
}

/**
 * create sprites,animation,sound related to intro scene
 */
Playground_1_2.prototype.introScene = function() {

    var self = this;


    this.animationContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.animationContainer);

    this.introPerson = new PIXI.Sprite.fromFrame('introbody.png');
    this.introPerson.position.set(675, 170);
    main.texturesToDestroy.push(this.introPerson);
    this.animationContainer.addChild(this.introPerson);

    this.hands = [
        new PIXI.Sprite.fromFrame('arm1.png'),
        new PIXI.Sprite.fromFrame('arm2.png'),
        new PIXI.Sprite.fromFrame('arm3.png'),
        new PIXI.Sprite.fromFrame('arm4.png')
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(646, 137);
        this.animationContainer.addChild(this.hands[i]);
        this.hands[i].visible = false;
    };

    this.introHead = new Animation({
        "image": "intro_face",
        "length": 2,
        "x": 761,
        "y": 7,
        "speed": 1,
        "pattern": false,
        "frameId": 2
    });
    this.animationContainer.addChild(this.introHead);

    this.intro_mouth = new Animation({
        "image": "mouth",
        "length": 5,
        "x": 829,
        "y": 160,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 5, 3, 2, 4, 1, 5],
        "frameId": 2
    });

    this.animationContainer.addChild(this.intro_mouth);

    this.animationContainer.visible = false;
}

/**
 * create sprites,animation,sound related to outro scene
 */
Playground_1_2.prototype.outroScene = function() {

    var self = this;

    this.outroContainer.visible = false;

    this.outro_image = new PIXI.Sprite.fromImage(Playground_1_2.imagePath + 'outro.png');
    this.outro_image.position.set(172, 194);
    main.texturesToDestroy.push(this.outro_image);
    this.outroContainer.addChild(this.outro_image);

    this.outroHead = new Animation({
        "image": "outro_face",
        "length": 2,
        "x": 786,
        "y": 34,
        "speed": 5,
        "pattern": false,
        "frameId": 2
    });
    this.outroContainer.addChild(this.outroHead);


    this.mouthOutro = new Animation({
        "image": "mouth_outro_",
        "length": 5,
        "x": 889,
        "y": 270,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 5, 3, 2, 4, 1, 5],
        "frameId": 0
    });

    this.outroContainer.addChild(this.mouthOutro);

    // back to intro page
    this.ovaigen = PIXI.Sprite.fromFrame('ova_igen.png');
    this.ovaigen.position.set(1358, 522);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.ovaigen.visible = false;
    this.outroContainer.addChild(this.ovaigen);
}

/**
 * load exercise related stuffs
 */
Playground_1_2.prototype.loadExercise = function() {

    var sandTray = new PIXI.Sprite.fromImage(Playground_1_2.imagePath + 'sand_tray.png');
    sandTray.position.set(3, 3);
    main.texturesToDestroy.push(sandTray);
    this.sceneContainer.addChild(sandTray);

    this.bucketContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.bucketContainer);

    //add tower
    this.towerBottom = PIXI.Sprite.fromFrame("tower-lower.png");
    this.towerBottom.position.set(1115, 466);
    this.towerBottom.tint = 0xe9e9e9;
    this.introContainer.addChild(this.towerBottom);

    this.towerUpper = PIXI.Sprite.fromFrame("tower-upper.png");
    this.towerUpper.position.set(27, -190);
    this.towerUpper.tint = 0xe9e9e9;
    this.towerBottom.addChild(this.towerUpper);

    //create texture of feedback towers
    this.towerTextureTop = PIXI.Texture.fromFrame("fb_upper.png");
    this.towerTextureBottom = PIXI.Texture.fromFrame("fb_lower.png");


    var v = 0;
    var yPos;

    for (var k = 0; k < 10; k++) {
        if (k == 5) {
            v = 0;
        }

        if (k < 5) {
            yPos = 733;
        } else {
            yPos = 926;
        }
        //now show color picking jars
        var pickerSprite = PIXI.Sprite.fromFrame("colorjar-" + this.colorsEnglish[k] + ".png");
        pickerSprite.position = {
            x: 112 + v * 312,
            y: yPos
        };
        pickerSprite.interactive = true;
        pickerSprite.buttonMode = true;
        pickerSprite.clickedColor = this.colorsNoun[k];
        pickerSprite.hitIndex = k;
        main.texturesToDestroy.push(pickerSprite);
        this.pickerButton.push(pickerSprite);
        this.bucketContainer.addChild(pickerSprite);

        //now add color labels to jars
        var colorLabel = new PIXI.Text("" + this.colors[k], {
            font: "55px Arial",
            align: "center",
        });
        colorLabel.position = {
            x: (pickerSprite.width - colorLabel.width) / 2,
            y: 80
        };
        pickerSprite.addChild(colorLabel);

        v++;
    }
    this.renderFeedBacks();
    this.runExercise();
}

Playground_1_2.prototype.renderFeedBacks = function() {
    var self = this;
    for (var k = 0; k < 10; k++) {
        var feedbackSprite = PIXI.Sprite.fromFrame("fb_blank.png");

        feedbackSprite.position = {
            x: 1737,
            y: 1070 - (k * 117)
        };
        main.texturesToDestroy.push(feedbackSprite);
        self.introContainer.addChild(feedbackSprite);
    }
}

/*Method to fetch two random elements from the given array*/
Playground_1_2.prototype.getTwoRandomElements = function() {
    var sourceArray = this.colorsNoun.slice();

    var randomItemIndex = makeUniqueRandom(this.getCombination.length);
    var randomItemsArray = this.getCombination[randomItemIndex];

    return [sourceArray[randomItemsArray[0]], sourceArray[randomItemsArray[1]]];
};


/**
 * fetch combination of 'k' elements from array 'set'. For example:
 * this.GetCombination([1,2,3],2); will return an array [[1,2],[1,3],[2,3]];
 * @param1  {array} - The array of numbers, in this exercise 0 to 9
 * @param2  {integer} - The number of elements we want in each group.
 * @return {array} -  An array of possible combination with sub array of 'k' elements as elements of main array
 */
Playground_1_2.prototype.GetCombination = function(set, k) {
    var i, j, combs, head, tailcombs;

    if (k > set.length || k <= 0) {
        return [];
    }

    if (k == set.length) {
        return [set];
    }

    if (k == 1) {
        combs = [];
        for (i = 0; i < set.length; i++) {
            combs.push([set[i]]);
        }
        return combs;
    }

    combs = [];
    for (i = 0; i < set.length - k + 1; i++) {
        head = set.slice(i, i + 1);
        tailcombs = this.GetCombination(set.slice(i + 1), k - 1);
        for (j = 0; j < tailcombs.length; j++) {
            combs.push(head.concat(tailcombs[j]));
        }
    }
    return combs;
}

/*
Generate Exercise Data
*/
Playground_1_2.prototype.createSet = function(kidIndex) {
    var self = this;
    for (var i = this.exerciseContainer.children.length - 1; i >= 0; i--) {
        this.exerciseContainer.removeChild(this.exerciseContainer.children[i]);
    };
    this.questionColors = self.getTwoRandomElements();
    //console.log("QUESTION COLORS = " + this.questionColors);

    //now create KID
    var kidToShow = PIXI.Sprite.fromFrame("kid" + kidIndex + ".png");
    kidToShow.position = {
        x: 796,
        y: (kidIndex == 1) ? 12 : 97
    };
    this.exerciseContainer.addChild(kidToShow);

    //add speech bubble
    this.speechBubble = PIXI.Sprite.fromFrame("speech-bubble.png");
    this.speechBubble.position.set(165, kidToShow.position.y);
    this.exerciseContainer.addChild(this.speechBubble);

    var defaultBubbleText = new PIXI.Text("Mitt torn ska vara", {
        font: "60px Arial",
        align: "center",
    });

    defaultBubbleText.position = {
        x: 80,
        y: 120
    };
    this.speechBubble.addChild(defaultBubbleText);


    var renderQuestion = this.questionColors.slice();
    renderQuestion.splice(1, 0, "och"); //add "och" between two elements (colors) in an array so that they can be easily iterated in loop
    var q = 0,
        r = 0,
        style,
        xPos,
        textTORender;

    for (var k = 0; k < 3; k++) {
        if (k != 1) {
            style = "bold 60px Arial"; //name of the colors should be bold
        } else {
            style = "60px Arial"; // "och" should not be bold
        }

        if (k == 2) {
            textTORender = renderQuestion[k] + "."; //add period sign to second color so that the sentence ends with fullstop
        } else {
            textTORender = renderQuestion[k];
        }

        var colorText = new PIXI.Text("" + textTORender, {
            font: style,
            align: "center",
        });

        if (k == 0) {
            xPos = 100;
            q = colorText.width;
            r = colorText.width;
        } else if (k == 1) {
            xPos = 100 + q + 25;
            q = colorText.width;
        } else {
            xPos = 100 + q + r + 50;
        }
        colorText.position = {
            x: xPos,
            y: 190
        };
        this.speechBubble.addChild(colorText);
    }
};


/**
 * manage exercise with new sets randomized
 */
Playground_1_2.prototype.runExercise = function() {
    var self = this;
    self.createSet(this.currentKidIndex);

    var chosenColor = [],
        chosenHitIndex = [];

    for (var k = 0; k < 10; k++) {
        this.pickerButton[k].click = this.pickerButton[k].tap = function() {
            if (addedListeners) return false;
            var obj = this;
            chosenColor.push(obj.clickedColor);
            chosenHitIndex.push(obj.hitIndex);

            if (chosenColor.length >= 2) {
                self.towerBottom.tint = self.colorCode[obj.hitIndex];
                self.changePickersInteractivity(false);

                if (self.questionColors.join() == chosenColor.join() || self.reverseJoin(self.questionColors) == chosenColor.join()) { //correct answer
                    self.currentKidIndex++;
                    var a = setTimeout(function() {
                        correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                        self.correctFeedback(chosenHitIndex);
                        self.towerBottom.tint = 0xe9e9e9;
                        self.towerUpper.tint = 0xe9e9e9;
                        chosenColor = [];
                        chosenHitIndex = [];
                    }, 1500);

                } else { //wrong answer
                    self.currentKidIndex--;
                    if (self.currentKidIndex < 1) self.currentKidIndex = 1;
                    var a = setTimeout(function() {
                        wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                        self.wrongFeedback();
                        self.towerBottom.tint = 0xe9e9e9;
                        self.towerUpper.tint = 0xe9e9e9;
                        chosenColor = [];
                        chosenHitIndex = [];
                    }, 1500);
                }



            } else {
                self.towerUpper.tint = self.colorCode[obj.hitIndex];
            }


        }
    }

    //add
};

Playground_1_2.prototype.changePickersInteractivity = function(interactivity) {
    for (var j = 0; j < this.pickerButton.length; j++) {
        this.pickerButton[j].interactive = interactivity;
    }
}

/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
Playground_1_2.prototype.correctFeedback = function(indexes) {
    var feedback = localStorage.getItem('playground_1_2');
    feedback++;

    localStorage.setItem('playground_1_2', feedback);
    this.updateFeedback(indexes);
    if (feedback >= Playground_1_2.totalFeedback) {
        setTimeout(this.onComplete.bind(this), 2000);
    } else {
        this.getNewExerciseSet();
    }
    this.changePickersInteractivity(true);
}

Playground_1_2.prototype.wrongFeedback = function() {
    var feedback = localStorage.getItem('playground_1_2');
    feedback--;

    if (feedback < 0)
        feedback = 0;

    localStorage.setItem('playground_1_2', feedback);
    var indexToRemove = this.feedbackContainer.children.length - 1;
    if (indexToRemove > -1) {
        this.feedbackContainer.removeChild(this.feedbackContainer.children[indexToRemove]);
    }

    this.changePickersInteractivity(true);
}

Playground_1_2.prototype.updateFeedback = function(indexes) {

    var self = this;
    main.eventRunning = false;
    var feedBackLength = self.feedbackContainer.children.length;
    var feedBackTower = new PIXI.Sprite(self.towerTextureBottom);
    if (indexes.length > 0) {
        feedBackTower.tint = self.colorCode[indexes[1]];
    }
    feedBackTower.position = {
        x: 1737,
        y: 1128 - (feedBackLength * 117)
    };

    self.feedbackContainer.addChild(feedBackTower);

    //add top part of feedbacktower
    feedbackTop = new PIXI.Sprite(self.towerTextureTop);
    if (indexes.length > 0) {
        feedbackTop.tint = self.colorCode[indexes[0]];
    }
    feedbackTop.position = {
        x: 8,
        y: -55
    };
    feedBackTower.addChild(feedbackTop);
}



Playground_1_2.prototype.getNewExerciseSet = function() {

    this.runExercise();
}

Playground_1_2.prototype.restartExercise = function() {

    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    localStorage.setItem('playground_1_2', 0);

    for (var i = this.feedbackContainer.children.length - 1; i >= 0; i--) {
        this.feedbackContainer.removeChild(this.feedbackContainer.children[i]);
    };

    this.currentKidIndex = 1;
    this.getNewExerciseSet();
};

/**
 * the outro scene is displayed
 */
Playground_1_2.prototype.onComplete = function() {

    var self = this;

    this.introMode = false;

    this.help.interactive = false;
    this.help.buttonMode = false;
    this.introContainer.visible = false;
    this.outroContainer.visible = true;
    this.fromOutro = 1;

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        self.fromOutro = 0;
        self.cancelOutro();
        self.resetExercise();
        self.restartExercise();
    }

    this.outroAnimation();

}

Playground_1_2.prototype.resetExercise = function() {
    this.ovaigen.visible = false;
    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    this.introMode = true;
};

/*
Method to Reverse the array. Need to check answer. For example if question is blue and green, the combination of (green, blue) and (blue,green) is correct
*/
Playground_1_2.prototype.reverseJoin = function(a) {
    var temp = [];
    temp[0] = a[1];
    temp[1] = a[0];
    return temp.join();
};

/**
 * clean memory by clearing reference of object
 */
Playground_1_2.prototype.cleanMemory = function() {


    if (this.sceneBackground)
        this.sceneBackground.clear();

    this.sceneBackground = null;

    if (this.assetsToLoads)
        this.assetsToLoad.length = 0;


    this.help = null;
    this.ovaigen = null;
    this.fromOutro = null;

    if (this.pickerButton)
        this.pickerButton.length = 0;

    if (this.colors)
        this.colors.length = 0;

    if (this.uniqueRandomFirst)
        this.uniqueRandomFirst.length = 0;

    if (this.uniqueRandomSecond)
        this.uniqueRandomSecond.length = 0;


    this.introContainer = null;
    this.outroContainer = null;
    this.exerciseContainer = null;
    this.animationContainer = null;
    this.cancelHandler = null;
    this.feedbackContainer = null;

    this.introPerson = null;
    this.intro_mouth = null;
    this.outro_image = null;
    this.mouthOutro = null;
    this.outroHead = null;


    this.bucketContainer = null;

    if (this.colorsEnglish)
        this.colorsEnglish.length = 0;

    if (this.colorsNoun)
        this.colorsNoun.length = 0;

    if (this.colorCode)
        this.colorCode.length = 0;

    if (this.currentKidIndex)
        this.currentKidIndex = null;

    if (this.questionColors)
        this.questionColors.length = 0;

    this.introHead = null;

    this.towerBottom = null;
    this.towerUpper = null;
    this.towerTextureTop = null;
    this.towerTextureBottom = null;
    this.speechBubble = null;

    this.common = null;

    this.introId = null;
    this.outroId = null;


    for (var i = this.sceneContainer.children.length - 1; i >= 0; i--) {
        this.sceneContainer.removeChild(this.sceneContainer.children[i]);
    };
    this.sceneContainer = null;
}