function Playground_1_1() {
    cl.show()

    PIXI.DisplayObjectContainer.call(this);

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position.set(77, 88);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();
    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.animationContainer = new PIXI.DisplayObjectContainer();
    main.CurrentExercise = "playground_1_1";

    main.texturesToDestroy = [];
    this.alreadySelectedColor = [];

    this.fromOutro = 0;

    this.common = new Common();

    this.childrensButton = [];
    this.pickerButton = [];

    this.loadSpriteSheet();

    // everything connected to this is rendered.
    this.addChild(this.sceneContainer);
}

Playground_1_1.constructor = Playground_1_1;
Playground_1_1.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Playground_1_1.imagePath = 'build/exercises/playground/playground_1_1/images/';
Playground_1_1.audioPath = 'build/exercises/playground/playground_1_1/audios/';


/**
 * load array of assets of different format
 */
Playground_1_1.prototype.loadSpriteSheet = function() {

    var self = this;

    this.introId = loadSound(Playground_1_1.audioPath + 'intro');
    this.outroId = loadSound(Playground_1_1.audioPath + 'outro');

    // create an array of assets to load
    this.assetsToLoad = [
        Playground_1_1.imagePath + "sprite_playground_1_1_1.json",
        Playground_1_1.imagePath + "sprite_playground_1_1_1.png",
        Playground_1_1.imagePath + "sprite_playground_1_1_2.json",
        Playground_1_1.imagePath + "sprite_playground_1_1_2.png",
        Playground_1_1.imagePath + "outro.png",
        Playground_1_1.imagePath + "sand_tray.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
Playground_1_1.prototype.spriteSheetLoaded = function() {

    // hide loader
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    // fill exercise background
    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0xB9D3F0);
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xCAAABB);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);

    // exercise instruction
    this.help = PIXI.Sprite.fromFrame("help.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);
    this.sceneContainer.addChild(this.help);

    this.loadExercise();
    this.introScene();
    this.outroScene();

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.outroContainer);
    this.introContainer.addChild(this.exerciseContainer);
    this.introContainer.addChild(this.animationContainer);
}

/**
 * play intro animation
 */
Playground_1_1.prototype.introAnimation = function() {

    createjs.Sound.stop();
    main.overlay.visible = true;

    if (this.fromOutro == 1) {
        this.fromOutro = 0;
        this.resetExercise();
        this.restartExercise();
    }

    this.help.interactive = false;
    this.help.buttonMode = false;

    this.animationContainer.visible = true;
    this.intro_mouth.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
0-4 sec arm_0
4-6 sec arm_1
6-7 sec arm_0
7-9 sec arm_2
stop at arm_0
*/
Playground_1_1.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    console.log(currentPostion);
    if (currentPostion >= 1000 && currentPostion < 5500) {
        this.showHandAt(0);
    } else if (currentPostion >= 5500 && currentPostion < 6500) {
        this.showHandAt(1);
    } else if (currentPostion >= 6500 && currentPostion < 8000) {
        this.showHandAt(0);
    } else if (currentPostion >= 8000 && currentPostion < 9891) {
        this.showHandAt(2);
    } else {
        this.showHandAt(0);
    }
};

/**
 * cancel running intro animation
 */
Playground_1_1.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);

    this.intro_mouth.hide();
    this.animationContainer.visible = false;
    this.help.interactive = true;
    this.help.buttonMode = true;

    this.ovaigen.visible = false;
    main.overlay.visible = false;
    addedListeners = false;
}

Playground_1_1.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.mouthOutro.show();
    }
    this.outroCancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.outroCancelHandler);

}

Playground_1_1.prototype.cancelOutro = function() {

    var self = this;

    createjs.Sound.stop();
    self.mouthOutro.hide();
    self.ovaigen.visible = true;
    self.help.interactive = true;
    self.help.buttonMode = true;
    main.overlay.visible = false;

};

Playground_1_1.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    };
    if (isNumeric(number))
        this.hands[number].visible = true;
}

/**
 * create sprites,animation,sound related to intro scene
 */
Playground_1_1.prototype.introScene = function() {

    var self = this;



    this.introPerson = new PIXI.Sprite.fromFrame('fb6_intro.png');
    this.introPerson.position.set(893, 17);
    main.texturesToDestroy.push(this.introPerson);
    this.animationContainer.addChild(this.introPerson);

    this.intro_mouth = new Animation({
        "image": "mouth",
        "length": 5,
        "x": 1001,
        "y": 233,
        "speed": 5,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 5, 3, 2, 4, 1, 5],
        "frameId": 2
    });

    this.animationContainer.addChild(this.intro_mouth);

    this.hands = [
        new PIXI.Sprite.fromFrame('arm1.png'),
        new PIXI.Sprite.fromFrame('arm2.png'),
        new PIXI.Sprite.fromFrame('arm3.png'),
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(710, 243);
        this.animationContainer.addChild(this.hands[i]);
        this.hands[i].visible = false;
    };
    this.animationContainer.visible = false;

    var redOrGreenBoxRect = createRect({
        w: 265,
        h: 79,
        color: 0xffffff
    });
    var redOrGreenBoxRaw = redOrGreenBoxRect.generateTexture();
    this.redOrGreenBox = new PIXI.Sprite(redOrGreenBoxRaw);
    this.redOrGreenBox.alpha = 0.6
    this.introContainer.addChild(this.redOrGreenBox);
    this.redOrGreenBox.visible = false;

}

/**
 * create sprites,animation,sound related to outro scene
 */
Playground_1_1.prototype.outroScene = function() {

    var self = this;

    this.outroContainer.visible = false;

    this.outro_image = new PIXI.Sprite.fromImage(Playground_1_1.imagePath + 'outro.png');
    this.outro_image.position.set(86, 104);
    main.texturesToDestroy.push(this.outro_image);
    this.outroContainer.addChild(this.outro_image);


    this.mouthOutro = new Animation({
        "image": "mouth_outro_",
        "length": 5,
        "x": 716,
        "y": 430,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 5, 3, 2, 4, 1, 5],
        "frameId": 0
    });

    this.outroContainer.addChild(this.mouthOutro);

    // back to intro page
    this.ovaigen = PIXI.Sprite.fromFrame('ova_igen.png');
    this.ovaigen.position.set(236, 686);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.ovaigen.visible = false;
    this.outroContainer.addChild(this.ovaigen);
}

/**
 * load exercise related stuffs
 */
Playground_1_1.prototype.loadExercise = function() {

    var sandTray = new PIXI.Sprite.fromImage(Playground_1_1.imagePath + 'sand_tray.png');
    sandTray.position.set(3, 3);
    main.texturesToDestroy.push(sandTray);
    this.sceneContainer.addChild(sandTray);

    this.childrenContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.childrenContainer);

    this.bucketContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.bucketContainer);

    //load 10 childrens

    var childrenPosition = [
        86, 170,
        304, 156,
        433, 177,
        627, 166,
        775, 180,
        906, 111,
        1098, 156,
        1237, 155,
        1361, 150,
        1555, 177
    ];
    this.bucketTexture = PIXI.Texture.fromFrame("bucket-white.png");
    this.pickerTexture = PIXI.Texture.fromFrame("color-bucket.png");

    this.arrowHead = new PIXI.Sprite.fromFrame('arrowhead.png');
    main.texturesToDestroy.push(this.arrowHead);
    this.childrenContainer.addChild(this.arrowHead);
    this.arrowHead.visible = false;


    this.colors = ["röd", "lila", "vit", "gul", "rosa", "svart", "blå", "grön", "brun", "grå"];

    var v = 0;
    var yPos;

    for (var k = 0; k < 10; k++) {
        //var idx = this.repositionedChildren[k];
        var childrenSprite = PIXI.Sprite.fromFrame("fb" + (k + 1) + ".png");
        childrenSprite.position = {
            x: childrenPosition[k * 2],
            y: childrenPosition[k * 2 + 1]
        };
        childrenSprite.interactive = true;
        childrenSprite.buttonMode = true;
        childrenSprite.hitIndex = k;
        childrenSprite.arrowY = childrenPosition[k * 2 + 1] - 100;
        childrenSprite.arrowX = childrenPosition[k * 2] + (childrenSprite.width - 71) / 2;
        main.texturesToDestroy.push(childrenSprite);
        this.childrenContainer.addChild(childrenSprite);
        this.childrensButton.push(childrenSprite);

        //now position the bucket;
        var buckets = new PIXI.Sprite(this.bucketTexture);
        buckets.position = {
            x: 151 + k * 162,
            y: 436
        };
        buckets.tint = 0xe9e9e9;
        buckets.interactive = false;
        buckets.buttonMode = false;
        main.texturesToDestroy.push(buckets);
        this.bucketContainer.addChild(buckets);
        if (k == 5) {
            v = 0;
        }

        if (k < 5) {
            yPos = 732;
        } else {
            yPos = 926;
        }
        //now show color picking jars
        var pickerSprite = new PIXI.Sprite(this.pickerTexture);
        pickerSprite.position = {
            x: 236 + v * 311,
            y: yPos
        };
        pickerSprite.interactive = false;
        pickerSprite.buttonMode = true;
        pickerSprite.clickedColor = this.colors[k];
        pickerSprite.hitIndex = k;
        main.texturesToDestroy.push(pickerSprite);
        this.pickerButton.push(pickerSprite);
        this.bucketContainer.addChild(pickerSprite);

        //now add color labels to jars
        var colorLabel = new PIXI.Text("" + this.colors[k], {
            font: "55px Arial"
        });
        colorLabel.position = {
            x: (pickerSprite.width - colorLabel.width) / 2,
            y: 78
        };
        pickerSprite.addChild(colorLabel);

        v++;
    }

    this.initObjectPooling();

}

/**
 * manage exercise with new sets randomized
 */
Playground_1_1.prototype.runExercise = function() {

    var self = this,
        noOfCorrectAnswer = 0,
        requiredColor,
        bottomAlreadyAdded = false,
        currentIndex,
        bucketAlreadyAdded = false;

    this.arrowHead.visible = false;
    this.jsonData = this.wallSlices[0];

    for (var j = 0; j < self.childrensButton.length; j++) {
        self.childrensButton[j].interactive = true;
    }

    for (var k = 0; k < this.childrensButton.length; k++) {

        this.childrensButton[k].click = this.childrensButton[k].tap = function() {
            if (addedListeners) return false;
            addedListeners = true;
            for (var j = 0; j < self.childrensButton.length; j++) {
                self.childrensButton[j].interactive = false;
            }
            this.interactive = true;

            self.changePickersInteractivity(true);
            var obj = this;

            self.arrowHead.position = {
                x: obj.arrowX,
                y: obj.arrowY
            };
            self.arrowHead.visible = true;


            bucketAlreadyAdded = false;
            currentIndex = obj.hitIndex;
            self.alreadySelectedColor.push(obj.hitIndex);

            //add bucket bottom for required color
            if (bottomAlreadyAdded === false) { //Add bucket bottom only if its not added, as user might click item twice.
                var bucketBottom = self.jsonData.image_bucket_bottom[obj.hitIndex];
                bucketBottom.position = {
                    x: 151 + obj.hitIndex * 162,
                    y: 436
                };
                self.exerciseContainer.addChild(bucketBottom);
                bottomAlreadyAdded = true;
            }

            requiredColor = self.jsonData.color[obj.hitIndex];
            pickedIndex = k;

            this.soundInstance = createjs.Sound.play(self.jsonData.sound[obj.hitIndex]);
            this.soundInstance.addEventListener("failed", handleFailed);
            this.soundInstance.addEventListener("complete", function() {
                addedListeners = false;
            });

        }

        this.pickerButton[k].click = this.pickerButton[k].tap = function() {
            var picker = this;
            if (addedListeners) return false;

            if (picker.clickedColor == requiredColor) { //Correct answer
                correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                self.redOrGreenBox.tint = 0x00ff00;
                self.common.showRedOrGreenBox(picker.position.x, picker.position.y + 65, self.redOrGreenBox);

                for (var j = 0; j < self.childrensButton.length; j++) {
                    if (self.alreadySelectedColor.indexOf(j) == -1) {
                        self.childrensButton[j].interactive = true;
                    }
                }
                //fill the bucket
                if (bucketAlreadyAdded === false) {
                    var coloredBucket = self.jsonData.image_bucket[currentIndex];
                    coloredBucket.position = {
                        x: 151 + currentIndex * 162,
                        y: 436
                    };
                    self.exerciseContainer.addChild(coloredBucket);
                    self.childrensButton[currentIndex].interactive = false;
                    self.childrensButton[currentIndex].buttonMode = false;
                    bucketAlreadyAdded = true;

                }

                bottomAlreadyAdded = false;
                self.changePickersInteractivity(false);
                noOfCorrectAnswer++;
                if (noOfCorrectAnswer == 10) { //all answered correctly, take to outro page
                    self.correctFeedback();
                }
            } else { //wrong answer
                self.redOrGreenBox.tint = 0xff0000;
                self.common.showRedOrGreenBox(picker.position.x, picker.position.y + 63, self.redOrGreenBox);
                wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
            }



        }
    }
}

Playground_1_1.prototype.changePickersInteractivity = function(interactivity) {
    for (var j = 0; j < this.pickerButton.length; j++) {
        this.pickerButton[j].interactive = interactivity;
    }
}

/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
Playground_1_1.prototype.correctFeedback = function() {
    addedListeners = false;
    var b = setTimeout(this.onComplete.bind(this), 1000);
}


/**
 * prefetch exercise data and cache number of sets defined only.
 */
Playground_1_1.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/playground/playground_1_1.json?nocache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        self.pool = new ExerciseSpritesPool(loader.json);

        //borrow two sprites once
        self.borrowWallSprites(1);
        self.runExercise();

    });
    loader.load();
}

Playground_1_1.prototype.borrowWallSprites = function(num) {

    var self = this;
    this.num = num;

    for (var i = 0; i < this.num; i++) {
        var sprite = this.pool.borrowSprite();
        this.poolSlices.push(sprite);
        var colorArr = [],
            colorBucketBottomArr = [],
            colorBucketArr = [],
            soundArr = [],
            spriteObj = sprite[0];

        for (var j = 0; j < spriteObj.color.length; j++) {

            var poolBucketBottomSprite = PIXI.Sprite.fromFrame(spriteObj.color_bucket_bottom[j]);
            colorBucketBottomArr.push(poolBucketBottomSprite);
            main.texturesToDestroy.push(poolBucketBottomSprite);

            var poolBucketSprite = PIXI.Sprite.fromFrame(spriteObj.color_bucket[j]);
            colorBucketArr.push(poolBucketSprite);
            main.texturesToDestroy.push(poolBucketSprite);

            var howl = loadSound(Playground_1_1.audioPath + StripExtFromFile(spriteObj.color_audio[j]));
            soundArr.push(howl);

            colorArr.push(spriteObj.color[j]);

        };



        shuffle(colorArr, colorBucketBottomArr, colorBucketArr, soundArr);



        this.wallSlices.push({
            color: colorArr,
            image_bucket_bottom: colorBucketBottomArr,
            image_bucket: colorBucketArr,
            sound: soundArr
        });
    }
};

Playground_1_1.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];

    this.pool.returnSprite(sprite);

    this.poolSlices = [];
    this.wallSlices.shift();
};

Playground_1_1.prototype.getNewExerciseSet = function() {
    for (var i = this.exerciseContainer.children.length - 1; i >= 0; i--) {
        this.exerciseContainer.removeChild(this.exerciseContainer.children[i]);
    };

    this.returnWallSprites();
    this.borrowWallSprites(1);
    this.runExercise();
}

Playground_1_1.prototype.restartExercise = function() {

    this.outroContainer.visible = false;
    this.introContainer.visible = true;

    this.getNewExerciseSet();
};

/**
 * the outro scene is displayed
 */
Playground_1_1.prototype.onComplete = function() {
    var self = this;

    this.introMode = false;

    this.help.interactive = false;
    this.help.buttonMode = false;
    this.introContainer.visible = false;
    this.outroContainer.visible = true;
    this.fromOutro = 1;
    this.alreadySelectedColor = [];

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        self.fromOutro = 0;
        self.cancelOutro();
        self.resetExercise();
        self.restartExercise();
    }

    this.outroAnimation();

}

Playground_1_1.prototype.resetExercise = function() {
    this.ovaigen.visible = false;
    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    this.introMode = true;
};

/**
 * clean memory by clearing reference of object
 */
Playground_1_1.prototype.cleanMemory = function() {


    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    if (this.assetsToLoads)
        this.assetsToLoad.length = 0;


    this.help = null;
    this.fromOutro = null;

    this.ovaigen = null;

    if (this.jsonData)
        this.jsonData.length = 0;



    if (this.wallSlices)
        this.wallSlices.length = 0;

    if (this.poolSlices)
        this.poolSlices.length = 0;

    if (this.childrensButton)
        this.childrensButton.length = 0;

    if (this.pickerButton)
        this.pickerButton.length = 0;

    if (this.colors)
        this.colors.length = 0;


    if (this.alreadySelectedColor)
        this.alreadySelectedColor.length = 0;



    this.pool = null;
    this.num = null;

    this.introContainer = null;
    this.outroContainer = null;
    this.exerciseContainer = null;
    this.animationContainer = null;
    this.cancelHandler = null;

    this.introPerson = null;
    this.intro_mouth = null;
    this.outro_image = null;
    this.mouthOutro = null;

    this.childrenContainer = null;
    this.bucketContainer = null;
    this.bucketTexture = null;
    this.pickerTexture = null;
    this.arrowHead = null;
    this.redOrGreenBox = null;

    this.common = null;


    this.introId = null;
    this.outroId = null;


    for (var i = this.sceneContainer.children.length - 1; i >= 0; i--) {
        this.sceneContainer.removeChild(this.sceneContainer.children[i]);
    };
    this.sceneContainer = null;
}