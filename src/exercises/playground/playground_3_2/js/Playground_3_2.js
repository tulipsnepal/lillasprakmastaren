function Playground_3_2() {
    cl.show()

    PIXI.DisplayObjectContainer.call(this);

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position.set(77, 88);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();

    localStorage.setItem('playground_3_2', 0);

    main.CurrentExercise = "playground_3_2";

    this.fromOutro = 0;

    main.eventRunning = false;

    main.texturesToDestroy = [];

    this.loadSpriteSheet();

    // everything connected to this is rendered.
    this.addChild(this.sceneContainer);
}

Playground_3_2.constructor = Playground_3_2;
Playground_3_2.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Playground_3_2.imagePath = 'build/exercises/playground/playground_3_2/images/';
Playground_3_2.audioPath = 'build/exercises/playground/playground_3_2/audios/';
Playground_3_2.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

/**
 * load array of assets of different format
 */
Playground_3_2.prototype.loadSpriteSheet = function() {
    this.introId = loadSound(Playground_3_2.audioPath + 'playpark_3_2_intro');
    this.outroId = loadSound(Playground_3_2.audioPath + 'playpark_3_2_outro');

    // create an array of assets to load
    this.assetsToLoad = [
        Playground_3_2.imagePath + "sprite_3_2.json",
        Playground_3_2.imagePath + "sprite_3_2.png",
        Playground_3_2.imagePath + "seesaw2.png",
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
Playground_3_2.prototype.spriteSheetLoaded = function() {

    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    // fill exercise background
    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0xd6def2);
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xcfc3d5);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1201);
    this.sceneContainer.addChild(this.sceneBackground);

    // fill exercise background layer
    this.grayLayer = new PIXI.Graphics();
    this.grayLayer.beginFill(0x878786);
    // draw a rectangle
    this.grayLayer.drawRect(0, 834, 1843, 367);
    this.sceneContainer.addChild(this.grayLayer);

    // exercise instruction
    this.help = PIXI.Sprite.fromFrame("help.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);
    this.sceneContainer.addChild(this.help);

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.loadScene();
    this.sceneContainer.addChild(this.introContainer);

    this.loadExercise();

}

Playground_3_2.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
        this.hands[i].position = this.hands[i].standPosition;
    };
    if (isNumeric(number))
        this.hands[number].visible = true;
}

/**
 * play intro animation
 */
Playground_3_2.prototype.introAnimation = function() {

    createjs.Sound.stop();
    main.overlay.visible = true;

    if (this.fromOutro == 1) {
        this.fromOutro = 0;
        this.resetExercise();
    }

    this.help.interactive = false;
    this.help.buttonMode = false;

    this.setKid3AtStand();
    this.kidSprite[0].position = this.kidSprite[0].standPosition;
    this.kidSprite[1].position = this.kidSprite[1].standPosition;
    this.kidSprite[3].position = this.kidSprite[3].standPosition;

    this.mouthParams.show();
    this.ovaigen.visible = false;

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
 0-1 sec arm_0
1-2.5 sec arm_1
2.5-4 sec arm_2
stop at arm_0

 */
Playground_3_2.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    console.log(currentPostion)
    if (currentPostion >= 0 && currentPostion < 1500) {
        this.showHandAt(0);
    } else if (currentPostion >= 1500 && currentPostion < 3000) {
        this.showHandAt(1);
    } else if (currentPostion >= 3000 && currentPostion < 4179) {
        this.showHandAt(2);
    } else {
        this.showHandAt(0);
    }
};

/**
 * cancel running intro animation
 */
Playground_3_2.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);

    this.setKid3AtSit();
    this.kidSprite[0].position = this.kidSprite[0].sitPosition;
    this.kidSprite[1].position = this.kidSprite[1].sitPosition;
    this.kidSprite[3].position = this.kidSprite[3].sitPosition;

    this.help.interactive = true;
    this.help.buttonMode = true;

    this.ovaigen.visible = false;
    main.overlay.visible = false;
}

Playground_3_2.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.mouthParams.show();
    }
    this.cancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.cancelHandler);
}


Playground_3_2.prototype.cancelOutro = function() {

    createjs.Sound.stop();
    this.mouthParams.hide(0);

    this.fps = 60;
    this.isPlaying = true;
    this.swingHandler = this.swingUpDown.bind(this);
    this.interval = Math.ceil(20000 / this.fps);
    this.swingInterval = setInterval(this.swingHandler, this.interval);

    this.ovaigen.visible = true;
    this.help.interactive = true;
    this.help.buttonMode = true;
    main.overlay.visible = false;

};

Playground_3_2.prototype.swingUpDown = function() {

    var rand = makeUniqueRandom(2)

    if (rand) {
        this.setSeesaw1();
        this.mouthContainer.visible = true;
    } else {
        this.setSeesaw2();
        this.mouthContainer.visible = false;
    }
};

/**
 * create sprites,animation,sound related to intro scene
 */
Playground_3_2.prototype.loadScene = function() {

    var self = this;

    this.seesaw0 = new PIXI.Texture.fromFrame('background.png');
    this.seesaw1 = new PIXI.Texture.fromFrame('seesaw1.png');
    this.seesaw2 = new PIXI.Texture.fromFrame(Playground_3_2.imagePath + 'seesaw2.png');

    this.seesaw = new PIXI.Sprite(this.seesaw0);
    this.seesaw.position.set(101, 744);
    main.texturesToDestroy.push(this.seesaw);
    this.introContainer.addChild(this.seesaw);

    this.kidContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.kidContainer);

    this.handContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.handContainer);

    this.mouthContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.mouthContainer);

    this.barrelContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.barrelContainer);

    this.generateKidSprite();

    this.barrelTexture = new PIXI.Texture.fromFrame('barrel.png');

    this.barrelPosition = [
        144, 553,
        444, 553,
        1021, 553,
        1333, 553,
    ];

    this.hands = [
        new PIXI.Sprite.fromFrame('arm1.png'),
        new PIXI.Sprite.fromFrame('arm2.png'),
        new PIXI.Sprite.fromFrame('arm3.png')
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].sitPosition = {
            x: 991,
            y: 495
        };
        this.hands[i].standPosition = {
            x: 991,
            y: 316
        };
        this.hands[i].position = this.hands[i].sitPosition;
        this.handContainer.addChild(this.hands[i]);
        this.hands[i].visible = false;
    };

    this.mouthParams = new Animation({
        "image": "mouth",
        "length": 5,
        "x": 1112,
        "y": 303,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 2, 4, 1, 3],
        "frameId": 0
    });
    this.mouthParams.show();

    this.mouthContainer.addChild(this.mouthParams);

    this.setKid3AtSit();

    for (var i = 0; i < 4; i++) {
        var barrelSprite = new PIXI.Sprite(this.barrelTexture);
        barrelSprite.position.x = this.barrelPosition[i * 2];
        barrelSprite.position.y = this.barrelPosition[i * 2 + 1];
        this.barrelContainer.addChild(barrelSprite);
    };

    // back to intro page
    this.ovaigen = PIXI.Sprite.fromFrame('ova_igen.png');
    this.ovaigen.position.set(772, 557);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.ovaigen.visible = false;
    this.introContainer.addChild(this.ovaigen);

}

Playground_3_2.prototype.setKid3AtSit = function() {

    this.showHandAt(0);
    this.hands[0].position = this.hands[0].sitPosition;

    this.mouthParams.position.y = 481;
    this.mouthParams.visible = true;
    this.mouthParams.hide(0);

    this.kidSprite[2].position = this.kidSprite[2].sitPosition;
}

Playground_3_2.prototype.setKid3AtStand = function() {

    this.hands[0].position = this.hands[0].standPosition;
    this.showHandAt(0);

    this.mouthContainer.visible = true;

    this.mouthParams.position.y = 300;
    this.mouthParams.visible = true;
    this.mouthParams.hide(0);

    this.kidSprite[2].position = this.kidSprite[2].standPosition;
}

Playground_3_2.prototype.setSeesaw0 = function() {

    this.mouthContainer.visible = false;

    this.seesaw.setTexture(this.seesaw0);
    this.seesaw.position.set(101, 744);
};

Playground_3_2.prototype.setSeesaw1 = function() {

    this.mouthContainer.visible = true;

    this.mouthParams.position.x = 1329 + 76;
    this.mouthParams.position.y = 680;
    this.mouthParams.visible = true;
    this.mouthParams.hide(0);

    this.seesaw.setTexture(this.seesaw1);
    this.seesaw.position.set(0, 315);
};

Playground_3_2.prototype.setSeesaw2 = function() {
    this.seesaw.setTexture(this.seesaw2);
    this.seesaw.position.set(0, 315);
};

Playground_3_2.prototype.generateKidSprite = function() {

    this.kidSprite = [];
    this.kidPosition = {
        'sit': [155, 328, 442, 362, 1031, 327, 1368, 357],
        'stand': [156, 153, 441, 184, 1030, 146, 1368, 178],
    }

    for (var i = 0; i < 4; i++) {

        var kid = new PIXI.Sprite.fromFrame('kid' + (i + 1) + '.png');
        main.texturesToDestroy.push(kid);

        kid.sitPosition = {
            x: this.kidPosition.sit[i * 2],
            y: this.kidPosition.sit[i * 2 + 1]
        };

        kid.standPosition = {
            x: this.kidPosition.stand[i * 2],
            y: this.kidPosition.stand[i * 2 + 1]
        };

        kid.position = kid.sitPosition;

        this.kidContainer.addChild(kid);

        kid.hitArea = new PIXI.Rectangle(0, 0, 200, 333);
        kid.interactive = true;
        kid.buttonMode = true;

        this.kidSprite.push(kid);

    };

};

/**
 * load exercise related stuffs
 */
Playground_3_2.prototype.loadExercise = function() {

    this.whiteGraphics = createRect({
        lineColor: '0x000000',
        lineWidth: 2,
        lineStyle: true
    });
    this.whiteTexture = this.whiteGraphics.generateTexture();

    this.redGraphics = createRect({
        "alpha": 0.6,
        "color": 0xff0000,
        "w": 244,
        "h": 244
    });
    this.redTexture = this.redGraphics.generateTexture();

    this.greenGraphics = createRect({
        "alpha": 0.6,
        "color": 0x00ff00,
        "w": 244,
        "h": 244
    });
    this.greenTexture = this.greenGraphics.generateTexture();

    // generate feedback
    this.feedbackContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.feedbackContainer);

    this.generateFeedback();

    // create exercise container to show dynamic sets
    this.exerciseBaseContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.addChild(this.exerciseBaseContainer);

    // create exercise container to show dynamic sets
    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.addChild(this.exerciseContainer);

    this.categoryPostion = [
        160, 608,
        464, 608,
        1046, 608,
        1348, 608,
    ];

    for (var i = 0; i < 4; i++) {
        var whiteBox = new PIXI.Sprite(this.whiteTexture);
        whiteBox.width = 230;
        whiteBox.height = 61;
        whiteBox.position.x = this.categoryPostion[i * 2];
        whiteBox.position.y = this.categoryPostion[i * 2 + 1];
        this.exerciseBaseContainer.addChild(whiteBox);
    };


    this.correctContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.correctContainer);

    this.wrongContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.wrongContainer);

    this.soundTexture = new PIXI.Texture.fromFrame('sound_grey.png');

    this.dropParams = {
        'position': this.barrelPosition,
        'w': 259,
        'h': 489
    };

    this.initObjectPooling();
}

Playground_3_2.prototype.runExercise = function() {

    this.jsonData = this.wallSlices[0];
    console.log(this.jsonData.word);

    this.dragButtons = [];

    this.dropIndex = 0;
    this.dropCount = [0, 0, 0, 0];
    this.wrongCount = [0, 0, 0, 0];
    this.evaluationIndex = {
        0: [],
        1: []
    };
    //this.itemsDragged = [];
    this.testItems = {
        0: [],
        1: [],
        2: [],
        3: []
    };

    this.testSpriteItems = [];

    this.testWordItems = [];
    this.dropTotal = 0;
    this.dropTotalLeft = 4;

    this.correctIndex = [];
    this.wrongIndex = [];

    for (var i = 0; i < 4; i++) {

        this.kidSprite[i].howl = this.jsonData.categorySound[i];
        this.kidSprite[i].click = this.kidSprite[i].tap = function(data) {
            if (addedListeners) return false;
            addedListeners = true;
            this.soundInstance = createjs.Sound.play(this.howl);
            this.soundInstance.addEventListener("failed", handleFailed);
            this.soundInstance.addEventListener("complete", function() {
                addedListeners = false;
            });
        }

        var upperText = this.jsonData.category[i].toUpperCase();

        var whiteBoxText = new PIXI.Text(upperText, {
            'font': 'bold 24px Arial',
            'align': 'center'
        });

        whiteBoxText.position.x = this.categoryPostion[i * 2] + 20;
        whiteBoxText.position.y = this.categoryPostion[i * 2 + 1] + 20;

        this.exerciseContainer.addChild(whiteBoxText);

        var whiteSprite = new PIXI.Sprite(this.whiteTexture);
        whiteSprite.position.set(739, 23 + (i * 254));
        whiteSprite.width = whiteSprite.height = 244;

        whiteSprite.rectText = this.jsonData.word[i];

        whiteSprite.imageSprite = this.jsonData.image[i];

        whiteSprite.imageSprite.position.set(4, 4);
        whiteSprite.imageSprite.width = whiteSprite.imageSprite.height = 240;
        whiteSprite.addChild(whiteSprite.imageSprite);

        whiteSprite.soundSprite = new PIXI.Sprite(this.soundTexture);
        whiteSprite.soundSprite.position.set(10, 180)
        whiteSprite.addChild(whiteSprite.soundSprite);

        whiteSprite.greenBox = new PIXI.Sprite(this.greenTexture);
        whiteSprite.addChild(whiteSprite.greenBox);

        whiteSprite.redBox = new PIXI.Sprite(this.redTexture);
        whiteSprite.addChild(whiteSprite.redBox);

        whiteSprite.redBox.alpha = whiteSprite.greenBox.alpha = 0.6;

        whiteSprite.greenBox.visible = false;
        whiteSprite.redBox.visible = false;

        this.exerciseContainer.addChild(whiteSprite);

        whiteSprite.soundSprite.interactive = true;
        whiteSprite.soundSprite.buttonMode = true;
        whiteSprite.soundSprite.howl = this.jsonData.sound[i];

        whiteSprite.soundSprite.click = whiteSprite.soundSprite.tap = this.onSoundPressed.bind(whiteSprite.soundSprite);

        whiteSprite.interactive = true;
        whiteSprite.buttonMode = true;



        whiteSprite.dragIndex = i;
        whiteSprite.categoryIndex = this.jsonData.category.indexOf(this.jsonData.answer[i]);

        this.dragButtons.push(whiteSprite);

        this.onDragging(i);

    };

};

Playground_3_2.prototype.onSoundPressed = function() {
    createjs.Sound.stop();
    this.soundInstance = createjs.Sound.play(this.howl, {
        interrupt: createjs.Sound.INTERRUPT_NONE
    });
    this.soundInstance.addEventListener("failed", handleFailed);
    this.soundInstance.addEventListener("complete", handleComplete);
};

Playground_3_2.prototype.onDragging = function(i) {

    var self = this;

    // use the mousedown and touchstart
    this.dragButtons[i].mousedown = this.dragButtons[i].touchstart = function(data) {
        if (addedListeners) return false;

        data.originalEvent.preventDefault()

        // store a reference to the data
        // The reason for this is because of multitouch
        // we want to track the movement of this particular touch

        this.data = data;
        this.alpha = 0.9;
        this.dragging = true;
        this.sx = this.data.getLocalPosition(this).x;
        this.sy = this.data.getLocalPosition(this).y;

        this.bringToFront();
        if (this.width == 65) { //when user tries to drag items within the barrel.
            this.width = this.height = 244;
            this.hitArea = new PIXI.Rectangle(0, 0, 244, 244);
            this.rectSprite.visible = false;
            this.rectWord.visible = false;
            this.soundSprite.visible = true;

        }


        // no dragging on sound icon area
        if (isInsideRectangle(this.sx, this.sy, this.soundSprite.position.x, this.soundSprite.position.y, this.soundSprite.position.x + 83, this.soundSprite.position.y + 81)) {
            this.dragging = false;
        }

    }

    this.dragButtons[i].mouseup = this.dragButtons[i].mouseupoutside = this.dragButtons[i].touchend = this.dragButtons[i].touchendoutside = function(data) {

        if (addedListeners) return false;
        var dragSelf = this;

        this.alpha = 1
        this.dragging = false;
        // set the interaction data to null
        this.data = null;

        self.isDropArea(this.dx, this.dy);

        /*IF PLAYER DRAGS ITEM FROM ONE BARREL TO ANOTHER, WE HAVE TO  ADJUST DROP TOTAL AND DROP COUNT
        BECAUSE SAME ITEMS DRAGGED MORE THAN ONCE SHOULD NOT INCREASE THE DROP TOTAL AND DROP COUNT. FOR THIS WE'VE USED
        ARRAY testItems WHICH CONTAINS THE dragIndex as well as dropIndex OF THE ITEM
        */
        //var chkidx = self.itemsDragged.indexOf(this.dragIndex);
        var chkidx = self.valueExist(self.testItems, this.dragIndex); //check if currently dragged item is in itemsDragged and find the index

        if (chkidx.length > 0) { //item exist

            self.dropCount[chkidx[0]]--; //decrease the drop count because it will be added when it is dropped
            self.dropTotal--; //decrease the total as well as it will be added when dropped
            //self.itemsDragged.splice(chkidx, 1); //remove the item in index "chkidx" as it will be set in again when dropped
            self.testItems[chkidx[0]].splice(chkidx[1], 1);

            var idxC = self.correctIndex.indexOf(this.dragIndex);
            if (idxC > -1) {
                self.correctIndex.splice(idxC, 1);
            }

            var idxW = self.wrongIndex.indexOf(this.dragIndex);
            if (idxW > -1) {
                self.wrongIndex.splice(idxW, 1);
                self.wrongCount[chkidx[0]]--;
            }


            var eval1 = self.evaluationIndex[1].indexOf(this.dragIndex);
            if (eval1 > -1) {
                self.evaluationIndex[1].splice(eval1, 1);
            }

            var eval0 = self.evaluationIndex[0].indexOf(this.dragIndex);
            if (eval0 > -1) {
                self.evaluationIndex[0].splice(eval0, 1);
            }

            // console.log("INDEX =" + self.correctContainer.getChildIndex(this.rectSprite));

            //remove rectangle sprite and text because the current item is dragged from one barrel to another
            var wrongIndexOfItem = self.wrongContainer.children.indexOf(this.rectSprite);
            var correctIndexOfItem = self.correctContainer.children.indexOf(this.rectSprite);

            if (wrongIndexOfItem > -1) {
                self.wrongContainer.removeChildAt(wrongIndexOfItem); //remove sprite
                self.wrongContainer.removeChildAt(wrongIndexOfItem); //remove text, as sprite is removed, the index of text becomes same as the sprite had before
            }


            if (correctIndexOfItem > -1) {
                self.correctContainer.removeChildAt(correctIndexOfItem);
                self.correctContainer.removeChildAt(correctIndexOfItem);
            }
        }

        if ((typeof(self.dropIndex) !== 'undefined') && (self.dropIndex !== null)) {

            self.testItems[self.dropIndex].push(this.dragIndex);

            this.width = 65;
            this.height = 65;

            dragSelf.hitArea = new PIXI.Rectangle(0, 0, 800, 300);

            this.rectSprite = new PIXI.Sprite(self.whiteTexture);


            this.rectSprite.position.x = self.barrelPosition[self.dropIndex * 2] + 50;
            this.rectSprite.position.y = self.barrelPosition[self.dropIndex * 2 + 1] + 80 + self.dropCount[self.dropIndex] * 70;
            this.rectSprite.width = 205;
            this.rectSprite.height = 64;

            this.rectSprite.interactive = true;
            this.rectSprite.buttonMode = true;

            this.rectWord = new PIXI.Text(this.rectText, {
                'font': '30px Arial'
            });

            this.rectWord.position.x = this.rectSprite.position.x + 20;
            this.rectWord.position.y = this.rectSprite.position.y + 20;

            this.position.x = self.barrelPosition[self.dropIndex * 2];
            // this.position.y = self.barrelPosition[self.dropIndex * 2 + 1] + 80 + self.dropCount[self.dropIndex] * 70;

            this.soundSprite.visible = false;
            self.testSpriteItems[this.dragIndex] = this.rectSprite;
            self.testWordItems[this.dragIndex] = this.rectWord;

            self.rearrangeItems();

            if (this.categoryIndex === self.dropIndex) {
                self.correctIndex.push(this.dragIndex);
                self.correctContainer.addChild(this.rectSprite);
                self.correctContainer.addChild(this.rectWord);
                self.evaluationIndex[1].push(this.dragIndex);
            } else {
                self.wrongIndex.push(this.dragIndex);
                self.wrongCount[self.dropIndex]++;
                self.wrongContainer.addChild(this.rectSprite);
                self.wrongContainer.addChild(this.rectWord);
                self.evaluationIndex[0].push(this.dragIndex);
            }



            if (self.dropTotal >= self.dropTotalLeft) {
                setTimeout(self.evaluateExercise.bind(self), 1000);
            }

        } else {

            this.sx = 0;
            this.sy = 0;
            this.dx = 0;
            this.dy = 0;

            // reset dragged element position
            this.hitArea = new PIXI.Rectangle(0, 0, 244, 244);
            this.width = this.height = 244;
            this.soundSprite.visible = true;
            this.position.x = 739;
            this.position.y = 23 + (this.dragIndex * 254);
        }
        /*console.log("DROP COUNT = " + self.dropCount);
        console.log("ITEMS DRAGGED = " + self.testItems);
        console.log("DROPPED TOTAL = " + self.dropTotal);
        console.log("DROP LEFT = " + self.dropTotalLeft);*/

    }

    // set the callbacks for when the mouse or a touch moves
    this.dragButtons[i].mousemove = this.dragButtons[i].touchmove = function(data) {
        if (addedListeners) return false;

        if (this.dragging) {
            // need to get parent coords..
            var newPosition = this.data.getLocalPosition(this.parent);
            this.position.x = newPosition.x - 100; // - this.sx;
            this.position.y = newPosition.y - 100; //- this.sy;
            this.dx = newPosition.x;
            this.dy = newPosition.y;
        }

    };
};

Playground_3_2.prototype.rearrangeItems = function(chkidx) {
    var self = this;

    for (var k = 0; k < 4; k++) {
        var chklength = self.testItems[k].length;
        if (chklength > 0) {
            var mItems = self.testItems[k];
            for (var x = 0; x < mItems.length; x++) {
                self.dragButtons[mItems[x]].position.y = self.barrelPosition[k * 2 + 1] + 80 + ((x + 1) * 70);
                self.testSpriteItems[mItems[x]].position.y = self.barrelPosition[k * 2 + 1] + 80 + ((x + 1) * 70);
                self.testWordItems[mItems[x]].position.y = self.testSpriteItems[mItems[x]].position.y + 20;
            }

        }
    }

};

/*Method to check if element exist in multidimensional array*/
Playground_3_2.prototype.valueExist = function(array, checkWhat) {
    var dropIndex, dragIndex;
    for (var k = 0; k < 4; k++) {
        var idchk = array[k].indexOf(checkWhat);
        if (idchk > -1) {
            dropIndex = k;
            dragIndex = idchk;
        }
    }
    if (dropIndex >= 0 && dragIndex >= 0) {
        return [dropIndex, dragIndex];
    } else {
        return [];
    }

};

/**
 * this function used when we drop all 4 sprites to different barrel
 * correct ones remains dropped
 * wrong ones are return back to original position
 */
Playground_3_2.prototype.evaluateExercise = function() {

    this.tintDroppedSprite();
    setTimeout(this.revertOnWrong.bind(this), 200);

};

Playground_3_2.prototype.tintDroppedSprite = function() {

    /* console.log("CORRECT INDEX = " + this.evaluationIndex[1]);
    console.log("INCORRECT INDEX = " + this.evaluationIndex[0]);
*/
    var self = this;
    var cIndex = this.evaluationIndex[1];
    for (var i = 0; i < cIndex.length; i++) {

        var buttonIndex = cIndex[i],
            button = this.dragButtons[buttonIndex];

        // button.tint = '0x00ff00';
        button.greenBox.visible = true;
        // main.common.showRedOrGreenBox(button.position.x, button.position.y, button.greenBox);
        button.interactive = false;
    };

    var wIndex = this.evaluationIndex[0];
    for (var i = 0; i < wIndex.length; i++) {

        var buttonIndex = wIndex[i],
            button = this.dragButtons[buttonIndex];

        button.redBox.visible = true;
        // button.tint = '0xff0000';
        // main.common.showRedOrGreenBox(button.position.x, button.position.y, button.redBox);

    };

};

Playground_3_2.prototype.revertOnWrong = function() {

    for (var i = 0; i < this.correctIndex.length; i++) {

        /* var buttonIndex = this.correctIndex[i],
             button = this.dragButtons[buttonIndex];

         button.tint = '0xFFFFFF';*/
        this.dropTotalLeft--;
    };

    for (var i = 0; i < this.wrongIndex.length; i++) {

        var buttonIndex = this.wrongIndex[i],
            button = this.dragButtons[buttonIndex];

        // button.tint = '0xFFFFFF';
        button.width = button.height = 244;
        button.soundSprite.visible = true;
        button.position.x = 739;
        button.position.y = 23 + (buttonIndex * 254);
        button.interactive = true;

        button.sx = 0;
        button.sy = 0;
        button.dx = 0;
        button.dy = 0;

        var chkidx = this.valueExist(this.testItems, this.wrongIndex[i]);
        if (chkidx.length > 0) {
            this.testItems[chkidx[0]].splice(chkidx[1], 1);
        }

        var echkidx = this.evaluationIndex[0].indexOf(this.wrongIndex[i]);
        if (echkidx > -1) {
            this.evaluationIndex[0].splice(echkidx, 1);
        }

    };

    for (var i = 0; i < 4; i++) {
        this.dropCount[i] = this.dropCount[i] - this.wrongCount[i];
    };

    if (this.dropTotalLeft < 1) {
        this.correctFeedback()
    } else {
        this.wrongFeedback();
    }



};

/**
 * check drag position is inside rectangular drop position
 * @param  {float} dx    drag positon x
 * @param  {float} dy    drag position y
 * @param  {integer} index
 * @return {boolean}       true/false
 */
Playground_3_2.prototype.isDropArea = function(dx, dy) {

    this.dropIndex = null;

    for (var i = 0; i < 4; i++) {
        var x1 = this.dropParams.position[i * 2],
            y1 = this.dropParams.position[i * 2 + 1],
            x2 = this.dropParams.position[i * 2] + this.dropParams.w,
            y2 = this.dropParams.position[i * 2 + 1] + this.dropParams.h;


        if (isInsideRectangle(dx, dy, x1, y1, x2, y2)) {
            this.dropIndex = i;
            this.dropCount[i]++;
            this.dropTotal++;
        }
    };
}

/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
Playground_3_2.prototype.correctFeedback = function() {

    correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);

    for (var k = 0; k < 4; k++) {
        this.dragButtons[k].redBox.visible = false;
        this.dragButtons[k].greenBox.visible = false;
    }

    var feedback = localStorage.getItem('playground_3_2');
    this.hiddenFeedback[feedback].alpha = 1;
    feedback++;
    localStorage.setItem('playground_3_2', feedback);

    this.dropIndex = 0;
    this.dropCount = [0, 0, 0, 0];
    this.wrongCount = [0, 0, 0, 0];
    this.dropTotal = 0;
    this.dropTotalLeft = 4;
    this.dragButtons.length = 0;
    this.testItems = {
        0: [],
        1: [],
        2: [],
        3: []
    };

    this.evaluationIndex = {
        0: [],
        1: []
    };

    this.testSpriteItems = [];
    this.testWordItems = [];

    this.correctIndex.length = 0;
    this.wrongIndex.length = 0;

    if (feedback >= Playground_3_2.totalFeedback) {
        setTimeout(this.onComplete.bind(this), 1000);
    } else {
        this.getNewExerciseSet();
    }
    addedListeners = false;

}

Playground_3_2.prototype.wrongFeedback = function() {

    wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);

    for (var k = 0; k < 4; k++) {
        this.dragButtons[k].redBox.visible = false;
        this.dragButtons[k].greenBox.visible = false;
    }

    for (var j = this.wrongContainer.children.length - 1; j >= 0; j--) {
        this.wrongContainer.removeChild(this.wrongContainer.children[j]);
    };

    this.dropIndex = 0;
    this.dropTotal = 0;
    this.wrongCount = [0, 0, 0, 0];

    this.correctIndex.length = 0;
    this.wrongIndex.length = 0;

    var feedback = localStorage.getItem('playground_3_2');
    feedback--;
    if (feedback >= 0) {
        this.hiddenFeedback[feedback].alpha = 0.2;
        localStorage.setItem('playground_3_2', feedback);
    };

    addedListeners = false;

}

Playground_3_2.prototype.restartExercise = function() {
    this.getNewExerciseSet();
};

Playground_3_2.prototype.getNewExerciseSet = function() {

    this.clearCorrectWrongContainer();


    for (var i = this.exerciseContainer.children.length - 1; i >= 0; i--) {
        this.exerciseContainer.removeChild(this.exerciseContainer.children[i]);
    };

    this.returnWallSprites();
    this.borrowWallSprites(1);
    this.runExercise();
}

Playground_3_2.prototype.clearCorrectWrongContainer = function() {
    for (var i = this.correctContainer.children.length - 1; i >= 0; i--) {
        this.correctContainer.removeChild(this.correctContainer.children[i]);
    };

    for (var i = this.wrongContainer.children.length - 1; i >= 0; i--) {
        this.wrongContainer.removeChild(this.wrongContainer.children[i]);
    };
};

/**
 * the outro scene is displayed
 */
Playground_3_2.prototype.onComplete = function() {

    var self = this;

    this.introMode = false;

    this.fromOutro = 1;
    this.clearCorrectWrongContainer();

    this.help.interactive = false;
    this.help.buttonMode = false;

    this.handContainer.visible = false;
    this.kidContainer.visible = false;
    this.barrelContainer.visible = false;
    this.exerciseBaseContainer.visible = false;
    this.exerciseContainer.visible = false;
    this.feedbackContainer.visible = false;

    this.setSeesaw1();

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        self.fromOutro = 0;
        self.resetExercise();
        self.getNewExerciseSet();
    }

    this.outroAnimation();

}

Playground_3_2.prototype.resetExercise = function() {

    createjs.Sound.stop();

    clearInterval(this.swingInterval);

    this.ovaigen.visible = false;

    this.mouthParams.x = 1112;
    this.mouthParams.y = 303;

    this.handContainer.visible = true;
    this.kidContainer.visible = true;
    this.barrelContainer.visible = true;
    this.exerciseBaseContainer.visible = true;
    this.exerciseContainer.visible = true;
    this.feedbackContainer.visible = true;

    this.setSeesaw0();
    this.setKid3AtSit();

    for (var i = this.feedbackContainer.children.length - 1; i >= 0; i--) {
        this.feedbackContainer.removeChild(this.feedbackContainer.children[i]);
    };
    localStorage.setItem('playground_3_2', 0);
    this.generateFeedback();

    this.help.interactive = true;
    this.help.buttonMode = true;
    this.mouthContainer.visible = true;

    addedListeners = false;
    this.introMode = true;

};

/**
 * generate output feedback
 */
Playground_3_2.prototype.generateFeedback = function(i) {

    this.hiddenFeedback = [];
    this.feedbackTexture = PIXI.Texture.fromFrame('feedback.png');

    for (var i = 0; i < 10; i++) {
        var feedbackSprite = new PIXI.Sprite(this.feedbackTexture);
        main.texturesToDestroy.push(feedbackSprite);
        feedbackSprite.position = {
            x: 1724,
            y: 1027 - (i * 108),
        }
        feedbackSprite.alpha = 0.2;
        this.feedbackContainer.addChild(feedbackSprite);
        this.hiddenFeedback.push(feedbackSprite);
    };

}

/**
 * prefetch exercise data and cache number of sets defined only.
 */
Playground_3_2.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/playground/playground_3_2.json?nocache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        self.pool = new ExerciseSpritesPool(loader.json);
        self.borrowWallSprites(2);
        self.runExercise();


    });
    loader.load();
}

Playground_3_2.prototype.borrowWallSprites = function(num) {

    var self = this;

    /*this.categoryId = {
        'möbler': 'cat_MOBLER',
        'djur': 'cat_DJUR',
        'namn': 'cat_NAMN',
        'fordon': 'cat_FORDON',
        'kläder': 'cat_KLÄDER',
        'färger': 'cat_FÄRGER',
        'räkneord': 'cat_RAKNEORD',
        'köksredskap': 'cat_KOKSREDSKAP',
    }*/

    for (var i = 0; i < num; i++) {

        var sprite = this.pool.borrowSprite(),
            spriteObj = sprite[0],
            wordArr = [],
            answerArr = [],
            imageArr = [],
            categoryArr = [],
            howlArr = [],
            categoryHowlArr = [];

        this.poolSlices.push(sprite);

        for (var j = 0; j < 4; j++) {
            var categoryName = spriteObj.category.name[j],
                imageSprite = PIXI.Sprite.fromImage('uploads/images/' + spriteObj.image[j]),
                soundHowl = loadSound('uploads/audios/' + StripExtFromFile(spriteObj.sound[j].toLowerCase())),
                categoryHowl = loadSound('uploads/categories/audios/' + StripExtFromFile(spriteObj.category.sound[j]));

            // categoryHowl = loadSound('uploads/categories/audios/' + this.categoryId[categoryName])

            wordArr.push(spriteObj.word[j]);
            answerArr.push(spriteObj.answer[j]);
            categoryArr.push(categoryName);
            imageArr.push(imageSprite);
            howlArr.push(soundHowl);
            categoryHowlArr.push(categoryHowl);

        };

        shuffle(imageArr, howlArr, wordArr, answerArr, categoryArr, categoryHowlArr);

        this.wallSlices.push({
            word: wordArr,
            answer: answerArr,
            image: imageArr,
            sound: howlArr,
            category: categoryArr,
            categorySound: categoryHowlArr,
        });
    }
};

Playground_3_2.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];

    this.pool.returnSprite(sprite);

    this.poolSlices = [];
    this.wallSlices.shift();
};

Playground_3_2.prototype.cleanMemory = function() {

    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    this.fromOutro = null;



    if (this.assetsToLoad)
        this.assetsToLoad.length = 0;

    for (var i = this.sceneContainer.children.length - 1; i >= 0; i--) {
        this.sceneContainer.removeChild(this.sceneContainer.children[i]);
    };
    this.sceneContainer = null;
};