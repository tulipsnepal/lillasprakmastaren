function Playground_4_1() {
    cl.show()

    PIXI.DisplayObjectContainer.call(this);

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position.set(77, 88);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();
    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.animationContainer = new PIXI.DisplayObjectContainer();
    this.feedbackContainer = new PIXI.DisplayObjectContainer();
    this.hoppingBoyContainer = new PIXI.DisplayObjectContainer();
    this.partsButton = [];

    localStorage.setItem('playground_4_1', 0);
    main.CurrentExercise = "playground_4_1";

    this.fromOutro = 0;

    this.common = new Common();

    this.bodyParts = ["hår", "öra", "ögon", "näsa", "panna", "mun", "haka", "hals", "axel", "armbåge", "arm", "hand", "häl", "knä", "ben", "mage", "lår", "tå", "fot"];

    // this.faceParts = ["panna", "ögon", "näsa", "mun", "tand", "tånder", "haka"];
    this.faceParts = ["hår", "öra", "ögon", "näsa", "panna", "mun", "haka", "hals", "axel", "armbåge", "arm", "hand", "häl", "knä", "ben", "mage", "lår", "tå", "fot"];

    main.texturesToDestroy = [];
    this.loadSpriteSheet();

    // everything connected to this is rendered.
    this.addChild(this.sceneContainer);
}

Playground_4_1.constructor = Playground_4_1;
Playground_4_1.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Playground_4_1.imagePath = 'build/exercises/playground/playground_4_1/images/';
Playground_4_1.audioPath = 'build/exercises/playground/playground_4_1/audios/';
Playground_4_1.totalFeedback = Main.TOTAL_FEEDBACK; // default 10


/**
 * load array of assets of different format
 */
Playground_4_1.prototype.loadSpriteSheet = function() {

    var self = this;

    this.introId = loadSound(Playground_4_1.audioPath + 'intro');
    this.outroId = loadSound(Playground_4_1.audioPath + 'outro');

    // create an array of assets to load
    this.assetsToLoad = [
        Playground_4_1.imagePath + "sprite_playground_4_1.json",
        Playground_4_1.imagePath + "sprite_playground_4_1.png",
        Playground_4_1.imagePath + "sprite_body_parts.json",
        Playground_4_1.imagePath + "sprite_body_parts.png",
        Playground_4_1.imagePath + "sprite_boys.json",
        Playground_4_1.imagePath + "sprite_boys.png"
    ];

    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();
}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
Playground_4_1.prototype.spriteSheetLoaded = function() {

    // hide loader
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    // fill exercise background
    this.sceneBackground = new PIXI.Graphics();
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xCAAABB);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);

    //add sky
    var sky = createRect({
        x: 3,
        y: 3,
        w: 1839,
        h: 362,
        color: 0xd6def2
    });
    this.sceneContainer.addChild(sky);

    //add road

    var road = createRect({
        x: 3,
        y: 363,
        w: 1839,
        h: 838,
        color: 0x878786
    });
    this.sceneContainer.addChild(road);

    // exercise instruction
    this.loadExercise();
    this.introScene();
    this.outroScene();
    this.help = PIXI.Sprite.fromFrame("help.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);
    this.sceneContainer.addChild(this.help);


    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.feedbackContainer);
    this.sceneContainer.addChild(this.exerciseContainer);
    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.animationContainer);
    this.sceneContainer.addChild(this.outroContainer);
    this.sceneContainer.addChild(this.hoppingBoyContainer);
};



/**
 * play intro animation
 */
Playground_4_1.prototype.introAnimation = function() {

    createjs.Sound.stop();
    main.overlay.visible = true;

    this.help.interactive = false;
    this.help.buttonMode = false;
    if (this.fromOutro == 1) {
        this.fromOutro = 0;
        this.resetExercise();
        this.restartExercise();
    }

    this.introContainer.visible = false;
    this.animationContainer.visible = true;
    // this.hopScotch.visible = false;
    // this.feedbackContainer.visible = false;

    this.intro_mouth.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
 0-3 sec arm_0
3-6 sec arm_1
6-9 sec arm_2
stop at arm_0

 */
Playground_4_1.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    console.log(currentPostion);
    if (currentPostion >= 1000 && currentPostion < 4000) {
        this.showHandAt(0, "intro");
    } else if (currentPostion >= 4000 && currentPostion < 6500) {
        this.showHandAt(1, "intro");
    } else if (currentPostion >= 6500 && currentPostion < 10000) {
        this.showHandAt(2, "intro");
    } else {
        this.showHandAt(0, "intro");
    }
};

/**
 * cancel running intro animation
 */
Playground_4_1.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);

    this.animationContainer.visible = false;
    this.hopScotch.visible = true;
    this.feedbackContainer.visible = true;
    this.introContainer.visible = true;
    this.intro_mouth.visible = false;
    this.help.interactive = true;
    this.help.buttonMode = true;

    this.ovaigen.visible = false;
    main.overlay.visible = false;
}

Playground_4_1.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    this.outroContainer.visible = true;

    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        var totalDuration = outroInstance.getDuration();
        self.timerId = setInterval(function() {
            var currentPostion = outroInstance.getPosition();
            /*
            0-5 sec arm_1
            5-stop arm_2
            */
            if (currentPostion >= 0 && currentPostion < 5000) {
                self.showHandAt(0, "outro");
                console.log("Position < 5000");
            } else {
                self.showHandAt(1, "outro");
                console.log("Position >= 5000");
            }
        }, 1);

        self.outro_mouth.show();
    }

    this.cancelHandler = this.cancelOutro.bind(this);

    outroInstance.addEventListener("complete", this.cancelHandler);

}

Playground_4_1.prototype.cancelOutro = function() {

    var self = this;
    clearInterval(this.timerId);
    createjs.Sound.stop();

    self.help.interactive = true;
    self.help.buttonMode = true;
    self.outro_mouth.hide();

    this.ovaigen.visible = true;
    main.overlay.visible = false;

};


Playground_4_1.prototype.showHandAt = function(number, scene) {
    var obj = scene == "outro" ? this.outro_hands : this.hands;
    if (obj !== null && obj.length !== 0) {
        for (var i = 0; i < obj.length; i++) {
            obj[i].visible = false;
        };
        if (isNumeric(number))
            obj[number].visible = true;
    }
}



/*
 *
 * create sprites, animation, sound related to intro scene */

Playground_4_1.prototype.introScene = function() {

    var self = this;

    this.introPerson = PIXI.Sprite.fromFrame("boy_intro.png");
    main.texturesToDestroy.push(this.introPerson);
    this.introPerson.position.set(112, 120);
    this.animationContainer.addChild(this.introPerson);

    //mouth
    this.intro_mouth = new Animation({
        "image": "mouth_intro_",
        "length": 5,
        "x": 292,
        "y": 316,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 5, 3, 2, 4, 1, 5],
        "frameId": 0
    });
    this.animationContainer.addChild(this.intro_mouth);



    //hands
    this.hands = [
        new PIXI.Sprite.fromFrame('arm_intro_1.png'),
        new PIXI.Sprite.fromFrame('arm_intro_2.png'),
        new PIXI.Sprite.fromFrame('arm_intro_3.png'),
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(287, 308);
        this.animationContainer.addChild(this.hands[i]);
        this.hands[i].visible = false;
    };
    this.animationContainer.visible = false;

}

/* *
 * create sprites, animation, sound related to outro scene */
Playground_4_1.prototype.outroScene = function() {

    var self = this;

    this.hoppingBoy = new Animation({
        "image": "hop",
        "length": 9,
        "x": 571,
        "y": 58,
        "speed": 2,
        "pattern": false
    });

    this.hoppingBoyContainer.addChild(this.hoppingBoy);
    this.hoppingBoy.visible = false;

    this.outroPerson = PIXI.Sprite.fromFrame("boy_outro.png");
    main.texturesToDestroy.push(this.outroPerson);
    this.outroPerson.position.set(1551, 407);
    this.outroContainer.addChild(this.outroPerson);

    //mouth
    this.outro_mouth = new Animation({
        "image": "mouth_outro_",
        "length": 5,
        "x": 1637,
        "y": 688,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 5, 3, 2, 4, 1, 5],
        "frameId": 0
    });
    this.outroContainer.addChild(this.outro_mouth);



    //hands
    this.outro_hands = [
        new PIXI.Sprite.fromFrame('arm_outro_1.png'),
        new PIXI.Sprite.fromFrame('arm_outro_2.png')
    ];
    for (var i = 0; i < this.outro_hands.length; i++) {
        this.outro_hands[i].position.set(1164, 660);
        this.outroContainer.addChild(this.outro_hands[i]);
        this.outro_hands[i].visible = false;
    };
    this.outroContainer.visible = false;

    this.outroContainer.visible = false;
    //back to intro page
    this.ovaigen = PIXI.Sprite.fromFrame('ova_igen.png');
    this.ovaigen.position.set(818, 531);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.ovaigen.visible = false;
    this.outroContainer.addChild(this.ovaigen);
}


Playground_4_1.prototype.renderFeedBack = function() {
    var self = this;

    var feedBackPosition = [
        638, 716,
        800, 680,
        874, 587,
        1042, 679,
        1101, 594,
        1234, 560,
        1275, 485,
        1423, 560,
        1474, 485,
        1571, 436
    ];

    for (var k = 0; k < 10; k++) {
        var feedbackSprite = PIXI.Sprite.fromFrame("fb" + (k + 1) + "_filled.png");

        feedbackSprite.position = {
            x: feedBackPosition[k * 2],
            y: feedBackPosition[k * 2 + 1]
        };

        main.texturesToDestroy.push(feedbackSprite);
        this.feedbackContainer.addChild(feedbackSprite);
        feedbackSprite.visible = false;
    }
};

/**
 * load exercise related stuffs
 */
Playground_4_1.prototype.loadExercise = function() {

    var spareChalk = new PIXI.Sprite.fromFrame("spare_chalk.png");
    main.texturesToDestroy.push(spareChalk);
    spareChalk.position.set(618, 1045);
    this.sceneContainer.addChild(spareChalk);

    //hopscotch
    this.hopScotch = new PIXI.Sprite.fromFrame("fb_base.png");
    main.texturesToDestroy.push(this.hopScotch);
    this.hopScotch.position.set(516, 438);
    this.sceneContainer.addChild(this.hopScotch);

    var boy = new PIXI.Sprite.fromFrame("boy.png");
    main.texturesToDestroy.push(boy);
    boy.position.set(112, 110);
    this.introContainer.addChild(boy);


    //add body parts
    var alpha = 0;

    var image_for_sprite = [
        "hair.png", "ear.png", "botheyes.png", "nose.png",
        "forehead.png", "mouth.png", "chin.png", "neck.png",
        "shoulder.png", "elbow.png", "arms.png", "hands.png", "heels.png",
        "knees.png", "legs.png", "stomach.png", "thighs.png", "toes.png", "feet.png"
    ];

    var partsPosition = [
        143, 110,
        218, 254,
        308, 261,
        368, 257,
        292, 204,
        304, 306,
        312, 342,
        258, 347,
        208, 368,
        111, 477,
        140, 397,
        201, 548,
        197, 1032,
        247, 830,
        203, 742,
        222, 520,
        222, 754,
        332, 1035,
        197, 991
    ];

    var pointerPosition = [
        247, 136,
        197, 243,
        292, 238,
        377, 270,
        316, 189,
        316, 283,
        329, 333,
        260, 348,
        197, 356,
        78, 486,
        393, 527,
        360, 678,
        146, 1045,
        247, 847,
        208, 919,
        288, 582,
        226, 754,
        432, 1035,
        260, 1048
    ];

    for (var k = 0; k < this.bodyParts.length; k++) {
        var parts = this.createBodyPartsButton({
            image: image_for_sprite[k],
            x: partsPosition[k * 2],
            y: partsPosition[k * 2 + 1],
            alpha: alpha,
            kValue: k,
            xpos: pointerPosition[k * 2],
            ypos: pointerPosition[k * 2 + 1],
            bodyPart: this.bodyParts[k]
        });
        this.partsButton.push(parts);
        this.introContainer.addChild(parts);
    }

    this.feedBackCircle = new PIXI.Sprite.fromFrame("circle.png");
    this.introContainer.addChild(this.feedBackCircle);
    this.feedBackCircle.visible = false;

    if (Device.iPad) {
        this.circleGraphics = createRect({
            w: 200,
            h: 100
        });
        this.circleGraphics.alpha = 0;

        this.circleGraphics.interactive = true;
        this.circleGraphics.buttonMode = true;

        this.circleGraphics.visible = false;

        this.introContainer.addChild(this.circleGraphics);
    }

    this.renderFeedBack();
    this.runExercise();

}

Playground_4_1.prototype.createBodyPartsButton = function(params) {
    var sprite = new PIXI.Sprite.fromFrame(params.image);
    sprite.position.set(params.x, params.y);
    // if (params.kValue != 2) {
    sprite.interactive = true;
    sprite.buttonMode = true;
    // }
    sprite.alpha = params.alpha || 1;
    sprite.bodyPart = params.bodyPart;
    sprite.xPos = params.xpos;
    sprite.yPos = params.ypos;
    sprite.hitArea = new PIXI.TransparencyHitArea.create(sprite);
    return sprite;
}


/**
 * manage exercise with new sets randomized
 */
Playground_4_1.prototype.runExercise = function() {

    var self = this;

    var rand = makeUniqueRandom(18);
    // console.log("uniqueRandom = " + rand);

    var questionWord = this.bodyParts[rand];

    var rect = createRect({
        w: 368,
        h: 108
    });
    var qText = new PIXI.Text("" + questionWord, {
        font: "75px Arial"
    });
    qText.position = {
        x: (368 - qText.width) / 2,
        y: (108 - qText.height) / 2 + 6
    };
    rect.addChild(qText);
    rect.position.set(760, 169);
    this.exerciseContainer.addChild(rect);

    this.hitAreaInteractivity(true);

    for (var k = 0; k < this.partsButton.length; k++) {
        this.partsButton[k].click = this.partsButton[k].tap = function() {
            self.hitAreaInteractivity(false);
            var obj = this;

            if (this.bodyPart == questionWord) { //correct answer
                correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                self.feedBackCircle.tint = 0x00ff00;
                self.common.showRedOrGreenBox(obj.xPos, obj.yPos, self.feedBackCircle);
                var a = setTimeout(function() {
                    self.correctFeedback();
                }, 500);
            } else { //wrong answer
                wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                self.feedBackCircle.tint = 0xff0000;
                self.common.showRedOrGreenBox(obj.xPos, obj.yPos, self.feedBackCircle);
                var a = setTimeout(function() {
                    self.wrongFeedback();
                }, 500);
            }
        }
    }

    if (Device.iPad) {

        if (this.faceParts.indexOf(qText.text) > -1) {
            var objIndex = this.bodyParts.indexOf(qText.text),
                obj = this.partsButton[objIndex];

            this.circleGraphics.position.x = obj.x - 30;
            this.circleGraphics.position.y = obj.y - 30;
            this.circleGraphics.width = obj.width + 60;
            this.circleGraphics.height = obj.height + 60;

            this.circleGraphics.visible = true;

            this.circleGraphics.click = this.circleGraphics.tap = function() {
                self.hitAreaInteractivity(false);

                if (self.faceParts.indexOf(obj.bodyPart) > -1) {
                    // console.log('yes index');
                    correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                    self.feedBackCircle.tint = 0x00ff00;
                    self.common.showRedOrGreenBox(obj.xPos, obj.yPos, self.feedBackCircle);
                    setTimeout(function() {
                        self.correctFeedback();
                    }, 500);
                } else { //wrong answer
                    // console.log('no index');
                    wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                    self.feedBackCircle.tint = 0xff0000;
                    self.common.showRedOrGreenBox(obj.xPos, obj.yPos, self.feedBackCircle);
                    setTimeout(function() {
                        self.wrongFeedback();
                    }, 500);
                }
            }
        }
    }

};

/**
 * toggle interactivity of hitarea
 */
Playground_4_1.prototype.hitAreaInteractivity = function(bool) {

    var bool = bool || false;

    if (Device.iPad) {
        this.circleGraphics.interactive = bool;
        this.circleGraphics.buttonMode = bool;
    }

    for (var k = 0; k < this.partsButton.length; k++) {
        this.partsButton[k].interactive = bool;
        this.partsButton[k].buttonMode = bool;
    }
}

/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
Playground_4_1.prototype.correctFeedback = function() {
    var self = this;
    var feedback = localStorage.getItem('playground_4_1');
    feedback++;

    localStorage.setItem('playground_4_1', feedback);
    this.updateFeedback();

    if (feedback >= Playground_4_1.totalFeedback) {
        var c = setTimeout(this.onComplete.bind(this), 2000);
    } else {
        var c = setTimeout(function() {
            self.getNewExerciseSet();
        }, 400);
    }
}

Playground_4_1.prototype.wrongFeedback = function() {
    var feedback = localStorage.getItem('playground_4_1');
    feedback--;

    if (feedback < 0)
        feedback = 0;

    localStorage.setItem('playground_4_1', feedback);
    this.updateFeedback();

}


Playground_4_1.prototype.updateFeedback = function() {

    var self = this;

    var counter = localStorage.getItem('playground_4_1');
    for (var i = 0; i < self.feedbackContainer.children.length; i++) {
        self.feedbackContainer.children[i].visible = false;
    }

    for (var j = 0; j < counter; j++) {
        self.feedbackContainer.children[j].visible = true;
    }
    setTimeout(function() {
        self.hitAreaInteractivity(true);
    }, 1000)
}

Playground_4_1.prototype.getNewExerciseSet = function() {

    this.exerciseContainer.removeChildAt(0);

    this.runExercise();


}

Playground_4_1.prototype.restartExercise = function() {

    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    this.exerciseContainer.visible = true;
    localStorage.setItem('playground_4_1', 0);
    this.getNewExerciseSet();
};

/**
 * the outro scene is displayed
 */
Playground_4_1.prototype.onComplete = function() {

    var self = this;

    this.introMode = false;

    this.help.interactive = false;
    this.help.buttonMode = false;
    self.introContainer.visible = false;
    this.exerciseContainer.visible = false;
    this.animationContainer.visible = false;
    this.fromOutro = 1;
    this.hoppingBoy.visible = true;

    var a = setTimeout(function() {
        self.hoppingBoy.showOnce(9);
    }, 500);

    var b = setTimeout(self.outroAnimation.bind(this), 5200);

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        self.fromOutro = 0;
        self.cancelOutro();
        self.animationContainer.visible = false;
        self.resetExercise();
        self.restartExercise();
    }
}

Playground_4_1.prototype.resetExercise = function() {
    this.ovaigen.visible = false;
    console.log(uniqueRandoms);
    if (uniqueRandoms) uniqueRandoms = [];
    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    this.hoppingBoy.visible = false;
    this.hoppingBoy.gotoAndStop(0);
    for (var i = this.feedbackContainer.children.length - 1; i >= 0; i--) {
        this.feedbackContainer.children[i].visible = false;
    };
    this.introMode = true;
};

/**
 * clean memory by clearing reference of object
 */
Playground_4_1.prototype.cleanMemory = function() {


    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    if (this.assetsToLoads)
        this.assetsToLoad.length = 0;

    this.hoppingBoyContainer = null;
    if (this.partsButton) this.partsButton.length = 0;
    this.common = null;
    if (this.bodyParts) this.bodyParts.length = 0;
    this.introPerson = null;
    this.intro_mouth = null;
    this.hoppingBoy = null;
    this.outroPerson = null;
    this.outro_mouth = null;
    this.outro_hands = null;
    this.fromOutro = null;



    this.hopScotch = null;
    this.feedBackCircle = null;

    this.help = null;
    this.ovaigen = null;

    this.introContainer = null;
    this.outroContainer = null;
    this.cancelHandler = null;

    this.introId = null;
    this.outroId = null;

    this.exerciseContainer = null;
    this.animationContainer = null;
    this.introPerson = null;
    this.intro_mouth = null;
    this.outroPerson = null;
    this.outro_mouth = null;
    this.hands = null;
    this.outro_hands = null;

    for (var i = this.sceneContainer.children.length - 1; i >= 0; i--) {
        this.sceneContainer.removeChild(this.sceneContainer.children[i]);
    };

    this.sceneContainer = null;
};