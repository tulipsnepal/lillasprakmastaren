function Playground_4_2() {
    cl.show()

    PIXI.DisplayObjectContainer.call(this);

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position.set(77, 88);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();
    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.animationContainer = new PIXI.DisplayObjectContainer();


    localStorage.setItem('playground_4_2', 0);
    main.CurrentExercise = "playground_4_2";

    main.eventRunning = false;

    this.verbs = [];
    this.nouns = [];

    this.fromOutro = 0;

    this.chancesUsed = 0;

    main.texturesToDestroy = [];


    this.loadGameJson();

    // everything connected to this is rendered.
    this.addChild(this.sceneContainer);
}

Playground_4_2.constructor = Playground_4_2;
Playground_4_2.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Playground_4_2.imagePath = 'build/exercises/playground/playground_4_2/images/';
Playground_4_2.audioPath = 'build/exercises/playground/playground_4_2/audios/';
Playground_4_2.totalFeedback = 8; //Main.TOTAL_FEEDBACK;


/**
 * load array of assets of different format
 */
Playground_4_2.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(Playground_4_2.audioPath + 'intro');
    this.outroId = loadSound(Playground_4_2.audioPath + 'outro');

    // create an array of assets to load
    this.assetsToLoad = [
        Playground_4_2.imagePath + "sprite_playground_4_2.json",
        Playground_4_2.imagePath + "sprite_playground_4_2.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();



}

Playground_4_2.prototype.loadGameJson = function() {
    var self = this;

    var exerciseJSON = new PIXI.JsonLoader('uploads/exercises/playground/playground_4_2.json?nocache=' + (new Date()).getTime());
    exerciseJSON.on('loaded', function(evt) {
        self.verbs = exerciseJSON.json[0].verbs;
        self.nouns = exerciseJSON.json[0].nouns;
        self.loadSpriteSheet();
    });
    exerciseJSON.load();
}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
Playground_4_2.prototype.spriteSheetLoaded = function() {

    // hide loader
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    // fill exercise background
    this.sceneBackground = new PIXI.Graphics();
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xCAAABB);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);

    //add sky
    var sky = createRect({
        x: 3,
        y: 3,
        w: 1839,
        h: 362,
        color: 0xd6def2
    });
    this.sceneContainer.addChild(sky);

    //add road

    var road = createRect({
        x: 3,
        y: 363,
        w: 1839,
        h: 838,
        color: 0x878786
    });
    this.sceneContainer.addChild(road);

    // exercise instruction
    this.loadExercise();
    this.introScene();
    this.outroScene();
    this.help = PIXI.Sprite.fromFrame("help.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);
    this.sceneContainer.addChild(this.help);


    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.outroContainer);
    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.exerciseContainer);
    this.sceneContainer.addChild(this.animationContainer);
}

/**
 * play intro animation
 */
Playground_4_2.prototype.introAnimation = function() {

    createjs.Sound.stop();
    main.overlay.visible = true;

    this.help.interactive = false;
    this.help.buttonMode = false;
    if (this.fromOutro == 1) {
        this.fromOutro = 0;
        this.restartExercise();
        this.resetExercise();
    }
    this.animationContainer.visible = true;
    this.introContainer.visible = false;
    //this.outro_hand.visible = false;

    this.intro_mouth.show();
    this.eyes.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
 0-2 sec arm_1 =>0
2-4 sec arm_2 =>1
4-6 sec arm_3 =>2
stop at arm_1 =>0

 */
Playground_4_2.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    if (currentPostion >= 1000 && currentPostion < 2000) {
        this.showHandAt(0);
    } else if (currentPostion >= 2000 && currentPostion < 4000) {
        this.showHandAt(1);
    } else if (currentPostion >= 4000 && currentPostion < 6000) {
        this.showHandAt(2);
    } else {
        this.showHandAt(0);
    }
};


/**
 * cancel running intro animation
 */
Playground_4_2.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);

    this.animationContainer.visible = false;
    this.introContainer.visible = true;
    this.intro_mouth.visible = false;
    this.eyes.visible = false;

    this.help.interactive = true;
    this.help.buttonMode = true;

    this.ovaigen.visible = false;
    main.overlay.visible = false;
}

Playground_4_2.prototype.outroAnimation = function() {

    var self = this;

    this.animationContainer.visible = true;

    //this.outro_hand.visible = true;
    for (var k = 0; k < this.hands.length; k++) {
        //  this.hands[k].visible = false;
    }

    this.outroInstance = createjs.Sound.play(this.outroId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady()) {
        this.timerId = setInterval(this.animateOutro.bind(this), 1);
        main.overlay.visible = true;
        self.intro_mouth.show();
        self.eyes.show();
    }

    this.cancelHandler = this.cancelOutro.bind(this);

    this.outroInstance.addEventListener("complete", this.cancelHandler);

}

/** for outro
 * handle mouth, hand with specific sound positions or queue points.
0-1.5 sec arm_1
1.5-stop arm_2
 */
Playground_4_2.prototype.animateOutro = function() {

    var currentPostion = this.outroInstance.getPosition();
    if (currentPostion >= 0 && currentPostion < 1500) {
        this.showHandAt(0);
    } else {
        this.showHandAt(1);
    }
};

Playground_4_2.prototype.cancelOutro = function() {

    var self = this;
    clearInterval(this.timerId);
    createjs.Sound.stop();

    self.help.interactive = true;
    self.help.buttonMode = true;
    self.intro_mouth.hide();

    this.ovaigen.visible = true;
    main.overlay.visible = false;

};


Playground_4_2.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    };
    if (isNumeric(number))
        this.hands[number].visible = true;
}

/*
 *
 * create sprites, animation, sound related to intro scene */

Playground_4_2.prototype.introScene = function() {

    var self = this;



    this.introPerson = PIXI.Sprite.fromFrame("intro_boy_body.png");
    main.texturesToDestroy.push(this.introPerson);
    this.introPerson.position.set(1073, 243);
    this.animationContainer.addChild(this.introPerson);

    //mouth
    this.intro_mouth = new Animation({
        "image": "mouth",
        "length": 5,
        "x": 1153,
        "y": 520,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 5, 3, 2, 4, 1, 5],
        "frameId": 0
    });
    this.animationContainer.addChild(this.intro_mouth);

    //eyes
    this.eyes = new Animation({
        "image": "eyes",
        "length": 2,
        "x": 1091,
        "y": 437,
        "speed": 1,
        "pattern": false,
        "frameId": 2
    });
    this.animationContainer.addChild(this.eyes);


    //hands
    this.hands = [
        new PIXI.Sprite.fromFrame('arm1.png'),
        new PIXI.Sprite.fromFrame('arm2.png'),
        new PIXI.Sprite.fromFrame('arm3.png')
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(692, 474);
        this.animationContainer.addChild(this.hands[i]);
        this.hands[i].visible = false;
    };
    this.animationContainer.visible = false;

}

/* *
 * create sprites, animation, sound related to outro scene */
Playground_4_2.prototype.outroScene = function() {

    var self = this;


    this.outroContainer.visible = false;
    //back to intro page
    this.ovaigen = PIXI.Sprite.fromFrame('ova_igen.png');
    this.ovaigen.position.set(356, 419);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.ovaigen.visible = false;
    this.outroContainer.addChild(this.ovaigen);
}



/**
 * load exercise related stuffs
 */
Playground_4_2.prototype.loadExercise = function() {

    var spareChalk = new PIXI.Sprite.fromFrame("spare_chalk.png");
    main.texturesToDestroy.push(spareChalk);
    spareChalk.position.set(618, 1044);
    this.sceneContainer.addChild(spareChalk);

    //hopscotch
    var hopScotch = new PIXI.Sprite.fromImage(Playground_4_2.imagePath + "hopscotch.png");
    main.texturesToDestroy.push(hopScotch);
    hopScotch.position.set(852, 692);
    this.sceneContainer.addChild(hopScotch);

    this.txtHoppingBoy = new PIXI.Texture.fromFrame('boy.png');
    this.txtHoppingBoyOnTwoFeet = new PIXI.Texture.fromFrame('boy_on_two_feet.png');
    this.txtHoppingBoyWobble = new PIXI.Texture.fromFrame('boy_wobble.png');
    this.txtHoppingBoyFall = new PIXI.Texture.fromFrame('boy_fell.png');

    this.hoppingBoySizes = [
        536, 687,
        504, 646,
        474, 607,
        436, 558,
        446, 571,
        419, 537,
        385, 494,
        370, 475,
        348, 447
    ];

    this.hoppingBoyPositions = [
        867, 367,
        987, 338,
        1081, 349,
        1170, 390,
        1294, 309,
        1384, 309,
        1470, 370,
        1558, 310,
        1641, 308
    ];

    this.hoppingBoy = new PIXI.Sprite(this.txtHoppingBoy);
    main.texturesToDestroy.push(this.hoppingBoy);
    this.hoppingBoy.interactive = true;
    this.hoppingBoy.buttonMode = true;

    this.positionHoppingBoy(0);
    this.introContainer.addChild(this.hoppingBoy);

    //now create 9 boxes to hold exercise data.
    var whiteRect = createRect({
        w: 247,
        h: 247,
        color: 0xffffff,
        lineStyle: 1,
        lineWidth: 1
    });

    this.whiteRectTexture = whiteRect.generateTexture();


    //
    this.runExercise();

}

Playground_4_2.prototype.positionHoppingBoy = function(k) {
    var self = this;
    self.hoppingBoy.width = self.hoppingBoySizes[k * 2];
    self.hoppingBoy.height = self.hoppingBoySizes[k * 2 + 1];
    self.hoppingBoy.position = {
        x: self.hoppingBoyPositions[k * 2],
        y: self.hoppingBoyPositions[k * 2 + 1]
    };
    self.hoppingBoy.hitArea = new PIXI.TransparencyHitArea.create(self.hoppingBoy);
};

/**
 * manage exercise with new sets randomized
 */
Playground_4_2.prototype.runExercise = function() {

    var self = this;
    var maxWordPerSet = 9;
    this.verbsPosition = [];
    this.selectedAnswers = [];
    this.clickedArrays = [];
    this.tileButton = [];
    this.hoppingBoy.interactive = false;

    shuffle(this.verbs);
    shuffle(this.nouns);

    var arrNoOfVerbToTake = [2, 3, 4, 5]; //array to determine how much verbs to fetch randomly between 2 to 5.
    var noOfVerbs = arrNoOfVerbToTake[Math.floor(Math.random() * arrNoOfVerbToTake.length)];

    var noOfNouns = 9 - noOfVerbs;
    var answerVerbs = [];
    var answerNouns = [];



    var verbsArr = this.getRandom(this.verbs, noOfVerbs);
    for (var k = 0; k < noOfVerbs; k++) {
        answerVerbs.push(1);
    }

    var nounsArr = this.getRandom(this.nouns, noOfNouns);
    for (var k = 0; k < noOfNouns; k++) {
        answerNouns.push(0);
    }
    console.log("VERBS => " + verbsArr);

    var totalSet = verbsArr.concat(nounsArr);
    var totalAnswers = answerVerbs.concat(answerNouns);

    shuffle(totalSet, totalAnswers);

    var y = 69,
        v = 0,
        w = 0;

    for (var k = 0; k < 9; k++) {
        var exerciseBoxes = new PIXI.Sprite(this.whiteRectTexture);

        if (k == 3 || k == 6) {
            v = 0;
            w++;
        }


        exerciseBoxes.position = {
            x: 93 + (v * 260),
            y: 69 + (w * 260)
        }

        v++;

        var textLabel = new PIXI.Text("" + totalSet[k], {
            font: "55px Arial"
        });

        textLabel.position = {
            x: (247 - textLabel.width) / 2 + 10,
            y: (247 - textLabel.height) / 2 + 10
        };

        //store verb positions
        if (totalAnswers[k] == 1) {
            this.verbsPosition.push(k);
        }

        exerciseBoxes.addChild(textLabel);
        exerciseBoxes.interactive = true;
        exerciseBoxes.buttonMode = true;
        exerciseBoxes.hitIndex = k;
        this.tileButton.push(exerciseBoxes);

        this.exerciseContainer.addChild(exerciseBoxes);
    }

    for (var x = 0; x < this.tileButton.length; x++) {
        this.tileButton[x].click = this.tileButton[x].tap = function() {
            var obj = this;
            var idx = self.clickedArrays.indexOf(this.hitIndex);

            if (idx > -1) {
                this.tint = 0xffffff;
                self.clickedArrays.splice(idx, 1);
                self.selectedAnswers.splice(idx, 1);
            } else {
                this.tint = 0xcccccc;
                self.clickedArrays.push(this.hitIndex);
                self.selectedAnswers.push(totalAnswers[this.hitIndex]);
            }

            if (self.clickedArrays.length > 0) { //enable boy button only if user has selected words
                self.hoppingBoy.interactive = true;
            }

        }
    }
    // console.log("verbs position = " + self.verbsPosition);

    this.hoppingBoy.click = this.hoppingBoy.tap = function() { //evaluate answer only after the hopping boy is clicked.
        self.hoppingBoy.interactive = false;

        /*
        The answer will be correct only if the length of selected words' array is equal to no. of verbs in the set
        and all elements in the array is same and each equals = 1
        */
        if (self.selectedAnswers.AllValuesSame() === true && self.selectedAnswers.length === noOfVerbs && self.selectedAnswers[0] === 1) {
            correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
            for (var y = 0; y < self.verbsPosition.length; y++) { //
                self.tileButton[self.verbsPosition[y]].tint = 0x00ff00;
            }

            var a = setTimeout(function() {

                self.selectedAnswers = [];
                self.clickedArrays = [];
                self.verbsPosition = [];
                self.chancesUsed = 0;
                self.correctFeedback();

                var pos = localStorage.getItem('playground_4_2');
                if (pos == 3 || pos == 6) {
                    self.hoppingBoy.setTexture(self.txtHoppingBoyOnTwoFeet);
                } else {
                    self.hoppingBoy.setTexture(self.txtHoppingBoy);
                }

                self.hoppingBoy.interactive = true;
            }, 2000);

        } else {
            wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
            if (self.chancesUsed == 0) { //first attempt is wrong
                self.hoppingBoy.setTexture(self.txtHoppingBoyWobble);
                var pos = localStorage.getItem('playground_4_2');
                if (pos == 3) {
                    self.hoppingBoy.position = {
                        x: 1170,
                        y: 350
                    };
                } else if (pos == 6) {
                    self.hoppingBoy.position = {
                        x: 1470,
                        y: 320
                    };
                }
                self.hoppingBoy.interactive = true;
                self.chancesUsed = 1;
            } else { // player has answered wrong in second attempt as well so wrong feedback
                self.hoppingBoy.setTexture(self.txtHoppingBoyFall);
                self.hoppingBoy.interactive = false;
                self.selectedAnswers = [];
                self.clickedArrays = [];

                for (var y = 0; y < self.verbsPosition.length; y++) {
                    self.tileButton[self.verbsPosition[y]].tint = 0x698f10;
                }

                var a = setTimeout(function() {
                    for (var y = 0; y < self.tileButton.length; y++) {
                        self.tileButton[y].tint = 0xffffff;
                    }
                    self.hoppingBoy.setTexture(self.txtHoppingBoy);
                    self.chancesUsed = 0;
                    self.wrongFeedback();
                    self.hoppingBoy.interactive = true;
                }, 2500);

            }
            // console.log("WRONG ANSWER");
        }
    }
};


Playground_4_2.prototype.getRandom = function(arr, n) {
    var result = new Array(n),
        len = arr.length,
        taken = new Array(len);

    // console.log(len);
    // console.log("num " + n);

    if (n > len)
        throw new RangeError("getRandom: more elements taken than available");

    while (n--) {
        var x = Math.floor(Math.random() * len);
        result[n] = arr[x in taken ? taken[x] : x];
        taken[x] = --len;
    }
    return result;
}



/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
Playground_4_2.prototype.correctFeedback = function() {
    var self = this;
    var feedback = localStorage.getItem('playground_4_2');
    feedback++;

    localStorage.setItem('playground_4_2', feedback);
    this.updateFeedback();

    if (feedback >= Playground_4_2.totalFeedback) {
        var c = setTimeout(this.onComplete.bind(this), 2000);
    } else {
        var c = setTimeout(function() {
            self.getNewExerciseSet();
            main.eventRunning = false;
        }, 400);
    }
}

Playground_4_2.prototype.wrongFeedback = function() {
    var feedback = localStorage.getItem('playground_4_2');
    feedback--;

    if (feedback < 0)
        feedback = 0;

    localStorage.setItem('playground_4_2', feedback);
    this.updateFeedback();

    this.getNewExerciseSet();
}


Playground_4_2.prototype.updateFeedback = function() {

    var self = this;

    var counter = localStorage.getItem('playground_4_2');
    console.log("COUNTER = " + counter);
    self.positionHoppingBoy(counter);
}

Playground_4_2.prototype.getNewExerciseSet = function() {

    for (var i = this.exerciseContainer.children.length - 1; i >= 0; i--) {
        this.exerciseContainer.removeChild(this.exerciseContainer.children[i]);
    };

    main.eventRunning = false;

    this.runExercise();
}

Playground_4_2.prototype.restartExercise = function() {

    this.introContainer.visible = true;
    this.outroContainer.visible = false;
    this.exerciseContainer.visible = true;
    localStorage.setItem('playground_4_2', 0);
    this.getNewExerciseSet();
};

/**
 * the outro scene is displayed
 */
Playground_4_2.prototype.onComplete = function() {

    var self = this;
    this.introMode = false;

    this.help.interactive = false;
    this.help.buttonMode = false;
    this.positionHoppingBoy(0);
    this.introContainer.visible = false;
    this.outroContainer.visible = true;
    this.exerciseContainer.visible = false;
    this.fromOutro = 1;

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        self.fromOutro = 0;
        self.cancelOutro(1);
        self.animationContainer.visible = false;
        // self.outro_hand.visible = false;
        self.resetExercise();
        self.restartExercise();
    }

    this.outroAnimation();

}

Playground_4_2.prototype.resetExercise = function() {
    this.ovaigen.visible = false;
    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    this.introMode = true;
};

/**
 * clean memory by clearing reference of object
 */
Playground_4_2.prototype.cleanMemory = function() {


    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    if (this.assetsToLoads)
        this.assetsToLoad.length = 0;


    this.help = null;
    this.ovaigen = null;


    this.introContainer = null;
    this.outroContainer = null;
    this.cancelHandler = null;

    this.fromOutro = null;

    this.introId = null;
    this.outroId = null;

    this.exerciseContainer = null;
    this.animationContainer = null;
    this.introPerson = null;
    this.intro_mouth = null;
    this.eyes = null;
    this.outroPerson = null;
    this.outro_mouth = null;
    this.outro_eyes = null;
    this.txtHoppingBoy = null;
    this.txtHoppingBoyWobble = null;
    this.txtHoppingBoyFall = null;
    this.hoppingBoy = null;
    this.whiteRectTexture = null;

    if (this.introInstance) this.introInstance = null;
    if (this.outroInstance) this.outroInstance = null;

    if (this.verbs) this.verbs.length = 0;
    if (this.nouns) this.nouns.length = 0;
    if (this.hoppingBoySizes) this.hoppingBoySizes.length = 0;
    if (this.hoppingBoyPositions) this.hoppingBoyPositions.length = 0;
    if (this.verbsPosition) this.verbsPosition.length = 0;
    if (this.selectedAnswers) this.selectedAnswers.length = 0;
    if (this.clickedArrays) this.clickedArrays.length = 0;
    if (this.tileButton) this.tileButton.length = 0;

    this.chancesUsed = null;

    for (var i = this.sceneContainer.children.length - 1; i >= 0; i--) {
        this.sceneContainer.removeChild(this.sceneContainer.children[i]);
    };

    this.sceneContainer = null;
};

Array.prototype.AllValuesSame = function() {

    if (this.length > 0) {
        for (var i = 1; i < this.length; i++) {
            if (this[i] !== this[0])
                return false;
        };

    }

    return true;
}