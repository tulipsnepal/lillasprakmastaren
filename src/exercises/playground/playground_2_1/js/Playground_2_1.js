function Playground_2_1() {
    cl.show()

    PIXI.DisplayObjectContainer.call(this);

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position.set(77, 88);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();
    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.animationContainer = new PIXI.DisplayObjectContainer();

    localStorage.setItem('playground_2_1', 0);
    main.CurrentExercise = "playground_2_1";

    main.eventRunning = false;

    main.texturesToDestroy = [];
    this.itemsDragged = [-1, -1, -1];
    this.dropArea = [false, false, false];

    this.common = new Common();

    this.loadSpriteSheet();

    // everything connected to this is rendered.
    this.addChild(this.sceneContainer);
}

Playground_2_1.constructor = Playground_2_1;
Playground_2_1.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Playground_2_1.imagePath = 'build/exercises/playground/playground_2_1/images/';
Playground_2_1.audioPath = 'build/exercises/playground/playground_2_1/audios/';
Playground_2_1.totalFeedback = Main.TOTAL_FEEDBACK; // default 10


/**
 * load array of assets of different format
 */
Playground_2_1.prototype.loadSpriteSheet = function() {

    var self = this;

    this.introId = loadSound(Playground_2_1.audioPath + 'intro');
    this.outroId = loadSound(Playground_2_1.audioPath + 'outro');

    // create an array of assets to load
    this.assetsToLoad = [
        Playground_2_1.imagePath + "sprite_playground_2_1.json",
        Playground_2_1.imagePath + "sprite_playground_2_1.png",
        Playground_2_1.imagePath + "sprite_playground_2_1_fb_1.json",
        Playground_2_1.imagePath + "sprite_playground_2_1_fb_1.png",
        Playground_2_1.imagePath + "sprite_playground_2_1_fb_2.json",
        Playground_2_1.imagePath + "sprite_playground_2_1_fb_2.png",
        Playground_2_1.imagePath + "sprite_playground_2_1_fb_3.json",
        Playground_2_1.imagePath + "sprite_playground_2_1_fb_3.png",
        Playground_2_1.imagePath + "base.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
Playground_2_1.prototype.spriteSheetLoaded = function() {

    // hide loader
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    // fill exercise background
    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0xB9D3F0);
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xCAAABB);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);

    // exercise instruction
    this.help = PIXI.Sprite.fromFrame("help.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);
    this.sceneContainer.addChild(this.help);

    this.loadExercise();
    this.introScene();
    this.outroScene();

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.outroContainer);
    this.sceneContainer.addChild(this.exerciseContainer);
    this.sceneContainer.addChild(this.animationContainer);
}

/**
 * play intro animation
 */
Playground_2_1.prototype.introAnimation = function() {

    createjs.Sound.stop();
    main.overlay.visible = true;

    if (!this.introMode)
        this.restartExercise();

    this.help.interactive = false;
    this.help.buttonMode = false;

    this.animationContainer.visible = true;
    this.feedbackClimber.visible = false;

    this.intro_mouth.show();
    this.EyesShow.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
 */
Playground_2_1.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    console.log(currentPostion)
    if (currentPostion >= 1000 && currentPostion < 3000) {
        /*        this.showHandAt(1);
            } else if (currentPostion >= 2000 && currentPostion < 3000) {
        */
        this.showHandAt(0);
    } else if (currentPostion >= 3000 && currentPostion < 5723) {
        this.showHandAt(1);
    } else {
        this.showHandAt(0);
    }
};

/**
 * cancel running intro animation
 */
Playground_2_1.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);

    this.feedbackClimber.visible = true;
    this.intro_mouth.hide();
    this.EyesShow.hide();
    this.animationContainer.visible = false;

    this.help.interactive = true;
    this.help.buttonMode = true;

    this.ovaigen.visible = false;
    main.overlay.visible = false;

}

Playground_2_1.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.intro_mouth.show();
        self.EyesShow.show();
    }
    this.outroCancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.outroCancelHandler);

}

Playground_2_1.prototype.cancelOutro = function() {

    var self = this;

    createjs.Sound.stop();
    self.animationContainer.visible = false;
    self.intro_mouth.hide(8);
    self.ovaigen.visible = true;

    self.help.interactive = true;
    self.help.buttonMode = true;
    main.overlay.visible = false;

};

Playground_2_1.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    };
    if (isNumeric(number))
        this.hands[number].visible = true;
}

/**
 * create sprites,animation,sound related to intro scene
 */
Playground_2_1.prototype.introScene = function() {

    var self = this;

    this.introPerson = new PIXI.Sprite.fromFrame('girl-intro.png');
    this.introPerson.position.set(436, 266);
    main.texturesToDestroy.push(this.introPerson);
    this.animationContainer.addChild(this.introPerson);

    this.hands = [
        new PIXI.Sprite.fromFrame('arm1.png'),
        new PIXI.Sprite.fromFrame('arm2.png')
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(958, 504);
        this.animationContainer.addChild(this.hands[i]);
        this.hands[i].visible = false;
    };

    this.intro_mouth = new Animation({
        "image": "mouth",
        "length": 5,
        "x": 1209,
        "y": 440,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 5, 3, 2, 4, 1, 5],
        "frameId": 2
    });

    this.animationContainer.addChild(this.intro_mouth);

    this.EyesShow = new Animation({
        "image": "eyes",
        "length": 2,
        "x": 1180,
        "y": 374,
        "speed": 1,
        "pattern": false,
        "frameId": 2
    });

    this.animationContainer.addChild(this.EyesShow);

    this.animationContainer.visible = false;
}

/**
 * create sprites,animation,sound related to outro scene
 */
Playground_2_1.prototype.outroScene = function() {

    var self = this;

    this.outroContainer.visible = false;

    // back to intro page
    this.ovaigen = PIXI.Sprite.fromFrame('ova_igen.png');
    this.ovaigen.position.set(597, 492);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.ovaigen.visible = false;
    this.outroContainer.addChild(this.ovaigen);
}

/**
 * load exercise related stuffs
 */
Playground_2_1.prototype.loadExercise = function() {

    var sandTray = new PIXI.Sprite.fromImage(Playground_2_1.imagePath + 'base.png');
    sandTray.position.set(3, 3);
    main.texturesToDestroy.push(sandTray);
    this.sceneContainer.addChild(sandTray);

    this.whiteTextBackground = createRect({
        w: 208,
        h: 88,
        color: 0xffffff
    });
    this.whiteTextRect = this.whiteTextBackground.generateTexture();

    for (var k = 0; k < 3; k++) {
        var answerBox = new PIXI.Sprite(this.whiteTextRect);
        answerBox.position = {
            x: 920,
            y: 270 + (k * 199)
        };
        this.introContainer.addChild(answerBox);
    }

    var fbTexture = new PIXI.Texture.fromFrame("girl-base.png");
    this.feedbackClimber = new PIXI.Sprite(fbTexture);
    this.feedbackClimber.position.set(1181, 3);
    this.sceneContainer.addChild(this.feedbackClimber);

    //
    this.initObjectPooling();
}


/**
 * manage exercise with new sets randomized
 */
Playground_2_1.prototype.runExercise = function() {
    var self = this;
    this.dragItem = [];
    this.questionWidth = [];


    this.chosenOne = {
        "0": '',
        "1": '',
        "2": ''
    };

    this.correctAnswer = [];

    /*this.dropArea1 = false;
    this.dropArea2 = false;
    this.dropArea3 = false;*/

    this.jsonData = this.wallSlices[0];

    for (var k = 0; k < 3; k++) {
        //render questions
        var text = this.jsonData.sentences[k];
        text = text.replace("_", " ____ ");
        var questions = new PIXI.Text("" + text, {
            font: "55px Arial"
        });
        questions.position = {
            x: 311,
            y: 269 + (k * 200) + 2
        };

        self.exerciseContainer.addChild(questions);

        //render answers


        var optionWords = new PIXI.Text("" + this.jsonData.options[k], {
            font: "55px Arial"
        });

        optionWords.position = {
            x: 920 + (208 - optionWords.width) / 2,
            y: questions.position.y + 10
        };

        optionWords.oldPosition = {
            x: 920 + (208 - optionWords.width) / 2,
            y: questions.position.y + 10
        };

        optionWords.interactive = true;
        optionWords.buttonMode = true;

        /* optionWords.dropPosition = [
             408, 246,
             408, 448,
             408, 650
         ];*/

        optionWords.dropPosition = [
            312, 246,
            312, 446,
            312, 646
        ];

        this.questionWidth.push(questions.width || 340);

        optionWords.dragIndex = k;
        this.correctAnswer.push(this.jsonData.answers[k]);
        optionWords.chosenOption = this.jsonData.options[k];
        self.exerciseContainer.addChild(optionWords);
        this.dragItem.push(optionWords);

    }

    for (var i = 0; i < this.dragItem.length; i++) {
        this.onDragging(i);
    };

    //add
};


/**
 * drag drop sprite
 * on correct drop, feedback is updated
 */
Playground_2_1.prototype.onDragging = function(i) {

    var self = this;

    // use the mousedown and touchstart
    this.dragItem[i].mousedown = this.dragItem[i].touchstart = function(data) {
        if (addedListeners) return false;
        data.originalEvent.preventDefault()

        var chkindex = self.itemsDragged.indexOf(this.dragIndex);
        if (chkindex > -1) {
            self.dropArea[chkindex] = false;
            self.itemsDragged[chkindex] = -1;
        }


        // store a refference to the data
        // The reason for this is because of multitouch
        // we want to track the movement of this particular touch
        this.data = data;
        this.alpha = 0.9;
        this.dragging = true;
        this.sx = this.data.getLocalPosition(this).x;
        this.sy = this.data.getLocalPosition(this).y;
        this.bringToFront();
    }

    this.dragItem[i].mouseup = this.dragItem[i].mouseupoutside = this.dragItem[i].touchend = this.dragItem[i].touchendoutside = function(data) {
        if (addedListeners) return false;
        var dragSelf = this;

        this.dragging = false;
        // set the interaction data to null
        this.data = null;
        this.alpha = 1;

        var dropArea = [];

        dropArea[0] = isInsideRectangle(this.dx, this.dy, this.dropPosition[0], this.dropPosition[1], this.dropPosition[0] + self.questionWidth[0] + 80, this.dropPosition[1] + 180);
        dropArea[1] = isInsideRectangle(this.dx, this.dy, this.dropPosition[2], this.dropPosition[3], this.dropPosition[2] + self.questionWidth[1] + 80, this.dropPosition[3] + 180);
        dropArea[2] = isInsideRectangle(this.dx, this.dy, this.dropPosition[4], this.dropPosition[5], this.dropPosition[4] + self.questionWidth[2] + 80, this.dropPosition[5] + 180);

        /* if (self.dropArea1) dropArea1 = false;
        if (self.dropArea2) dropArea2 = false;
        if (self.dropArea3) dropArea3 = false;*/

        /* var idxCheck = self.itemsDragged.indexOf(this.dragIndex);
         if (idxCheck > -1) {
             self.dropArea[idxCheck] = false;

             self.itemsDragged[idxCheck] = -1;
         }*/



        if (dropArea[0] || dropArea[1] || dropArea[2]) {
            if (dropArea[0]) {
                if (self.dropArea[0]) {
                    this.position.x = this.oldPosition.x;
                    this.position.y = this.oldPosition.y;
                    return false;
                }
                /*this.position = {
                    x: this.dropPosition[0] + (125 - this.width) / 2 + 5,
                    y: this.dropPosition[1] + (64 - this.height) / 2 + 15
                };*/
                this.position = {
                    x: 432,
                    y: this.dropPosition[1] + (64 - this.height) / 2 + 15
                };
                self.dropArea[0] = true;
                self.itemsDragged[0] = this.dragIndex;
                self.chosenOne[0] = this.chosenOption;
            }
            if (dropArea[1]) {
                if (self.dropArea[1]) {
                    this.position.x = this.oldPosition.x;
                    this.position.y = this.oldPosition.y;
                    return false;
                }

                this.position = {
                    x: 432,
                    y: this.dropPosition[3] + (64 - this.height) / 2 + 15
                };

                self.dropArea[1] = true;
                self.itemsDragged[1] = this.dragIndex;
                // self.itemsDropIndex[1] = 1;
                self.chosenOne[1] = this.chosenOption;
            }
            if (dropArea[2]) {

                if (self.dropArea[2]) {
                    this.position.x = this.oldPosition.x;
                    this.position.y = this.oldPosition.y;
                    return false;
                }

                this.position = {
                    x: 432,
                    y: this.dropPosition[5] + (64 - this.height) / 2 + 15
                };

                self.dropArea[2] = true;
                self.itemsDragged[2] = this.dragIndex;
                //self.itemsDropIndex[2] = 2;
                self.chosenOne[2] = this.chosenOption;
            }
            //  self.itemsDragged = self.itemsDragged.removeDuplicates();
            console.clear();
            console.log("DROP AREA 0 = " + self.dropArea[0]);
            console.log("DROP AREA 1 = " + self.dropArea[1]);
            console.log("DROP AREA 2 = " + self.dropArea[2]);
            console.log("ITEMS DRAGGED = " + self.itemsDragged);
            // this.position.x = 0;
            // this.position.y = 0;
            this.sx = 0;
            this.sy = 0;
            this.dx = 0;
            this.dy = 0;
            // this.interactive = false;

            // check if all dragged correctly or not
            if (self.dropArea[0] && self.dropArea[1] && self.dropArea[2]) {

                self.dragItem[0].interactive = false;
                self.dragItem[1].interactive = false;
                self.dragItem[2].interactive = false;

                if (self.chosenOne[0] == self.correctAnswer[0] && self.chosenOne[1] == self.correctAnswer[1] && self.chosenOne[2] == self.correctAnswer[2]) { //correct answer
                    var a = setTimeout(function() {
                        correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                        for (var m = 0; m < 3; m++) {
                            self.dragItem[m].setStyle({
                                font: "55px Arial",
                                fill: "green"
                            });
                        }

                    }, 1000);

                    var b = setTimeout(function() {
                        self.correctFeedback();
                    }, 1800);

                } else {
                    var itemsToReset = [];
                    var a = setTimeout(function() {
                        wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                        for (var m = 0; m < 3; m++) {
                            self.dragItem[m].interactive = false;
                            if (self.chosenOne[m] != self.correctAnswer[m]) {
                                itemsToReset.push(self.itemsDragged[m]);
                                //  self.showRedGreenBox(self.dragItem[m].dropPosition[m * 2], self.dragItem[m].dropPosition[m * 2 + 1], self.redButton);
                                self.dragItem[self.itemsDragged[m]].setStyle({
                                    font: "55px Arial",
                                    fill: "red"
                                });
                                self.dropArea[m] = false;

                            } else {
                                self.dragItem[self.itemsDragged[m]].setStyle({
                                    font: "55px Arial",
                                    fill: "green"
                                });
                            }
                        }
                    }, 1000);

                    

                    var b = setTimeout(function() {
                        for (var n = 0; n < itemsToReset.length; n++) {
                            self.dragItem[itemsToReset[n]].interactive = true;
                            self.dragItem[itemsToReset[n]].position.x = self.dragItem[itemsToReset[n]].oldPosition.x;
                            self.dragItem[itemsToReset[n]].position.y = self.dragItem[itemsToReset[n]].oldPosition.y;
                            self.dragItem[itemsToReset[n]].setStyle({
                                font: "55px Arial",
                                fill: "black"
                            });


                            /*console.log("DROP INDEXES = " + self.itemsDropIndex);
                            console.log("ITEMS DRAGGED = " + itemsToReset[n]);*/
                            var idx = self.itemsDragged.indexOf(itemsToReset[n]);
                            if (idx > -1) {
                                self.itemsDragged[idx] = -1;
                            }
                        }

                        self.wrongFeedback();

                    }, 1800);


                }


            }

        } else {
            var idx = self.itemsDragged.indexOf(this.dragIndex);
            if (idx > -1) {
                self.itemsDragged[idx] = -1;
                self.dropArea[idx] = false;
                //self.itemsDragged = self.itemsDragged.removeDuplicates();
            }

            // reset dragged element position
            this.position.x = this.oldPosition.x;
            this.position.y = this.oldPosition.y;
        }

    }

    // set the callbacks for when the mouse or a touch moves
    this.dragItem[i].mousemove = this.dragItem[i].touchmove = function(data) {
        // set the callbacks for when the mouse or a touch moves
        if (this.dragging) {
            // need to get parent coords..
            var newPosition = this.data.getLocalPosition(this.parent);
            this.position.x = newPosition.x - this.sx;
            this.position.y = newPosition.y - this.sy;
            this.dx = newPosition.x;
            this.dy = newPosition.y;
        }
    };

}



/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
Playground_2_1.prototype.correctFeedback = function() {
    var self = this;
    var feedback = localStorage.getItem('playground_2_1');
    feedback++;

    localStorage.setItem('playground_2_1', feedback);
    this.updateFeedback();

    if (feedback >= Playground_2_1.totalFeedback) {
        setTimeout(this.onComplete.bind(this), 2000);
    } else {
        setTimeout(function() {
            self.getNewExerciseSet();
            main.eventRunning = false;
        }, 400);
    }
}

Playground_2_1.prototype.wrongFeedback = function() {
    var feedback = localStorage.getItem('playground_2_1');
    feedback--;

    if (feedback < 0)
        feedback = 0;

    localStorage.setItem('playground_2_1', feedback);
    this.updateFeedback();

}


Playground_2_1.prototype.updateFeedback = function() {

    var self = this;

    var counter = localStorage.getItem('playground_2_1');

    if (counter > 0) {
        self.feedbackClimber.setTexture(new PIXI.Texture.fromFrame("fb" + counter + ".png"));
    } else {
        self.feedbackClimber.setTexture(new PIXI.Texture.fromFrame("girl-base.png"));
    }
}


/**
 * prefetch exercise data and cache number of sets defined only.
 */
Playground_2_1.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/playground/playground_2_1.json?nocache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        self.pool = new ExerciseSpritesPool(loader.json);
        //borrow two sprites once
        self.borrowWallSprites(2);
        self.runExercise();
    });
    loader.load();
}

Playground_2_1.prototype.borrowWallSprites = function(num) {

    var self = this;

    for (var i = 0; i < num; i++) {

        var sprite = this.pool.borrowSprite(),
            spriteObj = sprite[0],
            sentenceArr = [],
            answerArr = [],
            optionArr = [];

        this.poolSlices.push(sprite);

        for (var k = 0; k < 3; k++) {
            sentenceArr.push(spriteObj.exercise_sentences[k]);
            answerArr.push(spriteObj.answers[k]);
            optionArr.push(spriteObj.options[k]);
        }

        shuffle(sentenceArr, answerArr);

        this.wallSlices.push({
            sentences: sentenceArr,
            answers: answerArr,
            options: optionArr
        });
    }
};

Playground_2_1.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];

    this.pool.returnSprite(sprite);

    this.poolSlices = [];
    this.wallSlices.shift();
};



Playground_2_1.prototype.getNewExerciseSet = function() {
    this.returnWallSprites();
    this.borrowWallSprites(1);

    for (var i = this.exerciseContainer.children.length - 1; i >= 0; i--) {
        this.exerciseContainer.removeChild(this.exerciseContainer.children[i]);
    };

    this.itemsDragged = [-1, -1, -1];
    this.dropArea = [false, false, false];

    this.runExercise();
}

Playground_2_1.prototype.restartExercise = function() {

    this.resetExercise();

    this.outroContainer.visible = false;
    this.introContainer.visible = true;

    localStorage.setItem('playground_2_1', 0);
    this.feedbackClimber.setTexture(PIXI.Texture.fromFrame("girl-base.png"));

    this.updateFeedback();

    this.currentKidIndex = 1;
    this.getNewExerciseSet();
};

/**
 * the outro scene is displayed
 */
Playground_2_1.prototype.onComplete = function() {

    var self = this;

    this.introMode = false;
    this.intro_mouth.gotoAndStop(0);
    this.help.interactive = false;
    this.help.buttonMode = false;

    this.introContainer.visible = false;
    this.outroContainer.visible = true;

    this.exerciseContainer.visible = false;

    this.feedbackClimber.visible = false;
    this.animationContainer.visible = true;

    this.showHandAt(0);

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        self.cancelOutro();
        self.restartExercise();
    }

    this.outroAnimation();

}

Playground_2_1.prototype.resetExercise = function() {
    this.ovaigen.visible = false;
    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    this.exerciseContainer.visible = true;
    this.feedbackClimber.visible = true;
    this.introMode = true;
};

/**
 * clean memory by clearing reference of object
 */
Playground_2_1.prototype.cleanMemory = function() {


    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    if (this.assetsToLoads)
        this.assetsToLoad.length = 0;


    this.help = null;
    this.ovaigen = null;


    this.introContainer = null;
    this.outroContainer = null;
    this.exerciseContainer = null;
    this.animationContainer = null;
    this.cancelHandler = null;

    this.itemsDragged.length = 0;
    this.dropArea.length = 0;
    this.feedbackClimber = null;
    this.EyesShow = null;

    this.whiteTextBackground = null;
    this.whiteTextRect = null;
    this.chosenOne = null;
    if (this.correctAnswer) this.correctAnswer.length = 0;
    if (this.dragItem) this.dragItem.length = 0;

    this.introPerson = null;
    this.intro_mouth = null;

    this.common = null;


    this.introId = null;
    this.outroId = null;


    for (var i = this.sceneContainer.children.length - 1; i >= 0; i--) {
        this.sceneContainer.removeChild(this.sceneContainer.children[i]);
    };
    this.sceneContainer = null;
}