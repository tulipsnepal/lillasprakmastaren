function Playground_1_3() {
    cl.show()

    PIXI.DisplayObjectContainer.call(this);

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position.set(77, 88);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();
    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.helpContainer = new PIXI.DisplayObjectContainer();
    main.CurrentExercise = "playground_1_3";

    main.eventRunning = false;

    main.texturesToDestroy = [];

    this.common = new Common();

    this.fromOutro = 0;

    this.dragBrush = [];
    this.spriteFrame = [];
    this.circleFrame = [];
    this.circleTextFrame = [];

    this.getGameJsonData();

    this.loadGameJson();

    // everything connected to this is rendered.
    this.addChild(this.sceneContainer);
}

Playground_1_3.constructor = Playground_1_3;
Playground_1_3.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Playground_1_3.imagePath = 'build/exercises/playground/playground_1_3/images/';
Playground_1_3.audioPath = 'build/exercises/playground/playground_1_3/audios/';


/**
 * load array of assets of different format
 */
Playground_1_3.prototype.loadSpriteSheet = function() {

    var self = this;

    this.introId = loadSound(Playground_1_3.audioPath + 'intro');
    this.outroId = loadSound(Playground_1_3.audioPath + 'outro');

    // create an array of assets to load
    this.assetsToLoad = [

        Playground_1_3.imagePath + "sprite_playground_1_3.json",
        Playground_1_3.imagePath + "sprite_playground_1_3.png",
        Playground_1_3.imagePath + "sand_pit.png",
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

Playground_1_3.prototype.loadGameJson = function() {

    // this.random = makeUniqueRandom(5) + 1;
    this.random = makeUniqueRandom(5) + 1;

    this.outlineObj = this.motiveJson[this.random];

    this.jsonToLoad = this.outlineObj.sprite;
    this.jsonToLoad.push(Playground_1_3.imagePath + 'motive_' + this.random + '/outline.png');
    // create a new loader
    loader = new PIXI.AssetLoader(this.jsonToLoad);
    // use callback
    if (this.resetMode) {
        loader.onComplete = this.initGame.bind(this);
        this.resetMode = false;
    } else {
        loader.onComplete = this.loadSpriteSheet.bind(this);
    }
    //begin load
    loader.load();

}

Playground_1_3.prototype.getGameJsonData = function() {

    this.motiveJson = {
        1: {
            sprite: [
                Playground_1_3.imagePath + 'motive_1/water2.png',
                Playground_1_3.imagePath + 'motive_1/upperbody6.png',
                Playground_1_3.imagePath + 'motive_1/head4.png',
                Playground_1_3.imagePath + 'motive_1/bigstone6.png',
                Playground_1_3.imagePath + 'motive_1/eye5.png',
                Playground_1_3.imagePath + 'motive_1/middlebody5.png',
                Playground_1_3.imagePath + 'motive_1/lowerbody6.png',
                Playground_1_3.imagePath + 'motive_1/lowerfin4.png',
                Playground_1_3.imagePath + 'motive_1/lowergrass3.png',
                Playground_1_3.imagePath + 'motive_1/tail1.png',
                Playground_1_3.imagePath + 'motive_1/topgrass3.png',
                Playground_1_3.imagePath + 'motive_1/middlestone6.png',
                Playground_1_3.imagePath + 'motive_1/smallstone6.png',
            ],
            position: [
                0, 3,
                821, 270,
                1234, 290,
                1391, 578,
                1429, 406,
                652, 276,
                790, 457,
                840, 595,
                368, 579,
                361, 75,
                143, 249,
                167, 616,
                299, 757,
            ],
            hitText: [
                2, 6, 4, 6, 5, 5, 6, 4, 3, 1, 3, 6, 6
            ],
            hitPosition: [
                1095, 135,
                1140, 290,
                1319, 405,
                1515, 734,
                1445, 428,
                1097, 411,
                1121, 552,
                927, 617,
                519, 761,
                611, 220,
                231, 391,
                239, 680,
                321, 803,
            ]
        },
        2: {
            sprite: [
                Playground_1_3.imagePath + 'motive_2/middle_idol1.png',
                Playground_1_3.imagePath + 'motive_2/right_idol2.png',
                Playground_1_3.imagePath + 'motive_2/left_idol3.png',
                Playground_1_3.imagePath + 'motive_2/board4.png',
                Playground_1_3.imagePath + 'motive_2/half_pant5.png',
                Playground_1_3.imagePath + 'motive_2/hat6.png',
            ],
            position: [
                573, 161,
                1240, 47, -2, 107,
                2, 6,
                1261, 545,
                810, 5,

            ],
            hitText: [1, 2, 3, 4, 5, 6],
            hitPosition: [
                879, 548,
                1446, 347,
                275, 612,
                611, 35,
                1475, 619,
                1024, 81,

            ]
        },
        3: {
            sprite: [

                Playground_1_3.imagePath + 'motive_3/lemon1.png',
                Playground_1_3.imagePath + 'motive_3/outer_plate2.png',
                Playground_1_3.imagePath + 'motive_3/grapes3.png',
                Playground_1_3.imagePath + 'motive_3/board3.png',
                Playground_1_3.imagePath + 'motive_3/apple4.png',
                Playground_1_3.imagePath + 'motive_3/inner_plate5.png',
                Playground_1_3.imagePath + 'motive_3/scissor6.png',
            ],
            position: [
                239, 261,
                159, 122,
                1004, 426,
                0, 3,
                635, 29,
                254, 156,
                948, 90,
            ],
            hitText: [1, 2, 3, 3, 4, 5, 6],
            hitPosition: [
                444, 419,
                1617, 427,
                1151, 561,
                1500, 75,
                846, 213,
                746, 615,
                1112, 414,
            ],
        },
        4: {
            sprite: [
                Playground_1_3.imagePath + 'motive_4/background1.png',
                Playground_1_3.imagePath + 'motive_4/thirdinner_circle3.png',
                Playground_1_3.imagePath + 'motive_4/secondinner_circle4.png',
                Playground_1_3.imagePath + 'motive_4/seconddart4.png',
                Playground_1_3.imagePath + 'motive_4/fourthinner_circle5.png',
                Playground_1_3.imagePath + 'motive_4/inner_circle6.png',
                Playground_1_3.imagePath + 'motive_4/outer_circle6.png',
                Playground_1_3.imagePath + 'motive_4/dart2.png',
            ],
            position: [
                2, 6,
                386, 149,
                468, 234,
                1339, 522,
                260, 39,
                530, 305,
                160, 2,
                770, 10,
            ],
            hitText: [1, 3, 4, 4, 5, 6, 6, 2],
            hitPosition: [
                1459, 47,
                760, 644,
                538, 250,
                1700, 673,
                395, 77,
                599, 390,
                1008, 328,
                1108, 90,
            ],
        },
        5: {
            sprite: [

                Playground_1_3.imagePath + 'motive_5/board1.png',
                Playground_1_3.imagePath + 'motive_5/paper5.png',
                Playground_1_3.imagePath + 'motive_5/art2.png',
                Playground_1_3.imagePath + 'motive_5/shirt3.png',
                Playground_1_3.imagePath + 'motive_5/hair4.png',
                Playground_1_3.imagePath + 'motive_5/face5.png',
                Playground_1_3.imagePath + 'motive_5/desk6.png',
            ],
            position: [-2, 0,
                226, 579,
                488, 633,
                300, 288,
                584, 22,
                603, 126, -1, 587,
            ],
            hitText: [1, 5, 2, 3, 4, 5, 6],
            hitPosition: [
                1476, 93,
                301, 696,
                665, 674,
                1127, 509,
                848, 75,
                744, 99,
                1386, 636,
            ]
        },
    }

}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
Playground_1_3.prototype.spriteSheetLoaded = function() {

    // hide loader
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    // fill exercise background
    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0xB9D3F0);
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xCAAABB);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);

    // exercise instruction
    this.help = PIXI.Sprite.fromFrame("help.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);
    this.helpContainer.addChild(this.help);

    this.loadExercise();

    this.animationContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.addChild(this.animationContainer);

    this.introPerson = new PIXI.Sprite.fromFrame('introgirl.png');
    this.introPerson.position.set(897, 32);
    main.texturesToDestroy.push(this.introPerson);
    this.animationContainer.addChild(this.introPerson);

    this.intro_mouth = new Animation({
        "image": "mouth",
        "length": 5,
        "x": 1234,
        "y": 574,
        "speed": 5,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 5, 3, 2, 4, 1, 5],
        "frameId": 2
    });

    this.animationContainer.addChild(this.intro_mouth);

    this.animationContainer.interactive = true;
    this.animationContainer.buttonMode = true;

    this.animationContainer.visible = false;

    this.animationContainer.click = this.animationContainer.tap = this.cancelIntro.bind(this);

    // back to intro page
    this.ovaigen = PIXI.Sprite.fromFrame('ova_igen.png');
    this.ovaigen.position.set(1417, 702);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.ovaigen.visible = false;
    this.sceneContainer.addChild(this.ovaigen);

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.outroContainer);
    this.introContainer.addChild(this.exerciseContainer);
    this.sceneContainer.addChild(this.helpContainer);
}

/**
 * play intro animation
 */
Playground_1_3.prototype.introAnimation = function() {

    createjs.Sound.stop();
    main.overlay.visible = true;

    if (this.fromOutro == 1) {
        this.fromOutro = 0;
        this.resetExercise();
    }

    this.help.interactive = false;
    this.help.buttonMode = false;

    this.animationContainer.visible = true;
    this.intro_mouth.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * cancel running intro animation
 */
Playground_1_3.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);

    this.intro_mouth.hide();
    this.animationContainer.visible = false;
    this.help.interactive = true;
    this.help.buttonMode = true;

    this.ovaigen.visible = false;
    main.overlay.visible = false;
}

Playground_1_3.prototype.outroAnimation = function() {

    this.animationContainer.visible = true;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        this.intro_mouth.show();
    }
    this.cancelHandlerOutro = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.cancelHandlerOutro);

}

Playground_1_3.prototype.cancelOutro = function() {
    createjs.Sound.stop();
    this.intro_mouth.hide();
    this.animationContainer.visible = false;
    this.ovaigen.visible = true;
    this.help.interactive = true;
    this.help.buttonMode = true;
    main.overlay.visible = false;

}

/**
 * load exercise related stuffs
 */
Playground_1_3.prototype.loadExercise = function() {

    //ADD BRUSHES
    var colorNumbers = ["ett", "två", "tre", "fyra", "fem", "sex"],
        colorHex = ["0xfff200", "0x0f75bb", "0x39b54a", "0xed1c24", "0xfffffe", "0x6d6e71"];

    this.palleteTexture = new PIXI.Texture.fromFrame('whitepallette.png');

    for (var k = 1; k <= 6; k++) {

        var brush = PIXI.Sprite.fromFrame("brush" + k + ".png"),
            pallete = new PIXI.Sprite(this.palleteTexture);

        brush.position = {
            x: 203 + ((k - 1) * 281)+40,
            y: 963+30
        };

        brush.anchor.x = 0.5;
        brush.anchor.y = 0.5;
        brush.scale.x = brush.scale.y = 1.2;

        pallete.position.x = brush.position.x-90;
        pallete.position.y = brush.position.y + 100-30;


        pallete.tint = colorHex[k - 1];

        var colorLabel = new PIXI.Text("" + colorNumbers[k - 1], {
            font: "55px Arial"
        });

        colorLabel.position = {
            x: 203 + ((k - 1) * 281) - colorLabel.width - 10 - 60,
            y: 1074
        };

        this.introContainer.addChild(colorLabel);
        brush.interactive = true;
        brush.buttonMode = true;
        brush.colorIndex = k;
        brush.colorHex = colorHex[k - 1];

        this.introContainer.addChild(pallete);
        this.introContainer.addChild(brush);

        main.texturesToDestroy.push(brush);
        this.dragBrush.push(brush);
    }

    for (var i = 0; i < this.dragBrush.length; i++) {
        this.onDragging(i);
    };

    this.circleGraphics = createCircle({
        radius: 68 / 2,
    });

    this.circleTexture = this.circleGraphics.generateTexture();

    var sandTray = new PIXI.Sprite.fromImage(Playground_1_3.imagePath + 'sand_pit.png');
    sandTray.position.set(2, 2);
    main.texturesToDestroy.push(sandTray);
    this.sceneContainer.addChild(sandTray);

    this.outlineContainer = new PIXI.DisplayObjectContainer();
    this.outlineContainer.position.set(4, 29);
    this.sceneContainer.addChild(this.outlineContainer);

    this.initGame();

}

Playground_1_3.prototype.initGame = function() {

    this.dropIndex = null;
    this.dropTotal = 0;

    //if the motive is dart scene (motive 4) then position of dart and dart board is not looking natural. So scene other than dart, draw outline at the beginning, otherwise at the last
    if (this.random != 4) {
        var motiveOutline = new PIXI.Sprite.fromImage(Playground_1_3.imagePath + 'motive_' + this.random + '/outline.png');
        this.outlineContainer.addChild(motiveOutline);
    }

    this.outlineObj.sprite.pop();

    for (var i = 0; i < this.outlineObj.sprite.length; i++) {

        var outlineSprite = new PIXI.Sprite.fromImage(this.outlineObj.sprite[i]);
        outlineSprite.position.x = this.outlineObj.position[i * 2];
        outlineSprite.position.y = this.outlineObj.position[i * 2 + 1];

        outlineSprite.tint = '0x764c28';

        this.spriteFrame.push(outlineSprite);
        this.outlineContainer.addChild(outlineSprite);

        var circleHitArea = new PIXI.Sprite(this.circleTexture);
        circleHitArea.position.x = this.outlineObj.hitPosition[i * 2];
        circleHitArea.position.y = this.outlineObj.hitPosition[i * 2 + 1];

        circleHitArea.alpha = 0.6;
        this.circleFrame.push(circleHitArea);
        this.outlineContainer.addChild(circleHitArea);

        var circleText = new PIXI.Text(this.outlineObj.hitText[i], {
            font: '32px Arial'
        })

        circleText.position.x = circleHitArea.position.x + 26;
        circleText.position.y = circleHitArea.position.y + 22;
        this.outlineContainer.addChild(circleText);
        this.circleTextFrame.push(circleText);

    };

    //if the motive is dart scene (motive 4) then position of dart and dart board is not looking natural. So dart scene should have outline positioned at last

    if (this.random == 4) {
        var motiveOutline = new PIXI.Sprite.fromImage(Playground_1_3.imagePath + 'motive_' + this.random + '/outline.png');
        this.outlineContainer.addChild(motiveOutline);
    }



};

/**
 * drag drop sprite
 * on correct drop, feedback is updated
 */
Playground_1_3.prototype.onDragging = function(i) {

    var self = this;


    // use the sprite1down and touchstart
    this.dragBrush[i].mousedown = this.dragBrush[i].touchstart = function(data) {
        if (addedListeners) return false;
        data.originalEvent.preventDefault()

        // store a refference to the data
        // The reason for this is because of multitouch
        // we want to track the movement of this particular touch
        this.data = data;
        this.alpha = 0.9;
        this.dragging = true;
        this.sx = this.data.getLocalPosition(this).x;
        this.sy = this.data.getLocalPosition(this).y;
        this.bringToFront();

    }

    this.dragBrush[i].mouseup = this.dragBrush[i].mouseupoutside = this.dragBrush[i].touchend = this.dragBrush[i].touchendoutside = function(data) {
        if (addedListeners) return false;
        var dragSelf = this;

        this.dragging = false;
        // set the interaction data to null
        this.data = null;
        this.alpha = 1;

        self.isDropArea(this.dx, this.dy, this.colorIndex);
        /*console.log("SELF DROPINDEX = " + self.dropIndex);
        console.log("COLOR INDEX = " + this.colorIndex);
        console.log("OUTLINE DROP INDEX = " + self.outlineObj.hitText[self.dropIndex]);*/
        if ((typeof(self.dropIndex) !== 'undefined') && (self.dropIndex !== null)) {
            if (this.colorIndex === self.outlineObj.hitText[self.dropIndex]) {
                self.spriteFrame[self.dropIndex].tint = this.colorHex;
                self.circleFrame[self.dropIndex].visible = false;
                self.circleTextFrame[self.dropIndex].visible = false;
                self.dropTotal++;
                if (self.dropTotal >= self.spriteFrame.length) {
                    var b = setTimeout(function() {
                        self.onComplete();
                    }, 2000);
                };
            };
        }
        this.position.x = 203 + ((this.colorIndex - 1) * 281)+40;
        this.position.y = 963+30;
    }

    // set the callbacks for when the sprite1 or a touch moves
    this.dragBrush[i].mousemove = this.dragBrush[i].touchmove = function(data) {
        // set the callbacks for when the sprite1 or a touch moves
        if (this.dragging) {
            // need to get parent coords..
            var newPosition = this.data.getLocalPosition(this.parent);
            this.position.x = newPosition.x - this.sx;
            this.position.y = newPosition.y - this.sy;
            this.dx = newPosition.x;
            this.dy = newPosition.y;

        }
    };

}


/**
 * check drag position is inside rectangular drop position
 * @param  {float} dx    drag positon x
 * @param  {float} dy    drag position y
 * @param  {integer} index
 * @return {boolean}       true/false
 */
Playground_1_3.prototype.isDropArea = function(dx, dy, colorIndex) {

    this.dropIndex = null;

    for (var i = 0; i < this.outlineObj.sprite.length; i++) {
        /*var x1 = this.outlineObj.hitPosition[i * 2] - 20,
            y1 = this.outlineObj.hitPosition[i * 2 + 1] - 20,
            x2 = this.outlineObj.hitPosition[i * 2] + 150 - 20,
            y2 = this.outlineObj.hitPosition[i * 2 + 1] + 150 - 20;*/

        var x1 = this.outlineObj.hitPosition[i * 2] - 20 - 30,
            y1 = this.outlineObj.hitPosition[i * 2 + 1] - 20 - 30,
            x2 = this.outlineObj.hitPosition[i * 2] + 150 - 20 + 30,
            y2 = this.outlineObj.hitPosition[i * 2 + 1] + 150 - 20 + 30;

        if (isInsideRectangle(dx, dy, x1, y1, x2, y2)) {
            this.dropIndex = i;
        }

    };


}

/**
 * the outro scene is displayed
 */
Playground_1_3.prototype.onComplete = function() {

    var self = this;

    this.introMode = false;

    this.help.interactive = false;
    this.help.buttonMode = false;
    this.fromOutro = 1;

    for (var i = 0; i < this.dragBrush.length; i++) {
        this.dragBrush[i].interactive = false;
        this.dragBrush[i].buttonMode = false;
    };

    this.resetMode = true;

    this.ovaigen.click = this.ovaigen.tap = function(data) {

        self.fromOutro = 0;
        self.cancelOutro();
        self.resetExercise();
    }

    this.outroAnimation();

}

Playground_1_3.prototype.resetExercise = function() {

    this.ovaigen.visible = false;

    this.animationContainer.visible = false;

    for (var i = 0; i < this.outlineObj.sprite.length; i++) {
        this.spriteFrame[i].tint = '0x764c28';
        this.circleFrame[i].visible = true;
        this.circleTextFrame[i].visible = true;
    }

    this.dropTotal = 0;

    for (var i = 0; i < this.dragBrush.length; i++) {
        this.dragBrush[i].interactive = true;
        this.dragBrush[i].buttonMode = true;
    };

    if (this.resetMode) {

        for (var i = this.outlineContainer.children.length - 1; i >= 0; i--) {
            this.outlineContainer.removeChild(this.outlineContainer.children[i]);
        };

        this.spriteFrame.length = 0;
        this.circleFrame.length = 0;
        this.circleTextFrame.length = 0;
        this.loadGameJson();
    }

    this.introMode = true;

};



/**
 * clean memory by clearing reference of object
 */
Playground_1_3.prototype.cleanMemory = function() {


    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    if (this.assetsToLoads)
        this.assetsToLoad.length = 0;

    this.help = null;



    this.fromOutro = null;

    this.ovaigen = null;

    for (var i = this.sceneContainer.children.length - 1; i >= 0; i--) {
        this.sceneContainer.removeChild(this.sceneContainer.children[i]);
    };
    this.sceneContainer = null;
}
