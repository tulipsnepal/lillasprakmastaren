function Playground() {
    cl.show();

    PIXI.DisplayObjectContainer.call(this);

    this.tivoliContainer = new PIXI.DisplayObjectContainer();

    this.sceneContainer = new PIXI.DisplayObjectContainer();

    this.imageContainer = new PIXI.DisplayObjectContainer();

    this.menuContainer = new PIXI.DisplayObjectContainer();

    this.tivoliContainer.addChild(this.sceneContainer);
    this.tivoliContainer.addChild(this.menuContainer);

    //resize container
    this.imageContainer.position = {
        x: 0,
        y: -20
    }

    this.menuContainer.position = {
        x: 0,
        y: 1287 - 36
    }

    this.loadSpriteSheet();

    this.addChild(this.tivoliContainer);
}

Playground.constructor = Playground;
Playground.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

Playground.IMAGEPATH = "build/exercises/playground/playground_0/images/";
Playground.COMMONFOLDER = 'build/common/images/playground/';

Playground.prototype.loadSpriteSheet = function() {

    // create an array of assets to load
    var assetsToLoad = [
        Playground.IMAGEPATH + "base.png",
        Playground.COMMONFOLDER + 'menuskeleton.png',
        Playground.COMMONFOLDER + 'sprite_playground.json',
        Playground.COMMONFOLDER + 'sprite_playground.png',
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(assetsToLoad);

    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);

    //begin load
    loader.load();

}

Playground.prototype.spriteSheetLoaded = function() {

    cl.hide();

    // get backgroudn for tivoli page
    this.bkg1 = new Background(Playground.IMAGEPATH + "base.png");
    this.sceneContainer.addChild(this.bkg1);

    this.loadMenus();

    this.imageButtons();

}


Playground.prototype.homePage = function() {
    UnloadScene.apply(this);
    localStorage.removeItem('exercise');
    // createjs.Sound.removeAllSounds();
    history.back();
}

Playground.prototype.sceneChange = function(scene) {

    var prevExercise = localStorage.getItem('exercise');

    if (scene === prevExercise)
        return false;

    UnloadScene.apply(this);

    main.CurrentExercise = scene;

    localStorage.setItem('exercise', scene);

    main.page = new window[scene];
    main.page.introMode = true;

    loadSound('build/common/audios/' + main.CurrentExercise + "_help");

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0x000000);

    this.sceneContainer.addChild(main.page);

    this.cleanMemory();

};

Playground.prototype.loadMenus = function() {

    // position of 1 and 2 sub menu
    var x1 = 68,
        x2 = 191,
        x3 = 314,
        y = 0;

    this.menuParams = {
        path: Playground.COMMONFOLDER,
        background: 'menuskeleton.png',
        images: [
            'tillbaka.png',
            'farger.png',
            'jag_kan.png',
            'sortera.png',
            'kroppen.png'
        ],
        positions: [
            10, 129,
            246, 116,
            664, 103,
            1036, 67,
            1369, 107
        ],
        data: {
            1: {
                hover_image: 'menu_selected.png',
                hover_position: [255, -23],
                submenu_image: [
                    'sub_menu_1.png',
                    'sub_menu_2.png',
                    'sub_menu_3.png',
                ],
                submenu_position: [
                    200 + x1, y,
                    200 + x2, y,
                    200 + x3, y
                ]

            },
            2: {
                hover_image: 'menu_selected.png',
                hover_position: [645, -23],
                submenu_image: [
                    'sub_menu_1.png',
                    'sub_menu_2.png',
                ],
                submenu_position: [
                    630 + x1, y,
                    630 + x2 + 40, y
                ]
            },
            3: {
                hover_image: 'menu_selected.png',
                hover_position: [1035, -23],
                submenu_image: [
                    'sub_menu_1.png',
                    'sub_menu_2.png',
                ],
                submenu_position: [
                    1020 + x1, y,
                    1020 + x2 + 40, y
                ]
            },
            4: {
                hover_image: 'menu_selected.png',
                hover_position: [1425, -23],
                submenu_image: [
                    'sub_menu_1.png',
                    'sub_menu_2.png',
                ],
                submenu_position: [
                    1410 + x1, y,
                    1410 + x2 + 40, y
                ]
            },
            pink_image: 'menu_circle.png',
            move_position: {
                x: 10,
                y: 32
            },
            chop: { // optional
                images: [
                    'farger.png',
                    'jag_kan.png',
                    'sortera.png',
                    'kroppen.png'
                ],
                positions: [
                    246, 116,
                    664, 103,
                    1036, 67,
                    1369, 107
                ]
            }
        }

    };
    this.menu = new Menu(this.menuParams);

    this.menuContainer.addChild(this.menu);

    //tillbaka button
    this.menu.getChildAt(1).click = this.menu.getChildAt(1).tap = this.homePage.bind(this);

    //bokstavernai alfabet button
    this.menu.getChildAt(2).click = this.menu.getChildAt(2).tap = this.navMenu1.bind(this);

    //A till P
    this.menu.getChildAt(3).click = this.menu.getChildAt(3).tap = this.navMenu2.bind(this);

    //Q till Ö
    this.menu.getChildAt(4).click = this.menu.getChildAt(4).tap = this.navMenu3.bind(this);

    //Hela alfabetet
    this.menu.getChildAt(5).click = this.menu.getChildAt(5).tap = this.navMenu4.bind(this);


}

Playground.prototype.imageButtons = function() {

    this.navPositions = [
        288, 1000,
        1640, 235,
        1426, 900,
        728, 709
    ];

    this.navButtons = [];

    for (var i = 0; i < this.navPositions.length; i++) {
        var imageSprite = PIXI.Sprite.fromFrame('ova.png');

        imageSprite.buttonMode = true;
        imageSprite.position.x = this.navPositions[i * 2];
        imageSprite.position.y = this.navPositions[i * 2 + 1];
        imageSprite.interactive = true;

        this.imageContainer.addChild(imageSprite);

        this.navButtons.push(imageSprite);
        imageSprite = null;
    }

    //Hela alfabetet image button
    this.navButtons[3].click = this.navButtons[3].tap = this.navMenu4.bind(this);

    //A till P image button
    this.navButtons[1].click = this.navButtons[1].tap = this.navMenu2.bind(this);

    //Bokstäverna i alfabetet image button
    this.navButtons[0].click = this.navButtons[0].tap = this.navMenu1.bind(this);

    //Q till Ö  image button
    this.navButtons[2].click = this.navButtons[2].tap = this.navMenu3.bind(this);



    this.sceneContainer.addChild(this.imageContainer);

}

Playground.prototype.navMenu = function(submenu1, submenu2, submenu3, index) {

    this.sceneChange(submenu1);

    var self = this;

    this.pushStuffToHistory("history_other_Playground");

    this.subMenu = self.menu.getActiveMenu(index);
    self.menu.moveSelectedExerciseCircle(this.subMenu[0].position);
    self.sceneContainer.addChild(self.menuContainer);

    this.subMenu[0].click = this.subMenu[0].tap = function(data) {
        self.sceneChange(submenu1);

        self.menu.moveSelectedExerciseCircle(this.position);
        self.sceneContainer.addChild(self.menuContainer);

    }
    this.subMenu[1].click = this.subMenu[1].tap = function(data) {
        self.sceneChange(submenu2);

        self.menu.moveSelectedExerciseCircle(this.position);
        self.sceneContainer.addChild(self.menuContainer);
    }

    if (submenu3 != "") {
        this.subMenu[2].click = this.subMenu[2].tap = function(data) {
            self.sceneChange(submenu3);

            self.menu.moveSelectedExerciseCircle(this.position);
            self.sceneContainer.addChild(self.menuContainer);
        }
    }

}

Playground.prototype.navMenu1 = function() {
    this.navMenu('Playground_1_1', 'Playground_1_2', 'Playground_1_3', 1);
}


Playground.prototype.navMenu2 = function() {
    this.navMenu('Playground_2_1', 'Playground_2_2', '', 2);
}

Playground.prototype.navMenu3 = function() {
    this.navMenu('Playground_3_1', 'Playground_3_2', '', 3);
}

Playground.prototype.navMenu4 = function() {
    this.navMenu('Playground_4_1', 'Playground_4_2', '', 4);
}


Playground.prototype.pushStuffToHistory = function(key) {

    if (Device.ieVersion != 9) {
        var exHistory = new History();
        exHistory.PushState({
            scene: key
        });
        exHistory = null;
    }
}

Playground.prototype.cleanMemory = function() {
    main.eventRunning = false;
}