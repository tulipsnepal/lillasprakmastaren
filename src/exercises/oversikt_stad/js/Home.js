function Home() {

    PIXI.DisplayObjectContainer.call(this);

    this.assetFolder = "build/exercises/oversikt_stad/images/";
    // create an empty container to store home stuffs
    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.homeContainer = new PIXI.DisplayObjectContainer();
    // create an empty container for menu
    this.menuContainer = new PIXI.DisplayObjectContainer();

    this.commonMenuContainer = new PIXI.DisplayObjectContainer();
    /*this.commonMenuContainer.anchor = {
        x: 0.5,
        y: 0.5
    };*/

    //resize container
    this.menuContainer.position = this.commonMenuContainer.position = {
        x: 0,
        y: -30
    }

    main.texturesToDestroy = [];

    this.spriteSheetLoaded();
    this.sceneContainer.addChild(this.homeContainer);
    this.addChild(this.sceneContainer);

}

Home.constructor = Home;
Home.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

Home.prototype.spriteSheetLoaded = function() {

    cl.hide();
    // get home page background
    this.sceneBackground = new PIXI.Sprite.fromImage(this.assetFolder + "overviewcity-40pxcut.png");
    main.texturesToDestroy.push(this.sceneBackground);
    this.homeContainer.addChild(this.sceneBackground);

    this.animateSprite();

    this.menuBackground = new PIXI.Sprite.fromImage(this.assetFolder + "menu_back.png");
    this.menuBackground.position = {
        x: 0,
        y: 1290
    };
    main.texturesToDestroy.push(this.menuBackground);
    this.commonMenuContainer.addChild(this.menuBackground);

    this.homeContainer.addChild(this.menuContainer);

    this.imageButtons();

    this.homeMenu();

}

Home.prototype.animateSprite = function() {

    this.animateContainer = new PIXI.DisplayObjectContainer();
    this.animateContainer.position.x = 0;
    this.animateContainer.position.y = 0;
    this.homeContainer.addChild(this.animateContainer);

    this.animate = {
        'bio': {
            image: "bio_",
            length: 2,
            x: 1780,
            y: 761
        },
        'bensin': {
            image: "bensin_",
            length: 7,
            x: 123,
            y: 980
        },
        'flag': {
            image: "flag_",
            length: 2,
            x: 922,
            y: 370
        },
        'store': {
            image: "store_",
            length: 2,
            x: 1041,
            y: 841
        },
    }

    this.bio = new Animation(this.animate.bio);
    this.store = new Animation(this.animate.store);
    this.bensin = new Animation(this.animate.bensin);
    this.flag = new Animation(this.animate.flag);

    this.bio.show();
    this.store.show();
    this.bensin.show();
    this.flag.show();

    this.animateContainer.addChild(this.bio);
    this.animateContainer.addChild(this.store);
    this.animateContainer.addChild(this.bensin);
    this.animateContainer.addChild(this.flag);

}

/**
 * City Overview screen image buttons
 */
Home.prototype.imageButtons = function() {

    var self = this;

    this.imageButtons = [];

    this.imageButtonsName = [
        'IKON_TIVOLI.png',
        'FOOTBALL.png',
        'IKON_SKOLAN.png',
        'PLAYPARK.png',
        'TOYSTORE.png',
        'HOUSE.png',
    ];

    this.imageButtonsPosition = [
        1556, 93,
        6, 816 - 50,
        121, 486,
        446, 404,
        1502, 685,
        699, 760,
    ];

    for (var i = 0; i < this.imageButtonsName.length; i++) {
        var imageSprite = PIXI.Sprite.fromFrame(this.imageButtonsName[i]);

        imageSprite.buttonMode = true;
        imageSprite.position.x = this.imageButtonsPosition[i * 2];
        imageSprite.position.y = this.imageButtonsPosition[i * 2 + 1];
        imageSprite.interactive = true;
        imageSprite.hitIndex = i;
        // add sprite to container
        main.texturesToDestroy.push(imageSprite);
        this.menuContainer.addChild(imageSprite);

        this.imageButtons.push(imageSprite);
    }

    this.imageButtons[0].click = this.imageButtons[0].tap = this.imageButtons[1].click = this.imageButtons[1].tap = this.imageButtons[2].click = this.imageButtons[2].tap = this.imageButtons[3].click = this.imageButtons[3].tap = this.imageButtons[4].click = this.imageButtons[4].tap = this.imageButtons[5].click = this.imageButtons[5].tap = function(data) {
        createjs.Sound.stop();
        var index = this.hitIndex;

        if (index === 0) {
            self.pushToHistory("history_Tivoli");
            self.sceneChange('Tivoli');
        } else if (index === 2) {
            self.pushToHistory("history_School");
            self.sceneChange('School');
        } else if (index === 3) {
            self.pushToHistory("history_Playground");
            self.sceneChange('Playground');
        } else if (index === 4) {
            self.pushToHistory("history_Toystore");
            self.sceneChange('Toystore');
        } else if (index === 1) {
            self.pushToHistory("history_Football");
            self.sceneChange('Football');
        } else if (index === 5) {
            self.pushToHistory("history_House");
            self.sceneChange('House');
        }
    }
}

Home.prototype.homeMenu = function() {

    var self = this;

    // create an empty container for nav for clicked menu
    this.navContainer = new PIXI.DisplayObjectContainer();

    this.menuButtons = [];

    this.navButtons = [];

    this.navMenuButtons = [];

    this.ovaButtons = [];

    this.menuImages = [
        'leksaksaffaren_btn_withtext.png',
        'hoghuset_btn_withtext.png',
        'fotballsplanen_btn_withtext.png',
        'tivolit_btn_withtext.png',
        'lekparken_btn_withtext.png',
        'skolan_btn_withtext.png',
    ];

    this.menuPositions = [
        212, 1413,
        564, 1412,
        800, 1413,
        1129, 1412,
        1298, 1412,
        1553, 1412,
    ];

    this.navImages = [
        'leksaksaffaren_popup.png',
        'hoghuset_popup.png',
        'fotballsplanen_popup.png',
        'tivolit_popup.png',
        'lekparken_popup.png',
        'skolan_popup.png',
    ];

    this.menuSound = [
        loadSound("uploads/audios/menu_sounds/menu_toystore"),
        loadSound("uploads/audios/menu_sounds/menu_house"),
        loadSound("uploads/audios/menu_sounds/menu_football"),
        loadSound("uploads/audios/menu_sounds/menu_tivoli"),
        loadSound("uploads/audios/menu_sounds/menu_playground"),
        loadSound("uploads/audios/menu_sounds/menu_school")
    ];

    this.navPositions = [
        164, 1093,
        472, 1090,
        771, 1090,
        974, 1087,
        1173, 1087,
        1367, 1089,
    ];

    this.ovaPositions = [
        385, 1302,
        698, 1302,
        998, 1302,
        1195, 1302,
        1406, 1302,
        1540, 1302,
    ];

    for (var i = 0; i < self.menuImages.length; i++) {
        var menu = PIXI.Sprite.fromFrame(self.menuImages[i]);

        menu.buttonMode = true;
        menu.position.x = self.menuPositions[i * 2];
        menu.position.y = self.menuPositions[i * 2 + 1];
        menu.interactive = true;
        menu.hitIndex = i;
        main.texturesToDestroy.push(menu);
        self.commonMenuContainer.addChild(menu);
        menu.soundMenu = self.menuSound[i];
        self.menuButtons.push(menu);

        var nav = PIXI.Sprite.fromFrame(self.menuImages[i]);

        nav.buttonMode = true;
        nav.position.x = self.menuPositions[i * 2];
        nav.position.y = self.menuPositions[i * 2 + 1];
        nav.interactive = true;

        main.texturesToDestroy.push(nav);
        self.navButtons.push(nav);

        var navMenuImage = PIXI.Sprite.fromFrame(self.navImages[i]);

        navMenuImage.buttonMode = true;
        navMenuImage.position.x = self.navPositions[i * 2];
        navMenuImage.position.y = self.navPositions[i * 2 + 1];
        navMenuImage.interactive = true;
        main.texturesToDestroy.push(navMenuImage);
        self.navMenuButtons.push(navMenuImage);

        var ova_btn = PIXI.Sprite.fromFrame("ova_btn.png");
        ova_btn.buttonMode = true;
        ova_btn.position.x = self.ovaPositions[i * 2];
        ova_btn.position.y = self.ovaPositions[i * 2 + 1];
        ova_btn.interactive = true;
        main.texturesToDestroy.push(ova_btn);
        self.ovaButtons.push(ova_btn);
    }

    self.homeContainer.addChild(self.commonMenuContainer);

    self.menuButtons[0].click = self.menuButtons[0].tap = self.menuButtons[1].click = self.menuButtons[1].tap = self.menuButtons[2].click = self.menuButtons[2].tap = self.menuButtons[3].click = self.menuButtons[3].tap = self.menuButtons[4].click = self.menuButtons[4].tap = self.menuButtons[5].click = self.menuButtons[5].tap = function(data) {
        //SOUND HERE
        var index = this.hitIndex;

        createjs.Sound.stop();
        var soundInstance = createjs.Sound.play(this.soundMenu, {
            interrupt: createjs.Sound.INTERRUPT_NONE
        });
        soundInstance.addEventListener("failed", handleFailed);
        soundInstance.addEventListener("complete", handleComplete);

        for (var i = self.navContainer.children.length - 1; i >= 0; i--) {
            self.navContainer.removeChild(self.navContainer.children[i]);
        }

        self.navContainer.addChild(self.navButtons[index]);
        self.navContainer.addChild(self.navMenuButtons[index]);
        self.navContainer.addChild(self.ovaButtons[index]);

        self.navButtons[index].click = self.navButtons[index].tap = self.navMenuButtons[index].click = self.navMenuButtons[index].tap = function(data) {
            for (var i = self.navContainer.children.length - 1; i >= 0; i--) {
                self.navContainer.removeChild(self.navContainer.children[i]);
            }
        }

        self.ovaButtons[index].click = self.ovaButtons[index].tap = function(data) {
            createjs.Sound.stop();
            if (index === 0) {
                self.pushToHistory("history_Toystore");
                self.sceneChange('Toystore');
            } else if (index === 1) {
                self.pushToHistory("history_House");
                self.sceneChange('House');
            } else if (index === 3) {
                self.pushToHistory("history_Tivoli");
                self.sceneChange('Tivoli');
            } else if (index === 4) {
                self.pushToHistory("history_Playground");
                self.sceneChange('Playground');
            } else if (index === 5) {
                self.pushToHistory("history_School");
                self.sceneChange('School');
            } else if (index === 2) {
                self.pushToHistory("history_Football");
                self.sceneChange('Football');
            }
        }
    }

    self.commonMenuContainer.addChild(self.navContainer);

    // about button
    this.startpage1 = new PIXI.Sprite.fromImage(this.assetFolder + "startpage1.png");
    this.startpage2 = new PIXI.Sprite.fromImage(this.assetFolder + "startpage2.png");

    this.startpage1.position = this.startpage2.position = {
        x: 25,
        y: 110
    }

    this.startpage1.interactive = this.startpage2.interactive = true;
    this.startpage1.buttonMode = this.startpage2.buttonMode = true;

    this.startpage1.hitArea = new PIXI.TransparencyHitArea.create(this.startpage1);
    this.startpage2.hitArea = new PIXI.TransparencyHitArea.create(this.startpage2);

    this.commonMenuContainer.addChild(this.startpage2);
    this.commonMenuContainer.addChild(this.startpage1);

    this.startpage1.visible = this.startpage2.visible = false;

    this.greenbutton = PIXI.Sprite.fromFrame("greenbutton.png");
    this.greenbutton.buttonMode = true;
    this.greenbutton.position = {
        x: 25,
        y: 1365 - 10
    };

    this.greenbutton.interactive = true;
    main.texturesToDestroy.push(this.greenbutton);

    this.commonMenuContainer.addChild(this.greenbutton);

    this.greenbutton.click = this.greenbutton.tap = this.startPage.bind(this);

    this.startpage1.click = this.startpage1.tap = this.startpage2.click = this.startpage2.tap = function(data) {

        this.bringToFront();

        self.greenbutton.bringToFront();

    }

}

Home.prototype.startPage = function() {
    if (this.startpage1.visible) {
        this.startpage1.visible = this.startpage2.visible = false;
    } else {
        this.startpage1.visible = this.startpage2.visible = true;
    }
}

Home.prototype.pushToHistory = function(key) {
    localStorage.clear();
    localStorage.setItem('pushother', 0);
    if (Device.ieVersion != 9) {
        var exhistory = new History();
        exhistory.PushState({
            scene: key
        });
        exhistory = null;
    }
}

Home.prototype.sceneChange = function(scene) {
    this.cleanMemory();
    main.sceneChange(scene);
};


Home.prototype.cleanMemory = function() {
    this.assetFolder = null;
    this.sceneBackground = null;
    this.menuBackground = null;

    this.bio = null;
    this.store = null;
    this.bensin = null;
    this.flag = null;

    if (this.imageButtonsName)
        this.imageButtonsName.length = 0;

    if (this.imageButtons)
        this.imageButtons.length = 0;

    if (this.imageButtonsPosition)
        this.imageButtonsPosition.length = 0;

    if (this.navButtons)
        this.navButtons.length = 0;

    if (this.navMenuButtons)
        this.navMenuButtons.length = 0;

    if (this.ovaButtons)
        this.ovaButtons.length = 0;

    if (this.menuImages)
        this.menuImages.length = 0;

    if (this.menuPositions)
        this.menuPositions.length = 0;

    if (this.navImages)
        this.navImages.length = 0;

    if (this.navPositions)
        this.navPositions.length = 0;

    if (this.ovaPositions)
        this.ovaPositions.length = 0;

    this.greenbutton = null;
}