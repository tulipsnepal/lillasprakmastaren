function Toy_3_1(stage) {
    cl.show();

    PIXI.DisplayObjectContainer.call(this);
    main.CurrentExercise = "toy_3_1";
    this.baseContainer = new PIXI.DisplayObjectContainer();

    this.sceneContainer = new PIXI.DisplayObjectContainer();

    this.feedbackContainer = new PIXI.SpriteBatch();
    this.rhymeContainer = new PIXI.DisplayObjectContainer();
    this.animationContainer = new PIXI.DisplayObjectContainer();
    this.boxContainer = new PIXI.DisplayObjectContainer();
    this.shuffledToysContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position = {
        x: 77,
        y: 88
    };

    localStorage.setItem('toy_3_1', 0);

    this.baseContainer.addChild(this.sceneContainer);

    main.texturesToDestroy = [];

    this.loadSpriteSheet();

    // everything connected to stage is rendered.
    this.addChild(this.baseContainer);
}

Toy_3_1.constructor = Toy_3_1;
Toy_3_1.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Toy_3_1.imagePath = 'build/exercises/toystore/toy_3_1/images/';
Toy_3_1.audioPath = 'build/exercises/toystore/toy_3_1/audios/';
Toy_3_1.commonFolder = 'build/common/images/toystore/';
Toy_3_1.totalFeedback = Main.TOTAL_FEEDBACK || 10; // default 10

/**
 * preload sprites, audios and images
 */
Toy_3_1.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(Toy_3_1.audioPath + 'intro');
    this.outroId = loadSound(Toy_3_1.audioPath + 'outro');

    // create an array of assets to load
    var assetsToLoad = [
        Toy_3_1.imagePath + "sprite_toystore_3_1_1.json",
        Toy_3_1.imagePath + "sprite_toystore_3_1_1.png",
        Toy_3_1.imagePath + "sprite_toystore_3_1_2.json",
        Toy_3_1.imagePath + "sprite_toystore_3_1_2.png",
        Toy_3_1.imagePath + "shelf.png",
        Toy_3_1.imagePath + "shelf_with_feedback.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * on all assets loaded , load the exercise and json
 */
Toy_3_1.prototype.spriteSheetLoaded = function() {

    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    this.loadDefaults();
    this.initObjectPooling();
    this.sceneContainer.addChild(this.boxContainer);
    this.sceneContainer.addChild(this.rhymeContainer);
    this.sceneContainer.addChild(this.animationContainer);
    this.sceneContainer.addChild(this.shuffledToysContainer);

}

/**
 * create feedback at specific position
 */
Toy_3_1.prototype.renderFeedBack = function() {
    var self = this;

    var feedBackPositions = [
        254, 55,
        402, 77,
        507, 55,
        586, 67,
        561, 146,
        803, -23,
        881, -4,
        1060, 56,
        1199, 66,
        1373, 53
    ];

    for (var k = 0; k < 10; k++) {
        var feedBackSprite = PIXI.Sprite.fromFrame("fb_" + (k + 1) + ".png");
        feedBackSprite.position = {
            x: feedBackPositions[k * 2],
            y: feedBackPositions[k * 2 + 1]
        };
        main.texturesToDestroy.push(feedBackSprite);
        self.feedbackContainer.addChild(feedBackSprite);
        feedBackSprite.visible = false;
    }
};

/**
 * randomize toys position
 */
Toy_3_1.prototype.shuffledToys = function() {
    var self = this;
    var feedBackPositionsShuffled = [
        308, 493,
        251, 552,
        149, 750,
        116, 919,
        321, 681,
        199, 756,
        84, 867,
        431, 774,
        397, 895,
        257, 778
    ];

    for (var k = 9; k >= 0; k--) {
        if (k == 5 || k == 6 || k == 7 || k == 8) {
            var feedBackSprite = PIXI.Sprite.fromFrame("fbs_" + (k + 1) + ".png");
        } else {
            var feedBackSprite = PIXI.Sprite.fromFrame("fb_" + (k + 1) + ".png");
        }

        feedBackSprite.position = {
            x: feedBackPositionsShuffled[k * 2],
            y: feedBackPositionsShuffled[k * 2 + 1]
        };
        main.texturesToDestroy.push(feedBackSprite);
        self.shuffledToysContainer.addChild(feedBackSprite);
    }
}

/**
 * create white, red , green box graphics
 */
Toy_3_1.prototype.renderWhiteBoxes = function() {
    this.whiteBoxPositions = [
        798, 397,
        1065, 397,
        1332, 397,
        1599, 397,
        1076, 629,
        798, 949,
        1065, 949,
        1332, 949,
        1599, 949
    ];
    for (var j = 0; j < 9; j++) {

        var boxParams = {
            x: this.whiteBoxPositions[j * 2],
            y: this.whiteBoxPositions[j * 2 + 1],
            w: j == 4 ? 285 : 187,
            h: j == 4 ? 285 : 187,
            lineStyle: 1
        }
        var box = createRect(boxParams);
        this.boxContainer.addChild(box);
    }

    this.greenRect = createRect({
        w: 187,
        h: 187,
        color: 0x00ff00,
        alpha: 0.6,
        lineStyle: 0
    });
    this.redRect = createRect({
        w: 187,
        h: 187,
        color: 0xff0000,
        alpha: 0.6,
        lineStyle: 0
    });
}


/**
 * draw graphics, create textures , sprites and images
 * add movie clip for animation
 */
Toy_3_1.prototype.loadDefaults = function() {

    var self = this;

    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0xf6f19b);
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xd6e4ae);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);


    this.shelf = PIXI.Sprite.fromImage(Toy_3_1.imagePath + "shelf.png");
    this.shelf.position = {
        x: 66,
        y: 166
    }
    main.texturesToDestroy.push(this.shelf);
    this.sceneContainer.addChild(this.shelf);

    this.defaultQCharacter = PIXI.Sprite.fromFrame("q_at_shelf.png");
    this.defaultQCharacter.position = {
        x: 114,
        y: 20
    }

    this.toyHeap = PIXI.Sprite.fromFrame("heap.png");
    this.toyHeap.position = {
        x: 8,
        y: 647
    };


    main.texturesToDestroy.push(this.defaultQCharacter);
    main.texturesToDestroy.push(this.toyHeap);
    this.sceneContainer.addChild(this.defaultQCharacter);
    this.sceneContainer.addChild(this.toyHeap);
    this.toyHeap.visible = false;

    this.renderFeedBack();
    this.shuffledToys();
    this.renderWhiteBoxes();

    var handNames = ['ARM_to-side.png', 'ARM_2.png', 'arm_pointing_up.png', 'ARM_3.png', 'ARM_POINTINGATEXERSICE.png'];

    this.hands = [];

    for (var i = 0; i < handNames.length; i++) {
        var handSprite = PIXI.Sprite.fromFrame(handNames[i]);
        handSprite.position.set(621, 343);
        this.animationContainer.addChild(handSprite);
        handSprite.visible = false;
        this.hands.push(handSprite);
    }

    this.mouthParams = {
        "image": "q",
        "length": 3,
        "x": 218,
        "y": 286,
        "speed": 5,
        "pattern": true
    };

    this.animateMouth = new Animation(this.mouthParams);

    this.animationContainer.addChild(this.animateMouth);
    this.animateMouth.visible = false;

    this.help = PIXI.Sprite.fromFrame("topcorner.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position = {
        x: -77,
        y: -88
    }
    this.animationContainer.addChild(this.help);

    this.ovaigen = PIXI.Sprite.fromFrame("ovaigen.png");
    this.ovaigen.buttonMode = true;
    this.ovaigen.interactive = true;
    this.ovaigen.position = {
        x: 1352,
        y: 897
    };
    this.animationContainer.addChild(this.ovaigen);
    this.ovaigen.visible = false;



    this.outroArms = PIXI.Sprite.fromFrame("outro_q_arms.png");
    this.outroArms.position = {
        x: 78,
        y: 290
    }
    main.texturesToDestroy.push(this.outroArms);
    this.sceneContainer.addChild(this.outroArms);
    this.outroArms.visible = false;

    this.mouthOutroParams = {
        "image": "outro_q_",
        "length": 3,
        "x": 632,
        "y": 45,
        "speed": 6,
        "pattern": true,
        "frameId": 3
    };

    this.animateMouthOutro = new Animation(this.mouthOutroParams);

    this.animationContainer.addChild(this.animateMouthOutro);
    this.animateMouthOutro.visible = false;

    this.shelfOutro = PIXI.Sprite.fromImage(Toy_3_1.imagePath + "shelf_with_feedback.png");
    this.shelfOutro.position = {
        x: 75,
        y: 370
    }
    main.texturesToDestroy.push(this.shelfOutro);
    this.animationContainer.addChild(this.shelfOutro);
    this.shelfOutro.visible = false;

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.feedbackContainer);
    this.updateFeedback();

}

/**
 * hand visibility
 * @param  {int} number
 */
Toy_3_1.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    }
    if (isNumeric(number))
        this.hands[number].visible = true;
};

/**
 * play intro animation
 */
Toy_3_1.prototype.introAnimation = function() {

    createjs.Sound.stop();

    if (!this.introMode)
        this.restartExercise();

    this.help.interactive = false;
    this.help.buttonMode = false;

    main.overlay.visible = true;

    this.shuffledToysContainer.visible = false;
    this.defaultQCharacter.visible = false;
    this.toyHeap.visible = true;
    this.animateMouth.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
    1 - 4 SEC ARM_to side
    4 - 7 SEC ARM_2
    7 - 11 SEC ARM_POINTING_UP
    11 - 13 SEC ARM_3
    13 - 16 SEC ARM_POINTINGATEXERCISE
    STOP AT ARM_to side

 */
Toy_3_1.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    if (currentPostion >= 4000 && currentPostion < 7000) {
        this.showHandAt(1);
    } else if (currentPostion >= 7000 && currentPostion < 11000) {
        this.showHandAt(2);
    } else if (currentPostion >= 11000 && currentPostion < 13000) {
        this.showHandAt(3);
    } else if (currentPostion >= 13000 && currentPostion < 16000) {
        this.showHandAt(4);
    } else {
        this.showHandAt(0);
    }
};

/**
 * cancel running intro animation
 */
Toy_3_1.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);
    this.showHandAt();
    this.animateMouth.visible = false;
    this.defaultQCharacter.visible = true;
    this.toyHeap.visible = false;
    this.shuffledToysContainer.visible = true;
    this.help.interactive = true;
    this.help.buttonMode = true;

    this.ovaigen.visible = false;
    main.overlay.visible = false;
}

/**
 * play outro animation
 */
Toy_3_1.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.animateMouthOutro.show(true)
        self.help.interactive = false;
        self.help.buttonMode = false;
    }
    this.outroCancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.outroCancelHandler);

};

/**
 * stop outro animation
 */
Toy_3_1.prototype.cancelOutro = function() {

    var self = this;

    createjs.Sound.stop();
    self.ovaigen.visible = true;
    self.animateMouthOutro.hide(3);
    self.help.interactive = true;
    self.help.buttonMode = true;
    main.overlay.visible = false;

};

/**
 * reset exercise to default settings
 */
Toy_3_1.prototype.resetExercise = function() {
    var self = this;
    this.ovaigen.visible = false;
    self.shelfOutro.visible = false;
    localStorage.setItem('toy_3_1', 0);
    self.shelf.visible = true;
    self.boxContainer.visible = true;
    self.rhymeContainer.visible = true;
    self.animateMouthOutro.visible = false;
    self.defaultQCharacter.visible = true;
    self.outroArms.visible = false;
    self.feedbackContainer.visible = true;
    self.shelf.visible = true;
    self.showHideChildrenInContainer(self.shuffledToysContainer, true);
    self.shuffledToysContainer.visible = true;
    this.introMode = true;
}

/**
 * exercise logic
 */
Toy_3_1.prototype.runExercise = function() {
    var self = this,
        jsonData = this.wallSlices[0],
        itemPositions = [
            798, 397,
            1065, 397,
            1332, 397,
            1599, 397,
            798, 949,
            1065, 949,
            1332, 949,
            1599, 949
        ],
        imageButtons = [];


    this.soundTexture = new PIXI.Texture.fromFrame("sound_grey.png");

    for (var k = 0; k < jsonData.alphabets.length; k++) {
        var optionAlphabets = new PIXI.Text(jsonData.alphabets[k] + jsonData.alphabets[k].toLowerCase(), {
            font: "90px Arial",
            fill: "black",
            align: "center",
        });

        optionAlphabets.position = {
            x: itemPositions[k * 2] + (187 - optionAlphabets.width) / 2,
            y: itemPositions[k * 2 + 1] + 40
        };

        main.texturesToDestroy.push(optionAlphabets);
        self.rhymeContainer.addChild(optionAlphabets);

        var alphaInteractive = PIXI.Sprite.fromFrame("interactive.png");
        alphaInteractive.interactive = true;
        alphaInteractive.buttonMode = true;
        alphaInteractive.height = 187;
        alphaInteractive.width = 187;
        alphaInteractive.hitIndex = k;
        alphaInteractive.selectedAlphabet = jsonData.alphabets[k];
        alphaInteractive.position = {
            x: itemPositions[k * 2],
            y: itemPositions[k * 2 + 1]
        };
        main.texturesToDestroy.push(alphaInteractive);
        self.rhymeContainer.addChild(alphaInteractive);
        imageButtons.push(alphaInteractive);

        var sound = new PIXI.Sprite(this.soundTexture);

        sound.buttonMode = true;
        sound.position.x = itemPositions[k * 2];
        sound.position.y = itemPositions[k * 2 + 1] + 135;
        sound.interactive = true;
        self.rhymeContainer.addChild(sound);
        sound.hitIndex = k;
        sound.alphabetsHowl = jsonData.alphaAudios[k];
        sound.click = sound.tap = function(data) {
            var snd = this;
            if (addedListeners) return false;
            addedListeners = true;
            createjs.Sound.stop();
            if (jsonData.alphabets[snd.hitIndex] == "U" || jsonData.alphabets[snd.hitIndex] == "Y") {
                var soundToPlay = jsonData.alphabets[snd.hitIndex] == "U" ? 0 : 1;
                var vowelInstance = createjs.Sound.play(jsonData.vowel_alpha[soundToPlay]);
                vowelInstance.addEventListener('complete', function() {
                    var c = setTimeout(function() {
                        var alphabetsInstance = createjs.Sound.play(snd.alphabetsHowl);
                        alphabetsInstance.addEventListener('complete', function() {
                            addedListeners = false;
                        });
                    }, 300);
                });
            } else {
                var alphabetsInstance = createjs.Sound.play(snd.alphabetsHowl);
                alphabetsInstance.addEventListener('complete', function() {
                    addedListeners = false;
                });
            }

        }
    }

    //NOW ADD IMAGE
    var imageSprite = jsonData.image;

    /*imageSprite.position = {
        x: 1085,
        y: 632
    }*/
    imageSprite.position = {
        x: 1076,
        y: 629
    }
    imageSprite.height = imageSprite.width = 285;

    self.rhymeContainer.addChild(imageSprite);
    var soundSprite = new PIXI.Sprite(this.soundTexture);
    soundSprite.interactive = true;
    soundSprite.buttonMode = true;
    soundSprite.position = {
        x: 1076,
        y: 859
    };
    soundSprite.howl = jsonData.sound;
    self.rhymeContainer.addChild(soundSprite);

    soundSprite.click = soundSprite.tap = this.onSoundPressed.bind(soundSprite);

    //ADD QUESTION, i.e incomplete word
    this.questionText = new PIXI.Text(jsonData.question, {
        font: "70px Arial",
        fill: "black",
    });

    this.questionText.position = {
        x: 1427,
        y: 741
    };

    main.texturesToDestroy.push(this.questionText);
    self.rhymeContainer.addChild(this.questionText);


    for (var i = 0; i < jsonData.alphabets.length; i++) {
        imageButtons[i].click = imageButtons[i].tap = function(data) {
            if (addedListeners) return false;
            addedListeners = true;
            if (this.selectedAlphabet.toLowerCase() == jsonData.answer) {
                setTimeout(function() {
                    createjs.Sound.play(jsonData.sound);
                }, 300);
                self.questionText.setText(jsonData.question.toString().replace("_", jsonData.answer));
                self.showRedOrGreenBox(itemPositions[this.hitIndex * 2], itemPositions[this.hitIndex * 2 + 1], self.greenRect);
                setTimeout(function() {
                    self.correctFeedback();
                }, 1000);

            } else {
                wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                self.showRedOrGreenBox(itemPositions[this.hitIndex * 2], itemPositions[this.hitIndex * 2 + 1], self.redRect);
                setTimeout(function() {
                    self.wrongFeedback();
                }, 300);
            }
        }
    }

    this.rhymeContainer.addChild(this.greenRect);
    this.rhymeContainer.addChild(this.redRect);
    this.greenRect.visible = false;
    this.redRect.visible = false;

};


Toy_3_1.prototype.onSoundPressed = function() {
    this.soundInstance = createjs.Sound.play(this.howl, {
        interrupt: createjs.Sound.INTERRUPT_NONE
    });
    this.soundInstance.addEventListener("failed", handleFailed);
    this.soundInstance.addEventListener("complete", handleComplete);
};

/**
 * on correct answer, feedback increase and show respective feedback
 * get new set
 */
Toy_3_1.prototype.correctFeedback = function() {
    var self = this;
    var feedback = localStorage.getItem('toy_3_1');
    feedback++;
    localStorage.setItem('toy_3_1', feedback);
    self.updateFeedback();
    if (feedback >= Toy_3_1.totalFeedback) {
        setTimeout(function() {
            self.onComplete(); // go to outro scene
        }, 1200);
    } else {

        self.rhymeContainer.removeChildren();
        self.returnWallSprites();
        self.borrowWallSprites(1);
        self.runExercise();
    }
    addedListeners = false;
};

/**
 * on wrong answer, feedback decrease and hide respective feedback sprite
 */
Toy_3_1.prototype.wrongFeedback = function() {
    var self = this;
    var feedback = localStorage.getItem('toy_3_1');
    feedback--;

    if (feedback < 0)
        feedback = 0;

    localStorage.setItem('toy_3_1', feedback);
    self.updateFeedback();
    addedListeners = false;
};

/**
 * load outro scene
 */
Toy_3_1.prototype.onComplete = function() {

    var self = this;

    this.introMode = false;

    self.feedbackContainer.visible = false;

    self.showHideChildrenInContainer(self.feedbackContainer, false);
    self.animateMouthOutro.visible = true;
    this.outroArms.visible = true;
    this.shelf.visible = false;
    this.shelfOutro.visible = true;
    this.defaultQCharacter.visible = false;
    this.shuffledToysContainer.visible = false;
    this.boxContainer.visible = false;

    this.rhymeContainer.visible = false;

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        self.cancelOutro();
        self.restartExercise();
    }

    this.outroAnimation();

}

/**
 * update feedback on correct and wrong answer
 */
Toy_3_1.prototype.updateFeedback = function() {

    var self = this;

    var counter = localStorage.getItem('toy_3_1');

    self.showHideChildrenInContainer(self.feedbackContainer, false);
    self.showHideChildrenInContainer(self.shuffledToysContainer, true);

    for (var j = 0; j < counter; j++) {
        self.feedbackContainer.children[j].visible = true;
        self.shuffledToysContainer.children[9 - j].visible = false;
    }
}


/**
 * object pooling to get new object from exercise pool
 * borrow required number of objects (default 2)
 */
Toy_3_1.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/toystore/toystore_3_1.json?noache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        self.pool = new ExerciseSpritesPool(loader.json);
        self.generatePoolCategory();
        //borrow two sprites once
        self.borrowWallSprites(2);
        self.runExercise();

    });
    loader.load();
}

Toy_3_1.prototype.generatePoolCategory = function() {

    var keyArr = [];

    var spriteArr = this.pool.sprites,
        spriteLen = spriteArr.length;


    for (var i = 0; i < spriteLen; i++) {
        keyArr.push(spriteArr[i][0].image[0]);
    };

    this.poolCategory = keyArr.filter(onlyUnique);
    this.poolCategoryType = "image";

}

/**
 * borrow pool objects
 * and preload sprites and sounds in that object pool
 * @param  {int} num
 */
Toy_3_1.prototype.borrowWallSprites = function(num) {

    var rand, category, sprite, spriteObj;

    var self = this;
    this.num = num;

    for (var i = 0; i < this.num; i++) {
        //var sprite = this.pool.borrowSprite();

        rand = makeUniqueRandom(this.poolCategory.length);
        this.borrowCategory = this.poolCategory[rand];

        sprite = this.pool.borrowUniqueSprite(this.borrowCategory, this.lastBorrowSprite, this.poolCategoryType);

        this.poolSlices.push(sprite);
        var spriteObj = sprite[0];

        this.lastBorrowSprite = spriteObj.image;

        var alphabetsArr = ['R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y'];
        var alphaSoundArr = [];

        var soundObj = StripExtFromFile(spriteObj.sound);

        var howl = loadSound('uploads/audios/' + soundObj);

        for (var k = 0; k < alphabetsArr.length; k++) {
            var kt = k;
            var alphaHowl = loadSound('uploads/audios/alphabets/small/' + StripExtFromFile(alphabetsArr[k].toLowerCase()));
            alphaSoundArr.push(alphaHowl);
        }

        var vowelAlpha = [];
        for (var k = 0; k < 2; k++) {

            var vowelAlphaS = loadSound('uploads/audios/alphabets/capital/' + (k == 0 ? "U" : "Y"));
            vowelAlpha.push(vowelAlphaS);
        }


        shuffle(alphabetsArr, alphaSoundArr);

        this.wallSlices.push({
            image: PIXI.Sprite.fromImage("uploads/images/" + spriteObj.image),
            sound: howl,
            answer: spriteObj.answer,
            question: spriteObj.sentence,
            alphabets: alphabetsArr,
            alphaAudios: alphaSoundArr,
            vowel_alpha: vowelAlpha
        });
    }
};

/**
 * return objects that are used in exercise to pool
 */
Toy_3_1.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];
    main.eventRunning = false;
    this.pool.returnSprite(sprite);

    this.rhymeContainer.removeChildren();

    this.poolSlices = [];
    this.wallSlices.shift();
};

/**
 * reset exercise and get new set
 */
Toy_3_1.prototype.restartExercise = function() {
    var self = this;
    this.resetExercise();
    localStorage.setItem('toy_3_1', 0);
    self.showHideChildrenInContainer(self.feedbackContainer, false);
    this.rhymeContainer.removeChildren();
    this.returnWallSprites();
    this.borrowWallSprites(1);
    this.runExercise();
};

/**
 * show red green sprite based on feedback response
 * @param  {int} xPos
 * @param  {int} yPos
 * @param  {object} obj
 */
Toy_3_1.prototype.showRedOrGreenBox = function(xPos, yPos, obj) {
    var self = this;

    obj.position = {
        x: xPos,
        y: yPos
    };

    obj.visible = true;

    setTimeout(function() {
        obj.visible = false;
    }, 400);
};

/**
 * show hide children container
 * @param  {object} obj
 * @param  {boolean} show
 */
Toy_3_1.prototype.showHideChildrenInContainer = function(obj, show) {

    for (var j = 0; j < obj.children.length; j++) {
        obj.children[j].visible = show;
    }
}

/**
 * clear memory, container , grpahics
 */
Toy_3_1.prototype.cleanMemory = function() {

    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    this.shelf = null;
    this.defaultQCharacter = null;

    this.animateArms = null;

    this.help = null;
    this.ovaigen = null;

    if (this.poolSlices)
        this.poolSlices.length = 0;

    if (this.wallSlices)
        this.wallSlices.length = 0;

    this.pool = null;

    this.sceneContainer = null;
    this.feedbackContainer = null;
    this.rhymeContainer = null;
    this.animationContainer = null;

    this.boxContainer = null;
    this.shuffledToysContainer = null;
    this.whiteBoxPositions = null;
    this.toyHeap = null;

    if (this.mouthParams)
        this.mouthParams.length = 0;

    this.animateMouth = null;
    this.outroArms = null;
    this.animateMouthOutro = null;
    this.shelfOutro = null;
    this.soundTexture = null;
    this.questionText = null;

    this.baseContainer.removeChildren();
    this.baseContainer = null;
}