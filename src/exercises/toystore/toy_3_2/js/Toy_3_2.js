function Toy_3_2() {

	cl.show();

	PIXI.DisplayObjectContainer.call(this);

	this.sceneContainer = new PIXI.DisplayObjectContainer();
	this.sceneContainer.position.set(77, 88);

	this.introContainer = new PIXI.DisplayObjectContainer();
	this.outroContainer = new PIXI.DisplayObjectContainer();

	localStorage.setItem('toy_3_2', 0);
	main.CurrentExercise = "toy_3_2";

	this.common = new Common();

	main.texturesToDestroy = [];

	this.loadSpriteSheet();

	// everything connected to this is rendered.
	this.addChild(this.sceneContainer);
}

Toy_3_2.constructor = Toy_3_2;
Toy_3_2.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Toy_3_2.imagePath = 'build/exercises/toystore/toy_3_2/images/';
Toy_3_2.audioPath = 'build/exercises/toystore/toy_3_2/audios/';
Toy_3_2.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

/**
 * load array of assets of different format
 */
Toy_3_2.prototype.loadSpriteSheet = function() {

	this.introId = loadSound(Toy_3_2.audioPath + 'intro_a');
	this.outroId = loadSound(Toy_3_2.audioPath + 'outro');

	// create an array of assets to load
	this.assetsToLoad = [
		Toy_3_2.imagePath + "sprite_3_2.json",
		Toy_3_2.imagePath + "sprite_3_2.png",

	];
	// create a new loader
	loader = new PIXI.AssetLoader(this.assetsToLoad);
	// use callback
	loader.onComplete = this.spriteSheetLoaded.bind(this);
	//begin load
	loader.load();

}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
Toy_3_2.prototype.spriteSheetLoaded = function() {

	// hide loader
	cl.hide();

	if (this.sceneContainer.stage)
		this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

	// fill exercise background
	this.sceneBackground = new PIXI.Graphics();
	this.sceneBackground.beginFill(0xf6f19b);
	// set the line style to have a width of 5 and set the color to red
	this.sceneBackground.lineStyle(3, 0xd7e5ae);
	// draw a rectangle
	this.sceneBackground.drawRect(0, 0, 1843, 1202);
	this.sceneContainer.addChild(this.sceneBackground);

	// exercise instruction
	this.help = PIXI.Sprite.fromFrame("topcorner.png");
	this.help.interactive = true;
	this.help.buttonMode = true;
	this.help.position.set(-77, -88);
	this.sceneContainer.addChild(this.help);

	this.help.click = this.help.tap = this.introAnimation.bind(this);

	this.loadExercise();
	this.introScene();

	this.sceneContainer.addChild(this.introContainer);

}

/**
 * create sprites,animation,sound related to intro scene
 */
Toy_3_2.prototype.introScene = function() {

	var self = this;

	this.outroScene();
	this.introContainer.addChild(this.outroContainer);

	this.animationContainer = new PIXI.DisplayObjectContainer();
	this.introContainer.addChild(this.animationContainer);

	this.figure_intro = new PIXI.Sprite.fromFrame('figure_intro.png');
	this.figure_intro.position.set(2, 114);
	main.texturesToDestroy.push(this.figure_intro);

	this.mouths = new Animation({
		"image": "mouth_",
		"length": 4,
		"x": 284,
		"y": 332,
		"speed": 6,
		"pattern": true,
		"frameId": 3
	});


	this.figure_intro.visible = false;
	this.mouths.visible = false;

	// enable mouth,arms animation to container
	this.animationContainer.addChild(this.figure_intro);
	this.animationContainer.addChild(this.mouths);


	this.hands = [
		new PIXI.Sprite.fromFrame('arm_1.png'),
		new PIXI.Sprite.fromFrame('arm_2.png'),
		new PIXI.Sprite.fromFrame('arm_3.png'),
		new PIXI.Sprite.fromFrame('arm_4.png'),
	];
	for (var i = 0; i < this.hands.length; i++) {
		this.hands[i].position.set(299 + 23, 345);
		this.animationContainer.addChild(this.hands[i]);
		this.hands[i].visible = false;
	}
}

/**
 * hand visibility
 * @param  {int} number
 */
Toy_3_2.prototype.showHandAt = function(number) {
	for (var i = 0; i < this.hands.length; i++) {
		this.hands[i].visible = false;
	}
	if (isNumeric(number))
		this.hands[number].visible = true;
};

/**
 * play intro animation
 */
Toy_3_2.prototype.introAnimation = function() {

	createjs.Sound.stop();

	if (!this.introMode)
		this.restartExercise();

	this.help.interactive = false;
	this.help.buttonMode = false;

	main.overlay.visible = true;

	this.topImage0.visible = false;
	this.figure_intro.visible = true;

	this.mouths.show();

	this.introInstance = createjs.Sound.play(this.introId, {
		interrupt: createjs.Sound.INTERRUPT_ANY
	});

	if (createjs.Sound.isReady())
		this.timerId = setInterval(this.animateIntro.bind(this), 1);

	this.introCancelHandler = this.cancelIntro.bind(this);
	this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
0-2 SEC ARM_4
2-3 SEC ARM_2
3-4 SEC ARM_1
5-STOP ARM_4
 */
Toy_3_2.prototype.animateIntro = function() {

	var currentPostion = this.introInstance.getPosition();
	if (currentPostion >= 2000 && currentPostion < 3000) {
		this.showHandAt(1);
	} else if (currentPostion >= 3000 && currentPostion < 4000) {
		this.showHandAt(0);
	} else {
		this.showHandAt(3);
	}
};

/**
 * cancel running intro animation
 */
Toy_3_2.prototype.cancelIntro = function() {

	createjs.Sound.stop();

	clearInterval(this.timerId);
	this.mouths.hide(3);
	this.figure_intro.visible = false;
	this.topImage0.visible = true;
	this.mouths.visible = false;
	this.showHandAt();

	this.help.interactive = true;
	this.help.buttonMode = true;

	this.ovaigen.visible = false;
	main.overlay.visible = false;
}

/**
 * play outro animation
 */
Toy_3_2.prototype.outroAnimation = function() {

	var self = this;

	this.outroInstance = createjs.Sound.play(this.outroId);
	if (createjs.Sound.isReady()) {
		main.overlay.visible = true;
		self.help.interactive = false;
		self.help.buttonMode = false;

		self.mouths.show();
	}
	this.outroCancelHandler = this.cancelOutro.bind(this);
	this.outroInstance.addEventListener("complete", this.outroCancelHandler);

};

/**
 * stop running outro animation
 */
Toy_3_2.prototype.cancelOutro = function() {

	var self = this;

	createjs.Sound.stop();
	self.mouths.hide(3);
	self.mouths.visible = false;

	self.help.interactive = true;
	self.help.buttonMode = true;

	self.ovaigen.visible = true;
	main.overlay.visible = false;

};

/**
 * reset exercise with default values
 */
Toy_3_2.prototype.resetExercise = function() {
	var self = this;
	this.ovaigen.visible = false;
	localStorage.setItem('toy_3_2', 0);
	self.outroContainer.visible = false;
	self.introMode = true;
	self.mouths.position.set(284, 332);
}


/**
 * create sprites,animation,sound related to outro scene
 */
Toy_3_2.prototype.outroScene = function() {

	var self = this;

	this.outroContainer.visible = false;

	this.figure_outro = new PIXI.Sprite.fromFrame('figure_outro.png');
	this.figure_outro.position.set(685, 350);
	main.texturesToDestroy.push(this.figure_outro);
	this.outroContainer.addChild(this.figure_outro);

	// back to intro page
	this.ovaigen = new PIXI.Sprite.fromFrame('ovaigen.png');
	this.ovaigen.position.set(744, 787);
	this.ovaigen.interactive = true;
	this.ovaigen.buttonMode = true;
	this.ovaigen.visible = false;
	this.outroContainer.addChild(this.ovaigen);

	this.figure_hand_outro = new PIXI.Sprite.fromFrame('ted_hand.png');
	this.figure_hand_outro.position.set(702, 786);
	main.texturesToDestroy.push(this.figure_hand_outro);
	this.outroContainer.addChild(this.figure_hand_outro);

}

/**
 * load exercise related stuffs
 */
Toy_3_2.prototype.loadExercise = function() {

	this.exerciseBaseContainer = new PIXI.DisplayObjectContainer();
	this.introContainer.addChild(this.exerciseBaseContainer);

	// create rectangle graphics with white background in different sizes.
	var miniRect = createRect({
			w: 54,
			h: 54
		}),
		smallRect = createRect({
			w: 141,
			h: 141
		}),
		bigRect = createRect({
			w: 197,
			h: 197
		}),
		miniLine = createLine({
			x1: 250
		});

	// create textures from the rectangle graphics to reuse
	this.miniRectTexture = miniRect.generateTexture();
	this.smallRectTexture = smallRect.generateTexture();
	this.bigRectTexture = bigRect.generateTexture();
	this.miniLineTexture = miniLine.generateTexture();

	this.smallRectPosition = [
		492, 256,
		1140, 256,
		492, 764,
		1140, 764,
	];

	this.bigRect = {
		x: 701,
		y: 83,
		w: 210,
		h: 210
	}

	var smallRectSprite, bigRectSprite, bigX, bigY;

	var topImage1 = new PIXI.Sprite.fromFrame('top1.png'),
		topImage2 = new PIXI.Sprite.fromFrame('top2.png'),
		topImage3 = new PIXI.Sprite.fromFrame('top3.png');

	topImage1.position.set(1144, 156);
	topImage2.position.set(444, 630);
	topImage3.position.set(1144, 568);

	this.exerciseBaseContainer.addChild(topImage2);

	this.topImage0 = new PIXI.Sprite.fromFrame('top0.png');
	this.topImage0.position.set(393 + 60, 92);
	this.topImage0.rotation = 0.4;
	this.exerciseBaseContainer.addChild(this.topImage0);

	this.dropParams = {
		'position': [
			this.topImage0.position.x, this.topImage0.position.y,
			topImage1.position.x, topImage1.position.y,
			topImage2.position.x, topImage2.position.y,
			topImage3.position.x, topImage3.position.y
		],
		'w': 259,
		'h': 296 + 100
	};

	// create sprite from rectangle texture
	for (var i = 0; i < 4; i++) {
		smallRectSprite = new PIXI.Sprite(this.smallRectTexture);
		smallRectSprite.position.set(this.smallRectPosition[i * 2], this.smallRectPosition[i * 2 + 1]);
		this.exerciseBaseContainer.addChild(smallRectSprite);
	};

	this.spritePosition = [];

	for (var i = 0; i < 2; i++) {
		for (var j = 0; j < 5; j++) {
			// bigRectSprite = new PIXI.Sprite(this.bigRectTexture);
			bigX = this.bigRect.x + (i * this.bigRect.w);
			bigY = this.bigRect.y + (j * this.bigRect.h);
			this.spritePosition.push(bigX);
			this.spritePosition.push(bigY);
		};
	};

	this.exerciseBaseContainer.addChild(topImage1);
	this.exerciseBaseContainer.addChild(topImage3);

	this.dragParentContainer = new PIXI.DisplayObjectContainer();
	this.introContainer.addChild(this.dragParentContainer);

	this.feedbackPosition = {
		0: {
			x: 111,
			y: 184
		},
		2: {
			x: 111,
			y: 717
		},
		1: {
			x: 1375,
			y: 184
		},
		3: {
			x: 1375,
			y: 717
		}
	}

	var redRect = createRect({
		"alpha": 0.6,
		"color": 0xff0000,
		"w": 210,
		"h": 210,
	});

	var greenRect = createRect({
		"alpha": 0.6,
		"color": 0x00ff00,
		"w": 210,
		"h": 210,
	});

	this.redBox = redRect.generateTexture();
	this.greenBox = greenRect.generateTexture();
	//this.redBox.visible = false;

	this.initObjectPooling();
}

/**
 * manage exercise with new sets randomized
 */
Toy_3_2.prototype.runExercise = function() {

	var self = this;

	this.jsonData = this.wallSlices[0];
	var soundTexture = PIXI.Texture.fromFrame("sound_grey.png");

	this.iconWord = [];
	this.dragButtons = [];

	this.counter = {
		0: 0,
		1: 0,
		2: 0,
		3: 0
	}

	for (var i = 0; i < 4; i++) {
		var answerSprite = new PIXI.Text(this.jsonData.answer[i] + this.jsonData.answer[i].toLowerCase(), {
			font: "50px Arial",
		});
		main.texturesToDestroy.push(answerSprite);
		answerSprite.position.set(this.smallRectPosition[i * 2] + 37, this.smallRectPosition[i * 2 + 1] + 46);
		var alphaSound = new PIXI.Sprite(soundTexture);
		alphaSound.interactive = true;
		alphaSound.buttonMode = true;
		alphaSound.position = {
			x: this.smallRectPosition[i * 2] + 5 - 10,
			y: this.smallRectPosition[i * 2 + 1] + 100 - 6
		};
		alphaSound.howl = this.jsonData.alpha_sound_small[i];
		alphaSound.howlVowel = this.jsonData.alpha_sound_big[i];
		alphaSound.hitIndex = i;
		alphaSound.click = alphaSound.tap = function() {
			if (addedListeners) return false;
			addedListeners = true;
			createjs.Sound.stop();
			var alphaClicked = this;

			if (self.jsonData.answer[this.hitIndex] != "Z") {
				var vowelInstance = createjs.Sound.play(this.howlVowel);
				vowelInstance.addEventListener('complete', function() {
					var c = setTimeout(function() {
						var alphaIntance = createjs.Sound.play(alphaClicked.howl);
						alphaIntance.addEventListener('complete', handleComplete);
					}, 300);
				});
			} else {
				var alphaIntance = createjs.Sound.play(alphaClicked.howl);
				alphaIntance.addEventListener('complete', function() {
					addedListeners = false;
				});
			}
		}
		this.dragParentContainer.addChild(alphaSound);

		this.dragParentContainer.addChild(answerSprite);
	};

	for (var i = 0; i < 10; i++) {
		var dragContainer = new PIXI.DisplayObjectContainer(),
			imageSprite = new PIXI.Sprite(this.jsonData.image[i]),
			soundSprite = new PIXI.Sprite(soundTexture),
			redRectSprite = new PIXI.Sprite(this.redBox),
			greenRectSprite = new PIXI.Sprite(this.greenBox),
			spriteBase = new PIXI.Sprite(this.bigRectTexture);

		main.texturesToDestroy.push(imageSprite);

		spriteBase.position = {
			x: self.spritePosition[i * 2],
			y: self.spritePosition[i * 2 + 1]
		}

		/*imageSprite.width = 160;
		imageSprite.height = 160;

		imageSprite.position = {
			x: self.spritePosition[i * 2] + 20,
			y: self.spritePosition[i * 2 + 1] + 30
		}*/

		imageSprite.width = 197;
		imageSprite.height = 197;

		imageSprite.position = {
			x: self.spritePosition[i * 2],
			y: self.spritePosition[i * 2 + 1]
		}

		soundSprite.position = {
			x: self.spritePosition[i * 2],
			y: self.spritePosition[i * 2 + 1] + 150
		}

		redRectSprite.position = greenRectSprite.position = {
			x: self.spritePosition[i * 2],
			y: self.spritePosition[i * 2 + 1]
		}
		greenRectSprite.alpha = redRectSprite.alpha = 0.6;
		greenRectSprite.visible = redRectSprite.visible = false;
		dragContainer.redBoxItem = redRectSprite;
		dragContainer.greenBoxItem = greenRectSprite;

		dragContainer.addChild(spriteBase);
		dragContainer.addChild(imageSprite);
		dragContainer.addChild(soundSprite);
		dragContainer.addChild(greenRectSprite);
		dragContainer.addChild(redRectSprite);

		var lowerWord = this.jsonData.word[i].toLowerCase(),
			lowerGroupLetter = this.jsonData.group[i].toLowerCase(),
			dropIndex = 0,
			newHeight = 0;

		for (var k = 0; k < 4; k++) {

			var lowerAnswerLetter = this.jsonData.answer[k].toLowerCase();

			if (lowerGroupLetter === lowerAnswerLetter) {
				dropIndex = k;
				this.counter[dropIndex] ++;
			}

		};
		dragContainer.dropIndex = dropIndex;

		this.iconWord.push({
			'index': dropIndex,
			'image': this.jsonData.image[i],
			'word': lowerWord,
			'newHeight': (this.counter[dropIndex] * 70) - 70
		});

		soundSprite.interactive = true;
		soundSprite.howl = this.jsonData.sound[i];
		// play sprite sound on click/tap event
		soundSprite.click = soundSprite.tap = this.onSoundPressed.bind(soundSprite);

		dragContainer.howlPosition = soundSprite.position;

		dragContainer.interactive = true;
		dragContainer.buttonMode = true;
		this.dragButtons.push(dragContainer);
		this.dragParentContainer.addChild(dragContainer);


	};

	this.hiddenFeedback = [];

	for (var i = 0; i < 10; i++) {
		this.generateFeedback(i);
		this.onDragging(i);
	};

}

Toy_3_2.prototype.onSoundPressed = function() {
	createjs.Sound.stop();
	this.soundInstance = createjs.Sound.play(this.howl, {
		interrupt: createjs.Sound.INTERRUPT_NONE
	});
	this.soundInstance.addEventListener("failed", handleFailed);
	this.soundInstance.addEventListener("complete", handleComplete);
};


/**
 * generate output feedback
 */
Toy_3_2.prototype.generateFeedback = function(i) {

	var feedbackContainer = new PIXI.DisplayObjectContainer(),
		miniRectSprite = new PIXI.Sprite(this.miniRectTexture),
		miniLineSprite = new PIXI.Sprite(this.miniLineTexture),
		miniImageSprite = new PIXI.Sprite(this.iconWord[i].image),
		miniWordText = new PIXI.Text(this.iconWord[i].word, {
			font: "24px Arial"
		}),
		index = this.iconWord[i].index,
		newHeight = this.iconWord[i].newHeight;

	miniImageSprite.width = 40;
	miniImageSprite.height = 40;

	miniRectSprite.position = {
		x: this.feedbackPosition[index].x,
		y: this.feedbackPosition[index].y + newHeight - 20
	};

	miniImageSprite.position = {
		x: this.feedbackPosition[index].x + 14,
		y: this.feedbackPosition[index].y + 5 + newHeight
	};
	miniWordText.position = {
		x: this.feedbackPosition[index].x + 94,
		y: this.feedbackPosition[index].y + 10 + newHeight
	};

	miniLineSprite.position = {
		x: this.feedbackPosition[index].x - 11,
		y: this.feedbackPosition[index].y + 40 + newHeight
	};

	feedbackContainer.addChild(miniLineSprite);
	feedbackContainer.addChild(miniRectSprite);
	feedbackContainer.addChild(miniImageSprite);
	feedbackContainer.addChild(miniWordText);

	feedbackContainer.visible = false;

	this.hiddenFeedback.push(feedbackContainer);
	this.dragParentContainer.addChild(feedbackContainer);
}


/**
 * drag drop sprite
 * on correct drop, feedback is updated
 */
Toy_3_2.prototype.onDragging = function(i) {

	var self = this;

	// use the mousedown and touchstart
	this.dragButtons[i].mousedown = this.dragButtons[i].touchstart = function(data) {
		if (addedListeners) return false;

		data.originalEvent.preventDefault()
			// store a refference to the data
			// The reason for this is because of multitouch
			// we want to track the movement of this particular touch
		this.data = data;
		this.alpha = 0.9;
		this.dragging = true;
		this.sx = this.data.getLocalPosition(this).x;
		this.sy = this.data.getLocalPosition(this).y;
		this.bringToFront();
		this.soundClicked = false;
		this.spriteMoved = false;

		// no dragging on sound icon area
		if (isInsideRectangle(this.sx, this.sy, this.howlPosition.x, this.howlPosition.y, this.howlPosition.x + 83, this.howlPosition.y + 81)) {
			this.soundClicked = true;
			this.dragging = false;
		}

	}

	this.dragButtons[i].mouseup = this.dragButtons[i].mouseupoutside = this.dragButtons[i].touchend = this.dragButtons[i].touchendoutside = function(data) {

		if (addedListeners) return false;
		if (this.soundClicked) return false;
		var dragSelf = this;

		this.alpha = 1
		this.dragging = false;
		// set the interaction data to null
		this.data = null;

		self.redBox.position = {
			x: this.position.x,
			y: this.position.y
		};

		if (!this.spriteMoved) return;
		if (self.dropPositionPerIndex(this.dx, this.dy, this.dropIndex)) {
			this.interactive = false;
			correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
			this.greenBoxItem.visible = true;
			setTimeout(function() {
				self.correctFeedback(dragSelf.dropIndex, i);
				dragSelf.alpha = 0.2;
				dragSelf.position.set(0, 0);
				dragSelf.greenBoxItem.visible = false;
			}, 1000);
		} else {

			var drop0 = isInsideRectangle(this.dx, this.dy, 690, 60, 1120, 1140);

			if (drop0 === false) { //show wrong feedback sound and red box only if dragged to 1 of the 4 drop places.

				wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
				this.redBoxItem.visible = true;
				setTimeout(function() {
					// reset dragged element position
					dragSelf.position.set(0, 0);
					dragSelf.redBoxItem.visible = false;
				}, 500);
			} else {
				drop0 = false;
				dragSelf.position.set(0, 0);
				dragSelf.redBoxItem.visible = false;
			}
		}
	}

	// set the callbacks for when the mouse or a touch moves
	this.dragButtons[i].mousemove = this.dragButtons[i].touchmove = function(data) {
		if (addedListeners) return false;
		// set the callbacks for when the mouse or a touch moves
		if (this.dragging) {
			this.spriteMoved = true;
			// need to get parent coords..
			var newPosition = this.data.getLocalPosition(this.parent);
			this.position.x = newPosition.x - this.sx;
			this.position.y = newPosition.y - this.sy;
			this.dx = newPosition.x;
			this.dy = newPosition.y;
		}
	};

}

/**
 * check drag position is inside rectangular drop position
 * @param  {float} dx    drag positon x
 * @param  {float} dy    drag position y
 * @param  {integer} index
 * @return {boolean}       true/false
 */
Toy_3_2.prototype.dropPositionPerIndex = function(dx, dy, index) {

	var x1 = this.dropParams.position[index * 2],
		y1 = this.dropParams.position[index * 2 + 1],
		x2 = this.dropParams.position[index * 2] + this.dropParams.w,
		y2 = this.dropParams.position[index * 2 + 1] + this.dropParams.h;

	if (isInsideRectangle(dx, dy, x1, y1, x2, y2)) {
		// console.log('in');
		return true;
	} else {
		// console.log('out');
		return false;
	}
}

/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
Toy_3_2.prototype.correctFeedback = function(index, i) {

	var feedback = localStorage.getItem('toy_3_2');

	this.hiddenFeedback[i].visible = true;
	feedback++;
	localStorage.setItem('toy_3_2', feedback);

	if (feedback >= Toy_3_2.totalFeedback)
		setTimeout(this.onComplete.bind(this), 1000);
}

/**
 * prefetch exercise data and cache number of sets defined only.
 */
Toy_3_2.prototype.initObjectPooling = function() {

	var self = this;

	this.wallSlices = [];

	this.poolSlices = [];

	self.pool = {};

	var loader = new PIXI.JsonLoader('uploads/exercises/toystore/toystore_3_2.json?nocache=' + (new Date()).getTime());
	loader.on('loaded', function(evt) {
		//data is in loader.json
		self.pool = new ExerciseSpritesPool(loader.json);
		//borrow two sprites once
		self.borrowWallSprites(2);
		self.runExercise();

	});
	loader.load();
}

Toy_3_2.prototype.borrowWallSprites = function(num) {

	var self = this;

	this.num = num;

	for (var i = 0; i < this.num; i++) {

		var sprite = this.pool.borrowSprite();
		this.poolSlices.push(sprite);

		var imageArr = [],
			wordArr = [],
			groupArr = [],
			alphaSoundArrSmall = [],
			alphaSoundArrBig = [],
			soundArr = [];

		var spriteObj = sprite[0];


		for (var j = 0; j < spriteObj.image.length; j++) {

			var poolImageSprite = PIXI.Texture.fromImage('uploads/images/' + spriteObj.image[j]);
			imageArr.push(poolImageSprite);
			var howl = loadSound('uploads/audios/' + StripExtFromFile(spriteObj.sound[j]));
			soundArr.push(howl);
			wordArr.push(spriteObj.word[j]);
			groupArr.push(spriteObj.group[j]);

		};

		//NOW LETTER SOUNDS
		for (var k = 0; k < 4; k++) {
			var alphaHowl = loadSound('uploads/audios/alphabets/small/' + spriteObj.alphaaudio_small[k]);

			var alphaHowlBig = loadSound('uploads/audios/alphabets/capital/' + spriteObj.alphaaudio_big[k]);
			alphaSoundArrSmall.push(alphaHowl);
			alphaSoundArrBig.push(alphaHowlBig);
		}

		shuffle(imageArr, soundArr, wordArr, groupArr);

		this.wallSlices.push({
			word: wordArr,
			image: imageArr,
			sound: soundArr,
			group: groupArr,
			answer: ["Z", "Ä", "Ö", "Å"],
			alpha_sound_small: alphaSoundArrSmall,
			alpha_sound_big: alphaSoundArrBig
		});
	}
};

/**
 * return used sets to pool
 */
Toy_3_2.prototype.returnWallSprites = function() {
	var sprite = this.poolSlices[0];

	this.pool.returnSprite(sprite);

	this.poolSlices = [];
	this.wallSlices.shift();
};

/**
 * reset exercise and get new set
 */
Toy_3_2.prototype.restartExercise = function() {

	this.resetExercise();

	this.dragParentContainer.removeChildren();

	localStorage.setItem('toy_3_2', 0);

	this.returnWallSprites();
	this.borrowWallSprites(1);
	this.runExercise();

};

/**
 * the outro scene is displayed
 */
Toy_3_2.prototype.onComplete = function() {


	var self = this;

	this.introMode = false;
	this.outroContainer.visible = true;
	this.mouths.position.set(930, 573);

	this.ovaigen.click = this.ovaigen.tap = function(data) {
		self.cancelOutro();
		self.restartExercise();

	}

	this.outroAnimation();

}

/**
 * show red/green sprite on correct/wrong
 * @param  {int} xPos
 * @param  {int} yPos
 * @param  {object} obj
 */
Toy_3_2.prototype.showRedOrGreenBox = function(xPos, yPos, obj) {

	var self = this;

	obj.position = {
		x: xPos,
		y: yPos
	};

	obj.visible = true;

	setTimeout(function() {
		obj.visible = false;
	}, 400);


}

/**
 * clean memory by clearing reference of object
 */
Toy_3_2.prototype.cleanMemory = function() {

	this.introMode = null;

	if (this.assetsToLoad)
		this.assetsToLoad.length = 0;

	if (this.sceneBackground)
		this.sceneBackground.clear();

	this.sceneBackground = null;
	this.help = null;

	this.figure_intro = null;
	this.mouths = null;
	this.arms = null;
	this.introSound = null;

	this.figure_outro = null;
	this.ovaigen = null;
	this.figure_hand_outro = null;
	this.outroSound = null;

	this.miniRectTexture.destroy(true);
	this.smallRectTexture.destroy(true);
	this.bigRectTexture.destroy(true);
	this.miniLineTexture.destroy(true);

	this.miniRectTexture = null;
	this.smallRectTexture = null;
	this.bigRectTexture = null;
	this.miniLineTexture = null;
	this.redBox = null;

	if (this.smallRectPosition)
		this.smallRectPosition.length = 0;

	this.bigRect = null;
	this.dropParams = null;

	if (this.spritePosition)
		this.spritePosition.length = 0;

	this.feedbackPosition = null;

	if (this.jsonData)
		this.jsonData.length = 0;

	if (this.iconWord)
		this.iconWord.length = 0;

	if (this.dragButtons)
		this.dragButtons.length = 0;
	this.counter = null;

	if (this.hiddenFeedback)
		this.hiddenFeedback.length = 0;

	if (this.wallSlices)
		this.wallSlices.length = 0;

	if (this.poolSlices)
		this.poolSlices.length = 0;

	this.pool = null;
	this.num = null;

	this.introContainer = null;
	this.outroContainer = null;
	this.dragParentContainer = null;
	this.dragContainer = null;
	this.feedbackContainer = null;
	this.animationContainer = null;
	this.exerciseBaseContainer = null;

	this.sceneContainer.removeChildren();
	this.sceneContainer = null;
}