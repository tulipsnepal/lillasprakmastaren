function Toy_1_2(stage) {
    cl.show();

    PIXI.DisplayObjectContainer.call(this);
    main.CurrentExercise = "toy_1_2";
    this.baseContainer = new PIXI.DisplayObjectContainer();

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();
    this.blueFigureContainer = new PIXI.DisplayObjectContainer();
    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.feedbackContainer = new PIXI.SpriteBatch();
    this.animationContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position = {
        x: 77,
        y: 88
    };

    localStorage.setItem('toy_1_2', 0);

    this.baseContainer.addChild(this.sceneContainer);

    main.texturesToDestroy = [];

    this.loadSpriteSheet();

    // everything connected to stage is rendered.
    this.addChild(this.baseContainer);


}

Toy_1_2.constructor = Toy_1_2;
Toy_1_2.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Toy_1_2.imagePath = 'build/exercises/toystore/toy_1_2/images/';
Toy_1_2.audioPath = 'build/exercises/toystore/toy_1_2/audios/';
Toy_1_2.commonFolder = 'build/common/images/toystore/';
Toy_1_2.totalFeedback = Main.TOTAL_FEEDBACK || 10; // default 10

/**
 * preload images, sprites and audios
 */
Toy_1_2.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(Toy_1_2.audioPath + 'intro');
    this.outroId = loadSound(Toy_1_2.audioPath + 'outro');

    // create an array of assets to load
    var assetsToLoad = [
        Toy_1_2.imagePath + "sprite_toystore_1_2.json",
        Toy_1_2.imagePath + "sprite_toystore_1_2.png",
        Toy_1_2.imagePath + "sprite_toystore_1_2_outro.json",
        Toy_1_2.imagePath + "sprite_toystore_1_2_outro.png",
        Toy_1_2.imagePath + "box.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * on all assets loaded, load exercise and json related.
 */
Toy_1_2.prototype.spriteSheetLoaded = function() {
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);



    this.loadDefaults();
    this.renderFeedBacks();

    this.initObjectPooling();

    this.sceneContainer.addChild(this.feedbackContainer);
    this.introContainer.addChild(this.blueFigureContainer);
    this.introContainer.addChild(this.exerciseContainer);
    this.introContainer.addChild(this.animationContainer);
}

/**
 * set 20% visibility to all feedback sprites
 */
Toy_1_2.prototype.showHideAllFeedBacks = function() {

    for (var j = 0; j < this.feedbackContainer.children.length; j++) {
        this.feedbackContainer.children[j].alpha = 0.2;
    }
}

/**
 * generate all feedback sprites
 */
Toy_1_2.prototype.renderFeedBacks = function() {
    var self = this;
    for (var k = 0; k < 10; k++) {
        var feedbackSprite = PIXI.Sprite.fromFrame("feedback.png");

        feedbackSprite.position = {
            x: 1634,
            y: 983 - (k * 100)
        };
        main.texturesToDestroy.push(feedbackSprite);
        self.feedbackContainer.addChild(feedbackSprite);
        feedbackSprite.alpha = 0.2;
    }
}

/**
 * draw background, create sprites and movie clip
 */
Toy_1_2.prototype.loadDefaults = function() {

    var self = this;


    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0xf9e5e4);
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xd6e4ae);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);



    this.musicBox = PIXI.Sprite.fromImage(Toy_1_2.imagePath + "box.png");
    this.musicBox.position = {
        x: 414,
        y: 2
    };

    main.texturesToDestroy.push(this.musicBox);
    this.sceneContainer.addChild(this.musicBox);

    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.outroContainer);
    this.outroContainer.visible = false;

    this.blueFigureStand = PIXI.Sprite.fromFrame("blue_figure_spring.png");
    this.blueFigureStand.position = {
        x: 650,
        y: 541
    };

    main.texturesToDestroy.push(this.blueFigureStand);
    this.introContainer.addChild(this.blueFigureStand);
    this.blueFigureStand.visible = false;


    //Neutral blue figure
    this.blueFigureNeutral = PIXI.Sprite.fromFrame("blue_figure_0.png");
    this.blueFigureNeutral.position = {
        x: 620,
        y: 115
    };
    main.texturesToDestroy.push(this.blueFigureNeutral);
    this.blueFigureContainer.addChild(this.blueFigureNeutral);

    //Sad Blue Figure
    this.blueFigureSad = PIXI.Sprite.fromFrame("blue_figure_2.png");
    this.blueFigureSad.position = {
        x: 620,
        y: 115
    };
    main.texturesToDestroy.push(this.blueFigureSad);
    this.blueFigureContainer.addChild(this.blueFigureSad);
    this.blueFigureSad.visible = false;

    //Speaking Blue Figure
    this.blueFigureSpeaking = PIXI.Sprite.fromFrame("blue_figure_1.png");
    this.blueFigureSpeaking.position = {
        x: 620,
        y: 115
    };
    main.texturesToDestroy.push(this.blueFigureSpeaking);
    this.blueFigureContainer.addChild(this.blueFigureSpeaking);
    this.blueFigureSpeaking.visible = false;


    //Happy Blue Figure
    this.blueFigureHappy = PIXI.Sprite.fromFrame("blue_figure_happy.png");
    this.blueFigureHappy.position = {
        x: 624,
        y: 116
    };
    main.texturesToDestroy.push(this.blueFigureHappy);
    this.blueFigureContainer.addChild(this.blueFigureHappy);
    this.blueFigureHappy.visible = false;

    this.yellowBody = PIXI.Sprite.fromFrame("yellow_body.png");
    this.yellowBody.position = {
        x: 813,
        y: 213 - 50
    };
    main.texturesToDestroy.push(this.yellowBody);
    this.introContainer.addChild(this.yellowBody);
    this.yellowBody.visible = false;

    //Neutral Yellow Figure
    this.yellowFigureNeutral = PIXI.Sprite.fromFrame("yellow_figure_0.png");
    this.yellowFigureNeutral.position = {
        x: 813,
        y: 116
    };
    main.texturesToDestroy.push(this.yellowFigureNeutral);
    this.introContainer.addChild(this.yellowFigureNeutral);
    // this.yellowFigureNeutral.visible = false;

    //Sad Yellow Figure
    this.yellowFigureSad = PIXI.Sprite.fromFrame("yellow_figure_2.png");
    this.yellowFigureSad.position = {
        x: 813,
        y: 116
    };
    main.texturesToDestroy.push(this.yellowFigureSad);
    this.introContainer.addChild(this.yellowFigureSad);
    this.yellowFigureSad.visible = false;

    //Speaking Yellow Figure
    this.yellowFigureSpeaking = PIXI.Sprite.fromFrame("yellow_figure_1.png");
    this.yellowFigureSpeaking.position = {
        x: 813,
        y: 116
    };
    main.texturesToDestroy.push(this.yellowFigureSpeaking);
    this.introContainer.addChild(this.yellowFigureSpeaking);
    this.yellowFigureSpeaking.visible = false;


    //Happy Yellow Figure
    this.yellowFigureHappy = PIXI.Sprite.fromFrame("yellow_figure_happy.png");
    this.yellowFigureHappy.position = {
        x: 813,
        y: 116
    };
    main.texturesToDestroy.push(this.yellowFigureHappy);
    this.introContainer.addChild(this.yellowFigureHappy);
    this.yellowFigureHappy.visible = false;

    //Neutral Pink Figure
    this.pinkFigureNeutral = PIXI.Sprite.fromFrame("pink_figure_0.png");
    this.pinkFigureNeutral.position = {
        x: 1109,
        y: 177
    };
    main.texturesToDestroy.push(this.pinkFigureNeutral);
    this.introContainer.addChild(this.pinkFigureNeutral);

    //Sad Pink Figure
    this.pinkFigureSad = PIXI.Sprite.fromFrame("pink_figure_2.png");
    this.pinkFigureSad.position = {
        x: 1109,
        y: 177
    };
    main.texturesToDestroy.push(this.pinkFigureSad);
    this.introContainer.addChild(this.pinkFigureSad);
    this.pinkFigureSad.visible = false;

    //Speaking Pink Figure
    this.pinkFigureSpeaking = PIXI.Sprite.fromFrame("pink_figure_1.png");
    this.pinkFigureSpeaking.position = {
        x: 1109,
        y: 177
    };
    main.texturesToDestroy.push(this.pinkFigureSpeaking);
    this.introContainer.addChild(this.pinkFigureSpeaking);
    this.pinkFigureSpeaking.visible = false;


    //Happy Pink Figure
    this.pinkFigureHappy = PIXI.Sprite.fromFrame("pink_figure_happy.png");
    this.pinkFigureHappy.position = {
        x: 1109,
        y: 177
    };
    main.texturesToDestroy.push(this.pinkFigureHappy);
    this.introContainer.addChild(this.pinkFigureHappy);
    this.pinkFigureHappy.visible = false;


    //CREATE ALPHABET RECTANGLE
    this.box = createRect({
        x: 162,
        y: 417,
        w: 314,
        h: 314,
        lineStyle: 1
    });
    this.introContainer.addChild(this.box);

    this.animateIntroFigure = [
        new PIXI.Sprite.fromFrame('arms_1.png'),
        new PIXI.Sprite.fromFrame('arms_2.png'),
        new PIXI.Sprite.fromFrame('arms_3.png'),
    ];
    this.armsPosition = [
        817, 300,
        817, 330,
        817, 340,
    ];
    for (var i = 0; i < this.animateIntroFigure.length; i++) {
        this.animateIntroFigure[i].position.set(this.armsPosition[i * 2], this.armsPosition[i * 2 + 1]);
        this.animationContainer.addChild(this.animateIntroFigure[i]);
        this.animateIntroFigure[i].visible = false;
    }

    this.mouthParams = {
        "image": "mouth_yellow_",
        "length": 2,
        "x": 930,
        "y": 272,
        "speed": 6,
        "pattern": true,
        "frameId": 1
    };
    this.mouth = new Animation(this.mouthParams);
    this.introContainer.addChild(this.mouth);
    this.mouth.gotoAndStop(1);
    this.mouth.visible = false;

    this.help = PIXI.Sprite.fromFrame("topcorner.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position = {
        x: -77,
        y: -88
    }
    this.sceneContainer.addChild(this.help);

    //OUTRO STUFF

    this.outroBlueFigureParams = {
        "image": "blue_figure_outro_",
        "length": 3,
        "x": 548,
        "y": 109,
        "speed": 3,
        "pattern": false,
        "frameId": 3
    };
    this.animateBlueOutroFigure = new Animation(this.outroBlueFigureParams);
    this.animateBlueOutroFigure.visible = false;

    this.outroContainer.addChild(this.animateBlueOutroFigure);

    this.outroYellowFigureParams = {
        "image": "yellow_figure_outro_",
        "length": 3,
        "x": 813,
        "y": 83,
        "speed": 2,
        "pattern": false,
        "frameId": 3
    };
    this.animateYellowOutroFigure = new Animation(this.outroYellowFigureParams);
    this.animateYellowOutroFigure.visible = false;

    this.outroContainer.addChild(this.animateYellowOutroFigure);


    this.outroPinkFigureParams = {
        "image": "pink_figure_outro_",
        "length": 3,
        "x": 1110,
        "y": 141,
        "speed": 4,
        "pattern": false,
        "frameId": 3
    };
    this.animatePinkOutroFigure = new Animation(this.outroPinkFigureParams);
    this.animatePinkOutroFigure.visible = false;

    this.outroContainer.addChild(this.animatePinkOutroFigure);

    this.ovaigen = PIXI.Sprite.fromFrame("ovaigen.png");
    this.ovaigen.buttonMode = true;
    this.ovaigen.interactive = true;
    this.ovaigen.position = {
        x: 800,
        y: 802
    };
    this.outroContainer.addChild(this.ovaigen);
    this.ovaigen.visible = false;

    //OUTRO STUFF END

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.updateFeedback();

}

/**
 * show specific figure
 * @param  {int} number
 */
Toy_1_2.prototype.showFigureAt = function(number) {
    for (var i = 0; i < this.animateIntroFigure.length; i++) {
        this.animateIntroFigure[i].visible = false;
    }
    if (isNumeric(number)) {
        this.animateIntroFigure[number].visible = true;
    }
};

/**
 * play intro animation
 */
Toy_1_2.prototype.introAnimation = function() {


    clearTimeout(this.figureOneSpeaks);
    clearTimeout(this.figureTwoSpeaks);
    clearTimeout(this.figureThreeSpeaks);

    createjs.Sound.stop();

    if (!this.introMode)
        this.restartExercise();

    this.help.interactive = false;
    this.help.buttonMode = false;

    main.overlay.visible = true;

    this.feedbackContainer.visible = true;

    this.yellowFigureNeutral.visible = false;
    this.yellowBody.visible = true;

    this.yellowBody.bringToFront();
    this.animationContainer.bringToFront();
    this.mouth.visible = true;
    this.mouth.show();
    this.mouth.bringToFront();

    for (var k = 0; k < this.soundButtons.length; k++) {
        this.soundButtons[k].interactive = false;
    }

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
0-2.5 SEC ARMS_2
2.5 - 3.5 SEC ARMS_3
3.5-6 SEC ARMS_1
6-9 SEC ARMS_3
 */
Toy_1_2.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    if (currentPostion >= 2500 && currentPostion < 3500) {
        this.showFigureAt(2);
    } else if (currentPostion >= 3500 && currentPostion < 6000) {
        this.showFigureAt(0);
    } else if (currentPostion >= 6000 && currentPostion < 9000) {
        this.showFigureAt(2);
    } else {
        this.showFigureAt(1);
    }
};

/**
 * cancel running intro animation
 */
Toy_1_2.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);
    this.showFigureAt();

    this.yellowBody.visible = false;
    this.yellowFigureNeutral.visible = true;

    this.mouth.hide(0);
    this.mouth.visible = false;

    this.blueFigureStand.visible = false;
    this.help.interactive = true;
    this.help.buttonMode = true;

    this.enableSpriteInteraction();

    this.startFigureSpeak();

    this.ovaigen.visible = false;
    main.overlay.visible = false;
};

/**
 * enable sprites like sound and images buttons
 */
Toy_1_2.prototype.enableSpriteInteraction = function() {
    this.soundButtons[0].interactive = true;
    this.soundButtons[1].interactive = true;
    this.soundButtons[2].interactive = true;

    this.imageButtons[0].interactive = true;
    this.imageButtons[1].interactive = true;
    this.imageButtons[2].interactive = true;
};

/**
 * disable sprites like sounds and images from interaction
 */
Toy_1_2.prototype.disableSpriteInteraction = function() {
    this.soundButtons[0].interactive = false;
    this.soundButtons[1].interactive = false;
    this.soundButtons[2].interactive = false;

    this.imageButtons[0].interactive = false;
    this.imageButtons[1].interactive = false;
    this.imageButtons[2].interactive = false;
};

/**
 * play outro animation when outro scene is loaded
 */
Toy_1_2.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.help.interactive = false;
        self.help.buttonMode = false;
        self.animateBlueOutroFigure.show(true);
        self.animateYellowOutroFigure.show(true);
        self.animatePinkOutroFigure.show(true);
    }
    this.outroCancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.outroCancelHandler);

};

/**
 * cancel running outro animation
 */
Toy_1_2.prototype.cancelOutro = function() {

    createjs.Sound.stop();
    this.animateBlueOutroFigure.hide(0);
    this.animateYellowOutroFigure.hide(0);
    this.animatePinkOutroFigure.hide(0);
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.ovaigen.visible = true;
    main.overlay.visible = false;

};

/**
 * exercise logic
 */
Toy_1_2.prototype.runExercise = function() {
    var self = this;

    var jsonData = this.wallSlices[0];
    //render alphabets in the box start
    var optionAlphabet = new PIXI.Text(jsonData.answerAlphabet, {
        font: "150px Arial",
        fill: "black",
        align: "center",
    });

    optionAlphabet.position = {
        x: 162 + (314 - optionAlphabet.width) / 2,
        y: 417 + (314 - 170) / 2
    };

    main.texturesToDestroy.push(optionAlphabet);
    self.exerciseContainer.addChild(optionAlphabet);
    //end

    var happyFigures = [self.blueFigureHappy, self.yellowFigureHappy, self.pinkFigureHappy];
    var sadFigures = [self.blueFigureSad, self.yellowFigureSad, self.pinkFigureSad];

    self.speakingFigures = [self.blueFigureSpeaking, self.yellowFigureSpeaking, self.pinkFigureSpeaking];

    var neutralFigure = [self.blueFigureNeutral, self.yellowFigureNeutral, self.pinkFigureNeutral];


    var figures = ["blue_figure_0.png", "yellow_figure_0.png", "pink_figure_0.png"]
    var figurePosition = [
        620, 115,
        813, 116,
        1109, 177
    ];

    this.imageButtons = [];

    this.soundTexture = new PIXI.Texture.fromFrame("sound_grey.png");
    this.imageSprite = new PIXI.Texture.fromFrame("interactive.png");

    this.whiteGraphics = createRect({
        w: 60,
        h: 60,
        color: 0xffffff,
        lineStyle: 1
    });


    this.whiteRect = this.whiteGraphics.generateTexture();
    this.soundButtons = [];
    for (var k = 0; k < 3; k++) {
        var imageSprite = new PIXI.Sprite(this.imageSprite);
        imageSprite.interactive = true;
        imageSprite.buttonMode = true;
        imageSprite.position = {
            x: figurePosition[k * 2],
            y: figurePosition[k * 2 + 1]
        };

        imageSprite.hitIndex = k;
        imageSprite.selectedSound = jsonData.audioName[k];

        self.exerciseContainer.addChild(imageSprite);


        self.imageButtons.push(imageSprite);

        var sound = new PIXI.Sprite(this.soundTexture);

        var drawWhiteBox = new PIXI.Sprite(this.whiteRect);
        drawWhiteBox.position = {
            x: (k == 0) ? 680 : ((k == 1) ? 935 : 1191),
            y: 689
        };
        self.exerciseContainer.addChild(drawWhiteBox);

        drawWhiteBox.buttonMode = true;
        drawWhiteBox.interactive = true;
        sound.position = {
            x: -6,
            y: 6
        };

        drawWhiteBox.itemIndex = k;
        drawWhiteBox.addChild(sound);
        self.soundButtons.push(drawWhiteBox);
        drawWhiteBox.howl = jsonData.sound[k];

        drawWhiteBox.click = drawWhiteBox.tap = function(data) {
            createjs.Sound.play(this.howl);
            self.showFeedBackImages(self.speakingFigures[this.itemIndex], this);
        }

    }

    this.startFigureSpeak();

    self.imageButtons[0].click = self.imageButtons[0].tap = self.imageButtons[1].click = self.imageButtons[1].tap = self.imageButtons[2].click = self.imageButtons[2].tap = function(data) {

        if (addedListeners) return false;
        addedListeners = true;

        createjs.Sound.stop();

        clearTimeout(self.figureOneSpeaks);
        clearTimeout(self.figureTwoSpeaks);
        clearTimeout(self.figureThreeSpeaks);

        var hitObj = this;
        if (hitObj.selectedSound == jsonData.answerAlphabet) { //Correnct Answer
            correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
            self.showFeedBackImages(happyFigures[hitObj.hitIndex], neutralFigure[hitObj.hitIndex]);
            setTimeout(self.correctFeedback.bind(self), 400);

        } else { //wrong answer
            wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
            self.showFeedBackImages(sadFigures[hitObj.hitIndex], neutralFigure[hitObj.hitIndex]);
            setTimeout(self.wrongFeedback.bind(self), 400);
        }
    }
};

Toy_1_2.prototype.startFigureSpeak = function() {

    var self = this;

    var soundData = this.wallSlices[0];


    this.figureOneSpeaks = setTimeout(function() {
        if (addedListeners) return;

        createjs.Sound.play(soundData.sound[0]);
        self.showFeedBackImages(self.speakingFigures[0]);

    }, 2000);

    this.figureTwoSpeaks = setTimeout(function() {
        if (addedListeners) return;

        createjs.Sound.play(soundData.sound[1]);
        self.showFeedBackImages(self.speakingFigures[1]);

    }, 4000);

    this.figureThreeSpeaks = setTimeout(function() {

        if (addedListeners) return;

        createjs.Sound.play(soundData.sound[2]);

        if (createjs.Sound.isReady()) {
            self.showFeedBackImages(self.speakingFigures[2]);
        };
    }, 6000);
}

/**
 * on correct answer, feedback increment by 1
 * get next set on correct answer
 */
Toy_1_2.prototype.correctFeedback = function() {
    var self = this;
    var feedback = localStorage.getItem('toy_1_2');
    feedback++;
    localStorage.setItem('toy_1_2', feedback);
    self.updateFeedback();
    if (feedback >= Toy_1_2.totalFeedback) {
        setTimeout(function() {
            self.onComplete(); // go to outro scene
        }, 800);

    } else {

        self.exerciseContainer.removeChildren();
        self.returnWallSprites();
        self.borrowWallSprites(1);
        self.runExercise();
    }
    addedListeners = false;
};

/**
 * on wrong answer, decrement feedback by 1
 */
Toy_1_2.prototype.wrongFeedback = function() {
    var self = this;
    var feedback = localStorage.getItem('toy_1_2');
    feedback--;

    if (feedback < 0)
        feedback = 0;

    localStorage.setItem('toy_1_2', feedback);

    self.updateFeedback();
    addedListeners = false;
};

/**
 * load outro scene
 */
Toy_1_2.prototype.onComplete = function() {

    var self = this;
    this.introMode = false;
    this.outroContainer.visible = true;
    this.introContainer.visible = false;

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        self.cancelOutro();
        self.restartExercise();
    }

    this.outroAnimation();
}

/**
 * reset exercise to default values
 */
Toy_1_2.prototype.resetExercise = function() {
    var self = this;
    this.ovaigen.visible = false;
    localStorage.setItem('toy_1_2', 0);

    self.introContainer.visible = true;
    self.outroContainer.visible = false;
    self.feedbackContainer.visible = true;
    this.introMode = true;

};

/**
 * update feedback on correct or wrong answer check
 */
Toy_1_2.prototype.updateFeedback = function() {

    var self = this;

    var counter = localStorage.getItem('toy_1_2');

    self.showHideAllFeedBacks();

    for (var j = 0; j < counter; j++) {
        self.feedbackContainer.children[j].alpha = 1;

    }
}


/**
 * object pooling implementation to get 2 sets for exercise from the exercise pool
 */
Toy_1_2.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/toystore/toystore_1_2.json?noache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        /*console.log(loader.json);
        return false;*/
        self.pool = new ExerciseSpritesPool(loader.json);
        //borrow two sprites once
        self.borrowWallSprites(2);
        self.runExercise();

    });
    loader.load();
}

/**
 * borrow sprites and cache for smoother game play
 * audios,images are cached
 * @param  {int} num
 */
Toy_1_2.prototype.borrowWallSprites = function(num) {

    var self = this;
    this.num = num;

    for (var i = 0; i < this.num; i++) {
        var sprite = this.pool.borrowSprite();
        this.poolSlices.push(sprite);
        var audioNameArray = [];
        var audiosArray = [];
        var spriteObj = sprite[0];


        for (var j = 0; j < 3; j++) {
            audioNameArray.push(spriteObj.audio_name[j]);
            var howl = loadSound('uploads/audios/alphabets/capital/' + StripExtFromFile(spriteObj.audios[j]));
            audiosArray.push(howl);
        }



        //shuffle(audioNameArray, audiosArray);

        this.wallSlices.push({
            answerAlphabet: spriteObj.answer.alphabet,
            sound: audiosArray,
            audioName: audioNameArray,
        });
    }
};


/**
 * return used object to pool so we can use later if all sets are used in pool
 */
Toy_1_2.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];

    this.pool.returnSprite(sprite);

    this.exerciseContainer.removeChildren();

    this.poolSlices = [];
    this.wallSlices.shift();
};

/**
 * reset exercise and borrow next set
 */
Toy_1_2.prototype.restartExercise = function() {
    var self = this;

    this.resetExercise();

    localStorage.setItem('toy_1_2', 0);
    self.showHideAllFeedBacks();
    this.exerciseContainer.removeChildren();
    this.returnWallSprites();
    this.borrowWallSprites(1);
    this.runExercise();
};

/**
 * unload or destroy unused variable from memory to manage memory
 */
Toy_1_2.prototype.cleanMemory = function() {

    clearTimeout(this.figureOneSpeaks);
    clearTimeout(this.figureTwoSpeaks);
    clearTimeout(this.figureThreeSpeaks);

    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    this.person = null;

    this.animateBlueOutroFigure = null;

    this.help = null;
    this.ovaigen = null;

    if (this.poolSlices)
        this.poolSlices.length = 0;

    if (this.wallSlices)
        this.wallSlices.length = 0;

    this.pool = null;

    this.sceneContainer = null;
    this.introContainer = null;
    this.outroContainer = null;
    this.feedbackContainer = null;

    this.blueFigureContainer = null;
    this.exerciseContainer = null;
    this.animationContainer = null;

    this.musicBox = null;
    this.blueFigureStand = null;
    this.blueFigureNeutral = null;
    this.blueFigureSad = null;
    this.blueFigureSpeaking = null;
    this.blueFigureHappy = null;
    this.yellowFigureNeutral = null;
    this.yellowFigureSad = null;
    this.yellowFigureSpeaking = null;
    this.yellowFigureHappy = null;
    this.pinkFigureNeutral = null;
    this.pinkFigureSad = null;
    this.pinkFigureSpeaking = null;
    this.pinkFigureHappy = null;

    if (this.box)
        this.box.clear();

    if (this.outroBlueFigureParams)
        this.outroBlueFigureParams.length = 0;

    if (this.outroYellowFigureParams)
        this.outroYellowFigureParams.length = 0;

    if (this.outroPinkFigureParams)
        this.outroPinkFigureParams.length = 0;

    this.animateYellowOutroFigure = null;
    this.animatePinkOutroFigure = null;
    this.soundTexture = null;
    this.imageSprite = null;

    this.baseContainer.removeChildren();
    this.baseContainer = null;
}

/**
 * show feedback images for certain interval
 * @param  {object} objToShow
 * @param  {object} objToShowLater
 */
Toy_1_2.prototype.showFeedBackImages = function(objToShow, objToShowLater) {
    var self = this;

    objToShow.visible = true;
    //objToShow.bringToFront();
    if (objToShowLater) objToShowLater.visible = false;
    setTimeout(function() {
        objToShow.visible = false;
        if (objToShowLater) objToShowLater.visible = true;
    }, 600);

}