function Toy_4_2() {

	cl.show();

	PIXI.DisplayObjectContainer.call(this);

	this.sceneContainer = new PIXI.DisplayObjectContainer();
	this.sceneContainer.position.set(77, 88);

	this.introContainer = new PIXI.DisplayObjectContainer();
	this.outroContainer = new PIXI.DisplayObjectContainer();

	localStorage.setItem('toy_4_2', 0);

	main.CurrentExercise = "toy_4_2";

	main.eventRunning = false;

	main.texturesToDestroy = [];

	this.loadGameJson();

	// everything connected to this is rendered.
	this.addChild(this.sceneContainer);
}

Toy_4_2.constructor = Toy_4_2;
Toy_4_2.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Toy_4_2.imagePath = 'build/exercises/toystore/toy_4_2/images/';
Toy_4_2.audioPath = 'build/exercises/toystore/toy_4_2/audios/';
Toy_4_2.hitAlpa = 0;

/**
 * load array of assets of different format
 */
Toy_4_2.prototype.loadSpriteSheet = function() {

	this.introId = loadSound(Toy_4_2.audioPath + 'intro');
	this.outroId = loadSound(Toy_4_2.audioPath + 'outro');

	var self = this;

	// create an array of assets to load
	this.assetsToLoad = [
		Toy_4_2.imagePath + "sprite_4_2.json",
		Toy_4_2.imagePath + "sprite_4_2.png",
	];
	// create a new loader
	loader = new PIXI.AssetLoader(this.assetsToLoad);
	// use callback
	loader.onComplete = this.spriteSheetLoaded.bind(this);
	//begin load
	loader.load();

}

/**
 * load game json before assets loaded
 */
Toy_4_2.prototype.loadGameJson = function() {
	var self = this;
	// create empty object to load dot to dot game json data
	this.dotTodot = {};

	// load dot to dot json and store it in object.
	var jsonLoader = new PIXI.JsonLoader(Toy_4_2.imagePath + 'dot_to_dot.json');

	jsonLoader.on('loaded', function(evt) {
		//data is in loader.json
		// console.log(loader.json);
		self.dotTodot = jsonLoader.json;
		self.loadSpriteSheet();

	});
	jsonLoader.load();
}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
Toy_4_2.prototype.spriteSheetLoaded = function() {

	// hide loader
	cl.hide();

	if (this.sceneContainer.stage)
		this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

	// fill exercise background
	this.sceneBackground = new PIXI.Graphics();
	this.sceneBackground.beginFill(0xd4bbd0);
	// set the line style to have a width of 5 and set the color to red
	this.sceneBackground.lineStyle(3, 0xd7e5ae);
	// draw a rectangle
	this.sceneBackground.drawRect(0, 0, 1843, 1202);
	this.sceneContainer.addChild(this.sceneBackground);

	// exercise instruction
	this.help = PIXI.Sprite.fromFrame("topcorner.png");
	this.help.interactive = true;
	this.help.buttonMode = true;
	this.help.position.set(-77, -88);
	this.sceneContainer.addChild(this.help);

	this.help.click = this.help.tap = this.introAnimation.bind(this);

	this.loadExercise();

	this.sceneContainer.addChild(this.introContainer);
	this.introScene();

	this.sceneContainer.addChild(this.outroContainer);
	this.outroScene();

}

/**
 * create sprites,animation,sound related to intro scene
 */
Toy_4_2.prototype.introScene = function() {

	var self = this;

	this.figure_corner = new PIXI.Sprite.fromFrame('figure_corner.png');
	this.figure_corner.position.set(63, 621);
	main.texturesToDestroy.push(this.figure_corner);
	this.introContainer.addChild(this.figure_corner);

	this.animationContainer = new PIXI.DisplayObjectContainer();
	this.introContainer.addChild(this.animationContainer);

	this.animated_figure = new PIXI.Sprite.fromFrame('figure_intro.png');
	this.animated_figure.position.set(217, 176);
	main.texturesToDestroy.push(this.animated_figure);

	this.mouths = new Animation({
		"image": "mouth_",
		"length": 5,
		"x": 497,
		"y": 455,
		"speed": 6,
		"pattern": true,
		"frameId": 1
	});

	this.hands = [
		new PIXI.Sprite.fromFrame('arm_1.png'),
		new PIXI.Sprite.fromFrame('arm_3.png'),
		new PIXI.Sprite.fromFrame('arm_2.png'),
	];
	for (var i = 0; i < this.hands.length; i++) {
		this.hands[i].position.set(578, 529);
		this.animationContainer.addChild(this.hands[i]);
		this.hands[i].visible = false;
	}

	this.animationContainer.addChild(this.animated_figure);
	this.animationContainer.addChild(this.mouths);

	this.animationContainer.x = -140;

	this.animationContainer.visible = false;

}

/**
 * play intro animation
 */
Toy_4_2.prototype.introAnimation = function() {

	createjs.Sound.stop();

	if (!this.introMode)
		this.resetExercise();

	this.help.interactive = false;
	this.help.buttonMode = false;

	main.overlay.visible = true;

	this.figure_corner.visible = false;
	this.animationContainer.visible = true;

	this.mouths.show();
	this.showHandAt(0);

	this.introInstance = createjs.Sound.play(this.introId, {
		interrupt: createjs.Sound.INTERRUPT_ANY
	});

	if (createjs.Sound.isReady())
		this.timerId = setInterval(this.animateIntro.bind(this), 1);

	this.introCancelHandler = this.cancelIntro.bind(this);
	this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
* handle mouth, hand with specific sound positions or queue points.
START AT ARM_SIDE  => arm_0
0.5-2 SEC ARM_2 => arm_1
2-4 SEC ARM_1 => arm_2
4-9 SEC ARM_SIDE
9-12 SEC ARM_1
12-STOP ARM_SIDE
 */
Toy_4_2.prototype.animateIntro = function() {

	var currentPostion = this.introInstance.getPosition();

	if (currentPostion >= 500 && currentPostion < 2000) {
		this.showHandAt(1);
	} else if (currentPostion >= 2000 && currentPostion < 4000) {
		this.showHandAt(2);
	} else if (currentPostion >= 4000 && currentPostion < 9000) {
		this.showHandAt(0);
	} else if (currentPostion >= 9000 && currentPostion < 12000) {
		this.showHandAt(2);
	} else {
		this.showHandAt(0);
	}
};

/**
 * cancel running intro animation
 */
Toy_4_2.prototype.cancelIntro = function() {

	createjs.Sound.stop();

	clearInterval(this.timerId);

	this.mouths.hide(3);

	this.figure_corner.visible = true;
	this.animationContainer.visible = false;

	this.help.interactive = true;
	this.help.buttonMode = true;

	this.ovaigen.visible = false;
	main.overlay.visible = false;
}

/**
 * hand visibilty
 * @param  {int} number
 */
Toy_4_2.prototype.showHandAt = function(number) {
	for (var i = 0; i < this.hands.length; i++) {
		this.hands[i].visible = false;
	}
	if (isNumeric(number))
		this.hands[number].visible = true;
};

/**
 * play outro animation
 */
Toy_4_2.prototype.outroAnimation = function() {
	var self = this;

	var outroInstance = createjs.Sound.play(this.outroId);
	if (createjs.Sound.isReady()) {
		main.overlay.visible = true;
		self.help.interactive = false;
		self.help.buttonMode = false;

		self.mouths.show();
	}
	this.cancelHandler = this.cancelOutro.bind(this);
	outroInstance.addEventListener("complete", this.cancelHandler);

};

/**
 * cancel running outro animation
 */
Toy_4_2.prototype.cancelOutro = function() {

	var self = this;

	createjs.Sound.stop();
	self.mouths.hide(3);

	self.help.interactive = true;
	self.help.buttonMode = true;

	self.ovaigen.visible = true;
	main.overlay.visible = false;

};

/**
 * create sprites,animation,sound related to outro scene
 */
Toy_4_2.prototype.outroScene = function() {

	var self = this;

	this.outroContainer.visible = false;

	// back to intro page
	this.ovaigen = PIXI.Sprite.fromFrame('ovaigen.png');
	this.ovaigen.position.set(124, 859);
	this.ovaigen.interactive = true;
	this.ovaigen.buttonMode = true;
	this.ovaigen.visible = false;
	this.outroContainer.addChild(this.ovaigen);

}


/**
 * load exercise related stuffs
 */
Toy_4_2.prototype.loadExercise = function() {

	// create texture for rounded alphabet box
	this.alphabetBoxTexture = new PIXI.Texture.fromFrame("selectedbigsmall.png");

	this.upperAlphabetBox = new PIXI.Sprite(this.alphabetBoxTexture);
	this.upperAlphabetBox.position.set(105, 458);
	this.upperAlphabetBox.interactive = true;
	this.upperAlphabetBox.buttonMode = true;
	main.texturesToDestroy.push(this.upperAlphabetBox);

	this.lowerAlphabetBox = new PIXI.Sprite(this.alphabetBoxTexture);
	this.lowerAlphabetBox.position.set(105 + 131, 458);
	this.lowerAlphabetBox.interactive = true;
	this.lowerAlphabetBox.buttonMode = true;
	main.texturesToDestroy.push(this.lowerAlphabetBox);

	this.selectUpperCase = new PIXI.Text('A', {
		font: "50px Arial"
	});
	this.selectUpperCase.position.set(141, 484);
	main.texturesToDestroy.push(this.selectUpperCase);

	this.selectLowerCase = new PIXI.Text('a', {
		font: "50px Arial"
	});
	this.selectLowerCase.position.set(276, 489);
	main.texturesToDestroy.push(this.selectLowerCase);

	this.sceneContainer.addChild(this.upperAlphabetBox);
	this.sceneContainer.addChild(this.selectUpperCase);
	this.sceneContainer.addChild(this.lowerAlphabetBox);
	this.sceneContainer.addChild(this.selectLowerCase);

	// create new container to store all puzzle sprites
	this.exerciseContainer = new PIXI.DisplayObjectContainer();
	this.introContainer.addChild(this.exerciseContainer);

	this.upperAlphabetBox.click = this.upperAlphabetBox.tap = this.bigLetterVisible.bind(this);

	this.lowerAlphabetBox.click = this.lowerAlphabetBox.tap = this.smallLetterVisible.bind(this);

	this.bigLetterChange();

	this.getRandomDotToDot();

}


/**
 * show big letter
 */
Toy_4_2.prototype.bigLetterChange = function() {
	this.letterType = 'A';
	this.selectLowerCase.alpha = this.lowerAlphabetBox.alpha = 0.4;
	this.selectUpperCase.alpha = this.upperAlphabetBox.alpha = 1;

	this.hitLetters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'Å', 'Ä', 'Ö'];

}

/**
 * make big letters visible
 */
Toy_4_2.prototype.bigLetterVisible = function() {
	if (addedListeners) return false;
	this.bigLetterChange();
	this.DOT_TO_DOT();
}

/**
 * make small letters visible
 */
Toy_4_2.prototype.smallLetterVisible = function() {
	if (addedListeners) return false;
	this.letterType = 'a';
	this.selectLowerCase.alpha = this.lowerAlphabetBox.alpha = 1;
	this.selectUpperCase.alpha = this.upperAlphabetBox.alpha = 0.4;

	this.hitLetters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'å', 'ä', 'ö'];

	this.DOT_TO_DOT();
}

/**
 * get random game to play
 */
Toy_4_2.prototype.getRandomDotToDot = function() {

	localStorage.setItem('toy_4_2', 0);

	var rand = makeUniqueRandom(5);

	this.frame = this.dotTodot[rand];

	// create array of game related json and image file.
	this.gameAssetsToLoad = [
		Toy_4_2.imagePath + this.frame.name + ".json",
		Toy_4_2.imagePath + this.frame.name + ".png",
	];

	if (this.frame.name === 'snail') {
		this.gameAssetsToLoad.push(Toy_4_2.imagePath + "snail/snail_1.png");
		this.gameAssetsToLoad.push(
			Toy_4_2.imagePath + "snail/snail_dots.png");
	}

	// create a new loader
	var gameLoader = new PIXI.AssetLoader(this.gameAssetsToLoad);
	// use callback
	gameLoader.onComplete = this.DOT_TO_DOT.bind(this);
	//begin load
	gameLoader.load();
}

/**
 * create new sets of dot dot game
 */
Toy_4_2.prototype.DOT_TO_DOT = function() {

	localStorage.setItem('toy_4_2', 0);

	var frame = this.frame;

	var circleGraphics = createCircle({
		radius: this.frame.radius + 10,
	});

	this.circleTexture = circleGraphics.generateTexture();
	// this.exerciseContainer.scale.x = this.exerciseContainer.scale.y = 1.30;
	this.exerciseContainer.scale.x = this.frame.scalex;
	this.exerciseContainer.scale.y = this.frame.scaley;
	// set initial position of container.
	this.exerciseContainer.position.set(frame.x, frame.y);
	console.log(this.exerciseContainer.position);

	// clear exercise container before creating new sets of dot game
	if (this.dotLines)
		this.dotLines.length = 0;

	this.exerciseContainer.removeChildren();

	if (frame.baseSpriteIntro.isframe) {
		this.baseSpriteIntro = new PIXI.Sprite.fromFrame(frame.baseSpriteIntro.image);
	} else {
		this.baseSpriteIntro = new PIXI.Sprite.fromImage(Toy_4_2.imagePath + frame.baseSpriteIntro.image);
	}

	this.baseSpriteIntro.position.set(frame.baseSpriteIntro.x, frame.baseSpriteIntro.y);
	this.exerciseContainer.addChild(this.baseSpriteIntro);

	if (frame.baseSpriteOutro.isframe) {
		this.baseSpriteOutro = new PIXI.Sprite.fromFrame(frame.baseSpriteOutro.image);
	} else {
		this.baseSpriteOutro = new PIXI.Sprite.fromImage(Toy_4_2.imagePath + frame.baseSpriteOutro.image);
	}

	this.baseSpriteOutro.position.set(frame.baseSpriteOutro.x, frame.baseSpriteOutro.y);
	this.baseSpriteOutro.visible = false;
	this.exerciseContainer.addChild(this.baseSpriteOutro);

	if (frame.dotTodotSprite.isframe) {
		this.dotTodotSprite = new PIXI.Sprite.fromFrame(frame.dotTodotSprite.image);
	} else {
		this.dotTodotSprite = new PIXI.Sprite.fromImage(Toy_4_2.imagePath + frame.dotTodotSprite.image);
	}

	this.dotTodotSprite.position.set(frame.dotTodotSprite.x, frame.dotTodotSprite.y);
	this.exerciseContainer.addChild(this.dotTodotSprite);

	if (frame.letterSprite.isframe) {
		this.letterSprite = new PIXI.Sprite.fromFrame(frame.letterSprite.imagePrefix + this.letterType + frame.letterSprite.imageFormat);
	} else {
		this.letterSprite = new PIXI.Sprite.fromImage(Toy_4_2.imagePath + frame.letterSprite.imagePrefix + this.letterType + frame.letterSprite.imageFormat);
	}
	if (this.letterType === 'a')
		this.letterSprite.position.set(frame.letterSprite.xa, frame.letterSprite.ya);
	else
		this.letterSprite.position.set(frame.letterSprite.xA, frame.letterSprite.yA);

	// this.exerciseContainer.addChild(this.letterSprite);
	this.generateDotToDotLetters();

	this.linePosition = frame.linePosition;

	this.generateDotToDoTLines();

	this.hitAreaPosition = frame.hitAreaPosition;
}

/**
 * generate hit area in all black circle spot in dot to dot game
 */
Toy_4_2.prototype.generateDotToDotLetters = function() {

	this.hitArea = [];
	this.pointHitArea = [];
	this.letterTextArr = [];

	for (var i = 0; i < this.frame.hitAreaPosition.length; i++) {

		var dotHitArea = new PIXI.Sprite(this.circleTexture);

		dotHitArea.width = dotHitArea.height = 35;

		dotHitArea.position.x = this.frame.hitAreaPosition[i * 2] - 10
		dotHitArea.position.y = this.frame.hitAreaPosition[i * 2 + 1] - 10

		dotHitArea.interactive = true;
		dotHitArea.buttonMode = true;
		// dotHitArea.tint = '0xcc0000'
		dotHitArea.alpha = Toy_4_2.hitAlpa;

		main.texturesToDestroy.push(dotHitArea);

		// if (i === 0) dotHitArea.interactive = false;

		this.pointHitArea.push(dotHitArea);
		this.exerciseContainer.addChild(dotHitArea);

	};

	for (var i = 0; i < this.frame.letterPosition.length / 2; i++) {

		var letter = new PIXI.Text(this.hitLetters[i], {
			font: "50px Arial"
		});

		letter.position.set(this.frame.letterPosition[i * 2], this.frame.letterPosition[i * 2 + 1]);

		main.texturesToDestroy.push(letter);

		this.letterTextArr.push(letter);

		this.exerciseContainer.addChild(letter);

		var letterHitArea = new PIXI.Sprite(this.circleTexture);

		letterHitArea.position.x = this.frame.letterPosition[i * 2] - 14
		letterHitArea.position.y = this.frame.letterPosition[i * 2 + 1] - 6;

		letterHitArea.interactive = true;
		letterHitArea.buttonMode = true;
		// letterHitArea.tint = '0xcc0000'
		letterHitArea.alpha = Toy_4_2.hitAlpa;

		main.texturesToDestroy.push(letterHitArea);

		// if (i === 0) letterHitArea.interactive = false;

		this.hitArea.push(letterHitArea);
		this.exerciseContainer.addChild(letterHitArea);

	};

	this.startGame = false;
	var self = this;

	this.pointHitArea[0].click = this.pointHitArea[0].tap = this.hitArea[0].click = this.hitArea[0].tap = function(data) {

		if (addedListeners) return false;

		self.letterTextArr[0].setStyle({
			font: "50px Arial",
			fill: "green"
		});

		self.startGame = true;
	}

	for (var i = 1; i < this.hitArea.length; i++) {
		this.joinLines(i);
	};

	this.exerciseContainer.visible = true;
};

/**
 * generate sprite of lines from A to Z
 */
Toy_4_2.prototype.generateDotToDoTLines = function() {

	this.dotLines = [];

	for (var i = 0; i < this.linePosition.length / 2; i++) {

		var lineSprite = new PIXI.Sprite.fromFrame('line' + (i + 1) + '.png');
		lineSprite.position.set(this.linePosition[i * 2], this.linePosition[i * 2 + 1]);

		lineSprite.visible = false;

		this.dotLines.push(lineSprite);

		this.exerciseContainer.addChild(lineSprite);

	};
}

/**
 * join lines when letter clicked in right order
 */
Toy_4_2.prototype.joinLines = function(i) {

	var self = this;

	this.pointHitArea[i].click = this.pointHitArea[i].tap = this.hitArea[i].click = this.hitArea[i].tap = function(data) {

		if (addedListeners) return false;

		if (!self.startGame) return;

		if (localStorage.getItem('toy_4_2') == (i - 1)) {
			self.letterTextArr[i].setStyle({
				font: "50px Arial",
				fill: "green"
			});
			self.dotLines[i - 1].visible = true;
			self.correctFeedback();
		};
	}
}

/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
Toy_4_2.prototype.correctFeedback = function() {
	var feedback = localStorage.getItem('toy_4_2');
	feedback++;
	localStorage.setItem('toy_4_2', feedback);

	if (feedback >= this.hitArea.length - 1)
		setTimeout(this.onComplete.bind(this), 1000);
}

Toy_4_2.prototype.resetLetterColor = function() {

	var len = this.letterTextArr.length;

	for (var i = 0; i < len; i++) {
		this.letterTextArr[i].setStyle({
			font: "50px Arial",
			fill: "black"
		});
	};

	len = null;

};
/**
 * the outro scene is displayed
 */
Toy_4_2.prototype.onComplete = function() {

	var self = this;
	this.introMode = false;

	this.resetLetterColor();

	// this.exerciseContainer.scale.x = this.exerciseContainer.scale.y = 1.30;

	this.exerciseContainer.position.set(this.frame.x, this.frame.y);
	console.log(this.exerciseContainer.position);

	for (var i = 0; i < this.hitArea.length; i++) {
		this.hitArea[i].interactive = false;
	};
	this.baseSpriteIntro.visible = false;
	this.baseSpriteOutro.visible = true;
	this.figure_corner.visible = false;
	this.animationContainer.visible = true;
	this.outroContainer.visible = true;

	self.showHandAt(0);

	this.lowerAlphabetBox.interactive = false;
	this.upperAlphabetBox.interactive = false;

	this.resetMode = true;

	this.ovaigen.click = this.ovaigen.tap = function() {
		self.cancelOutro();
		self.resetExercise();
	}

	this.outroAnimation();

}


/**
 * reset exercise to default values
 */
Toy_4_2.prototype.resetExercise = function() {
	var self = this;

	this.exerciseContainer.visible = false;

	this.ovaigen.visible = false;
	localStorage.setItem('toy_4_2', 0);

	if (this.resetMode) {
		self.bigLetterChange();
		this.getRandomDotToDot();
		this.resetMode = false;
	} else {
		self.bigLetterChange();
	}

	self.outroContainer.visible = false;
	self.figure_corner.visible = true;
	self.baseSpriteOutro.visible = false;
	self.baseSpriteIntro.visible = true;
	self.animationContainer.visible = false;

	self.lowerAlphabetBox.interactive = true;
	self.upperAlphabetBox.interactive = true;

	this.introMode = true;
};

/**
 * clean memory by clearing reference of object
 */
Toy_4_2.prototype.cleanMemory = function() {

	this.introMode = null;
	this.dotTodot = null;

	if (this.assetsToLoad)
		this.assetsToLoad.length = 0;

	if (this.sceneBackground)
		this.sceneBackground.clear();

	this.sceneBackground = null;
	this.help = null;
	this.figure_corner = null;
	this.animated_figure = null;
	this.mouths = null;
	this.arms = null;
	this.introSound = null;
	this.outroSound = null;

	this.introContainer = null;
	this.outroContainer = null;
	this.exerciseContainer = null;
	this.animationContainer = null;
	this.alphabetBoxTexture = null;
	this.upperAlphabetBox = null;
	this.lowerAlphabetBox = null;
	this.selectUpperCase = null;
	this.selectLowerCase = null;
	this.letterType = null;
	this.frame = null;

	if (this.gameAssetsToLoad)
		this.gameAssetsToLoad.length = 0;

	this.baseSpriteIntro = null;
	this.baseSpriteOutro = null;
	this.dotTodotSprite = null;
	this.letterSprite = null;

	if (this.linePosition)
		this.linePosition.length = 0;

	if (this.hitAreaPosition)
		this.hitAreaPosition.length = 0;

	if (this.hitArea)
		this.hitArea.length = 0;

	if (this.dotLines)
		this.dotLines.length = 0;

	this.ovaigen = null;

	this.startGame = null;

	this.sceneContainer.removeChildren();
	this.sceneContainer = null;

}