function Toy_4_1() {

	cl.show();

	PIXI.DisplayObjectContainer.call(this);

	this.sceneContainer = new PIXI.DisplayObjectContainer();
	this.sceneContainer.position.set(77, 88);

	this.introContainer = new PIXI.DisplayObjectContainer();
	this.outroContainer = new PIXI.DisplayObjectContainer();

	localStorage.setItem('toy_4_1', 0);

	main.CurrentExercise = "toy_4_1";

	main.texturesToDestroy = [];

	this.loadSpriteSheet();

	// everything connected to this is rendered.
	this.addChild(this.sceneContainer);
}

Toy_4_1.constructor = Toy_4_1;
Toy_4_1.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Toy_4_1.imagePath = 'build/exercises/toystore/toy_4_1/images/';
Toy_4_1.audioPath = 'build/exercises/toystore/toy_4_1/audios/';
Toy_4_1.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

/**
 * load array of assets of different format
 */
Toy_4_1.prototype.loadSpriteSheet = function() {

	this.introId = loadSound(Toy_4_1.audioPath + 'intro');
	this.outroId = loadSound(Toy_4_1.audioPath + 'outro');

	// create an array of assets to load
	this.assetsToLoad = [
		Toy_4_1.imagePath + "sprite_4_1.json",
		Toy_4_1.imagePath + "sprite_4_1.png",
		Toy_4_1.imagePath + "puzzle_4_1.json",
		Toy_4_1.imagePath + "puzzle_4_1.png",
		Toy_4_1.imagePath + "outro_4_1.json",
		Toy_4_1.imagePath + "outro_4_1.png",

	];
	// create a new loader
	loader = new PIXI.AssetLoader(this.assetsToLoad);
	// use callback
	loader.onComplete = this.spriteSheetLoaded.bind(this);
	//begin load
	loader.load();

}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
Toy_4_1.prototype.spriteSheetLoaded = function() {

	// hide loader
	cl.hide();

	if (this.sceneContainer.stage)
		this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

	// fill exercise background
	this.sceneBackground = new PIXI.Graphics();
	this.sceneBackground.beginFill(0xd6c5ba);
	// set the line style to have a width of 5 and set the color to red
	this.sceneBackground.lineStyle(3, 0xd7e5ae);
	// draw a rectangle
	this.sceneBackground.drawRect(0, 0, 1843, 1202);
	this.sceneContainer.addChild(this.sceneBackground);

	// exercise instruction
	this.help = PIXI.Sprite.fromFrame("topcorner.png");
	this.help.interactive = true;
	this.help.buttonMode = true;
	this.help.position.set(-77, -88);
	this.sceneContainer.addChild(this.help);

	this.help.click = this.help.tap = this.introAnimation.bind(this);

	this.loadExercise();

	this.sceneContainer.addChild(this.introContainer);
	this.introScene();

	this.sceneContainer.addChild(this.outroContainer);
	this.outroScene();

}

/**
 * create sprites,animation,sound related to intro scene
 */
Toy_4_1.prototype.introScene = function() {

	var self = this;

	this.animationContainer = new PIXI.DisplayObjectContainer();
	this.introContainer.addChild(this.animationContainer);

	this.animated_figure = new PIXI.Sprite.fromFrame('animated_figure.png');
	this.animated_figure.position.set(366, 339);
	main.texturesToDestroy.push(this.animated_figure);
	this.animationContainer.addChild(this.animated_figure);

	this.eyes = new Animation({
		"image": "eyes_",
		"length": 3,
		"x": 464,
		"y": 430,
		"speed": 1,
		"pattern": true,
		"frameId": 3
	});

	this.mouths = new Animation({
		"image": "mouth_",
		"length": 4,
		"x": 485,
		"y": 513,
		"speed": 3,
		"pattern": true,
		"frameId": 3
	});

	// enable eyes,mouth,arms animation to container
	this.animationContainer.addChild(this.eyes);
	this.animationContainer.addChild(this.mouths);

	this.hands = [
		new PIXI.Sprite.fromFrame('arm_1.png'),
		new PIXI.Sprite.fromFrame('arm_2.png'),
		new PIXI.Sprite.fromFrame('arm_3.png'),
	];
	for (var i = 0; i < this.hands.length; i++) {
		this.hands[i].position.set(602 + 22, 576);
		this.animationContainer.addChild(this.hands[i]);
		this.hands[i].visible = false;
	}

	this.animationContainer.visible = false;

}

/**
 * play intro animation
 */
Toy_4_1.prototype.introAnimation = function() {

	createjs.Sound.stop();

	if (!this.introMode)
		this.resetExercise();

	this.help.interactive = false;
	this.help.buttonMode = false;

	main.overlay.visible = true;

	this.figure_corner.visible = false;
	this.animationContainer.visible = true;

	this.mouths.show();
	this.showHandAt(1)
	this.eyes.show();

	this.introInstance = createjs.Sound.play(this.introId, {
		interrupt: createjs.Sound.INTERRUPT_ANY
	});

	if (createjs.Sound.isReady())
		this.timerId = setInterval(this.animateIntro.bind(this), 1);

	this.introCancelHandler = this.cancelIntro.bind(this);
	this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
* handle mouth, hand with specific sound positions or queue points.
0-2.5 SEC ARM_2
2.5-4 SEC ARM_3
4-7 SEC ARM_2
7-9 SEC ARM_1
9-11 SEC ARM_2
11-STOP AT ARM_3
 */
Toy_4_1.prototype.animateIntro = function() {

	var currentPostion = this.introInstance.getPosition();
	if (currentPostion >= 0 && currentPostion < 2500) {
		this.showHandAt(1);
	} else if (currentPostion >= 2500 && currentPostion < 4000) {
		this.showHandAt(2);
	} else if (currentPostion >= 4000 && currentPostion < 7000) {
		this.showHandAt(1);
	} else if (currentPostion >= 7000 && currentPostion < 9000) {
		this.showHandAt(0);
	} else if (currentPostion >= 9000 && currentPostion < 11000) {
		this.showHandAt(1);
	} else if (currentPostion >= 11000) {
		this.showHandAt(2);
	}

};

/**
 * cancel running intro animation
 */
Toy_4_1.prototype.cancelIntro = function() {

	createjs.Sound.stop();

	clearInterval(this.timerId);
	this.mouths.hide(3);

	this.figure_corner.visible = true;
	this.animationContainer.visible = false;

	this.help.interactive = true;
	this.help.buttonMode = true;

	this.ovaigen.visible = false;
	main.overlay.visible = false;
}

/**
 * hand visibility
 * @param  {int} number handIndex
 */
Toy_4_1.prototype.showHandAt = function(number) {
	for (var i = 0; i < this.hands.length; i++) {
		this.hands[i].visible = false;
	}
	if (isNumeric(number))
		this.hands[number].visible = true;
};

/**
 * play outro animation
 */
Toy_4_1.prototype.outroAnimation = function() {
	var self = this;

	this.outroInstance = createjs.Sound.play(this.outroId);
	if (createjs.Sound.isReady()) {
		main.overlay.visible = true;
		self.help.interactive = false;
		self.help.buttonMode = false;

		self.mouthsOutro.show();
	}
	this.outroCancelHandler = this.cancelOutro.bind(this);
	this.outroInstance.addEventListener("complete", this.outroCancelHandler);

};

/**
 * stop running outro animation
 */
Toy_4_1.prototype.cancelOutro = function() {

	var self = this;

	createjs.Sound.stop();
	self.mouthsOutro.hide(3);

	self.help.interactive = true;
	self.help.buttonMode = true;

	self.ovaigen.visible = true;
	main.overlay.visible = false;

};

/**
 * create sprites,animation,sound related to outro scene
 */
Toy_4_1.prototype.outroScene = function() {

	var self = this;

	this.outroContainer.visible = false;

	this.outroAnimatedFigure = new PIXI.Sprite.fromFrame('figure.png');
	this.outroAnimatedFigure.position.set(1212, 442);
	main.texturesToDestroy.push(this.outroAnimatedFigure);
	this.outroContainer.addChild(this.outroAnimatedFigure);

	this.mouthsOutro = new Animation({
		"image": "mouth_outro_",
		"length": 4,
		"x": 1473,
		"y": 814,
		"speed": 4,
		"pattern": true,
		"frameId": 3
	});

	this.outroContainer.addChild(this.mouthsOutro);

	// back to intro page
	this.ovaigen = PIXI.Sprite.fromFrame('ovaigen.png');
	this.ovaigen.position.set(1077, 694);
	this.ovaigen.interactive = true;
	this.ovaigen.buttonMode = true;
	this.ovaigen.visible = false;
	this.outroContainer.addChild(this.ovaigen);

}

/**
 * get random outro sprite
 */
Toy_4_1.prototype.getRandomOutroSprite = function() {
	var rand = makeUniqueRandom(5) + 1;
	this.outroPuzzleSprite = new PIXI.Sprite.fromFrame('fb_bild' + rand + '.png');
	this.outroPuzzleSprite.position.set(412, 35);
	main.texturesToDestroy.push(this.outroPuzzleSprite);
	this.outroContainer.addChildAt(this.outroPuzzleSprite, 0);
}

/**
 * load exercise related stuffs
 */
Toy_4_1.prototype.loadExercise = function() {
	// create texture for rounded alphabet box
	this.alphabetBoxTexture = new PIXI.Texture.fromFrame("selectedbigsmall.png");

	this.upperAlphabetBox = new PIXI.Sprite(this.alphabetBoxTexture);
	this.upperAlphabetBox.position.set(54, 628);
	this.upperAlphabetBox.interactive = true;
	this.upperAlphabetBox.buttonMode = true;
	main.texturesToDestroy.push(this.upperAlphabetBox);

	this.lowerAlphabetBox = new PIXI.Sprite(this.alphabetBoxTexture);
	this.lowerAlphabetBox.position.set(54 + 130, 628);
	this.lowerAlphabetBox.interactive = true;
	this.lowerAlphabetBox.buttonMode = true;
	main.texturesToDestroy.push(this.lowerAlphabetBox);

	this.selectUpperCase = new PIXI.Text('A', {
		font: "50px Arial"
	});
	this.selectUpperCase.position.set(89, 654);
	main.texturesToDestroy.push(this.selectUpperCase);

	this.selectLowerCase = new PIXI.Text('a', {
		font: "50px Arial"
	});
	this.selectLowerCase.position.set(226, 654);
	main.texturesToDestroy.push(this.selectLowerCase);

	this.sceneContainer.addChild(this.upperAlphabetBox);
	this.sceneContainer.addChild(this.selectUpperCase);
	this.sceneContainer.addChild(this.lowerAlphabetBox);
	this.sceneContainer.addChild(this.selectLowerCase);

	this.figure_corner = new PIXI.Sprite.fromFrame('figure_corner.png');
	this.figure_corner.position.set(98, 764);
	main.texturesToDestroy.push(this.figure_corner);
	this.introContainer.addChild(this.figure_corner);

	// create new container to store all puzzle sprites
	this.puzzleContainer = new PIXI.DisplayObjectContainer();
	this.introContainer.addChild(this.puzzleContainer);

	this.upperAlphabetBox.click = this.upperAlphabetBox.tap = this.bigLetterVisible.bind(this);

	this.lowerAlphabetBox.click = this.lowerAlphabetBox.tap = this.smallLetterVisible.bind(this);

	this.bigLetterVisible();
}

/**
 * make big letters visible
 */
Toy_4_1.prototype.bigLetterVisible = function() {
	if (addedListeners) return false;
	this.managePuzzle('big_');
	this.selectLowerCase.alpha = this.lowerAlphabetBox.alpha = 0.4;
	this.selectUpperCase.alpha = this.upperAlphabetBox.alpha = 1;
}

/**
 * make small letters visible
 */
Toy_4_1.prototype.smallLetterVisible = function() {
	if (addedListeners) return false;
	this.managePuzzle('small_');
	this.selectLowerCase.alpha = this.lowerAlphabetBox.alpha = 1;
	this.selectUpperCase.alpha = this.upperAlphabetBox.alpha = 0.4;
}

/**
 * create new sets of puzzle game
 */
Toy_4_1.prototype.managePuzzle = function(puzzleType) {

	// clear puzzle container before creating new sets of puzzle
	this.puzzleContainer.removeChildren();

	// display puzzle background
	this.puzzleBase = new PIXI.Sprite.fromFrame('puzzlebg.png');
	this.puzzleBase.position.set(412, 35);
	main.texturesToDestroy.push(this.puzzleBase);
	this.puzzleContainer.addChild(this.puzzleBase);

	this.puzzleParams = {
		'w': 143,
		'h': 148,
		'position': [
			414, 36,
			520, 36,
			667, 36,
			851, 36,

			414, 182,
			560, 149,
			708, 182,
			854, 182,

			414, 325,
			523, 326,
			670, 294,
			854, 292,

			414, 433,
			526, 473,
			671, 440,
			854, 438,

			414, 619,
			562, 582,
			708, 581,
			817, 621,

			414, 728,
			525, 765,
			671, 765,
			855, 728,

			414, 875,
			527, 873,
			707, 911,
			853, 910,

		],
		'shufflePosition': [
			1070, 60,
			1291, 80,
			1491, 80,
			1180, 257,
			1402, 257,
			1084, 449,
			1357, 510,
			1585, 472,
			1159, 746,
			1464, 764,
		]
	};

	this.puzzleLeft = [];
	this.puzzleRight = [];

	this.indexLeft = [];
	this.indexRight = [];

	this.arrValues = generateUniqueRandomNubers({
		'max': 27,
		'total': 10
	});

	for (var i = 0; i < 28; i++) {

		var puzzleSprite = new PIXI.Sprite.fromFrame(puzzleType + (i + 1) + '.png');
		main.texturesToDestroy.push(puzzleSprite);

		if (this.arrValues.indexOf(i) > -1) {
			this.indexRight.push(i);
			this.puzzleRight.push(puzzleSprite);
		} else {
			this.indexLeft.push(i);
			this.puzzleLeft.push(puzzleSprite);
		}

	};

	var shufflePosition0 = [],
		shufflePosition1 = [];

	for (var i = 0; i < 20; i++) {
		if (i % 2 === 0) {
			shufflePosition0.push(this.puzzleParams.shufflePosition[i])
		} else {
			shufflePosition1.push(this.puzzleParams.shufflePosition[i])
		}
	};

	shuffle(shufflePosition0, shufflePosition1);

	// generate puzzle for the left side
	for (var i = 0; i < this.puzzleLeft.length; i++) {

		this.puzzleLeft[i].position.set(this.puzzleParams.position[this.indexLeft[i] * 2], this.puzzleParams.position[this.indexLeft[i] * 2 + 1]);

		this.puzzleContainer.addChild(this.puzzleLeft[i]);

	};

	// // generate puzzle for the right side
	for (var i = 0; i < this.puzzleRight.length; i++) {

		this.puzzleRight[i].position.set(shufflePosition0[i], shufflePosition1[i])

		this.puzzleRight[i].interactive = true;
		this.puzzleRight[i].buttonMode = true;

		this.puzzleRight[i].oldPosition = {
			x: shufflePosition0[i],
			y: shufflePosition1[i],
		}
		this.puzzleRight[i].dropIndex = this.indexRight[i];
		this.puzzleContainer.addChild(this.puzzleRight[i]);
	};

	for (var i = 0; i < this.puzzleRight.length; i++) {
		this.onDragging(i);
	};
}

/**
 * drag drop sprite
 * on correct drop, feedback is updated
 */
Toy_4_1.prototype.onDragging = function(i) {

	var self = this;

	// use the mousedown and touchstart
	this.puzzleRight[i].mousedown = this.puzzleRight[i].touchstart = function(data) {
		if (addedListeners) return false;

		data.originalEvent.preventDefault()
			// store a refference to the data
			// The reason for this is because of multitouch
			// we want to track the movement of this particular touch
		this.data = data;
		this.alpha = 0.9;
		this.dragging = true;
		this.sx = this.data.getLocalPosition(this).x;
		this.sy = this.data.getLocalPosition(this).y;
		this.bringToFront();

	}

	this.puzzleRight[i].mouseup = this.puzzleRight[i].mouseupoutside = this.puzzleRight[i].touchend = this.puzzleRight[i].touchendoutside = function(data) {
		if (addedListeners) return false;
		this.alpha = 1
		this.dragging = false;
		// set the interaction data to null
		this.data = null;

		if (self.dropPositionPerIndex(this.dx, this.dy, this.dropIndex)) {
			this.position = {
				x: self.puzzleParams.position[this.dropIndex * 2],
				y: self.puzzleParams.position[this.dropIndex * 2 + 1]
			};
			this.interactive = false;
			correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
			setTimeout(function() {
				self.correctFeedback();
			}, 1000);
		} else {
			// reset dragged element position
			this.position.x = this.oldPosition.x;
			this.position.y = this.oldPosition.y;
		}
	}

	// set the callbacks for when the mouse or a touch moves
	this.puzzleRight[i].mousemove = this.puzzleRight[i].touchmove = function(data) {
		if (addedListeners) return false;
		// set the callbacks for when the mouse or a touch moves
		if (this.dragging) {
			// need to get parent coords..
			var newPosition = this.data.getLocalPosition(this.parent);
			this.position.x = newPosition.x - this.sx;
			this.position.y = newPosition.y - this.sy;
			this.dx = newPosition.x;
			this.dy = newPosition.y;
		}
	};

}

/**
 * check drag position is inside rectangular drop position
 * @param  {float} dx    drag positon x
 * @param  {float} dy    drag position y
 * @param  {integer} index
 * @return {boolean}       true/false
 */
Toy_4_1.prototype.dropPositionPerIndex = function(dx, dy, index) {

	var x1 = this.puzzleParams.position[index * 2],
		y1 = this.puzzleParams.position[index * 2 + 1],
		x2 = this.puzzleParams.position[index * 2] + 146,
		y2 = this.puzzleParams.position[index * 2 + 1] + 147;

	if (isInsideRectangle(dx, dy, x1, y1, x2, y2))
		return true;
	else
		return false;
}

/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
Toy_4_1.prototype.correctFeedback = function() {
	var feedback = localStorage.getItem('toy_4_1');
	feedback++;
	localStorage.setItem('toy_4_1', feedback);

	if (feedback >= Toy_4_1.totalFeedback)
		setTimeout(this.onComplete.bind(this), 1000);
}

/**
 * the outro scene is displayed
 */
Toy_4_1.prototype.onComplete = function() {

	var self = this;

	this.introMode = false;

	this.introContainer.visible = false;
	this.getRandomOutroSprite();
	this.outroContainer.visible = true;

	this.lowerAlphabetBox.interactive = false;
	this.upperAlphabetBox.interactive = false;

	this.resetMode = true;

	this.ovaigen.click = this.ovaigen.tap = function() {
		self.cancelOutro();
		self.resetExercise();
	}

	this.outroAnimation();

}

/**
 * reset exercise to default values
 */
Toy_4_1.prototype.resetExercise = function() {
	var self = this;
	this.ovaigen.visible = false;
	localStorage.setItem('toy_4_1', 0);
	self.introMode = true;

	self.outroContainer.visible = false;
	self.introContainer.visible = true;

	self.lowerAlphabetBox.interactive = true;
	self.upperAlphabetBox.interactive = true;

	if (this.resetMode) {
		self.outroContainer.removeChildAt(0);
		this.resetMode = false;
	}

	self.bigLetterVisible();
};

/**
 * clean memory by clearing reference of object
 */
Toy_4_1.prototype.cleanMemory = function() {

	if (this.assetsToLoad)
		this.assetsToLoad.length = 0;

	if (this.sceneBackground)
		this.sceneBackground.clear();

	this.sceneBackground = null;

	this.help = null;

	this.animated_figure = null;
	this.eyes = null;
	this.mouths = null;
	this.arms = null;
	this.introSound = null;



	this.outroPuzzleSprite = null;
	this.outroAnimatedFigure = null;
	this.mouthsOutro = null;
	this.ovaigen = null;
	this.outroSound = null;

	this.introMode = null;
	this.alphabetBoxTexture = null;
	this.upperAlphabetBox = null;
	this.lowerAlphabetBox = null;
	this.selectUpperCase = null;
	this.selectLowerCase = null;
	this.figure_corner = null;

	this.puzzleBase = null;

	if (this.puzzleParams)
		this.puzzleParams.length = 0;

	if (this.puzzleLeft)
		this.puzzleLeft.length = 0;

	if (this.puzzleRight)
		this.puzzleRight.length = 0;

	if (this.indexLeft)
		this.indexLeft.length = 0;

	if (this.indexRight)
		this.indexRight.length = 0;

	if (this.arrValues)
		this.arrValues.length = 0;

	this.introContainer = null;
	this.outroContainer = null;
	this.puzzleContainer = null;

	this.sceneContainer.removeChildren();
	this.sceneContainer = null;
}