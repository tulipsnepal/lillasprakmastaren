function Toy_2_1(stage) {

    cl.show();

    PIXI.DisplayObjectContainer.call(this);
    main.CurrentExercise = "toy_2_1";
    this.baseContainer = new PIXI.DisplayObjectContainer();

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.robotPartsContainer = new PIXI.DisplayObjectContainer();
    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();
    this.feedbackContainer = new PIXI.SpriteBatch();
    this.alphabetContainer = new PIXI.DisplayObjectContainer();
    this.exerciseAlphabetContainer = new PIXI.DisplayObjectContainer();
    this.imageContainer = new PIXI.DisplayObjectContainer();
    this.animationContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position = {
        x: 77,
        y: 88
    };

    localStorage.setItem('toy_2_1', 0);

    this.baseContainer.addChild(this.sceneContainer);


    main.texturesToDestroy = [];
    main.soundToDestroy = [];
    this.alphaHowls = [];
    this.alphaVowelHowls = [];
    this.answer = "";
    this.jsonContents = {};

    this.defaultGroup = "ABCD";

    this.loadJsonData();

    // everything connected to stage is rendered.
    this.addChild(this.baseContainer);


}

Toy_2_1.constructor = Toy_2_1;
Toy_2_1.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Toy_2_1.imagePath = 'build/exercises/toystore/toy_2_1/images/';
Toy_2_1.audioPath = 'build/exercises/toystore/toy_2_1/audios/';
Toy_2_1.commonFolder = 'build/common/images/toystore/';
Toy_2_1.totalFeedback = 10; // default 10

/**
 * preload sprites ,images and audios
 */
Toy_2_1.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(Toy_2_1.audioPath + 'intro');
    this.outroId = loadSound(Toy_2_1.audioPath + 'outro');

    // create an array of assets to load
    var assetsToLoad = [
        Toy_2_1.imagePath + "sprite_toystore_2_1.json",
        Toy_2_1.imagePath + "sprite_toystore_2_1.png",
        Toy_2_1.imagePath + "box.png",
        Toy_2_1.imagePath + "toylady_robot.png",
        Toy_2_1.imagePath + "bg-lined.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * load exercise json data before sprite sheet loaded
 */
Toy_2_1.prototype.loadJsonData = function() {
    var self = this;
    var jsonloader = new PIXI.JsonLoader('uploads/exercises/toystore/toystore_2_1.json?noache=' + (new Date()).getTime());
    jsonloader.on('loaded', function(evt) {
        self.jsonContents = jsonloader;
        self.loadSpriteSheet();
    });
    jsonloader.load();
}

/**
 * on all assets loaded and josn sets fetched, load exercise
 */
Toy_2_1.prototype.spriteSheetLoaded = function() {
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    this.loadDefaults();
    this.renderFeedBacks();
    this.renderWhiteBoxes();

    this.renderRobotParts();
    this.renderAlphabetGroups();
    this.initObjectPooling(this.defaultGroup);
    this.renderExerciseAlphabets();

    this.alphabetContainer.addChild(this.imageContainer);
    this.introContainer.addChild(this.alphabetContainer);
    this.introContainer.addChild(this.feedbackContainer);
    this.introContainer.addChild(this.animationContainer);
    this.alphabetContainer.addChild(this.exerciseAlphabetContainer);

}

/**
 * show hide children in container
 * @param  {object} obj
 * @param  {boolean} show
 */
Toy_2_1.prototype.showHideChildrenInContainer = function(obj, show) {

    for (var j = 0; j < obj.children.length; j++) {
        obj.children[j].visible = show;
    }
}

/**
 * create white sprite
 */
Toy_2_1.prototype.renderWhiteBoxes = function() {
    this.whiteBoxPositions = [
        416, 471,
        669, 471,
        922, 471,
        1175, 471,
        726, 89
    ];
    for (var j = 0; j < 5; j++) {

        var boxParams = {
            x: this.whiteBoxPositions[j * 2],
            y: this.whiteBoxPositions[j * 2 + 1],
            w: j == 4 ? 339 : 220,
            h: j == 4 ? 339 : 220,
            lineStyle: 1
        }
        var box = createRect(boxParams);
        this.introContainer.addChild(box);
    }

    this.greenRect = createRect({
        w: 220,
        h: 220,
        color: 0x00ff00,
        alpha: 0.6,
        lineStyle: 0
    });
    this.redRect = createRect({
        w: 220,
        h: 220,
        color: 0xff0000,
        alpha: 0.6,
        lineStyle: 0
    });
}

/**
 * create robot sprites that will be shown in feedback update
 */
Toy_2_1.prototype.renderRobotParts = function() {
    var self = this;
    var partsPosition = [
        1300, 1067,
        1232, 960,
        1334, 961,
        1094, 934,
        839, 949,
        1075, 1083,
        710, 952,
        430, 897,
        394, 1045,
        633, 963
    ];

    for (var k = 0; k < 10; k++) {
        var partSprite = PIXI.Sprite.fromFrame("rbt_" + (k + 1) + ".png");

        partSprite.position = {
            x: partsPosition[k * 2],
            y: partsPosition[k * 2 + 1]
        };

        main.texturesToDestroy.push(partSprite);
        self.robotPartsContainer.addChild(partSprite);
    }

    partsPosition = null;
}

/**
 * create feedback sprites
 */
Toy_2_1.prototype.renderFeedBacks = function() {
    var self = this;
    var feedBackPosition = [
        1548, 795,
        1471, 852,
        1587, 722,
        1499, 720,
        1465, 474,
        1695, 492,
        1464, 514,
        1512, 311,
        1526, 320,
        1561, 202
    ];

    for (var k = 0; k < 10; k++) {
        var feedbackSprite = PIXI.Sprite.fromFrame("fb_" + (k + 1) + ".png");


        feedbackSprite.position = {
            x: feedBackPosition[k * 2],
            y: feedBackPosition[k * 2 + 1]
        };

        main.texturesToDestroy.push(feedbackSprite);
        self.feedbackContainer.addChild(feedbackSprite);
        feedbackSprite.visible = false;
    }

    feedBackPosition = null;
}

/**
 * render alphabet groups on left side of exercise
 */
Toy_2_1.prototype.renderAlphabetGroups = function() {
    var self = this;
    var groupsToShow = ["A B C D", "E F G H", "I  J  K  L", "M N O P"];
    var groups = ["ABCD", "EFGH", "IJKL", "MNOP"];
    var groupPositions = [
        95, 407,
        95, 487,
        95, 571,
        95, 654
    ];

    var greenButton = PIXI.Sprite.fromFrame("button_green.png");
    greenButton.position.set(63, 390);
    main.texturesToDestroy.push(greenButton);
    self.alphabetContainer.addChild(greenButton);


    var groupButtons = [];
    for (var k = 0; k < 4; k++) {
        var alphabets = new PIXI.Text(groupsToShow[k], {
            font: "60px Arial",
            fill: k == 0 ? "black" : "grey"
        });

        alphabets.position = {
            x: groupPositions[k * 2],
            y: groupPositions[k * 2 + 1]
        };
        alphabets.interactive = true;
        alphabets.buttonMode = true;
        alphabets.hitIndex = k;
        self.alphabetContainer.addChild(alphabets);
        groupButtons.push(alphabets);
    }

    groupButtons[0].click = groupButtons[0].tap = groupButtons[1].click = groupButtons[1].tap = groupButtons[2].click = groupButtons[2].tap = groupButtons[3].click = groupButtons[3].tap = function(data) {

        for (var k = 0; k < 4; k++) {
            groupButtons[k].setStyle({
                font: "60px Arial",
                fill: "grey"
            });
        }

        groupButtons[this.hitIndex].setStyle({
            font: "60px Arial",
            fill: "black"
        });

        greenButton.position = {
            x: groupButtons[this.hitIndex].position.x - 32,
            y: groupButtons[this.hitIndex].position.y - 17
        };
        /*        localStorage.setItem('toy_2_1', 0);
                self.showHideChildrenInContainer(self.feedbackContainer, false);
                self.showHideChildrenInContainer(self.robotPartsContainer, true);
        */
        self.defaultGroup = groups[this.hitIndex];
        self.alphaHowls = [];
        self.alphaVowelHowls = []
        self.initObjectPooling(self.defaultGroup);
        self.renderExerciseAlphabets();
    }

}

/**
 * draw background, create sprites and movie clip
 */
Toy_2_1.prototype.loadDefaults = function() {

    var self = this;


    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0xD5DFF1);
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xd6e4ae);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);

    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.robotPartsContainer);
    this.sceneContainer.addChild(this.outroContainer);
    this.outroContainer.visible = false;

    this.torsoObject = PIXI.Sprite.fromFrame("torso.png");
    this.torsoObject.position.set(811, 584);
    main.texturesToDestroy.push(this.torsoObject);
    this.animationContainer.addChild(this.torsoObject);

    this.deskObject = PIXI.Sprite.fromImage(Toy_2_1.imagePath + "box.png");
    this.deskObject.position.set(3, 828);
    main.texturesToDestroy.push(this.deskObject);
    this.animationContainer.addChild(this.deskObject);

    this.generateHandSprite();

    this.headRestPosition = PIXI.Sprite.fromFrame("head_1.png");
    this.headRestPosition.position.set(819, 437);
    main.texturesToDestroy.push(this.headRestPosition);
    this.animationContainer.addChild(this.headRestPosition);

    this.headUpPosition = PIXI.Sprite.fromFrame("head_looking_up.png");
    this.headUpPosition.position.set(817, 379);
    main.texturesToDestroy.push(this.headUpPosition);
    this.animationContainer.addChild(this.headUpPosition);
    this.headUpPosition.visible = false;


    this.generateMouthSprite();

    this.animationContainer.visible = false;

    this.help = PIXI.Sprite.fromFrame("topcorner.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position = {
        x: -77,
        y: -88
    }
    this.sceneContainer.addChild(this.help);

    //OUTRO STUFF
    this.outroBgLines = PIXI.Sprite.fromImage(Toy_2_1.imagePath + "bg-lined.png");
    this.outroBgLines.position.set(0, 0);
    main.texturesToDestroy.push(this.outroBgLines);
    this.outroContainer.addChild(this.outroBgLines);

    this.outroCharacter = PIXI.Sprite.fromImage(Toy_2_1.imagePath + "toylady_robot.png");
    this.outroCharacter.position.set(286, 36);
    main.texturesToDestroy.push(this.outroCharacter);
    this.outroContainer.addChild(this.outroCharacter);

    this.outroMouthParams = {
        "image": "mouth_",
        "length": 4,
        "x": 1222,
        "y": 407,
        "speed": 6,
        "pattern": false,
        "frameId": 3
    };
    this.animationOutroMouth = new Animation(this.outroMouthParams);

    this.outroContainer.addChild(this.animationOutroMouth);

    this.ovaigen = PIXI.Sprite.fromFrame("ovaigen.png");
    this.ovaigen.buttonMode = true;
    this.ovaigen.interactive = true;
    this.ovaigen.position = {
        x: 736,
        y: 335
    };
    this.outroContainer.addChild(this.ovaigen);
    this.ovaigen.visible = false;

    //OUTRO STUFF END
    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.updateFeedback();

}

Toy_2_1.prototype.generateHandSprite = function() {
    this.hands = [
        new PIXI.Sprite.fromFrame('hand_1.png'),
        new PIXI.Sprite.fromFrame('hand_2.png'),
        new PIXI.Sprite.fromFrame('hand_3.png'),
        new PIXI.Sprite.fromFrame('hand_4.png'),
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(331, 435);
        this.animationContainer.addChild(this.hands[i]);
        this.hands[i].visible = false;
    }

    this.showHandAt(0);
};

Toy_2_1.prototype.generateMouthSprite = function() {

    this.mouthHead1Params = {
        "image": "mouth_head1_mouth",
        "length": 3,
        "x": 878,
        "y": 715,
        "speed": 6,
        "pattern": false,
        "frameId": 1
    };
    this.mouthHead1 = new Animation(this.mouthHead1Params);
    this.animationContainer.addChild(this.mouthHead1);

    this.mouthHead2Params = {
        "image": "mouth_up",
        "length": 3,
        "x": 887,
        "y": 552,
        "speed": 6,
        "pattern": false,
        "frameId": 1
    };
    this.mouthHead2 = new Animation(this.mouthHead2Params);
    this.animationContainer.addChild(this.mouthHead2);
    this.mouthHead2.visible = false;

};

/**
 * play intro animation
 */
Toy_2_1.prototype.introAnimation = function() {

    createjs.Sound.stop();

    main.overlay.visible = true;
    this.animationContainer.visible = true;
    this.headUpPosition.visible = false;
    this.headRestPosition.visible = true;

    if (!this.introMode)
        this.restartExercise();

    this.showHandAt(0);

    this.help.interactive = false;
    this.help.buttonMode = false;

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
USE HEAD: HEAD_1
USE MOUTHES: MOUTH_1-MOUTH_3
ARMS:
0-0.75 SEK HAND_1+HAND_1
0.75-3 SEK HAND_1 + HAND LEFT POINTING
3-5 SEK HAND_1 + HAND_1
(SWITCH TO HEAD_LOOKING UP, USE MOUTHES MOUTH_UP1, MOUTH_UP_2,
MOUTH_UP_3)
ARMS:
5-8.5 SEK HAND_1 + HAND_POINTING
8,5 - 10 SEK HAND_1 + ARM_POINTING AT IMAGE
STOP AT : 1ST HEAD + HAND_1 + HAND_1
 */
Toy_2_1.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    if (currentPostion >= 750 && currentPostion < 3000) {
        this.showHandAt(1);
    } else if (currentPostion >= 3000 && currentPostion < 5000) {
        this.showHandAt(0);
    } else if (currentPostion >= 5000 && currentPostion < 8500) {
        this.headRestPosition.visible = false;
        this.headUpPosition.visible = true;
        this.mouthHead1.visible = false;
        this.mouthHead2.visible = true;
        this.mouthHead2.show();
        this.showHandAt(2);
    } else if (currentPostion >= 8500 && currentPostion < 1000) {
        this.showHandAt(3);
    } else {

        this.showHandAt(0);

        this.headUpPosition.visible = false;
        this.headRestPosition.visible = true;

        this.mouthHead1.visible = true;
        this.mouthHead2.visible = false;
        this.mouthHead1.show();
    }
};

/**
 * cancel running intro animation
 */
Toy_2_1.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    this.animationContainer.visible = false;
    this.headRestPosition.visible = true;
    this.headUpPosition.visible = false;
    this.help.interactive = true;
    this.help.buttonMode = true;

    this.ovaigen.visible = false;
    main.overlay.visible = false;
};

/**
 * play outro animation
 */
Toy_2_1.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.help.interactive = false;
        self.help.buttonMode = false;
        self.animationOutroMouth.show();
    }
    this.outroCancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.outroCancelHandler);
};

/**
 * stop running outro instance clicking anywhere in exercise
 */
Toy_2_1.prototype.cancelOutro = function() {

    var self = this;

    createjs.Sound.stop();
    self.animationOutroMouth.hide(1);
    self.ovaigen.visible = true;
    self.help.interactive = true;
    self.help.buttonMode = true;
    main.overlay.visible = false;
};

/**
 * show target hand and hide others
 * @param  {int} number
 */
Toy_2_1.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    }
    if (isNumeric(number))
        this.hands[number].visible = true;
};

/**
 * play exercise sets based on chosen alphabets group
 */
Toy_2_1.prototype.renderExerciseAlphabets = function() {
    var self = this;
    var textPositions = [
        416, 471,
        669, 471,
        922, 471,
        1175, 471
    ];

    var brk = self.defaultGroup.split('');
    var alphaButtons = [];

    for (var k = self.exerciseAlphabetContainer.children.length - 1; k >= 0; k--) {
        self.exerciseAlphabetContainer.removeChild(self.exerciseAlphabetContainer.children[k]);
    };

    this.soundTexture = new PIXI.Texture.fromFrame("sound_grey.png");
    for (var k = 0; k < brk.length; k++) {

        var alphabets = new PIXI.Text(brk[k] + brk[k].toLowerCase(), {
            font: "70px Arial"
        });

        alphabets.position = {
            x: textPositions[k * 2] + (221 - alphabets.width) / 2,
            y: textPositions[k * 2 + 1] + 70
        };
        alphabets.hitIndex = k;
        self.exerciseAlphabetContainer.addChild(alphabets);


        var alphaInteractive = PIXI.Sprite.fromFrame("interactive.png");
        alphaInteractive.interactive = true;
        alphaInteractive.buttonMode = true;
        alphaInteractive.height = 221;
        alphaInteractive.width = 221;
        alphaInteractive.hitIndex = k;
        alphaInteractive.selectedAlphabet = brk[k];
        alphaInteractive.position = {
            x: textPositions[k * 2],
            y: textPositions[k * 2 + 1]
        };

        self.exerciseAlphabetContainer.addChild(alphaInteractive);


        var sound = new PIXI.Sprite(this.soundTexture);

        sound.buttonMode = true;
        sound.position.x = textPositions[k * 2];
        sound.position.y = textPositions[k * 2 + 1] + 170;
        sound.interactive = true;
        self.exerciseAlphabetContainer.addChild(sound);
        var soundObj = self.alphaHowls;
        sound.alphabetsHowl = soundObj[k];
        var soundObjVowels = self.alphaVowelHowls;
        sound.alphabetVowels = soundObjVowels[k];
        sound.hitArea = new PIXI.Rectangle(-10, -10, 150, 150);

        sound.click = sound.tap = function(data) {
            var vol = this;
            if (addedListeners) return false;
            addedListeners = true;

            if (this.alphabetVowels) {
                this.alphabetInstance = createjs.Sound.play(this.alphabetVowels);
                this.alphabetInstance.addEventListener("failed", handleFailed);
                this.alphabetInstance.addEventListener("complete", function() {
                    setTimeout(function() {
                        var vowelIntance = createjs.Sound.play(vol.alphabetsHowl);
                        vowelIntance.addEventListener("complete", function() {
                            addedListeners = false;
                        });
                    }, 300);

                });
            } else {
                this.alphabetInstance = createjs.Sound.play(this.alphabetsHowl);
                this.alphabetInstance.addEventListener("failed", handleFailed);
                this.alphabetInstance.addEventListener("complete", function() {
                    addedListeners = false;
                });
            }

        }


        alphaButtons.push(alphaInteractive);
    }

    alphaButtons[0].click = alphaButtons[0].tap = alphaButtons[1].click = alphaButtons[1].tap = alphaButtons[2].click = alphaButtons[2].tap = alphaButtons[3].click = alphaButtons[3].tap = function(data) {
        if (addedListeners) return false;
        addedListeners = true;

        var hitObj = this;
        if (hitObj.selectedAlphabet == self.Answer) { //Correnct Answer
            correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
            self.showRedOrGreenBox(textPositions[hitObj.hitIndex * 2], textPositions[hitObj.hitIndex * 2 + 1], self.greenRect);
            setTimeout(self.correctFeedback.bind(self), 200);

        } else { //wrong answer
            wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
            self.showRedOrGreenBox(textPositions[hitObj.hitIndex * 2], textPositions[hitObj.hitIndex * 2 + 1], self.redRect);
            setTimeout(self.wrongFeedback.bind(self), 200);
        }
    }

}

/**
 * create image sprites and audios dynamically
 */
Toy_2_1.prototype.runExercise = function() {
    var self = this;

    for (var k = self.imageContainer.children.length - 1; k >= 0; k--) {
        self.imageContainer.removeChild(self.imageContainer.children[k]);
    }

    var jsonData = this.wallSlices[0];

    //render alphabets in the box start
    self.Answer = jsonData.answer;
    var imageSprite = jsonData.image;

    /*imageSprite.position = {
        x: 726 + (339 - 300) / 2,
        y: 89 + (339 - 300) / 2
    };


    imageSprite.height = imageSprite.width = 300;*/
    imageSprite.width = imageSprite.height = 339;
    imageSprite.position = {
        x: 726,
        y: 89
    };

    main.texturesToDestroy.push(imageSprite);
    self.imageContainer.addChild(imageSprite);
    imageSprite = null;

    var imageSoundTexture = PIXI.Sprite.fromFrame("sound_grey.png");
    imageSoundTexture.position = {
        x: 726,
        y: 89 + 289
    };

    imageSoundTexture.interactive = true;
    imageSoundTexture.buttonMode = true;
    imageSoundTexture.howl = jsonData.sound;

    self.imageContainer.addChild(imageSoundTexture);

    imageSoundTexture.click = imageSoundTexture.tap = this.onSoundPressed.bind(imageSoundTexture);

    //render alphabets in the box end

    this.alphabetContainer.addChild(this.greenRect);
    this.alphabetContainer.addChild(this.redRect);
    this.greenRect.visible = false;
    this.redRect.visible = false;

};

Toy_2_1.prototype.onSoundPressed = function() {
    createjs.Sound.stop();
    this.soundInstance = createjs.Sound.play(this.howl, {
        interrupt: createjs.Sound.INTERRUPT_NONE
    });
    this.soundInstance.addEventListener("failed", handleFailed);
    this.soundInstance.addEventListener("complete", handleComplete);
    addedListeners = false;
};

/**
 * on correct answer, feedback increment by 1
 */
Toy_2_1.prototype.correctFeedback = function() {
    var self = this;
    var feedback = localStorage.getItem('toy_2_1');
    feedback++;
    localStorage.setItem('toy_2_1', feedback);
    self.updateFeedback();
    if (feedback >= Toy_2_1.totalFeedback) {
        setTimeout(self.onComplete.bind(self), 1200);
    } else {
        self.returnWallSprites();
        self.borrowWallSprites(1);
        self.runExercise();
    }
    addedListeners = false;
}

/**
 * on wrong feedback, feedback decrement by 1
 */
Toy_2_1.prototype.wrongFeedback = function() {
    var self = this;
    var feedback = localStorage.getItem('toy_2_1');
    feedback--;

    if (feedback < 0)
        feedback = 0;

    localStorage.setItem('toy_2_1', feedback);

    self.updateFeedback();
    addedListeners = false;
}

/**
 * update feedback on correct/wrong answer
 */
Toy_2_1.prototype.updateFeedback = function() {

    var self = this;
    var counter = localStorage.getItem('toy_2_1');

    self.showHideChildrenInContainer(self.feedbackContainer, false);
    self.showHideChildrenInContainer(self.robotPartsContainer, true);

    for (var j = 0; j < counter; j++) {
        self.feedbackContainer.children[j].visible = true;
        self.robotPartsContainer.children[j].visible = false;
    }
}


/**
 * load outro sceene
 */
Toy_2_1.prototype.onComplete = function() {

    var self = this;

    this.introContainer.visible = false;
    this.outroContainer.visible = true;

    this.introMode = false;

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        self.cancelOutro();
        self.restartExercise();
    }

    this.outroAnimation();

}

/**
 * reset exercise to default values
 */
Toy_2_1.prototype.resetExercise = function() {
    var self = this;
    this.ovaigen.visible = false;
    localStorage.setItem('toy_2_1', 0);

    self.introContainer.visible = true;
    self.outroContainer.visible = false;
    for (var k = 0; k < self.robotPartsContainer.children.length; k++) {
        self.robotPartsContainer.children[k].visible = true;
    }

    this.introMode = true;
};

/**
 * object pooling to get borrow 2 sets at the begining of exercise
 * create object pool that provides us more flexibility to preload upcoming sets
 * @param  {string} group
 */
Toy_2_1.prototype.initObjectPooling = function(group) {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];
    self.pool = null;
    self.pool = {};
    var slicedGroup = group.split('');

    self.pool = new ExerciseSpritesPool(self.jsonContents.json[group]);
    self.generatePoolCategory();
    self.borrowWallSprites(2);


    for (var k = 0; k < slicedGroup.length; k++) {
        var t = k;
        var alphabetsHowl = loadSound('uploads/audios/alphabets/small/' + StripExtFromFile(slicedGroup[k].toLowerCase()));

        self.alphaHowls.push(alphabetsHowl);

        if (slicedGroup[k].toLowerCase() == "a" || slicedGroup[k].toLowerCase() == "e" || slicedGroup[k].toLowerCase() == "i" || slicedGroup[k].toLowerCase() == "o") {
            var alphabetsVowelHowl = loadSound('uploads/audios/alphabets/capital/' + StripExtFromFile(slicedGroup[k]));

        } else {
            var alphabetsVowelHowl = false;
        }

        self.alphaVowelHowls.push(alphabetsVowelHowl);

    }

    self.runExercise();
}


Toy_2_1.prototype.generatePoolCategory = function() {

        var keyArr = [];

        var spriteArr = this.pool.sprites,
            spriteLen = spriteArr.length;

        for (var i = 0; i < spriteLen; i++) {
            keyArr.push(spriteArr[i][0].word);
        };


        this.poolCategory = keyArr.filter(onlyUnique);

        this.poolIsNotArray = true;
        this.poolCategoryType = "word";
    }
    /**
     * borrow number of objects from pool
     * preload audios, images to work in the exercise
     * @param  {int} num
     */
Toy_2_1.prototype.borrowWallSprites = function(num) {

    var rand, category, sprite, spriteObj;

    var self = this;
    this.num = num;

    for (var i = 0; i < this.num; i++) {
        //var sprite = this.pool.borrowSprite();
        rand = makeUniqueRandom(this.poolCategory.length);
        this.borrowCategory = this.poolCategory[rand];

        sprite = this.pool.borrowUniqueSprite(this.borrowCategory, this.lastBorrowSprite, this.poolCategoryType, this.poolIsNotArray);

        this.poolSlices.push(sprite);

        var spriteObj = sprite[0];
        this.lastBorrowSprite = spriteObj.word;
        var howl = loadSound('uploads/audios/' + StripExtFromFile(spriteObj.sound));

        this.wallSlices.push({
            word: spriteObj.word,
            image: PIXI.Sprite.fromImage('uploads/images/' + spriteObj.image),
            sound: howl,
            answer: spriteObj.answer
        });
    }
};

/**
 * return used objects to pool
 */
Toy_2_1.prototype.returnWallSprites = function() {
    var self = this;
    var sprite = this.poolSlices[0];

    this.pool.returnSprite(sprite);

    this.poolSlices = [];
    this.wallSlices.shift();
};

/**
 * restart exercise by resetting the game and fetch new sets from pool
 */
Toy_2_1.prototype.restartExercise = function() {

    var self = this;

    this.resetExercise();

    localStorage.setItem('toy_2_1', 0);
    self.showHideChildrenInContainer(self.feedbackContainer, false);
    this.imageContainer.removeChild(this.imageContainer.children[0]);
    this.returnWallSprites();
    this.borrowWallSprites(1);
    this.runExercise();
};

/**
 * unload used memory from exercise properties
 */
Toy_2_1.prototype.cleanMemory = function() {

    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    this.animationOutroMouth = null;
    this.whiteBoxPositions = null;

    if (this.greenRect)
        this.greenRect.clear();

    if (this.redRect)
        this.redRect.clear();

    this.help = null;
    this.ovaigen = null;

    if (this.poolSlices)
        this.poolSlices.length = 0;

    if (this.wallSlices)
        this.wallSlices.length = 0;

    this.pool = null;
    this.loader = null;

    this.sceneContainer = null;
    this.introContainer = null;
    this.outroContainer = null;
    this.feedbackContainer = null;
    this.alphabetContainer = null;
    this.animationContainer = null;
    this.robotPartsContainer = null;
    this.exerciseAlphabetContainer = null;
    this.imageContainer = null;
    this.alphaHowls = null;
    this.alphaVowelHowls = null;
    this.defaultGroup = null;
    this.Answer = null;

    this.answer = null;
    this.jsonContents = null;
    this.torsoObject = null;
    this.deskObject = null;
    this.headRestPosition = null;
    this.headUpPosition = null;
    this.animateArms = null;


    this.outroBgLines = null;
    this.outroCharacter = null;

    if (this.outroMouthParams)
        this.outroMouthParams.length = 0;

    this.baseContainer.removeChildren();
    this.baseContainer = null;
}

/**
 * red or green highlight visibility based on correct and wrong answer
 * for certain interval of time only
 * @param  {int} xPos
 * @param  {int} yPos
 * @param  {object} obj
 */
Toy_2_1.prototype.showRedOrGreenBox = function(xPos, yPos, obj) {
    var self = this;

    obj.position = {
        x: xPos,
        y: yPos
    };

    obj.visible = true;

    setTimeout(function() {
        obj.visible = false;
    }, 400);


}