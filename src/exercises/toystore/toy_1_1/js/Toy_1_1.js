function Toy_1_1(stage) {
    cl.show();

    PIXI.DisplayObjectContainer.call(this);
    main.CurrentExercise = "toy_1_1";
    this.baseContainer = new PIXI.DisplayObjectContainer();

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();
    this.feedbackContainer = new PIXI.SpriteBatch();
    this.MusicBoxLineContainer = new PIXI.SpriteBatch();
    this.alphabetContainer = new PIXI.DisplayObjectContainer();
    this.animationContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position = {
        x: 77,
        y: 88
    };

    localStorage.setItem('toy_1_1', 0);

    this.baseContainer.addChild(this.sceneContainer);

    main.texturesToDestroy = [];

    this.loadSpriteSheet();

    this.introBeingPlayed = false;

    // everything connected to stage is rendered.
    this.addChild(this.baseContainer);

    this.soundPlayed = false;
}

Toy_1_1.constructor = Toy_1_1;
Toy_1_1.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Toy_1_1.imagePath = 'build/exercises/toystore/toy_1_1/images/';
Toy_1_1.audioPath = 'build/exercises/toystore/toy_1_1/audios/';
Toy_1_1.commonFolder = 'build/common/images/toystore/';
Toy_1_1.totalFeedback = Main.TOTAL_FEEDBACK || 10; // default 10

/**
 * preload audios,sprites and images
 * @return {[type]} [description]
 */
Toy_1_1.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(Toy_1_1.audioPath + 'intro');
    this.outroId = loadSound(Toy_1_1.audioPath + 'outro');
    // create an array of assets to load
    var assetsToLoad = [
        Toy_1_1.imagePath + "sprite_toystore_1_1_1.json",
        Toy_1_1.imagePath + "sprite_toystore_1_1_1.png",
        Toy_1_1.imagePath + "sprite_toystore_1_1_2.json",
        Toy_1_1.imagePath + "sprite_toystore_1_1_2.png",
        Toy_1_1.imagePath + "musicbox_outro.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * on all assets loaded, load default functions to run the game.
 */
Toy_1_1.prototype.spriteSheetLoaded = function() {
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    this.loadDefaults();
    this.renderFeedBacks();
    this.renderWhiteBoxes();

    this.initObjectPooling();

    this.introContainer.addChild(this.alphabetContainer);
    this.introContainer.addChild(this.feedbackContainer);
    this.introContainer.addChild(this.MusicBoxLineContainer);
    this.introContainer.addChild(this.animationContainer);

}

/**
 * show hide all feedback based on opacity
 */
Toy_1_1.prototype.showHideAllFeedBacks = function() {

    for (var j = 0; j < this.feedbackContainer.children.length; j++) {
        this.feedbackContainer.children[j].visible = true;
        this.feedbackContainer.children[j].alpha = 0.2;
    }
}

/**
 * render white boxes
 */
Toy_1_1.prototype.renderWhiteBoxes = function() {
    this.whiteBoxPositions = [
        162, 225,
        537, 225,
        162, 595,
        537, 595
    ];
    for (var j = 0; j < 4; j++) {

        var boxParams = {
            x: this.whiteBoxPositions[j * 2],
            y: this.whiteBoxPositions[j * 2 + 1],
            w: 313,
            h: 313,
            lineStyle: 1
        }
        var box = createRect(boxParams);
        this.introContainer.addChild(box);
    }

    this.greenRect = createRect({
        w: 313,
        h: 313,
        color: 0x00ff00,
        alpha: 0.6,
        lineStyle: 0
    });
    this.redRect = createRect({
        w: 313,
        h: 313,
        color: 0xff0000,
        alpha: 0.6,
        lineStyle: 0
    });
}

/**
 * render feedback before game start
 * at first the visiblility for feedback sprites is set to 20%
 */
Toy_1_1.prototype.renderFeedBacks = function() {
    var self = this;
    var feedBackPosition = [
        1070, 891,
        1069, 826,
        1067, 736,
        1069, 683,
        1067, 619,
        1070, 534,
        1079, 519,
        1059, 440,
        1097, 326
    ];

    for (var k = 0; k < 9; k++) {
        var feedbackSprite = PIXI.Sprite.fromFrame("fb_" + (k + 1 + 1) + ".png");


        feedbackSprite.position = {
            x: feedBackPosition[k * 2],
            y: feedBackPosition[k * 2 + 1]
        };

        main.texturesToDestroy.push(feedbackSprite);
        self.feedbackContainer.addChild(feedbackSprite);
        if (k == 8) { //add music box lines
            var musicBoxLines = PIXI.Sprite.fromFrame("lines_musicbox.png");
            musicBoxLines.position.set(1062, 531);
            main.texturesToDestroy.push(musicBoxLines);
            this.MusicBoxLineContainer.addChild(musicBoxLines);
        }
        feedbackSprite.visible = true;
        feedbackSprite.alpha = 0.2;
    }

    var feedBack1 = PIXI.Sprite.fromFrame("fb_1.png");
    feedBack1.position.set(1058, 918);
    main.texturesToDestroy.push(feedBack1);
    self.feedbackContainer.addChild(feedBack1);
    feedBack1.visible = false;
}

/**
 * draw background, create sprites and movie clip animation
 */
Toy_1_1.prototype.loadDefaults = function() {

    var self = this;


    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0xf9e5e4);
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xd6e4ae);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);

    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.outroContainer);
    this.outroContainer.visible = false;

    this.musicBox = PIXI.Sprite.fromFrame("musicbox_start.png");
    this.musicBox.position = {
        x: 1057,
        y: 107
    };
    main.texturesToDestroy.push(this.musicBox);
    this.introContainer.addChild(this.musicBox);

    this.mouthParams = {
        "image": "mouth_",
        "length": 4,
        "x": 963,
        "y": 630,
        "speed": 4,
        "pattern": true,
        "frameId": 3
    };
    this.animateMouth = new Animation(this.mouthParams);
    this.animateMouth.visible = false;

    this.animateIntroFigure = [
        new PIXI.Sprite.fromFrame('intro_1.png'),
        new PIXI.Sprite.fromFrame('intro_2.png'),
        new PIXI.Sprite.fromFrame('intro_3.png'),
    ];
    for (var i = 0; i < this.animateIntroFigure.length; i++) {
        this.animateIntroFigure[i].position.set(815, 317);
        this.animationContainer.addChild(this.animateIntroFigure[i]);
        this.animateIntroFigure[i].visible = false;
    }

    this.animationContainer.addChild(this.animateMouth);

    //sitting yello figure start
    this.sittingFigure = PIXI.Sprite.fromFrame("yellow_figure_sitting.png");
    this.sittingFigure.buttonMode = true;
    this.sittingFigure.interactive = true;
    this.sittingFigure.position = {
        x: 1376,
        y: 87
    };
    main.texturesToDestroy.push(this.sittingFigure);
    this.introContainer.addChild(this.sittingFigure);

    this.help = PIXI.Sprite.fromFrame("topcorner.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position = {
        x: -77,
        y: -88
    }
    this.sceneContainer.addChild(this.help);

    //OUTRO STUFF
    this.outroMusicbox = PIXI.Sprite.fromImage(Toy_1_1.imagePath + "musicbox_outro.png");
    this.outroMusicbox.position.set(499, 511);
    main.texturesToDestroy.push(this.outroMusicbox);
    this.outroContainer.addChild(this.outroMusicbox);

    this.outroFigureParams = {
        "image": "pos",
        "length": 5,
        "x": 553,
        "y": 54,
        "speed": 5,
        "pattern": false,
        "frameId": 0
    };
    this.animateOutroFigure = new Animation(this.outroFigureParams);

    this.outroContainer.addChild(this.animateOutroFigure);

    this.ovaigen = PIXI.Sprite.fromFrame("ovaigen.png");
    this.ovaigen.buttonMode = true;
    this.ovaigen.interactive = true;
    this.ovaigen.position = {
        x: 1265,
        y: 761
    };
    this.outroContainer.addChild(this.ovaigen);
    this.ovaigen.visible = false;

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.updateFeedback();

}

/**
 * figure visibility
 * @param  {int} number
 */
Toy_1_1.prototype.showFigureAt = function(number) {
    for (var i = 0; i < this.animateIntroFigure.length; i++) {
        this.animateIntroFigure[i].visible = false;
    }
    if (isNumeric(number))
        this.animateIntroFigure[number].visible = true;
};

/**
 * play intro animation
 */
Toy_1_1.prototype.introAnimation = function() {

    createjs.Sound.stop();

    clearTimeout(this.timeoutSittingFigure);

    this.introBeingPlayed = true;
    this.soundPlayed = false;

    if (!this.introMode)
        this.restartExercise();

    this.help.interactive = false;
    this.help.buttonMode = false;

    main.overlay.visible = true;

    this.introContainer.visible = true;
    this.outroContainer.visible = false;
    this.sittingFigure.visible = false;
    this.animateMouth.visible = true;
    this.help.interactive = false;
    this.help.buttonMode = false;

    this.animateMouth.show(true);

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
 * 0-1 seC YELLOW FIGURE STANDING
 * 1-3 SEC YELLOW_FIGURE_POINTINGATHERSELF
 * 3-5 SEC YELLOW_FIGURE_POINTING
 * 5-7 SEC YELLOW FIGURE STANDING *
 */
Toy_1_1.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    if (currentPostion >= 1000 && currentPostion < 3000) {
        this.showFigureAt(2);
    } else if (currentPostion >= 3000 && currentPostion < 6000) {
        this.showFigureAt(1);
    } else {
        this.showFigureAt(0);
    }
};

/**
 * cancel running intro animation
 */
Toy_1_1.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);

    this.animateMouth.hide(3);
    this.animateMouth.visible = false;
    this.showFigureAt();

    this.help.interactive = true;
    this.help.buttonMode = true;

    this.sittingFigure.visible = true;
    this.introBeingPlayed = false;

    this.timeoutSittingFigure = setTimeout(this.sittingFigurePlay.bind(this), 1000);

    this.soundPlayed = true;

    this.ovaigen.visible = false;
    main.overlay.visible = false;
};

/**
 * play outro animation
 */
Toy_1_1.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.help.interactive = false;
        self.help.buttonMode = false;
        self.animateOutroFigure.show(true);
    }

    this.cancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.cancelHandler);

};

/**
 * cancel running outro animation
 */
Toy_1_1.prototype.cancelOutro = function() {

    createjs.Sound.stop();
    this.animateOutroFigure.hide(5);
    this.ovaigen.visible = true;
    this.help.interactive = true;
    this.help.buttonMode = true;
    main.overlay.visible = false;

};

/**
 * exercise logic
 */
Toy_1_1.prototype.runExercise = function() {
    var self = this;

    var jsonData = this.wallSlices[0];
    //render alphabets in the box start
    var textPositions = [
        162, 225,
        537, 225,
        162, 595,
        537, 595
    ];
    var imageButtons = [];

    for (var k = 0; k < jsonData.alpha_big_small.length; k++) {
        var alphabets = new PIXI.Text(jsonData.alpha_big_small[k], {
            font: "100px Arial",
            align: "center",
        });

        alphabets.position = {
            x: textPositions[k * 2] + (313 - alphabets.width) / 2,
            y: textPositions[k * 2 + 1] + 105
        };
        self.alphabetContainer.addChild(alphabets);
        var imgsprite = PIXI.Sprite.fromFrame("interactive.png");
        imgsprite.interactive = true;
        imgsprite.buttonMode = true;
        imgsprite.height = 313;
        imgsprite.width = 313;
        imgsprite.hitIndex = k;
        imgsprite.alphabetsHowl = jsonData.alphaSound[k];
        imgsprite.position = {
            x: textPositions[k * 2],
            y: textPositions[k * 2 + 1]
        };

        self.alphabetContainer.addChild(imgsprite);

        imageButtons.push(imgsprite);

    }

    imageButtons[0].click = imageButtons[0].tap = imageButtons[1].click = imageButtons[1].tap = imageButtons[2].click = imageButtons[2].tap = imageButtons[3].click = imageButtons[3].tap = function(data) {

        if (addedListeners) return false;
        if (!self.soundPlayed) return false;

        var hitObj = this;
        if (jsonData.alphabets[hitObj.hitIndex] == jsonData.answer) { //Correnct Answer
            correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
            self.showRedOrGreenBox(self.whiteBoxPositions[hitObj.hitIndex * 2], self.whiteBoxPositions[hitObj.hitIndex * 2 + 1], self.greenRect);
            setTimeout(self.correctFeedback.bind(self), 400);

        } else { //wrong answer
            wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
            self.showRedOrGreenBox(self.whiteBoxPositions[hitObj.hitIndex * 2], self.whiteBoxPositions[hitObj.hitIndex * 2 + 1], self.redRect);
            setTimeout(self.wrongFeedback.bind(self), 400);

        }
    }

    //render alphabets in the box end
    this.alphabetContainer.addChild(this.greenRect);
    this.alphabetContainer.addChild(this.redRect);
    this.greenRect.visible = false;
    this.redRect.visible = false;

    this.sittingFigure.howl = jsonData.sound;
    this.sittingFigure.click = this.sittingFigure.tap = this.sittingFigurePlay.bind(this);

    this.timeoutSittingFigure = setTimeout(this.sittingFigurePlay.bind(this), 2000);


};

/**
 * on correct feedback, increment by 1
 * borrow next set to continue exercise
 */
Toy_1_1.prototype.correctFeedback = function() {
    var self = this;
    var feedback = localStorage.getItem('toy_1_1');
    feedback++;
    localStorage.setItem('toy_1_1', feedback);
    self.updateFeedback();
    if (feedback >= Toy_1_1.totalFeedback) {
        self.onComplete(); // go to outro scene
    } else {
        self.alphabetContainer.removeChildren();
        self.soundPlayed = false;
        self.returnWallSprites();
        self.borrowWallSprites(1);
        self.runExercise();
    }
};

/**
 * on wrong feedback, decrement by 1
 */
Toy_1_1.prototype.wrongFeedback = function() {
    var self = this;
    var feedback = localStorage.getItem('toy_1_1');
    feedback--;

    if (feedback < 0)
        feedback = 0;

    localStorage.setItem('toy_1_1', feedback);

    self.updateFeedback();
};

/**
 * play sitting figure sound
 */
Toy_1_1.prototype.sittingFigurePlay = function() {
    var self = this;

    if (this.introBeingPlayed == true) return false;
    if (addedListeners) return false;

    addedListeners = true;
    var soundInstance = createjs.Sound.play(self.sittingFigure.howl, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });
    soundInstance.addEventListener("failed", handleFailed);
    soundInstance.addEventListener("complete", function() {
        self.soundPlayed = true;
        addedListeners = false;
    });
};

/**
 * load outro scene
 */
Toy_1_1.prototype.onComplete = function() {

    var self = this;

    this.introMode = false;

    this.introContainer.visible = false;
    this.outroContainer.visible = true;

    this.outroAnimation();

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        self.cancelOutro();
        self.resetExercise();
        self.restartExercise();
    }
}

/**
 * reset exercise to default values
 */
Toy_1_1.prototype.resetExercise = function() {

    var self = this;

    this.ovaigen.visible = false;
    localStorage.setItem('toy_1_1', 0);

    self.introContainer.visible = true;
    self.outroContainer.visible = false;

    this.introMode = true;

};

/**
 * update specific feedback sprite
 */
Toy_1_1.prototype.updateFeedback = function() {

    var self = this;
    var counter = localStorage.getItem('toy_1_1');

    self.showHideAllFeedBacks();

    if (counter >= 1) {
        self.feedbackContainer.children[9].alpha = 1;
    }
    for (var j = 0; j < counter - 1; j++) {
        self.feedbackContainer.children[j].alpha = 1;

    }
}

/**
 * init object pooling from exercise pool
 * to get 2 sets for gameplay
 */
Toy_1_1.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/toystore/toystore_1_1.json?noache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        self.pool = new ExerciseSpritesPool(loader.json);
        //borrow two sprites once
        self.borrowWallSprites(2);
        self.runExercise();

    });
    loader.load();
}

/**
 * preload sprites , sounds
 */
Toy_1_1.prototype.borrowWallSprites = function(num) {

    var self = this;
    this.num = num;

    for (var i = 0; i < this.num; i++) {
        var sprite = this.pool.borrowSprite();
        this.poolSlices.push(sprite);
        var alphaArray = [];
        var alphaBigSmallArray = [];
        var alphaSoundArray = [];

        var spriteObj = sprite[0];

        for (var j = 0; j < spriteObj.utf8_safe_alphabets.length; j++) {
            alphaArray.push(spriteObj.alphabets[j]);
            alphaBigSmallArray.push(spriteObj.alphabets_to_show[j]);
            var alphabetHowl = loadSound('uploads/audios/alphabets/capital/' + spriteObj.utf8_safe_alphabets[j]);
            alphaSoundArray.push(alphabetHowl);
        }

        var howl = loadSound('uploads/audios/alphabets/capital/' + StripExtFromFile(spriteObj.answer.audio));

        shuffle(alphaArray, alphaBigSmallArray, alphaSoundArray);

        this.wallSlices.push({
            alphabets: alphaArray,
            alpha_big_small: alphaBigSmallArray,
            sound: howl,
            answer: spriteObj.answer.alphabet,
            alphaSound: alphaSoundArray
        });
    }
};

/**
 * return used objects to pool to reuse later
 */
Toy_1_1.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];

    this.pool.returnSprite(sprite);

    this.alphabetContainer.removeChildren();

    this.poolSlices = [];
    this.wallSlices.shift();
};

/**
 * restart exercise by reset followed by clearing container
 * get next set for game play
 */
Toy_1_1.prototype.restartExercise = function() {

    var self = this;

    this.resetExercise();

    localStorage.setItem('toy_1_1', 0);

    self.showHideAllFeedBacks();

    this.alphabetContainer.removeChildren();

    this.returnWallSprites();
    this.borrowWallSprites(1);
    this.runExercise();
};

/**
 * unload destroy object , clear timeouts, clear graphics
 * for memory management purpose
 */
Toy_1_1.prototype.cleanMemory = function() {

    clearTimeout(this.timeoutSittingFigure);

    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    this.introBeingPlayed = null;

    this.person = null;

    if (this.mouthParams)
        this.mouthParams.length = 0;

    this.animateMouth = null;
    this.animateOutroFigure = null;

    if (this.greenRect)
        this.greenRect.clear();

    if (this.redRect)
        this.redRect.clear();

    this.help = null;
    this.ovaigen = null;

    if (this.poolSlices)
        this.poolSlices.length = 0;

    if (this.wallSlices)
        this.wallSlices.length = 0;

    this.pool = null;

    this.sceneContainer = null;
    this.introContainer = null;
    this.outroContainer = null;
    this.feedbackContainer = null;
    this.alphabetContainer = null;
    this.animationContainer = null;
    this.MusicBoxLineContainer = null;

    this.baseContainer.removeChildren();
    this.baseContainer = null;
}

/**
 * show red or green highlight in white sprite for 400ms
 * @param  {int} xPos
 * @param  {int} yPos
 * @param  {object} obj
 */
Toy_1_1.prototype.showRedOrGreenBox = function(xPos, yPos, obj) {
    var self = this;

    obj.position = {
        x: xPos,
        y: yPos
    };

    obj.visible = true;

    setTimeout(function() {
        obj.visible = false;
    }, 400);


}