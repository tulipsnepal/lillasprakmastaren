function Toystore() {

    cl.show();

    PIXI.DisplayObjectContainer.call(this);

    this.tivoliContainer = new PIXI.DisplayObjectContainer();

    this.sceneContainer = new PIXI.DisplayObjectContainer();

    this.imageContainer = new PIXI.DisplayObjectContainer();

    this.menuContainer = new PIXI.DisplayObjectContainer();

    this.tivoliContainer.addChild(this.sceneContainer);
    this.tivoliContainer.addChild(this.menuContainer);

    //resize container
    this.imageContainer.position = {
        x: 0,
        y: -20
    }

    this.menuContainer.position = {
        x: 0,
        y: 1287 - 36
    }

    this.loadSpriteSheet();

    this.addChild(this.tivoliContainer);
}

Toystore.constructor = Toystore;
Toystore.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

Toystore.IMAGEPATH = "build/exercises/toystore/toy_0/images/";
Toystore.COMMONFOLDER = 'build/common/images/toystore/';

Toystore.prototype.loadSpriteSheet = function() {

    // create an array of assets to load
    var assetsToLoad = [
        Toystore.IMAGEPATH + "background_toystore.png",
        Toystore.COMMONFOLDER + 'menuskeleton.png',
        Toystore.COMMONFOLDER + 'sprite_toystore.json',
        Toystore.COMMONFOLDER + 'sprite_toystore.png',
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(assetsToLoad);

    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);

    //begin load
    loader.load();

}

Toystore.prototype.spriteSheetLoaded = function() {

    cl.hide();

    // get backgroudn for tivoli page
    this.bkg1 = new Background(Toystore.IMAGEPATH + "background_toystore.png");
    this.sceneContainer.addChild(this.bkg1);

    this.loadMenus();

    this.imageButtons();

}


Toystore.prototype.homePage = function() {
    UnloadScene.apply(this);
    localStorage.removeItem('exercise');
    // createjs.Sound.removeAllSounds();
    history.back();
}

Toystore.prototype.sceneChange = function(scene) {

    var prevExercise = localStorage.getItem('exercise');

    if (scene === prevExercise)
        return false;

    UnloadScene.apply(this);

    main.CurrentExercise = scene;

    localStorage.setItem('exercise', scene);

    main.page = new window[scene];
    main.page.introMode = true

    loadSound('build/common/audios/' + main.CurrentExercise + "_help");

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0x000000);

    this.sceneContainer.addChild(main.page);

    this.cleanMemory();

};

Toystore.prototype.loadMenus = function() {

    // position of 1 and 2 sub menu
    var x1 = 68,
        x2 = 191,
        y = -6;

    this.menuParams = {
        path: Toystore.COMMONFOLDER,
        background: 'menuskeleton.png',
        images: [
            'tillbaka.png',
            'bokstaverna.png',
            'a-till-p.png',
            'q-till-o.png',
            'hela.png'
        ],
        positions: [
            36, 134,
            300, 123,
            707, 80,
            1019, 95,
            1414, 114
        ],
        data: {
            1: {
                hover_image: 'menu1hover.png',
                hover_position: [310, -23],
                submenu_image: [
                    'sub_menu_1.png',
                    'sub_menu_2.png',
                ],
                submenu_position: [
                    310 + x1, y,
                    310 + x2, y
                ]

            },
            2: {
                hover_image: 'menu2hover.png',
                hover_position: [685, -23],
                submenu_image: [
                    'sub_menu_1.png',
                    'sub_menu_2.png',
                ],
                submenu_position: [
                    685 + x1, y,
                    685 + x2, y
                ]
            },
            3: {
                hover_image: 'menu3hover.png',
                hover_position: [1028, -23],
                submenu_image: [
                    'sub_menu_1.png',
                    'sub_menu_2.png',
                ],
                submenu_position: [
                    1028 + x1, y,
                    1028 + x2, y
                ]
            },
            4: {
                hover_image: 'menu4hover.png',
                hover_position: [1405, -23],
                submenu_image: [
                    'sub_menu_1.png',
                    'sub_menu_2.png',
                ],
                submenu_position: [
                    1405 + x1, y,
                    1405 + x2, y
                ]
            },
            pink_image: 'selected_submenu.png',
            move_position: {
                x: 10,
                y: 32
            },
            chop: { // optional
                images: [
                    'robotwa.png',
                    'rabbits.png',
                    'puzzle.png'
                ],
                positions: [
                    707, 80,
                    1019, 95,
                    1414, 114
                ]
            }
        }

    };
    this.menu = new Menu(this.menuParams);

    this.menuContainer.addChild(this.menu);

    //tillbaka button
    this.menu.getChildAt(1).click = this.menu.getChildAt(1).tap = this.homePage.bind(this);

    //bokstavernai alfabet button
    this.menu.getChildAt(2).click = this.menu.getChildAt(2).tap = this.navMenu1.bind(this);

    //A till P
    this.menu.getChildAt(3).click = this.menu.getChildAt(3).tap = this.navMenu2.bind(this);

    //Q till Ö
    this.menu.getChildAt(4).click = this.menu.getChildAt(4).tap = this.navMenu3.bind(this);

    //Hela alfabetet
    this.menu.getChildAt(5).click = this.menu.getChildAt(5).tap = this.navMenu4.bind(this);


}

Toystore.prototype.imageButtons = function() {

    this.navPositions = [
        1199, 161,
        19, 550,
        735, 479,
        1017, 952
    ];

    this.navButtons = [];

    for (var i = 0; i < this.navPositions.length; i++) {
        var imageSprite = PIXI.Sprite.fromFrame('ova.png');

        imageSprite.buttonMode = true;
        imageSprite.position.x = this.navPositions[i * 2];
        imageSprite.position.y = this.navPositions[i * 2 + 1];
        imageSprite.interactive = true;

        this.imageContainer.addChild(imageSprite);

        this.navButtons.push(imageSprite);
        imageSprite = null;
    }

    //Hela alfabetet image button
    this.navButtons[3].click = this.navButtons[3].tap = this.navMenu4.bind(this);

    //A till P image button
    this.navButtons[1].click = this.navButtons[1].tap = this.navMenu2.bind(this);

    //Bokstäverna i alfabetet image button
    this.navButtons[0].click = this.navButtons[0].tap = this.navMenu1.bind(this);

    //Q till Ö  image button
    this.navButtons[2].click = this.navButtons[2].tap = this.navMenu3.bind(this);



    this.sceneContainer.addChild(this.imageContainer);

}

Toystore.prototype.navMenu = function(submenu1, submenu2, index) {

    this.sceneChange(submenu1);

    var self = this;

    this.pushStuffToHistory("history_other_Toystore");

    this.subMenu = self.menu.getActiveMenu(index);
    self.menu.moveSelectedExerciseCircle(this.subMenu[0].position);
    self.sceneContainer.addChild(self.menuContainer);

    this.subMenu[0].click = this.subMenu[0].tap = function(data) {
        self.sceneChange(submenu1);

        self.menu.moveSelectedExerciseCircle(this.position);
        self.sceneContainer.addChild(self.menuContainer);

    }
    this.subMenu[1].click = this.subMenu[1].tap = function(data) {
        self.sceneChange(submenu2);

        self.menu.moveSelectedExerciseCircle(this.position);
        self.sceneContainer.addChild(self.menuContainer);
    }

}

Toystore.prototype.navMenu1 = function() {
    this.navMenu('Toy_1_1', 'Toy_1_2', 1);
}


Toystore.prototype.navMenu2 = function() {
    this.navMenu('Toy_2_1', 'Toy_2_2', 2);
}

Toystore.prototype.navMenu3 = function() {
    this.navMenu('Toy_3_1', 'Toy_3_2', 3);
}

Toystore.prototype.navMenu4 = function() {
    this.navMenu('Toy_4_1', 'Toy_4_2', 4);
}


Toystore.prototype.pushStuffToHistory = function(key) {

    if (Device.ieVersion != 9) {
        var exHistory = new History();
        exHistory.PushState({
            scene: key
        });
        exHistory = null;
    }
}

Toystore.prototype.cleanMemory = function() {
    main.eventRunning = false;
}