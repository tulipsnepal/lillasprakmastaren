function Toy_2_2(stage) {

    cl.show();

    PIXI.DisplayObjectContainer.call(this);
    main.CurrentExercise = "toy_2_2";
    this.baseContainer = new PIXI.DisplayObjectContainer();

    this.sceneContainer = new PIXI.DisplayObjectContainer();

    this.feedbackContainer = new PIXI.SpriteBatch();
    this.rhymeContainer = new PIXI.DisplayObjectContainer();
    this.animationContainer = new PIXI.DisplayObjectContainer();

    this.sceneContainer.position = {
        x: 77,
        y: 88
    };

    localStorage.setItem('toy_2_2', 0);

    this.baseContainer.addChild(this.sceneContainer);

    main.texturesToDestroy = [];

    this.loadSpriteSheet();

    // everything connected to stage is rendered.
    this.addChild(this.baseContainer);
}

Toy_2_2.constructor = Toy_2_2;
Toy_2_2.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Toy_2_2.imagePath = 'build/exercises/toystore/toy_2_2/images/';
Toy_2_2.audioPath = 'build/exercises/toystore/toy_2_2/audios/';
Toy_2_2.commonFolder = 'build/common/images/toystore/';
Toy_2_2.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

/**
 * preload assetes, images and audios by assetloader
 */
Toy_2_2.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(Toy_2_2.audioPath + 'intro');
    this.outroId = loadSound(Toy_2_2.audioPath + 'outro');

    // create an array of assets to load
    var assetsToLoad = [
        Toy_2_2.imagePath + "sprite_toystore_2_2.json",
        Toy_2_2.imagePath + "sprite_toystore_2_2.png",
        Toy_2_2.imagePath + "marching_robots.json",
        Toy_2_2.imagePath + "marching_robots.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * on all assets loaded , load exercise
 */
Toy_2_2.prototype.spriteSheetLoaded = function() {
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    this.loadDefaults();
    this.renderMarchingRobots();
    this.initObjectPooling();

    this.sceneContainer.addChild(this.rhymeContainer);
    this.sceneContainer.addChild(this.animationContainer);
}

/**
 * create feedback sprite with 20% visibility
 */
Toy_2_2.prototype.renderFeedBack = function() {
    var self = this;
    for (var k = 0; k < 10; k++) {
        var feedBackSprite = PIXI.Sprite.fromFrame("fb.png");
        feedBackSprite.position = {
            x: 342 + (k * 118),
            y: 12
        };
        main.texturesToDestroy.push(feedBackSprite);
        self.feedbackContainer.addChild(feedBackSprite);
        feedBackSprite.alpha = 0.2;
    }
};


/**
 * render marching robots movie clip animation
 */
Toy_2_2.prototype.renderMarchingRobots = function() {
    var self = this;
    var robotsParams = {
        "image": "mr_",
        "length": 11,
        "x": 124,
        "y": 172,
        "speed": 3,
        "pattern": false,
        "frameId": 0
    };
    this.marchRobots = new Animation(robotsParams);
    this.marchRobots.gotoAndStop(0);
    this.sceneContainer.addChild(this.marchRobots);
    this.marchRobots.visible = false;
};

/**
 * load exercise defautls like background, intro and outro scene sprites and movie clip
 */
Toy_2_2.prototype.loadDefaults = function() {

    var self = this;

    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0xC9D0E8);
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xF4CDE2);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);


    this.person = PIXI.Sprite.fromFrame("robot.png");
    this.person.position = {
        x: 1274,
        y: 491
    }
    main.texturesToDestroy.push(this.person);
    this.sceneContainer.addChild(this.person);

    this.robotFacingUser = PIXI.Sprite.fromFrame("robotfacinguser-withouttaprecorder.png");
    this.robotFacingUser.position = {
        x: 1274,
        y: 491
    }
    main.texturesToDestroy.push(this.robotFacingUser);
    this.sceneContainer.addChild(this.robotFacingUser);
    this.robotFacingUser.visible = false;

    this.defaultArms = PIXI.Sprite.fromFrame("arm_default.png");
    this.defaultArms.position = {
        x: 1151,
        y: 748
    }

    main.texturesToDestroy.push(this.defaultArms);
    this.animationContainer.addChild(this.defaultArms);

    this.renderFeedBack();

    this.hands = [
        new PIXI.Sprite.fromFrame('arm_0.png'),
        new PIXI.Sprite.fromFrame('arm_1.png'),
        new PIXI.Sprite.fromFrame('arm_2.png'),
        new PIXI.Sprite.fromFrame('arm_3.png'),
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(1151, 447);
        this.animationContainer.addChild(this.hands[i]);
        this.hands[i].visible = false;
    }



    this.help = PIXI.Sprite.fromFrame("topcorner.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position = {
        x: -77,
        y: -88
    }
    this.animationContainer.addChild(this.help);

    this.ovaigen = PIXI.Sprite.fromFrame("ovaigen.png");
    this.ovaigen.buttonMode = true;
    this.ovaigen.interactive = true;
    this.ovaigen.position = {
        x: 1352,
        y: 897
    };
    this.animationContainer.addChild(this.ovaigen);
    this.ovaigen.visible = false;

    this.outroRobot = PIXI.Sprite.fromFrame("robotbodywithtaperecorder.png");

    this.outroRobot.position = {
        x: 817,
        y: 381
    };
    this.animationContainer.addChild(this.outroRobot);
    this.outroRobot.visible = false;

    this.help.click = this.help.tap = this.introAnimation.bind(this);


    this.sceneContainer.addChild(this.feedbackContainer);
    this.updateFeedback();

    this.greenRect = createRect({
        w: 284,
        h: 284,
        color: 0x00ff00,
        alpha: 0.6,
        lineStyle: 0
    });
    this.redRect = createRect({
        w: 284,
        h: 284,
        color: 0xff0000,
        alpha: 0.6,
        lineStyle: 0
    });

    this.whiteLayer = createRect({
        x: 1842,
        y: 172,
        w: 224,
        h: 220,
        color: 0xFFFFFF
    });
    this.animationContainer.addChild(this.whiteLayer);
    this.pinkLayer = createRect({
        x: 1842,
        y: 172,
        w: 3,
        h: 220,
        color: 0xF4CDE2
    }); //
    this.animationContainer.addChild(this.pinkLayer);
}

/**
 * target hand visibility
 * @param  {int} number
 */
Toy_2_2.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    }
    if (isNumeric(number))
        this.hands[number].visible = true;
};

/**
 * play intro animation
 */
Toy_2_2.prototype.introAnimation = function() {

    createjs.Sound.stop();

    if (!this.introMode)
        this.restartExercise();

    this.help.interactive = false;
    this.help.buttonMode = false;

    this.person.visible = false;
    this.robotFacingUser.visible = true;

    main.overlay.visible = true;

    this.defaultArms.visible = false;
    this.showHandAt(0);

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
1-4 SEC. ARM_0
4-7 SEC ARM_1
7-9 SEC ARM_2
9-12 SEC ARM_3
STOP AT ARM_1
 */
Toy_2_2.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    if (currentPostion >= 4000 && currentPostion < 8000) {
        this.showHandAt(1);
    } else if (currentPostion >= 8000 && currentPostion < 9500) {
        console.log(currentPostion);
        this.showHandAt(2);
    } else if (currentPostion >= 9500 && currentPostion < 12000) {
        this.showHandAt(3);
    } else {
        this.showHandAt(1);
    }
};

/**
 * cancel running intro animation
 */
Toy_2_2.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);
    this.showHandAt();

    this.defaultArms.visible = true;

    this.robotFacingUser.visible = false;
    this.person.visible = true;

    this.help.interactive = true;
    this.help.buttonMode = true;
    this.feedbackContainer.visible = true;

    this.ovaigen.visible = false;
    main.overlay.visible = false;
}

/**
 * play outro animation
 */
Toy_2_2.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.marchRobots.showOnce(0);
        var b = setTimeout(function() {
            self.marchRobots.visible = false;
        }, 3000);

        self.help.interactive = false;
        self.help.buttonMode = false;
    }
    this.outroCancelHanlder = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.outroCancelHanlder);
};

/**
 * cancel running outro animation
 */
Toy_2_2.prototype.cancelOutro = function() {

    var self = this;

    createjs.Sound.stop();
    self.marchRobots.visible = false;

    self.ovaigen.visible = true;

    self.help.interactive = true;
    self.help.buttonMode = true;
    main.overlay.visible = false;

};

/**
 * exercise logic
 */
Toy_2_2.prototype.runExercise = function() {
    var self = this;
    var jsonData = this.wallSlices[0];
    var boxPositions = [
            332, 204,
            636, 204,
            940, 204,
            1245, 204,
            800, 554
        ],
        imageButtons = [],
        soundButtons = [],
        baseButtons = []

    for (var k = 0; k < 5; k++) {
        var rectParams = {
                x: boxPositions[k * 2],
                y: boxPositions[k * 2 + 1],
                w: 284,
                h: 284,
                lineStyle: 1
            },
            soundTexture = PIXI.Texture.fromFrame("sound_grey.png"),
            soundSprite = new PIXI.Sprite(soundTexture),
            spriteBase = createRect(rectParams);

        self.rhymeContainer.addChild(spriteBase);

        if (k == 4) //alphabet
        {
            var answerAlphabet = new PIXI.Text(jsonData.answer_alphabet + jsonData.answer_alphabet.toLowerCase(), {
                font: "150px Arial",
                fill: "black"
            });
            main.texturesToDestroy.push(answerAlphabet);
            answerAlphabet.position = {
                x: boxPositions[4 * 2] + (284 - answerAlphabet.width) / 2,
                y: boxPositions[4 * 2 + 1] + 50 + 10
            }
            self.rhymeContainer.addChild(answerAlphabet);

        } else {
            var imageSprite = jsonData.image[k];
            imageSprite.interactive = true;
            imageSprite.buttonMode = true;
            imageSprite.clickedAlphabet = jsonData.answer[k];
            imageSprite.answer = jsonData.answer_alphabet;
            imageSprite.buttonIndex = k;

            /*imageSprite.position = {
                x: boxPositions[k * 2] + (284 - 241) / 2,
                y: boxPositions[k * 2 + 1] + (284 - 241) / 2
            }*/
            imageSprite.position = {
                x: boxPositions[k * 2],
                y: boxPositions[k * 2 + 1]
            }
            imageSprite.width = imageSprite.height = 284;
            imageButtons.push(imageSprite);
            self.rhymeContainer.addChild(imageSprite);
        }



        soundSprite.position = {
            x: boxPositions[k * 2],
            y: boxPositions[k * 2 + 1] + 284 - 62
        }
        soundSprite.interactive = true;
        soundSprite.buttonMode = true;

        baseButtons.push(spriteBase);

        soundButtons.push(soundSprite);


        self.rhymeContainer.addChild(soundSprite);

        soundSprite.howl = jsonData.sound[k];
        soundSprite.vowelToPlay = jsonData.vowel_alphabet;
        soundSprite.index = k;

        soundSprite.click = soundSprite.tap = function(data) {
            var btnClicked = this;

            if (addedListeners) return false;
            addedListeners = true;
            if (btnClicked.vowelToPlay != null && typeof btnClicked.vowelToPlay !== "undefined" && btnClicked.index == 4) {
                var vowelSoundInstance = createjs.Sound.play(btnClicked.vowelToPlay);
                vowelSoundInstance.addEventListener("failed", handleFailed);
                vowelSoundInstance.addEventListener("complete", function() {
                    var c = setTimeout(function() {
                        var soundInstance = createjs.Sound.play(btnClicked.howl);
                        soundInstance.addEventListener("failed", handleFailed);
                        soundInstance.addEventListener("complete", function() {
                            addedListeners = false;
                        });
                    }, 300);
                    //addedListeners = false;
                });
            } else {
                var soundInstance = createjs.Sound.play(this.howl);
                soundInstance.addEventListener("failed", handleFailed);
                soundInstance.addEventListener("complete", function() {
                    addedListeners = false;
                });
            }

        }
    }


    for (var i = 0; i < 4; i++) {
        imageButtons[i].click = imageButtons[i].tap = function(data) {
            if (addedListeners) return false;
            addedListeners = true;
            if (this.clickedAlphabet == this.answer) {
                correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                self.showRedOrGreenBox(boxPositions[this.buttonIndex * 2], boxPositions[this.buttonIndex * 2 + 1], self.greenRect);
                setTimeout(self.correctFeedback.bind(self), 300);

            } else {
                wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                self.showRedOrGreenBox(boxPositions[this.buttonIndex * 2], boxPositions[this.buttonIndex * 2 + 1], self.redRect);
                setTimeout(self.wrongFeedback.bind(self), 300);
            }
        }
    }

    this.rhymeContainer.addChild(this.greenRect);
    this.rhymeContainer.addChild(this.redRect);
    this.greenRect.visible = false;
    this.redRect.visible = false;

};

/**
 * on correct feedback, feedback increment
 * get new sets
 */
Toy_2_2.prototype.correctFeedback = function() {
    var self = this;
    var feedback = localStorage.getItem('toy_2_2');
    feedback++;
    localStorage.setItem('toy_2_2', feedback);
    self.updateFeedback();
    if (feedback >= Toy_2_2.totalFeedback) {
        setTimeout(self.onComplete.bind(self), 600);
    } else {

        self.rhymeContainer.removeChildren();
        self.returnWallSprites();
        self.borrowWallSprites(1);
        self.runExercise();
    }
    addedListeners = false;
};

/**
 * on wrong feedback , feedback decrease
 */
Toy_2_2.prototype.wrongFeedback = function() {
    var self = this;
    var feedback = localStorage.getItem('toy_2_2');
    feedback--;

    if (feedback < 0)
        feedback = 0;

    localStorage.setItem('toy_2_2', feedback);
    self.updateFeedback();
    addedListeners = false;
};

/**
 * load outro scene
 */
Toy_2_2.prototype.onComplete = function() {

    var self = this;

    this.introMode = false;

    self.person.visible = false;
    self.feedbackContainer.visible = false;
    self.showHideChildrenInContainer();
    self.outroRobot.visible = true;
    this.defaultArms.visible = false;

    this.rhymeContainer.visible = false;

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        self.cancelOutro();
        self.restartExercise();
    }
    this.outroAnimation();
}

/**
 * reset exercie to default values
 */
Toy_2_2.prototype.resetExercise = function() {
    var self = this;
    this.ovaigen.visible = false;
    localStorage.setItem('toy_2_2', 0);

    self.rhymeContainer.visible = true;
    self.defaultArms.visible = true;
    self.feedbackContainer.visible = true;
    self.person.visible = true;
    self.outroRobot.visible = false;

    this.introMode = true;
};

/**
 * update feedback on correct or wrong
 */
Toy_2_2.prototype.updateFeedback = function() {

    var self = this;

    var counter = localStorage.getItem('toy_2_2');

    self.showHideChildrenInContainer();

    for (var j = 0; j < counter; j++) {
        self.feedbackContainer.children[j].alpha = 1;
    }
    addedListeners = false;
}


/**
 * pool objects and borrow 2 sets at begining
 */
Toy_2_2.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/toystore/toystore_2_2.json?noache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        self.pool = new ExerciseSpritesPool(loader.json);
        //borrow two sprites once
        self.generatePoolCategory();
        self.borrowWallSprites(2);
        self.runExercise();
    });

    loader.load();
}


Toy_2_2.prototype.generatePoolCategory = function() {

    var keyArr = [];

    var spriteArr = this.pool.sprites,
        spriteLen = spriteArr.length;

    for (var i = 0; i < spriteLen; i++) {
        keyArr.push(spriteArr[i][0].answer);
    };

    this.poolCategory = keyArr.filter(onlyUnique);

    this.poolIsNotArray = true;
    this.poolCategoryType = "answer";
}

/**
 * borrow 2 sets and preload sprites and audios
 * @param  {int} num
 */
Toy_2_2.prototype.borrowWallSprites = function(num) {

    var rand, category, sprite, spriteObj;

    var self = this;
    this.num = num;

    for (var i = 0; i < this.num; i++) {
        // var sprite = this.pool.borrowSprite();
        rand = makeUniqueRandom(this.poolCategory.length);
        this.borrowCategory = this.poolCategory[rand];

        sprite = this.pool.borrowUniqueSprite(this.borrowCategory, this.lastBorrowSprite, this.poolCategoryType, this.poolIsNotArray);

        this.poolSlices.push(sprite);
        var imageArr = [];
        var soundArr = [];
        var answerArr = [];
        var spriteObj = sprite[0];
        this.lastBorrowSprite = spriteObj.answer;
        for (var j = 0; j < spriteObj.image.length; j++) {

            var poolImageSprite = PIXI.Sprite.fromImage('uploads/images/' + spriteObj.image[j]);
            imageArr.push(poolImageSprite);
            main.texturesToDestroy.push(poolImageSprite);

            var howl = loadSound('uploads/audios/' + StripExtFromFile(spriteObj.sound[j]));
            soundArr.push(howl);
            answerArr.push(spriteObj.answer_options[j]);

        };

        var alphabetAudio = loadSound('uploads/audios/alphabets/small/' + StripExtFromFile(spriteObj.answer.toLowerCase()));

        var vowelAlphabet = "";
        if (spriteObj.answer.toLowerCase() == "a" || spriteObj.answer.toLowerCase() == "e" || spriteObj.answer.toLowerCase() == "i" || spriteObj.answer.toLowerCase() == "o") {
            vowelAlphabet = loadSound('uploads/audios/alphabets/capital/' + spriteObj.answer);
        } else {
            vowelAlphabet = null;
        }

        shuffle(imageArr, soundArr, answerArr);

        soundArr.push(alphabetAudio);

        this.wallSlices.push({
            answer_alphabet: spriteObj.answer,
            image: imageArr,
            sound: soundArr,
            answer: answerArr,
            vowel_alphabet: vowelAlphabet
        });
    }
};

/**
 * return used sets of objects to pool
 */
Toy_2_2.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];
    this.pool.returnSprite(sprite);

    this.rhymeContainer.removeChildren();

    this.poolSlices = [];
    this.wallSlices.shift();
};

/**
 * reset exercise and get new sets
 */
Toy_2_2.prototype.restartExercise = function() {
    var self = this;
    this.resetExercise();
    localStorage.setItem('toy_2_2', 0);
    self.showHideChildrenInContainer();
    this.rhymeContainer.removeChildren();
    this.returnWallSprites();
    this.borrowWallSprites(1);
    this.runExercise();
};

/**
 * show red or green highlight for correct and wrong sprites
 * @param  {int} xPos
 * @param  {int} yPos
 * @param  {object} obj
 */
Toy_2_2.prototype.showRedOrGreenBox = function(xPos, yPos, obj) {
    var self = this;

    obj.position = {
        x: xPos,
        y: yPos
    };

    obj.visible = true;

    setTimeout(function() {
        obj.visible = false;
    }, 400);


};

/**
 * set all feedback to 20% visibility at once
 */
Toy_2_2.prototype.showHideChildrenInContainer = function() {

    for (var j = 0; j < this.feedbackContainer.children.length; j++) {
        this.feedbackContainer.children[j].alpha = 0.2;
    }
}

/**
 * clean memory by detroying unused variables
 */
Toy_2_2.prototype.cleanMemory = function() {


    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    this.person = null;
    this.defaultArms = null;

    this.animateArms = null;

    this.help = null;
    this.ovaigen = null;

    if (this.poolSlices)
        this.poolSlices.length = 0;

    if (this.wallSlices)
        this.wallSlices.length = 0;

    this.pool = null;

    this.sceneContainer = null;
    this.feedbackContainer = null;
    this.rhymeContainer = null;
    this.animationContainer = null;

    this.marchRobots = null;
    this.outroRobot = null;
    this.whiteLayer = null;
    this.pinkLayer = null;

    this.baseContainer.removeChildren();
    this.baseContainer = null;
}