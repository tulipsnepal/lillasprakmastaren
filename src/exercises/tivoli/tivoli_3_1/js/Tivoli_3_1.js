function Tivoli_3_1() {
  cl.show();

  PIXI.DisplayObjectContainer.call(this);
  main.CurrentExercise = "longshort1";
  this.baseContainer = new PIXI.DisplayObjectContainer();

  this.sceneContainer = new PIXI.DisplayObjectContainer();
  this.introContainer = new PIXI.DisplayObjectContainer();
  this.outroContainer = new PIXI.DisplayObjectContainer();

  this.dragParentContainer = new PIXI.DisplayObjectContainer();

  this.rhymeContainer = new PIXI.DisplayObjectContainer();
  this.rhymeContainerBase = new PIXI.DisplayObjectContainer();

  this.sceneContainer.position = {
    x: 77,
    y: 88
  };

  localStorage.setItem('tivoli_3_1', 0);
  localStorage.setItem('tivoli_3_1_long', 0);
  localStorage.setItem('tivoli_3_1_short', 0);

  main.texturesToDestroy = [];

  this.baseContainer.addChild(this.sceneContainer);

  this.loadSpriteSheet();

  // everything connected to stage is rendered.
  this.addChild(this.baseContainer);
}

Tivoli_3_1.constructor = Tivoli_3_1;
Tivoli_3_1.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Tivoli_3_1.imagePath = 'build/exercises/tivoli/tivoli_3_1/images/';
Tivoli_3_1.audioPath = 'build/exercises/tivoli/tivoli_3_1/audios/';
Tivoli_3_1.commonFolder = 'build/common/images/tivoli/';
Tivoli_3_1.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

/**
 * load sprites, audios, imaegs befor game starts
 */
Tivoli_3_1.prototype.loadSpriteSheet = function() {

  this.introId = loadSound(Tivoli_3_1.audioPath + 'intro');
  this.outroId = loadSound(Tivoli_3_1.audioPath + 'outro');

  // create an array of assets to load
  var assetsToLoad = [
    Tivoli_3_1.imagePath + "sprite_3_1.json",
    Tivoli_3_1.imagePath + "sprite_3_1.png",
    Tivoli_3_1.imagePath + "kortaord.png",
  ];
  // create a new loader
  loader = new PIXI.AssetLoader(assetsToLoad);
  // use callback
  loader.onComplete = this.spriteSheetLoaded.bind(this);
  //begin load
  loader.load();

}

/**
 * on assets loaded , call needed function
 */
Tivoli_3_1.prototype.spriteSheetLoaded = function() {
  cl.hide();

  if (this.sceneContainer.stage)
    this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);


  this.initObjectPooling();

  this.loadDefaults();

  this.introContainer.addChild(this.dragParentContainer);
  this.introContainer.addChild(this.rhymeContainerBase);
  this.introContainer.addChild(this.rhymeContainer);

}

/**
 * draw background, create sprites and movie clip animation
 */
Tivoli_3_1.prototype.loadDefaults = function() {

  var self = this;

  this.sceneBackground = new PIXI.Graphics();
  this.sceneBackground.beginFill(0xC9D0E8);
  // set the line style to have a width of 5 and set the color to red
  this.sceneBackground.lineStyle(3, 0xF4CDE2);
  // draw a rectangle
  this.sceneBackground.drawRect(0, 0, 1843, 1202);
  this.sceneContainer.addChild(this.sceneBackground);

  this.sceneContainer.addChild(this.introContainer);
  this.sceneContainer.addChild(this.outroContainer);

  this.carousel_intro = PIXI.Sprite.fromFrame("carousel.png");
  this.carousel_intro.position = {
    x: 543,
    y: 2
  }
  main.texturesToDestroy.push(this.carousel_intro);
  this.introContainer.addChild(this.carousel_intro);

  this.longBox = PIXI.Sprite.fromFrame("langaord.png");
  this.shortBox = PIXI.Sprite.fromImage(Tivoli_3_1.imagePath + "kortaord.png");
  main.texturesToDestroy.push(this.longBox);
  main.texturesToDestroy.push(this.shortBox);

  this.longBox.position = {
    x: 531,
    y: 337
  }
  this.shortBox.position = {
    x: 1034,
    y: 337
  }
  this.introContainer.addChild(this.longBox);
  this.introContainer.addChild(this.shortBox);

  this.person = PIXI.Sprite.fromFrame("carousel-girl-long.png");
  this.defaultArms = PIXI.Sprite.fromFrame("hands_resting.png");
  main.texturesToDestroy.push(this.person);
  main.texturesToDestroy.push(this.defaultArms);

  this.person.position = {
    x: 1604,
    y: 482
  }

  this.defaultArms.position = {
    x: 835,
    y: 418
  }
  this.introContainer.addChild(this.person);
  this.introContainer.addChild(this.defaultArms);

  this.mouthParams = {
    "image": "mouth_",
    "length": 3,
    "x": 1689,
    "y": 651,
    "speed": 6,
    "pattern": true,
    "frameId": 1
  };
  this.animateMouth = new Animation(this.mouthParams);

  this.sceneContainer.addChild(this.animateMouth);

  this.hands = [
    new PIXI.Sprite.fromFrame('arm_up.png'),
    new PIXI.Sprite.fromFrame('arm_1.png'),
    new PIXI.Sprite.fromFrame('arm_long.png'),
    new PIXI.Sprite.fromFrame('arm_short.png')
  ];

  for (var i = 0; i < this.hands.length; i++) {
    this.hands[i].position.set(835, 418);
    this.introContainer.addChild(this.hands[i]);
    this.hands[i].visible = false;
  }

  this.animateMouth.gotoAndStop(3);

  this.help = PIXI.Sprite.fromFrame("topcorner.png");
  this.help.interactive = true;
  this.help.buttonMode = true;
  this.help.position = {
    x: -77,
    y: -88
  }
  this.sceneContainer.addChild(this.help);

  this.carousel_outro = PIXI.Sprite.fromImage(Tivoli_3_1.imagePath + "carousel_outro.png");
  main.texturesToDestroy.push(this.carousel_outro);


  this.animateOutroMouth = new Animation({
    "image": "mouth_outro_",
    "length": 4,
    "x": 1034,
    "y": 589,
    "speed": 6,
    "pattern": true,
    "frameId": 1
  });


  this.ovaigen = PIXI.Sprite.fromFrame("ovaigen.png");

  this.carousel_outro.position = {
    x: 3,
    y: 1
  }

  this.ovaigen.buttonMode = true;
  this.ovaigen.interactive = true;
  this.ovaigen.position = {
    x: 1255,
    y: 795
  };

  this.outroContainer.addChild(this.carousel_outro);
  this.outroContainer.addChild(this.ovaigen);
  this.outroContainer.addChild(this.animateOutroMouth);
  this.outroContainer.visible = false;

  this.ovaigen.visible = false;

  this.help.click = this.help.tap = this.introAnimation.bind(this);

  this.loadExerciseBase();

}

/**
 * hide all hands at once and show only specific hand
 * @param  {int} number
 */
Tivoli_3_1.prototype.showHandAt = function(number) {
  for (var i = 0; i < this.hands.length; i++) {
    this.hands[i].visible = false;
  }
  if (isNumeric(number))
    this.hands[number].visible = true;
};

/**
 * start intro animation
 */
Tivoli_3_1.prototype.introAnimation = function() {

  var self = this;

  createjs.Sound.stop();

  if (!this.introMode)
    this.restartExercise();

  main.overlay.visible = true;

  self.defaultArms.visible = true;

  this.outroContainer.visible = false;
  this.introContainer.visible = true;

  this.help.interactive = false;
  this.help.buttonMode = false;

  this.introInstance = createjs.Sound.play(this.introId, {
    interrupt: createjs.Sound.INTERRUPT_ANY
  });

  if (createjs.Sound.isReady()) {
    self.defaultArms.visible = false;
    self.animateMouth.show();
    this.timerId = setInterval(this.animateIntro.bind(this), 1);
  }

  this.introCancelHandler = this.cancelIntro.bind(this);
  this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
0-2 sec arm_up
2-5 sec arm_1
5-8 sec arm_long
8-10 sec arm_short
stop at hand_resting

new PIXI.Sprite.fromFrame('arm_up.png'),
    new PIXI.Sprite.fromFrame('arm_1.png'),
    new PIXI.Sprite.fromFrame('arm_long.png'),
    new PIXI.Sprite.fromFrame('arm_short.png')
 */
Tivoli_3_1.prototype.animateIntro = function() {

  var self = this;

  var currentPostion = this.introInstance.getPosition();
  if (currentPostion >= 0 && currentPostion < 2000) {
    self.showHandAt(0);
  } else if (currentPostion >= 2000 && currentPostion < 5000) {
    self.showHandAt(1);
  } else if (currentPostion >= 5000 && currentPostion < 8000) {
    self.showHandAt(2);
  } else if (currentPostion >= 8000 && currentPostion <= 10000) {
    self.showHandAt(3);
  } else if (currentPostion > 10000) {
    self.showHandAt();
    self.defaultArms.visible = true;
  }

}

/**
 * cancel running intro animation
 */
Tivoli_3_1.prototype.cancelIntro = function() {

  var self = this;

  createjs.Sound.stop();

  clearInterval(self.timerId);
  self.animateMouth.hide(3);
  self.showHandAt();
  self.defaultArms.visible = true;

  self.help.interactive = true;
  self.help.buttonMode = true;

  self.ovaigen.visible = false;
  main.overlay.visible = false;

};

/**
 * play outro animation
 */
Tivoli_3_1.prototype.outroAnimation = function() {

  var self = this;

  var outroInstance = createjs.Sound.play(this.outroId);
  if (createjs.Sound.isReady()) {
    main.overlay.visible = true;
    self.animateOutroMouth.show();

  }
  this.outroCancelHandler = this.cancelOutro.bind(this);
  outroInstance.addEventListener("complete", this.outroCancelHandler);

};

/**
 * cancel running outro animation
 */
Tivoli_3_1.prototype.cancelOutro = function() {

  var self = this;

  createjs.Sound.stop();
  self.animateOutroMouth.hide(0);
  self.help.interactive = true;
  self.help.buttonMode = true;
  self.ovaigen.visible = true;

  main.overlay.visible = false;

};

/**
 * load exercise base by creating sprites
 */
Tivoli_3_1.prototype.loadExerciseBase = function() {

  this.spritePosition = [];

  for (var i = 0; i < 5; i++) {

    var xDiff = i * 261;
    var yDiff = 253;

    var box1 = createRect({
        "alpha": 0.2
      }),
      box2 = createRect({
        "alpha": 0.2
      });

    box1.position.set(130 + xDiff, 641);
    box2.position.set(130 + xDiff, 641 + yDiff);

    this.rhymeContainerBase.addChild(box1);
    this.rhymeContainerBase.addChild(box2);

    this.spritePosition.push(130 + xDiff, 641, 130 + xDiff, 641 + yDiff);

  };
  // console.log(this.spritePosition);
};

/**
 * exercise logic
 */
Tivoli_3_1.prototype.runExercise = function() {

  var self = this;

  var jsonData = this.wallSlices[0];
  console.log(jsonData.answer);

  var soundTexture = PIXI.Texture.fromFrame("sound_grey.png");

  var dragButtons = [];

  for (var i = 0; i < 10; i++) {
    var dragContainer = new PIXI.DisplayObjectContainer(),
      imageSprite = jsonData.image[i],
      soundSprite = new PIXI.Sprite(soundTexture),
      spriteBase = createRect(),
      wordText = new PIXI.Text(jsonData.word[i], {
        font: "34px Arial"
      });
    main.texturesToDestroy.push(wordText);
    this.dragParentContainer.addChild(dragContainer);

    imageSprite.width = imageSprite.height = 241;
    imageSprite.buttonIndex = i;
    imageSprite.isAnswerLong = jsonData.answer[i];

    spriteBase.position = imageSprite.position = {
      x: self.spritePosition[i * 2],
      y: self.spritePosition[i * 2 + 1]
    }

    soundSprite.position = {
      x: self.spritePosition[i * 2],
      y: self.spritePosition[i * 2 + 1] + 241 - 62
    }

    dragContainer.addChild(spriteBase);
    dragContainer.addChild(imageSprite);
    dragContainer.addChild(soundSprite);

    dragContainer.dragIndex = i;
    dragContainer.interactive = true;
    dragContainer.buttonMode = true;

    dragContainer.howl = jsonData.sound[i];
    dragContainer.howlPositionX = soundSprite.position.x;
    dragContainer.howlPositionY = soundSprite.position.y;

    dragContainer.spriteBase = spriteBase;
    dragContainer.imageSprite = imageSprite;
    dragContainer.imageSpritePosition = imageSprite.position;
    dragContainer.soundSprite = soundSprite;
    dragContainer.word = wordText;

    dragButtons.push(dragContainer);
  };

  // use the mousedown and touchstart
  for (var i = 0; i < dragButtons.length; i++) {

    dragButtons[i].mousedown = dragButtons[i].touchstart = function(data) {
      if (addedListeners) return false;
      data.originalEvent.preventDefault()

      // store a refference to the data
      // The reason for this is because of multitouch
      // we want to track the movement of this particular touch
      this.data = data;
      this.alpha = 0.9;
      this.dragging = true;
      this.sx = this.data.getLocalPosition(this).x;
      this.sy = this.data.getLocalPosition(this).y;

      this.bringToFront();

      if (isInsideRectangle(this.sx, this.sy, this.howlPositionX, this.howlPositionY, this.howlPositionX + 83, this.howlPositionY + 81)) {
        createjs.Sound.stop();
        createjs.Sound.play(this.howl);

        this.dragging = false;
      }

    }

    dragButtons[i].mouseup = dragButtons[i].mouseupoutside = dragButtons[i].touchend = dragButtons[i].touchendoutside = function(data) {

      if (addedListeners) return false;
      var dragObj = this;

      this.alpha = 1
      this.dragging = false;
      // set the interaction data to null
      this.data = null;

      this.position.x = 0;
      this.position.y = 0;

      var isDropped = false; // to check dropped in target rectangular area
      var isDroppedLong = false; // to check dropped area is hat
      var isDroppedShort = false; // to check dropped area is sax

      // check source image points exists in target rectangle area
      if (isInsideRectangle(this.dx, this.dy, self.longBox.position.x, self.longBox.position.y, self.longBox.position.x + 241, self.longBox.position.y + 241)) {

        isDropped = true;
        isDroppedLong = true;

        dragObj.visible = false;

      }

      // check source image points exists in target rectangle area
      if (isInsideRectangle(this.dx, this.dy, self.shortBox.position.x, self.shortBox.position.y, self.shortBox.position.x + 241, self.shortBox.position.y + 241)) {

        isDropped = true;
        isDroppedShort = true;

        dragObj.visible = false;

      }

      // move the sprite to its designated hat or sax position
      if (isDropped) {

        if (isDroppedLong) {

          if (this.imageSprite.isAnswerLong)
            correctFeedback('long');
          else
            wrongFeedback('long');

        }

        if (isDroppedShort) {

          if (!this.imageSprite.isAnswerLong)
            correctFeedback('short');
          else
            wrongFeedback('short');

        }
        this.position.x = 0;
        this.position.y = 0;
        this.sx = 0;
        this.sy = 0;
        this.dx = 0;
        this.dy = 0;

      } else {
        this.position.x = 0;
        this.position.y = 0;
      }

      function correctFeedback(lenType) {
        self.rhymeContainer.removeChildren();

        var targetPosition = (lenType === 'long') ? self.longBox.position : self.shortBox.position;

        var targetImageSprite = dragObj.imageSprite,
          targetSoundSprite = dragObj.soundSprite;

        greenBox.position.x = targetPosition.x;
        greenBox.position.y = targetPosition.y;

        targetImageSprite.position = targetPosition;

        targetSoundSprite.position.x = targetPosition.x;
        targetSoundSprite.position.y = targetPosition.y + 241 - 62;

        self.rhymeContainer.addChild(targetImageSprite);
        self.rhymeContainer.addChild(targetSoundSprite);
        self.rhymeContainer.addChild(greenBox);

        correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);

        setTimeout(function() {
          placeCorrectFeedback(lenType);
        }, 800);

      }

      function placeCorrectFeedback(lenType) {

        self.rhymeContainer.removeChildren();
        self.dragParentContainer.removeChild(dragObj);

        var feedback = localStorage.getItem('tivoli_3_1'),
          feedbackLong = localStorage.getItem('tivoli_3_1_long'),
          feedbackShort = localStorage.getItem('tivoli_3_1_short');

        feedback++;
        localStorage.setItem('tivoli_3_1', feedback);

        if (lenType === 'long') {
          var feedbackX = 189;
          var feedbackY = 35 + (feedbackLong * 75);
          var lineX = 300;
        } else {
          var feedbackX = 1515;
          var feedbackY = 35 + (feedbackShort * 75);
          var lineX = 100;
        }

        var whiteSprite = createRect({
          w: 66,
          h: 66
        });
        whiteSprite.position.set(feedbackX - 80, feedbackY - 10);

        var graphics = new PIXI.Graphics();
        // set a fill and line style
        graphics.lineStyle(1, 0x000000, 1);
        graphics.moveTo(feedbackX, feedbackY + 43);
        graphics.lineTo(feedbackX + lineX, feedbackY + 43);

        dragObj.imageSprite.position = {
          x: feedbackX - 80,
          y: feedbackY - 10,
        }

        dragObj.word.position = {
          x: feedbackX + 20,
          y: feedbackY,
        }

        dragObj.imageSprite.width = 66;
        dragObj.imageSprite.height = 66;

        self.dragParentContainer.addChild(whiteSprite);
        self.dragParentContainer.addChild(dragObj.imageSprite);
        self.dragParentContainer.addChild(dragObj.word);
        self.dragParentContainer.addChild(graphics);

        (lenType === 'long') ? feedbackLong++ : feedbackShort++;

        localStorage.setItem('tivoli_3_1_long', feedbackLong);
        localStorage.setItem('tivoli_3_1_short', feedbackShort);

        //go to outro page
        if (feedback >= Tivoli_3_1.totalFeedback) {
          setTimeout(function() {
            self.onComplete();
          }, 1600);
        }

      }

      function wrongFeedback(lenType) {

        self.rhymeContainer.removeChildren();

        var targetPosition = (lenType === 'long') ? self.longBox.position : self.shortBox.position;

        var targetImageSprite = dragObj.imageSprite,
          targetSoundSprite = dragObj.soundSprite;

        // redBox.position.set(targetPosition.x, targetPosition.y);
        redBox.position.x = targetPosition.x;
        redBox.position.y = targetPosition.y;

        targetImageSprite.position = targetPosition;

        targetSoundSprite.position.x = targetPosition.x;
        targetSoundSprite.position.y = targetPosition.y + 241 - 62;

        self.rhymeContainer.addChild(targetImageSprite);
        self.rhymeContainer.addChild(targetSoundSprite);
        self.rhymeContainer.addChild(redBox);

        wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);

        setTimeout(function() {

          self.rhymeContainer.removeChildren();

          dragObj.visible = true;
          dragObj.imageSprite.position = dragObj.imageSpritePosition;
          dragObj.soundSprite.position = {
            x: dragObj.imageSprite.position.x,
            y: dragObj.imageSprite.position.y + 241 - 62
          }
          dragObj.addChild(dragObj.imageSprite);
          dragObj.addChild(dragObj.soundSprite);
        }, 300);
      }

    }

    // set the callbacks for when the mouse or a touch moves
    dragButtons[i].mousemove = dragButtons[i].touchmove = function(data) {
      if (addedListeners) return false;
      if (this.dragging) {
        // need to get parent coords..
        var newPosition = this.data.getLocalPosition(this.parent);
        this.position.x = newPosition.x - this.sx;
        this.position.y = newPosition.y - this.sy;
        this.dx = newPosition.x;
        this.dy = newPosition.y;
      }
    }

  }; //for loop ends
};

/**
 * on complete , load outro scene
 */
Tivoli_3_1.prototype.onComplete = function() {

  var self = this;

  this.introMode = false;

  this.introContainer.visible = false;
  this.outroContainer.visible = true;

  this.help.interactive = false;
  this.help.buttonMode = false;

  //this.animateMouth.position.set(1673, 586);
  // this.animateMouth.gotoAndStop(3);
  this.animateMouth.visible = false;

  this.ovaigen.click = this.ovaigen.tap = function(data) {
    self.cancelOutro();
    self.restartExercise();
  }

  this.outroAnimation();

}

/**
 * set to default values , and clear container
 */
Tivoli_3_1.prototype.resetExercise = function() {

  var self = this;

  this.dragParentContainer.removeChildren();

  localStorage.setItem('tivoli_3_1', 0);
  localStorage.setItem('tivoli_3_1_long', 0);
  localStorage.setItem('tivoli_3_1_short', 0);

  this.ovaigen.visible = false;
  self.outroContainer.visible = false;
  self.introContainer.visible = true;
  self.animateMouth.position.set(1689, 651);
  self.animateMouth.visible = true;

  this.introMode = true;

};

/**
 * load exercise json using jsonloader and grab 2 sets
 */
Tivoli_3_1.prototype.initObjectPooling = function() {

  var self = this;

  this.wallSlices = [];

  this.poolSlices = [];

  self.pool = {};

  var loader = new PIXI.JsonLoader('uploads/exercises/tivoli/tivoli_3_1.json?nocache=' + (new Date()).getTime());
  loader.on('loaded', function(evt) {
    //data is in loader.json
    self.pool = new ExerciseSpritesPool(loader.json);
    //borrow two sprites once
    self.num = 1;
    self.borrowWallSprites(self.num);
  });

  loader.load();
}

/**
 * cache sprites, audios
 */
Tivoli_3_1.prototype.borrowWallSprites = function() {

  this.sprite = this.pool.borrowSprite();

  this.poolSlices.push(this.sprite);

  if (this.spriteObj)
    this.spriteObj = null;

  this.spriteObj = this.sprite[0];

  main.common.startPreload(this.spriteObj);

};

Tivoli_3_1.prototype.createWallSlices = function() {

  for (var i = 0; i < this.num; i++) {

    var imageArr = [],
      answerArr = [],
      wordArr = [],
      soundArr = [];

    for (var j = 0; j < this.spriteObj.image.length; j++) {
      var poolImageSprite = PIXI.Sprite.fromImage('uploads/images/' + this.spriteObj.image[j]);
      imageArr.push(poolImageSprite);
      main.texturesToDestroy.push(poolImageSprite);

      // var howl = loadSound('uploads/audios/' + StripExtFromFile(this.spriteObj.sound[j]));
      var howl = 'uploads/audios/' + this.spriteObj.sound[j];
      soundArr.push(howl);

      answerArr.push(this.spriteObj.answer[j]);
      wordArr.push(this.spriteObj.word[j]);

    };

    shuffle(imageArr, soundArr, answerArr, wordArr);

    this.wallSlices.push({
      word: wordArr,
      image: imageArr,
      sound: soundArr,
      answer: answerArr,
    });
  }

  this.runExercise();

};

/**
 * send back used object to pool to re-use later
 */
Tivoli_3_1.prototype.returnWallSprites = function() {

  var sprite = this.poolSlices[0];

  this.pool.returnSprite(sprite);

  this.rhymeContainer.removeChildren();

  this.poolSlices = [];
  this.wallSlices.shift();
};

/**
 * reset exercise and get next set
 */
Tivoli_3_1.prototype.restartExercise = function() {

  this.resetExercise();

  this.returnWallSprites();
  this.num = 1;
  this.borrowWallSprites();

};

/**
 * unload/destroy used variables and objects and clear container
 */
Tivoli_3_1.prototype.cleanMemory = function() {

  if (this.sceneBackground)
    this.sceneBackground.clear();
  this.sceneBackground = null;

  this.carousel_intro = null;
  this.longBox = null;
  this.shortBox = null;
  this.person = null;
  this.defaultArms = null;

  if (this.mouthParams)
    this.mouthParams.length = 0;

  this.animateMouth = null;

  this.help = null;
  this.ovaigen = null;
  this.carousel_outro = null;

  if (this.spritePosition)
    this.spritePosition.length = 0;

  this.num = null;

  if (this.poolSlices)
    this.poolSlices.length = 0;

  if (this.wallSlices)
    this.wallSlices.length = 0;

  this.pool = null;

  this.sceneContainer = null;
  this.introContainer = null;
  this.outroContainer = null;
  this.dragParentContainer = null;
  this.rhymeContainer = null;
  this.rhymeContainerBase = null;

  this.baseContainer.removeChildren();
  this.baseContainer = null;

}