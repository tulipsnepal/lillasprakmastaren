function Tivoli_2_2(stage) {

    cl.show();
    main.CurrentExercise = "enochett2";
    PIXI.DisplayObjectContainer.call(this);

    this.baseContainer = new PIXI.DisplayObjectContainer();

    this.sceneContainer = new PIXI.DisplayObjectContainer();

    this.frontContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position = {
        x: 77,
        y: 88
    };

    localStorage.removeItem('tivoli_2_2');

    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();

    main.texturesToDestroy = [];

    this.loadSpriteSheet();

    // everything connected to stage is rendered.
    this.addChild(this.baseContainer);

}

Tivoli_2_2.constructor = Tivoli_2_2;
Tivoli_2_2.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Tivoli_2_2.imagePath = 'build/exercises/tivoli/tivoli_2_2/images/';
Tivoli_2_2.audioPath = 'build/exercises/tivoli/tivoli_2_2/audios/';
Tivoli_2_2.commonFolder = 'build/common/images/tivoli/';
Tivoli_2_2.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

/**
 * load assets like sprites, images, audios before exercise start
 */
Tivoli_2_2.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(Tivoli_2_2.audioPath + 'intro');
    this.outroId = loadSound(Tivoli_2_2.audioPath + 'outro');

    // create an array of assets to load
    var assetsToLoad = [
        Tivoli_2_2.imagePath + "defaultbg.png",
        Tivoli_2_2.imagePath + "sprite_2_2.json",
        Tivoli_2_2.imagePath + "sprite_2_2.png",
        Tivoli_2_2.imagePath + "defaultbgOutro.png",
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * on assets loaded, pool exercise sets as objects
 * and load current exercise sprites
 */
Tivoli_2_2.prototype.spriteSheetLoaded = function() {

    cl.hide();

    this.loadDefaults();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    this.initObjectPooling();
}

/**
 * update feedback on based on correct or wrong answer
 * increment / decrement of feeback
 */
Tivoli_2_2.prototype.updateFeedback = function() {

    var self = this;

    var counter = localStorage.getItem('tivoli_2_2');

    self.feedbackContainer.removeChildren();

    this.feedbackSprites = [];

    var ingredientsPosition = [
        1605 - 77, 922 - 88, //eggs
        1408 - 77, 770 - 88, //yellow
        1467 - 77, 805 - 88, //leaves
        1462 - 77, 923 - 88, //mushrom
        1109 - 77, 897 - 88, //butterfly
        1224 - 77, 798 - 88, //tin drops
        1108 - 77, 756 - 88, //tanglar
        1282 - 77, 748 - 88, //frog
        1228 - 77, 908 - 88, //slem
        1357 - 77, 842 - 88, //spider
    ];

    for (var i = counter - 1; i >= 0; i--) {
        var imageSprite = PIXI.Sprite.fromFrame('fb' + [i + 1] + '.png');

        imageSprite.position = {
            x: ingredientsPosition[i * 2],
            y: ingredientsPosition[i * 2 + 1]
        };
        main.texturesToDestroy.push(imageSprite);
        this.feedbackSprites.push(imageSprite);
        self.feedbackContainer.addChild(imageSprite);
    };
};

/**
 * exercise logic
 */
Tivoli_2_2.prototype.getExerciseSet = function() {

    var self = this;

    self.exerciseContainer.removeChildren();

    var jsonData = this.wallSlices[0];
    console.log(jsonData.answer);

    this.textHitArea = [];

    var textPosition = [
        492, 339 - 20,
        492, 440 - 15,
        492, 551 - 17,
    ];

    var textArr = ["en", "och", "ett"];

    // create some text
    for (var i = 0; i < 3; i++) {
        var text = new PIXI.Text(textArr[i], {
            font: "74px Arial"
        });
        text.position = {
            x: textPosition[i * 2] - 6,
            y: textPosition[i * 2 + 1]
        };

        text.hitIndex = i;
        text.interactive = true;
        text.buttonMode = true;
        main.texturesToDestroy.push(text);
        this.exerciseContainer.addChild(text);
        this.textHitArea.push(text);
    };

    this.sentence = new PIXI.Text(jsonData.sentence, {
        font: "70px Arial",
        align: "center",
    });

    this.sentence.position = {
        x: 177,
        y: 935
    }
    main.texturesToDestroy.push(this.sentence);
    this.exerciseContainer.addChild(this.sentence);

    var imagePosition = [
        176, 645,
        654, 645,
    ];

    var howls = {};

    var highlightPosition = [
        423, 306,
        423, 306 + 112,
        423, 306 + 112 + 112,
    ];

    // create some text
    for (var i = 0; i < 2; i++) {
        var imageSprite = jsonData.image[i],
            soundSprite = PIXI.Sprite.fromFrame("sound_grey.png");

        imageSprite.position = {
            x: imagePosition[i * 2],
            y: imagePosition[i * 2 + 1]
        };
        imageSprite.width = imageSprite.height = 241;

        soundSprite.position = {
            x: imagePosition[i * 2],
            y: imagePosition[i * 2 + 1] + 180
        }

        soundSprite.howl = jsonData.sound[i];

        soundSprite.interactive = true;
        soundSprite.buttonMode = true;

        soundSprite.click = soundSprite.tap = this.onSoundPressed.bind(soundSprite);

        this.exerciseContainer.addChild(imageSprite);
        this.exerciseContainer.addChild(soundSprite);
    }

    //check the word missing; perform exercise
    this.textHitArea[0].click = this.textHitArea[0].tap = this.textHitArea[1].click = this.textHitArea[1].tap = this.textHitArea[2].click = this.textHitArea[2].tap = function(data) {
        var textObj = this;
        if (addedListeners) return false;

        textObj.interactive = false;

        addedListeners = true;

        if (jsonData.answer === this.hitIndex) { //correct
            greenSmallBox.position = {
                x: highlightPosition[this.hitIndex * 2],
                y: highlightPosition[this.hitIndex * 2 + 1]
            }
            self.exerciseContainer.addChildAt(greenSmallBox, 0);

            var feedback = localStorage.getItem('tivoli_2_2');
            feedback--;

            if (feedback < 0) feedback = 0;

            localStorage.setItem('tivoli_2_2', feedback);

            self.updateFeedback();

            self.animateMouth.visible = false;
            self.hand.visible = false;
            self.showHandAt(2);
            self.face.visible = false;
            self.faceCorrect.visible = true;

            correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);

            self.sentence.setText(jsonData.sentence.replace(/_{2,}/g, ' ' + textObj.text + ' '));

            setTimeout(function() {
                textObj.interactive = true;
                self.exerciseContainer.removeChildAt(0);
                addedListeners = false;
                self.correctFeedback();
            }, 1200);

        } else { //wrong
            redSmallBox.position = {
                x: highlightPosition[this.hitIndex * 2],
                y: highlightPosition[this.hitIndex * 2 + 1]
            }
            self.exerciseContainer.addChildAt(redSmallBox, 0);

            wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
            setTimeout(function() {

                textObj.interactive = true;
                self.exerciseContainer.removeChildAt(0);
                addedListeners = false;
                self.wrongFeedback();

            }, 100);
        }
    }


};

Tivoli_2_2.prototype.onSoundPressed = function() {
    createjs.Sound.stop();
    this.soundInstance = createjs.Sound.play(this.howl, {
        interrupt: createjs.Sound.INTERRUPT_NONE
    });
    this.soundInstance.addEventListener("failed", handleFailed);
    this.soundInstance.addEventListener("complete", handleComplete);
};

/**
 * on wrong answer, increment feedback (negative feedback)
 */
Tivoli_2_2.prototype.wrongFeedback = function() {

    var self = this;

    var feedback = localStorage.getItem('tivoli_2_2');
    feedback++;

    if (feedback > Tivoli_2_2.totalFeedback) feedback = Tivoli_2_2.totalFeedback;

    localStorage.setItem('tivoli_2_2', feedback);

    self.updateFeedback();

}

/**
 * on correect feedback, decrement feedback by 1
 * and get next sets by returning used set
 */
Tivoli_2_2.prototype.correctFeedback = function() {

    var self = this;

    var feedback = localStorage.getItem('tivoli_2_2');
    if (feedback < 0) feedback = 0;

    self.showHandAt();
    self.hand.visible = true;
    self.faceCorrect.visible = false;
    self.face.visible = true;
    self.animateMouth.visible = true;

    self.textHitArea = null;

    if (feedback == 0) {
        self.onComplete();
    } else {
        self.returnWallSprites();
        self.borrowWallSprites(1);
        self.getExerciseSet();
    }

}

/**
 * on complete exercise, load outro scene and perfrom outro animatoin after 1 sec
 */
Tivoli_2_2.prototype.onComplete = function() {

    var self = this;

    this.introMode = false;

    this.exerciseContainer.visible = false;
    this.outroContainer.visible = true;

    this.animateMouth.gotoAndStop(3);

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        self.cancelOutro();
        this.visible = false;
        self.outroContainer.visible = false;
        self.exerciseContainer.visible = true;
        self.restartExercise();
    }

    this.outroAnimation();

}

/**
 * update feedback and init exercise
 */
Tivoli_2_2.prototype.runExercise = function() {

    localStorage.setItem('tivoli_2_2', Tivoli_2_2.totalFeedback);

    this.feedbackContainer = new PIXI.DisplayObjectContainer();
    this.frontContainer.addChild(this.feedbackContainer);

    this.updateFeedback();

    this.getExerciseSet();

};

/**
 * load all needed sprites and also handle hand and mouth movie clip animation
 */
Tivoli_2_2.prototype.loadDefaults = function() {

    var self = this;

    this.bkg1 = new PIXI.Sprite.fromImage(Tivoli_2_2.imagePath + "defaultbg.png");
    main.texturesToDestroy.push(this.bkg1);
    this.sceneContainer.addChild(this.bkg1);

    this.face = PIXI.Sprite.fromFrame('defaultface.png');
    main.texturesToDestroy.push(this.face);
    this.faceCorrect = PIXI.Sprite.fromFrame('facepot.png');
    main.texturesToDestroy.push(this.faceCorrect);
    this.hand = PIXI.Sprite.fromFrame('defaulthand.png');
    main.texturesToDestroy.push(this.hand);

    this.face.position = {
        x: 1419,
        y: 29
    };
    this.faceCorrect.position = {
        x: 1380,
        y: 0
    };
    this.hand.position = {
        x: 642,
        y: 166
    };

    //instruction icon
    this.help = PIXI.Sprite.fromFrame("topcorner.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position = {
        x: -77,
        y: -88
    }

    this.sceneContainer.addChild(this.outroContainer);
    this.outroContainer.visible = false;

    this.sceneContainer.addChild(this.help);

    this.mouthParams = {
        "image": "mouth-",
        "length": 3,
        "x": 1509,
        "y": 439,
        "speed": 6,
        "pattern": true,
        "frameId": 3
    };
    this.animateMouth = new Animation(this.mouthParams);

    this.frontContainer.addChild(this.hand);

    this.frontContainer.addChild(this.exerciseContainer);

    this.hands = [
        new PIXI.Sprite.fromFrame('hand-1.png'),
        new PIXI.Sprite.fromFrame('hand-2.png'),
        new PIXI.Sprite.fromFrame('hand-3.png'),
        new PIXI.Sprite.fromFrame('defaulthand.png')
    ];
    for (var i = 0; i < this.hands.length; i++) {
        if (i == 1) {
            this.hands[i].position.set(658, 166);
        } else {
            this.hands[i].position.set(642, 166);
        }
        this.frontContainer.addChild(this.hands[i]);
        this.hands[i].visible = false;
    }

    this.frontContainer.addChild(this.face);

    this.frontContainer.addChild(this.animateMouth);

    this.frontContainer.addChild(this.faceCorrect);
    this.faceCorrect.visible = false;

    this.outroBkg = new PIXI.Sprite.fromImage(Tivoli_2_2.imagePath + "defaultbgOutro.png");
    main.texturesToDestroy.push(this.outroBkg);
    this.outroContainer.addChild(this.outroBkg);

    this.ovaigen = PIXI.Sprite.fromFrame("ovaigen.png");
    this.ovaigen.buttonMode = true;
    this.ovaigen.interactive = true;
    this.ovaigen.position = {
        x: 486,
        y: 801
    };
    this.outroContainer.addChild(this.ovaigen);
    this.ovaigen.visible = false;

    // bubbles and outro eyes
    this.bubbles = new Animation({
        "image": "bubble_",
        "length": 3,
        "x": 970,
        "y": 64,
        "speed": 4,
        "pattern": false
    });

    this.sceneContainer.addChild(this.bubbles);
    this.bubbles.show();


    this.witchEyes = new Animation({
        "image": "eyes_",
        "length": 2,
        "x": 1486,
        "y": 295,
        "speed": 1,
        "pattern": false
    });

    this.outroContainer.addChild(this.witchEyes);


    //instruction icon click
    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.frontContainer);
    this.baseContainer.addChild(this.sceneContainer);

}

/**
 * show hand as specified by arguments
 */
Tivoli_2_2.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    }
    if (isNumeric(number))
        this.hands[number].visible = true;
};

/**
 * start intro animation
 */
Tivoli_2_2.prototype.introAnimation = function() {

    var self = this;

    createjs.Sound.stop();

    if (!this.introMode)
        this.restartExercise();

    this.exerciseContainer.visible = true;
    this.outroContainer.visible = false

    this.help.interactive = false;
    this.help.buttonMode = false;

    main.overlay.visible = true;

    self.hand.visible = false;
    self.face.visible = false;

    self.face.visible = true;
    self.animateMouth.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
 0-3 sec arm_resting => 3
3-5 sec arm_intro1 =>0
5-7 sec arm_resting =>3
7-10 sec arm_intro2 => 1
10-13 sec arm_puttin_in_pot => 2
stop at arm_resting =>3

 */
Tivoli_2_2.prototype.animateIntro = function() {

    var self = this;

    var currentPostion = this.introInstance.getPosition();

    if (currentPostion >= 1000 && currentPostion < 3000) {
        self.showHandAt(3);
    } else if (currentPostion >= 3000 && currentPostion < 5000) {
        self.showHandAt(0);
    } else if (currentPostion >= 5000 && currentPostion < 7000) {
        self.showHandAt(3);
    } else if (currentPostion >= 7000 && currentPostion < 10000) {
        self.showHandAt(1);
    } else if (currentPostion >= 10000 && currentPostion < 13000) {
        self.showHandAt(2);
    } else {
        self.showHandAt(3);
    }

};

/**
 * cancel running intro animation
 */
Tivoli_2_2.prototype.cancelIntro = function() {

    var self = this;

    createjs.Sound.stop();

    clearInterval(self.timerId);

    self.face.visible = false;
    self.animateMouth.hide(3);

    self.hand.visible = true;
    self.showHandAt();

    self.face.visible = true;

    self.help.interactive = true;
    self.help.buttonMode = true;

    self.ovaigen.visible = false;

    main.overlay.visible = false;

};

/**
 * play outro animation
 */
Tivoli_2_2.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.help.interactive = false;
        self.help.buttonMode = false;
        self.bubbles.show();
        self.witchEyes.show();
        self.face.visible = false;
        self.hand.visible = false;

        self.animateMouth.show();
    }
    this.outroCancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.outroCancelHandler);
};

/**
 * cancel running outro animation.
 */
Tivoli_2_2.prototype.cancelOutro = function() {

    var self = this;

    createjs.Sound.stop();
    self.animateMouth.hide(3);

    self.help.interactive = true;
    self.help.buttonMode = true;

    self.ovaigen.visible = true;

    main.overlay.visible = false;

};

/**
 * reset feedback and get new sets
 * @return {[type]} [description]
 */
Tivoli_2_2.prototype.restartExercise = function() {
    var self = this;

    localStorage.setItem('tivoli_2_2', Tivoli_2_2.totalFeedback);
    self.face.visible = true;
    self.hand.visible = true;
    this.updateFeedback();

    this.returnWallSprites();
    this.borrowWallSprites(1);

    this.getExerciseSet();

    this.introMode = true;
}

/**
 * pool next 2 sets by loading exercise json using jsonloader
 */
Tivoli_2_2.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/tivoli/tivoli_2_2.json?nocache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        self.pool = new ExerciseSpritesPool(loader.json);
        //borrow two sprites once
        self.borrowWallSprites(2);
        self.runExercise();

    });
    loader.load();
}

/**
 * load/cache sprites, audios
 */
Tivoli_2_2.prototype.borrowWallSprites = function(num) {

    var self = this;

    this.num = num;

    for (var i = 0; i < num; i++) {
        var sprite = this.pool.borrowSprite();
        this.poolSlices.push(sprite);
        var imageArr = [];
        var soundArr = [];
        var spriteObj = sprite[0];
        for (var j = 0; j < spriteObj.image.length; j++) {
            var poolImageSprite = PIXI.Sprite.fromImage('uploads/images/' + spriteObj.image[j]);
            main.texturesToDestroy.push(poolImageSprite);
            imageArr.push(poolImageSprite);

            var howl = loadSound('uploads/audios/' + StripExtFromFile(spriteObj.sound[j]));

            soundArr.push(howl);
        };

        this.wallSlices.push({
            image: imageArr,
            sound: soundArr,
            answer: spriteObj.answer,
            sentence: spriteObj.sentence
        });
    }
};

/**
 * return used pool object
 */
Tivoli_2_2.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];

    this.pool.returnSprite(sprite);

    this.poolSlices = [];
    this.wallSlices.shift();
};

/**
 * unload/destroy used variables to manage memeory during exercise switch
 */
Tivoli_2_2.prototype.cleanMemory = function() {

    this.bkg1 = null;

    this.face = null;

    this.faceCorrect = null;

    this.hand = null;

    this.help = null;

    if (this.mouthParams)
        this.mouthParams.length = 0;

    this.animateMouth = null;
    this.animateHand = null;

    this.introSound = null;

    this.outroBkg = null;
    this.bubbles = null;
    this.witchEyes = null;

    this.ovaigen = null;

    this.outroSound = null;

    if (this.feedbackSprites)
        this.feedbackSprites.length = 0;

    if (this.textHitArea)
        this.textHitArea.length = 0;

    this.num = null;
    this.pool = null;

    if (this.poolSlices)
        this.poolSlices.length = 0;

    if (this.wallSlices)
        this.wallSlices.length = 0;

    this.sceneContainer = null;
    this.frontContainer = null;
    this.exerciseContainer = null;
    this.outroContainer = null;

    this.baseContainer.removeChildren();
    this.baseContainer = null;

}