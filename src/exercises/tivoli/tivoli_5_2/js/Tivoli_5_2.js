function Tivoli_5_2() {
  cl.show();

  PIXI.DisplayObjectContainer.call(this);
  main.CurrentExercise = "dendet2";
  this.baseContainer = new PIXI.DisplayObjectContainer();

  this.sceneContainer = new PIXI.DisplayObjectContainer();

  this.sceneContainer.position = {
    x: 77,
    y: 88
  };

  localStorage.setItem('tivoli_5_2', 0);

  this.baseContainer.addChild(this.sceneContainer);

  main.texturesToDestroy = [];

  this.loadSpriteSheet();

  // everything connected to stage is rendered.
  this.addChild(this.baseContainer);
}

Tivoli_5_2.constructor = Tivoli_5_2;
Tivoli_5_2.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Tivoli_5_2.imagePath = 'build/exercises/tivoli/tivoli_5_2/images/';
Tivoli_5_2.audioPath = 'build/exercises/tivoli/tivoli_5_2/audios/';
Tivoli_5_2.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

/**
 * preload sprites , audios , images
 */
Tivoli_5_2.prototype.loadSpriteSheet = function() {

  this.introId = loadSound(Tivoli_5_2.audioPath + 'intro');
  this.outroId = loadSound(Tivoli_5_2.audioPath + 'outro');

  // create an array of assets to load
  var assetsToLoad = [
    Tivoli_5_2.imagePath + "sprite_5_2.json",
    Tivoli_5_2.imagePath + "sprite_5_2.png",
    Tivoli_5_2.imagePath + "mainbg.png"
  ];
  // create a new loader
  loader = new PIXI.AssetLoader(assetsToLoad);
  // use callback
  loader.onComplete = this.spriteSheetLoaded.bind(this);
  //begin load
  loader.load();

}

/**
 * init game on assets loaded.
 */
Tivoli_5_2.prototype.spriteSheetLoaded = function() {

  cl.hide();

  if (this.sceneContainer.stage)
    this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

  this.loadDefaults();

  this.loadExerciseBase();

  this.initObjectPooling();

}

/**
 * draw background, creates sprites and movie clip animation
 */
Tivoli_5_2.prototype.loadDefaults = function() {

  var self = this;

  this.sceneBackground = new PIXI.Graphics();
  this.sceneBackground.beginFill(0xC9D0E8);
  // set the line style to have a width of 5 and set the color to red
  this.sceneBackground.lineStyle(3, 0xF4CDE2);
  // draw a rectangle
  this.sceneBackground.drawRect(0, 0, 1843, 1202);
  this.sceneContainer.addChild(this.sceneBackground);

  this.brownLayer = createRect({
    x: 3,
    y: 1071,
    w: 1840,
    h: 22,
    color: 0x936037
  });
  this.sceneContainer.addChild(this.brownLayer);

  this.blackLayer = createRect({
    x: 3,
    y: 1092,
    w: 1840,
    h: 108,
    color: 0x333334
  });
  this.sceneContainer.addChild(this.blackLayer);

  this.sky_1 = PIXI.Sprite.fromFrame('sky_1.png');
  this.sky_2 = PIXI.Sprite.fromFrame('sky_2.png');
  this.sky_3 = PIXI.Sprite.fromFrame('sky_3.png');

  main.texturesToDestroy.push(this.sky_1);
  main.texturesToDestroy.push(this.sky_2);
  main.texturesToDestroy.push(this.sky_3);

  this.sky_2.position.set(226, 0);
  this.sky_1.position.set(1, 48);
  this.sky_3.position.set(1624, 74);

  this.sceneContainer.addChild(this.sky_1);
  this.sceneContainer.addChild(this.sky_2);
  this.sceneContainer.addChild(this.sky_3);

  this.help = PIXI.Sprite.fromFrame("topcorner.png");
  this.help.interactive = true;
  this.help.buttonMode = true;
  this.help.position.set(-77, -88);
  this.sceneContainer.addChild(this.help);

  this.introContainer = new PIXI.DisplayObjectContainer();
  this.sceneContainer.addChild(this.introContainer);

  this.rhymeContainerBase = new PIXI.DisplayObjectContainer();
  this.introContainer.addChild(this.rhymeContainerBase);

  this.rhymeContainer = new PIXI.DisplayObjectContainer();
  this.introContainer.addChild(this.rhymeContainer);

  this.hands = [
    new PIXI.Sprite.fromFrame('arm_1.png'),
    new PIXI.Sprite.fromFrame('arm_2.png')
  ];
  for (var i = 0; i < this.hands.length; i++) {
    this.hands[i].position.set(297, 603);
    this.introContainer.addChild(this.hands[i]);
    this.hands[i].visible = false;
  }

  this.girlhelp = PIXI.Sprite.fromFrame('girl_explaining_exercise.png');
  this.girlhelp.position.set(104, 665);
  main.texturesToDestroy.push(this.girlhelp);
  this.introContainer.addChild(this.girlhelp);
  this.girlhelp.visible = false;

  this.mouthParams = {
    "image": "mouth_",
    "length": 3,
    "x": 290,
    "y": 778,
    "speed": 6,
    "pattern": true,
    "frameId": 0
  };
  this.mouths = new Animation(this.mouthParams);
  this.introContainer.addChild(this.mouths);
  this.mouths.visible = false;

  this.ovaigen = PIXI.Sprite.fromFrame('ovaigen.png');
  this.ovaigen.position.set(250, 465);
  this.introContainer.addChild(this.ovaigen);
  this.ovaigen.interactive = true;
  this.ovaigen.buttonMode = true;
  this.ovaigen.visible = false;

  this.help.click = this.help.tap = this.introAnimation.bind(this);
}

/**
 * start intro animation
 */
Tivoli_5_2.prototype.introAnimation = function() {

  var self = this;

  createjs.Sound.stop();

  if (!this.introMode)
    this.resetExercise();

  this.help.interactive = false;
  this.help.buttonMode = false;

  main.overlay.visible = true;

  self.girlhelp.visible = true;
  self.mouths.show();

  this.feedbackContainer.visible = false;

  this.introInstance = createjs.Sound.play(this.introId, {
    interrupt: createjs.Sound.INTERRUPT_ANY
  });

  if (createjs.Sound.isReady())
    this.timerId = setInterval(this.animateIntro.bind(this), 1);

  this.introCancelHandler = this.cancelIntro.bind(this);
  this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.

 0-1 sec no arm
1-3 sec arm_0
3-5 sec arm_pointing_up
5-6 sec arm_pointing_to _side

 */
Tivoli_5_2.prototype.animateIntro = function() {

  var self = this;

  var currentPostion = this.introInstance.getPosition();
  if (currentPostion >= 4000 && currentPostion < 5000) {
    self.showHandAt(1);
  } else if (currentPostion >= 5000 && currentPostion < 6000) {
    self.showHandAt(0);
  } else if (currentPostion >= 6000 && currentPostion < 7000) {
    self.showHandAt(1);
  } else if (currentPostion >= 7000) {
    self.showHandAt();
  }
};

/**
 * cancel running intro animation
 */
Tivoli_5_2.prototype.cancelIntro = function() {

  var self = this;

  createjs.Sound.stop();

  clearInterval(self.timerId);
  self.girlhelp.visible = false;
  self.mouths.hide();
  self.mouths.visible = false;
  self.showHandAt();

  self.help.interactive = true;
  self.help.buttonMode = true;

  this.feedbackContainer.visible = true;

  this.ovaigen.visible = false;
  main.overlay.visible = false;

};

/**
 * play outro animation
 */
Tivoli_5_2.prototype.outroAnimation = function() {

  var self = this;

  var outroInstance = createjs.Sound.play(this.outroId);
  if (createjs.Sound.isReady()) {
    main.overlay.visible = true;
    self.help.interactive = false;
    self.help.buttonMode = false;
  }
  this.cancelHandler = this.cancelOutro.bind(this);
  outroInstance.addEventListener("complete", this.cancelHandler);

};

/**
 * cancel runnning outro
 */
Tivoli_5_2.prototype.cancelOutro = function() {

  var self = this;

  if (self.requestId)
    cancelAnimationFrame(self.requestId);

  if (self.requestId2)
    cancelAnimationFrame(self.requestId2);

  createjs.Sound.stop();
  self.help.interactive = true;
  self.help.buttonMode = true;
  self.ovaigen.visible = true;
  main.overlay.visible = false;

  self.resetWaggon();

};

/**
 * create sprites, texutres for game
 */
Tivoli_5_2.prototype.loadExerciseBase = function() {

  this.feedbackContainer = new PIXI.DisplayObjectContainer();
  this.introContainer.addChild(this.feedbackContainer);

  this.waggonContainer = new PIXI.DisplayObjectContainer();
  this.introContainer.addChild(this.waggonContainer);

  /*NOW WE NEED A PORTION FOR A WHITE FRAME IN THE GAME, SO THAT WHEN GAME IS FINISHED, THE SCROLLING CARRIAGES SEEMS TO BE GOING BEHIND THE WHITE FRAME.
    IF WE DONT DO SO, THE CARRIAGES SCROLL THROUGH THE WHITE FRAME, WHICH LOOKS UNREALISTIC. TO UNDERSTAND WHAT I MEAN THE FOLLOWING CODE CAN BE COMMENTED OUT AND
    SEE THE CARRIAGES SCROLLING ANIMATION AT OUTRO PAGE.
   */
  this.whiteLayer = createRect({
    x: 1842,
    y: 880,
    w: 224,
    h: 220,
    color: 0xFFFFFF
  });
  this.introContainer.addChild(this.whiteLayer);
  this.pinkLayer = createRect({
    x: 1842,
    y: 880,
    w: 3,
    h: 220,
    color: 0xF4CDE2
  }); //
  this.introContainer.addChild(this.pinkLayer);

  this.whiteBoxPosition = [
    182, 165,
    182, 165 + 266,
    182 + 814, 165,
    182 + 814, 165 + 266
  ];

  this.quesTextPosition = [
    445, 240,
    445, 240 + 300,
    445 + 841, 240,
    445 + 841, 240 + 300
  ];

  this.ansTextPosition = [];

  for (var i = 0; i < 4; i++) {

    this.bigBox = createRect({
      x: this.whiteBoxPosition[i * 2],
      y: this.whiteBoxPosition[i * 2 + 1],
      w: 241,
      h: 241,
      color: 0x000000
    });
    this.rhymeContainerBase.addChild(this.bigBox);

    this.smallBox = createRect({
      x: 555 + (i * 192),
      y: 757,
      w: 171,
      h: 109,
      color: 0x000000
    });
    this.rhymeContainerBase.addChild(this.smallBox);
    this.ansTextPosition.push(555 + 29 + (i * 192), 757 + 12);

  };

  this.greenRect = createRect({
    w: 247,
    h: 246,
    color: 0x00ff00,
    alpha: 0.6,
    lineStyle: 0
  });
  this.redRect = createRect({
    w: 247,
    h: 246,
    color: 0xff0000,
    alpha: 0.6,
    lineStyle: 0
  });

  this.waggonPosition = [];

  for (var i = 0; i < 10; i++) {
    this.waggonPosition.push(i * 167 + 334);
  }

  this.resetWaggon();

};

Tivoli_5_2.prototype.resetWaggon = function(arguments) {

  if (this.requestId)
    cancelAnimationFrame(this.requestId);

  if (this.requestId2)
    cancelAnimationFrame(this.requestId2);

  this.waggonContainer.removeChildren();
  this.feedbackContainer.removeChildren();

  this.waggonContainer.position = {
    x: 0,
    y: 0
  };

  this.feedbackContainer.position.x = 0;

  if (this.feedbackSpriteArr) {
    this.feedbackSpriteArr.length = 0;
  }

  if (this.waggonTimer)
    clearTimeout(this.waggonTimer);

  this.feedbackSpriteArr = [];

  for (var i = 0; i < 10; i++) {
    var feedbackSprite = PIXI.Sprite.fromFrame('fb_' + i + '.png');

    feedbackSprite.position = {
      x: -10,
      y: 886
    };

    feedbackSprite.visible = false;

    main.texturesToDestroy.push(feedbackSprite);

    this.feedbackSpriteArr.push(feedbackSprite);

    this.feedbackContainer.addChild(feedbackSprite);
  }

}

/**
 * exercise logic
 */
Tivoli_5_2.prototype.runExercise = function() {

  var self = this;

  this.jsonData = this.wallSlices[0];

  this.dragText = [];

  this.dropArea1 = false;
  this.dropArea2 = false;
  this.dropArea3 = false;
  this.dropArea4 = false;

  var soundTexture = PIXI.Texture.fromFrame("sound_grey.png"),
    imageSprite = null,
    soundSprite = null,
    questionText = null,
    answers = ["det", "han", "den", "hon"],
    answerText = null;

  for (var i = 0; i < 4; i++) {

    imageSprite = this.jsonData.image[i];

    soundSprite = new PIXI.Sprite(soundTexture);

    imageSprite.position.set(this.whiteBoxPosition[i * 2], this.whiteBoxPosition[
      i * 2 + 1]);

    imageSprite.width = imageSprite.height = 241;

    soundSprite.interactive = true;
    soundSprite.buttonMode = true;
    soundSprite.howl = this.jsonData.sound[i];
    soundSprite.position.set(this.whiteBoxPosition[i * 2], this.whiteBoxPosition[
      i * 2 + 1] + 196);

    questionText = new PIXI.Text(this.jsonData.sentence[i].replace("____",
      "___"), {
      font: "68px Arial",
      align: "center",
      wordWrapWidth: 600,
      wordWrap: true,
    });
    main.texturesToDestroy.push(questionText);

    questionText.position.x = this.quesTextPosition[i * 2];
    questionText.position.y = this.quesTextPosition[i * 2 + 1];

    answerText = new PIXI.Text(answers[i].ucfirst(), {
      font: "68px Arial"
    });

    main.texturesToDestroy.push(answerText);

    answerText.position = {
      x: this.ansTextPosition[i * 2],
      y: this.ansTextPosition[i * 2 + 1] + 10
    };

    answerText.interactive = true;
    answerText.buttonMode = true;

    this.rhymeContainer.addChild(imageSprite);
    this.rhymeContainer.addChild(soundSprite);

    this.rhymeContainer.addChild(this.greenRect);
    this.rhymeContainer.addChild(this.redRect);
    this.greenRect.visible = false;
    this.redRect.visible = false;

    this.rhymeContainer.addChild(questionText);
    this.rhymeContainer.addChild(answerText);


    // play sprite sound on click/tap event
    soundSprite.click = soundSprite.tap = this.onSoundPressed.bind(
      soundSprite);

    answerText.answer = answers[i];
    answerText.dropPosition = this.quesTextPosition;
    answerText.oldPosition = {
      x: this.ansTextPosition[i * 2],
      y: this.ansTextPosition[i * 2 + 1] + 10
    };
    this.dragText.push(answerText);
  };

  for (var i = 0; i < 4; i++) {
    this.onDragging(i);
  };

};

Tivoli_5_2.prototype.onSoundPressed = function() {
  createjs.Sound.stop();
  this.soundInstance = createjs.Sound.play(this.howl, {
    interrupt: createjs.Sound.INTERRUPT_NONE
  });
  this.soundInstance.addEventListener("failed", handleFailed);
  this.soundInstance.addEventListener("complete", handleComplete);
};

/**
 * drag logic
 */
Tivoli_5_2.prototype.onDragging = function(i) {
  var self = this;
  // use the mousedown and touchstart
  this.dragText[i].mousedown = this.dragText[i].touchstart = function(data) {
    if (addedListeners) return false;
    data.originalEvent.preventDefault()

    this.bringToFront();
    // store a refference to the data
    // The reason for this is because of multitouch
    // we want to track the movement of this particular touch
    this.data = data;
    this.alpha = 0.9;
    this.dragging = true;
    this.sx = this.data.getLocalPosition(this).x;
    this.sy = this.data.getLocalPosition(this).y;
  }

  this.dragText[i].mouseup = this.dragText[i].mouseupoutside = this.dragText[
    i].touchend = this.dragText[i].touchendoutside = function(data) {
    if (addedListeners) return false;
    this.alpha = 1
    this.dragging = false;
    // set the interaction data to null
    this.data = null;

    var dropArea1 = null,
      dropArea2 = null,
      dropArea3 = null,
      dropArea4 = null;

    dropArea1 = isInsideRectangle(this.dx, this.dy, this.dropPosition[0],
      this.dropPosition[1] - 28, this.dropPosition[0] + 447, this.dropPosition[
        1] + 42 + 28);

    dropArea2 = isInsideRectangle(this.dx, this.dy, this.dropPosition[2],
      this.dropPosition[3] - 28, this.dropPosition[2] + 447, this.dropPosition[
        3] + 42 + 28);

    dropArea3 = isInsideRectangle(this.dx, this.dy, this.dropPosition[4],
      this.dropPosition[5] - 28, this.dropPosition[4] + 447, this.dropPosition[
        5] + 42 + 28);

    dropArea4 = isInsideRectangle(this.dx, this.dy, this.dropPosition[6],
      this.dropPosition[7] - 28, this.dropPosition[6] + 447, this.dropPosition[
        7] + 42 + 28);

    if (self.dropArea1) dropArea1 = false;
    if (self.dropArea2) dropArea2 = false;
    if (self.dropArea3) dropArea3 = false;
    if (self.dropArea4) dropArea4 = false;

    if (dropArea1 || dropArea2 || dropArea3 || dropArea4) {

      this.interactive = false;

      if (dropArea1) {
        // console.log("Chosen answer = " + this.answer + ", Correct answer = " + self.jsonData.answer[0]);
        if (this.answer === self.jsonData.answer[0]) {
          correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
          self.showRedOrGreenBox(self.whiteBoxPosition[0], self.whiteBoxPosition[
            1], self.greenRect);
          this.position.x = this.dropPosition[0];
          this.position.y = this.dropPosition[1] + 5;

          self.dropArea1 = true;
        } else {
          wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
          self.showRedOrGreenBox(self.whiteBoxPosition[0], self.whiteBoxPosition[
            1], self.redRect);
          this.position.x = this.oldPosition.x;
          this.position.y = this.oldPosition.y;
          this.interactive = true;
          self.dropArea1 = false;
        }
      }

      if (dropArea2) {
        if (this.answer === self.jsonData.answer[1]) {
          correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
          self.showRedOrGreenBox(self.whiteBoxPosition[2], self.whiteBoxPosition[
            3], self.greenRect);
          this.position.x = this.dropPosition[2];
          this.position.y = this.dropPosition[3] + 5;
          self.dropArea2 = true;
        } else {
          wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
          self.showRedOrGreenBox(self.whiteBoxPosition[2], self.whiteBoxPosition[
            3], self.redRect);
          this.position.x = this.oldPosition.x;
          this.position.y = this.oldPosition.y;
          this.interactive = true;
          self.dropArea2 = false;
        }
      }

      if (dropArea3) {
        if (this.answer === self.jsonData.answer[2]) {
          correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
          self.showRedOrGreenBox(self.whiteBoxPosition[4], self.whiteBoxPosition[
            5], self.greenRect);
          this.position.x = this.dropPosition[4];
          this.position.y = this.dropPosition[5] + 5;
          self.dropArea3 = true;
        } else {
          wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
          self.showRedOrGreenBox(self.whiteBoxPosition[4], self.whiteBoxPosition[
            5], self.redRect);
          this.position.x = this.oldPosition.x;
          this.position.y = this.oldPosition.y;
          this.interactive = true;
          self.dropArea3 = false;
        }
      }

      if (dropArea4) {
        if (this.answer === self.jsonData.answer[3]) {
          correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
          self.showRedOrGreenBox(self.whiteBoxPosition[6], self.whiteBoxPosition[
            7], self.greenRect);
          this.position.x = this.dropPosition[6];
          this.position.y = this.dropPosition[7] + 5;
          self.dropArea4 = true;
        } else {
          wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
          self.showRedOrGreenBox(self.whiteBoxPosition[6], self.whiteBoxPosition[
            7], self.redRect);
          this.position.x = this.oldPosition.x;
          this.position.y = this.oldPosition.y;
          this.interactive = true;
          self.dropArea4 = false;
        }
      }

      // check if all dragged correctly or not
      if (self.dropArea1 && self.dropArea2 && self.dropArea3 && self.dropArea4) {
        setTimeout(function() {
          //correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
          self.correctFeedback();
        }, 2000);
      }

    } else {
      // reset dragged element position
      this.position.x = this.oldPosition.x;
      this.position.y = this.oldPosition.y;
    }
  }

  // set the callbacks for when the mouse or a touch moves
  this.dragText[i].mousemove = this.dragText[i].touchmove = function(data) {
    if (addedListeners) return false;
    // set the callbacks for when the mouse or a touch moves
    if (this.dragging) {
      // need to get parent coords..
      var newPosition = this.data.getLocalPosition(this.parent);
      this.position.x = newPosition.x - this.sx;
      this.position.y = newPosition.y - this.sy;
      this.dx = newPosition.x;
      this.dy = newPosition.y;
    }
  };
}

/**
 * show red/green sprite on correct/wrong
 * @param  {int} xPos
 * @param  {int} yPos
 * @param  {object} obj
 */
Tivoli_5_2.prototype.showRedOrGreenBox = function(xPos, yPos, obj) {
  var self = this;

  obj.position = {
    x: xPos,
    y: yPos
  };

  obj.visible = true;

  setTimeout(function() {
    obj.visible = false;
  }, 400);


}

/**
 * on correct answer update feedback
 */
Tivoli_5_2.prototype.correctFeedback = function() {

  setTimeout(this.updateFeedback.bind(this), 1000);
}

/**
 * load outro scene
 */
Tivoli_5_2.prototype.onComplete = function() {

  var self = this;

  this.introMode = false;

  this.rhymeContainer.visible = false;
  this.rhymeContainerBase.visible = false;

  this.ovaigen.click = this.ovaigen.tap = this.resetExercise.bind(this);

  this.outroAnimation();

}

/**
 * reset values to default
 */
Tivoli_5_2.prototype.resetExercise = function() {

  var self = this;

  self.resetWaggon();

  createjs.Sound.stop();

  this.ovaigen.visible = false;
  localStorage.setItem('tivoli_5_2', 0);

  self.rhymeContainer.visible = true;
  self.rhymeContainerBase.visible = true;

  self.feedbackContainer.position.x = 0;

  this.introMode = true;

  self.runNextSet();


};

/**
 * update feedback
 * @param  {string} type
 * @return {int}      total feedback value
 */
Tivoli_5_2.prototype.updateFeedback = function() {

  var self = this;

  self.requestId = null;

  var len = localStorage.getItem('tivoli_5_2');

  if (len < 1)
    self.waggonContainer.position.x = 0;

  len++;

  if (len < 0) {
    localStorage.setItem('tivoli_5_2', 0);
    return false;
  } else if (len >= Tivoli_5_2.totalFeedback) {
    len = Tivoli_5_2.totalFeedback;
    localStorage.setItem('tivoli_5_2', Tivoli_5_2.totalFeedback);
  } else {
    localStorage.setItem('tivoli_5_2', len);
  }

  function animateFeedback(index) {

    self.feedbackSpriteArr[index].visible = true;

    var step = function() {

      var finalPosition = (index < 1) ? 332 : 167;

      if (self.feedbackSpriteArr[index].position.x < finalPosition)
        self.feedbackSpriteArr[index].position.x += 18;
      else
        return;

      self.requestId = requestAnimFrame(step);
    }
    step();

  }

  var currentFeedback = len - 1;

  clearTimeout(self.waggonTimer);

  animateFeedback(currentFeedback);

  if (currentFeedback < Tivoli_5_2.totalFeedback)
    setTimeout(this.runNextSet.bind(this), 1000);

  if (currentFeedback > 0)
    self.waggonTimer = setTimeout(this.animateWaggon.bind(this), 1200);
  else
    self.waggonContainer.addChild(self.feedbackSpriteArr[0]);


}

Tivoli_5_2.prototype.animateWaggon = function() {

  var self = this;

  self.requestId2 = null;

  if (self.requestId) {
    cancelAnimationFrame(self.requestId);

    var feedback = localStorage.getItem('tivoli_5_2') - 1;
    self.waggonContainer.addChild(self.feedbackSpriteArr[feedback]);
    self.waggonContainer.position.x = 0;
    var total = self.waggonContainer.children.length;
    var temparr = [];
    for (var i = feedback; i >= 0; i--) {
      temparr.push(self.waggonPosition[i]);
    }
    for (var i = 0; i < feedback; i++) {
      self.waggonContainer.children[i].position.x = temparr[i] - 167
    }

  }

  function animateWaggonContainer() {

    var feedback = localStorage.getItem('tivoli_5_2'),
      stopX = (feedback >= Tivoli_5_2.totalFeedback) ? 167 * Tivoli_5_2.totalFeedback + 167 :
      167,
      speed = (feedback >= Tivoli_5_2.totalFeedback) ? 14 : 14;

    if (feedback >= Tivoli_5_2.totalFeedback)
      self.onComplete();

    var step = function() {

      if (self.waggonContainer.position.x <= stopX) {
        self.waggonContainer.position.x += speed;
      } else {
        return;
      }
      self.requestId2 = requestAnimFrame(step);
    }
    step();
  }

  animateWaggonContainer();
}

/**
 * borrow objects from exercise pool to preload
 */
Tivoli_5_2.prototype.initObjectPooling = function() {

  var self = this;

  this.wallSlices = [];

  this.poolSlices = [];

  self.pool = {};

  var loader = new PIXI.JsonLoader(
    'uploads/exercises/tivoli/tivoli_5_2.json?noache=' + (new Date()).getTime()
  );
  loader.on('loaded', function(evt) {
    //data is in loader.json
    self.pool = new ExerciseSpritesPool(loader.json);
    //borrow two sprites once
    self.borrowWallSprites(2);
    self.runExercise();

  });
  loader.load();
}

/**
 * cache borrowed sprites
 * @param  {int} num total number of pool object
 */
Tivoli_5_2.prototype.borrowWallSprites = function(num) {

  var self = this;

  for (var i = 0; i < num; i++) {
    var sprite = this.pool.borrowSprite();
    this.poolSlices.push(sprite);

    var imageArr = [],
      soundArr = [],
      answerArr = [],
      wordArr = [],
      sentenceArr = [],
      spriteObj = sprite[0];


    for (var j = 0; j < 4; j++) {
      var poolImageSprite = PIXI.Sprite.fromImage('uploads/images/' +
        spriteObj.image[j]);
      imageArr.push(poolImageSprite);
      main.texturesToDestroy.push(poolImageSprite);

      var howl = loadSound('uploads/audios/' + StripExtFromFile(spriteObj.sound[
        j]));

      soundArr.push(howl);

      wordArr.push(spriteObj.word[j]);
      answerArr.push(spriteObj.answer[j]);
      sentenceArr.push(spriteObj.sentence[j]);
    };

    shuffle(imageArr, soundArr, wordArr, answerArr, sentenceArr);

    this.wallSlices.push({
      image: imageArr,
      sound: soundArr,
      answer: answerArr,
      word: wordArr,
      sentence: sentenceArr
    });


  }
};

/**
 * hand visibility
 * @param  {int} number
 */
Tivoli_5_2.prototype.showHandAt = function(number) {
  for (var i = 0; i < this.hands.length; i++) {
    this.hands[i].visible = false;
  }
  if (isNumeric(number))
    this.hands[number].visible = true;
};

/**
 * return back used objects to pool
 */
Tivoli_5_2.prototype.returnWallSprites = function() {
  var sprite = this.poolSlices[0];

  this.pool.returnSprite(sprite);

  this.rhymeContainer.removeChildren();

  this.poolSlices = [];
  this.wallSlices.shift();

};

/**
 * run next set
 */
Tivoli_5_2.prototype.runNextSet = function() {
  this.returnWallSprites();
  this.borrowWallSprites(1);
  this.runExercise();
};

String.prototype.ucfirst = function() {
  return this.charAt(0).toUpperCase() + this.slice(1).toLowerCase()
}

/**
 * memory management
 */
Tivoli_5_2.prototype.cleanMemory = function() {

  if (this.sceneBackground)
    this.sceneBackground.clear();

  this.sceneBackground = null;

  if (this.brownLayer)
    this.brownLayer.clear();

  this.brownLayer = null;

  if (this.blackLayer)
    this.blackLayer.clear();

  this.blackLayer = null;

  this.sky_1 = null;
  this.sky_2 = null;
  this.sky_3 = null;

  if (this.mouthParams)
    this.mouthParams.length = 0;


  this.arms = null;
  this.mouths = null;

  this.girlhelp = null;

  this.help = null;
  this.ovaigen = null;

  this.whiteLayer = null;
  this.pinkLayer = null;
  this.greenRect = null;
  this.redRect = null;
  this.bigBox = null;
  this.smallBox = null;

  if (this.jsonData)
    this.jsonData.length = 0;

  if (this.dragText)
    this.dragText.length = 0;

  this.dropArea1 = null;
  this.dropArea2 = null;
  this.dropArea3 = null;
  this.dropArea4 = null;

  if (this.poolSlices)
    this.poolSlices.length = 0;

  if (this.wallSlices)
    this.wallSlices.length = 0;

  this.pool = null;

  if (this.feedbackPositionX)
    this.feedbackPositionX.length = 0;

  this.sceneContainer = null;
  this.introContainer = null;
  this.rhymeContainer = null;
  this.rhymeContainerBase = null;
  this.feedbackContainer = null;

  this.baseContainer.removeChildren();
  this.baseContainer = null;
}