function Tivoli_1_1() {
    cl.show();

    PIXI.DisplayObjectContainer.call(this);

    main.CurrentExercise = "rimord1";
    //ceate empty container to store all rimord scene stuffs
    this.baseContainer = new PIXI.DisplayObjectContainer();

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    //ceate empty container to store all rimord scene stuffs
    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();

    //create empty container for rhyme game
    this.imageContainer = new PIXI.DisplayObjectContainer();

    //create empty container for rhyme game
    this.rhymeContainer = new PIXI.DisplayObjectContainer();

    //create empty container for bag game
    this.feedbackContainer = new PIXI.DisplayObjectContainer();

    //create empty menu container
    this.menuContainer = new PIXI.DisplayObjectContainer();

    this.commonMenuContainer = new PIXI.DisplayObjectContainer();

    localStorage.setItem('tivoli_1_1', 0);

    main.texturesToDestroy = [];

    this.sceneContainer.position = {
        x: 77,
        y: 88
    };
    this.baseContainer.addChild(this.sceneContainer);
    this.loadSpriteSheet();
    this.addChild(this.baseContainer);



}

Tivoli_1_1.constructor = Tivoli_1_1;
Tivoli_1_1.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

Tivoli_1_1.imagePath = 'build/exercises/tivoli/tivoli_1_1/images/';
Tivoli_1_1.audioPath = 'build/exercises/tivoli/tivoli_1_1/audios/';
Tivoli_1_1.commonFolder = 'build/common/images/tivoli/';
Tivoli_1_1.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

/**
 * load images,sounds, sprites
 */
Tivoli_1_1.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(Tivoli_1_1.audioPath + 'intro');
    this.outroId = loadSound(Tivoli_1_1.audioPath + 'outro');

    // create an array of assets to load
    var assetsToLoad = [
        Tivoli_1_1.imagePath + "girlwithwheel.png",
        Tivoli_1_1.imagePath + "girlwithwheeloutro.png",
        Tivoli_1_1.imagePath + "sprite_1_1.json",
        Tivoli_1_1.imagePath + "sprite_1_1.png",
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

};


Tivoli_1_1.prototype.spriteSheetLoaded = function() {

    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    this.loadDefaults();

};

/**
 * load exercise default sprites
 */
Tivoli_1_1.prototype.loadDefaults = function() {

    var self = this;

    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0xC9D0E8);
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xF4CDE2);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);

    this.baseContainer.addChild(this.introContainer);

    // girl with table without hand and mouth
    this.girlwithwheel = new PIXI.Sprite.fromImage(Tivoli_1_1.imagePath + "girlwithwheel.png");
    this.girlwithwheel.position = {
        x: 105,
        y: 264
    };
    main.texturesToDestroy.push(this.girlwithwheel);
    this.introContainer.addChild(this.girlwithwheel);

    this.wheelParams = {
        "image": "wheel_",
        "length": 2,
        "x": 642,
        "y": 464,
    };
    this.wheel = new Animation(this.wheelParams);

    this.introContainer.addChild(this.wheel);
    this.wheel.show();

    this.help = PIXI.Sprite.fromFrame("topcorner.png");
    this.help.position = {
        x: 0,
        y: 0
    };
    this.help.buttonMode = true;
    this.help.interactive = true;
    this.baseContainer.addChild(this.help);

    this.mouthParams = {
        "image": "mouth_",
        "length": 3,
        "x": 332,
        "y": 475,
        "speed": 6,
        "pattern": true,
        "frameId": 1
    };
    this.mouth = new Animation(this.mouthParams);
    this.introContainer.addChild(this.mouth);
    this.mouth.gotoAndStop(2);

    this.hands = [
        new PIXI.Sprite.fromFrame('hand_1.png'),
        new PIXI.Sprite.fromFrame('hand_2.png'),
        new PIXI.Sprite.fromFrame('hand_3.png'),
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(374, 560);
        this.introContainer.addChild(this.hands[i]);
        this.hands[i].visible = false;
    }

    this.showHandAt(2);

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.baseContainer.addChild(this.outroContainer);
    this.outroContainer.visible = false;

    this.girlwithwheeloutro = new PIXI.Sprite.fromImage(Tivoli_1_1.imagePath + "girlwithwheeloutro.png");
    this.girlwithwheeloutro.position = {
        x: 86,
        y: 91
    };
    main.texturesToDestroy.push(this.girlwithwheeloutro);
    this.outroContainer.addChild(this.girlwithwheeloutro);

    this.mouthParamsOutro = {
        "image": "mouth_outro_",
        "length": 3,
        "x": 1024,
        "y": 552,
        "speed": 6,
        "pattern": true,
        "frameId": 2
    };
    this.mouthOutro = new Animation(this.mouthParamsOutro);
    this.outroContainer.addChild(this.mouthOutro);

    this.eyesOutro_1 = new PIXI.Sprite.fromFrame("eye_1.png");
    this.eyesOutro_2 = new PIXI.Sprite.fromFrame("eye_2.png");
    this.eyesOutro_1.position.set(1004, 474);
    this.eyesOutro_2.position.set(1004, 474);
    this.outroContainer.addChild(this.eyesOutro_1);
    this.outroContainer.addChild(this.eyesOutro_2);
    this.eyesOutro_2.visible = false;

    this.back_again = PIXI.Sprite.fromFrame("ovaigen.png");
    this.back_again.position = {
        x: 1342,
        y: 433
    };
    this.back_again.buttonMode = true;
    this.back_again.interactive = true;
    this.outroContainer.addChild(this.back_again);
    this.back_again.visible = false;

    this.feedbackPosition = [
        464, 255,
        581, 255,
        697, 255,
        814, 255,
        930, 255,
        1047, 255,
        1163, 255,
        1279, 255,
        1396, 255,
        1513, 255,
    ];
    this.feedbackTexture = PIXI.Texture.fromFrame('bag.png');
    this.introContainer.addChild(this.feedbackContainer);
    this.generateFeedback();

    this.initObjectPooling();
    this.introContainer.addChild(this.imageContainer);
    this.introContainer.addChild(this.rhymeContainer);
};

/**
 * show/hide hand at specific position
 * @param  {intege} number
 */
Tivoli_1_1.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    }
    if (isNumeric(number))
        this.hands[number].visible = true;
};

/**
 * play intro animation
 */
Tivoli_1_1.prototype.introAnimation = function() {

    createjs.Sound.stop();

    if (!this.introMode)
        this.resetFeedback();

    this.outroContainer.visible = false;
    this.introContainer.visible = true;

    this.help.interactive = false;
    this.help.buttonMode = false;

    main.overlay.visible = true;
    this.mouth.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });
    console.log(this.introInstance)

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
 0-1 sec arm_1  =>2
1-2,5 sec arm_2   => 1
2,5 -4 sec arm_3 => 0
stop at arm_1 => 2
 */

/*Tivoli_1_1.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();

    if (currentPostion >= 0 && currentPostion < 500) {
        this.showHandAt(2); //default hand
    } else if (currentPostion >= 500 && currentPostion < 2000) {
        this.showHandAt(1); //top hand
    } else if (currentPostion >= 2000 && currentPostion < 3000) {
        this.showHandAt(0); //bottom hand
    } else if (currentPostion >= 3000 && currentPostion < 4000) {
        this.showHandAt(1); //top hand
    } else {
        this.showHandAt(2); //default hand
    }
};*/

Tivoli_1_1.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    console.log(currentPostion);

    if (currentPostion >= 0 && currentPostion < 500) {
        this.showHandAt(2); //default hand
    } else if (currentPostion >= 500 && currentPostion < 2000) {
        this.showHandAt(1); //top hand
    } else if (currentPostion >= 2000 && currentPostion < 3000) {
        this.showHandAt(0); //bottom hand
    } else if (currentPostion >= 3000 && currentPostion < 4260) {
        this.showHandAt(1); //top hand
    } else {
        this.showHandAt(2); //default hand
    }
};


/**
 * handle mouth, hand,eyes with specific sound positions or queue points.
0-2.5 sec eyes_1
2.5 - 4 sec eyes_2
stop at eyes_1
 */

Tivoli_1_1.prototype.animateOutro = function() {

    var currentPostion = this.outroInstance.getPosition();

    if (currentPostion >= 1000 && currentPostion < 2500) {
        this.eyesOutro_1.visible = true;
        this.eyesOutro_2.visible = false;
    } else if (currentPostion >= 2500 && currentPostion < 4000) {
        this.eyesOutro_1.visible = false;
        this.eyesOutro_2.visible = true;
    } else {
        this.eyesOutro_1.visible = true;
        this.eyesOutro_2.visible = false;
    }
};

/**
 * cancel running intro animation
 */
Tivoli_1_1.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);

    this.mouth.hide(2);
    this.showHandAt(2);

    this.help.interactive = true;
    this.help.buttonMode = true;

    main.overlay.visible = false;
};

/**
 * play outro animation on completing intro page
 */
Tivoli_1_1.prototype.outroAnimation = function() {

    this.outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        this.mouthOutro.show();
        this.timerOutroId = setInterval(this.animateOutro.bind(this), 1);
    }

    this.outroCancelHandler = this.cancelOutro.bind(this);
    this.outroInstance.addEventListener("complete", this.outroCancelHandler);

};

/**
 * cancel running outro animation clickin anywhere on canvas
 */
Tivoli_1_1.prototype.cancelOutro = function() {

    createjs.Sound.stop();
    this.back_again.visible = true;

    this.help.interactive = true;
    this.help.buttonMode = true;

    this.mouthOutro.hide(2);

    this.resetFeedback();
    this.getNewExerciseSet();

    main.overlay.visible = false;

};

/**
 * reset feedback and set intro mode to true
 */
Tivoli_1_1.prototype.resetFeedback = function() {
    this.hiddenFeedback.length = 0;
    this.feedbackContainer.removeChildren();
    localStorage.setItem('tivoli_1_1', 0);
    this.generateFeedback();
    this.introMode = true;
}

/**
 * get new sets clearing used container
 */
Tivoli_1_1.prototype.restartExercise = function() {

    var self = this;

    this.resetFeedback();
    this.back_again.visible = false;

    this.rhymeContainer.removeChildren();

    this.returnWallSprites();
    this.borrowWallSprites(1);
    this.runExercise();
};

/**
 * perform correct or wrong feedack
 * @param  {boolean} match
 */
Tivoli_1_1.prototype.updateGoodieBag = function(match) {

    if (match)
        this.correctFeedback();
    else
        this.wrongFeedback();

};

/**
 * get next exercise set on the queue
 */
Tivoli_1_1.prototype.getNewExerciseSet = function() {

    this.rhymeContainer.removeChildren();

    this.returnWallSprites();
    this.borrowWallSprites(1);
    this.runExercise();
};

/**
 * increment feedback by one on correct
 */
Tivoli_1_1.prototype.correctFeedback = function() {

    var feedback = localStorage.getItem('tivoli_1_1');
    this.hiddenFeedback[feedback].alpha = 1;
    feedback++;
    localStorage.setItem('tivoli_1_1', feedback);

    if (feedback >= Tivoli_1_1.totalFeedback) {
        addedListeners = true;
        setTimeout(this.onComplete.bind(this), 1000);
    } else
        this.getNewExerciseSet();


};

/**
 * feedback decrement by one on wrong
 */
Tivoli_1_1.prototype.wrongFeedback = function() {

    var feedback = localStorage.getItem('tivoli_1_1');
    feedback--;
    if (feedback >= 0) {
        this.hiddenFeedback[feedback].alpha = 0.2;
        localStorage.setItem('tivoli_1_1', feedback);
    };

};


/**
 * generate feedback sprites with visibility 20%
 */
Tivoli_1_1.prototype.generateFeedback = function() {

    this.hiddenFeedback = [];

    for (var i = 0; i < 10; i++) {
        var feedbackSprite = new PIXI.Sprite(this.feedbackTexture);
        feedbackSprite.position.x = this.feedbackPosition[i * 2];
        feedbackSprite.position.y = this.feedbackPosition[i * 2 + 1];
        feedbackSprite.alpha = 0.2;
        this.feedbackContainer.addChild(feedbackSprite);
        this.hiddenFeedback.push(feedbackSprite);
    };
}

/**
 * this sets the page to outro mode and hide all intro stuffs from showing
 */
Tivoli_1_1.prototype.onComplete = function() {

    var self = this;

    addedListeners = false;

    this.introMode = false;

    localStorage.removeItem('tivoli_1_1');

    this.introContainer.visible = false;

    this.help.interactive = false;
    this.help.buttonMode = false;

    this.outroContainer.visible = true;

    this.back_again.updateCache();

    this.back_again.click = this.back_again.tap = function(data) {
        self.cancelOutro();
        self.outroContainer.visible = false;
        self.introContainer.visible = true;
        self.restartExercise();
    };

    this.outroAnimation();
};

/**
 * game logic
 */
Tivoli_1_1.prototype.runExercise = function() {

    var self = this;

    var rhymesButtons = [];

    var redButtons = [];

    var greenButtons = [];

    var whiteButtons = [];

    var soundButtons = [];

    var howls = {};

    var feedback_status, match;

    var rhymePosition = [919, 570, 477, 947, 754, 947, 1032, 947, 1309, 947, ];

    var howlPosition = [919, 751, 477, 1127, 755, 1127, 1032, 1127, 1310, 1127, ];

    var jsonData = this.wallSlices[0];
    console.log(jsonData.answer);

    this.soundTexture = new PIXI.Texture.fromFrame("sound_grey.png");

    for (var i = 0; i < 5; i++) {

        var whiteHighlight = createRect();

        var sound = new PIXI.Sprite(this.soundTexture);

        sound.buttonMode = true;
        sound.position.x = howlPosition[i * 2];
        sound.position.y = howlPosition[i * 2 + 1];
        sound.interactive = true;
        sound.buttonIndex = i;

        if (i < 1) {
            // create new sprite using the texture
            var imageSprite = jsonData.question[0];
            sound.howl = jsonData.question[1];
        } else {
            var imageSprite = jsonData.image[i - 1];
            imageSprite.buttonMode = true;
            imageSprite.interactive = true;
            imageSprite.buttonIndex = i;
            imageSprite.width = imageSprite.height = 241;
            imageSprite.answer = jsonData.answer[i - 1];

            sound.howl = jsonData.sound[i - 1];
        }

        imageSprite.position = whiteHighlight.position = {
            x: rhymePosition[i * 2],
            y: rhymePosition[i * 2 + 1]
        };

        this.rhymeContainer.addChild(whiteHighlight);
        self.rhymeContainer.addChild(imageSprite);
        self.rhymeContainer.addChild(sound);

        rhymesButtons.push(imageSprite);
        whiteButtons.push(whiteHighlight);
        soundButtons.push(sound);

        sound.click = sound.tap = this.onSoundPressed.bind(sound);

    }

    rhymesButtons[1].click = rhymesButtons[1].tap = rhymesButtons[2].click = rhymesButtons[2].tap = rhymesButtons[3].click = rhymesButtons[3].tap = rhymesButtons[4].click = rhymesButtons[4].tap = function(data) {

        var hitObj = this;

        if (addedListeners) return false;
        addedListeners = true;

        hitObj.interactive = false;

        var index = this.buttonIndex;

        if (this.answer) {
            greenBox.position = this.position;

            self.rhymeContainer.removeChild(whiteButtons[index]);
            self.rhymeContainer.addChild(rhymesButtons[index]);
            self.rhymeContainer.addChild(soundButtons[index]);
            self.rhymeContainer.addChild(greenBox);

            correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY)

            setTimeout(function() {

                hitObj.interactive = true;

                var feedbackStorage = localStorage.getItem('tivoli_1_1');
                console.log(feedbackStorage, Tivoli_1_1.totalFeedback - 1, 'here')

                if (feedbackStorage != (Tivoli_1_1.totalFeedback - 1)) {
                    console.log(feedbackStorage, Tivoli_1_1.totalFeedback - 1, 'there')

                    self.rhymeContainer.removeChild(greenBox);
                    self.rhymeContainer.removeChild(rhymesButtons[index]);
                    self.rhymeContainer.removeChild(soundButtons[index]);

                    self.rhymeContainer.addChild(whiteButtons[index]);
                    self.rhymeContainer.addChild(rhymesButtons[index]);
                    self.rhymeContainer.addChild(soundButtons[index]);

                }

                addedListeners = false;

                self.updateGoodieBag(true);

            }, 200);

        } else {
            redBox.position = this.position;

            self.rhymeContainer.removeChild(whiteButtons[index]);
            self.rhymeContainer.addChild(rhymesButtons[index]);
            self.rhymeContainer.addChild(soundButtons[index]);
            self.rhymeContainer.addChild(redBox);

            wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY)
            setTimeout(function() {
                hitObj.interactive = true;
                self.rhymeContainer.removeChild(redBox);
                self.rhymeContainer.removeChild(rhymesButtons[index]);
                self.rhymeContainer.removeChild(soundButtons[index]);

                self.rhymeContainer.addChild(whiteButtons[index]);
                self.rhymeContainer.addChild(rhymesButtons[index]);
                self.rhymeContainer.addChild(soundButtons[index]);
                addedListeners = false;
                self.updateGoodieBag(false);
            }, 200);

        }

    };
};

Tivoli_1_1.prototype.onSoundPressed = function() {
    createjs.Sound.stop();
    this.soundInstance = createjs.Sound.play(this.howl, {
        interrupt: createjs.Sound.INTERRUPT_NONE
    });
    this.soundInstance.addEventListener("failed", handleFailed);
    this.soundInstance.addEventListener("complete", handleComplete);
};

/**
 * load exercise data
 * borrow next 2 sets
 */
Tivoli_1_1.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    this.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/tivoli/tivoli_1_1.json?nocache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        self.pool = new ExerciseSpritesPool(loader.json);
        //borrow two sprites once
        self.generatePoolCategory();
        self.borrowWallSprites(2);
        self.runExercise();

    });
    loader.load();
};


Tivoli_1_1.prototype.generatePoolCategory = function() {

    var keyArr = [];

    var spriteArr = this.pool.sprites,
        spriteLen = spriteArr.length;

    for (var i = 0; i < spriteLen; i++) {
        keyArr.push(spriteArr[i][0].word[0]);
    };

    this.poolCategory = keyArr.filter(onlyUnique);

}

/**
 * borrow sprites
 */
Tivoli_1_1.prototype.borrowWallSprites = function(num) {
    var rand, category, sprite, spriteObj;

    var self = this;
    this.num = num;

    for (var i = 0; i < this.num; i++) {

        rand = makeUniqueRandom(this.poolCategory.length);
        this.borrowCategory = this.poolCategory[rand];

        console.log('rand', rand)
        console.log('borrow category', this.borrowCategory);

        sprite = this.pool.borrowUniqueSprite(this.borrowCategory, this.lastBorrowSprite);
        this.poolSlices.push(sprite);

        var questionArr = [],
            imageArr = [],
            answerArr = [],
            soundArr = [];

        var spriteObj = sprite[0];

        this.lastBorrowSprite = spriteObj.word[0];



        for (var j = 0; j < spriteObj.image.length; j++) {
            var poolAnswerSprite = PIXI.Sprite.fromImage('uploads/images/' + spriteObj.image[j]);
            imageArr.push(poolAnswerSprite);
            main.texturesToDestroy.push(poolAnswerSprite);

            var poolAnswerHowl = loadSound('uploads/audios/' + StripExtFromFile(spriteObj.sound[j]));

            soundArr.push(poolAnswerHowl);
            answerArr.push(spriteObj.answer[j]);
        }

        var poolQuestionSprite = PIXI.Sprite.fromImage('uploads/images/' + spriteObj.word[0]);
        questionArr.push(poolQuestionSprite);
        main.texturesToDestroy.push(poolQuestionSprite);

        var questionHowl = loadSound('uploads/audios/' + StripExtFromFile(spriteObj.word[1]));

        questionArr.push(questionHowl);

        shuffle(imageArr, soundArr, answerArr);

        this.wallSlices.push({
            question: questionArr,
            image: imageArr,
            sound: soundArr,
            answer: answerArr
        });
    }
};

/**
 * return unused sets
 */
Tivoli_1_1.prototype.returnWallSprites = function() {
    this.pool.returnSprite(this.poolSlices[0]);

    for (var i = this.rhymeContainer.children.length - 1; i >= 0; i--) {
        this.rhymeContainer.removeChild(this.rhymeContainer.children[i]);
    }

    this.poolSlices.length = 0;
    this.wallSlices.shift();
};

/**
 * clean memory
 */
Tivoli_1_1.prototype.cleanMemory = function() {

    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    this.girlwithwheel = null;

    this.wheel = null;
    this.wheelParams = null;

    this.help = null;

    this.mouth = null;
    this.mouthParams = null;

    this.hand = null;

    this.girlwithwheeloutro = null;

    if (this.mouthParamsOutro)
        this.mouthParamsOutro.length = 0;

    this.mouthOutro = null;

    this.back_again = null;

    this.bag = null;

    if (this.poolSlices)
        this.poolSlices.length = 0;

    if (this.wallSlices)
        this.wallSlices.length = 0;

    this.pool = null;
    this.num = null;
    this.timerId = null;
    this.timerOutroId = null;

    this.sceneContainer = null;
    this.introContainer = null;
    this.outroContainer = null;
    this.imageContainer = null;
    this.rhymeContainer = null;
    this.feedbackContainer = null;
    this.menuContainer = null;
    this.commonMenuContainer = null;

    this.baseContainer.removeChildren();
    this.baseContainer = null;
};