/**
 * Roller Coaster 1
 */
function Tivoli_5_1() {
    cl.show();

    PIXI.DisplayObjectContainer.call(this);
    main.CurrentExercise = "dendet1";
    this.baseContainer = new PIXI.DisplayObjectContainer();

    this.sceneContainer = new PIXI.DisplayObjectContainer();

    this.feedbackContainer = new PIXI.DisplayObjectContainer();

    this.animationContainer = new PIXI.DisplayObjectContainer();
    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();
    this.rhymeContainer = new PIXI.DisplayObjectContainer();
    this.personContainer = new PIXI.DisplayObjectContainer();

    this.sceneContainer.position = {
        x: 77,
        y: 88
    };

    localStorage.setItem('tivoli_5_1', 0);

    this.baseContainer.addChild(this.sceneContainer);

    main.texturesToDestroy = [];

    this.loadSpriteSheet();

    this.DenDet = ['Den', 'Det'];

    // everything connected to stage is rendered.
    this.addChild(this.baseContainer);
}

Tivoli_5_1.constructor = Tivoli_5_1;
Tivoli_5_1.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Tivoli_5_1.imagePath = 'build/exercises/tivoli/tivoli_5_1/images/';
Tivoli_5_1.audioPath = 'build/exercises/tivoli/tivoli_5_1/audios/';
Tivoli_5_1.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

/**
 * preloader for audios,sprites and images
 */
Tivoli_5_1.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(Tivoli_5_1.audioPath + 'intro');
    this.outroId = loadSound(Tivoli_5_1.audioPath + 'outro');

    // create an array of assets to load
    var assetsToLoad = [
        Tivoli_5_1.imagePath + "sprite_5_1.json",
        Tivoli_5_1.imagePath + "sprite_5_1.png",
        Tivoli_5_1.imagePath + "sprite/ani_1.png",
        Tivoli_5_1.imagePath + "sprite/ani_2.png",
        Tivoli_5_1.imagePath + "sprite/ani_3.png",
        Tivoli_5_1.imagePath + "sprite/ani_4.png",
        Tivoli_5_1.imagePath + "sprite/ani_5.png",
        Tivoli_5_1.imagePath + "sprite/ani_6.png",
        Tivoli_5_1.imagePath + "sprite/ani_7.png",
        Tivoli_5_1.imagePath + "sprite/ani_8.png",
        Tivoli_5_1.imagePath + "sprite/ani_9.png",
        Tivoli_5_1.imagePath + "sprite/ani_10.png",
        Tivoli_5_1.imagePath + "sprite/ani_11.png",
        Tivoli_5_1.imagePath + "sprite/ani_12.png",
        Tivoli_5_1.imagePath + "sprite/ani_13.png",
        Tivoli_5_1.imagePath + "sprite/ani_14.png",
        Tivoli_5_1.imagePath + "sprite/ani_15.png",
        Tivoli_5_1.imagePath + "sprite/ani_16.png",
        Tivoli_5_1.imagePath + "sprite/ani_17.png",
        Tivoli_5_1.imagePath + "sprite/ani_18.png",
        Tivoli_5_1.imagePath + "rollercoaster.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * hide canvas loader, call functions on assets loaded
 */
Tivoli_5_1.prototype.spriteSheetLoaded = function() {

    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    this.loadDefaults();

    this.initObjectPooling();

    this.personContainer.addChild(this.animationContainer);
    this.introContainer.addChild(this.rhymeContainer);

}

/**
 * create sprites, movice clip and draw background using PIXI graphics
 */
Tivoli_5_1.prototype.loadDefaults = function() {

    var self = this;

    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0xC9D0E8);
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xF4CDE2);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);

    //white translucent background
    this.whiteBox = createRect({
        x: 631,
        y: 234,
        w: 1050,
        h: 750,
        color: 0xFFFFFF,
        alpha: 0.7
    })
    this.introContainer.addChild(this.whiteBox);



    this.blackTrack = createRect({
        x: 3,
        y: 1093,
        w: 1837,
        h: 107,
        color: 0X333334
    })
    this.sceneContainer.addChild(this.blackTrack);

    this.rollerCoasterBackground = PIXI.Sprite.fromImage(Tivoli_5_1.imagePath + "rollercoaster.png");
    this.rollerCoasterBackground.position = {
        x: 3,
        y: 32
    };
    main.texturesToDestroy.push(this.rollerCoasterBackground);
    this.sceneContainer.addChild(this.rollerCoasterBackground);

    //exercise tagrope and white box
    this.sceneContainer.addChild(this.introContainer);
    this.introContainer.addChild(this.personContainer);
    this.sceneContainer.addChild(this.outroContainer);
    this.outroContainer.visible = false;

    this.tagRope = PIXI.Sprite.fromFrame("tagrope.png");
    this.tagRope.position = {
        x: 947,
        y: 3
    };
    main.texturesToDestroy.push(this.tagRope);
    this.introContainer.addChild(this.tagRope);

    this.animationContainerForArms = new PIXI.DisplayObjectContainer();
    this.personContainer.addChild(this.animationContainerForArms);

    this.hands = [
        new PIXI.Sprite.fromFrame('arm_0.png'),
        new PIXI.Sprite.fromFrame('arm_1.png'),
        new PIXI.Sprite.fromFrame('arm_2.png')
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(602, 552);
        this.animationContainerForArms.addChild(this.hands[i]);
        this.hands[i].visible = false;
    }

    this.person = PIXI.Sprite.fromFrame("carriage.png");
    this.person.position = {
        x: 414,
        y: 789
    }
    main.texturesToDestroy.push(this.person);
    this.personContainer.addChild(this.person);

    this.defaultArms = PIXI.Sprite.fromFrame("arm_resting.png");
    this.defaultArms.position = {
        x: 621,
        y: 936
    }
    main.texturesToDestroy.push(this.defaultArms);
    this.animationContainerForArms.addChild(this.defaultArms);

    this.mouthParams = {
        "image": "mouth_",
        "length": 3,
        "x": 600,
        "y": 905,
        "speed": 6,
        "pattern": true,
        "frameId": 3
    };
    this.animateMouth = new Animation(this.mouthParams);

    this.animationContainer.addChild(this.animateMouth);
    this.personContainer.visible = false;

    this.help = PIXI.Sprite.fromFrame("topcorner.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position = {
        x: -77,
        y: -88
    }
    this.sceneContainer.addChild(this.help);

    this.ovaigen = PIXI.Sprite.fromFrame("ovaigen.png");
    this.ovaigen.buttonMode = true;
    this.ovaigen.interactive = true;
    this.ovaigen.position = {
        x: 56,
        y: 589
    };
    this.outroContainer.addChild(this.ovaigen);
    this.ovaigen.visible = false;
    //OUTROL ROLLERCOASTER ANIMATION
    //now stuffs for animation
    this.animParams = {
        "image": "ani_",
        "length": 18,
        "fromImage": true,
        "imageFolder": "build/exercises/tivoli/tivoli_5_1/images/sprite/",
        "x": 718,
        "y": 12,
        "speed": 6,
        "pattern": false,
        "frameId": 17
    };



    this.outroAnimationSprite = new Animation(this.animParams);
    this.outroContainer.addChild(this.outroAnimationSprite);
    this.outroAnimationSprite.visible = false;

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.startingCarriage = PIXI.Sprite.fromFrame("feedback_start.png");
    this.startingCarriage.position = {
        x: 3,
        y: 912
    };
    main.texturesToDestroy.push(this.startingCarriage);
    this.introContainer.addChild(this.startingCarriage);

    this.sceneContainer.addChild(this.feedbackContainer);

    var coverWhite = createRect({
        w: 300,
        h: 100,
        color: 0xffffff
    });
    coverWhite.position.set(580, -101);
    this.sceneContainer.addChild(coverWhite);

    this.updateFeedback();

    this.addFeedBackCarriage();

}

/**
 * start intro animation
 */
Tivoli_5_1.prototype.introAnimation = function() {

    var self = this;

    createjs.Sound.stop();

    if (!this.introMode)
        this.restartExercise();

    this.help.interactive = false;
    this.help.buttonMode = false;

    main.overlay.visible = true;

    self.personContainer.visible = true;
    self.defaultArms.visible = true;
    self.animateMouth.show(true);
    self.startingCarriage.visible = false;
    self.feedbackContainer.visible = false;

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
 0-3 sec arm_resting
3-5 sec arm_0
5-6 sec arm_1
6-7 sec arm_2
stop at arm_resting

 */
Tivoli_5_1.prototype.animateIntro = function() {

    var self = this;

    var currentPostion = this.introInstance.getPosition();
    if (currentPostion >= 0 && currentPostion < 3000) {
        self.defaultArms.visible = true;
    } else if (currentPostion >= 3000 && currentPostion < 5000) {
        self.showHandAt(0);
        self.defaultArms.visible = false;
    } else if (currentPostion >= 5000 && currentPostion < 6000) {
        self.showHandAt(1);
        self.defaultArms.visible = false;
    } else if (currentPostion >= 6000 && currentPostion < 7000) {
        self.showHandAt(2);
        self.defaultArms.visible = false;
    }
};

/**
 * cancel running intro animation
 */
Tivoli_5_1.prototype.cancelIntro = function() {

    var self = this;

    createjs.Sound.stop();

    var score = localStorage.getItem("tivoli_5_1");
    if (score < 1) {
        self.startingCarriage.visible = true;
    }
    self.feedbackContainer.visible = true;

    clearInterval(self.timerId);
    self.animateMouth.hide(3);
    self.showHandAt();
    self.defaultArms.visible = true;

    self.help.interactive = true;
    self.help.buttonMode = true;
    self.personContainer.visible = false;
    this.ovaigen.visible = false;
    main.overlay.visible = false;

};

/**
 * play outro movie clip animation
 */
Tivoli_5_1.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.feedbackContainer.visible = false;
        self.outroAnimationSprite.visible = true;
        self.outroAnimationSprite.showOnce();
    }
    this.outroCancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.outroCancelHandler);

};

/**
 * cancel running outro animation
 */
Tivoli_5_1.prototype.cancelOutro = function() {

    var self = this;

    createjs.Sound.stop();
    self.outroAnimationSprite.gotoAndStop(14);
    self.outroAnimationSprite.visible = false;
    self.ovaigen.visible = true;
    self.help.interactive = true;
    self.help.buttonMode = true;
    main.overlay.visible = false;

};


/**
 * exercise logic
 */
Tivoli_5_1.prototype.runExercise = function() {
    var self = this;
    var jsonData = this.wallSlices[0];
    var baseButtons = [],
        boxPosition = [
            849, 335,
            1040, 335
        ],
        questionPosition = [
            877, 353,
            1075, 353,
        ];



    for (var i = 0; i < 2; i++) {
        var spriteBase = PIXI.Sprite.fromFrame("button.png");

        spriteBase.position = {
            x: boxPosition[i * 2],
            y: boxPosition[i * 2 + 1]
        };

        spriteBase.interactive = true;
        spriteBase.buttonMode = true;
        spriteBase.hitIndex = i;
        main.texturesToDestroy.push(spriteBase);
        self.rhymeContainer.addChild(spriteBase);

        var question = new PIXI.Text(this.DenDet[i], {
            font: "60px Arial"
        });

        question.position = {
            x: questionPosition[i * 2],
            y: questionPosition[i * 2 + 1] + 10
        };
        main.texturesToDestroy.push(question);

        self.rhymeContainer.addChild(question);

        baseButtons.push(spriteBase);
    }

    this.questionContainer = new PIXI.DisplayObjectContainer();
    this.rhymeContainer.addChild(this.questionContainer);

    //ADD IMAGE RELATED TO SENTENCE
    var imageBox = createRect({
        x: 871,
        y: 513,
        color: 0xFFFFFF
    });
    console.log(imageBox.width);
    console.log(imageBox.height);
    this.questionContainer.addChild(imageBox);

    var imgSprite = jsonData.image[0];
    imgSprite.position = {
        x: 871,
        y: 513
    };
    imgSprite.width = imgSprite.height = 241;
    this.questionContainer.addChild(imgSprite);

    //NOW ADD SOUND AND SOUND ICON TO IMAGE
    var soundSprite = new PIXI.Sprite(PIXI.Texture.fromFrame("sound_grey.png"));
    soundSprite.position = {
        x: 871,
        y: 513 + 241 - 62
    }
    soundSprite.interactive = true;
    soundSprite.buttonMode = true;
    soundSprite.howl = jsonData.sound[0];

    soundSprite.click = soundSprite.tap = this.onSoundPressed.bind(soundSprite);

    this.questionContainer.addChild(soundSprite);

    //NOW ADD SENTENCE
    var qSentence = jsonData.sentence[0].replace(/_{2,}/g, "___ ");
    self.sentence = new PIXI.Text(qSentence, {
        font: "45px Arial",
        align: "center",
    });
    console.log(qSentence);
    main.texturesToDestroy.push(self.sentence);
    self.sentence.position = {
        x: 1169,
        y: 606
    };
    // console.log("answer = " + jsonData.answer[0]);
    this.questionContainer.addChild(self.sentence);
    for (var j = 0; j < 2; j++) {
        baseButtons[j].click = baseButtons[j].tap = function(data) {

            if (addedListeners) return false;
            addedListeners = true;

            var hitObj = this;
            if (jsonData.answer[0] == this.hitIndex) {
                var green = PIXI.Sprite.fromFrame("green.png");
                console.log('dentet', self.DenDet[hitObj.hitIndex]);
                self.sentence.setText(qSentence.replace("___ ", "" + self.DenDet[hitObj.hitIndex] + " "));
                baseButtons[hitObj.hitIndex].addChild(green);
                correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);

                setTimeout(function() {
                    baseButtons[hitObj.hitIndex].removeChild(green);
                    self.correctFeedback();
                }, 800);


            } else {
                var red = PIXI.Sprite.fromFrame("red.png");
                baseButtons[hitObj.hitIndex].addChild(red);
                wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);

                setTimeout(function() {
                    baseButtons[hitObj.hitIndex].removeChild(red);
                    self.wrongFeedback();
                }, 200);
            }
        }
    }


};

Tivoli_5_1.prototype.onSoundPressed = function() {
    this.soundInstance = createjs.Sound.play(this.howl, {
        interrupt: createjs.Sound.INTERRUPT_NONE
    });
    this.soundInstance.addEventListener("failed", handleFailed);
    this.soundInstance.addEventListener("complete", handleComplete);
};

/**
 * on correct answer feedback increase
 */
Tivoli_5_1.prototype.correctFeedback = function() {
    var self = this;

    var feedback = localStorage.getItem('tivoli_5_1');
    feedback++;

    if (feedback >= Tivoli_5_1.totalFeedback) {
        localStorage.setItem('tivoli_5_1', feedback);
        self.updateFeedback();

        setTimeout(function() {
            self.onComplete(); // go to outro scene
        }, 300);
    } else {
        localStorage.setItem('tivoli_5_1', feedback);
        self.updateFeedback();
        self.rhymeContainer.removeChildren();
        self.returnWallSprites();
        self.borrowWallSprites(1);
        self.runExercise();
    }
    addedListeners = false;

}

/**
 * on wrong answer, feedback decrease
 */
Tivoli_5_1.prototype.wrongFeedback = function() {
    var self = this;
    var feedback = localStorage.getItem('tivoli_5_1');
    feedback--;

    if (feedback < 0)
        feedback = 0;

    localStorage.setItem('tivoli_5_1', feedback);

    self.updateFeedback();
    addedListeners = false;

}

/**
 * load outro scene
 */
Tivoli_5_1.prototype.onComplete = function() {

    var self = this;

    //hide last carriage and show rollercoaster animation now.
    this.introContainer.visible = false;
    this.outroContainer.visible = true;
    var lastCarriage = localStorage.getItem('tivoli_5_1') - 1;
    self.feedbackContainer.visible = false;
    //beyond this comment may need modification

    this.introMode = false;

    this.rhymeContainer.visible = false;
    this.defaultArms.visible = false;

    this.showHandAt(1);

    this.help.interactive = false;
    this.help.buttonMode = false;

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        self.cancelOutro();
        self.help.interactive = true;
        self.help.buttonMode = true;
        self.restartExercise();
    }

    this.outroAnimation();
}

/**
 * add feedback carriage on correct
 */
Tivoli_5_1.prototype.addFeedBackCarriage = function() {
    var self = this;
    var feedBackPosition = [
        317, 852,
        337, 763,
        360, 670,
        384, 577,
        415, 478,
        449, 380,
        485, 287,
        529, 193,
        577, 106,
        700, 0
    ];

    for (var k = 0; k < 10; k++) {
        if (k < 9) {
            var feedbackSprite = PIXI.Sprite.fromFrame("feedback_" + k + ".png");
        } else {
            var feedbackSprite = PIXI.Sprite.fromFrame("feedback_" + 0 + ".png");
            feedbackSprite.rotation = 0.4
        }

        feedbackSprite.position = {
            x: feedBackPosition[k * 2],
            y: feedBackPosition[k * 2 + 1]
        };

        main.texturesToDestroy.push(feedbackSprite);
        self.feedbackContainer.addChild(feedbackSprite);

        feedbackSprite.visible = false;

    }
}

/**
 * update feedack
 * @return {[type]} [description]
 */
Tivoli_5_1.prototype.updateFeedback = function() {

    var self = this;
    var counter = localStorage.getItem('tivoli_5_1');

    for (var i = 0; i < self.feedbackContainer.children.length; i++) {
        self.feedbackContainer.children[i].visible = false;
    };

    if (counter > 0) {
        for (var j = 0; j < counter; j++) {

            if (j == counter - 1) {
                self.feedbackContainer.children[j].visible = true;
                self.startingCarriage.visible = false;
            }
        }
    };
    if (counter == 0) {
        self.startingCarriage.visible = true;
    }


}

/**
 * pool objects from exercise pool and get 2 sets to preload
 */
Tivoli_5_1.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/tivoli/tivoli_5_1.json?noache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        self.pool = new ExerciseSpritesPool(loader.json);
        //borrow two sprites once
        self.generatePoolCategory();
        self.borrowWallSprites(2);
        self.runExercise();

    });
    loader.load();
}

Tivoli_5_1.prototype.generatePoolCategory = function() {

    var keyArr = [];

    var spriteArr = this.pool.sprites,
        spriteLen = spriteArr.length;

    for (var i = 0; i < spriteLen; i++) {
        keyArr.push(spriteArr[i][0].image[0]);
    };

    this.poolCategory = keyArr.filter(onlyUnique);
    this.poolCategoryType = 'image';
}

/**
 * cached the borrowed 2 sets
 */
Tivoli_5_1.prototype.borrowWallSprites = function(num) {

    var rand, category, sprite, spriteObj;
    var self = this;

    for (var i = 0; i < num; i++) {
        rand = makeUniqueRandom(this.poolCategory.length);
        this.borrowCategory = this.poolCategory[rand];

        sprite = this.pool.borrowUniqueSprite(this.borrowCategory, this.lastBorrowSprite, this.poolCategoryType);
        //  var sprite = this.pool.borrowSprite();
        this.poolSlices.push(sprite);
        var imageArr = [];
        var soundArr = [];
        var answerArr = [];
        var wordArr = [];
        var spriteObj = sprite[0];

        for (var j = 0; j < spriteObj.image.length; j++) {
            this.lastBorrowSprite = spriteObj.image[j];
            var poolImageSprite = PIXI.Sprite.fromImage('uploads/images/' + spriteObj.image[j]);
            imageArr.push(poolImageSprite);
            main.texturesToDestroy.push(poolImageSprite);

            var howl = loadSound('uploads/audios/' + StripExtFromFile(spriteObj.sound[j]));
            soundArr.push(howl);

            answerArr.push(spriteObj.answer[j]);
            wordArr.push(spriteObj.sentence[j]);
        };

        shuffle(imageArr, soundArr, answerArr, wordArr);

        this.wallSlices.push({
            sentence: wordArr,
            image: imageArr,
            sound: soundArr,
            answer: answerArr,
        });
    }
};

/**
 * hand visibility by argument
 * @param  {int} number
 */
Tivoli_5_1.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    }
    if (isNumeric(number))
        this.hands[number].visible = true;
};

/**
 * return used pool
 */
Tivoli_5_1.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];

    this.pool.returnSprite(sprite);

    this.rhymeContainer.removeChildren();

    this.poolSlices = [];
    this.wallSlices.shift();
};

/**
 * reset exercise to default values and clear container
 */
Tivoli_5_1.prototype.resetExercise = function() {

    var self = this;

    this.ovaigen.visible = false;

    self.rhymeContainer.visible = true;
    self.introContainer.visible = true;
    self.defaultArms.visible = true;

    this.showHandAt();

    self.outroContainer.visible = false;
    self.outroAnimationSprite.gotoAndStop(0);
    self.feedbackContainer.visible = true;
    self.startingCarriage.visible = true;

    this.introMode = true;

};

/**
 * reset exercise and update feedback and get next sets
 */
Tivoli_5_1.prototype.restartExercise = function() {

    var self = this;

    this.resetExercise();

    localStorage.setItem('tivoli_5_1', 0);
    this.updateFeedback();

    self.rhymeContainer.removeChildren();

    this.returnWallSprites();
    this.borrowWallSprites(1);
    this.runExercise();
};

/**
 * unload destroy variables for memory management
 */
Tivoli_5_1.prototype.cleanMemory = function() {

    this.DenDet = null;

    if (this.sceneBackground)
        this.sceneBackground.clear();

    this.sceneBackground = null;

    if (this.whiteBox)
        this.whiteBox.clear();

    this.whiteBox = null;

    if (this.blackTrack)
        this.blackTrack.clear();

    this.blackTrack = null;

    this.rollerCoasterBackground = null;
    this.tagRope = null;

    this.introSound = null;
    this.outroSound = null;

    if (this.mouthParams)
        this.mouthParams.length = 0;

    if (this.animParams)
        this.animParams.length = 0;

    this.animateArms = null;
    this.animateMouth = null;
    this.outroAnimationSprite = null;

    this.startingCarriage = null;

    this.personContainer = null;
    this.person = null;
    this.defaultArms = null;

    this.animationContainerForArms = null;

    this.help = null;
    this.ovaigen = null;

    if (this.poolSlices)
        this.poolSlices.length = 0;

    if (this.wallSlices)
        this.wallSlices.length = 0;

    this.pool = null;

    this.sceneContainer = null;
    this.feedbackContainer = null;
    this.animationContainer = null;
    this.introContainer = null;
    this.outroContainer = null;
    this.rhymeContainer = null;
    this.personContainer = null;

    this.baseContainer.removeChildren();
    this.baseContainer = null;
}