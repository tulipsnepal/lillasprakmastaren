/*
Compound words, join two words to make composite word. test
*/
function Tivoli_4_1() {
  cl.show();
  PIXI.DisplayObjectContainer.call(this);
  main.CurrentExercise = "samman1";
  this.baseContainer = new PIXI.DisplayObjectContainer();

  this.sceneContainer = new PIXI.DisplayObjectContainer();
  this.introContainer = new PIXI.DisplayObjectContainer();
  this.outroContainer = new PIXI.DisplayObjectContainer();

  /*this.rhymeContainer = new PIXI.DisplayObjectContainer();*/
  this.rhymeContainerBase = new PIXI.DisplayObjectContainer();
  this.animationContainer = new PIXI.DisplayObjectContainer();
  /*this.answerContainerOne = new PIXI.DisplayObjectContainer();
	this.answerContainerTwo = new PIXI.DisplayObjectContainer();*/
  this.questionContainer = new PIXI.DisplayObjectContainer();
  this.personContainer = new PIXI.DisplayObjectContainer();
  this.sceneContainer.position = {
    x: 77,
    y: 88
  };

  this.baseContainer.addChild(this.sceneContainer);

  main.texturesToDestroy = [];

  this.loadSpriteSheet();

  // everything connected to stage is rendered.
  this.addChild(this.baseContainer);
}

Tivoli_4_1.constructor = Tivoli_4_1;
Tivoli_4_1.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Tivoli_4_1.imagePath = 'build/exercises/tivoli/tivoli_4_1/images/';
Tivoli_4_1.audioPath = 'build/exercises/tivoli/tivoli_4_1/audios/';

/**
 * load sprites, audios, imaegs befor game starts
 */
Tivoli_4_1.prototype.loadSpriteSheet = function() {

  this.introId = loadSound(Tivoli_4_1.audioPath + 'intro');
  this.outroId = loadSound(Tivoli_4_1.audioPath + 'outro');

  // create an array of assets to load
  var assetsToLoad = [
    Tivoli_4_1.imagePath + "sprite_4_1.json",
    Tivoli_4_1.imagePath + "sprite_4_1.png",
    Tivoli_4_1.imagePath + "bench.png"
  ];
  // create a new loader
  loader = new PIXI.AssetLoader(assetsToLoad);
  // use callback
  loader.onComplete = this.spriteSheetLoaded.bind(this);
  //begin load
  loader.load();

}

/**
 * on assets loaded , call needed function
 */
Tivoli_4_1.prototype.spriteSheetLoaded = function() {
  cl.hide();

  if (this.sceneContainer.stage)
    this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

  this.loadDefaults();

  this.initObjectPooling();
  this.introContainer.addChild(this.rhymeContainerBase);
  this.introContainer.addChild(this.questionContainer);
  this.introContainer.addChild(this.personContainer);
  this.introContainer.addChild(this.animationContainer);

}

/**
 * draw background, create sprites and movie clip animation
 */
Tivoli_4_1.prototype.loadDefaults = function() {

  var self = this;

  this.sceneBackground = new PIXI.Graphics();
  this.sceneBackground.beginFill(0xC9D0E8);
  // set the line style to have a width of 5 and set the color to red
  this.sceneBackground.lineStyle(3, 0xF4CDE2);
  // draw a rectangle
  this.sceneBackground.drawRect(0, 0, 1843, 1202);
  this.sceneContainer.addChild(this.sceneBackground);
  this.sceneContainer.addChild(this.introContainer);
  this.sceneContainer.addChild(this.outroContainer);

  //add bench

  this.introBench = PIXI.Sprite.fromImage(Tivoli_4_1.imagePath + "bench.png");
  this.introBench.position.set(855, 830);
  this.introContainer.addChild(this.introBench);


  this.person = PIXI.Sprite.fromFrame("park-manager.png");
  this.person.position = {
    x: 0,
    y: 218
  }
  main.texturesToDestroy.push(this.person);
  this.personContainer.addChild(this.person);

  this.defaultMouth = PIXI.Sprite.fromFrame("mouth_1.png");
  this.defaultMouth.position = {
    x: 198,
    y: 484
  }
  main.texturesToDestroy.push(this.defaultMouth);
  this.personContainer.addChild(this.defaultMouth);

  this.defaultArms = PIXI.Sprite.fromFrame("arm_1.png");
  this.defaultArms.position = {
    x: 263,
    y: 339
  }
  main.texturesToDestroy.push(this.defaultArms);
  this.animationContainer.addChild(this.defaultArms);
  this.mouthParams = {
    "image": "mouth_",
    "length": 3,
    "x": 198,
    "y": 484,
    "speed": 6,
    "pattern": true,
    "frameId": 1
  };
  this.animateMouth = new Animation(this.mouthParams);

  this.animationContainer.addChild(this.animateMouth);

  this.hands = [
    new PIXI.Sprite.fromFrame('arm_1.png'),
    new PIXI.Sprite.fromFrame('arm_2.png'),
    new PIXI.Sprite.fromFrame('arm_3.png'),
    new PIXI.Sprite.fromFrame('arm_0.png')
  ];
  for (var i = 0; i < this.hands.length; i++) {
    if (i == 3) { //adjusting hand position as arm_0.png was added later
      this.hands[i].position.set(263, 345);
    } else {
      this.hands[i].position.set(263, 339);
    }
    this.animationContainer.addChild(this.hands[i]);
    this.hands[i].visible = false;
  }

  this.animateMouth.gotoAndStop(3);

  //instruction icon
  this.help = PIXI.Sprite.fromFrame("topcorner.png");
  this.help.interactive = true;
  this.help.buttonMode = true;
  this.help.position = {
    x: -77,
    y: -88
  }
  this.sceneContainer.addChild(this.help);

  //OUTRO STUFFS
  this.girl_outro_torso = PIXI.Sprite.fromFrame('grilwithbroomsackorig.png'),
    this.head_outro_1 = PIXI.Sprite.fromFrame("head_1.png"),
    this.head_outro_2 = PIXI.Sprite.fromFrame("head_2.png"),
    this.topcornerOutro = PIXI.Sprite.fromFrame("topcorner.png"),
    this.ovaigen = PIXI.Sprite.fromFrame("ovaigen.png"),
    this.mouth_outro = PIXI.Sprite.fromFrame("mouth_outro_1.png");

  main.texturesToDestroy.push(this.girl_outro_torso);
  main.texturesToDestroy.push(this.head_outro_1);
  main.texturesToDestroy.push(this.head_outro_2);
  main.texturesToDestroy.push(this.mouth_outro);

  this.girl_outro_torso.position = {
    x: 320,
    y: 306
  }

  this.head_outro_2.position = {
    x: 1280,
    y: 77
  }

  this.mouth_outro.position = {
    x: 1369,
    y: 298
  }

  this.head_outro_1.position = {
    x: 1280,
    y: 77
  }

  this.ovaigen.buttonMode = true;
  this.ovaigen.interactive = true;
  this.ovaigen.position = {
    x: 250,
    y: 463
  };


  this.outroContainer.addChild(this.ovaigen);

  this.ovaigen.visible = false;


  this.outroContainer.addChild(this.girl_outro_torso);
  this.outroContainer.addChild(this.head_outro_2);

  this.outroContainer.addChild(this.head_outro_1);
  this.head_outro_1.visible = false;

  this.outroContainer.addChild(this.mouth_outro);
  this.outroContainer.visible = false;

  this.mouthOutroParams = {
    "image": "mouth_outro_",
    "length": 3,
    "x": 1369,
    "y": 298,
    "speed": 6,
    "pattern": true,
    "frameId": 1
  };
  this.animateMouthOutro = new Animation(this.mouthOutroParams);
  this.outroContainer.addChild(this.animateMouthOutro);
  self.animateMouthOutro.gotoAndStop(3);

  this.help.click = this.help.tap = this.introAnimation.bind(this);

  this.loadExerciseBase();

}

/**
 * start intro animation
 */
Tivoli_4_1.prototype.introAnimation = function() {

  var self = this;

  createjs.Sound.stop();

  if (!this.introMode)
    this.restartExercise();

  this.help.interactive = false;
  this.help.buttonMode = false;

  main.overlay.visible = true;

  self.defaultMouth.visible = false;
  self.defaultArms.visible = false;

  self.animateMouth.show();

  this.introInstance = createjs.Sound.play(this.introId, {
    interrupt: createjs.Sound.INTERRUPT_ANY
  });

  if (createjs.Sound.isReady())
    this.timerId = setInterval(this.animateIntro.bind(this), 1);

  this.introCancelHandler = this.cancelIntro.bind(this);
  this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.

 0-2 SEC ARM_0 => 3
2-4 SEC ARM_1 => 0
4-5 SEC ARM_3 => 2
5-6 SEC ARM_2 => 1
STOP AT ARM_1 => 3

 */
Tivoli_4_1.prototype.animateIntro = function() {

  var self = this;

  var currentPostion = this.introInstance.getPosition();
  if (currentPostion >= 1000 && currentPostion < 2000) {
    self.showHandAt(3);
  } else if (currentPostion >= 2000 && currentPostion < 4000) {
    self.showHandAt(0);
  } else if (currentPostion >= 4000 && currentPostion < 5000) {
    self.showHandAt(2);
  } else if (currentPostion >= 5000 && currentPostion < 6000) {
    self.showHandAt(1);
  } else {
    self.showHandAt(3);
  }

};

/**
 * cancel running intro animation
 */
Tivoli_4_1.prototype.cancelIntro = function() {

  createjs.Sound.stop();

  clearInterval(this.timerId);
  this.animateMouth.hide(3);
  this.showHandAt();
  this.defaultArms.visible = true;

  this.help.interactive = true;
  this.help.buttonMode = true;

  this.ovaigen.visible = false;
  main.overlay.visible = false;

};

/**
 * play outro animation
 */
Tivoli_4_1.prototype.outroAnimation = function() {

  var self = this;

  var outroInstance = createjs.Sound.play(this.outroId);
  if (createjs.Sound.isReady()) {
    main.overlay.visible = true;
    self.head_outro_1.visible = false;
    self.head_outro_2.visible = true;

    self.animateMouthOutro.show();
    self.mouth_outro.visible = false;
  }
  this.outroCancelHandler = this.cancelOutro.bind(this);
  outroInstance.addEventListener("complete", this.outroCancelHandler);

};

/**
 * cancel running outro instance
 */
Tivoli_4_1.prototype.cancelOutro = function() {

  createjs.Sound.stop();

  this.animateMouthOutro.hide(3);
  this.animateMouthOutro.visible = false;
  this.head_outro_1.visible = true;
  this.head_outro_2.visible = false;
  this.help.interactive = true;
  this.help.buttonMode = true;

  this.ovaigen.visible = true;
  main.overlay.visible = false;

};

/**
 * hide all hands at once and show only specific hand
 * @param  {int} number
 */
Tivoli_4_1.prototype.showHandAt = function(number) {
  for (var i = 0; i < this.hands.length; i++) {
    this.hands[i].visible = false;
  }
  if (isNumeric(number))
    this.hands[number].visible = true;
};

/**
 * create sprites, textures for exercise
 */
Tivoli_4_1.prototype.loadExerciseBase = function() {

  this.spritePosition = [];

  var whiteRect = createRect({
    w: 242,
    h: 242
  });
  this.whiteRectTexture = whiteRect.generateTexture();

  this.soundTexture = PIXI.Texture.fromFrame("sound_grey.png");

  this.answerContainerPosition = [
    484, 501 + 50,
    751, 501 + 50
  ];

  this.questionBox = new PIXI.Sprite(this.whiteRectTexture);
  this.questionBox.width = this.questionBox.height = 356;
  this.questionBox.position.set(578, 120);

  this.questionContainer.addChild(this.questionBox);
  this.questionObjContainer = new PIXI.DisplayObjectContainer();
};



Tivoli_4_1.prototype.getQuestion = function() {

  var self = this;

  this.questionBox.removeChildren();

  var nextIndex = makeUniqueRandom(6);

  this.question = this.questionSet[nextIndex].join('');

  var questionName = getNonUnicodeName(this.question);

  var questionSprite = PIXI.Sprite.fromImage('uploads/images/' + questionName + '.png'),
    questionSoundSprite = new PIXI.Sprite(this.soundTexture),
    questionText = new PIXI.Text(this.question, {
      font: "28px Arial",
      align: "center"
    });

  this.questionBox.addChild(questionSprite);


  questionSoundSprite.position.set(-5, 196);
  questionSoundSprite.interactive = true;
  questionSoundSprite.buttonMode = true;
  this.questionBox.addChild(questionSoundSprite);

  questionText.position = {
    x: (356 - 242) - 60,
    y: 210 + 34
  };
  this.questionBox.addChild(questionText);


  questionSoundSprite.howl = loadSound('uploads/audios/' + questionName);
  questionSoundSprite.click = questionSoundSprite.tap = function(data) {
    if (addedListeners) return false;
    addedListeners = true;
    var soundInstance = createjs.Sound.play(this.howl);
    soundInstance.addEventListener("failed", handleFailed);
    soundInstance.addEventListener("complete", function() {
      addedListeners = false;
    });
  }
}

/**
 * run exercise logic
 */
Tivoli_4_1.prototype.runExercise = function() {

  var self = this;

  var jsonData = this.wallSlices[0];

  var x = 0,
    y = 0;

  //ADD QUESTION
  var optionButtons = [];

  this.getQuestion();

  for (var i = 0; i < 12; i++) {

    if (i == 3 || i == 6 || i == 9) {
      x = 0;
      y++;
    }

    var xDiff = x * 253,
      yDiff = y * 253;

    var box1 = new PIXI.Sprite(this.whiteRectTexture);
    box1.position = {
      x: 1032 + xDiff,
      y: 80 + yDiff
    }

    this.rhymeContainerBase.addChild(box1);

    x++;

    var imageSprite = jsonData.image[i],
      wordSprite = jsonData.word[i],
      soundSprite = new PIXI.Sprite(this.soundTexture);


    soundSprite.interactive = true;
    soundSprite.buttonMode = true;

    //  optionContainer.addChild(spriteBase);
    box1.addChild(imageSprite);

    box1.addChild(soundSprite);

    soundSprite.position = {
      x: -5,
      y: 196
    };

    var label = new PIXI.Text(wordSprite, {
      font: "34px Arial",
      align: "center",
    })
    label.position = {
      x: (242 - label.width) / 2 - 35,
      y: 210 + 34
    };
    box1.addChild(label);
    label.visible = false;
    box1.label = label;

    box1.oPosition = {
      x: 1032 + xDiff,
      y: 80 + yDiff
    };

    box1.interactive = true;
    box1.buttonMode = true;

    box1.hitArea = new PIXI.Rectangle(0, 0, 242, 200);

    soundSprite.howl = jsonData.sound[i];

    box1.howlPositionX = soundSprite.position.x;
    box1.howlPositionY = soundSprite.position.y;

    box1.wordSprite = wordSprite;
    box1.hitIndex = i;


    soundSprite.click = soundSprite.tap = this.onSoundPressed.bind(soundSprite);

    optionButtons.push(box1);

  };

  var wordsChosen = [];

  var boxIndexChosen = [];

  var correctIndex = 0;

  for (var j = 0; j < optionButtons.length; j++) {
    var NoOfAnswerChosen = 0;
    var a1, a2;
    optionButtons[j].click = optionButtons[j].tap = function(data) {
      var obj = this;
      if (addedListeners) return false;
      var check = boxIndexChosen.indexOf(obj.hitIndex);
      if (check > -1) { //undo feature user can put first selection back to stack
        obj.position = {
          x: obj.oPosition.x,
          y: obj.oPosition.y
        };
        obj.label.visible = false;
        NoOfAnswerChosen = 0;
        boxIndexChosen = [];
        wordsChosen = [];
      } else {
        NoOfAnswerChosen++;

        if (NoOfAnswerChosen <= 2) {

          obj.label.visible = true;

          if (NoOfAnswerChosen == 1) {
            obj.position = {
              x: self.answerContainerPosition[0],
              y: self.answerContainerPosition[1],
            };
            wordsChosen.push(obj.label.text);
            boxIndexChosen.push(obj.hitIndex);
          } else {
            obj.position = {
              x: self.answerContainerPosition[2],
              y: self.answerContainerPosition[3],
            };
            wordsChosen.push(obj.label.text);
            boxIndexChosen.push(obj.hitIndex);
          }

          if (wordsChosen.length == 2) {

            if (wordsChosen[0] + wordsChosen[1] === self.question) //correct Answer
            {
              correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
              optionButtons[boxIndexChosen[0]].tint = 0x00ff00;
              optionButtons[boxIndexChosen[1]].tint = 0x00ff00;
              optionButtons[boxIndexChosen[0]].alpha = 0.6;
              optionButtons[boxIndexChosen[1]].alpha = 0.6;
              var b = setTimeout(function() {
                optionButtons[boxIndexChosen[0]].tint = 0xffffff;
                optionButtons[boxIndexChosen[1]].tint = 0xffffff;
                optionButtons[boxIndexChosen[0]].alpha = 1;
                optionButtons[boxIndexChosen[1]].alpha = 1;
              }, 900);
              correctIndex++;

              if (correctIndex == 6) {
                setTimeout(function() {
                  self.onComplete();
                }, 1600);

              } else {
                setTimeout(function() {

                  optionButtons[boxIndexChosen[0]].visible = false;
                  optionButtons[boxIndexChosen[1]].visible = false;

                  self.getQuestion();

                  wordsChosen = [];
                  boxIndexChosen = [];

                }, 1600);
              }
              NoOfAnswerChosen = 0;

            } else {
              wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
              optionButtons[boxIndexChosen[0]].tint = 0xff0000;
              optionButtons[boxIndexChosen[1]].tint = 0xff0000;
              optionButtons[boxIndexChosen[0]].alpha = 0.6;
              optionButtons[boxIndexChosen[1]].alpha = 0.6;
              var b = setTimeout(function() {
                optionButtons[boxIndexChosen[0]].label.visible = false;
                optionButtons[boxIndexChosen[1]].label.visible = false;
                optionButtons[boxIndexChosen[0]].tint = 0xffffff;
                optionButtons[boxIndexChosen[1]].tint = 0xffffff;
                optionButtons[boxIndexChosen[0]].alpha = 1;
                optionButtons[boxIndexChosen[1]].alpha = 1;
                optionButtons[boxIndexChosen[0]].position = {
                  x: optionButtons[boxIndexChosen[0]].oPosition.x,
                  y: optionButtons[boxIndexChosen[0]].oPosition.y
                };
                optionButtons[boxIndexChosen[1]].position = {
                  x: optionButtons[boxIndexChosen[1]].oPosition.x,
                  y: optionButtons[boxIndexChosen[1]].oPosition.y
                };
                wordsChosen = [];
                boxIndexChosen = [];
                NoOfAnswerChosen = 0;
              }, 1200);



            }
          }
        }
      }


    }

  }
  //test test

};


Tivoli_4_1.prototype.onSoundPressed = function() {
  createjs.Sound.stop();
  this.soundInstance = createjs.Sound.play(this.howl, {
    interrupt: createjs.Sound.INTERRUPT_NONE
  });
  this.soundInstance.addEventListener("failed", handleFailed);
  this.soundInstance.addEventListener("complete", handleComplete);
};

/**
 * load outro scene
 */
Tivoli_4_1.prototype.onComplete = function() {

  var self = this;

  this.introMode = false;

  this.introContainer.visible = false;
  this.outroContainer.visible = true;

  this.help.interactive = false;
  this.help.buttonMode = false;

  this.ovaigen.click = this.ovaigen.tap = function(data) {
    self.cancelOutro();
    self.help.interactive = true;
    self.help.buttonMode = true;

    self.restartExercise();
  }

  this.outroAnimation();

}

/**
 * set to default values , and clear container
 */
Tivoli_4_1.prototype.resetExercise = function() {

  this.ovaigen.visible = false;
  this.outroContainer.visible = false;
  this.introContainer.visible = true;
  this.introMode = true;

};

Tivoli_4_1.prototype.setPool = function() {

  this.pool = [
    ["arm", "båge"],
    ["bad", "anka"],
    ["bad", "ring"],
    ["barn", "stol"],
    ["barn", "vagn"],
    ["blå", "bär"],
    ["bläck", "fisk"],
    ["brev", "låda"],
    ["cykel", "hjälm"],
    ["cykel", "korg"],
    ["dörr", "matta"],
    ["flod", "häst"],
    ["fot", "boll"],
    ["glas", "ögon"],
    ["glass", "bil"],
    ["gräs", "matta"],
    ["hallon", "saft"],
    ["hals", "duk"],
    ["hals", "tablett"],
    ["hand", "duk"],
    ["hand", "väska"],
    ["hatt", "ask"],
    ["hopp", "rep"],
    ["hus", "bil"],
    ["hus", "båt"],
    ["hus", "vagn"],
    ["häst", "sko"],
    ["häst", "svans"],
    ["is", "björn"],
    ["is", "glass"],
    ["jord", "gubbe"],
    ["jord", "nöt"],
    ["jul", "gran"],
    ["jul", "tomte"],
    ["korv", "bröd"],
    ["lakrits", "pipa"],
    ["mask", "ros"],
    ["nagel", "lack"],
    ["napp", "flaska"],
    ["orm", "bunke"],
    ["peppar", "kaka"],
    ["pil", "båge"],
    ["pil", "tavla"],
    ["polis", "bil"],
    ["regn", "båge"],
    ["regn", "rock"],
    ["rygg", "säck"],
    ["saft", "glas"],
    ["sand", "låda"],
    ["segel", "båt"],
    ["sjö", "lejon"],
    ["sjö", "stjärna"],
    ["sko", "horn"],
    ["snö", "boll"],
    ["snö", "gubbe"],
    ["sol", "ros"],
    ["sol", "stol"],
    ["tablett", "ask"],
    ["tak", "lampa"],
    ["tand", "borste"],
    ["tråd", "rulle"],
    ["tvål", "ask"],
    ["ägg", "kopp"]
  ];

  // cloning array
  this.tempPool = this.pool.slice(0);

  shuffle(this.tempPool);

}

/**
 * load exercise json using jsonloader and grab 2 sets
 */
Tivoli_4_1.prototype.initObjectPooling = function() {

  this.wallSlices = [];

  this.setPool();

  this.generateNewSet();

  this.borrowWallSprites();

  this.runExercise();

}

Tivoli_4_1.prototype.generateNewSet = function() {

  var len = this.tempPool.length;

  // console.log('len', len)

  if (len < 7) {
    this.tempPool.length = 0;
    this.tempPool = this.pool.slice(0);
    shuffle(this.tempPool);
    len = this.tempPool.length;
    // console.log('newlen', len)
  };

  var uniqueRangeArr = uniqueRandomNumbersInRange(len);
  // console.log(uniqueRangeArr)

  // Sort numbers in an array in descending order:
  uniqueRangeArr.sort(function(a, b) {
    return b - a
  });

  this.questionSet = [];
  this.answerSet = [];

  for (var i = 0; i < uniqueRangeArr.length; i++) {
    var tempIndex = uniqueRangeArr[i];
    this.questionSet.push(this.tempPool[tempIndex]);
    this.answerSet.push(this.tempPool[tempIndex][0], this.tempPool[tempIndex][1]);
  }

  // console.log(uniqueRangeArr)

  for (var i = 0; i < uniqueRangeArr.length; i++) {
    this.tempPool.splice(uniqueRangeArr[i], 1);
  }

  shuffle(this.answerSet);

  // console.log("answerSet", this.answerSet)
}

Tivoli_4_1.prototype.borrowWallSprites = function() {

  var image = [],
    sound = [],
    word = [];

  for (var i = 0; i < this.answerSet.length; i++) {
    var answerName = getNonUnicodeName(this.answerSet[i]);

    var imageSprite = PIXI.Sprite.fromImage('uploads/images/' + answerName + '.png');
    main.texturesToDestroy.push(imageSprite);

    var soundInstance = loadSound('uploads/audios/' + answerName);

    word.push(this.answerSet[i]);
    image.push(imageSprite);
    sound.push(soundInstance);

  }

  this.wallSlices.push({
    word: word,
    image: image,
    sound: sound
  });

}

/**
 * reset exercise and get next set
 */
Tivoli_4_1.prototype.restartExercise = function() {

  this.resetExercise();

  this.rhymeContainerBase.removeChildren();

  this.wallSlices.length = 0;
  this.questionSet.length = 0;
  this.answerSet.length = 0;


  shuffle(this.tempPool);

  this.generateNewSet();

  this.borrowWallSprites();
  this.runExercise();
};

/**
 * unload/destroy used variables and objects and clear container
 */
Tivoli_4_1.prototype.cleanMemory = function() {

  this.introMode = null;
  if (this.sceneBackground)
    this.sceneBackground.clear();

  this.sceneBackground = null;

  this.person = null;
  this.defaultMouth = null;
  this.defaultArms = null;

  if (this.mouthParams) this.mouthParams.length = 0;

  this.animateMouth = null;
  this.animateArms = null;

  this.introSound = null;
  this.outroSound = null;

  this.help = null;
  this.girl_outro_torso = null;
  this.head_outro_1 = null;
  this.head_outro_2 = null;
  this.mouth_outro = null;

  if (this.mouthOutroParams)
    this.mouthOutroParams.length = 0;

  this.animateMouthOutro = null;

  if (this.pool) this.pool.length = 0;
  if (this.tempPool) this.tempPool.length = 0;
  if (this.answerSet) this.answerSet.length = 0;
  if (this.questionSet) this.questionSet.length = 0;

  this.sceneContainer = null;
  this.introContainer = null;
  this.outroContainer = null;
  //	this.rhymeContainer = null;
  this.rhymeContainerBase = null;
  this.animationContainer = null;
  /*this.answerContainerOne = null;
	this.answerContainerTwo = null;*/
  this.questionContainer = null;
  this.personContainer = null;

  this.baseContainer.removeChildren();
  this.baseContainer = null;
}