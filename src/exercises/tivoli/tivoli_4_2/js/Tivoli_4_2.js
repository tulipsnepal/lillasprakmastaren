function Tivoli_4_2() {
    cl.show();

    PIXI.DisplayObjectContainer.call(this);
    main.CurrentExercise = "samman2";
    this.baseContainer = new PIXI.DisplayObjectContainer();

    this.sceneContainer = new PIXI.DisplayObjectContainer();

    this.sceneContainer.position = {
        x: 77,
        y: 88
    };

    localStorage.setItem('tivoli_4_2', 0);

    this.baseContainer.addChild(this.sceneContainer);

    main.texturesToDestroy = [];

    this.itemsDragged = [-1, -1, -1];
    this.dropArea = [false, false, false];

    this.loadSpriteSheet();

    // everything connected to stage is rendered.
    this.addChild(this.baseContainer);
}

Tivoli_4_2.constructor = Tivoli_4_2;
Tivoli_4_2.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Tivoli_4_2.imagePath = 'build/exercises/tivoli/tivoli_4_2/images/';
Tivoli_4_2.audioPath = 'build/exercises/tivoli/tivoli_4_2/audios/';
Tivoli_4_2.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

/**
 * load sprites, audios, imaegs befor game starts
 */
Tivoli_4_2.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(Tivoli_4_2.audioPath + 'intro');
    this.outroId = loadSound(Tivoli_4_2.audioPath + 'outro');

    // create an array of assets to load
    var assetsToLoad = [
        Tivoli_4_2.imagePath + "sprite_4_2.json",
        Tivoli_4_2.imagePath + "sprite_4_2.png",
        Tivoli_4_2.imagePath + "sprite_fb_4_2.json",
        Tivoli_4_2.imagePath + "sprite_fb_4_2.png",
        Tivoli_4_2.imagePath + "fence.png",
        Tivoli_4_2.imagePath + "circleandline.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * on assets loaded , call needed function
 */
Tivoli_4_2.prototype.spriteSheetLoaded = function() {

    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    this.loadDefaults();

    this.loadExerciseBase();

    this.initObjectPooling();

}

/**
 * draw background, create sprites and movie clip animation
 */
Tivoli_4_2.prototype.loadDefaults = function() {

    var self = this;

    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0xC9D0E8);
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xF4CDE2);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);

    this.fence = PIXI.Sprite.fromImage(Tivoli_4_2.imagePath + "fence.png");
    this.fence.position.set(5, 547);
    main.texturesToDestroy.push(this.fence);
    this.sceneContainer.addChild(this.fence);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.addChild(this.introContainer);

    this.help = PIXI.Sprite.fromFrame("topcorner.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);
    this.sceneContainer.addChild(this.help);

    this.feedbackman = PIXI.Sprite.fromFrame("testyourmanminimal.png");
    this.feedbackman.position.set(1414, 19);
    main.texturesToDestroy.push(this.feedbackman);
    this.introContainer.addChild(this.feedbackman);

    this.feedbackContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.feedbackContainer);

    this.feedBackMeter = PIXI.Sprite.fromImage(Tivoli_4_2.imagePath + "circleandline.png");
    this.feedBackMeter.position.set(1522, 351);
    main.texturesToDestroy.push(this.feedBackMeter);
    this.introContainer.addChild(this.feedBackMeter);

    this.parkgirl = PIXI.Sprite.fromFrame("park_girl.png");
    this.parkgirl.position.set(930, 217);
    this.introContainer.addChild(this.parkgirl);
    main.texturesToDestroy.push(this.parkgirl);
    this.parkgirl.visible = false;

    this.puncharea = PIXI.Sprite.fromFrame("punch_area.png");
    this.puncharea.position.set(1423, 916);
    main.texturesToDestroy.push(this.puncharea);
    this.introContainer.addChild(this.puncharea);

    this.puncharea_down = PIXI.Sprite.fromFrame("punch_area_kopia.png");
    this.puncharea_down.position = {
        x: 1423,
        y: 926
    };

    main.texturesToDestroy.push(this.puncharea_down);
    this.introContainer.addChild(this.puncharea_down);
    this.puncharea_down.visible = false;

    this.hammer = PIXI.Sprite.fromFrame("sara.png");
    this.hammer.position.set(1323, 855);
    main.texturesToDestroy.push(this.hammer);
    this.introContainer.addChild(this.hammer);
    this.hammer.visible = false;

    this.hands = [
        new PIXI.Sprite.fromFrame('arm_1.png'),
        new PIXI.Sprite.fromFrame('arm_2.png'),
        new PIXI.Sprite.fromFrame('arm_3.png'),
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(762, 491);
        this.sceneContainer.addChild(this.hands[i]);
        this.hands[i].visible = false;
    }

    this.blowParams = {
        "image": "sara_blow",
        "length": 4,
        "x": 962,
        "y": 293,
        "speed": 6,
        "frameId": 0
    };
    this.blow = new Animation(this.blowParams);
    this.introContainer.addChild(this.blow);

    this.hatParams = {
        "image": "hat_",
        "length": 2,
        "x": 1517,
        "y": -77,
        "frameId": 0,
    };
    this.hat = new Animation(this.hatParams);
    this.introContainer.addChild(this.hat);

    this.blinkingCheeks = {
        "image": "cheeks_",
        "length": 3,
        "x": 1437,
        "y": 95,
        "speed": 6,
        "frameId": 0,
    };
    this.blinking_cheeks = new Animation(this.blinkingCheeks);
    this.introContainer.addChild(this.blinking_cheeks);
    this.blinking_cheeks.visible = false;

    this.parkgirloutro = PIXI.Sprite.fromFrame("ladymuscle.png");
    this.parkgirloutro.position.set(this.parkgirl.position.x - 172, this.parkgirl.position.y);
    main.texturesToDestroy.push(this.parkgirloutro);
    this.introContainer.addChild(this.parkgirloutro);
    this.parkgirloutro.visible = false;

    this.mouthParams = {
        "image": "mouth_",
        "length": 3,
        "x": 1077 - 16,
        "y": 479,
        "speed": 6,
        "pattern": true,
        "frameId": 0
    };
    this.mouths = new Animation(this.mouthParams);
    this.introContainer.addChild(this.mouths);
    this.mouths.visible = false;

    this.ovaigen = PIXI.Sprite.fromFrame('ovaigen.png');
    this.ovaigen.position.set(250, 465);
    this.introContainer.addChild(this.ovaigen);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.ovaigen.visible = false;

    this.help.click = this.help.tap = this.introAnimation.bind(this);

}


/**
 * start intro animation
 */
Tivoli_4_2.prototype.introAnimation = function() {

    var self = this;

    createjs.Sound.stop();

    if (!this.introMode)
        this.resetExercise();

    this.help.interactive = false;
    this.help.buttonMode = false;

    main.overlay.visible = true;

    self.parkgirl.visible = true;
    self.hammer.visible = true;
    self.puncharea.visible = true;
    self.blow.visible = false;

    self.mouths.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

}

/**
 * handle mouth, hand with specific sound positions or queue points.
0-2 sec arm_to_side => 0
2-4 sec arm_pointing_downwards =>2
4-6 sec arm_pointing_upwards =>1
6-8 sec arm_to_side =>0

 */
Tivoli_4_2.prototype.animateIntro = function() {

    var self = this;


    var currentPostion = this.introInstance.getPosition();
    if (currentPostion >= 1000 && currentPostion < 2000) {
        self.showHandAt(0);
    } else if (currentPostion >= 2000 && currentPostion < 4000) {
        self.showHandAt(2);
    } else if (currentPostion >= 4000 && currentPostion < 6000) {
        self.showHandAt(1);
    } else if (currentPostion >= 6000 && currentPostion < 8000) {
        self.showHandAt(0);
    } else {
        self.showHandAt(0);
    }


};

/**
 * cancel running intro animation
 */
Tivoli_4_2.prototype.cancelIntro = function() {

    var self = this;

    createjs.Sound.stop();

    clearInterval(self.timerId);

    self.mouths.hide();
    self.parkgirl.visible = false;
    self.hammer.visible = false;
    self.mouths.visible = false;
    self.showHandAt();
    self.blow.visible = true;

    self.help.interactive = true;
    self.help.buttonMode = true;

    this.ovaigen.visible = false;
    main.overlay.visible = false;

};

/**
 * play outro animation
 */
Tivoli_4_2.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.mouths.show(true);
        self.hat.show();
        self.hat.gotoAndStop(1);
        self.hat.loop = true;
        self.blinking_cheeks.show();
    }
    this.outroCancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.outroCancelHandler);

};

/**
 * cancel running outro instance
 */
Tivoli_4_2.prototype.cancelOutro = function() {

    createjs.Sound.stop();
    this.mouths.hide(0);
    this.hat.hide(0);
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.ovaigen.visible = true;
    main.overlay.visible = false;

};

/**
 * create sprites, textures for exercise
 */
Tivoli_4_2.prototype.loadExerciseBase = function() {

    this.rhymeContainerBase = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.rhymeContainerBase);

    this.rhymeContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.rhymeContainer);

    this.spritePositionX = [];
    this.spritePositionY1 = [];
    this.spritePositionY2 = [];
    this.dragBase = [];

    var boxv1 = null,
        boxv2 = null,
        xDiff = null,
        xDiffH = null,
        yDiff = null,
        w = 241,
        h = 241;

    for (var i = 0; i < 3; i++) {

        xDiff = 278;
        // yDiff = i * 275;
        yDiff = i * 290;
        xDiffH = i * 264;

        boxv1 = createRect({
            x: 158,
            y: 31 + yDiff,
            color: 0x000000
        });

        boxv2 = PIXI.Sprite.fromFrame('dottedborder.png');
        main.texturesToDestroy.push(boxv2);
        boxv2.position.set(158 + xDiff, 31 + yDiff);

        this.rhymeContainerBase.addChild(boxv1);
        this.rhymeContainerBase.addChild(boxv2);

        // horizontal box
        this.spritePositionX.push(72 + xDiffH, 907);

        // vertical boxes
        this.spritePositionY1.push(158, 31 + yDiff);
        this.spritePositionY2.push(158 + xDiff, 31 + yDiff);

    };
};


/**
 * run exercise logic
 */
Tivoli_4_2.prototype.runExercise = function() {

    var self = this;

    this.dragItem = [];
    this.feedBackBox = [];

    this.ls = [];
    this.rs = {
        "0": '',
        "1": '',
        "2": ''
    }
    this.fs = [];
    this.outputSprites = [];

    this.jsonData = this.wallSlices[0];
    this.jsonAnswer = this.jsonData.answer[0];

    this.dragSprites = [];

    /*this.dropArea1 = false;
    this.dropArea2 = false;
    this.dropArea3 = false;*/

    var soundTexture = PIXI.Texture.fromFrame("sound_grey.png"),
        spriteContainer = null,
        baseSprite = null,
        imageSprite = null,
        soundSprite = null;

    for (var i = 0; i < 3; i++) {
        for (var j = 0; j < 3; j++) {
            spriteContainer = new PIXI.DisplayObjectContainer();
            this.dragContainer = new PIXI.DisplayObjectContainer();


            spriteContainer.width = 241;
            spriteContainer.height = 241;
            spriteContainer.buttonMode = true;
            spriteContainer.interactive = true;

            wordSprite = this.jsonData.word[i][j];
            imageSprite = this.jsonData.image[i][j];
            soundSprite = new PIXI.Sprite(soundTexture);

            imageSprite.width = imageSprite.height = 241;


            if (i === 0) { //vertical word1
                spriteContainer.position.set(this.spritePositionY1[j * 2], this.spritePositionY1[j * 2 + 1]);
                self.ls.push(wordSprite);
            } else if (i === 1) { //horizontal word2
                spriteContainer.position.set(this.spritePositionX[j * 2], this.spritePositionX[j * 2 + 1]);
                baseSprite = createRect();
                spriteContainer.addChild(baseSprite);
            } else if (i === 2) { //vertical word3
                self.fs.push(wordSprite);
                spriteContainer.position.set(this.spritePositionY2[j * 2] + 278, this.spritePositionY2[j * 2 + 1]);
                baseSprite = createRect();
                spriteContainer.addChild(baseSprite);
                spriteContainer.visible = false;
            }

            soundSprite.position.set(0, 183);

            spriteContainer.addChild(imageSprite);
            spriteContainer.addChild(soundSprite);

            if (i === 2) {
                soundSprite.visible = false;
            }


            this.rhymeContainer.addChild(spriteContainer);
            this.rhymeContainer.addChild(this.dragContainer);


            if (i === 2) {
                var text = new PIXI.Text(wordSprite, {
                    font: "30px Arial",
                    fill: "#000000",
                    strokeThickness: 5,
                    stroke: "#ffffff"
                });
                text.position.set((241 - text.width) / 2, 241 - text.height - 10 + 50);
                imageSprite.addChild(text);
                self.outputSprites.push(spriteContainer);
            }

            var self = this;
            soundSprite.interactive = true;
            soundSprite.howl = this.jsonData.sound[i][j];

            if (i === 1) { // drag elements
                spriteContainer.wordSprite = wordSprite;
                spriteContainer.outputData = this.jsonAnswer;
                spriteContainer.dropPosition = this.spritePositionY2;
                spriteContainer.oldPosition = {
                    x: this.spritePositionX[j * 2],
                    y: this.spritePositionX[j * 2 + 1]
                };
                spriteContainer.dragIndex = j;
                spriteContainer.howlPosition = soundSprite.position;
                this.dragSprites.push(spriteContainer);
            }

            // play sprite sound on click/tap event
            soundSprite.click = soundSprite.tap = this.onSoundPressed.bind(soundSprite);

        }; //inner looop
    }; //outer loop

    var feedBackRect = null;

    var feedbackGraphics = createRect({
        w: 241,
        h: 241,
        color: 0xffffff
    });

    var feedbackTexture = feedbackGraphics.generateTexture();

    for (var i = 0; i < 3; i++) {
        feedBackRect = new PIXI.Sprite(feedbackTexture);
        feedBackRect.position = {
            x: this.spritePositionY2[i * 2],
            y: this.spritePositionY2[i * 2 + 1]
        };
        this.rhymeContainer.addChild(feedBackRect);
        feedBackRect.visible = false;
        this.feedBackBox.push(feedBackRect);
        this.onDragging(i);
    };

};

Tivoli_4_2.prototype.onSoundPressed = function() {
    createjs.Sound.stop();
    this.soundInstance = createjs.Sound.play(this.howl, {
        interrupt: createjs.Sound.INTERRUPT_NONE
    });
    this.soundInstance.addEventListener("failed", handleFailed);
    this.soundInstance.addEventListener("complete", handleComplete);
};


/**
 * dragging logic
 * @param  {int} i
 */
Tivoli_4_2.prototype.onDragging = function(i) {
    var self = this;


    // use the mousedown and touchstart
    this.dragSprites[i].mousedown = this.dragSprites[i].touchstart = function(data) {
        if (addedListeners) return false;
        self.dragContainer.addChild(this); //This is used to fix the issue when object goes behind the another object while dragging
        data.originalEvent.preventDefault()
            // store a refference to the data
            // The reason for this is because of multitouch
            // we want to track the movement of this particular touch

        var chkindex = self.itemsDragged.indexOf(this.dragIndex);
        if (chkindex > -1) {
            self.dropArea[chkindex] = false;
            self.itemsDragged[chkindex] = -1;
        }

        this.data = data;
        this.alpha = 0.9;
        this.dragging = true;
        this.sx = this.data.getLocalPosition(this).x;
        this.sy = this.data.getLocalPosition(this).y;

        // no dragging on sound icon area
        if (isInsideRectangle(this.sx, this.sy, this.howlPosition.x, this.howlPosition.y, this.howlPosition.x + 83, this.howlPosition.y + 81)) {
            this.dragging = false;
        }
    }

    this.dragSprites[i].mouseup = this.dragSprites[i].mouseupoutside = this.dragSprites[i].touchend = this.dragSprites[i].touchendoutside = function(data) {
        if (addedListeners) return false;
        this.alpha = 1
        this.dragging = false;
        // set the interaction data to null
        this.data = null;


        var dropArea = [];

        dropArea[0] = isInsideRectangle(this.dx, this.dy, this.dropPosition[0], this.dropPosition[1], this.dropPosition[0] + 241, this.dropPosition[1] + 241);
        dropArea[1] = isInsideRectangle(this.dx, this.dy, this.dropPosition[2], this.dropPosition[3], this.dropPosition[2] + 241, this.dropPosition[3] + 241);
        dropArea[2] = isInsideRectangle(this.dx, this.dy, this.dropPosition[4], this.dropPosition[5], this.dropPosition[4] + 241, this.dropPosition[5] + 241);


        if (dropArea[0] || dropArea[1] || dropArea[2]) {

            if (dropArea[0]) {
                if (self.dropArea[0]) {
                    this.position.x = this.oldPosition.x;
                    this.position.y = this.oldPosition.y;
                    return false;
                }

                this.position = {
                    x: this.dropPosition[0],
                    y: this.dropPosition[1]
                };
                self.dropArea[0] = true;
                self.rs[0] = this.wordSprite;
                self.itemsDragged[0] = this.dragIndex;
            }


            if (dropArea[1]) {
                if (self.dropArea[1]) {
                    this.position.x = this.oldPosition.x;
                    this.position.y = this.oldPosition.y;
                    return false;
                }

                this.position = {
                    x: this.dropPosition[2],
                    y: this.dropPosition[3]
                };

                self.dropArea[1] = true;
                self.itemsDragged[1] = this.dragIndex;
                self.rs[1] = this.wordSprite;
            }


            if (dropArea[2]) {

                if (self.dropArea[2]) {
                    this.position.x = this.oldPosition.x;
                    this.position.y = this.oldPosition.y;
                    return false;
                }

                this.position = {
                    x: this.dropPosition[4],
                    y: this.dropPosition[5]
                };

                self.dropArea[2] = true;
                self.itemsDragged[2] = this.dragIndex;
                self.rs[2] = this.wordSprite;
            }

            console.clear();
            console.log("DROP AREA 0 = " + self.dropArea[0]);
            console.log("DROP AREA 1 = " + self.dropArea[1]);
            console.log("DROP AREA 2 = " + self.dropArea[2]);
            console.log("ITEMS DRAGGED = " + self.itemsDragged);


            this.sx = 0;
            this.sy = 0;
            this.dx = 0;
            this.dy = 0;
            //this.interactive = false;

            // check if all dragged correctly or not
            if (self.dropArea[0] && self.dropArea[1] && self.dropArea[2]) {
                var join = [];
                join[0] = self.ls[0] + self.rs[0];
                join[1] = self.ls[1] + self.rs[1];
                join[2] = self.ls[2] + self.rs[2];

                if (join[0] === self.fs[0] && join[1] === self.fs[1] && join[2] === self.fs[2]) { //all correct
                    correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                    self.showFeedBackBox(self.feedBackBox[0], '0x00ff00', 800);
                    self.showFeedBackBox(self.feedBackBox[1], '0x00ff00', 800);
                    self.showFeedBackBox(self.feedBackBox[2], '0x00ff00', 800);

                    setTimeout(function() {
                        self.correctFeedback();
                    }, 1000);
                } else {
                    var itemsToReset = [];
                    wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);

                    for (var m = 0; m < 3; m++) {
                        self.dragSprites[m].interactive = false;

                        if (join[m] != self.fs[m]) {
                            itemsToReset.push(self.itemsDragged[m]);
                            self.showFeedBackBox(self.feedBackBox[m], '0xff0000', 800);
                            self.dropArea[m] = false;
                        } else {
                            self.outputSprites[m].visible = true;
                            self.showFeedBackBox(self.feedBackBox[m], '0x00ff00', 800);
                        }
                    }

                    var b = setTimeout(function() {
                        for (var n = 0; n < itemsToReset.length; n++) {
                            self.dragSprites[itemsToReset[n]].interactive = true;
                            self.dragSprites[itemsToReset[n]].position.x = self.dragSprites[itemsToReset[n]].oldPosition.x;
                            self.dragSprites[itemsToReset[n]].position.y = self.dragSprites[itemsToReset[n]].oldPosition.y;

                            var idx = self.itemsDragged.indexOf(itemsToReset[n]);
                            if (idx > -1) {
                                self.itemsDragged[idx] = -1;
                            }
                        }
                        self.wrongFeedback();
                    }, 2000);
                }
            }

        } else {
            // reset dragged element position
            var idx = self.itemsDragged.indexOf(this.dragIndex);
            if (idx > -1) {
                self.itemsDragged[idx] = -1;
                self.dropArea[idx] = false;
            }

            // reset dragged element position
            this.position.x = this.oldPosition.x;
            this.position.y = this.oldPosition.y;
        }
    }

    // set the callbacks for when the mouse or a touch moves
    this.dragSprites[i].mousemove = this.dragSprites[i].touchmove = function(data) {
        if (addedListeners) return false;
        // set the callbacks for when the mouse or a touch moves
        if (this.dragging) {
            // need to get parent coords..
            var newPosition = this.data.getLocalPosition(this.parent);
            this.position.x = newPosition.x - this.sx;
            this.position.y = newPosition.y - this.sy;
            this.dx = newPosition.x;
            this.dy = newPosition.y;
        }
    };
}

/**
 * on correct answer
 */
Tivoli_4_2.prototype.correctFeedback = function() {
    var self = this;

    var p = setTimeout(function() {
        self.outputSprites[0].visible = true;
    }, 100);

    var q = setTimeout(function() {
        self.outputSprites[1].visible = true;
    }, 400);

    var r = setTimeout(function() {
        self.outputSprites[2].visible = true;
    }, 800);

    setTimeout(onCorrect, 3000);

    function onCorrect() {
        self.blow.show();


        setTimeout(function() { //show green feedback only after hammering the punch pad
            self.puncharea.visible = false;
            self.puncharea_down.visible = true;
            self.blow.hide();


            if (self.updateFeedback('correct') >= Tivoli_4_2.totalFeedback) {
                setTimeout(self.onComplete(), 1000);
            } else {
                setTimeout(self.getNextSet(), 1000);
            }
        }, 600);
    }

}

/**
 * on wrong answer
 */
Tivoli_4_2.prototype.wrongFeedback = function() {

    this.updateFeedback('wrong');
}

/**
 * load outro scene
 */
Tivoli_4_2.prototype.onComplete = function() {

    var self = this;

    this.introMode = false;

    self.puncharea_down.visible = false;
    self.puncharea.visible = true;

    this.blow.visible = false;
    this.rhymeContainer.visible = false;
    this.rhymeContainerBase.visible = false;

    this.hammer.visible = true;
    this.parkgirloutro.visible = true;
    this.mouths.visible = true;

    this.mouths.position.set(1077, 479);

    this.help.interactive = false;
    this.help.buttonMode = false;

    this.ovaigen.click = this.ovaigen.tap = function() {
        self.cancelOutro();
        self.resetExercise();
    }

    this.outroAnimation();

}

/**
 * reset exercise to default values
 */
Tivoli_4_2.prototype.resetExercise = function() {

    var self = this;

    this.help.interactive = true;
    this.help.buttonMode = true;

    this.ovaigen.visible = false;
    localStorage.setItem('tivoli_4_2', 0);

    self.hammer.visible = false;
    self.parkgirloutro.visible = false;
    self.mouths.position.set(1077 - 16, 479);
    self.mouths.visible = false;

    self.blow.visible = true;
    self.rhymeContainer.visible = true;
    self.rhymeContainerBase.visible = true;

    self.blinking_cheeks.hide();
    self.blinking_cheeks.visible = false;

    self.feedbackContainer.removeChildren();

    self.runNextSet();

    this.introMode = true;
};

/**
 * udpate feedback
 */
Tivoli_4_2.prototype.updateFeedback = function(type) {

    var self = this;

    var len = localStorage.getItem('tivoli_4_2');

    if (type === 'correct') {
        len++;
    } else {
        len--;
    }
    var returnLen = len;

    if (len < 0) {
        localStorage.setItem('tivoli_4_2', 0);
        return false;
    } else if (len >= Tivoli_4_2.totalFeedback) {
        len = Tivoli_4_2.totalFeedback;
        localStorage.setItem('tivoli_4_2', Tivoli_4_2.totalFeedback);
        returnlen = len + 1;
    } else {
        localStorage.setItem('tivoli_4_2', len);
    }

    self.feedbackContainer.removeChildren();

    for (var j = 0; j < len; j++) {
        createFeedback(j);
    }

    function createFeedback(j) {


        if (j == 9) { //for last  rectangle, curve at top
            var feedBackTop = PIXI.Sprite.fromFrame("feedback_10.png");
            feedBackTop.position = {
                x: 1554,
                y: 300
            };
            main.texturesToDestroy.push(feedBackTop);
            self.feedbackContainer.addChild(feedBackTop);
            feedBackTop = null;
        } else if (j == 0) { //for first feedback rectangle curve at bottom
            var feedBackBottom = PIXI.Sprite.fromFrame("feedback_1.png");
            feedBackBottom.position = {
                x: 1554,
                y: 838
            };
            main.texturesToDestroy.push(feedBackBottom);
            self.feedbackContainer.addChild(feedBackBottom);
            feedBackBottom = null;
        } else {
            var feedbackSprite = createRect({
                x: 1554,
                y: 838 - (j * 61),
                w: 63,
                h: 61,
                color: 0x63a634
            });
            self.feedbackContainer.addChild(feedbackSprite);
        }
        //showing pink dot in meter
        if (j > 0) {
            var pinkDot = PIXI.Sprite.fromFrame("feedbacklighton.png");
            pinkDot.position = {
                x: 1620,
                y: 845 - (j - 1) * 61
            };
            main.texturesToDestroy.push(pinkDot);
            self.feedbackContainer.addChild(pinkDot);

        }
    }

    return returnLen;

}


Tivoli_4_2.prototype.setPool = function() {

    this.pool = [
        ["arm", "båge"],
        ["bad", "anka"],
        ["bad", "ring"],
        ["barn", "stol"],
        ["barn", "vagn"],
        ["blå", "bär"],
        ["bläck", "fisk"],
        ["brev", "låda"],
        // ["citron", "fjäril"],
        ["cykel", "hjälm"],
        ["cykel", "korg"],
        ["dörr", "matta"],
        ["flod", "häst"],
        ["fot", "boll"],
        ["glas", "ögon"],
        ["glass", "bil"],
        ["gräs", "matta"],
        ["hallon", "saft"],
        ["hals", "duk"],
        ["hals", "tablett"],
        ["hand", "duk"],
        ["hand", "väska"],
        ["hatt", "ask"],
        ["hopp", "rep"],
        ["hus", "bil"],
        ["hus", "båt"],
        ["hus", "vagn"],
        ["häst", "sko"],
        ["häst", "svans"],
        ["is", "björn"],
        ["is", "glass"],
        ["jord", "gubbe"],
        ["jord", "nöt"],
        ["jul", "gran"],
        ["jul", "tomte"],
        ["korv", "bröd"],
        ["lakrits", "pipa"],
        ["mask", "ros"],
        ["nagel", "lack"],
        ["napp", "flaska"],
        // ["nyckel", "ring"],
        ["orm", "bunke"],
        ["peppar", "kaka"],
        ["pil", "båge"],
        ["pil", "tavla"],
        ["polis", "bil"],
        ["regn", "båge"],
        ["regn", "rock"],
        ["rygg", "säck"],
        ["saft", "glas"],
        ["sand", "låda"],
        ["segel", "båt"],
        ["sjö", "lejon"],
        ["sjö", "stjärna"],
        ["sko", "horn"],
        ["snö", "boll"],
        ["snö", "gubbe"],
        ["sol", "ros"],
        ["sol", "stol"],
        ["tablett", "ask"],
        ["tak", "lampa"],
        ["tand", "borste"],
        ["tråd", "rulle"],
        ["tvål", "ask"],
        ["ägg", "kopp"]
    ];

    // cloning array
    this.tempPool = this.pool.slice(0);

    shuffle(this.tempPool);

}

/**
 * load exercise json using jsonloader and grab 2 sets
 */
Tivoli_4_2.prototype.initObjectPooling = function() {

    this.wallSlices = [];

    this.setPool();

    this.borrowCount = 2;

    this.generateNewSet();

    this.borrowWallSprites();

    this.runExercise();

}

Tivoli_4_2.prototype.generateNewSet = function() {

    this.set = [];

    for (var k = 0; k < this.borrowCount; k++) {

        var len = this.tempPool.length;

        // console.log('len', len)

        if (len < 3) {
            this.tempPool.length = 0;
            this.tempPool = this.pool.slice(0);
            shuffle(this.tempPool);
            len = this.tempPool.length;
            // console.log('newlen', len)
        };

        var uniqueRangeArr = uniqueRandomNumbersInRange(len, 3);

        // Sort numbers in an array in descending order:
        uniqueRangeArr.sort(function(a, b) {
            return b - a
        });

        var tempSet = {
            "answer": {
                "word": []
            },
            "word": []
        };

        for (var i = 0; i < uniqueRangeArr.length; i++) {
            var tempIndex = uniqueRangeArr[i];
            tempSet.answer.word.push(this.tempPool[tempIndex].join(''));
            tempSet.word.push(this.tempPool[tempIndex][0], this.tempPool[tempIndex][1]);
        }

        for (var i = 0; i < uniqueRangeArr.length; i++) {
            this.tempPool.splice(uniqueRangeArr[i], 1);
        }

        this.set.push(tempSet);
    }

    console.log(JSON.stringify(this.set));
}

/**
 * borrowed next 2 sets to cache
 */
Tivoli_4_2.prototype.borrowWallSprites = function() {

    for (var i = 0; i < this.borrowCount; i++) {

        var imageArr1 = [],
            imageArr2 = [],
            imageArr3 = [],
            soundArr1 = [],
            soundArr2 = [],
            soundArr3 = [],
            wordArr1 = [],
            wordArr2 = [],
            wordArr3 = [],
            answerArr = [],
            selfSprite = this.set[i];

        for (var j = 0; j < 6; j++) {

            var answerName = getNonUnicodeName(selfSprite.word[j]);

            var howl = loadSound('uploads/audios/' + answerName);
            //separate array into odd and even array
            if ((j + 2) % 2 == 0) {

                var poolImageSprite1 = PIXI.Sprite.fromImage('uploads/images/' + answerName + '.png');
                imageArr1.push(poolImageSprite1);
                main.texturesToDestroy.push(poolImageSprite1);

                soundArr1.push(howl);
                wordArr1.push(selfSprite.word[j]);

            } else {

                var poolImageSprite2 = PIXI.Sprite.fromImage('uploads/images/' + answerName + '.png');
                imageArr2.push(poolImageSprite2);
                main.texturesToDestroy.push(poolImageSprite2);

                soundArr2.push(howl);
                wordArr2.push(selfSprite.word[j]);
            }
        };

        for (var k = 0; k < 3; k++) {

            var questionName = getNonUnicodeName(selfSprite.answer.word[k]);

            var poolImageSprite3 = PIXI.Sprite.fromImage('uploads/images/' + questionName + '.png');
            imageArr3.push(poolImageSprite3);
            main.texturesToDestroy.push(poolImageSprite3);

            wordArr3.push(selfSprite.answer.word[k]);
            var howl1 = loadSound('uploads/audios/' + questionName);
            soundArr3.push(howl1);
        };
        answerArr.push(selfSprite.answer.compute);

        shuffle(imageArr1, imageArr3, soundArr1, soundArr3, wordArr1, wordArr3);
        shuffle(imageArr2, soundArr2, wordArr2);

        this.wallSlices.push({
            word: [wordArr1, wordArr2, wordArr3],
            image: [imageArr1, imageArr2, imageArr3],
            sound: [soundArr1, soundArr2, soundArr3],
            answer: answerArr,
        });
    }

    console.log(this.wallSlices);
};

/**
 * hide all hands at once and show only specific hand
 * @param  {int} number
 */
Tivoli_4_2.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    }
    if (isNumeric(number))
        this.hands[number].visible = true;
};

Tivoli_4_2.prototype.restartExercise = function() {
    this.getNextSet();
}

/**
 * borrow next set
 */
Tivoli_4_2.prototype.getNextSet = function() {

    this.puncharea_down.visible = false;
    this.puncharea.visible = true;
    this.runNextSet();

};

/**
 * borrow next set
 */
Tivoli_4_2.prototype.runNextSet = function() {

    this.itemsDragged = [-1, -1, -1];

    this.dropArea = [false, false, false];

    this.rhymeContainer.removeChildren();

    this.wallSlices.shift();

    this.set.length = 0;

    this.borrowCount = 1;

    this.generateNewSet();

    this.borrowWallSprites();

    this.runExercise();

};


/*Showing feedback*/
Tivoli_4_2.prototype.showFeedBackBox = function(obj, tintColor, duration) {
    obj.visible = true;
    obj.alpha = 0.6;
    obj.tint = tintColor;

    var c = setTimeout(function() {
        obj.visible = false;
    }, duration || 600);
};

/**
 * memory management by cleaning unused variables, grpahics
 */
Tivoli_4_2.prototype.cleanMemory = function() {

    this.introMode = null;
    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    this.fence = null;
    this.help = null;
    this.feedbackman = null;
    this.feedBackMeter = null;
    this.parkgirl = null;
    this.puncharea = null;
    this.puncharea_down = null;
    this.hammer = null;

    if (this.blowParams)
        this.blowParams.length = 0;

    if (this.hatParams)
        this.hatParams.length = 0;

    if (this.blinkingCheeks)
        this.blinkingCheeks.length = 0;

    if (this.mouthParams)
        this.mouthParams.length = 0;

    if (this.itemsDragged)
        this.itemsDragged.length = 0;

    if (this.dropArea)
        this.dropArea.length = 0;

    if (this.feedBackBox)
        this.feedBackBox.length = 0;

    if (this.dragItem)
        this.dragItem.length = 0;

    if (this.ls)
        this.ls.length = 0;

    if (this.rs)
        this.rs.length = 0;

    if (this.fs)
        this.fs.length = 0;

    if (this.pool) this.pool.length = 0;
    if (this.tempPool) this.tempPool.length = 0;
    if (this.set) this.set.length = 0;

    this.borrowCount = null;

    this.blow = null;
    this.hat = null;
    this.blinking_cheeks = null;
    this.parkgirloutro = null;
    this.mouths = null;

    this.ovaigen = null;

    this.introSound = null;
    this.outroSound = null;

    this.rhymeContainer = null;
    this.rhymeContainerBase = null;
    this.feedbackContainer = null;
    this.introContainer = null;
    this.sceneContainer = null;

    this.baseContainer.removeChildren();

    this.baseContainer = null;

}