/**
 * tivoli_1_2 outro
 */
function Tivoli_3_2() {
  cl.show();

  PIXI.DisplayObjectContainer.call(this);
  main.CurrentExercise = "longshort2";
  this.baseContainer = new PIXI.DisplayObjectContainer();

  this.sceneContainer = new PIXI.DisplayObjectContainer();

  this.feedbackContainer = new PIXI.DisplayObjectContainer();
  this.rhymeContainer = new PIXI.DisplayObjectContainer();
  this.animationContainer = new PIXI.DisplayObjectContainer();
  this.sceneContainer.position = {
    x: 77,
    y: 88
  };

  localStorage.setItem('tivoli_3_2', 0);

  this.baseContainer.addChild(this.sceneContainer);

  main.texturesToDestroy = [];

  this.loadSpriteSheet();

  // everything connected to stage is rendered.
  this.addChild(this.baseContainer);
}

Tivoli_3_2.constructor = Tivoli_3_2;
Tivoli_3_2.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Tivoli_3_2.imagePath = 'build/exercises/tivoli/tivoli_3_2/images/';
Tivoli_3_2.audioPath = 'build/exercises/tivoli/tivoli_3_2/audios/';
Tivoli_3_2.commonFolder = 'build/common/images/tivoli/';
Tivoli_3_2.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

Tivoli_3_2.prototype.loadSpriteSheet = function() {

  this.introId = loadSound(Tivoli_3_2.audioPath + 'intro');
  this.outroId = loadSound(Tivoli_3_2.audioPath + 'outro');

  // create an array of assets to load
  var assetsToLoad = [
    Tivoli_3_2.imagePath + "sprite_3_2.json",
    Tivoli_3_2.imagePath + "sprite_3_2.json",
    Tivoli_3_2.imagePath + "sprite_3_2.png",
    Tivoli_3_2.imagePath + "sprite_3_2.png"
  ];
  // create a new loader
  loader = new PIXI.AssetLoader(assetsToLoad);
  // use callback
  loader.onComplete = this.spriteSheetLoaded.bind(this);
  //begin load
  loader.load();

}

/**
 * load sprites, audios at the begining
 */
Tivoli_3_2.prototype.spriteSheetLoaded = function() {
  cl.hide();

  if (this.sceneContainer.stage)
    this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

  this.loadDefaults();

  this.initObjectPooling();

  this.sceneContainer.addChild(this.animationContainer);
  this.sceneContainer.addChild(this.rhymeContainer);

}

/**
 * create sprites, draw backgrond and create movice clip for hand mouth
 * @return {[type]} [description]
 */
Tivoli_3_2.prototype.loadDefaults = function() {

  var self = this;

  this.sceneBackground = new PIXI.Graphics();
  this.sceneBackground.beginFill(0xC9D0E8);
  // set the line style to have a width of 5 and set the color to red
  this.sceneBackground.lineStyle(3, 0xF4CDE2);
  // draw a rectangle
  this.sceneBackground.drawRect(0, 0, 1843, 1202);
  this.sceneContainer.addChild(this.sceneBackground);

  this.person = PIXI.Sprite.fromFrame("carousel-girl.png");
  this.person.position = {
    x: 143,
    y: 175
  }
  main.texturesToDestroy.push(this.person);
  this.sceneContainer.addChild(this.person);

  this.defaultArms = PIXI.Sprite.fromFrame("arms_default.png");
  this.defaultArms.position = {
    x: 39,
    y: 488
  }
  main.texturesToDestroy.push(this.defaultArms);
  this.animationContainer.addChild(this.defaultArms);

  this.mouthParams = {
    "image": "mouth_",
    "length": 3,
    "x": 290,
    "y": 417,
    "speed": 6,
    "pattern": true,
    "frameId": 3
  };
  this.animateMouth = new Animation(this.mouthParams);

  this.animationContainer.addChild(this.animateMouth);

  this.hands = [
    new PIXI.Sprite.fromFrame('arms_1.png'),
    new PIXI.Sprite.fromFrame('arms_2.png'),
    new PIXI.Sprite.fromFrame('arms_longest.png'),
    new PIXI.Sprite.fromFrame('arms_shortest.png')
  ];
  for (var i = 0; i < this.hands.length; i++) {
    this.hands[i].position.set(38, 487);
    this.animationContainer.addChild(this.hands[i]);
    this.hands[i].visible = false;
  }

  this.help = PIXI.Sprite.fromFrame("topcorner.png");
  this.help.interactive = true;
  this.help.buttonMode = true;
  this.help.position = {
    x: -77,
    y: -88
  }
  this.animationContainer.addChild(this.help);

  this.ovaigen = PIXI.Sprite.fromFrame("ovaigen.png");
  this.ovaigen.buttonMode = true;
  this.ovaigen.interactive = true;
  this.ovaigen.position = {
    x: 752,
    y: 572
  };
  this.animationContainer.addChild(this.ovaigen);
  this.ovaigen.visible = false;

  this.help.click = this.help.tap = this.introAnimation.bind(this);

  this.sceneContainer.addChild(this.feedbackContainer);
  this.generateFeedback();

  this.soundTexture = PIXI.Texture.fromFrame("sound_grey.png");

}

/**
 * show target hand
 */
Tivoli_3_2.prototype.showHandAt = function(number) {
  for (var i = 0; i < this.hands.length; i++) {
    this.hands[i].visible = false;
  }
  if (isNumeric(number))
    this.hands[number].visible = true;
};

/**
 * start intro animation
 */
Tivoli_3_2.prototype.introAnimation = function() {

  var self = this;

  createjs.Sound.stop();

  if (!this.introMode)
    this.resetExercise();

  this.help.interactive = false;
  this.help.buttonMode = false;

  main.overlay.visible = true;

  self.defaultArms.visible = false;
  self.rhymeContainer.children[0].visible = false;
  self.animateMouth.show();

  this.introInstance = createjs.Sound.play(this.introId, {
    interrupt: createjs.Sound.INTERRUPT_ANY
  });

  if (createjs.Sound.isReady())
    this.timerId = setInterval(this.animateIntro.bind(this), 1);

  this.introCancelHandler = this.cancelIntro.bind(this);
  this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
 0-3 sec arms_2 (nb, no word on the sign)
3-7 sec arms_shortest
7-8 sec arms_2
8-10 sec arms_longest
stop at arms_2

 */
Tivoli_3_2.prototype.animateIntro = function() {

  var self = this;

  var currentPostion = this.introInstance.getPosition();
  console.log(currentPostion);

  if (currentPostion >= 0 && currentPostion < 3200) {
    self.showHandAt(1); //no letter
  } else if (currentPostion >= 3200 && currentPostion < 7200) {
    self.showHandAt(3); //shortest
  } else if (currentPostion >= 7200 && currentPostion < 8000) {
    self.showHandAt(1); //no letter
  } else if (currentPostion >= 8000 && currentPostion < 11168) {
    self.showHandAt(2); //longest
  }

};

/**
 * cancel running intro animation
 */
Tivoli_3_2.prototype.cancelIntro = function() {


  var self = this;
  createjs.Sound.stop();
  clearInterval(self.timerId);
  self.animateMouth.hide(3);

  self.showHandAt(2); //no letter

  setTimeout(function() {
    self.showHandAt(1); //no letter
  }, 1000);

  setTimeout(function() {
    self.showHandAt();
    self.defaultArms.visible = true;
    self.rhymeContainer.children[0].visible = true;

    self.help.interactive = true;
    self.help.buttonMode = true;

    self.ovaigen.visible = false;
    main.overlay.visible = false;
  }, 2500);


};

/**
 * play outro animation
 */
Tivoli_3_2.prototype.outroAnimation = function() {

  var self = this;

  var outroInstance = createjs.Sound.play(this.outroId);

  if (createjs.Sound.isReady()) {
    main.overlay.visible = true;
    self.animateMouth.show(true);
  }

  this.outroCancelHandler = this.cancelOutro.bind(this);
  outroInstance.addEventListener("complete", this.outroCancelHandler);

};

/**
 * cancel running outro animation
 */
Tivoli_3_2.prototype.cancelOutro = function() {

  var self = this;

  createjs.Sound.stop();

  self.animateMouth.hide(3);

  self.ovaigen.visible = true;
  self.help.interactive = true;
  self.help.buttonMode = true;

  main.overlay.visible = false;

};

/**
 * exercise logic
 */
Tivoli_3_2.prototype.runExercise = function() {

  var self = this;

  var jsonData = this.wallSlices[0];
  console.log(jsonData.answer);

  var quesList = ['kortast', 'längst'];

  var quesIdx = randomIntFromInterval(0, 1);

  var isQuestionLong = (quesIdx > 0) ? "1" : "0";

  var question = new PIXI.Text(quesList[quesIdx], {
    font: "87px Arial"
  });
  question.position = {
    x: 311,
    y: 732 - 6
  };
  main.texturesToDestroy.push(question);
  this.rhymeContainer.addChild(question);

  var imageButtons = [],
    soundButtons = [],
    baseButtons = [],
    rhymePosition = [835, 1260].randomize();

  for (var i = 0; i < 2; i++) {
    var imageSprite = jsonData.image[i],
      soundSprite = new PIXI.Sprite(this.soundTexture),
      spriteBase = createRect();

    imageSprite.interactive = true;
    imageSprite.buttonMode = true;
    imageSprite.buttonIndex = i;
    imageSprite.width = imageSprite.height = 241;
    imageSprite.answer = jsonData.answer[i];

    spriteBase.position = imageSprite.position = {
      x: rhymePosition[i],
      y: 466
    }

    soundSprite.position = {
      x: rhymePosition[i],
      y: 466 + 241 - 62
    }

    soundSprite.interactive = true;
    soundSprite.buttonMode = true;

    var wordText = new PIXI.Text(jsonData.word[i], {
      font: "60px Arial",
      align: "center",
    });
    main.texturesToDestroy.push(wordText);
    wordText.position = {
      x: rhymePosition[i],
      y: 466 + 241 + 20
    }

    baseButtons.push(spriteBase);
    imageButtons.push(imageSprite);
    soundButtons.push(soundSprite);

    self.rhymeContainer.addChild(spriteBase);
    self.rhymeContainer.addChild(imageSprite);
    self.rhymeContainer.addChild(soundSprite);
    self.rhymeContainer.addChild(wordText);

    soundSprite.howl = jsonData.sound[i];
    soundSprite.click = soundSprite.tap = this.onSoundPressed.bind(soundSprite);
  };

  imageButtons[0].click = imageButtons[0].tap = imageButtons[1].click =
    imageButtons[1].tap = function(data) {

      if (addedListeners) return false;

      addedListeners = true;
      var hitObj = this;

      if (isQuestionLong === hitObj.answer) { //correct
        greenBox.position = this.position;
        self.rhymeContainer.addChild(greenBox);

        correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);

        setTimeout(function() {
          self.rhymeContainer.removeChild(greenBox)
          addedListeners = false;
          self.correctFeedback();

        }, 200);
      } else { //wrong
        redBox.position = this.position;
        self.rhymeContainer.addChild(redBox);

        wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);

        setTimeout(function() {
          self.rhymeContainer.removeChild(redBox)
          addedListeners = false;
          self.wrongFeedback();
        }, 200);
      }

    }

};

Tivoli_3_2.prototype.onSoundPressed = function() {
  createjs.Sound.stop();
  this.soundInstance = createjs.Sound.play(this.howl, {
    interrupt: createjs.Sound.INTERRUPT_NONE
  });
  this.soundInstance.addEventListener("failed", handleFailed);
  this.soundInstance.addEventListener("complete", handleComplete);
};

/**
 * on correct answer , increment feedback by 1
 * get next set to play
 */
Tivoli_3_2.prototype.correctFeedback = function() {

  var feedback = localStorage.getItem('tivoli_3_2');

  this.hiddenFeedback[feedback].alpha = 1;
  feedback++;
  localStorage.setItem('tivoli_3_2', feedback);

  if (feedback >= Tivoli_3_2.totalFeedback) {
    this.onComplete(); // go to outro scene
  } else {
    this.rhymeContainer.removeChildren();

    this.returnWallSprites();
    this.borrowWallSprites(1);
    this.runExercise();
  }
}

/**
 * on wrong answer, decrement feedback by 1
 */
Tivoli_3_2.prototype.wrongFeedback = function() {

  var feedback = localStorage.getItem('tivoli_3_2');
  feedback--;
  if (feedback >= 0) {
    this.hiddenFeedback[feedback].alpha = 0.2;
    localStorage.setItem('tivoli_3_2', feedback);
  };

}

/**
 * on complete, load outro scene
 */
Tivoli_3_2.prototype.onComplete = function() {

  var self = this;

  this.introMode = false;

  this.rhymeContainer.visible = false;
  this.defaultArms.visible = false;

  this.help.interactive = false;
  this.help.buttonMode = false;
  this.showHandAt(1);

  this.ovaigen.click = this.ovaigen.tap = function(data) {

    self.cancelOutro();
    self.restartExercise();
  }

  this.outroAnimation();
}

/**
 * reset values to default and generate feedback
 * @return {[type]} [description]
 */
Tivoli_3_2.prototype.resetExercise = function() {

  var self = this;

  this.ovaigen.visible = false;

  this.generateFeedback();

  self.rhymeContainer.visible = true;
  self.defaultArms.visible = true;
  self.showHandAt();

  this.introMode = true;

};

/**
 * generate feedback sprites with visibility 20%
 */
Tivoli_3_2.prototype.generateFeedback = function() {

  localStorage.setItem('tivoli_3_2', 0);

  this.feedbackContainer.removeChildren();

  this.hiddenFeedback = [];

  for (var i = 0; i < 10; i++) {
    var feedbackSprite = new PIXI.Sprite.fromFrame("feedback.png");
    feedbackSprite.position = {
      x: 1666,
      y: 981 - (i * 100)
    };
    feedbackSprite.alpha = 0.2;
    this.feedbackContainer.addChild(feedbackSprite);
    this.hiddenFeedback.push(feedbackSprite);
  };
}

/**
 * perform object pooling to get 2 sets to preload for gameplay
 */
Tivoli_3_2.prototype.initObjectPooling = function() {

  var self = this;

  this.wallSlices = [];

  this.poolSlices = [];

  self.pool = {};

  var loader = new PIXI.JsonLoader(
    'uploads/exercises/tivoli/tivoli_3_2.json?noache=' + (new Date()).getTime()
  );
  loader.on('loaded', function(evt) {
    //data is in loader.json
    self.pool = new ExerciseSpritesPool(loader.json.sets);
    //borrow two sprites once
    self.borrowWallSprites(2);
    self.runExercise();

  });
  loader.load();
}

/**
 * borrow next set by caching audios ands sprites
 */
Tivoli_3_2.prototype.borrowWallSprites = function(num) {

  var self = this;
  this.num = num;

  for (var i = 0; i < this.num; i++) {

    var sprite = this.pool.borrowSprite();
    this.poolSlices.push(sprite);

    var imageArr = [];
    var soundArr = [];
    var answerArr = [];
    var wordArr = [];
    var spriteObj = sprite[0];
    for (var j = 0; j < spriteObj.image.length; j++) {

      var poolImageSprite = PIXI.Sprite.fromImage('uploads/images/' +
        spriteObj.image[j]);
      imageArr.push(poolImageSprite);
      main.texturesToDestroy.push(poolImageSprite);

      var howl = loadSound('uploads/audios/' + StripExtFromFile(spriteObj.sound[
        j]));

      soundArr.push(howl);

      answerArr.push(spriteObj.answer[j]);
      wordArr.push(spriteObj.word[j]);
    };

    shuffle(imageArr, soundArr, answerArr, wordArr);

    this.wallSlices.push({
      word: wordArr,
      image: imageArr,
      sound: soundArr,
      answer: answerArr,
    });
  }
};

/**
 * return used pool object to pool
 */
Tivoli_3_2.prototype.returnWallSprites = function() {

  var sprite = this.poolSlices[0];

  this.pool.returnSprite(sprite);

  this.rhymeContainer.removeChildren()

  this.poolSlices = [];
  this.wallSlices.shift();
};

/**
 * reset exercise and get next set
 */
Tivoli_3_2.prototype.restartExercise = function() {

  this.resetExercise();

  this.rhymeContainer.removeChildren();

  this.returnWallSprites();
  this.borrowWallSprites(1);
  this.runExercise();

};

/**
 * unload destroy variabls and objects on switch exercise
 */
Tivoli_3_2.prototype.cleanMemory = function() {

  if (this.sceneBackground)
    this.sceneBackground.clear();
  this.sceneBackground = null;

  this.person = null;

  if (this.mouthParams)
    this.mouthParams.length = 0;

  this.animateMouth = null;

  this.introSound = null;
  this.outroSound = null;

  this.help = null;
  this.ovaigen = null;

  if (this.poolSlices)
    this.poolSlices.length = 0;

  if (this.wallSlices)
    this.wallSlices.length = 0;

  this.pool = null;

  this.sceneContainer = null;
  this.feedbackContainer = null;
  this.rhymeContainer = null;
  this.animationContainer = null;

  this.baseContainer.removeChildren();
  this.baseContainer = null;
}