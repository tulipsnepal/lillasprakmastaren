function Tivoli_1_2() {
    cl.show();

    PIXI.DisplayObjectContainer.call(this);
    main.CurrentExercise = "rimord2";
    this.baseContainer = new PIXI.DisplayObjectContainer();

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();

    this.feedbackContainer = new PIXI.DisplayObjectContainer();
    this.rhymeContainer = new PIXI.DisplayObjectContainer();
    this.animationContainer = new PIXI.DisplayObjectContainer();

    this.sceneContainer.position = {
        x: 77,
        y: 88
    };

    localStorage.setItem('tivoli_1_2', 0);

    this.baseContainer.addChild(this.sceneContainer);

    main.texturesToDestroy = [];

    this.loadSpriteSheet();
    // everything connected to stage is rendered.
    this.addChild(this.baseContainer);
}

Tivoli_1_2.constructor = Tivoli_1_2;
Tivoli_1_2.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Tivoli_1_2.commonFolder = 'build/common/images/tivoli/';
Tivoli_1_2.imagePath = 'build/exercises/tivoli/tivoli_1_2/images/';
Tivoli_1_2.audioPath = 'build/exercises/tivoli/tivoli_1_2/audios/';
Tivoli_1_2.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

/**
 * load audios,images, and sprites with pixi asset loader
 * @return {[type]}
 */
Tivoli_1_2.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(Tivoli_1_2.audioPath + 'intro');
    this.outroId = loadSound(Tivoli_1_2.audioPath + 'outro');

    // create an array of assets to load
    var assetsToLoad = [
        Tivoli_1_2.imagePath + "defaultbg.png",
        Tivoli_1_2.imagePath + "sprite_1_2.json",
        Tivoli_1_2.imagePath + "sprite_1_2.png",
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * on all assets loaded, call necessary functions
 */
Tivoli_1_2.prototype.spriteSheetLoaded = function() {

    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    this.loadDefaults();

    this.soundTexture = PIXI.Texture.fromFrame("sound_grey.png");

    this.initObjectPooling();

    this.sceneContainer.addChild(this.rhymeContainer);
    this.sceneContainer.addChild(this.animationContainer);

}

/**
 * exercise logic
 */
Tivoli_1_2.prototype.runExercise = function() {

    var self = this;

    var jsonData = this.wallSlices[0];
    console.log(jsonData.answer);

    //create stuffs
    var rhymePosition = [
        108, 255,
        418, 255,
        108, 582,
        414, 582,
    ];

    var imageButtons = [];
    var soundButtons = [];
    var correctButtons = [];
    var wrongButtons = [];
    var whiteButtons = [];

    var soundHeight = 242 - 62;

    for (var i = 0; i < 4; i++) {
        var imageSprite = jsonData.image[i],
            soundSprite = new PIXI.Sprite(this.soundTexture),
            whiteSprite = createRect();

        whiteSprite.position = imageSprite.position = {
            x: rhymePosition[i * 2],
            y: rhymePosition[i * 2 + 1]
        }

        soundSprite.position = {
            x: rhymePosition[i * 2],
            y: rhymePosition[i * 2 + 1] + soundHeight
        }

        self.rhymeContainer.addChild(whiteSprite);
        self.rhymeContainer.addChild(imageSprite);
        self.rhymeContainer.addChild(soundSprite);

        imageSprite.interactive = true;
        imageSprite.buttonMode = true;
        imageSprite.buttonIndex = i;
        imageSprite.width = imageSprite.height = 241;
        imageSprite.answer = jsonData.answer[i];

        whiteButtons.push(whiteSprite);
        imageButtons.push(imageSprite);
        soundButtons.push(soundSprite);

        soundSprite.howl = jsonData.sound[i];

        soundSprite.interactive = true;
        soundSprite.buttonMode = true;

        soundSprite.click = soundSprite.tap = this.onSoundPressed.bind(soundSprite);

    };

    imageButtons[0].click = imageButtons[0].tap = imageButtons[1].click = imageButtons[1].tap = imageButtons[2].click = imageButtons[2].tap = imageButtons[3].click = imageButtons[3].tap = function(data) {

        var hitObj = this;

        if (addedListeners) return false;
        addedListeners = true;

        hitObj.interactive = false;

        var index = this.buttonIndex;

        self.rhymeContainer.removeChild(imageButtons[index]);
        self.rhymeContainer.removeChild(soundButtons[index]);

        if (this.answer === 1) {
            greenBox.position = this.position;
            self.rhymeContainer.addChild(imageButtons[index]);
            self.rhymeContainer.addChild(soundButtons[index]);
            self.rhymeContainer.addChild(greenBox);

            self.mouth.visible = false;
            self.head.visible = false;
            self.headCorrect.visible = true;
            self.showHandAt(1);

            correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
            setTimeout(function() {
                hitObj.interactive = true;
                self.rhymeContainer.removeChild(greenBox);
                soundButtons.length = 0;
                self.showHandAt(0);
                self.correctFeedback()
                addedListeners = false;
            }, 1000);

        } else {

            redBox.position = this.position;
            self.rhymeContainer.addChild(imageButtons[index]);
            self.rhymeContainer.addChild(soundButtons[index]);
            self.rhymeContainer.addChild(redBox);

            wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
            setTimeout(function() {
                hitObj.interactive = true;
                self.rhymeContainer.removeChild(redBox);
                self.wrongFeedback();
                addedListeners = false;
            }, 100);

        }
    }

};

Tivoli_1_2.prototype.onSoundPressed = function() {
    createjs.Sound.stop();
    this.soundInstance = createjs.Sound.play(this.howl, {
        interrupt: createjs.Sound.INTERRUPT_NONE
    });
    this.soundInstance.addEventListener("failed", handleFailed);
    this.soundInstance.addEventListener("complete", handleComplete);
};

/**
 * on correct answer, increment feedback by 1
 * then get next exercise set to play
 */
Tivoli_1_2.prototype.correctFeedback = function() {

    var self = this;

    self.headCorrect.visible = false;
    self.head.visible = true;
    self.mouth.visible = true;

    var feedback = localStorage.getItem('tivoli_1_2');
    feedback++;

    if (feedback >= Tivoli_1_2.totalFeedback) {
        self.onComplete(); // go to outro scene
    } else {
        localStorage.setItem('tivoli_1_2', feedback);
        self.updateFeedback();
        soundButtons = [];

        this.rhymeContainer.removeChildren();

        this.wallSlices.shift();

        this.set.length = 0;

        this.borrowCount = 1;

        this.borrowWallSprites();

    }

};

/**
 * on wrong answer, decrement feeback by 1
 */
Tivoli_1_2.prototype.wrongFeedback = function() {

    var feedback = localStorage.getItem('tivoli_1_2');
    feedback--;

    if (feedback < 0)
        feedback = 0;

    localStorage.setItem('tivoli_1_2', feedback);

    this.updateFeedback();
};

/**
 * on all 10 feedback, load outro scene and do outro animation
 * @return {[type]}
 */
Tivoli_1_2.prototype.onComplete = function() {

    var self = this;

    this.introMode = false;

    this.rhymeContainer.visible = false;
    this.feedbackContainer.visible = false;

    this.showHandAt();

    this.sideArm.visible = true;
    this.animateBulb.visible = true;
    this.animateBulb.show(true);

    this.help.interactive = false;
    this.help.buttonMode = false;

    this.back_again.click = this.back_again.tap = function(data) {
        self.cancelOutro();
        this.visible = false;
        self.resetExercise();
        self.restartExercise();
    }

    this.outroAnimation();

}

/**
 * reset exercise with feedback clear
 * @return {[type]}
 */
Tivoli_1_2.prototype.resetExercise = function() {

    this.resetFeedback();

    this.back_again.visible = false;

    this.sideArm.visible = false;
    this.showHandAt(0);
    this.help.visible = true;

    this.rhymeContainer.visible = true;
    this.feedbackContainer.visible = true;

    this.animateBulb.visible = false;

};

/**
 * generate feedback
 */
Tivoli_1_2.prototype.updateFeedback = function() {

    var self = this;

    var feedbackPositions = [
        1093, 193,
        1273, 265,
        1392, 415,
        1387, 594,
        1275, 743,
        1094, 809,
        919, 751,
        790, 602,
        797, 416,
        911, 265,
    ];

    //create a texture from an image path
    var feedbackTexture = PIXI.Texture.fromFrame('bulb.png');

    var counter = localStorage.getItem('tivoli_1_2');

    self.feedbackContainer.removeChildren();

    for (var j = 0; j < counter; j++) {
        createFeedback(j);
    }

    function createFeedback(j) {
        var feedbackSprite = new PIXI.Sprite(feedbackTexture);
        feedbackSprite.position = {
            x: feedbackPositions[j * 2],
            y: feedbackPositions[j * 2 + 1]
        };
        main.texturesToDestroy.push(feedbackSprite);
        self.feedbackContainer.addChild(feedbackSprite);
    }

}

Tivoli_1_2.prototype.setPool = function() {

    this.pool = [
        ["and", "tand", "hand", "strand", "sand", "rand"],
        ["blad", "stad", "bad"],
        ["däck", "häck", "säck"],
        ["ko", "sko", "bro", "klo"],
        ["lok", "bok", "krok"],
        ["ram", "gam", "dam"],
        ["såg", "tåg", "våg"],
        ["grus", "hus", "ljus", "lus", "mus"],
        ["is", "gris", "polis", "ris", "spis"],
        ["får", "hår", "lår", "spår", "sår", "tår"],
        ["kjol", "sol", "stol"],
        ["hatt", "katt", "natt", "ratt", "skatt", "tratt"],
        ["lax", "tax", "sax"],
        ["bil", "fil", "mil", "pil", "sil"],
        ["ben", "gren", "ren", "sten"],
        ["skägg", "vägg", "ägg"],
        ["gås", "lås", "mås", "sås"],
        ["hål", "mål", "nål", "skål", "tvål", "ål"],
        ["mos", "nos", "ros"],
        ["mal", "skal", "val"],
        ["kran", "gran", "svan"],
        ["glass", "tass", "vass"]
    ];

    this.poolData = ["and", "tand", "hand", "strand", "sand", "rand", "blad", "stad", "bad", "däck", "häck", "säck", "ko", "sko", "bro", "klo", "lok", "bok", "krok", "ram", "gam", "dam", "såg", "tåg", "våg", "grus", "hus", "ljus", "lus", "mus", "is", "gris", "polis", "ris", "spis", "får", "hår", "lår", "spår", "sår", "tår", "kjol", "sol", "stol", "hatt", "katt", "natt", "ratt", "skatt", "tratt", "lax", "tax", "sax", "bil", "fil", "mil", "pil", "sil", "ben", "gren", "ren", "sten", "skägg", "vägg", "ägg", "gås", "lås", "mås", "sås", "hål", "mål", "nål", "skål", "tvål", "ål", "mos", "nos", "ros", "mal", "skal", "val", "kran", "gran", "svan", "glass", "tass", "vass"];

    // cloning array
    this.tempPool = this.pool.slice(0);

    shuffle(this.tempPool);

}

/**
 * load exercise json using jsonloader and grab 2 sets
 */
Tivoli_1_2.prototype.initObjectPooling = function() {

    this.wallSlices = [];

    this.setPool();

    this.borrowCount = 2;

    this.borrowWallSprites();

}

Tivoli_1_2.prototype.generateNewSet = function() {

    this.set = [];

    for (var k = 0; k < this.borrowCount; k++) {

        var len = this.tempPool.length;

        console.log('len', len)

        if (len < 1) {
            this.tempPool.length = 0;
            this.tempPool = this.pool.slice(0);
            shuffle(this.tempPool);
            len = this.tempPool.length;
            console.log('newlen', len)
        };

        var randomIndexArr = uniqueRandomNumbersInRange(len, 1),
            tempIndex = randomIndexArr[0],
            newSet = this.tempPool[tempIndex];

        this.tempPool.splice(tempIndex, 1);

        var uniqueRangeArr = uniqueRandomNumbersInRange(newSet.length, 3);

        var tempSet = {
            "word": [],
            "answer": []
        };

        for (var i = 0; i < uniqueRangeArr.length; i++) {
            tempSet.word.push(newSet[uniqueRangeArr[i]]);
            tempSet.answer.push(0);
        }


        var tempPoolData = [];
        for (var i = 0; i < this.poolData.length; i++) {
            if (newSet.indexOf(this.poolData[i]) === -1) {
                tempPoolData.push(this.poolData[i]);
            }
        }

        shuffle(tempPoolData);

        var getNonUniqueIndex = uniqueRandomNumbersInRange(tempPoolData.length, 1);

        tempSet.word.push(tempPoolData[getNonUniqueIndex]);
        tempSet.answer.push(1);

        this.set.push(tempSet);
    }

    console.log(JSON.stringify(this.set));
}

/**
 * cache borrowed sprites and preload sounds from the sets to use in the game
 */
Tivoli_1_2.prototype.borrowWallSprites = function() {

    this.generateNewSet();

    var self = this;
    for (var i = 0; i < this.borrowCount; i++) {

        var imageArr = [],
            soundArr = [],
            answerArr = [];

        var spriteObj = this.set[i];

        for (var j = 0; j < 4; j++) {

            var answerName = getNonUnicodeName(spriteObj.word[j]);

            var poolImageSprite = PIXI.Sprite.fromImage('uploads/images/' + answerName + '.png');

            imageArr.push(poolImageSprite);
            main.texturesToDestroy.push(poolImageSprite);

            var howl = loadSound('uploads/audios/' + answerName);
            soundArr.push(howl);
            answerArr.push(spriteObj.answer[j]);
        };

        shuffle(imageArr, soundArr, answerArr);

        this.wallSlices.push({
            image: imageArr,
            sound: soundArr,
            answer: answerArr
        });
    }

    this.runExercise();

};

/**
 * return old sets to pool to reuse later
 * clear exercise container for new sets to display
 */
Tivoli_1_2.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];

    this.pool.returnSprite(sprite);

    this.rhymeContainer.removeChildren();

    this.poolSlices = [];
    this.wallSlices.shift();
};

/**
 * add all intro/outro sprites to scene container
 */
Tivoli_1_2.prototype.loadDefaults = function() {

    var self = this;

    /*var background = new PIXI.Graphics();
    background.beginFill(0xC9D0E8);
    // set the line style to have a width of 5 and set the color to red
    background.lineStyle(3, 0xF4CDE2);
    // draw a rectangle
    background.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(background);

    background = null;*/
    this.bkg1 = new Background(Tivoli_1_2.imagePath + "defaultbg.png");
    this.sceneContainer.addChild(this.bkg1);

    this.head = PIXI.Sprite.fromFrame("head_1.png");
    main.texturesToDestroy.push(this.head);
    this.headCorrect = PIXI.Sprite.fromFrame("head_correct.png");
    main.texturesToDestroy.push(this.headCorrect);

    this.head.position = {
        x: 1489,
        y: 179
    }
    this.headCorrect.position = {
        x: 1381,
        y: 202
    }

    this.sceneContainer.addChild(this.head);
    this.sceneContainer.addChild(this.headCorrect);
    this.headCorrect.visible = false;

    this.help = PIXI.Sprite.fromFrame("topcorner.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position = {
        x: -77,
        y: -88
    }
    this.sceneContainer.addChild(this.help);

    this.mouthParams = {
        "image": "mouth_",
        "length": 3,
        "x": 1510,
        "y": 403,
        "speed": 6,
        "pattern": true,
        "frameId": 3
    };
    this.mouth = new Animation(this.mouthParams);
    this.sceneContainer.addChild(this.mouth);

    this.bulbParams = {
        "image": "circlebulb",
        "length": 2,
        "x": 787,
        "y": 194,
    };
    this.animateBulb = new Animation(this.bulbParams);
    this.sceneContainer.addChild(this.animateBulb);
    this.animateBulb.visible = false;

    this.sideArm = PIXI.Sprite.fromFrame("arm_side.png");
    this.sideArm.position = {
        x: 1383,
        y: 566
    }
    main.texturesToDestroy.push(this.sideArm);
    this.sceneContainer.addChild(this.sideArm);
    this.sideArm.visible = false;

    this.back_again = PIXI.Sprite.fromFrame("ovaigen.png");
    this.back_again.buttonMode = true;
    this.back_again.interactive = true;
    this.back_again.position = {
        x: 364 - 77,
        y: 440 - 88
    };
    this.sceneContainer.addChild(this.back_again);
    this.back_again.visible = false;

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.feedbackContainer);
    this.updateFeedback();

    this.hands = [
        new PIXI.Sprite.fromFrame('arm_1.png'),
        new PIXI.Sprite.fromFrame('arm_2.png'),
        new PIXI.Sprite.fromFrame('arm_3.png'),
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(1034, 433);
        this.sceneContainer.addChild(this.hands[i]);
        this.hands[i].visible = false;
    }

    this.showHandAt(0);

}

/**
 * show specific hand at call
 */
Tivoli_1_2.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    }
    if (isNumeric(number))
        this.hands[number].visible = true;
};

/**
 * start intro animation
 */
Tivoli_1_2.prototype.introAnimation = function() {

    createjs.Sound.stop();

    if (!this.introMode)
        this.resetExercise();

    this.outroContainer.visible = false;
    this.introContainer.visible = true;

    this.help.interactive = false;
    this.help.buttonMode = false;

    main.overlay.visible = true;

    this.mouth.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
0-1 sec arm_holding_box => 0
1-3 sec arm_bulb => 1
3-6 sec arm_pointing_at_words => 2
6-8 sec arm_holding_box =>  0

 */
Tivoli_1_2.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    if (currentPostion >= 1000 && currentPostion < 3000) {
        this.showHandAt(1);
    } else if (currentPostion >= 3000 && currentPostion < 6000) {
        this.showHandAt(2);
    } else {
        this.showHandAt(0);
    }

};

/**
 * cancel running intro animation
 */
Tivoli_1_2.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);
    this.mouth.hide(3);
    this.showHandAt(0);
    this.help.interactive = true;
    this.help.buttonMode = true;

    main.overlay.visible = false;
};

/**
 * start outro animation
 */
Tivoli_1_2.prototype.outroAnimation = function() {

    this.outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        this.mouth.show();
    }

    this.outroCancelHandler = this.cancelOutro.bind(this);
    this.outroInstance.addEventListener("complete", this.outroCancelHandler);

};

/**
 * cancel running outro animation
 */
Tivoli_1_2.prototype.cancelOutro = function() {

    createjs.Sound.stop();

    this.mouth.hide(3);
    this.back_again.visible = true;
    this.help.interactive = true;
    this.help.buttonMode = true;

    main.overlay.visible = false;

};

/**
 * reset feedback and set to intro mode
 */
Tivoli_1_2.prototype.resetFeedback = function() {

    localStorage.setItem('tivoli_1_2', 0);

    this.feedbackContainer.removeChildren();

    this.introMode = true;
}

/**
 * return used sets and get new sets for game play
 */
Tivoli_1_2.prototype.restartExercise = function() {

    var self = this;

    this.resetFeedback();

    this.rhymeContainer.removeChildren();

    this.wallSlices.shift();

    this.set.length = 0;

    this.borrowCount = 1;

    this.borrowWallSprites();
};

/**
 * clean unused variables
 */
Tivoli_1_2.prototype.cleanMemory = function() {

    this.bkg1 = null;
    this.head = null;
    this.headCorrect = null;
    this.help = null;

    if (this.mouthParams)
        this.mouthParams.length = 0;

    this.mouth = null;
    this.hand = null;
    this.introSound = null;

    if (this.bulbParams)
        this.bulbParams.length = 0;

    this.animateBulb = null;
    this.sideArm = null;
    this.back_again = null;

    this.num = null;

    if (this.poolSlices)
        this.poolSlices.length = 0;

    if (this.wallSlices)
        this.wallSlices.length = 0;

    this.pool = null;

    this.sceneContainer = null;
    this.introContainer = null;
    this.outroContainer = null;
    this.feedbackContainer = null;
    this.rhymeContainer = null;
    this.animationContainer = null;

    this.baseContainer.removeChildren();

    this.baseContainer = null;

}