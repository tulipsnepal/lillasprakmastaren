function Tivoli_2_1() {

    cl.show();

    PIXI.DisplayObjectContainer.call(this);
    main.CurrentExercise = "enochett1";
    //ceate empty container to store scene
    this.enochettContainer = new PIXI.DisplayObjectContainer();

    this.dragContainer = new PIXI.DisplayObjectContainer();
    //create empty container to load exercise without menus

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.lightContainer = new PIXI.DisplayObjectContainer();
    this.frontContainer = new PIXI.DisplayObjectContainer();
    //this.menuContainer = new PIXI.DisplayObjectContainer();

    this.dimContainer = new PIXI.DisplayObjectContainer();
    this.dimContainer.position = {
        x: 77,
        y: 88
    };

    this.dimContainer.addChild(this.sceneContainer);
    this.enochettContainer.addChild(this.dimContainer);

    localStorage.removeItem('lights');
    localStorage.removeItem('dim-alpha');

    main.texturesToDestroy = [];

    this.loadSpriteSheet();

    // everything connected to stage is rendered.
    this.addChild(this.enochettContainer);

};

Tivoli_2_1.constructor = Tivoli_2_1;
Tivoli_2_1.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Tivoli_2_1.imagePath = 'build/exercises/tivoli/tivoli_2_1/images/';
Tivoli_2_1.audioPath = 'build/exercises/tivoli/tivoli_2_1/audios/';
Tivoli_2_1.commonFolder = 'build/common/images/tivoli/';
Tivoli_2_1.totalLights = 10; // default 10
Tivoli_2_1.defaultAlpha = 0; // default 0

/**
 * load sounds, sprites
 */
Tivoli_2_1.prototype.loadSpriteSheet = function() {

    this.ghostSoundId = loadSound(Tivoli_2_1.audioPath + 'intro_ghost');
    this.draculaSoundId = loadSound(Tivoli_2_1.audioPath + 'intro_vampire');
    this.outroId = loadSound(Tivoli_2_1.audioPath + 'outro');

    // create an array of assets to load
    var assetsToLoad = [
        Tivoli_2_1.imagePath + "center_tivoli2.1intro.png",
        Tivoli_2_1.imagePath + "sprite_2_1.json",
        Tivoli_2_1.imagePath + "sprite_2_1.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

};


/**
 * on assets loaded , load exercise sprites and pool exercises sets
 */
Tivoli_2_1.prototype.spriteSheetLoaded = function() {

    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    this.initObjectPooling();

    this.enochettContainer.addChild(this.dragContainer);

    this.loadDefaults();

};

/**
 * exercise logic
 */
Tivoli_2_1.prototype.runExercise = function() {

        var self = this;

        var jsonData = this.wallSlices[0];
        console.log(jsonData.answer);

        //create stuffs
        self.whiteImage = createRect();

        self.dragSound = PIXI.Sprite.fromFrame("sound_grey.png");
        self.dragImage = jsonData.image[0];

        //set position
        self.whiteImage.position = {
            x: 914,
            y: 697
        };

        self.whiteImage.width = self.whiteImage.height = 241;

        self.dragImage.position = {
            x: 914,
            y: 697
        };
        self.dragSound.position = {
            x: 915,
            y: 877
        };


        self.dragContainer.removeChildren();


        //add the sprites
        self.dragContainer.addChild(self.whiteImage);
        self.dragContainer.addChild(self.dragImage);
        self.dragContainer.addChild(self.dragSound);

        // enable the sprite to be interactive.. this will allow it to respond to mouse and touch events
        self.dragContainer.interactive = true;
        // this button mode will mean the hand cursor appears when you rollover the sprite with your mouse
        //add sprite to container
        self.dragContainer.buttonMode = true;

        self.dragSound.interactive = true;
        self.dragSound.buttonMode = true;
        self.dragSound.howl = jsonData.sound[0];

        // use the mousedown and touchstart
        self.dragContainer.mousedown = self.dragContainer.touchstart = function(data) {
            if (addedListeners) return false;

            data.originalEvent.preventDefault()

            // store a refference to the data
            // The reason for this is because of multitouch
            // we want to track the movement of this particular touch
            this.data = data;
            this.alpha = 0.9;
            this.dragging = true;
            this.sx = this.data.getLocalPosition(self.dragContainer).x;
            this.sy = this.data.getLocalPosition(self.dragContainer).y;

            if (isInsideRectangle(this.sx, this.sy, 915, 877, 915 + 83, 877 + 81)) {
                this.dragging = false;
                this.alpha = 1;

                createjs.Sound.play(self.dragSound.howl);
            }
        };

        // set the events for when the mouse is released or a touch is released
        self.dragContainer.mouseup = self.dragContainer.mouseupoutside = self.dragContainer.touchend = self.dragContainer.touchendoutside = function(data) {

            if (addedListeners) return false;

            this.alpha = 1
            this.dragging = false;
            // set the interaction data to null
            this.data = null;

            this.position.x = 0;
            this.position.y = 0;

            var dragObj = this;

            var hatCoord = [567, 917, 808, 1161];

            var saxCoord = [1311, 1006, 1553, 1249];

            self.dragContainer.removeChildren();

            var isDropped = false; // to check dropped in target rectangular area
            var isDroppedHat = false; // to check dropped area is hat
            var isDroppedSax = false; // to check dropped area is sax

            // check source image points exists in target rectangle area
            if (isInsideRectangle(this.dx, this.dy, hatCoord[0], hatCoord[1], hatCoord[2], hatCoord[3])) {

                self.whiteImage.position = self.dragImage.position = redBox.position = greenBox.position = {
                    x: hatCoord[0],
                    y: hatCoord[1]
                };

                self.dragSound.position = {
                    x: hatCoord[0],
                    y: hatCoord[1] + 185
                };

                isDropped = true;
                isDroppedHat = true;
            }

            // check source image points exists in target rectangle area
            if (isInsideRectangle(this.dx, this.dy, saxCoord[0], saxCoord[1], saxCoord[2], saxCoord[3])) {
                self.whiteImage.position = self.dragImage.position = redBox.position = greenBox.position = {
                    x: saxCoord[0],
                    y: saxCoord[1]
                };

                self.dragSound.position = {
                    x: saxCoord[0],
                    y: saxCoord[1] + 185
                };

                isDropped = true;
                isDroppedSax = true;
            }

            // move the sprite to its designated hat or sax position
            if (isDropped) {
                self.dragContainer.interactive = false;
                /*if (self.dragSound.howl instanceof Howl) {
                    self.dragSound.howl.unload();
                };*/


                if (isDroppedHat) {

                    if (jsonData.answer === 'en')
                        onCorrect();
                    else
                        onWrong();
                }

                if (isDroppedSax) {

                    if (jsonData.answer === 'ett')
                        onCorrect();
                    else
                        onWrong();
                }
                this.position.x = 0;
                this.position.y = 0;
                this.sx = 0;
                this.sy = 0;
                this.dx = 0;
                this.dy = 0;
            } else {
                this.position.x = 0;
                this.position.y = 0;
                self.whiteImage.position = self.dragImage.position = {
                    x: 914,
                    y: 697
                };
                self.dragSound.position = {
                    x: 915,
                    y: 877
                };
                self.dragContainer.addChild(self.whiteImage);
                self.dragContainer.addChild(self.dragImage);
                self.dragContainer.addChild(self.dragSound);
            }

            function onCorrect() {

                self.dragContainer.addChild(self.whiteImage);
                self.dragContainer.addChild(self.dragImage);
                self.dragContainer.addChild(self.dragSound);
                self.dragContainer.addChild(greenBox);

                self.correctFeedback();
            }

            function onWrong() {

                self.dragContainer.addChild(self.whiteImage);
                self.dragContainer.addChild(self.dragImage);
                self.dragContainer.addChild(self.dragSound);
                self.dragContainer.addChild(redBox);

                self.wrongFeedback();
            }
        };

        // set the callbacks for when the mouse or a touch moves
        self.dragContainer.mousemove = self.dragContainer.touchmove = function(data) {
            if (this.dragging) {
                this.bringToFront();
                // need to get parent coords..
                var newPosition = this.data.getLocalPosition(this.parent);
                this.position.x = newPosition.x - this.sx;
                this.position.y = newPosition.y - this.sy;
                this.dx = newPosition.x;
                this.dy = newPosition.y;
            }
        }
    }
    /**
     * on correct answer, dim background
     */
Tivoli_2_1.prototype.correctFeedback = function() {

    var self = this;

    var dimAlpha = localStorage.getItem('dim-alpha');
    dimAlpha = parseFloat(dimAlpha) + 0.1;

    if (dimAlpha > 1) dimAlpha = 1;

    localStorage.setItem('dim-alpha', dimAlpha);
    self.draculaEyes.alpha = self.ghostEyes.alpha = self.dimBkg.alpha = dimAlpha;

    var counter = localStorage.getItem('lights');
    counter--;
    if (counter < 1) counter = 0;
    localStorage.setItem('lights', counter);
    self.updateLights();

    correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);

    setTimeout(function() {

        self.dragContainer.removeChildren();

        if (counter > 0) {
            // get next set on correct
            self.returnWallSprites();
            self.borrowWallSprites(1);
            self.runExercise();
        } else {
            self.onComplete();
        }

    }, 150);
};

/**
 * on complete all 10 sets, change scene to outro and play outro animation sound
 * @return {[type]} [description]
 */
Tivoli_2_1.prototype.onComplete = function() {

    var self = this;

    this.introMode = false;

    self.hat.visible = false;
    self.sax.visible = false;

    self.ovaigen.click = self.ovaigen.tap = function(data) {
        self.cancelOutro();
        self.resetExercise();
        self.restartExercise();
    }

    setTimeout(self.outroAnimation.bind(self), 1000);
};

/**
 * clear exercise to default values
 */
Tivoli_2_1.prototype.resetExercise = function() {

    var self = this;

    this.ovaigen.visible = false;

    localStorage.setItem('lights', Tivoli_2_1.totalLights);
    localStorage.setItem('dim-alpha', Tivoli_2_1.defaultAlpha);
    self.draculaEyes.alpha = self.ghostEyes.alpha = self.dimBkg.alpha = Tivoli_2_1.defaultAlpha;
    self.help.visible = true;
    self.hat.visible = true;
    self.sax.visible = true;

    this.introMode = true;
};

/**
 * clear to default values and update feedback and reset the exercise with new sets
 */
Tivoli_2_1.prototype.restartExercise = function() {
    var self = this;

    localStorage.setItem('lights', Tivoli_2_1.totalLights);
    localStorage.setItem('dim-alpha', Tivoli_2_1.defaultAlpha);
    self.draculaEyes.alpha = self.ghostEyes.alpha = self.dimBkg.alpha = Tivoli_2_1.defaultAlpha;

    this.updateLights();

    this.returnWallSprites();
    this.borrowWallSprites(1);

    this.runExercise();
};

/**
 * on wrong answer, increase the lights
 */
Tivoli_2_1.prototype.wrongFeedback = function() {
    var self = this;
    var dimAlpha = localStorage.getItem('dim-alpha');
    dimAlpha = parseFloat(dimAlpha) - 0.1;

    if (dimAlpha < 0) dimAlpha = 0;

    localStorage.setItem('dim-alpha', dimAlpha);
    self.draculaEyes.alpha = self.ghostEyes.alpha = self.dimBkg.alpha = dimAlpha;

    var counter = localStorage.getItem('lights');
    if (counter < Tivoli_2_1.totalLights) {
        counter++;
        localStorage.setItem('lights', counter);
        self.updateLights();
    }

    wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
    setTimeout(function() {
        self.dragContainer.position.x = 0;
        self.dragContainer.position.y = 0;
        self.dragContainer.dx = 0;
        self.dragContainer.dy = 0;

        self.dragContainer.removeChild(redBox);
        self.whiteImage.position = self.dragImage.position = {
            x: 914,
            y: 697
        };
        self.dragSound.position = {
            x: 915,
            y: 877
        };
        self.dragContainer.interactive = true;

    }, 150);


}


/**
 * load all exercise sprites for intro and outro scene
 */
Tivoli_2_1.prototype.loadDefaults = function() {

    var self = this;

    this.bkg1 = new Background(Tivoli_2_1.imagePath + "center_tivoli2.1intro.png");
    this.sceneContainer.addChild(this.bkg1);

    this.dimBkg = PIXI.Sprite.fromFrame("dimmer.png");
    this.dimBkg.scale = {
        x: 2,
        y: 2
    }
    this.dimBkg.alpha = Tivoli_2_1.defaultAlpha;
    // main.texturesToDestroy.push(this.dimBkg);
    this.dimContainer.addChild(this.dimBkg);


    this.draculaEyes = new Animation({
        "image": "dracula-eyes-",
        "length": 2,
        "x": 230 + 77,
        "y": 316 + 88,
        "speed": 3,
        "pattern": true
    });
    this.draculaEyes.alpha = Tivoli_2_1.defaultAlpha;
    this.frontContainer.addChild(this.draculaEyes);


    this.ghostEyes = new Animation({
        "image": "ghost-eyes-",
        "length": 2,
        "x": 1500 + 77,
        "y": 360 + 88,
        "speed": 2,
        "pattern": true
    });
    this.ghostEyes.alpha = Tivoli_2_1.defaultAlpha;
    this.frontContainer.addChild(this.ghostEyes);

    self.createHandle();

    this.hat = PIXI.Sprite.fromFrame("dotted_en.png");
    this.sax = PIXI.Sprite.fromFrame("dotted_ett.png");

    main.texturesToDestroy.push(this.hat);
    main.texturesToDestroy.push(this.sax);

    this.hat.position = {
        x: 564,
        y: 917
    };
    this.sax.position = {
        x: 1308,
        y: 1006
    };

    this.frontContainer.addChild(this.hat);
    this.frontContainer.addChild(this.sax);

    this.help = PIXI.Sprite.fromFrame("topcorner.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.frontContainer.addChild(this.help);

    this.draculaParams = {
        "image": "dracula-mouth-",
        "length": 3,
        "x": 308 - 77,
        "y": 539 - 88,
        "speed": 6,
        "pattern": true,
        "frameId": 3
    };
    this.animateDraculaMouth = new Animation(this.draculaParams);
    this.sceneContainer.addChild(this.animateDraculaMouth);

    this.ghostParams = {
        "image": "ghost-mouth-",
        "length": 3,
        "x": 1558 - 77,
        "y": 547 - 88,
        "speed": 6,
        "pattern": true,
        "frameId": 1
    };

    this.animateGhostMouth = new Animation(this.ghostParams);
    this.sceneContainer.addChild(this.animateGhostMouth);

    this.ovaigen = PIXI.Sprite.fromFrame("ovaigen.png");

    this.ovaigen.buttonMode = true;
    this.ovaigen.interactive = true;
    this.ovaigen.position = {
        x: 857 - 77,
        y: 683 - 88
    };

    this.frontContainer.addChild(this.ovaigen);
    this.ovaigen.visible = false;

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    localStorage.setItem('dim-alpha', Tivoli_2_1.defaultAlpha);

    localStorage.setItem('lights', Tivoli_2_1.totalLights);
    self.updateLights();
    self.enochettContainer.addChild(self.frontContainer);
    self.enochettContainer.addChild(self.lightContainer);
}

/**
 * play intro animation with sounds loaded
 */
Tivoli_2_1.prototype.introAnimation = function() {

    var self = this;

    this.introCancelHandler = this.cancelIntro.bind(this);

    createjs.Sound.stop();

    main.overlay.visible = true;

    if (!this.introMode) {
        this.resetExercise();
        this.restartExercise();
    }


    this.ghostSoundInstance = createjs.Sound.play(this.ghostSoundId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady()) {
        self.help.interactive = false;
        self.help.buttonMode = false;
        self.animateGhostMouth.show();
    }

    this.ghostSoundInstance.addEventListener("complete", function() {

        self.animateGhostMouth.hide(3);

        self.draculaSoundInstance = createjs.Sound.play(self.draculaSoundId, {
            interrupt: createjs.Sound.INTERRUPT_ANY
        });

        if (createjs.Sound.isReady()) {
            self.animateDraculaMouth.show();
        }

        self.draculaSoundInstance.addEventListener("complete", self.introCancelHandler);
    });

};

/**
 * cancel running intro animation
 */
Tivoli_2_1.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    this.animateDraculaMouth.hide(3);
    this.animateGhostMouth.hide(1);

    this.help.interactive = true;
    this.help.buttonMode = true;
    // debugger;
    this.ovaigen.visible = false;

    main.overlay.visible = false;
};

/**
 * play outro animation with sounds loaded
 */
Tivoli_2_1.prototype.outroAnimation = function() {

    this.outroInstance = createjs.Sound.play(this.outroId);

    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        this.help.interactive = false;
        this.help.buttonMode = false;
        this.ghostEyes.show();
        this.draculaEyes.show();
    }
    this.outroCancelHandler = this.cancelOutro.bind(this);
    this.outroInstance.addEventListener("complete", this.outroCancelHandler);

};

/**
 * cancel running outro animation instance
 */
Tivoli_2_1.prototype.cancelOutro = function() {

    createjs.Sound.stop();

    this.help.interactive = true;
    this.help.buttonMode = true;

    this.ovaigen.visible = true;

    main.overlay.visible = false;

};

/**
 * create handle to show feedgback sprites
 */
Tivoli_2_1.prototype.createHandle = function() {

    var self = this;
    // show light handles
    var handleTexture = PIXI.Texture.fromFrame('torchedout.png');
    for (var i = 0; i < Tivoli_2_1.totalLights; i++) {
        createHandle(561, 225, i);
    }

    function createHandle(width, height, step) {
        var adder = step * 107;
        var handle = new PIXI.Sprite(handleTexture);
        handle.position = {
            x: width + adder,
            y: height
        };
        main.texturesToDestroy.push(handle);
        self.frontContainer.addChild(handle);
    }
};

/**
 * update feedback as light sprite to increase/decrease on correct or wrong answer
 */
Tivoli_2_1.prototype.updateLights = function() {

    var self = this;

    //create a texture from an image path
    var lightTexture = PIXI.Texture.fromFrame('torchonly.png');

    var counter = localStorage.getItem('lights');

    self.lightContainer.removeChildren();

    for (var j = 0; j < counter; j++) {
        createLights(513, 101, j);
    }

    function createLights(width, height, step) {
        var adder = step * 107;
        var light = new PIXI.Sprite(lightTexture);
        light.position = {
            x: width + adder,
            y: height
        };
        main.texturesToDestroy.push(light);
        self.lightContainer.addChild(light);
    }

}

/**
 * load exercise json sets and get upcoming 2 sets to start the game.
 * @return {[type]} [description]
 */
Tivoli_2_1.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    this.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/tivoli/tivoli_2_1.json?nocache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        self.pool = new ExerciseSpritesPool(loader.json);
        //borrow two sprites once
        self.generatePoolCategory();
        self.borrowWallSprites(2);
        self.runExercise();

    });
    loader.load();
}

Tivoli_2_1.prototype.generatePoolCategory = function() {

    var keyArr = [];

    var spriteArr = this.pool.sprites,
        spriteLen = spriteArr.length;

    for (var i = 0; i < spriteLen; i++) {
        keyArr.push(spriteArr[i][0].word[0]);
    };

    this.poolCategory = keyArr.filter(onlyUnique);
}

/**
 * pre-load or cache the pooled sprites and sounds.
 */
Tivoli_2_1.prototype.borrowWallSprites = function(num) {

    var rand, category, sprite, spriteObj;

    var self = this;

    this.num = num;

    for (var i = 0; i < this.num; i++) {
        rand = makeUniqueRandom(this.poolCategory.length);
        this.borrowCategory = this.poolCategory[rand];

        sprite = this.pool.borrowUniqueSprite(this.borrowCategory, this.lastBorrowSprite);
        this.poolSlices.push(sprite);
        var imageArr = [];
        var soundArr = [];
        spriteObj = sprite[0];
        for (var j = 0; j < spriteObj.image.length; j++) {
            this.lastBorrowSprite = spriteObj.image[j];

            var poolImageSprite = PIXI.Sprite.fromImage('uploads/images/' + spriteObj.image[j]);
            imageArr.push(poolImageSprite);
            main.texturesToDestroy.push(poolImageSprite);

            var howl = loadSound('uploads/audios/' + StripExtFromFile(spriteObj.sound[j]));
            soundArr.push(howl);
        };

        this.wallSlices.push({
            image: imageArr,
            sound: soundArr,
            answer: spriteObj.answer
        });
    }
};

/**
 * return used sets to pool
 */
Tivoli_2_1.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];

    this.pool.returnSprite(sprite);

    this.poolSlices = [];
    this.wallSlices.shift();
};

/**
 * unload and destroy unused varialbes and clear container.
 */
Tivoli_2_1.prototype.cleanMemory = function() {

    this.bkg1 = null;
    this.dimBkg = null;
    this.draculaEyes = null;
    this.ghostEyes = null;
    this.hat = null
    this.sax = null;
    this.help = null;

    if (this.draculaParams)
        this.draculaParams.length = 0;

    if (this.ghostParams)
        this.ghostParams.length = 0;

    this.animateDraculaMouth = null;
    this.animateGhostMouth = null;

    this.ovaigen = null;

    this.num = null;
    this.pool = null;

    if (this.poolSlices)
        this.poolSlices.length = 0;

    if (this.wallSlices)
        this.wallSlices.length = 0;

    this.dragContainer = null;
    this.sceneContainer = null;
    this.lightContainer = null;
    this.frontContainer = null;
    // this.menuContainer = null;
    this.dimContainer = null;

    //this.enochettContainer.removeChildren();
    this.enochettContainer = null;

    this.ghostSoundId = null;
    this.draculaSoundId = null;
    this.outroId = null;

    this.introMode = null;



    this.introCancelHandler = null;
    this.ghostSoundInstance = null;
    this.draculaSoundInstance = null;

    this.outroInstance = null;
    this.outroCancelHandler = null;

}