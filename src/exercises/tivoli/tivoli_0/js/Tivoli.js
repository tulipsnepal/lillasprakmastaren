function Tivoli() {

    cl.show();

    PIXI.DisplayObjectContainer.call(this);

    this.tivoliContainer = new PIXI.DisplayObjectContainer();

    this.sceneContainer = new PIXI.DisplayObjectContainer();

    this.imageContainer = new PIXI.DisplayObjectContainer();

    this.menuContainer = new PIXI.DisplayObjectContainer();

    this.tivoliContainer.addChild(this.sceneContainer);
    this.tivoliContainer.addChild(this.menuContainer);

    //resize container
    this.imageContainer.position = {
        x: 0,
        y: -20
    }

    this.menuContainer.position = {
        x: 0,
        y: 1287 - 40
    }

    main.texturesToDestroy = [];
    main.soundToDestroy = [];

    this.loadSpriteSheet();

    this.addChild(this.tivoliContainer);
}

Tivoli.constructor = Tivoli;
Tivoli.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

Tivoli.imagePath = "build/exercises/tivoli/tivoli_0/images/";
Tivoli.COMMONFOLDER = 'build/common/images/tivoli/';

Tivoli.prototype.loadSpriteSheet = function() {

    // create an array of assets to load
    this.assetsToLoad = [
        Tivoli.imagePath + "tivoli_innercircus-40pxcut.png",
        Tivoli.COMMONFOLDER + 'menuskeleton.png',
        Tivoli.COMMONFOLDER + 'sprite_tivoli.json',
        Tivoli.COMMONFOLDER + 'sprite_tivoli.png',
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);

    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);

    //begin load
    loader.load();

}

Tivoli.prototype.spriteSheetLoaded = function() {

    cl.hide();

    // get backgroudn for tivoli page
    this.bkg1 = new PIXI.Sprite.fromImage(Tivoli.imagePath + "tivoli_innercircus-40pxcut.png");
    this.sceneContainer.addChild(this.bkg1);

    this.loadMenus();

    this.imageButtons();

}

Tivoli.prototype.sceneChange = function(scene) {

    var prevExercise = localStorage.getItem('exercise');

    if (scene === prevExercise)
        return false;


    UnloadScene.apply(this);

    main.CurrentExercise = scene;

    localStorage.setItem('exercise', scene);

    main.page = new window[scene];
    main.page.introMode = true;

    loadSound('build/common/audios/' + main.CurrentExercise + "_help");

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0x000000);

    this.sceneContainer.addChild(main.page);

    this.cleanMemory();

};

Tivoli.prototype.loadMenus = function() {

    // position of 1 and 2 sub menu
    var x1 = 13 + 20,
        x2 = 132 + 24,
        y = 14 - 20;

    this.menuParams = {
        path: Tivoli.COMMONFOLDER,
        background: 'menuskeleton.png',
        images: [
            'tillbaka.png',
            'rimord.png',
            'en_och_ett.png',
            'langa_och.png',
            'samman.png',
            'den.png',
        ],
        positions: [
            10, 133,
            217, 151 - 20,
            502, 117 + 18,
            819, 126,
            1155, 144 - 30,
            1477 - 35, 67 - 10,
        ],
        data: {
            1: {
                hover_image: 'menu1hover.png',
                hover_position: [194, -23],
                submenu_image: [
                    'sub_menu_1.png',
                    'sub_menu_2.png',
                ],
                submenu_position: [
                    194 + x1, y,
                    194 + x2, y
                ]

            },
            2: {
                hover_image: 'menu2hover.png',
                hover_position: [493, -23],
                submenu_image: [
                    'sub_menu_1.png',
                    'sub_menu_2.png',
                ],
                submenu_position: [
                    493 + x1, y,
                    493 + x2, y
                ]
            },
            3: {
                hover_image: 'menu3hover.png',
                hover_position: [785, -23],
                submenu_image: [
                    'sub_menu_1.png',
                    'sub_menu_2.png',
                ],
                submenu_position: [
                    785 + x1 + 20, y,
                    785 + x2 + 40, y
                ]
            },
            4: {
                hover_image: 'menu4hover.png',
                hover_position: [1153, -23 - 8],
                submenu_image: [
                    'sub_menu_1.png',
                    'sub_menu_2.png',
                ],
                submenu_position: [
                    1153 + x1, y,
                    1153 + x2, y
                ]
            },
            5: {
                hover_image: 'menu5hover.png',
                hover_position: [1465, -23],
                submenu_image: [
                    'sub_menu_1.png',
                    'sub_menu_2.png',
                ],
                submenu_position: [
                    1465 + x1, y,
                    1465 + x2 + 20, y
                ]
            },
            pink_image: 'pinkbar.png',
            move_position: {
                x: 10,
                y: 32
            }
        }

    };
    this.menu = new Menu(this.menuParams);

    this.menuContainer.addChild(this.menu);

    //tillbaka button
    this.menu.getChildAt(1).click = this.menu.getChildAt(1).tap = this.tivoliOverview.bind(this);

    //rimord button
    this.menu.getChildAt(2).click = this.menu.getChildAt(2).tap = this.navMenu1.bind(this);

    //enochett button
    this.menu.getChildAt(3).click = this.menu.getChildAt(3).tap = this.navMenu2.bind(this);

    //carousel menu button
    this.menu.getChildAt(4).click = this.menu.getChildAt(4).tap = this.navMenu3.bind(this);

    //samman-satta ord
    this.menu.getChildAt(5).click = this.menu.getChildAt(5).tap = this.navMenu4.bind(this);

    // den det hon han
    this.menu.getChildAt(6).click = this.menu.getChildAt(6).tap = this.navMenu5.bind(this);
}

Tivoli.prototype.imageButtons = function() {

    this.navPositions = [
        300, 307,
        344 - 55, 634 + 10,
        209, 1118,
        1025, 794,
        1716, 415,
    ];

    this.navButtons = [];

    for (var i = 0; i < this.navPositions.length; i++) {
        var imageSprite = PIXI.Sprite.fromFrame('ova.png');

        imageSprite.buttonMode = true;
        imageSprite.position.x = this.navPositions[i * 2];
        imageSprite.position.y = this.navPositions[i * 2 + 1];
        imageSprite.interactive = true;

        this.imageContainer.addChild(imageSprite);

        this.navButtons.push(imageSprite);
        imageSprite = null;
    }

    //carousel image button
    this.navButtons[3].click = this.navButtons[3].tap = this.navMenu3.bind(this);

    //rimord image button
    this.navButtons[1].click = this.navButtons[1].tap = this.navMenu1.bind(this);

    //enochett image button
    this.navButtons[0].click = this.navButtons[0].tap = this.navMenu2.bind(this);

    //enochett image button
    this.navButtons[2].click = this.navButtons[2].tap = this.navMenu4.bind(this);

    //enochett image button
    this.navButtons[4].click = this.navButtons[4].tap = this.navMenu5.bind(this);

    this.sceneContainer.addChild(this.imageContainer);

}

Tivoli.prototype.tivoliOverview = function() {
    UnloadScene.apply(this);
    localStorage.removeItem('exercise');
    // createjs.Sound.removeAllSounds();
    history.back();
}

Tivoli.prototype.navMenu = function(submenu1, submenu2, index) {

    this.sceneChange(submenu1);

    var self = this;

    this.pushStuffToHistory("history_other_Tivoli");

    this.subMenu = self.menu.getActiveMenu(index);
    self.menu.moveSelectedExerciseCircle(this.subMenu[0].position);
    self.sceneContainer.addChild(self.menuContainer);

    this.subMenu[0].click = this.subMenu[0].tap = function(data) {
        self.sceneChange(submenu1);

        self.menu.moveSelectedExerciseCircle(this.position);
        self.sceneContainer.addChild(self.menuContainer);

    }
    this.subMenu[1].click = this.subMenu[1].tap = function(data) {

        self.sceneChange(submenu2);

        self.menu.moveSelectedExerciseCircle(this.position);
        self.sceneContainer.addChild(self.menuContainer);
    }

}

Tivoli.prototype.pushStuffToHistory = function(key) {

    if (Device.ieVersion != 9) {
        var exHistory = new History();
        exHistory.PushState({
            scene: key
        });
        exHistory = null;
    }
}

Tivoli.prototype.navMenu1 = function() {
    this.navMenu('Tivoli_1_1', 'Tivoli_1_2', 1);
}

Tivoli.prototype.navMenu2 = function() {
    this.navMenu('Tivoli_2_1', 'Tivoli_2_2', 2);
}

Tivoli.prototype.navMenu3 = function() {
    this.navMenu('Tivoli_3_1', 'Tivoli_3_2', 3);
}

Tivoli.prototype.navMenu4 = function() {
    this.navMenu('Tivoli_4_1', 'Tivoli_4_2', 4);
}

Tivoli.prototype.navMenu5 = function() {
    this.navMenu('Tivoli_5_1', 'Tivoli_5_2', 5);
}

Tivoli.prototype.cleanMemory = function() {
    main.eventRunning = false;
}