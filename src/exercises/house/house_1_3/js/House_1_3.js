/**
 * house_1_3 intro

 house helptexts audio not used yet.
 */
function House_1_3() {
    cl.show();

    PIXI.DisplayObjectContainer.call(this);
    main.CurrentExercise = "house_1_3";
    //ceate empty container to store all alfabetet scene stuffs
    this.baseContainer = new PIXI.DisplayObjectContainer();

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    //ceate empty container to store all alfabetet scene stuffs
    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();
    this.exerciseContainer = new PIXI.DisplayObjectContainer();

    localStorage.removeItem('house_1_3');

    this.fromOutro = 0;

    main.texturesToDestroy = [];

    this.sceneContainer.position = {
        x: 77,
        y: 88
    };
    this.baseContainer.addChild(this.sceneContainer);
    this.loadSpriteSheet();
    this.addChild(this.baseContainer);

}

House_1_3.constructor = House_1_3;
House_1_3.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

House_1_3.imagePath = 'build/exercises/house/house_1_3/images/';
House_1_3.audioPath = 'build/exercises/house/house_1_3/audios/';
House_1_3.commonFolder = 'build/common/images/house/';
House_1_3.totalFeedback = 29; // default 10

House_1_3.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(House_1_3.audioPath + 'intro');
    this.outroId = loadSound(House_1_3.audioPath + 'outro');

    // create an array of assets to load
    var assetsToLoad = [
        House_1_3.imagePath + "sprite_1_3.json",
        House_1_3.imagePath + "sprite_1_3.png",
        House_1_3.imagePath + "SkolaTextLight.png",
        House_1_3.imagePath + "SkolaTextLight.xml"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

};

House_1_3.prototype.spriteSheetLoaded = function() {

    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF); //d7ead3

    this.loadDefaults();
};



House_1_3.prototype.loadDefaults = function() {

    var self = this;

    this.tileHeightWidth = 180;


    var whiteCardGraphic = createRect({
        w: 188,
        h: 188
    });

    var whiteCard = whiteCardGraphic.generateTexture();

    this.feedBackBlock = whiteCard;

    this.alphabetQuestionCard = new PIXI.Sprite(whiteCard);
    this.alphabetQuestionCard.position = {
        x: 470,
        y: 335 + 25
    };
    main.texturesToDestroy.push(this.alphabetQuestionCard)


    var tileGraphics = new PIXI.Graphics();
    tileGraphics.beginFill(0xFFFFFF);
    tileGraphics.lineStyle(1, 0x000000);
    tileGraphics.drawRect(0, 0, this.tileHeightWidth, this.tileHeightWidth);
    this.tileTexture = tileGraphics.generateTexture();


    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0x9ECEA0); //9ecea0
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xf3c873);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);

    //this.sceneBackground.beginFill(0x9ecea0); //9ecea0

    this.sceneContainer.addChild(this.sceneBackground);
    //this.sceneBackground.drawRect(0, 0, 1843, 431);

    this.sceneContainer.addChild(this.introContainer);

    // girl with arms crossed
    this.lillamasteren = new PIXI.Sprite.fromFrame('lillaspartan.png');
    this.lillamasteren.position = {
        x: 68,
        y: 243
    };

    this.handHoldingLetter = new PIXI.Sprite.fromFrame("arm_holiding_letter.png");
    this.handHoldingLetter.position.set(349, 501);

    this.poster = new PIXI.Sprite.fromFrame("correct_poster.png");
    this.poster.position = {
        x: 870,
        y: 21
    };

    main.texturesToDestroy.push(this.lillamasteren);
    main.texturesToDestroy.push(this.poster);
    main.texturesToDestroy.push(this.handHoldingLetter);
    this.sceneContainer.addChild(this.poster);
    this.sceneContainer.addChild(this.lillamasteren);
    this.sceneContainer.addChild(this.handHoldingLetter);

    this.help = PIXI.Sprite.fromFrame("help.png");
    this.help.position = {
        x: 0,
        y: 0
    };

    this.help.buttonMode = true;
    this.help.interactive = true;
    this.baseContainer.addChild(this.help);
    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.mouthParams = {
        "image": "mouth",
        "length": 5,
        "x": 283,
        "y": 428,
        "speed": 6,
        "pattern": true,
        "frameId": 1
    };
    this.mouth = new Animation(this.mouthParams);
    this.sceneContainer.addChild(this.mouth);
    this.mouth.gotoAndStop(5);

    //the pattern of the eye needs to change less than mouth
    this.eyeParams = {
        "image": "eyes",
        "length": 2,
        "x": 273,
        "y": 371,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1],
        "frameId": 1
    };

    this.eye = new Animation(this.eyeParams);
    this.sceneContainer.addChild(this.eye);
    this.eye.gotoAndStop(1);

    this.hands = [
        new PIXI.Sprite.fromFrame('arm1.png'),
        new PIXI.Sprite.fromFrame('arm_pointing_board.png'),
        new PIXI.Sprite.fromFrame('arm_pointing_further.png'),
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(160, 488);
        this.sceneContainer.addChild(this.hands[i]);
        this.hands[i].visible = false;
    }

    this.showHandAt(0);

    this.sceneContainer.addChild(this.outroContainer);
    this.outroContainer.visible = false;
    this.sceneContainer.addChild(this.exerciseContainer);

    this.back_again = PIXI.Sprite.fromFrame("ovaigen.png");
    this.back_again.position = {
        x: 442,
        y: 417
    };
    this.back_again.buttonMode = true;
    this.back_again.interactive = true;
    this.sceneContainer.addChild(this.back_again);
    this.back_again.visible = false;

    this.loadGameData();

    this.handHoldingLetter.bringToFront();

};

House_1_3.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    }
    if (isNumeric(number)) {
        this.hands[number].bringToFront();
        this.hands[number].visible = true;
    }
};

/**
 * play intro animation
 */
House_1_3.prototype.introAnimation = function() {

    createjs.Sound.stop();
    main.overlay.visible = true;

    if (this.fromOutro == 1) {
        this.fromOutro = 0;
        this.alphabetQuestionCard.visible = true;
        this.resetFeedback();
        this.resetExercise();
        this.restartExercise();
    }

    this.outroContainer.visible = false;
    this.introContainer.visible = true;

    this.help.interactive = false;
    this.help.buttonMode = false;

    this.eye.show();
    this.mouth.show();
    this.eye.show();

    this.handHoldingLetter.bringToFront();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.

start at arm_0 => 0
0.5-3 sec arm_pointing further => 2
3-5 sec arm_pointing_atsign => 1
5 - stop arm_0 => 0

 */
House_1_3.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    if (currentPostion >= 500 && currentPostion < 3000) {
        this.showHandAt(2);
    } else if (currentPostion >= 3000 && currentPostion < 5000) {
        this.showHandAt(1);
    } else {
        this.showHandAt(0);
    }
};

/**
 * cancel running intro animation
 */
House_1_3.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);
    this.mouth.hide(5);
    this.eye.hide(1);
    this.showHandAt(0);

    this.help.interactive = true;
    this.help.buttonMode = true;

    this.back_again.visible = false;
    main.overlay.visible = false;
}

House_1_3.prototype.outroAnimation = function() {

    var self = this;
    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        main.page.showHandAt(1);
        var totalDuration = outroInstance.getDuration();
        if (self.timerId) {
            clearInterval(self.timerId);
            self.timerId = null;
        }
        self.timerId = setInterval(function() {
            var currentPostion = outroInstance.getPosition();
            if (currentPostion > 0 && currentPostion < 1000) {
                self.showHandAt(1);
            } else {
                self.showHandAt(1);
                self.handHoldingLetter.visible = true;
                self.back_again.visible = true;
                self.handHoldingLetter.bringToFront();
                self.hands[1].bringToFront();
            }
        }, 1);
        self.mouth.show();
        self.eye.show();
    }


    this.outroCancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.outroCancelHandler)

};


House_1_3.prototype.cancelOutro = function() {

    var self = this;

    createjs.Sound.stop();

    self.mouth.hide(5);

    self.eye.hide(1);
    self.showHandAt(1);
    clearInterval(self.timerId);
    self.timerId = null;

    self.help.interactive = true;
    self.help.buttonMode = true;
    main.overlay.visible = false;

};

House_1_3.prototype.resetFeedback = function() {
    var self = this;
    localStorage.setItem('house_1_3', 0);

}


House_1_3.prototype.restartExercise = function() {
    var self = this;
    this.resetFeedback();
    this.exerciseContainer.removeChildren();
    main.page.showHandAt(0);
    this.runExercise();
};

House_1_3.prototype.onComplete = function() {

    var self = this;

    this.introMode = false;

    localStorage.removeItem('house_1_3');
    this.handHoldingLetter.visible = false;
    this.introContainer.visible = false;
    this.fromOutro = 1;
    this.help.interactive = false;
    this.help.buttonMode = false;
    this.alphabetQuestionCard.visible = false;
    this.outroContainer.visible = true;

    this.back_again.click = this.back_again.tap = function(data) {
        self.fromOutro = 0;
        self.alphabetQuestionCard.visible = true;
        self.resetExercise();
        self.restartExercise();
    };

    this.outroAnimation();
};

House_1_3.prototype.resetExercise = function() {
    createjs.Sound.stop();
    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    this.back_again.visible = false;
    this.help.interactive = true;
    this.help.buttonMode = true;

    this.mouth.hide(5);
    this.eye.hide(1);
    this.showHandAt(0);

    this.introMode = true;

    addedListeners = false;
}

House_1_3.prototype.cardText = function() {
    var self = this;
    //this.randomid = makeUniqueRandom(29);

    //    var text = this.alphabetToDrawCard[this.randomid];
    var text = this.alphabetToDrawCard[Math.floor(Math.random() * this.alphabetToDrawCard.length)];

    var idxOfCurrentWord = this.alphabetToDrawCard.indexOf(text);
    if (idxOfCurrentWord > -1) this.alphabetToDrawCard.splice(idxOfCurrentWord, 1); //remove displayed alphabet from array so that it won't repeat next time

    var ansText = new PIXI.BitmapText(text + " ", {
        font: "100px SkolaTextLight",
        fill: "black"
    });
    this.exerciseContainer.addChild(this.alphabetQuestionCard);

    /*ansText.position = {
        x: (this.alphabetQuestionCard.width - ansText.width) / 2,
        y: (this.alphabetQuestionCard.height - ansText.height) / 2 - 20
    };*/
    ansText.position = {
        x: 64,
        y: 37
    }
    console.log(ansText.position)
    if (this.alphabetQuestionCard.children.length > 0) {
        this.alphabetQuestionCard.removeChildAt(0);
    }
    this.alphabetQuestionCard.addChild(ansText);

    self.displayText = text;

};

House_1_3.prototype.runExercise = function() {

    var self = this;

    this.tileClickButtons = [];

    var alphabetToDrawTile = [];
    this.alphabetToDrawCard = [];

    var tilePositionStart = [880, 30]; //Bottom point, need to start from bottom cause the top parts needs to be overlapped by nextblock

    var jsonData = this.alphabets;

    //this.soundTexture = new PIXI.Texture.fromFrame("sound_grey.png");]
    for (var j = 0; j < jsonData.caps.length; j++) {
        if (Math.floor((Math.random() * 10) + 1) % 2 == 0) {
            alphabetToDrawTile.push(jsonData.caps[j] + "__");
            this.alphabetToDrawCard.push(jsonData.small[j]);
        } else {
            alphabetToDrawTile.push("__" + jsonData.small[j]);
            this.alphabetToDrawCard.push(jsonData.caps[j]);
        }
    }
    shuffle(this.alphabetToDrawCard);
    self.cardText();

    for (var i = 0; i < jsonData.caps.length; i++) {
        //Tile part the right side covering pictures.
        var whiteTile = new PIXI.Sprite(this.tileTexture);

        //this.alphabetTiles.push(alphabetToDrawCard);

        var tileText = new PIXI.BitmapText(alphabetToDrawTile[i] + " ", {
            font: "75px SkolaTextLight",
            fill: "black"
        });
        /*tileText.position = {
            x: 20,
            y: 60
        };*/
        tileText.position = {
            x: (whiteTile.width - tileText.width) / 2,
            y: (whiteTile.height - tileText.height) / 2 - 15
        };

        whiteTile.addChild(tileText);
        whiteTile.interactive = true;
        whiteTile.buttonMode = true;

        whiteTile.correctAlphabet = " " + jsonData.caps[i] + jsonData.small[i] + " ";

        if (i >= 0 && i < 5) {
            yPos = tilePositionStart[1] + 0 * this.tileHeightWidth + 1 * 0;
        } else if (i >= 5 && i < 10) {
            yPos = tilePositionStart[1] + 1 * this.tileHeightWidth + 1 * 1;
        } else if (i >= 10 && i < 15) {
            yPos = tilePositionStart[1] + 2 * this.tileHeightWidth + 1 * 2;
        } else if (i >= 15 && i < 20) {
            yPos = tilePositionStart[1] + 3 * this.tileHeightWidth + 1 * 3;
        } else if (i >= 20 && i < 25) {
            yPos = tilePositionStart[1] + 4 * this.tileHeightWidth + 1 * 4;
        } else if (i >= 25 && i < 30) {
            yPos = tilePositionStart[1] + 5 * this.tileHeightWidth + 1 * 5;
        }

        whiteTile.position = {
            x: tilePositionStart[0] + (i % 5) * this.tileHeightWidth + 3 * (i % 5) + 10,
            y: yPos + 10
        };
        whiteTile.tilePosition = whiteTile.position;
        whiteTile.clickedTile = alphabetToDrawTile[i];
        whiteTile.hitIndex = i;

        this.exerciseContainer.addChild(whiteTile);
        main.texturesToDestroy.push(whiteTile);


        this.tileClickButtons.push(whiteTile);

    }

    for (var k = 0; k < this.tileClickButtons.length; k++) {
        this.tileClickButtons[k].click = this.tileClickButtons[k].tap = function() {
            if (addedListeners) return;
            addedListeners = true;
            var button = this;
            var clickedAnswer = button.clickedTile.replace("__", "").toLowerCase();
            var displayedCard = self.displayText.replace("__", "").toLowerCase();
            var correctText = displayedCard.toUpperCase() + " " + displayedCard.toLowerCase();
            if (displayedCard == clickedAnswer) { //correct answer

                correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                self.showRedGreenBox(this.tilePosition.x, this.tilePosition.y, self.feedBackBlock, 0x00ff00);
                self.correctFeedback(this.hitIndex, correctText);

            } else { //wrong answer

                wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                self.showRedGreenBox(this.tilePosition.x, this.tilePosition.y, self.feedBackBlock, 0xff0000);
                addedListeners = false;

            }
        }
    }

};

House_1_3.prototype.showRedGreenBox = function(xPos, yPos, obj, tint) {
    var self = this;
    var ft = new PIXI.Sprite(obj);
    this.sceneContainer.addChild(ft);
    ft.tint = tint;
    ft.position = {
        x: xPos,
        y: yPos
    };
    ft.width = 180;
    ft.height = 180;
    ft.alpha = 0.5;
    ft.visible = true;

    setTimeout(function() {
        ft.visible = false;
    }, 1200);

}


House_1_3.prototype.correctFeedback = function(tileId, correctText) {
    var self = this;
    var feedback = localStorage.getItem('house_1_3');
    feedback++;
    localStorage.setItem('house_1_3', feedback);

    //self.tileClickButtons[tileId].
    var obj = self.tileClickButtons[tileId]
    obj.getChildAt(0).setText(correctText);
    obj.getChildAt(0).position = {
        x: (obj.width - obj.getChildAt(0).width) / 2 + 10,
        y: (obj.height - obj.getChildAt(0).height) / 2 - 20,
    };
    setTimeout(function() {
        obj.visible = false;
        self.cardText(); //reset display Card with another alphabet
        addedListeners = false;
    }, 2000);



    if (feedback >= House_1_3.totalFeedback)
        setTimeout(this.onComplete.bind(this), 1000);
}

House_1_3.prototype.loadGameData = function() {
    var self = this;
    // create empty object to load dot to dot game json data
    this.alphabets = {
        "caps": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "Å", "Ä", "Ö"],
        "small": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "å", "ä", "ö"]
    };

    self.runExercise();

}

House_1_3.prototype.cleanMemory = function() {

    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    this.help = null;

    this.mouth = null;
    this.mouthParams = null;



    this.back_again = null;


    this.tileHeightWidth = null;
    this.feedBackBlock = null;
    this.alphabetQuestionCard = null;
    this.lillamasteren = null;
    this.poster = null;



    this.eyeParams = null;
    this.eye = null;
    this.hands = null;
    this.fromOutro = null;
    if (this.alphabetToDrawCard) this.alphabetToDrawCard.length = 0;
    if (this.tileClickButtons) this.tileClickButtons.length = 0;


    this.sceneContainer = null;
    this.introContainer = null;
    this.outroContainer = null;
    this.baseContainer.removeChildren();
    this.baseContainer = null;

};