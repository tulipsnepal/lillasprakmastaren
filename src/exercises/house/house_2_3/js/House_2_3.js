function House_2_3() {
    cl.show()

    PIXI.DisplayObjectContainer.call(this);

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position.set(77, 88);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();

    localStorage.setItem('house_2_3', 0);
    main.CurrentExercise = "house_2_3";

    main.texturesToDestroy = [];
    this.fromOutro = 0;

    this.common = new Common();

    this.loadSpriteSheet();

    // everything connected to this is rendered.
    this.addChild(this.sceneContainer);
}

House_2_3.constructor = House_2_3;
House_2_3.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
House_2_3.imagePath = 'build/exercises/house/house_2_3/images/';
House_2_3.audioPath = 'build/exercises/house/house_2_3/audios/';
House_2_3.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

/**
 * load array of assets of different format
 */
House_2_3.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(House_2_3.audioPath + 'intro');
    this.outroId = loadSound(House_2_3.audioPath + 'outro');

    // create an array of assets to load
    this.assetsToLoad = [
        House_2_3.imagePath + "sprite_2_3.json",
        House_2_3.imagePath + "sprite_2_3.png",
        House_2_3.imagePath + "base_outro.png",
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
House_2_3.prototype.spriteSheetLoaded = function() {

    // hide loader
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    // fill exercise background
    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0x88d4f7);
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xf7c86b);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);

    // exercise instruction
    this.help = PIXI.Sprite.fromFrame("help.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);

    this.loadExercise();
    this.introScene();

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.outroContainer);

    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.addChild(this.exerciseContainer);

    this.sceneContainer.addChild(this.help);
}

/**
 * play intro animation
 */
House_2_3.prototype.introAnimation = function() {

    createjs.Sound.stop();
    main.overlay.visible = true;

    if (this.fromOutro == 1) {
        this.fromOutro = 0;
        this.resetExercise();
        this.restartExercise();
    }

    this.enableKeypad(false);

    this.animationContainer.visible = true;

    this.help.interactive = false;
    this.help.buttonMode = false;
    this.mouths.show();


    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
 0-5 sec arm_1 =>  0
5-7 sec arm 2 => 2
7-9 sec arm_3 => 1
stop at arm_1 => 0
 */
House_2_3.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    if (currentPostion >= 1000 && currentPostion < 5000) {
        this.showHandAt(0);
    } else if (currentPostion >= 5000 && currentPostion < 7800) {
        this.showHandAt(2);
    } else if (currentPostion >= 7800 && currentPostion < 9000) {
        this.showHandAt(1);
    } else {
        this.showHandAt(0);
    }
};

/**
 * cancel running intro animation
 */
House_2_3.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);
    this.mouths.hide();

    this.animationContainer.visible = false;

    this.help.interactive = true;
    this.help.buttonMode = true;

    this.enableKeypad(true);

    this.ovaigen.visible = false;
    main.overlay.visible = false;
}

House_2_3.prototype.outroAnimation = function() {
    var self = this;

    this.enableKeypad(false);

    var outroInstance = createjs.Sound.play(this.outroId);

    if (createjs.Sound.isReady()) {

        main.overlay.visible = true;
        self.mouthOutro.show();

    }
    this.cancelHandler = function() {
        newLeftDoorPos = 760;

        function moveLeftDoor() {
            if (self.door_closed_left.position.x < newLeftDoorPos - 3) {
                self.door_closed_left.position.x += 5;
                requestAnimFrame(moveLeftDoor);
            }
        }

        requestAnimFrame(moveLeftDoor);

        newRightDoorPos = 996;

        function moveRightDoor() {
            if (self.door_closed_right.position.x > newRightDoorPos) {
                self.door_closed_right.position.x -= 5;
                requestAnimFrame(moveRightDoor);
            }
        }

        requestAnimFrame(moveRightDoor);

        var b = setTimeout(function() {
            newLiftPosition = 900;

            function moveLiftDown() {
                if (self.outroLilla.position.y < newLiftPosition) {
                    self.outroLilla.position.y += 5;
                    requestAnimFrame(moveLiftDown);
                }
            }

            requestAnimFrame(moveLiftDown);
        }, 2000);

        self.cancelOutro();

    }
    outroInstance.addEventListener("complete", this.cancelHandler);
}

House_2_3.prototype.cancelOutro = function() {
    var self = this;
    createjs.Sound.stop();
    self.mouthOutro.hide();

    self.help.interactive = true;
    self.help.buttonMode = true;

    self.ovaigen.visible = true;
    main.overlay.visible = false;

};


House_2_3.prototype.handVisible = function(index) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    };
    if (index !== undefined)
        this.hands[index].visible = true;
}

/**
 * create sprites,animation,sound related to intro scene
 */
House_2_3.prototype.introScene = function() {

    var self = this;

    this.animationContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.animationContainer);

    this.lillasparten_intro = new PIXI.Sprite.fromFrame('lillasparten.png');
    this.lillasparten_intro.position = {
        x: 1344,
        y: 371
    };
    main.texturesToDestroy.push(this.lillasparten_intro);
    this.animationContainer.addChild(this.lillasparten_intro);

    this.mouths = new Animation({
        "image": "mouth_intro_",
        "length": 4,
        "x": 1468,
        "y": 544,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 2, 4, 1],
        "frameId": 2
    });

    this.animationContainer.addChild(this.mouths);

    this.hands = [
        new PIXI.Sprite.fromFrame('arm1intro.png'),
        new PIXI.Sprite.fromFrame('arm2intro.png'),
        new PIXI.Sprite.fromFrame('arm3intro.png'),
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(1172, 366);
        this.animationContainer.addChild(this.hands[i]);
        this.hands[i].visible = false;
    }

    this.showHandAt(0);

    this.animationContainer.visible = false;

    this.outroScene();
}

/**
 * create sprites,animation,sound related to outro scene
 */
House_2_3.prototype.outroScene = function() {

    var self = this;

    this.outroContainer.visible = false;

    this.outroLilla = new PIXI.Sprite.fromImage(House_2_3.imagePath + "outrolilla.png");
    this.outroLilla.position.x = 758;
    this.outroLilla.position.y = 58;
    this.outroContainer.addChild(this.outroLilla);

    this.liftMasker = createRect({
        w: 500,
        h: 400,
        color: "0xffffff"
    });
    this.liftMasker.position.set(750, 1200);
    this.outroContainer.addChild(this.liftMasker);


    this.mouthOutro = new Animation({
        "image": "mouth_outro_",
        "length": 4,
        "x": 957,
        "y": 202,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 3, 2, 4, 1],
        "frameId": 0
    });

    this.outroContainer.addChild(this.mouthOutro);

    this.door_closed_left = new PIXI.Sprite.fromFrame('door_closed_outro_left.png');
    this.door_closed_left.position.x = 588;
    this.door_closed_left.position.y = 58;

    this.outroContainer.addChild(this.door_closed_left);

    this.door_closed_right = new PIXI.Sprite.fromFrame('door_closed_outro_right.png');
    this.door_closed_right.position.x = 1162;
    this.door_closed_right.position.y = 58;

    this.outroContainer.addChild(this.door_closed_right);

    this.outroBackground = new PIXI.Sprite.fromImage(House_2_3.imagePath + "base_outro.png");
    this.outroContainer.addChild(this.outroBackground);

    // back to intro page
    this.ovaigen = PIXI.Sprite.fromFrame('ovaigen.png');
    this.ovaigen.position.set(1379, 348);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.outroContainer.addChild(this.ovaigen);
}

/**
 * load exercise related stuffs
 */
House_2_3.prototype.loadExercise = function() {

    this.exerciseBaseContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.addChild(this.exerciseBaseContainer);

    this.keyboardContainer = new PIXI.DisplayObjectContainer();
    this.keyboardContainer.position = {
        x: 150,
        y: 813
    };
    this.sceneContainer.addChild(this.keyboardContainer);


    this.sidelift = new PIXI.Sprite.fromFrame("sidelift.png");
    this.sidelift.position = {
        x: 1366,
        y: 2
    };

    this.exerciseBaseContainer.addChild(this.sidelift);

    this.boxTexture = new PIXI.Texture.fromFrame("box.png");

    this.boxColumn1Start = [1472, 382];

    this.boxColumn2Start = [1577, 382];
    this.boxDiff = 72;


    // position, size parameter for exercise
    this.params = {
        size: {
            white: {
                width: 240,
                height: 240
            },
            miniWhite: {
                width: 84,
                height: 84
            }
        },
        letters: [
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'Å', 'Ä', 'Ö',
        ]
    }

    this.whiteGraphics = createRect({
        w: this.params.size.white.width,
        h: this.params.size.white.height
    });
    this.whiteTexture = this.whiteGraphics.generateTexture();

    this.keypad = new PIXI.Sprite.fromFrame('keyboard.png');
    this.keypad.position.set(0, 0);
    main.texturesToDestroy.push(this.keypad);
    this.keyboardContainer.addChild(this.keypad);

    this.circleGraphics = createCircle({
        radius: 50,
        color: 0x616161
    });

    this.circleTexture = this.circleGraphics.generateTexture();

    this.soundTexture = new PIXI.Texture.fromFrame('sound_grey.png');

    this.keypad = [];

    this.keyboardFirstRowStartPoint = [135, 0];
    this.keyboardFirstRowGap = 120;
    this.keyboardSecondRowStartPoint = [90, 95];
    this.keyboardSecondRowGap = 130;
    this.keyboardThirdRowStartPoint = [45, 182];
    this.keyboardThirdRowGap = 140;

    var alphabetsLen = this.params.letters.length + 1; //+1 for sudda
    for (var i = 0; i < alphabetsLen; i++) {
        var alphabetsSprite = new PIXI.Sprite(this.circleTexture);
        if (i >= 0 && i < 10) {
            alphabetsSprite.position.x = this.keyboardFirstRowStartPoint[0] + i * this.keyboardFirstRowGap;
            alphabetsSprite.position.y = this.keyboardFirstRowStartPoint[1];

        } else if (i >= 10 && i < 20) {
            alphabetsSprite.position.x = this.keyboardSecondRowStartPoint[0] + (i % 10) * this.keyboardSecondRowGap;
            alphabetsSprite.position.y = this.keyboardSecondRowStartPoint[1];

        } else if (i >= 20 && i < 30) {
            alphabetsSprite.position.x = this.keyboardThirdRowStartPoint[0] + (i % 20) * this.keyboardThirdRowGap;
            alphabetsSprite.position.y = this.keyboardThirdRowStartPoint[1];

        } else {
            alphabetsSprite.position.x = 0 + i * 10;
            alphabetsSprite.position.y = 0;
        }

        alphabetsSprite.alpha = 0;
        alphabetsSprite.interactive = true;
        alphabetsSprite.buttonMode = true;
        alphabetsSprite.index = i;
        this.keypad.push(alphabetsSprite);
        this.keyboardContainer.addChild(alphabetsSprite);
    };

    this.boxes = [];
    for (var i = 8; i >= 0; i -= 2) {
        this.boxes[i] = new PIXI.Sprite(this.boxTexture);
        this.boxes[i].position = {
            x: this.boxColumn1Start[0],
            y: this.boxColumn1Start[1] + (i / 2) * this.boxDiff
        };
        this.exerciseBaseContainer.addChild(this.boxes[i]);
    };
    for (var i = 9; i >= 0; i -= 2) {
        this.boxes[i] = new PIXI.Sprite(this.boxTexture);

        this.boxes[i].position = {
            x: this.boxColumn2Start[0],
            y: this.boxColumn2Start[1] + ((i - 1) / 2) * this.boxDiff
        };
        this.exerciseBaseContainer.addChild(this.boxes[i]);
    };

    this.lillaPicking = [];
    for (i = 0; i < 10; i++) {
        this.lillaPicking[i] = new PIXI.Sprite.fromFrame("fb_pick_" + (i + 1) + ".png");
        this.lillaPicking[i].position = {
            x: 1424,
            y: 178
        };
        this.exerciseBaseContainer.addChild(this.lillaPicking[i]);
        this.lillaPicking[i].visible = false;
    }
    this.lillaPicking.reverse();
    this.initObjectPooling();
}

House_2_3.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    };
    if (isNumeric(number))
        this.hands[number].visible = true;
}

/**
 * manage exercise with new sets randomized
 */
House_2_3.prototype.runExercise = function() {

    var self = this;

    this.jsonData = this.wallSlices[0];
    this.evaluationAnswer = "";

    this.currentMarkerAt = 0;
    this.markers = [];
    this.answer = this.jsonData.answer;

    var letters = this.jsonData.word.split(''),
        answer = this.jsonData.answer.split(''),
        lettersStartPos = [865 - (letters.length / 2 * 56 + letters.length / 2 * 25), 493]; //626
    this.markerStartPos = [865 - (letters.length / 2 * 56 + letters.length / 2 * 25), 645]; //626

    for (var j = 0; j < letters.length; j++) {

        var word = new PIXI.Text(letters[j], {
            'font': '80px Arial'
        });
        var frontPadding = (56 - word.width) / 2;
        word.position.x = lettersStartPos[0] + 56 * j + frontPadding + 25 * j;
        word.position.y = lettersStartPos[1]

        this.exerciseContainer.addChild(word);

        var marker = new PIXI.Text("_", {
            'font': '80px Arial'
        });

        marker.position.x = this.markerStartPos[0] + 56 * j + 25 * j;
        marker.position.y = this.markerStartPos[1] - 20

        this.exerciseContainer.addChild(marker);
        this.markers.push(marker);

    };

    var whiteSprite = new PIXI.Sprite(this.whiteTexture),
        imageSprite = this.jsonData.image,
        soundSprite = new PIXI.Sprite(this.soundTexture);

    soundSprite.position.x = 10;
    soundSprite.position.y = 180;
    soundSprite.interactive = true;
    soundSprite.buttonMode = true;
    soundSprite.howl = this.jsonData.sound;

    whiteSprite.position.set(726, 175);

    imageSprite.position.x = 0;
    imageSprite.position.y = 0;
    imageSprite.width = imageSprite.height = 240;

    whiteSprite.addChild(imageSprite);
    whiteSprite.addChild(soundSprite);

    this.exerciseContainer.addChild(whiteSprite);
    this.whiteSprite = whiteSprite;

    for (var i = 0; i < this.keypad.length; i++) {
        this.onKeypadHit(i);
    }
    this.keyPressHandler = this.onKeyPress.bind(this);
    this.view.addEventListener("keydown", this.keyPressHandler, false);
    this.markers[this.currentMarkerAt].setStyle({
        'font': '80px Arial',
        'strokeThickness': 6
    });

    soundSprite.click = soundSprite.tap = this.onSoundPressed.bind(soundSprite);

}

House_2_3.prototype.onSoundPressed = function() {
    this.soundInstance = createjs.Sound.play(this.howl, {
        interrupt: createjs.Sound.INTERRUPT_NONE
    });
    this.soundInstance.addEventListener("failed", handleFailed);
    this.soundInstance.addEventListener("complete", handleComplete);
};



House_2_3.prototype.view = window;

House_2_3.prototype.onKeyPress = function(e) {
    var self = this;
    //console.log(e);

    if (addedListeners) return false;
    //if (e.shiftKey) return false;

    addedListeners = true;

    var code = e.charCode || e.keyCode,
        character = String.fromCharCode(code),
        keyChar = character, //.toLowerCase(),
        keyIndex = self.params.letters.indexOf(keyChar);
    console.log(keyChar);


    if (code == 8) {
        keyIndex = self.keypad.length - 1;
        self.undoFeature();
        e.preventDefault();
    }



    console.log(keyIndex);
    if (isNumeric(keyIndex) && keyIndex >= 0 && keyIndex < self.keypad.length - 1) {
        if (self.currentMarkerAt < self.markers.length) {
            self.keypad[keyIndex].alpha = 0.6;
            setTimeout(function() {
                self.keypad[keyIndex].alpha = 0;
            }, 100)
        }
        self.onKeyClickTap(keyIndex);
    } else {
        addedListeners = false;
    }
}

House_2_3.prototype.undoFeature = function() {
    if (this.currentMarkerAt > 0) {
        this.markers[this.currentMarkerAt].setStyle({
            'font': '80px Arial'
        });
        this.currentMarkerAt--;
        this.markers[this.currentMarkerAt].setStyle({
            'font': '80px Arial',
            'strokeThickness': 6
        });
        this.markers[this.currentMarkerAt].setText("_");
        this.markers[this.currentMarkerAt].position.x = this.markerStartPos[0] + 56 * this.currentMarkerAt + 25 * this.currentMarkerAt;
        this.markers[this.currentMarkerAt].position.y = this.markers[this.currentMarkerAt].position.y;
        this.evaluationAnswer = this.evaluationAnswer.substr(0, this.evaluationAnswer.length - 1);
    }
}

/**
 * perform task on keypad on click/tap
 * @param  {integer} i
 */
House_2_3.prototype.onKeypadHit = function(i) {
    var self = this,
        key = this.keypad[i];

    key.click = key.tap = function(data) {
        /*if (self.evaluationAnswer.length >= self.exactAnswer.length) {
            self.markerGraphics.visible = false;
            return false;
        };*/
        if (this.index >= self.keypad.length - 1) {
            self.undoFeature();
        } else {
            self.onKeyClickTap(this.index);
        }
    }

    key.mousedown = key.touchstart = function(data) {
        if (self.currentMarkerAt < self.markers.length)
            this.alpha = 0.6;
    }

    key.mouseup = key.mouseupoutside = key.touchend = key.touchendoutside = function(data) {
        this.alpha = 0;
    }

}

House_2_3.prototype.onKeyClickTap = function(index) {
    var self = this;

    self.markers[self.currentMarkerAt].setStyle({
        'font': '80px Arial'
    });

    self.markers[self.currentMarkerAt].setText("" + self.params.letters[index]);

    frontPadding = (56 - self.markers[self.currentMarkerAt].width) / 2;

    // self.markers[self.currentMarkerAt].position.x = self.markerStartPos[0] + 56 * self.currentMarkerAt + frontPadding + 25 * self.currentMarkerAt;
    // self.markers[self.currentMarkerAt].position.y -= 65;

    self.evaluationAnswer += self.params.letters[index];

    self.evaluateRound();
    addedListeners = false;
};

House_2_3.prototype.evaluateRound = function() {

    var self = this;

    self.currentMarkerAt++;
    if (self.currentMarkerAt < self.markers.length - 1) {
        self.markers[self.currentMarkerAt].setStyle({
            'font': '80px Arial',
            'strokeThickness': 6
        });
    }

    if (self.currentMarkerAt == self.markers.length) {
        if (self.currentMarkerAt === self.markers.length) {
            self.enableKeypad(false);
            setTimeout(function() {
                for (var i = 0; i < self.keypad.length; i++) {
                    self.keypad[i].alpha = 0;
                }
                if (self.evaluationAnswer == self.answer) {
                    correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                    self.showGreenAnswer();
                    self.correctFeedback();
                } else {
                    self.wrongFeedback();
                }
            }, 1000)
        };

    }
}

House_2_3.prototype.showGreenAnswer = function() {
    for (var i = this.markers.length - 1; i >= 0; i--) {
        this.markers[i].setStyle({
            'font': '80px Arial',
            'fill': '#009640'
        });
    };
}

/**
 * toggle keypad enable/disable
 */
House_2_3.prototype.enableKeypad = function(bool) {

    if (bool) {
        for (var i = 0; i < this.keypad.length; i++) {
            this.keypad[i].interactive = true;
            this.keypad[i].buttonMode = true;
        }
        //this.markerGraphics.visible = true;
    } else {
        for (var i = 0; i < this.keypad.length; i++) {
            this.keypad[i].interactive = false;
            this.keypad[i].buttonMode = false;
        }
        //this.markerGraphics.visible = false;
    }


}

/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
House_2_3.prototype.correctFeedback = function() {

    var self = this;

    var feedback = localStorage.getItem('house_2_3');

    self.boxes[feedback].visible = false;

    self.lillaPicking[feedback].visible = true;

    setTimeout(function() {
        self.enableKeypad(true);
        self.currentMarkerAt = 0;
        self.evaluationAnswer = "";
        self.lillaPicking[feedback].visible = false;


        feedback++;
        localStorage.setItem('house_2_3', feedback);

        if (feedback >= House_2_3.totalFeedback) {
            setTimeout(self.onComplete.bind(self), 1000);
        } else {
            addedListeners = false;
            self.view.removeEventListener("keydown", self.keyPressHandler, false);
            self.getNewExerciseSet();
        }
    }, 1000);

}

/**
 * for correction only the letters that are wrong should go down
 */
House_2_3.prototype.wrongFeedback = function() {

    var self = this;

    wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);

    var arrEvaluationAnswer = this.evaluationAnswer.split(''),
        arrActualAnswer = this.answer.split(''),
        arrAnswer = [],
        arrMarkers = [];

    for (var i = 0; i < arrEvaluationAnswer.length; i++) {
        if (arrEvaluationAnswer[i] !== arrActualAnswer[i]) {
            arrMarkers.push(this.markers[i]);
            // this.markers[i].position.x = this.markerStartPos[0] + 56 * i + 25 * i;
            arrAnswer.push(arrActualAnswer[i]);
        }
    };

    this.markers.length = 0;
    this.markers = arrMarkers;

    for (var i = this.markers.length - 1; i >= 0; i--) {
        this.markers[i].setStyle({
            'font': '80px Arial',
            'fill': '#dc3732'
        });
    };

    setTimeout(function() {
        for (var i = self.markers.length - 1; i >= 0; i--) {
            self.markers[i].setStyle({
                'font': '80px Arial',
                'fill': '#000000'
            });
            self.markers[i].setText("_");
            // self.markers[i].position.y += 65;

        };
        self.markerStartPos.length = 0;
        self.markerStartPos[0] = self.markers[0].position.x;
        self.markerStartPos[1] = self.markers[0].position.y;

        self.currentMarkerAt = 0;
        self.markers[self.currentMarkerAt].setStyle({
            'font': '80px Arial',
            'strokeThickness': 6
        });

        self.evaluationAnswer = "";
        self.answer = "";
        self.answer = arrAnswer.join('');

        self.enableKeypad(true);
        addedListeners = false;
    }, 1000);

};

/**
 * prefetch exercise data and cache number of sets defined only.
 */
House_2_3.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/house/house_2_3.json?nocache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        self.pool = new ExerciseSpritesPool(loader.json);
        //borrow two sprites once
        self.generatePoolCategory();
        self.borrowWallSprites(2);
        self.runExercise();

    });
    loader.load();
}


House_2_3.prototype.generatePoolCategory = function() {

    var keyArr = [];

    var spriteArr = this.pool.sprites,
        spriteLen = spriteArr.length;

    for (var i = 0; i < spriteLen; i++) {
        keyArr.push(spriteArr[i][0].word[0]);
    };

    this.poolCategory = keyArr.filter(onlyUnique);

}


House_2_3.prototype.borrowWallSprites = function(num) {

    var rand, category, sprite, spriteObj;
    var self = this;

    for (var i = 0; i < num; i++) {

        rand = makeUniqueRandom(this.poolCategory.length);
        this.borrowCategory = this.poolCategory[rand];

        sprite = this.pool.borrowUniqueSprite(this.borrowCategory, this.lastBorrowSprite);
        spriteObj = sprite[0];
        this.lastBorrowSprite = spriteObj.word[0];

        this.poolSlices.push(sprite);


        var imageSprite = PIXI.Sprite.fromImage('uploads/images/' + spriteObj.image),
            soundHowl = loadSound('uploads/audios/' + StripExtFromFile(spriteObj.sound));

        this.wallSlices.push({
            image: imageSprite,
            sound: soundHowl,
            answer: spriteObj.answer,
            word: spriteObj.word
        });
    }
};

House_2_3.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];

    this.pool.returnSprite(sprite);

    this.poolSlices = [];
    this.wallSlices.shift();
};

House_2_3.prototype.getNewExerciseSet = function() {
    this.exerciseContainer.removeChildren();

    this.returnWallSprites();
    this.borrowWallSprites(1);
    this.runExercise();
}

House_2_3.prototype.restartExercise = function() {

    for (var i = this.boxes.length - 1; i >= 0; i--) {
        this.boxes[i].visible = true;
    };

    localStorage.setItem('house_2_3', 0);
    this.outroContainer.visible = false;
    this.introMode = true;


    this.view.removeEventListener("keydown", this.keyPressHandler, false);

    this.getNewExerciseSet();

};

/**
 * the outro scene is displayed
 */
House_2_3.prototype.onComplete = function() {

    var self = this;

    this.ovaigen.visible = false;

    this.introMode = false;
    this.outroLilla.position.x = 758;
    this.outroLilla.position.y = 58;

    this.help.interactive = false;
    this.help.buttonMode = false;
    this.introContainer.visible = false;
    this.exerciseContainer.visible = false;
    this.outroContainer.visible = true;
    this.fromOutro = 1;

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        self.fromOutro = 0;
        self.exerciseContainer.visible = true;
        self.cancelOutro();
        self.resetExercise();
        self.restartExercise();
    }

    this.outroAnimation();
}

House_2_3.prototype.resetExercise = function() {
    this.enableKeypad(true);
    this.door_closed_left.position.x = 588;
    this.door_closed_right.position.x = 1162;
    this.outroContainer.visible = false;
    this.exerciseContainer.visible = true;
    this.introContainer.visible = true;
    this.help.interactive = true;
    this.help.buttonMode = true;
};

/**
 * clean memory by clearing reference of object
 */
House_2_3.prototype.cleanMemory = function() {

    this.fromOutro = null;
    this.common = null;

    this.introId = null;
    this.outroId = null;

    if (this.assetsToLoad) this.assetsToLoad.length = 0;

    if (this.sceneBackground) {
        this.sceneBackground.clear();
        this.sceneBackground = null;
    }

    this.help = null;

    this.exerciseContainer.removeChildren();
    this.exerciseContainer = null;

    this.introInstance = null;
    this.timerId = null;

    this.introCancelHandler = null;

    this.cancelHandler = null;

    this.animationContainer.removeChildren();
    this.animationContainer = null;

    this.lillasparten_intro = null;
    this.mouths = null;

    if (this.hands) this.hands.length = 0;

    this.outroContainer.removeChildren();
    this.outroContainer = null;

    if (this.liftMasker) {
        this.liftMasker.clear();
        this.liftMasker = null;
    }

    this.mouthOutro = null;

    this.door_closed_left = null;
    this.door_closed_right = null;

    this.outroBackground = null;
    this.ovaigen = null;

    this.exerciseBaseContainer.removeChildren();
    this.exerciseBaseContainer = null;

    this.keyboardContainer.removeChildren();
    this.keyboardContainer = null;

    this.sidelift = null;
    this.boxTexture = null;

    if (this.boxColumn1Start) this.boxColumn1Start.length = 0;
    if (this.boxColumn2Start) this.boxColumn2Start.length = 0;

    this.boxDiff = null;

    wipe(this.params);
    this.params = null;

    if (this.whiteGraphics) {
        this.whiteGraphics.clear();
        this.whiteGraphics = null;
        this.whiteTexture = null;
    };

    this.keypad = null;

    if (this.circleGraphics) {
        this.circleGraphics.clear();
        this.circleGraphics = null;
        this.circleTexture = null;
    };

    if (this.keypad) this.keypad.length = 0;

    this.soundTexture = null;

    this.keyboardFirstRowStartPoint.length = 0;
    this.keyboardFirstRowGap = null;
    this.keyboardSecondRowStartPoint.length = 0;
    this.keyboardSecondRowGap = null;
    this.keyboardThirdRowStartPoint.length = 0;
    this.keyboardThirdRowGap = null;

    if (this.boxes) this.boxes.length = 0;

    if (this.lillaPicking) this.lillaPicking.length = 0;

    if (this.jsonData) this.jsonData.length = 0;

    this.currentMarkerAt = null;
    if (this.markers) this.markers.length = 0;

    this.answer = null;

    this.soundInstance = null;

    this.evaluationAnswer = null;

    if (this.wallSlices) this.wallSlices.length = 0;
    if (this.poolSlices) this.poolSlices.length = 0;

    wipe(this.pool);
    this.pool = null;

    this.view.removeEventListener("keydown", this.keyPressHandler, false);
    this.keyPressHandler = null;

    this.introContainer.removeChildren();
    this.introContainer = null;

    this.sceneContainer.removeChildren();
    this.sceneContainer = null;
}