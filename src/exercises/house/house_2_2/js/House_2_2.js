function House_2_2() {
    cl.show()

    PIXI.DisplayObjectContainer.call(this);

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position.set(77, 88);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();

    localStorage.setItem('house_2_2', 0);
    main.CurrentExercise = "house_2_2";

    main.texturesToDestroy = [];

    this.common = new Common();
    this.fromOutro = 0;

    this.loadSpriteSheet();

    // everything connected to this is rendered.
    this.addChild(this.sceneContainer);
}

House_2_2.constructor = House_2_2;
House_2_2.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
House_2_2.imagePath = 'build/exercises/house/house_2_2/images/';
House_2_2.audioPath = 'build/exercises/house/house_2_2/audios/';
House_2_2.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

/**
 * load array of assets of different format
 */
House_2_2.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(House_2_2.audioPath + 'intro');
    this.outroId = loadSound(House_2_2.audioPath + 'outro');

    // create an array of assets to load
    this.assetsToLoad = [
        House_2_2.imagePath + "sprite_2_2.json",
        House_2_2.imagePath + "sprite_2_2.png",
        House_2_2.imagePath + "outro_base.png",
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
House_2_2.prototype.spriteSheetLoaded = function() {

    // hide loader
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    // fill exercise background
    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0x9BD6F6);
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xf7c86b);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);

    // exercise instruction
    this.help = PIXI.Sprite.fromFrame("help.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);

    this.loadExercise();
    this.introScene();

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.outroContainer);

    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.addChild(this.exerciseContainer);

    this.sceneContainer.addChild(this.help);
}

/**
 * play intro animation
 */
House_2_2.prototype.introAnimation = function() {

    createjs.Sound.stop();
    main.overlay.visible = true;

    this.introContainer.visible = true;
    this.lift.visible = false;
    if (this.fromOutro == 1) {
        this.fromOutro = 0;
        this.resetExercise();
        this.restartExercise();

        this.lift.position = {
            x: 1633,
            y: 1065
        };
        this.exerciseContainer.visible = true;
    }

    this.enableKeypad(false);

    this.help.interactive = false;
    this.help.buttonMode = false;


    this.flats.visible = false;

    this.mouths.show();
    this.eyes.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};


/**
 * handle mouth, hand with specific sound positions or queue points.
 */
/*
0-2 sec arm_1 0
2-5 sec arm_2 1
5-7 sec arm_3 2
Stop at arm_1 0
 */

House_2_2.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    if (currentPostion >= 1000 && currentPostion < 2500) {
        this.showHandAt(0);
    } else if (currentPostion >= 2500 && currentPostion < 5500) {
        this.showHandAt(1);
    } else if (currentPostion >= 5500 && currentPostion < 7000) {
        this.showHandAt(2);
    } else {
        this.showHandAt(0);
    }
};

/**
 * cancel running intro animation
 */
House_2_2.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    this.introContainer.visible = false;
    this.lift.visible = true;
    this.flats.visible = true;
    this.mouths.hide(6);
    this.eyes.hide(2);
    this.help.interactive = true;
    this.help.buttonMode = true;

    this.enableKeypad(true);

    this.ovaigen.visible = false;
    main.overlay.visible = false;
}

House_2_2.prototype.outroAnimation = function() {


    var self = this;

    this.outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.mouthOutro.show();
    }
    this.outroCancelHandler = this.cancelOutro.bind(this);
    this.outroInstance.addEventListener("complete", this.outroCancelHandler);
}

House_2_2.prototype.cancelOutro = function() {

    var self = this;

    createjs.Sound.stop();
    self.mouthOutro.hide();
    self.ovaigen.visible = true;
    self.help.interactive = true;
    self.help.buttonMode = true;
    main.overlay.visible = false;

};


House_2_2.prototype.handVisible = function(index) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    };
    if (index !== undefined)
        this.hands[index].visible = true;
}

/**
 * create sprites,animation,sound related to intro scene
 */
House_2_2.prototype.introScene = function() {

    var self = this;

    this.introBg = new PIXI.Graphics();
    this.introBg.beginFill(0xe7f2f7);
    // set the line style to have a width of 5 and set the color to red
    this.introBg.lineStyle(1, 0x000000);
    // draw a rectangle
    this.introBg.drawRect(0, 0, 1843, 800);
    this.introContainer.addChild(this.introBg);

    this.lift_intro = new PIXI.Sprite.fromFrame('lift_intro.png');
    this.lift_intro.position = {
        x: 1333,
        y: 117
    };
    main.texturesToDestroy.push(this.lift_intro);
    this.introContainer.addChild(this.lift_intro);

    this.animationContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.animationContainer);

    this.eyes = new Animation({
        "image": "eyes",
        "length": 2,
        "x": 1555,
        "y": 325,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 1, 1, 1, 2, 2, 1],
        "frameId": 2
    });

    this.animationContainer.addChild(this.eyes);

    this.mouths = new Animation({
        "image": "mouth_intro_",
        "length": 4,
        "x": 1582,
        "y": 382,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 2, 4, 1],
        "frameId": 2
    });

    this.animationContainer.addChild(this.mouths);

    this.hands = [
        new PIXI.Sprite.fromFrame('arm_1.png'),
        new PIXI.Sprite.fromFrame('arm_2.png'),
        new PIXI.Sprite.fromFrame('arm_3.png'),
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(1373, 412);
        this.introContainer.addChild(this.hands[i]);
        this.hands[i].visible = false;
    }

    this.showHandAt(1);

    this.animationContainer.visible = true;

    this.introContainer.visible = false;

    this.outroScene();
}

House_2_2.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    }
    if (isNumeric(number)) {
        this.hands[number].visible = true;
        this.hands[number].bringToFront();
    }
};

/**
 * create sprites,animation,sound related to outro scene
 */
House_2_2.prototype.outroScene = function() {

    var self = this;

    this.outroContainer.visible = false;

    this.outroBackground = new PIXI.Sprite.fromImage(House_2_2.imagePath + "outro_base.png");
    this.outroContainer.addChild(this.outroBackground);

    this.eyesOutro = new Animation({
        "image": "eyes_outro_",
        "length": 2,
        "x": 797,
        "y": 253,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 1, 2, 2, 1, 1, 1, 1],
        "frameId": 0
    });

    this.outroContainer.addChild(this.eyesOutro);


    this.mouthOutro = new Animation({
        "image": "mouth_outro_",
        "length": 4,
        "x": 837,
        "y": 353,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 3, 2, 4, 1],
        "frameId": 0
    });

    this.outroContainer.addChild(this.mouthOutro);

    // back to intro page
    this.ovaigen = PIXI.Sprite.fromFrame('ovaigen.png');
    this.ovaigen.position.set(1204, 792);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.ovaigen.visible = false;
    this.outroContainer.addChild(this.ovaigen);
}

/**
 * load exercise related stuffs
 */
House_2_2.prototype.loadExercise = function() {


    this.exerciseBaseContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.addChild(this.exerciseBaseContainer);

    this.keyboardContainer = new PIXI.DisplayObjectContainer();
    this.keyboardContainer.position = {
        x: 150,
        y: 813
    };
    this.sceneContainer.addChild(this.keyboardContainer);


    this.flats = new PIXI.Sprite.fromFrame("flats.png");
    this.flats.position = {
        x: 1623,
        y: 3
    };

    this.exerciseBaseContainer.addChild(this.flats);

    this.lift = new PIXI.Sprite.fromFrame("lift.png");

    this.lift.position = {
        x: 1633,
        y: 1065
    };
    this.newLiftPosition = 1065;

    this.liftMoveDistance = 107;

    this.exerciseBaseContainer.addChild(this.lift);

    // position, size parameter for exercise
    this.params = {
        size: {
            white: {
                width: 240,
                height: 240
            },
            miniWhite: {
                width: 84,
                height: 84
            }
        },
        letters: [
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'å', 'ä', 'ö',
        ]
    }

    this.whiteGraphics = createRect({
        w: this.params.size.white.width,
        h: this.params.size.white.height
    });
    this.whiteTexture = this.whiteGraphics.generateTexture();

    this.keypad = new PIXI.Sprite.fromFrame('keyboard.png');
    this.keypad.position.set(0, 0);
    main.texturesToDestroy.push(this.keypad);
    this.keyboardContainer.addChild(this.keypad);

    this.circleGraphics = createCircle({
        radius: 50,
        color: 0x616161
    });

    this.circleTexture = this.circleGraphics.generateTexture();

    this.keypad = [];

    this.keyboardFirstRowStartPoint = [135, 0];
    this.keyboardFirstRowGap = 120;
    this.keyboardSecondRowStartPoint = [90, 95];
    this.keyboardSecondRowGap = 130;
    this.keyboardThirdRowStartPoint = [45, 182];
    this.keyboardThirdRowGap = 140;

    var alphabetsLen = this.params.letters.length + 1; //+1 for sudda
    for (var i = 0; i < alphabetsLen; i++) {
        var alphabetsSprite = new PIXI.Sprite(this.circleTexture);
        if (i >= 0 && i < 10) {
            alphabetsSprite.position.x = this.keyboardFirstRowStartPoint[0] + i * this.keyboardFirstRowGap;
            alphabetsSprite.position.y = this.keyboardFirstRowStartPoint[1];

        } else if (i >= 10 && i < 20) {
            alphabetsSprite.position.x = this.keyboardSecondRowStartPoint[0] + (i % 10) * this.keyboardSecondRowGap;
            alphabetsSprite.position.y = this.keyboardSecondRowStartPoint[1];

        } else if (i >= 20 && i < 30) {
            alphabetsSprite.position.x = this.keyboardThirdRowStartPoint[0] + (i % 20) * this.keyboardThirdRowGap;
            alphabetsSprite.position.y = this.keyboardThirdRowStartPoint[1];

        } else {
            alphabetsSprite.position.x = 0 + i * 10;
            alphabetsSprite.position.y = 0;
        }

        alphabetsSprite.alpha = 0;
        alphabetsSprite.interactive = true;
        alphabetsSprite.buttonMode = true;
        alphabetsSprite.index = i;
        this.keypad.push(alphabetsSprite);
        this.keyboardContainer.addChild(alphabetsSprite);
    };

    this.initObjectPooling();
}

House_2_2.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    };
    if (isNumeric(number))
        this.hands[number].visible = true;
}

/**
 * manage exercise with new sets randomized
 */
House_2_2.prototype.runExercise = function() {

    var self = this;

    this.jsonData = this.wallSlices[0];
    this.evaluationAnswer = "";

    this.currentMarkerAt = 0;
    this.markers = [];
    this.answer = this.jsonData.answer;

    var letters = this.jsonData.word.split(''),
        answer = this.jsonData.answer.split(''),
        lettersStartPos = [865 - (letters.length / 2 * 56 + letters.length / 2 * 25), 493]; //626

    this.markerStartPos = [865 - (letters.length / 2 * 56 + letters.length / 2 * 25), 645]; //626

    for (var j = 0; j < letters.length; j++) {

        var word = new PIXI.Text(letters[j], {
            'font': '80px Arial'
        });

        var frontPadding = (56 - word.width) / 2;

        word.position.x = lettersStartPos[0] + 56 * j + frontPadding + 25 * j;
        word.position.y = lettersStartPos[1]

        this.exerciseContainer.addChild(word);

        var marker = new PIXI.Text("_", {
            'font': '80px Arial'
        });

        marker.position.x = this.markerStartPos[0] + 56 * j + 25 * j;
        marker.position.y = this.markerStartPos[1]

        this.exerciseContainer.addChild(marker);
        this.markers.push(marker);

    };

    var whiteSprite = new PIXI.Sprite(this.whiteTexture),
        imageSprite = this.jsonData.image,
        soundSprite = new PIXI.Sprite.fromFrame("sound_grey.png");

    whiteSprite.position.set(726, 175);

    imageSprite.position.x = 0;
    imageSprite.position.y = 0;

    imageSprite.width = imageSprite.height = 240;

    soundSprite.position.x = 0;
    soundSprite.position.y = 190;
    soundSprite.interactive = true;
    soundSprite.buttonMode = true;
    soundSprite.howl = this.jsonData.sound;

    whiteSprite.addChild(imageSprite);
    whiteSprite.addChild(soundSprite);

    this.exerciseContainer.addChild(whiteSprite);
    this.whiteSprite = whiteSprite;

    soundSprite.click = soundSprite.tap = this.onSoundPressed.bind(soundSprite);

    for (var i = 0; i < this.keypad.length; i++) {
        this.onKeypadHit(i);
    }
    this.keyPressHandler = this.onKeyPress.bind(this);
    this.view.addEventListener("keydown", this.keyPressHandler, false);
    this.markers[this.currentMarkerAt].setStyle({
        'font': '80px Arial',
        'strokeThickness': 6
    });

}

House_2_2.prototype.onSoundPressed = function() {

    this.soundInstance = createjs.Sound.play(this.howl, {
        interrupt: createjs.Sound.INTERRUPT_NONE
    });
    this.soundInstance.addEventListener("failed", handleFailed);
    this.soundInstance.addEventListener("complete", handleComplete);
};

House_2_2.prototype.view = window;

House_2_2.prototype.onKeyPress = function(e) {
    var self = this;

    if (addedListeners) return false;
    if (e.shiftKey) return false;

    addedListeners = true;

    var code = e.charCode || e.keyCode,
        character = String.fromCharCode(code),
        keyChar = character.toLowerCase(),
        keyIndex = self.params.letters.indexOf(keyChar);

    if (code == 8) {
        keyIndex = self.keypad.length - 1;
        self.undoFeature();
        e.preventDefault();
    }

    if (isNumeric(keyIndex) && keyIndex >= 0 && keyIndex < self.keypad.length - 1) {
        if (self.currentMarkerAt < self.markers.length) {
            self.keypad[keyIndex].alpha = 0.6;
            setTimeout(function() {
                self.keypad[keyIndex].alpha = 0;
            }, 100)
        }
        self.onKeyClickTap(keyIndex);
    } else {
        addedListeners = false;
    }
}

House_2_2.prototype.undoFeature = function() {

    if (this.currentMarkerAt > 0) {
        this.markers[this.currentMarkerAt].setStyle({
            'font': '80px Arial'
        });
        this.currentMarkerAt--;
        this.markers[this.currentMarkerAt].setStyle({
            'font': '80px Arial',
            'strokeThickness': 6
        });
        this.markers[this.currentMarkerAt].setText("_");
        this.markers[this.currentMarkerAt].position.x = this.markerStartPos[0] + 56 * this.currentMarkerAt + 25 * this.currentMarkerAt;
        // this.markers[this.currentMarkerAt].position.y += 65;
        this.evaluationAnswer = this.evaluationAnswer.substr(0, this.evaluationAnswer.length - 1);
    }
}

/**
 * perform task on keypad on click/tap
 * @param  {integer} i
 */
House_2_2.prototype.onKeypadHit = function(i) {
    var self = this,
        key = this.keypad[i];

    key.click = key.tap = function(data) {
        if (this.index >= self.keypad.length - 1) {
            self.undoFeature();
        } else {
            self.onKeyClickTap(this.index);
        }
    }

    key.mousedown = key.touchstart = function(data) {
        if (self.currentMarkerAt < self.markers.length)
            this.alpha = 0.6;
    }

    key.mouseup = key.mouseupoutside = key.touchend = key.touchendoutside = function(data) {
        this.alpha = 0;
    }

}

House_2_2.prototype.onKeyClickTap = function(index) {
    var self = this;

    self.markers[self.currentMarkerAt].setStyle({
        'font': '80px Arial'
    });
    self.markers[self.currentMarkerAt].setText("" + self.params.letters[index]);
    // frontPadding = (56 - self.markers[self.currentMarkerAt].width) / 2;
    // self.markers[self.currentMarkerAt].position.x = self.markerStartPos[0] + 56 * self.currentMarkerAt + frontPadding + 25 * self.currentMarkerAt;
    // self.markers[self.currentMarkerAt].position.y -= 65;

    // self.markers[self.currentMarkerAt].position.y -= 65;

    self.evaluationAnswer += self.params.letters[index];

    self.evaluateRound();
    addedListeners = false;
};

House_2_2.prototype.evaluateRound = function() {

    var self = this;

    self.currentMarkerAt++;
    if (self.currentMarkerAt < self.markers.length - 1) {
        self.markers[self.currentMarkerAt].setStyle({
            'font': '80px Arial',
            'strokeThickness': 6
        });
    }
    if (self.currentMarkerAt == self.markers.length) {
        if (self.currentMarkerAt === self.markers.length) {
            self.enableKeypad(false);
            setTimeout(function() {
                for (var i = 0; i < self.keypad.length; i++) {
                    self.keypad[i].alpha = 0;
                }
                if (self.evaluationAnswer == self.answer) {
                    correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                    self.showGreenAnswer();
                    self.correctFeedback();
                } else {
                    self.wrongFeedback();
                }
            }, 1000)
        };

    }
}

/**
 * for correction only the letters that are wrong should go down
 */
House_2_2.prototype.wrongFeedback = function() {

    var self = this;

    wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);

    var arrEvaluationAnswer = this.evaluationAnswer.split(''),
        arrActualAnswer = this.answer.split(''),
        arrAnswer = [],
        arrMarkers = [];

    for (var i = 0; i < arrEvaluationAnswer.length; i++) {
        if (arrEvaluationAnswer[i] !== arrActualAnswer[i]) {
            arrMarkers.push(this.markers[i]);
            // this.markers[i].position.x = this.markerStartPos[0] + 56 * i + 25 * i;
            arrAnswer.push(arrActualAnswer[i]);
        }
    };

    this.markers.length = 0;
    this.markers = arrMarkers;

    for (var i = this.markers.length - 1; i >= 0; i--) {
        this.markers[i].setStyle({
            'font': '80px Arial',
            'fill': '#dc3732'
        });
    };

    setTimeout(function() {
        for (var i = self.markers.length - 1; i >= 0; i--) {
            self.markers[i].setStyle({
                'font': '80px Arial',
                'fill': '#000000'
            });
            self.markers[i].setText("_");
            // self.markers[i].position.y += 65;

        };
        self.markerStartPos.length = 0;
        self.markerStartPos[0] = self.markers[0].position.x;
        self.markerStartPos[1] = self.markers[0].position.y;

        self.currentMarkerAt = 0;
        self.markers[self.currentMarkerAt].setStyle({
            'font': '80px Arial',
            'strokeThickness': 6
        });

        self.evaluationAnswer = "";
        self.answer = "";
        self.answer = arrAnswer.join('');

        self.enableKeypad(true);
        addedListeners = false;
    }, 1000);

};

House_2_2.prototype.showGreenAnswer = function() {
    for (var i = this.markers.length - 1; i >= 0; i--) {
        this.markers[i].setStyle({
            'font': '80px Arial',
            'fill': '#009640'
        });
    };
}

House_2_2.prototype.showRedAnswer = function() {
    var self = this;
    for (var i = this.markers.length - 1; i >= 0; i--) {
        this.markers[i].setStyle({
            'font': '80px Arial',
            'fill': '#dc3732'
        });
    };
    setTimeout(function() {
        for (var i = self.markers.length - 1; i >= 0; i--) {
            self.markers[i].setStyle({
                'font': '80px Arial',
                'fill': '#000000'
            });
        };
    }, 1000);
}

/**
 * toggle keypad enable/disable
 */
House_2_2.prototype.enableKeypad = function(bool) {

    if (bool) {
        for (var i = 0; i < this.keypad.length; i++) {
            this.keypad[i].interactive = true;
            this.keypad[i].buttonMode = true;
        }
    } else {
        for (var i = 0; i < this.keypad.length; i++) {
            this.keypad[i].interactive = false;
            this.keypad[i].buttonMode = false;
        }
    }


}

/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
House_2_2.prototype.correctFeedback = function() {

    var self = this;

    self.newLiftPosition -= self.liftMoveDistance;

    function moveLift() {
        if (self.lift.position.y > self.newLiftPosition) {
            self.lift.position.y -= 10;
            requestAnimFrame(moveLift);
        }
    }

    requestAnimFrame(moveLift);

    setTimeout(function() {
        self.enableKeypad(true);
        self.currentMarkerAt = 0;
        self.evaluationAnswer = "";

        var feedback = localStorage.getItem('house_2_2');

        feedback++;
        localStorage.setItem('house_2_2', feedback);

        if (feedback >= House_2_2.totalFeedback) {
            setTimeout(self.onComplete.bind(self), 1000);
        } else {
            addedListeners = false;
            self.view.removeEventListener("keydown", self.keyPressHandler, false);
            self.getNewExerciseSet();
        }
    }, 1000);

}


/**
 * prefetch exercise data and cache number of sets defined only.
 */
House_2_2.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/house/house_2_2.json?nocache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        self.pool = new ExerciseSpritesPool(loader.json);
        //borrow two sprites once
        self.generatePoolCategory();
        self.borrowWallSprites(2);
        self.runExercise();

    });
    loader.load();
}

House_2_2.prototype.generatePoolCategory = function() {

    var keyArr = [];

    var spriteArr = this.pool.sprites,
        spriteLen = spriteArr.length;

    for (var i = 0; i < spriteLen; i++) {
        keyArr.push(spriteArr[i][0].word[0]);
    };

    this.poolCategory = keyArr.filter(onlyUnique);

}

House_2_2.prototype.borrowWallSprites = function(num) {
    var rand, category, sprite, spriteObj;
    var self = this;

    for (var i = 0; i < num; i++) {
        rand = makeUniqueRandom(this.poolCategory.length);
        this.borrowCategory = this.poolCategory[rand];



        sprite = this.pool.borrowUniqueSprite(this.borrowCategory, this.lastBorrowSprite);
        spriteObj = sprite[0];

        this.poolSlices.push(sprite);

        this.lastBorrowSprite = spriteObj.word[0];
        var imageSprite = PIXI.Sprite.fromImage('uploads/images/' + spriteObj.image);
        var soundHowl = loadSound('uploads/audios/' + StripExtFromFile(spriteObj.sound));

        this.wallSlices.push({
            image: imageSprite,
            answer: spriteObj.answer,
            sound: soundHowl,
            word: spriteObj.word
        });
    }
};

House_2_2.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];

    this.pool.returnSprite(sprite);

    this.poolSlices = [];
    this.wallSlices.shift();
};

House_2_2.prototype.getNewExerciseSet = function() {
    this.exerciseContainer.removeChildren();

    this.returnWallSprites();
    this.borrowWallSprites(1);
    this.runExercise();
}

House_2_2.prototype.restartExercise = function() {
    this.newLiftPosition = 1059;

    this.animationContainer.visible = true;

    localStorage.setItem('house_2_2', 0);
    this.outroContainer.visible = false;
    this.introMode = true;


    this.view.removeEventListener("keydown", this.keyPressHandler, false);

    this.getNewExerciseSet();

};

/**
 * the outro scene is displayed
 */
House_2_2.prototype.onComplete = function() {

    var self = this;

    this.introMode = false;

    this.help.interactive = false;
    this.help.buttonMode = false;
    this.introContainer.visible = false;
    this.exerciseContainer.visible = false;
    this.outroContainer.visible = true;
    this.fromOutro = 1;

    this.lift.position.y = 1065;

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        self.fromOutro = 0;
        self.exerciseContainer.visible = true;
        self.cancelOutro();
        self.resetExercise();
        self.restartExercise();
    }

    this.outroAnimation();

}

House_2_2.prototype.resetExercise = function() {
    this.ovaigen.visible = false;
    this.outroContainer.visible = false;

    this.introMode = true;
};

/**
 * clean memory by clearing reference of object
 */
House_2_2.prototype.cleanMemory = function() {

    this.common = null;
    this.fromOutro = null;

    this.introId = null;
    this.outroId = null;

    if (this.assetsToLoad) this.assetsToLoad.length = 0;

    if (this.sceneBackground) this.sceneBackground.clear();
    this.sceneBackground = null;

    this.help = null;

    this.exerciseContainer.removeChildren();
    this.exerciseContainer

    this.lift = null;
    this.flats = null;
    this.introInstance = null;
    this.timerId = null;
    this.introCancelHandler = null;

    this.outroInstance = null;
    this.outroCancelHandler = null;

    if (this.introBg) this.introBg.clear();
    this.introBg = null;

    this.lift_intro = null;
    this.eyes = null;
    this.mouths = null;

    if (this.hands) this.hands.length = 0;
    this.outroBackground = null;
    this.eyesOutro = null;

    this.mouthOutro = null;
    this.ovaigen = null;

    this.exerciseBaseContainer.removeChildren();
    this.exerciseBaseContainer = null;

    this.keyboardContainer.removeChildren();
    this.keyboardContainer = null;

    this.flats = null;
    this.newLiftPosition = null;
    this.liftMoveDistance = null;

    wipe(this.params);
    this.params = null;

    if (this.whiteGraphics) this.whiteGraphics.clear();
    this.whiteGraphics = null;

    this.whiteTexture = null;

    if (this.circleGraphics) {
        this.circleGraphics.clear();
        this.circleGraphics = null;
        this.circleTexture = null;
    };

    if (this.keypad) this.keypad.length = 0;

    this.keyboardFirstRowStartPoint.length = 0
    this.keyboardFirstRowGap = null;
    this.keyboardSecondRowStartPoint.length = 0
    this.keyboardSecondRowGap = null;
    this.keyboardThirdRowStartPoint.length = 0
    this.keyboardThirdRowGap = null;

    if (this.jsonData) this.jsonData.length = 0;
    this.evaluationAnswer = null;
    this.currentMarkerAt = null;
    if (this.markers) this.markers.length = 0;
    this.answer = null;

    if (this.markerStartPos) this.markerStartPos.length = 0;

    this.soundInstance = null;
    this.newLiftPosition = null;

    if (this.wallSlices) this.wallSlices.length = 0;
    if (this.poolSlices) this.poolSlices.length = 0;

    wipe(this.pool);
    this.pool = null;

    this.view.removeEventListener("keydown", this.keyPressHandler, false);
    this.keyPressHandler = null;


    this.introContainer.removeChildren();
    this.introContainer = null;

    this.sceneContainer.removeChildren();
    this.sceneContainer = null;
}