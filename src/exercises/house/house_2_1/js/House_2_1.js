/**
 * house_2_1 intro
 */
function House_2_1() {
    cl.show();

    PIXI.DisplayObjectContainer.call(this);
    main.CurrentExercise = "house_2_1";
    //ceate empty container to store all alfabetet scene stuffs
    this.baseContainer = new PIXI.DisplayObjectContainer();

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    //ceate empty container to store all alfabetet scene stuffs
    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();

    this.exerciseContainer = new PIXI.DisplayObjectContainer();

    //create empty menu container
    this.menuContainer = new PIXI.DisplayObjectContainer();

    this.commonMenuContainer = new PIXI.DisplayObjectContainer();


    localStorage.removeItem('house_2_1');
    this.fromOutro = 0;

    main.texturesToDestroy = [];

    this.sceneContainer.position = {
        x: 77,
        y: 88
    };
    this.baseContainer.addChild(this.sceneContainer);
    this.loadSpriteSheet();
    this.addChild(this.baseContainer);

}

House_2_1.constructor = House_2_1;
House_2_1.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

House_2_1.imagePath = 'build/exercises/house/house_2_1/images/';
House_2_1.audioPath = 'build/exercises/house/house_2_1/audios/';
House_2_1.commonFolder = 'build/common/images/house/';
House_2_1.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

House_2_1.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(House_2_1.audioPath + 'intro');
    this.outroId = loadSound(House_2_1.audioPath + 'outro');

    // create an array of assets to load
    var assetsToLoad = [
        House_2_1.imagePath + "base.png",
        House_2_1.imagePath + "intropopup.png",
        House_2_1.imagePath + "sprite_2_1.json",
        House_2_1.imagePath + "sprite_2_1.png",
        House_2_1.imagePath + "elevator.png",
        House_2_1.imagePath + "outrobase.png",
        House_2_1.imagePath + "feedback_outro_1.png",
        House_2_1.imagePath + "feedback_outro_2.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

};

House_2_1.prototype.spriteSheetLoaded = function() {

    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF); //d7ead3

    this.loadDefaults();
};



House_2_1.prototype.loadDefaults = function() {

    var self = this;

    this.sceneBackground = new PIXI.Sprite.fromImage(House_2_1.imagePath + "base.png"); // need to change the image with the rectangle

    this.sceneContainer.addChild(this.sceneBackground);

    //this.sceneBackground.drawRect(0, 0, 1843, 431);

    this.exerciseBackground = new PIXI.Sprite.fromImage(House_2_1.imagePath + "elevator.png");
    this.exerciseBackground.position = {
        x: 460,
        y: 0
    };

    this.sceneBackgroundTopWhite = new PIXI.Graphics();
    this.sceneBackgroundTopWhite.beginFill(0xFFFFFF); //9ecea0
    this.sceneBackgroundTopWhite.drawRect(0, 0, 1843, 88);
    this.sceneBackgroundTopWhite.position.y = -88;

    this.exerciseBackground.visible = false;
    this.exerciseContainer.addChild(this.exerciseBackground);
    this.sceneContainer.addChild(this.exerciseContainer);
    this.sceneContainer.addChild(this.sceneBackgroundTopWhite);
    this.sceneContainer.addChild(this.introContainer);

    // girl with arms crossed
    this.lilla = new PIXI.Sprite.fromFrame('lilla.png');
    this.lilla.position = {
        x: 1353,
        y: 596
    };

    this.intropopup = new PIXI.Sprite.fromImage(House_2_1.imagePath + "intropopup.png");
    this.intropopup.position = {
        x: 1084,
        y: 619
    };

    this.groupPeopleIntro = new PIXI.Sprite.fromFrame("feedback_intro.png");
    this.groupPeopleIntro.position = {
        x: 1,
        y: 300
    };

    main.texturesToDestroy.push(this.lilla);

    this.introContainer.addChild(this.groupPeopleIntro);
    this.introContainer.addChild(this.intropopup);
    this.introContainer.addChild(this.lilla);
    this.help = PIXI.Sprite.fromFrame("help.png");
    this.help.position = {
        x: 0,
        y: 0
    };

    this.help.buttonMode = true;
    this.help.interactive = true;
    this.baseContainer.addChild(this.help);
    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.back_again = PIXI.Sprite.fromFrame("ovaigen.png");
    this.back_again.position = {
        x: 74,
        y: 628
    };
    this.back_again.buttonMode = true;
    this.back_again.interactive = true;
    this.outroContainer.addChild(this.back_again);
    this.back_again.visible = false;

    this.mouthParams = {
        "image": "mouth",
        "length": 5,
        "x": 1432,
        "y": 913,
        "speed": 6,
        "pattern": true,
        "frameId": 1
    };
    this.mouth = new Animation(this.mouthParams);
    this.introContainer.addChild(this.mouth);
    this.mouth.gotoAndStop(5);

    //the pattern of the eye needs to change less than mouth
    this.eyeParams = {
        "image": "eyes",
        "length": 2,
        "x": 1408,
        "y": 815,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1],
        "frameId": 1
    };

    this.eye = new Animation(this.eyeParams);
    this.introContainer.addChild(this.eye);
    this.eye.gotoAndStop(1);

    this.hands = [
        new PIXI.Sprite.fromFrame('arm_rest.png'),
        new PIXI.Sprite.fromFrame('arm1.png'),
        new PIXI.Sprite.fromFrame('arm2.png'),
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(1068, 668);
        this.introContainer.addChild(this.hands[i]);
        this.hands[i].visible = false;
    }

    this.showHandAt(0);

    this.outroAnim = new Animation({
        "image": House_2_1.imagePath + "feedback_outro_",
        "length": 2,
        "x": 493,
        "y": 250,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 1, 2, 2, 1, 1, 2, 1, 2, 1, 2, 1, 2, 1, 1, 2, 1, 1, 2, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 1, 2, 2, 1],
        "frameId": 1
    });
    this.exerciseContainer.addChild(this.outroAnim);
    this.outroAnim.visible = false;

    this.outroOvaigenMouth = new Animation({
        "image": "outro_mouth_",
        "length": 2,
        "x": 1104,
        "y": 664,
        "speed": 6,
        "frameId": 0
    });
    this.exerciseContainer.addChild(this.outroOvaigenMouth);
    this.outroOvaigenMouth.visible = false;

    this.sceneContainer.addChild(this.outroContainer);
    this.outroContainer.visible = false;
    this.introContainer.visible = false;

    this.moveLiftHandler = this.moveLift.bind(this);

    this.loadExercise();
    this.loadGameJson();
};

House_2_1.prototype.loadExercise = function() {
    this.peopleArr = [];
    person = new PIXI.Sprite.fromFrame("fb5.png")
    person.correctPosition = {
        x: 1061,
        y: 256
    };
    this.peopleArr.push(person);
    person = new PIXI.Sprite.fromFrame("fb6.png")
    person.correctPosition = {
        x: 907,
        y: 304
    };
    this.peopleArr.push(person);
    person = new PIXI.Sprite.fromFrame("fb8.png")
    person.correctPosition = {
        x: 754,
        y: 386
    };
    this.peopleArr.push(person);
    person = new PIXI.Sprite.fromFrame("fb9.png")
    person.correctPosition = {
        x: 666,
        y: 457
    };

    this.peopleArr.push(person);
    person = new PIXI.Sprite.fromFrame("fb10.png")
    person.correctPosition = {
        x: 492,
        y: 444
    };
    this.peopleArr.push(person);
    person = new PIXI.Sprite.fromFrame("fb7.png")
    person.correctPosition = {
        x: 788,
        y: 529
    };
    this.peopleArr.push(person);
    person = new PIXI.Sprite.fromFrame("fb3.png")
    person.correctPosition = {
        x: 1158,
        y: 400
    };
    this.peopleArr.push(person);
    person = new PIXI.Sprite.fromFrame("fb4.png")
    person.correctPosition = {
        x: 1009,
        y: 553
    };
    this.peopleArr.push(person);
    person = new PIXI.Sprite.fromFrame("fb2.png")
    person.correctPosition = {
        x: 1345,
        y: 472
    };

    this.peopleArr.push(person);
    person = new PIXI.Sprite.fromFrame("fb1.png")
    person.correctPosition = {
        x: 1535,
        y: 480
    };
    this.peopleArr.push(person);

    this.feedBackBtnTexture = new PIXI.Texture.fromFrame("feedback_button.png");
    this.speechBubbleTexture = new PIXI.Texture.fromFrame("speechbubble.png");
    wordCardGraphic = new PIXI.Graphics();
    wordCardGraphic.beginFill(0xFFFFFF); //9ecea0
    wordCardGraphic.lineStyle(1, 0x000000);
    wordCardGraphic.drawRect(0, 0, 413, 80);
    this.wordCardTexture = wordCardGraphic.generateTexture();

    correctWordCardGraphic = new PIXI.Graphics();
    correctWordCardGraphic.beginFill(0x009640); //9ecea0
    correctWordCardGraphic.lineStyle(1, 0x009640);
    correctWordCardGraphic.drawRect(0, 0, 413, 80);
    this.correctWordCardTexture = correctWordCardGraphic.generateTexture();

    wrongWordCardGraphic = new PIXI.Graphics();
    wrongWordCardGraphic.beginFill(0xff6666); //9ecea0
    wrongWordCardGraphic.lineStyle(1, 0xff4444);
    wrongWordCardGraphic.drawRect(0, 0, 413, 80);
    this.wrongWordCardTexture = wrongWordCardGraphic.generateTexture();

}


House_2_1.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    }
    if (isNumeric(number))
        this.hands[number].visible = true;
};

/**
 * play intro animation
 */
House_2_1.prototype.introAnimation = function() {

    if (this.requestId)
        cancelAnimationFrame(this.requestId);

    createjs.Sound.stop();
    main.overlay.visible = true;

    if (this.fromOutro == 1) {
        this.restartExercise();
        clearTimeout(this.changeQuestionTimeout);
    }

    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    this.hideExerciseStuffForIntro(false);

    this.help.interactive = false;
    this.help.buttonMode = false;
    this.eye.show();

    this.mouth.show();
    this.eye.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
0-1 sec arm_0 =>arm_rest.png => 0
1-4 sec arm_1 =>arm1.png => 1
4-5 sec arm_0  =>arm_rest.png =>0
5-7 sec arm_2 =>arm2.png =>2
7-stop arm_0  =>arm_rest.png =>0

 */
House_2_1.prototype.animateIntro = function() {

    var currentPosition = this.introInstance.getPosition();
    if (currentPosition >= 1000 && currentPosition < 4000) {
        this.showHandAt(1);
    } else if (currentPosition >= 4000 && currentPosition < 5000) {
        this.showHandAt(0);
    } else if (currentPosition >= 5000 && currentPosition < 7000) {
        this.showHandAt(2);
    } else {
        this.showHandAt(0);
    }
};

/**
 * cancel running intro animation
 */
House_2_1.prototype.cancelIntro = function() {

    var self = this;
    this.hideExerciseStuffForIntro(true);
    createjs.Sound.stop();

    clearInterval(this.timerId);
    this.mouth.hide(5);
    this.eye.hide(1);
    this.showHandAt(0);

    this.help.interactive = true;
    this.help.buttonMode = true;

    addedListeners = false;

    this.qBubble.visible = true;

    self.introContainer.visible = false;

    if (this.fromOutro == 1) {
        this.changeQuestionTimeout = setTimeout(this.changeQuestion.bind(this), 800);
        this.fromOutro = 0;
    }

    this.back_again.visible = false;
    main.overlay.visible = false;
}

House_2_1.prototype.outroAnimation = function() {

    var self = this;

    this.outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {

        self.outroAnim.show();
        main.overlay.visible = true;

        this.outroMouthTimeout = setTimeout(function() {
            self.outroAnim.hide(0);
            self.outroOvaigenMouth.show();
        }, 3200);

        if (self.timerId) {
            clearInterval(self.timerId);
            self.timerId = null;
        }
    }

    this.outroCancelHandler = this.cancelOutro.bind(this);

    this.outroInstance.addEventListener("complete", this.outroCancelHandler);

};

House_2_1.prototype.moveLift = function() {

    if (this.exerciseContainer.position.y > -1200) {

        this.exerciseContainer.position.y -= 10;
        this.requestId = requestAnimFrame(this.moveLiftHandler);

    }
};

House_2_1.prototype.cancelOutro = function() {

    createjs.Sound.stop();

    this.requestId = requestAnimFrame(this.moveLiftHandler);

    /* this.mouth.hide(0);
     this.eye.hide(1);*/
    // this.outroAnim.hide(0);
    this.outroOvaigenMouth.hide(0);

    clearInterval(this.timerId);
    this.timerId = null;

    this.help.interactive = true;
    this.help.buttonMode = true;

    this.back_again.visible = true;
    main.overlay.visible = false;
};

House_2_1.prototype.clearOutroScene = function() {


    localStorage.setItem('house_2_1', 0);
    this.resetExercise();

    this.sceneBackground.texture = PIXI.Texture.fromImage(House_2_1.imagePath + "base.png");
    this.cardContainer.visible = true;
    this.feedbackContainer.visible = true;
    this.exerciseBackground.visible = false;
    this.exerciseContainer.position.y = 0;

    this.exerciseContainer.removeChildren();

    this.exerciseContainer.addChild(this.exerciseBackground);

    this.exerciseContainer.addChild(this.outroAnim);
    this.exerciseContainer.addChild(this.outroOvaigenMouth);

    this.outroOvaigenMouth.visible = false;

    clearTimeout(this.outroMouthTimeout);

    this.outroAnim.visible = false;
};


House_2_1.prototype.restartExercise = function() {

    this.clearOutroScene();

    this.runExercise();
};

House_2_1.prototype.onComplete = function() {

    var self = this;

    this.qBubble.visible = false;

    this.introMode = false;
    for (var k = 0; k < this.peopleArr.length; k++) {
        this.peopleArr[k].visible = false;
    }

    localStorage.removeItem('house_2_1');


    this.introContainer.visible = false;

    this.help.interactive = false;
    this.help.buttonMode = false;
    this.fromOutro = 1;
    this.outroOvaigenMouth.visible = true;
    this.outroAnim.visible = true;
    this.outroContainer.visible = true;

    this.sceneBackground.texture = PIXI.Texture.fromImage(House_2_1.imagePath + "outrobase.png");
    this.cardContainer.visible = false;
    this.feedbackContainer.visible = false;
    this.exerciseBackground.visible = true;

    this.back_again.click = this.back_again.tap = function(data) {

        if (self.requestId)
            cancelAnimationFrame(self.requestId);

        self.fromOutro = 0;
        self.restartExercise();
    };

    this.outroAnimation();

};

House_2_1.prototype.resetExercise = function() {

    var self = this;

    createjs.Sound.stop();
    this.outroContainer.visible = false;
    this.back_again.visible = false;

    this.sceneBackground.texture = PIXI.Texture.fromImage(House_2_1.imagePath + "base.png");
    this.exerciseContainer.position.y = 0;
    this.exerciseBackground.visible = false;

    this.mouth.hide(5);
    this.eye.hide(1);
    this.showHandAt(0);

    this.help.interactive = true;
    this.help.buttonMode = true;

    this.introMode = true;

    addedListeners = false;
};

House_2_1.prototype.createSound = function(filename) {
    var audio = filename.toLowerCase();
    audio = audio.replace("å", "_ao_");
    audio = audio.replace("ä", "_ae_");
    audio = audio.replace("ö", "_oe_");
    return StripExtFromFile(audio);
}

House_2_1.prototype.runExercise = function() {
    var self = this;

    this.questionBubble = [];
    this.questionSoundArray = [];

    this.answerCards = [];

    this.shuffledPeopleNo = [
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9
    ];
    shuffle(this.shuffledPeopleNo);

    var cardPositionStart = [1390, 55]; //Bottom point, need to start from bottom cause the top parts needs to be overlapped by nextblock

    var cardHeight = 82; // add 10px for gap between cards


    var randomSet = Math.floor((Math.random() * this.gameSets.length));

    var jsonData = this.gameSets[randomSet];

    for (var i = 0; i < jsonData.question.length; i++) {
        //console.log(this.cubeTexture);

        var questionHowl = loadSound('uploads/audios/house_2_1_audios/' + self.createSound(jsonData.answer[i]));
        this.questionSoundArray.push(questionHowl);

        //Card part the right side
        var whiteCard = new PIXI.Sprite(this.wordCardTexture);

        this.answerCards[i] = whiteCard;

        var cardText = new PIXI.Text(" " + jsonData.answer[i] + " ", {
            font: "40px Arial",
            fill: "black",
            align: "center" // align center does not work for single line text http://www.goodboydigital.com/pixijs/docs/classes/Text.html
        });
        cardText.position = {
            x: 20,
            y: 28
        };
        whiteCard.addChild(cardText);
        main.texturesToDestroy.push(cardText);

        whiteCard.answerId = i;

        whiteCard.position = {
            x: cardPositionStart[0],
            y: cardPositionStart[1] + i * (cardHeight + 10) // +10 is for the gap between cards.
        };

        whiteCard.interactive = true;
        whiteCard.buttonMode = true;

        main.texturesToDestroy.push(whiteCard);

        //Creating alphabetCards
        questionBubble = new PIXI.Sprite(this.speechBubbleTexture);
        ans = jsonData.question[i].split(" ");
        ansWord = ans[ans.length - 1];
        ansSentence = "";
        for (var j = 0; j < ans.length - 1; j++) {
            ansSentence += ans[j] + " ";
        };
        questionBubble.answerId = i;
        questionBubble.answerText = ansSentence;
        questionBubble.answerWord = ansWord;

        questionText = new PIXI.Text(" " + jsonData.question[i] + " ", {
            font: "50px Arial",
            fill: "black",
            wordWrap: true,
            wordWrapWidth: 250,
        });
        questionText.position = {
            x: 60,
            y: 60
        };

        questionBubble.addChild(questionText);

        this.questionBubble[i] = questionBubble;

        this.questionBubble[i].position = {
            x: 233,
            y: 60
        };
    }

    shuffle(this.answerCards);

    this.feedbackContainer = new PIXI.DisplayObjectContainer();
    this.exerciseContainer.addChild(this.feedbackContainer);

    this.cardContainer = new PIXI.DisplayObjectContainer();
    this.exerciseContainer.addChild(this.cardContainer);

    for (var i = this.answerCards.length - 1; i >= 0; i--) {
        this.answerCards[i].position = {
            x: cardPositionStart[0],
            y: cardPositionStart[1] + i * (cardHeight + 10) // +10 is for the gap between cards.
        };
        this.onCardSelect(i);
        this.cardContainer.addChild(this.answerCards[i]);
    };

    this.changeQuestionTimeout = setTimeout(this.changeQuestion.bind(this), 800);

};

House_2_1.prototype.hideExerciseStuffForIntro = function(visibility) {
    for (var k = 0; k < this.peopleArr.length; k++) {
        this.peopleArr[k].visible = visibility;
    }
    this.questionBubble[this.currentBubble].visible = visibility;
}

House_2_1.prototype.onCardSelect = function(i) {

    var self = this;
    this.answerCards[i].click = this.answerCards[i].tap = function(data) {
        if (addedListeners) return;
        addedListeners = true;
        // console.log(self.answerCards[i].answerId);
        // console.log(self.currentAnswer);
        if (self.answerCards[i].answerId == self.currentAnswer) {
            self.answerCards[i].texture = self.correctWordCardTexture;
            self.answerCards[i].interactive = false;
            self.answerCards[i].buttonMode = false;
            correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
            self.questionBubble[self.currentAnswer].getChildAt(0).setText(" " + self.questionBubble[self.currentAnswer].answerText + " ");
            questionWord = new PIXI.Text(" " + self.questionBubble[self.currentAnswer].answerWord + " ", {
                font: "bold 50px Arial",
                fill: "green",
                wordWrap: true,
                wordWrapWidth: 400,
            });
            questionWord.position = {
                x: 50,
                y: 120
            };
            self.questionBubble[self.currentAnswer].addChild(questionWord);
            setTimeout(self.correctFeedback.bind(self), 1000);
        } else {
            wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
            self.answerCards[i].texture = self.wrongWordCardTexture;
            setTimeout(function() {
                self.answerCards[i].texture = self.wordCardTexture;
            }, 1000);
            addedListeners = false;
        }
    }
}

House_2_1.prototype.pushInsideLift = function() {

    var self = this;

    self.questionBubble[self.currentAnswer].visible = false;

    self.askingPerson.position = self.askingPerson.correctPosition;

    setTimeout(function() {
        self.cardContainer.bringToFront();
    }, 500);
};

House_2_1.prototype.playSound = function(soundToPlay) {
    createjs.Sound.stop();
    this.soundInstance = createjs.Sound.play(soundToPlay, {
        interrupt: createjs.Sound.INTERRUPT_NONE
    });
    this.soundInstance.addEventListener("failed", handleFailed);
    this.soundInstance.addEventListener("complete", handleComplete);
};

House_2_1.prototype.changeQuestion = function() {
    var self = this;
    var num = this.shuffledPeopleNo.shift();
    this.currentBubble = num;
    this.qBubble = this.questionBubble[num];
    this.qBubble.visible = true;
    this.askingPerson = this.peopleArr[num];

    //create bubble sound icon
    var bubbleSound = PIXI.Sprite.fromFrame("sound_grey.png");
    bubbleSound.position.set(180, 200);
    bubbleSound.interactive = true;
    bubbleSound.buttonMode = true;
    this.qBubble.addChild(bubbleSound);
    bubbleSound.click = bubbleSound.tap = function() {
        self.playSound(self.questionSoundArray[num]);
    }

    this.askingPerson.position = {
        x: 101,
        y: 340
    };
    this.askingPerson.visible = true;
    this.currentAnswer = this.qBubble.answerId;
    this.exerciseContainer.addChild(this.askingPerson);
    this.exerciseContainer.addChild(this.qBubble);

    for (i = num; i < this.peopleArr.length; i++) {
        this.peopleArr[i].bringToFront();
    }

    this.cardContainer.bringToFront();

}

House_2_1.prototype.updateOnCorrect = function() {
    var feedback = localStorage.getItem('house_2_1');
    feedbackBtnStartPoint = [1240, 408];
    feedbackBtnGap = 5;
    feedBackBtn = new PIXI.Sprite(this.feedBackBtnTexture);
    feedBackBtn.position = {
        x: feedbackBtnStartPoint[0],
        y: feedbackBtnStartPoint[1] - (18 * feedback) - (feedback * feedbackBtnGap) //18 is height of feedback btn
    };
    this.feedbackContainer.addChild(feedBackBtn);
    feedback++;
    localStorage.setItem('house_2_1', feedback);
    if (feedback >= House_2_1.totalFeedback) {
        setTimeout(this.onComplete.bind(this), 1000);
    } else {
        this.changeQuestion();
    }
    addedListeners = false;
};


House_2_1.prototype.correctFeedback = function(fileId) {

    var self = this;

    setTimeout(self.pushInsideLift.bind(self), 1000);
    setTimeout(self.updateOnCorrect.bind(self), 2000);


}

House_2_1.prototype.loadGameJson = function() {
    var self = this;
    // create empty object to load dot to dot game json data
    this.gameSets = {};

    // load json.
    var jsonLoader = new PIXI.JsonLoader('uploads/exercises/house/house_2_1.json?nocache=' + (new Date()).getTime());

    jsonLoader.on('loaded', function(evt) {
        self.gameSets = jsonLoader.json;
        self.runExercise();
    });
    jsonLoader.load();
}


House_2_1.prototype.cleanMemory = function() {

    if (this.sceneBackground)
        this.sceneBackground = null;

    this.help = null;

    this.mouth = null;
    this.mouthParams = null;

    if (this.questionSoundArray) this.questionSoundArray.length = 0;

    this.hand = null;

    this.mouthOutro = null;

    this.back_again = null;

    this.bag = null;

    this.num = null;

    this.sceneContainer = null;
    this.introContainer = null;
    this.outroAnim = null;
    this.outroContainer = null;
    this.menuContainer = null;
    this.commonMenuContainer = null;

    this.baseContainer.removeChildren();
    this.baseContainer = null;

};