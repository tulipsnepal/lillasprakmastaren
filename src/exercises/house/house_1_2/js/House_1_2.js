/**
 * house_1_2 intro
 */
function House_1_2() {
    cl.show();

    PIXI.DisplayObjectContainer.call(this);
    main.CurrentExercise = "house_1_2";
    //ceate empty container to store all alfabetet scene stuffs
    this.baseContainer = new PIXI.DisplayObjectContainer();

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    //ceate empty container to store all alfabetet scene stuffs
    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();

    this.exerciseContainer = new PIXI.DisplayObjectContainer();

    //create empty menu container
    this.menuContainer = new PIXI.DisplayObjectContainer();

    this.commonMenuContainer = new PIXI.DisplayObjectContainer();

    localStorage.removeItem('house_1_2');

    this.fromOutro = 0; //This variable is used to detect if user presses ? button from outro page.

    main.texturesToDestroy = [];

    this.sceneContainer.position = {
        x: 77,
        y: 88
    };
    this.baseContainer.addChild(this.sceneContainer);
    this.loadSpriteSheet();
    this.addChild(this.baseContainer);

}

House_1_2.constructor = House_1_2;
House_1_2.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

House_1_2.imagePath = 'build/exercises/house/house_1_2/images/';
House_1_2.audioPath = 'build/exercises/house/house_1_2/audios/';
House_1_2.commonFolder = 'build/common/images/house/';
House_1_2.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

House_1_2.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(House_1_2.audioPath + 'intro');
    this.outroId = loadSound(House_1_2.audioPath + 'outro');

    // create an array of assets to load
    var assetsToLoad = [
        House_1_2.imagePath + "sprite_1_2.json",
        House_1_2.imagePath + "sprite_1_2.png",
        House_1_2.imagePath + "SkolaTextLight.png",
        House_1_2.imagePath + "SkolaTextLight.xml"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

};

House_1_2.prototype.spriteSheetLoaded = function() {

    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF); //d7ead3

    this.loadDefaults();
};



House_1_2.prototype.loadDefaults = function() {

    var self = this;

    this.loadExercise();

    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0x9ecea0); //9ecea0
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xf3c873);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);

    //this.sceneBackground.beginFill(0x9ecea0); //9ecea0

    this.sceneContainer.addChild(this.sceneBackground);

    //this.sceneBackground.drawRect(0, 0, 1843, 431);

    this.sceneContainer.addChild(this.introContainer);

    // girl with arms crossed
    this.lillamasteren = new PIXI.Sprite.fromFrame('lillamasteren.png');
    this.lillamasteren.position = {
        x: 55,
        y: 602
    };
    this.woodenrack = new PIXI.Sprite.fromFrame("woodenrack.png");
    this.woodenrack.position = {
        x: 87,
        y: 460
    };

    main.texturesToDestroy.push(this.lillamasteren);
    this.sceneContainer.addChild(this.woodenrack);
    this.sceneContainer.addChild(this.exerciseContainer);
    this.sceneContainer.addChild(this.lillamasteren);
    this.help = PIXI.Sprite.fromFrame("help.png");
    this.help.position = {
        x: 0,
        y: 0
    };

    this.help.buttonMode = true;
    this.help.interactive = true;
    this.baseContainer.addChild(this.help);
    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.mouthParams = {
        "image": "mouth",
        "length": 5,
        "x": 367,
        "y": 911,
        "speed": 6,
        "pattern": true,
        "frameId": 1
    };
    this.mouth = new Animation(this.mouthParams);
    this.sceneContainer.addChild(this.mouth);
    this.mouth.gotoAndStop(1);

    //the pattern of the eye needs to change less than mouth
    this.eyeParams = {
        "image": "eyes",
        "length": 2,
        "x": 329,
        "y": 798,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1],
        "frameId": 1
    };

    this.eye = new Animation(this.eyeParams);
    this.sceneContainer.addChild(this.eye);
    this.eye.gotoAndStop(1);

    this.hands = [
        new PIXI.Sprite.fromFrame('arm1.png'),
        new PIXI.Sprite.fromFrame('arm2.png'),
        new PIXI.Sprite.fromFrame('arm3.png'),
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(413, 428);
        this.sceneContainer.addChild(this.hands[i]);
        this.hands[i].visible = false;
    }

    this.showHandAt(0);

    this.sceneContainer.addChild(this.outroContainer);
    this.outroContainer.visible = false;

    this.back_again = PIXI.Sprite.fromFrame("ovaigen.png");
    this.back_again.position = {
        x: 1141,
        y: 894
    };
    this.back_again.buttonMode = true;
    this.back_again.interactive = true;
    this.sceneContainer.addChild(this.back_again);
    this.back_again.visible = false;

    this.loadGameJson();


};

House_1_2.prototype.loadExercise = function() {
    this.coloredFileArr = [];
    this.coloredFileArr.push(new PIXI.Texture.fromFrame("greenfile.png"));
    this.coloredFileArr.push(new PIXI.Texture.fromFrame("bluefile.png"));
    this.coloredFileArr.push(new PIXI.Texture.fromFrame("greyfile.png"));
    this.coloredFileArr.push(new PIXI.Texture.fromFrame("lightbluefile.png"));
    this.coloredFileArr.push(new PIXI.Texture.fromFrame("lightgreen.png"));
    this.coloredFileArr.push(new PIXI.Texture.fromFrame("lightpink.png"));
    this.coloredFileArr.push(new PIXI.Texture.fromFrame("lightyellow.png"));
    this.coloredFileArr.push(new PIXI.Texture.fromFrame("mediumgreenfile.png"));
    this.coloredFileArr.push(new PIXI.Texture.fromFrame("pinkfile.png"));
    this.coloredFileArr.push(new PIXI.Texture.fromFrame("red.png"));
    this.coloredFileArr.push(new PIXI.Texture.fromFrame("yellowfile.png"));

    this.whitefileTexture = new PIXI.Texture.fromFrame("whitefile.png");

    clearBlock = new PIXI.Graphics();
    clearBlock.beginFill(0xFFFFFF);
    clearBlock.drawRect(0, 0, 100, 100);
    clearBlock.clear();
    this.clearBlock = clearBlock.generateTexture();

    whiteBlock = new PIXI.Graphics();
    whiteBlock.beginFill(0xFFFFFF);
    whiteBlock.drawRect(0, 0, 100, 100);
    this.whiteBlock = whiteBlock.generateTexture();
    redBlock = new PIXI.Graphics();
    redBlock.beginFill(0xFF0000);
    redBlock.drawRect(0, 0, 100, 100);
    this.redBlock = redBlock.generateTexture();

    var rectRed = createRect({
        w: 136,
        h: 115,
        color: 0xff0000
    });

    var rectGreen = createRect({
        w: 136,
        h: 115,
        color: 0x00ff00
    });

    this.feedBackRedBlock = rectRed.generateTexture();
    this.feedBackGreenBlock = rectGreen.generateTexture();

}

House_1_2.prototype.showRedGreenBox = function(xPos, yPos, obj) {
    var self = this;
    var ft = new PIXI.Sprite(obj);
    this.sceneContainer.addChild(ft);
    ft.position = {
        x: xPos,
        y: yPos
    };
    ft.alpha = 0.5;
    ft.visible = true;

    setTimeout(function() {
        ft.visible = false;
    }, 800);

}


House_1_2.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    }
    if (isNumeric(number))
        this.hands[number].visible = true;
};

/**
 * play intro animation
 */
House_1_2.prototype.introAnimation = function() {

    createjs.Sound.stop();
    main.overlay.visible = true;

    if (this.fromOutro == 1) {
        this.fromOutro = 0;
        this.resetFeedback();
        this.restartExercise();

    }

    this.outroContainer.visible = false;
    this.introContainer.visible = true;

    this.help.interactive = false;
    this.help.buttonMode = false;

    this.mouth.show();
    this.eye.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.

0-1 sec arm_0   => arm1.png => 0
1-2 sec arm_side => arm2.png => 1
2-3 sec arm_0    = >arm1.png => 0
3-4 sec arm_up  => arm3.png => 2
4-stop at arm_0 =>arm1.png => 0

 */
House_1_2.prototype.animateIntro = function() {

    var currentPosition = this.introInstance.getPosition();
    if (currentPosition >= 0 && currentPosition < 1000) {
        this.showHandAt(0);
    } else if (currentPosition >= 1000 && currentPosition < 2000) {
        this.showHandAt(2);
    } else if (currentPosition >= 2000 && currentPosition < 3500) {
        this.showHandAt(1);
    } else if (currentPosition >= 3500 && currentPosition < 4000) {
        this.showHandAt(0);
    } else {
        this.showHandAt(2);
    }
};

/**
 * cancel running intro animation
 */
House_1_2.prototype.cancelIntro = function() {

    clearInterval(this.timerId);

    createjs.Sound.stop();
    this.mouth.hide(1);
    this.eye.hide(1);
    this.showHandAt(0);

    this.help.interactive = true;
    this.help.buttonMode = true;

    this.back_again.visible = false;
    main.overlay.visible = false;
}

House_1_2.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        var totalDuration = outroInstance.getDuration();
        if (self.timerId) {
            clearInterval(self.timerId);
            self.timerId = null;
        }
        self.timerId = setInterval(function() {
            var currentPosition = outroInstance.getPosition();
        }, 1);
        self.mouth.show();
        self.eye.show();
    }

    this.outroCancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.outroCancelHandler)

};

House_1_2.prototype.cancelOutro = function() {

    clearInterval(this.timerId);
    this.timerId = null;

    createjs.Sound.stop();
    this.mouth.hide(1);
    this.eye.hide(1);

    this.help.interactive = true;
    this.help.buttonMode = true;

    this.back_again.visible = true;
    main.overlay.visible = false;
};

House_1_2.prototype.resetFeedback = function() {
    var self = this;
    localStorage.setItem('house_1_2', 0);
}


House_1_2.prototype.restartExercise = function() {
    var self = this;

    this.back_again.visible = false;

    this.introMode = true;

    this.resetFeedback();

    this.coloredFileArr = [];
    this.coloredFileArr.push(new PIXI.Texture.fromFrame("greenfile.png"));
    this.coloredFileArr.push(new PIXI.Texture.fromFrame("bluefile.png"));
    this.coloredFileArr.push(new PIXI.Texture.fromFrame("greyfile.png"));
    this.coloredFileArr.push(new PIXI.Texture.fromFrame("lightbluefile.png"));
    this.coloredFileArr.push(new PIXI.Texture.fromFrame("lightgreen.png"));
    this.coloredFileArr.push(new PIXI.Texture.fromFrame("lightpink.png"));
    this.coloredFileArr.push(new PIXI.Texture.fromFrame("lightyellow.png"));
    this.coloredFileArr.push(new PIXI.Texture.fromFrame("mediumgreenfile.png"));
    this.coloredFileArr.push(new PIXI.Texture.fromFrame("pinkfile.png"));
    this.coloredFileArr.push(new PIXI.Texture.fromFrame("red.png"));
    this.coloredFileArr.push(new PIXI.Texture.fromFrame("yellowfile.png"));

    this.exerciseContainer.removeChildren();

    this.loadGameJson();
};

House_1_2.prototype.onComplete = function() {

    var self = this;
    this.introMode = false;

    localStorage.removeItem('house_1_2');

    this.introContainer.visible = false;

    this.help.interactive = false;
    this.help.buttonMode = false;

    this.outroContainer.visible = true;
    this.fromOutro = 1;

    this.back_again.click = this.back_again.tap = function(data) {
        self.fromOutro = 0;
        self.resetExercise();
        self.restartExercise();
    };

    this.outroAnimation();
};

House_1_2.prototype.resetExercise = function() {

    createjs.Sound.stop();

    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    this.back_again.visible = false;

    this.help.interactive = true;
    this.help.buttonMode = true;

    this.mouth.hide(5);
    this.eye.hide(1);
    this.showHandAt(0);

    this.introMode = true;
    addedListeners = false;

};


House_1_2.prototype.runExercise = function() {

    var self = this;

    shuffle(this.coloredFileArr);

    this.alphabetFiles = [];

    this.shuffledAlphabetBlocks = [];

    this.whiteFileSpritePosition = [];

    var filePositionStart = [125, 216];

    var fileWidth = 150;

    var shuffledAlphabetPositions = [
        [895, 706],
        [1027, 804],
        [908, 951],
        [1244, 708],
        [1343, 851],
        [1205, 974],
        [1519, 778],
        [1632, 908],
        [1440, 1044],
        [1644, 672],
    ];

    var jsonData = this.alphabets;

    shuffle(shuffledAlphabetPositions);
    shuffledAlphabetPositions.unshift([895, 706]);

    var positionAdjustedForDraggedItem = [];

    for (var i = 0; i < 11; i++) {
        var whiteFileSprite = new PIXI.Sprite(this.whitefileTexture);
        var fileText = new PIXI.BitmapText(jsonData.alphabet[i] + " ", {
            font: "75px SkolaTextLight",
            fill: "black"
        });
        fileText.position = {
            x: (whiteFileSprite.width - fileText.width) / 2 + 25,
            y: (whiteFileSprite.height - fileText.height) / 2 - 20
        };

        whiteFileSprite.addChild(fileText);

        this.alphabetFiles[i] = whiteFileSprite;

        whiteFileSprite.position = {
            x: filePositionStart[0] + i * fileWidth,
            y: filePositionStart[1]
        };
        this.whiteFileSpritePosition.push(whiteFileSprite.position);

        positionAdjustedForDraggedItem.push(whiteFileSprite.position.x + fileText.position.x);
        this.exerciseContainer.addChild(whiteFileSprite);
        main.texturesToDestroy.push(whiteFileSprite);

    }

    var adjustSpaceForWideAlphabets = 0,
        wideChars = ["W", "M"]; //the characters which has bigger width than other chars. These chars messes up position so adjust position for only these chars

    for (var i = 0; i < shuffledAlphabetPositions.length; i++) {
        var ansSprite = new PIXI.Sprite(this.whiteBlock);

        var ansText = new PIXI.BitmapText(jsonData.answer[i] + " ", {
            font: "75px SkolaTextLight",
            fill: "black"
        });
        ansText.position = {
            x: (ansSprite.width - ansText.width) / 2,
            y: (ansSprite.height - ansText.height) / 2 - 5
        };
        ansSprite.addChild(ansText);

        ansSprite.interactive = true;
        ansSprite.buttonMode = true;
        this.shuffledAlphabetBlocks[i] = ansSprite;
        this.shuffledAlphabetBlocks[i].position = {
            x: shuffledAlphabetPositions[i][0],
            y: shuffledAlphabetPositions[i][1]
        };

        this.shuffledAlphabetBlocks[i].oldPosition = {
            x: shuffledAlphabetPositions[i][0],
            y: shuffledAlphabetPositions[i][1]
        };

        var idx = wideChars.indexOf(jsonData.answer[i]);
        adjustSpaceForWideAlphabets = 0;
        if (idx > -1) {
            adjustSpaceForWideAlphabets = 12;
        }

        this.shuffledAlphabetBlocks[i].dropPosition = {
            x: positionAdjustedForDraggedItem[i] - ansText.width + adjustSpaceForWideAlphabets,
            y: filePositionStart[1] + 140
        };

        this.exerciseContainer.addChild(ansSprite);
        this.shuffledAlphabetBlocks[i].positioned = false;
        this.shuffledAlphabetBlocks[i].fileId = i;
        this.onDragging(i);
    }
    // According to document the first file needs to be filled and colored
    this.shuffledAlphabetBlocks[0].position = {
        x: this.shuffledAlphabetBlocks[0].dropPosition.x,
        y: this.shuffledAlphabetBlocks[0].dropPosition.y
    };

    this.shuffledAlphabetBlocks[0].getChildAt(0).position = {
        x: -5,
        y: 10
    };
    this.shuffledAlphabetBlocks[0].positioned = true;
    this.shuffledAlphabetBlocks[0].interactive = false;
    //this.shuffledAlphabetBlocks[0].clear();
    this.shuffledAlphabetBlocks[0].texture = this.clearBlock;
    this.alphabetFiles[0].texture = this.coloredFileArr.shift();
};

House_1_2.prototype.onDragging = function(i) {
    var self = this;

    // use the mousedown and touchstart
    this.shuffledAlphabetBlocks[i].mousedown = this.shuffledAlphabetBlocks[i].touchstart = function(data) {
        if (addedListeners) return;

        data.originalEvent.preventDefault()
            // store a refference to the data
            // The reason for this is because of multitouch
            // we want to track the movement of this particular touch
        this.data = data;
        this.alpha = 0.9;
        this.dragging = true;
        this.sx = this.data.getLocalPosition(this).x;
        this.sy = this.data.getLocalPosition(this).y;
        this.bringToFront();

    }

    this.shuffledAlphabetBlocks[i].mouseup = this.shuffledAlphabetBlocks[i].mouseupoutside = this.shuffledAlphabetBlocks[i].touchend = this.shuffledAlphabetBlocks[i].touchendoutside = function(data) {

        if (addedListeners) return;

        this.alpha = 1
        this.dragging = false;
        // set the interaction data to null
        this.data = null;
        noDropArea = true;
        if (self.dropPositionPerIndex(this.dx, this.dy, self.alphabetFiles[i].position)) {
            this.position = {
                x: this.dropPosition.x,
                y: this.dropPosition.y
            };
            this.getChildAt(0).position = {
                x: -5,
                y: 10
            };
            this.positioned = true;
            this.interactive = false;

            self.showRedGreenBox(self.whiteFileSpritePosition[this.fileId].x + 6, self.whiteFileSpritePosition[this.fileId].y + 130, self.feedBackGreenBlock);

            this.texture = self.clearBlock;

            fileId = this.fileId;
            console.log(fileId);

            correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);

            setTimeout(function() {
                self.alphabetFiles[fileId].texture = self.coloredFileArr.shift();
                self.correctFeedback(fileId);
            }, 1000);

            //check if the blocks above are placed and replace them for graphical fix to look it above.
            //console.log(self.shuffledAlphabetBlocks);

        } else {
            // reset dragged element position
            for (j = 0; j < self.alphabetFiles.length; j++) {
                if (!self.shuffledAlphabetBlocks[j].positioned)
                    if (self.dropPositionPerIndex(this.dx, this.dy, self.alphabetFiles[j].position)) {
                        this.position = {
                            x: self.shuffledAlphabetBlocks[j].dropPosition.x - 25,
                            y: self.shuffledAlphabetBlocks[j].dropPosition.y
                        };
                        this.interactive = false;
                        this.alpha = 0.5;

                        self.showRedGreenBox(279 + ((j - 1) * 150) + 5, 341 + 5, self.feedBackRedBlock);

                        wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                        setTimeout(function() {
                            self.shuffledAlphabetBlocks[i].alpha = 1;
                            self.shuffledAlphabetBlocks[i].interactive = true;
                            self.shuffledAlphabetBlocks[i].texture = self.whiteBlock;
                            self.shuffledAlphabetBlocks[i].position.x = self.shuffledAlphabetBlocks[i].oldPosition.x;
                            self.shuffledAlphabetBlocks[i].position.y = self.shuffledAlphabetBlocks[i].oldPosition.y;
                        }, 1000);
                        noDropArea = false;
                        break;
                    }
            }

            if (noDropArea) {
                this.position.x = this.oldPosition.x;
                this.position.y = this.oldPosition.y;
            }
        }
    }

    // set the callbacks for when the mouse or a touch moves
    this.shuffledAlphabetBlocks[i].mousemove = this.shuffledAlphabetBlocks[i].touchmove = function(data) {
        // set the callbacks for when the mouse or a touch moves
        if (this.dragging) {
            // need to get parent coords..
            var newPosition = this.data.getLocalPosition(this.parent);
            this.position.x = newPosition.x - this.sx;
            this.position.y = newPosition.y - this.sy;
            this.dx = newPosition.x;
            this.dy = newPosition.y;
        }
    };
}

House_1_2.prototype.dropPositionPerIndex = function(dx, dy, index) {
    var x1 = index.x,
        y1 = index.y,
        x2 = index.x + 150,
        y2 = index.y + 403;

    if (isInsideRectangle(dx, dy, x1, y1, x2, y2))
        return true;
    else
        return false;
}

House_1_2.prototype.correctFeedback = function(fileId) {
    var feedback = localStorage.getItem('house_1_2');
    feedback++;
    localStorage.setItem('house_1_2', feedback);
    console.log(localStorage.getItem("house_1_2"));

    //this.alphabetFiles[fileId].texture = this.coloredFileArr.shift();
    if (feedback >= House_1_2.totalFeedback)
        setTimeout(this.onComplete.bind(this), 1000);
}

House_1_2.prototype.loadGameJson = function() {
    var self = this;
    // create empty object to load dot to dot game json data
    this.alphabets = {};

    var jsonLetters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'Å', 'Ä', 'Ö'];

    var randomSets = generateUniqueRandomNubers({
        'max': 28,
        'total': 11
    });

    var alphabet = [],
        answer = [];

    for (var i = 0; i < 11; i++) {
        var letterSets = jsonLetters[randomSets[i]];
        alphabet.push(letterSets.toLowerCase());
        answer.push(letterSets);
    };
    this.alphabets = {
        'alphabet': alphabet.sort(),
        'answer': answer.sort()
    };
    self.runExercise();

}

House_1_2.prototype.cleanMemory = function() {

    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    this.help = null;

    this.mouth = null;
    this.mouthParams = null;

    this.fromOutro = null;

    this.hand = null;

    this.mouthOutro = null;

    this.back_again = null;

    this.bag = null;

    this.num = null;

    this.sceneContainer = null;
    this.introContainer = null;
    this.outroContainer = null;
    this.menuContainer = null;
    this.commonMenuContainer = null;

    this.baseContainer.removeChildren();
    this.baseContainer = null;

};