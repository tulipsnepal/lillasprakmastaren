/**
 * house_1_1 intro
 */
function House_1_1() {
    cl.show();

    PIXI.DisplayObjectContainer.call(this);
    main.CurrentExercise = "house_1_1";
    //ceate empty container to store all alfabetet scene stuffs
    this.baseContainer = new PIXI.DisplayObjectContainer();

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    //ceate empty container to store all alfabetet scene stuffs
    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();

    this.exerciseContainer = new PIXI.DisplayObjectContainer();

    this.towerContainer = new PIXI.DisplayObjectContainer();
    //create empty menu container
    this.menuContainer = new PIXI.DisplayObjectContainer();

    this.commonMenuContainer = new PIXI.DisplayObjectContainer();

    localStorage.removeItem('house_1_1');

    this.fromOutro = 0; //This variable is used to detect if user presses ? button from outro page.

    main.texturesToDestroy = [];

    this.sceneContainer.position = {
        x: 77,
        y: 88
    };
    this.baseContainer.addChild(this.sceneContainer);
    this.loadSpriteSheet();
    this.addChild(this.baseContainer);

}

House_1_1.constructor = House_1_1;
House_1_1.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

House_1_1.imagePath = 'build/exercises/house/house_1_1/images/';
House_1_1.audioPath = 'build/exercises/house/house_1_1/audios/';
House_1_1.commonFolder = 'build/common/images/house/';
House_1_1.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

House_1_1.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(House_1_1.audioPath + 'intro');
    this.outroId = loadSound(House_1_1.audioPath + 'outro');

    // create an array of assets to load
    var assetsToLoad = [
        House_1_1.imagePath + "sprite_1_1.json",
        House_1_1.imagePath + "sprite_1_1.png",
        House_1_1.imagePath + "SkolaTextBold.xml",
        House_1_1.imagePath + "SkolaTextBold.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

};

House_1_1.prototype.spriteSheetLoaded = function() {

    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF); //d7ead3

    this.loadDefaults();
};



House_1_1.prototype.loadDefaults = function() {

    var self = this;

    this.loadExercise();

    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0xd7ead3); //9ecea0
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0x818e82);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);

    this.sceneBackground.beginFill(0x9ecea0); //9ecea0

    this.sceneContainer.addChild(this.sceneBackground);

    this.sceneBackground.drawRect(0, 0, 1843, 431);

    this.sceneContainer.addChild(this.introContainer);

    // girl with arms crossed
    this.girlarmscrossed = new PIXI.Sprite(this.lillaarmcrossedTexture);
    this.girlarmscrossed.position = {
        x: 65,
        y: 216
    };

    main.texturesToDestroy.push(this.girlarmscrossed);
    this.introContainer.addChild(this.girlarmscrossed);
    this.exerciseContainer.addChild(this.towerContainer);
    this.introContainer.addChild(this.exerciseContainer);
    this.help = PIXI.Sprite.fromFrame("help.png");
    this.help.position = {
        x: 0,
        y: 0
    };

    this.help.buttonMode = true;
    this.help.interactive = true;
    this.baseContainer.addChild(this.help);
    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.back_again = PIXI.Sprite.fromFrame("ovaigen.png");
    this.back_again.position = {
        x: 1183,
        y: 383
    };
    this.back_again.buttonMode = true;
    this.back_again.interactive = true;
    this.outroContainer.addChild(this.back_again);
    this.back_again.visible = false;

    this.mouthParams = {
        "image": "mouth",
        "length": 5,
        "x": 186,
        "y": 389,
        "speed": 6,
        "pattern": true,
        "frameId": 1
    };
    this.mouth = new Animation(this.mouthParams);
    this.introContainer.addChild(this.mouth);
    this.mouth.gotoAndStop(5);
    this.mouth.visible = false;

    //the pattern of the eye needs to change less than mouth
    this.eyeParams = {
        "image": "eyes",
        "length": 2,
        "x": 161,
        "y": 321,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1],
        "frameId": 1
    };

    this.eye = new Animation(this.eyeParams);
    this.introContainer.addChild(this.eye);
    this.eye.gotoAndStop(1);
    this.eye.visible = false;

    this.hands = [
        new PIXI.Sprite.fromFrame('arm_point.png'),
        new PIXI.Sprite.fromFrame('arm_side.png'),
        new PIXI.Sprite.fromFrame('arm_up.png'),
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(285, 172);
        this.introContainer.addChild(this.hands[i]);
        this.hands[i].visible = false;
    }

    this.showHandAt(null);

    this.sceneContainer.addChild(this.outroContainer);
    this.outroContainer.visible = false;

    this.mouthParamsOutro = {
        "image": "mouthoutro",
        "length": 5,
        "x": 210,
        "y": 416,
        "speed": 6,
        "pattern": true,
        "frameId": 2
    };
    this.mouthOutro = new Animation(this.mouthParamsOutro);
    this.outroContainer.addChild(this.mouthOutro);

    this.loadGameJson();

};

House_1_1.prototype.loadExercise = function() {
    this.cubeTexture = new PIXI.Texture.fromFrame("cube-caps.png");
    this.blankCubeTexture = new PIXI.Texture.fromFrame("cube-dotted.png");
    this.greenCubeTexture = new PIXI.Texture.fromFrame("green-cube.png");
    this.redCubeTexture = new PIXI.Texture.fromFrame("red-cube.png");
    this.lillaarmcrossedTexture = new PIXI.Texture.fromFrame("lillaarmcrossed.png");
    this.lillaonearmedTexture = new PIXI.Texture.fromFrame("lillaonearmed.png");
    this.readytopushTexture = new PIXI.Texture.fromFrame("readytopush.png");
    this.pushTexture = new PIXI.Texture.fromFrame("push.png");
}


House_1_1.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    }
    if (isNumeric(number)) {
        this.hands[number].visible = true;
        this.hands[number].bringToFront();
    }
};

/**
 * play intro animation
 */
House_1_1.prototype.introAnimation = function() {

    createjs.Sound.stop();
    main.overlay.visible = true;

    this.resetExercise();

    if (this.fromOutro == 1) {
        this.fromOutro = 0;
        this.resetFeedback();
        this.restartExercise();
    }


    this.help.interactive = false;
    this.help.buttonMode = false;

    this.girlarmscrossed.texture = this.lillaonearmedTexture;

    this.girlarmscrossed.position.x = this.girlarmscrossed.position.x;

    this.girlarmscrossed.bringToFront();
    this.eye.bringToFront();
    this.mouth.bringToFront();

    this.mouth.show();
    this.eye.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
 */
/*
1-2 sec arm_point 0
2-4 sec arm_side 1
4-5 sec arm_point 0
5-6 sec arm_up 2
6-8 sec arm_side 1
new PIXI.Sprite.fromFrame('arm_point.png'),
        new PIXI.Sprite.fromFrame('arm_side.png'),
        new PIXI.Sprite.fromFrame('arm_up.png'),
 */
House_1_1.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    if (currentPostion >= 1000 && currentPostion < 2000) {
        this.showHandAt(0);
    } else if (currentPostion >= 2000 && currentPostion < 4000) {
        this.showHandAt(1);
    } else if (currentPostion >= 4000 && currentPostion < 5000) {
        this.showHandAt(0);
    } else if (currentPostion >= 5000 && currentPostion < 6000) {
        this.showHandAt(2);
    } else if (currentPostion >= 6000 && currentPostion < 8000) {
        this.showHandAt(1);
    } else {
        this.showHandAt(1);
    }
};

/**
 * cancel running intro animation
 */
House_1_1.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);
    this.mouth.visible = false;
    this.eye.visible = false;
    this.showHandAt(null);

    this.help.interactive = true;
    this.help.buttonMode = true;
    this.girlarmscrossed.texture = this.lillaarmcrossedTexture;
    this.girlarmscrossed.position.x = this.girlarmscrossed.position.x;

    this.back_again.visible = false;
    main.overlay.visible = false;
}

House_1_1.prototype.outroAnimation = function() {

    var self = this;

    self.girlarmscrossed.bringToFront();

    this.mouthOutro.bringToFront();

    var outroInstance = createjs.Sound.play(this.outroId);
    self.girlarmscrossed.position.x += 16;

    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        var totalDuration = outroInstance.getDuration();

        if (self.timerId) {
            clearInterval(self.timerId);
            self.timerId = null;
        }
        self.timerId = setInterval(function() {
            var currentPostion = outroInstance.getPosition();
            if (currentPostion >= 1 && currentPostion < 3300) {
                self.girlarmscrossed.texture = self.readytopushTexture;

                self.mouthOutro.show();
            } else if (currentPostion >= 3300 && currentPostion < 3800) {
                self.girlarmscrossed.texture = self.readytopushTexture;
                self.mouthOutro.hide(2);
            } else if (currentPostion >= 3800 && currentPostion < 4300) {
                self.girlarmscrossed.texture = self.pushTexture;
                requestAnimationFrame(tiltTower);
                self.mouthOutro.hide(2);
            } else if (currentPostion >= 4300 && currentPostion < 5800) {
                self.mouthOutro.hide(5);
            } else if (currentPostion >= 5800 && currentPostion < 8100) {
                self.mouthOutro.show();
                self.girlarmscrossed.texture = self.pushTexture;
            } else {
                self.mouthOutro.visible = false;
                self.girlarmscrossed.texture = self.lillaarmcrossedTexture;
            }
        }, 1);
        self.mouthOutro.show();
    }
    self.exerciseContainer.tiltAnimation = true;

    self.exerciseContainer.position.x = 695;
    self.exerciseContainer.position.y = 895;
    self.exerciseContainer.pivot.x = 695;
    self.exerciseContainer.pivot.y = 895;

    function tiltTower() {
        if (self.exerciseContainer.tiltAnimation == true) {
            self.exerciseContainer.rotation += 0.001;
            if (self.exerciseContainer.rotation >= 0.26) {
                self.exerciseContainer.tiltAnimation = false;
                requestAnimationFrame(messCubes);
            }
            requestAnimationFrame(tiltTower);
        }
    }

    var mess_position_rotaion = {};

    for (i = 0; i < 20; i++) {
        mess_position_rotaion[i] = {};
        mess_position_rotaion[i][0] = Math.floor((Math.random() * 919) + 681); //x pos
        mess_position_rotaion[i][1] = Math.floor((Math.random() * 390) + 602); //y pos
        if (Math.floor((Math.random() * 10) + 1) % 2 == 0)
            mess_position_rotaion[i][2] = -(Math.random() * 1).toFixed(2); //rotation
        else
            mess_position_rotaion[i][2] = (Math.random() * 1).toFixed(2); //rotation

    }

    function messCubes() {
        self.exerciseContainer.rotation = 0;
        for (var i = 0; i < self.shuffledAlphabetBlocks.length; i++) {
            self.shuffledAlphabetBlocks[i].scale.x = 0.83;
            self.shuffledAlphabetBlocks[i].scale.y = 0.83;
            self.shuffledAlphabetBlocks[i].position.x = mess_position_rotaion[i][0];
            self.shuffledAlphabetBlocks[i].position.y = mess_position_rotaion[i][1];
            self.shuffledAlphabetBlocks[i].rotation = mess_position_rotaion[i][2];
            self.alphabetBlocks[i].scale.x = 0.83;
            self.alphabetBlocks[i].scale.y = 0.83;
            self.alphabetBlocks[i].position.x = mess_position_rotaion[10 + i][0];
            self.alphabetBlocks[i].position.y = mess_position_rotaion[10 + i][1];
            self.alphabetBlocks[i].rotation = mess_position_rotaion[10 + i][2];
        }
        self.back_again.visible = true;
        addedListeners = false;
    }

    this.cancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.cancelHandler);

};

House_1_1.prototype.resetFeedback = function() {
    var self = this;
    localStorage.setItem('house_1_1', 0);
}


House_1_1.prototype.restartExercise = function() {

    this.resetFeedback();
    this.towerContainer.removeChildren();
    this.exerciseContainer.removeChildren();
    this.exerciseContainer.addChild(this.towerContainer);

    this.loadGameJson();

};

House_1_1.prototype.onComplete = function() {

    var self = this;

    this.introMode = false;

    localStorage.removeItem('house_1_1');

    this.introContainer.visible = false;

    this.help.interactive = false;
    this.help.buttonMode = false;

    //change the color to green
    for (var i = 0; i < self.shuffledAlphabetBlocks.length; i++) {
        self.shuffledAlphabetBlocks[i].texture = self.greenCubeTexture;
        self.answerBlocks[i].visible = false;
    }

    self.fromOutro = 1;

    //remove the tower and the girl from intro container to outro container
    //simply reusing same component instead of creating two of the same.
    this.introContainer.removeChild(this.girlarmscrossed);
    this.introContainer.removeChild(this.exerciseContainer);
    this.outroContainer.addChild(this.girlarmscrossed);
    this.outroContainer.addChild(this.exerciseContainer);

    this.outroContainer.visible = true;

    this.resetMode = true;

    this.back_again.click = this.back_again.tap = function(data) {
        self.fromOutro = 0;
        self.resetExercise();
        self.restartExercise();
    };

    this.outroAnimation();

};

House_1_1.prototype.cancelOutro = function() {

    createjs.Sound.stop();

    this.girlarmscrossed.texture = this.lillaarmcrossedTexture;
    this.girlarmscrossed.position.x -= 16;
    this.mouthOutro.visible = false;

    clearInterval(this.timerId);
    this.timerId = null;

    this.help.interactive = true;
    this.help.buttonMode = true;

    this.back_again.visible = true;
    main.overlay.visible = false;
};

House_1_1.prototype.resetExercise = function() {

    var self = this;

    createjs.Sound.stop();

    self.back_again.visible = false;
    self.help.interactive = true;
    self.help.buttonMode = true;
    self.girlarmscrossed.position.x = 65;

    if (this.resetMode) {
        for (var i = 0; i < self.shuffledAlphabetBlocks.length; i++) {
            self.shuffledAlphabetBlocks[i].texture = self.cubeTexture;
            self.answerBlocks[i].visible = true;
        }

        self.outroContainer.removeChild(self.girlarmscrossed);
        self.outroContainer.removeChild(self.exerciseContainer);
        self.introContainer.addChild(self.girlarmscrossed);
        self.introContainer.addChild(self.exerciseContainer);

        self.exerciseContainer.position.x = 695;
        self.exerciseContainer.position.y = 895;
        self.exerciseContainer.pivot.x = 695;
        self.exerciseContainer.pivot.y = 895;
        self.exerciseContainer.rotation = 0;

        this.resetMode = false;
    }

    this.introMode = true;

    self.outroContainer.visible = false;
    self.introContainer.visible = true;
};

House_1_1.prototype.runExercise = function() {

    var self = this;

    this.alphabetBlocks = [];

    this.shuffledAlphabetBlocks = [];

    this.answerBlocks = [];

    var alphabetPositionStart = [490, 942]; //Bottom point, need to start from bottom cause the top parts needs to be overlapped by nextblock

    var blockFaceHeight = 99;

    var answerPositionStart = [593, 942];

    var shuffledAlphabetPositions = [
        [877, 582],
        [1148, 519],
        [831, 800],
        [1385, 513],
        [1573, 639],
        [1380, 779],
        [1028, 912],
        [1287, 978],
        [1536, 954],
        [1116, 696]
    ];

    var jsonData = this.alphabets;

    //this.soundTexture = new PIXI.Texture.fromFrame("sound_grey.png");

    shuffle(shuffledAlphabetPositions);

    for (var i = 0; i < jsonData.alphabet.length; i++) {
        //console.log(this.cubeTexture);
        var cubeSprite = new PIXI.Sprite(this.cubeTexture);
        var blankCubeSprite = new PIXI.Sprite(this.blankCubeTexture);
        var cubeText = new PIXI.BitmapText(jsonData.alphabet[i] + " ", {
            font: "75px SkolaTextBold",
            fill: "black"
        });
        /*cubeText.position = {
            x: -3,
            y: 45 - 76
        };*/
        cubeText.position = {
            x: (cubeSprite.width - cubeText.width) / 2 - 15,
            y: (cubeSprite.height - cubeText.height) / 2 + 10
        };
        cubeSprite.addChild(cubeText);

        this.alphabetBlocks[i] = cubeSprite;

        cubeSprite.position = {
            x: alphabetPositionStart[0],
            y: alphabetPositionStart[1] - i * blockFaceHeight
        };
        this.towerContainer.addChild(cubeSprite);
        main.texturesToDestroy.push(cubeSprite);

        blankCubeSprite.position = {
            x: answerPositionStart[0],
            y: answerPositionStart[1] - i * blockFaceHeight
        }

        this.answerBlocks[i] = blankCubeSprite;
        this.towerContainer.addChild(blankCubeSprite);
        main.texturesToDestroy.push(blankCubeSprite);

    }

    for (var i = 0; i < shuffledAlphabetPositions.length; i++) {
        var ansCubeSprite = new PIXI.Sprite(this.cubeTexture);
        var ansCubeText = new PIXI.BitmapText(jsonData.answer[i] + " ", {
            font: "75px SkolaTextBold",
            fill: "black"
        });
        /* ansCubeText.position = {
             x: 5,
             y: 40 - 76
         };*/
        ansCubeText.position = {
            x: (ansCubeSprite.width - ansCubeText.width) / 2 - 20,
            y: (ansCubeSprite.height - ansCubeText.height) / 2 - 5
        };
        ansCubeSprite.addChild(ansCubeText);

        ansCubeSprite.interactive = true;
        ansCubeSprite.buttonMode = true;
        this.shuffledAlphabetBlocks[i] = ansCubeSprite;
        this.shuffledAlphabetBlocks[i].position = {
            x: shuffledAlphabetPositions[i][0],
            y: shuffledAlphabetPositions[i][1]
        };
        this.shuffledAlphabetBlocks[i].oldPosition = {
            x: shuffledAlphabetPositions[i][0],
            y: shuffledAlphabetPositions[i][1]
        };
        this.shuffledAlphabetBlocks[i].dropPosition = {
            x: answerPositionStart[0],
            y: (answerPositionStart[1]) - i * blockFaceHeight
        };
        this.exerciseContainer.addChild(ansCubeSprite);
        this.shuffledAlphabetBlocks[i].positioned = false;
        //main.texturesToDestroy.push(ansCubeSprite);
        this.onDragging(i);
    }

};

House_1_1.prototype.onDragging = function(i) {
    var self = this;

    // use the mousedown and touchstart
    this.shuffledAlphabetBlocks[i].mousedown = this.shuffledAlphabetBlocks[i].touchstart = function(data) {
        if (addedListeners) return;

        data.originalEvent.preventDefault()
            // store a refference to the data
            // The reason for this is because of multitouch
            // we want to track the movement of this particular touch
        this.data = data;
        this.alpha = 0.9;
        this.dragging = true;
        this.sx = this.data.getLocalPosition(this).x;
        this.sy = this.data.getLocalPosition(this).y;
        this.bringToFront();

    }

    this.shuffledAlphabetBlocks[i].mouseup = this.shuffledAlphabetBlocks[i].mouseupoutside = this.shuffledAlphabetBlocks[i].touchend = this.shuffledAlphabetBlocks[i].touchendoutside = function(data) {

        if (addedListeners) return;

        this.alpha = 1
        this.dragging = false;
        // set the interaction data to null
        this.data = null;

        noDropArea = true;
        if (self.dropPositionPerIndex(this.dx, this.dy, this.dropPosition)) {
            this.position = {
                x: this.dropPosition.x,
                y: this.dropPosition.y
            };
            this.positioned = true;
            this.interactive = false;
            //correct_sound.play();
            correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
            this.texture = self.greenCubeTexture;
            setTimeout(function() {
                self.correctFeedback();
            }, 1000);
            //check if the blocks above are placed and replace them for graphical fix to look it above.
            //console.log(self.shuffledAlphabetBlocks);
            for (var j = i; j < self.shuffledAlphabetBlocks.length; j++) {
                if (self.shuffledAlphabetBlocks[j].positioned) {
                    self.shuffledAlphabetBlocks[j].bringToFront();
                }
            }


        } else {
            // reset dragged element position

            for (j = 0; j < self.answerBlocks.length; j++) {
                if (!self.shuffledAlphabetBlocks[j].positioned)
                    if (self.dropPositionPerIndex(this.dx, this.dy, self.answerBlocks[j].position)) {
                        this.position = {
                            x: self.answerBlocks[j].position.x,
                            y: self.answerBlocks[j].position.y
                        };
                        for (var k = j; k < self.shuffledAlphabetBlocks.length; k++) {
                            if (self.shuffledAlphabetBlocks[k].positioned && k != i) {
                                self.shuffledAlphabetBlocks[k].bringToFront();
                            }
                        }
                        this.interactive = false;
                        self.shuffledAlphabetBlocks[i].texture = self.redCubeTexture;
                        wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                        setTimeout(function() {
                            self.shuffledAlphabetBlocks[i].interactive = true;
                            self.shuffledAlphabetBlocks[i].texture = self.cubeTexture;
                            self.shuffledAlphabetBlocks[i].position.x = self.shuffledAlphabetBlocks[i].oldPosition.x;
                            self.shuffledAlphabetBlocks[i].position.y = self.shuffledAlphabetBlocks[i].oldPosition.y;
                        }, 1000);
                        noDropArea = false;
                        break;
                    }
            }

            if (noDropArea) {
                this.position.x = this.oldPosition.x;
                this.position.y = this.oldPosition.y;
            }


        }
    }

    // set the callbacks for when the mouse or a touch moves
    this.shuffledAlphabetBlocks[i].mousemove = this.shuffledAlphabetBlocks[i].touchmove = function(data) {
        // set the callbacks for when the mouse or a touch moves
        if (this.dragging) {
            // need to get parent coords..
            var newPosition = this.data.getLocalPosition(this.parent);
            this.position.x = newPosition.x - this.sx;
            this.position.y = newPosition.y - this.sy;
            this.dx = newPosition.x;
            this.dy = newPosition.y;
        }
    };
}

House_1_1.prototype.dropPositionPerIndex = function(dx, dy, index) {
    var x1 = index.x,
        y1 = index.y,
        x2 = index.x + 124,
        y2 = index.y + 137;

    if (isInsideRectangle(dx, dy, x1, y1, x2, y2))
        return true;
    else
        return false;
}

House_1_1.prototype.correctFeedback = function() {
    var feedback = localStorage.getItem('house_1_1');
    feedback++;
    localStorage.setItem('house_1_1', feedback);

    if (feedback >= House_1_1.totalFeedback)
        setTimeout(this.onComplete.bind(this), 1000);
}

House_1_1.prototype.loadGameJson = function() {
    var self = this;
    // create empty object to load dot to dot game json data
    this.alphabets = {};

    var jsonLetters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'Å', 'Ä', 'Ö'];

    var initialValue = makeUniqueRandom(20),
        finalValue = initialValue + 10;
    alphabet = [],
        answer = [];

    for (var i = initialValue; i < finalValue; i++) {
        alphabet.push(jsonLetters[i]);
        answer.push(jsonLetters[i].toLowerCase());
    };
    shuffle(alphabet, answer);
    this.alphabets = {
        'alphabet': alphabet,
        'answer': answer
    };
    self.runExercise();
}

House_1_1.prototype.cleanMemory = function() {

    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    this.help = null;

    this.mouth = null;
    this.mouthParams = null;

    this.hand = null;

    if (this.mouthParamsOutro)
        this.mouthParamsOutro.length = 0;
    this.mouthOutro = null;

    this.back_again = null;

    this.bag = null;

    this.num = null;
    this.fromOutro = null;

    this.exerciseContainer = null;
    this.sceneContainer = null;
    this.introContainer = null;
    this.outroContainer = null;
    this.menuContainer = null;
    this.commonMenuContainer = null;

    this.baseContainer.removeChildren();
    this.baseContainer = null;

};