function House() {

    cl.show();

    PIXI.DisplayObjectContainer.call(this);

    this.houseContainer = new PIXI.DisplayObjectContainer();

    this.sceneContainer = new PIXI.DisplayObjectContainer();

    this.imageContainer = new PIXI.DisplayObjectContainer();

    this.menuContainer = new PIXI.DisplayObjectContainer();

    this.houseContainer.addChild(this.sceneContainer);
    this.houseContainer.addChild(this.menuContainer);

    //resize container
    this.imageContainer.position = {
        x: 0,
        y: -20
    }

    this.menuContainer.position = {
        x: 0,
        y: 1287 - 40
    }


    main.texturesToDestroy = [];
    main.soundToDestroy = [];

    loadSound('build/common/audios/house_1_3_help');

    this.loadSpriteSheet();

    this.addChild(this.houseContainer);
}

House.constructor = House;
House.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

House.imagePath = "build/exercises/house/house_0/images/";
House.COMMONFOLDER = 'build/common/images/house/';

House.prototype.loadSpriteSheet = function() {

    // create an array of assets to load
    var assetsToLoad = [
        House.imagePath + "base.png",
        House.COMMONFOLDER + 'menuskeleton.png',
        House.COMMONFOLDER + 'sprite_house.json',
        House.COMMONFOLDER + 'sprite_house.png',
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(assetsToLoad);

    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);

    //begin load
    loader.load();

}

House.prototype.spriteSheetLoaded = function() {

    cl.hide();

    // get backgroudn for house page
    this.bkg1 = new PIXI.Sprite.fromImage(House.imagePath + "base.png");
    this.sceneContainer.addChild(this.bkg1);

    this.loadMenus();

    this.imageButtons();

}

House.prototype.sceneChange = function(scene) {

    var prevExercise = localStorage.getItem('exercise');

    if (scene === prevExercise)
        return false;


    UnloadScene.apply(this);

    main.CurrentExercise = scene;

    localStorage.setItem('exercise', scene);

    main.page = new window[scene];
    main.page.introMode = true;

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0x000000);

    this.sceneContainer.addChild(main.page);

    this.cleanMemory();

};

House.prototype.loadMenus = function() {

    // position of 1 and 2 sub menu
    var x1 = 13 + 40,
        x2 = 132 + 124,
        x3 = 251 + 184,
        y = 14 - 20 - 50;

    this.menuParams = {
        path: House.COMMONFOLDER,
        background: 'menuskeleton.png',
        images: [
            'tillbaka.png',
            'alfabetet.png',
            'stora.png'
        ],
        positions: [
            18, 133,
            417, 74,
            1210, 70
        ],
        data: {
            1: {
                hover_image: 'menuhighlight.png',
                hover_position: [528, -73],
                submenu_image: [
                    'sub_menu_1.png',
                    'sub_menu_2.png',
                    'sub_menu_3.png',
                ],
                submenu_position: [
                    543 + x1, y,
                    543 + x2, y,
                    543 + x3, y
                ]

            },
            2: {
                hover_image: 'menuhighlight2.png',
                hover_position: [1136, -73],
                submenu_image: [
                    'sub_menu_1.png',
                    'sub_menu_2.png',
                    'sub_menu_3.png',
                ],
                submenu_position: [
                    1138 + x1, y,
                    1138 + x2, y,
                    1138 + x3, y
                ]
            },
            pink_image: 'circle.png',
            move_position: {
                x: 10,
                y: 32
            },
            chop: { // optional
                images: [
                    'alfabetet.png',
                    'stora.png'
                ],
                positions: [
                    417, 74,
                    1210, 70
                ]
            }
        }

    };
    this.menu = new Menu(this.menuParams);

    this.menuContainer.addChild(this.menu);

    //tillbaka button
    this.menu.getChildAt(1).click = this.menu.getChildAt(1).tap = this.houseOverview.bind(this);

    //rimord button
    this.menu.getChildAt(2).click = this.menu.getChildAt(2).tap = this.navMenu1.bind(this);

    //enochett button
    this.menu.getChildAt(3).click = this.menu.getChildAt(3).tap = this.navMenu2.bind(this);

}

House.prototype.imageButtons = function() {

    this.navPositions = [
        700, 390,
        1472, 432,
    ];

    this.navButtons = [];

    for (var i = 0; i < this.navPositions.length; i++) {
        var imageSprite = PIXI.Sprite.fromFrame('ova.png');

        imageSprite.buttonMode = true;
        imageSprite.position.x = this.navPositions[i * 2];
        imageSprite.position.y = this.navPositions[i * 2 + 1];
        imageSprite.interactive = true;

        this.imageContainer.addChild(imageSprite);

        this.navButtons.push(imageSprite);
        imageSprite = null;
    }

    //carousel image button
    this.navButtons[0].click = this.navButtons[0].tap = this.navMenu1.bind(this);

    //rimord image button
    this.navButtons[1].click = this.navButtons[1].tap = this.navMenu2.bind(this);

    this.sceneContainer.addChild(this.imageContainer);

}

House.prototype.houseOverview = function() {
    UnloadScene.apply(this);
    localStorage.removeItem('exercise');
    // createjs.Sound.removeAllSounds();
    history.back();
}

House.prototype.navMenu = function(submenu1, submenu2, submenu3, index) {

    this.sceneChange(submenu1);

    var self = this;

    this.pushStuffToHistory("history_other_House");

    this.subMenu = self.menu.getActiveMenu(index);
    self.menu.moveSelectedExerciseCircle(this.subMenu[0].position);
    self.sceneContainer.addChild(self.menuContainer);

    this.subMenu[0].click = this.subMenu[0].tap = function(data) {
        self.sceneChange(submenu1);

        self.menu.moveSelectedExerciseCircle(this.position);
        self.sceneContainer.addChild(self.menuContainer);

    }
    this.subMenu[1].click = this.subMenu[1].tap = function(data) {

        self.sceneChange(submenu2);

        self.menu.moveSelectedExerciseCircle(this.position);
        self.sceneContainer.addChild(self.menuContainer);
    }
    this.subMenu[2].click = this.subMenu[2].tap = function(data) {

        self.sceneChange(submenu3);

        self.menu.moveSelectedExerciseCircle(this.position);
        self.sceneContainer.addChild(self.menuContainer);
    }

}

House.prototype.pushStuffToHistory = function(key) {

    if (Device.ieVersion != 9) {
        var exHistory = new History();
        exHistory.PushState({
            scene: key
        });
        exHistory = null;
    }
}

House.prototype.navMenu1 = function() {
    this.navMenu('House_1_1', 'House_1_2', 'House_1_3', 1);
}

House.prototype.navMenu2 = function() {
    this.navMenu('House_2_1', 'House_2_2', 'House_2_3', 2);
}

House.prototype.cleanMemory = function() {
    main.eventRunning = false;
}