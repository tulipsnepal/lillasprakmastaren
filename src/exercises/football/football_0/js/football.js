function Football() {

	cl.show();

	PIXI.DisplayObjectContainer.call(this);

	this.tivoliContainer = new PIXI.DisplayObjectContainer();

	this.sceneContainer = new PIXI.DisplayObjectContainer();

	this.imageContainer = new PIXI.DisplayObjectContainer();

	this.menuContainer = new PIXI.DisplayObjectContainer();

	this.tivoliContainer.addChild(this.sceneContainer);
	this.tivoliContainer.addChild(this.menuContainer);

	//resize container
	this.imageContainer.position = {
		x: 0,
		y: -20
	}

	this.menuContainer.position = {
		x: 0,
		y: 1287 - 36
	}

	this.loadSpriteSheet();

	this.addChild(this.tivoliContainer);
}

Football.constructor = Football;
Football.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

Football.IMAGEPATH = "build/exercises/football/football_0/images/";
Football.COMMONFOLDER = 'build/common/images/football/';

Football.prototype.loadSpriteSheet = function() {

	// create an array of assets to load
	var assetsToLoad = [
		Football.IMAGEPATH + "football_0.png",
		Football.COMMONFOLDER + 'menuskeleton.png',
		Football.COMMONFOLDER + 'sprite_football.json',
		Football.COMMONFOLDER + 'sprite_football.png',
	];
	// create a new loader
	loader = new PIXI.AssetLoader(assetsToLoad);

	// use callback
	loader.onComplete = this.spriteSheetLoaded.bind(this);

	//begin load
	loader.load();

}

Football.prototype.spriteSheetLoaded = function() {

	cl.hide();

	// get backgroudn for tivoli page
	this.bkg1 = new Background(Football.IMAGEPATH + "football_0.png");
	this.sceneContainer.addChild(this.bkg1);

	this.loadMenus();

	this.imageButtons();

}

Football.prototype.homePage = function() {
	UnloadScene.apply(this);
	localStorage.removeItem('exercise');
	// createjs.Sound.removeAllSounds();
	history.back();
}

Football.prototype.sceneChange = function(scene) {

	var prevExercise = localStorage.getItem('exercise');

	if (scene === prevExercise)
		return false;


	UnloadScene.apply(this);

	main.CurrentExercise = scene;

	localStorage.setItem('exercise', scene);

	main.page = new window[scene];
	main.page.introMode = true;

	loadSound('build/common/audios/' + main.CurrentExercise + "_help");

	if (this.sceneContainer.stage)
		this.sceneContainer.stage.setBackgroundColor(0x000000);

	this.sceneContainer.addChild(main.page);

	this.cleanMemory();

};

Football.prototype.loadMenus = function() {
	// position of 1 and 2 sub menu
	var x1 = 78,
		x2 = 251,
		y = 4;

	this.menuParams = {
		path: Football.COMMONFOLDER,
		background: 'menuskeleton.png',
		images: [
			'tillbaka.png',
			'lasa_meningar.png',
			'korta_meningar.png',
			'bygga_meningar.png'
		],
		positions: [
			18, 133,
			351, 115,
			834, 76,
			1279, 110
		],
		data: {
			1: {
				hover_image: 'menu_selected.png',
				hover_position: [320, -20],
				submenu_image: [
					'sub_menu_1.png',
					'sub_menu_2.png',
				],
				submenu_position: [
					320 + x1, y,
					320 + x2, y
				]

			},
			2: {
				hover_image: 'menu_selected.png',
				hover_position: [786, -20],
				submenu_image: [
					'sub_menu_1.png',
					'sub_menu_2.png',
				],
				submenu_position: [
					786 + x1, y,
					786 + x2, y
				]
			},
			3: {
				hover_image: 'menu_selected.png',
				hover_position: [1255, -20],
				submenu_image: [
					'sub_menu_1.png',
					'sub_menu_2.png',
				],
				submenu_position: [
					1255 + x1, y,
					1255 + x2, y
				]
			},
			pink_image: 'circle.png',
			move_position: {
				x: 10,
				y: 32
			},
			chop: { // optional
				images: [
					'lasa_meningar.png',
					'korta_meningar.png',
					'bygga_meningar.png'
				],
				positions: [
					351, 115,
					834, 76,
					1279, 110
				]
			}
		}

	};
	this.menu = new Menu(this.menuParams);

	this.menuContainer.addChild(this.menu);

	//tillbaka button
	this.menu.getChildAt(1).click = this.menu.getChildAt(1).tap = this.homePage.bind(this);

	//bokstavernai alfabet button
	this.menu.getChildAt(2).click = this.menu.getChildAt(2).tap = this.navMenu1.bind(this);

	//A till P
	this.menu.getChildAt(3).click = this.menu.getChildAt(3).tap = this.navMenu2.bind(this);

	//Q till Ö
	this.menu.getChildAt(4).click = this.menu.getChildAt(4).tap = this.navMenu3.bind(this);



}

Football.prototype.imageButtons = function() {

	this.navPositions = [
		1637, 899,
		837, 585,
		1377, 267
	];

	this.navButtons = [];

	for (var i = 0; i < this.navPositions.length; i++) {
		var imageSprite = PIXI.Sprite.fromFrame('ova.png');

		imageSprite.buttonMode = true;
		imageSprite.position.x = this.navPositions[i * 2];
		imageSprite.position.y = this.navPositions[i * 2 + 1];
		imageSprite.interactive = true;

		this.imageContainer.addChild(imageSprite);

		this.navButtons.push(imageSprite);
		imageSprite = null;
	}

	//Korta Meninger image button
	this.navButtons[1].click = this.navButtons[1].tap = this.navMenu2.bind(this);

	//Lasa Meninger image button
	this.navButtons[0].click = this.navButtons[0].tap = this.navMenu1.bind(this);

	//Bygga Meninger image button
	this.navButtons[2].click = this.navButtons[2].tap = this.navMenu3.bind(this);



	this.sceneContainer.addChild(this.imageContainer);

}

Football.prototype.navMenu = function(submenu1, submenu2, index) {
	this.sceneChange(submenu1);

	var self = this;

	this.pushStuffToHistory("history_other_Football");

	this.subMenu = self.menu.getActiveMenu(index);
	self.menu.moveSelectedExerciseCircle(this.subMenu[0].position);
	self.sceneContainer.addChild(self.menuContainer);

	this.subMenu[0].click = this.subMenu[0].tap = function(data) {
		self.sceneChange(submenu1);

		self.menu.moveSelectedExerciseCircle(this.position);
		self.sceneContainer.addChild(self.menuContainer);

	}
	this.subMenu[1].click = this.subMenu[1].tap = function(data) {
		self.sceneChange(submenu2);

		self.menu.moveSelectedExerciseCircle(this.position);
		self.sceneContainer.addChild(self.menuContainer);
	}
}

Football.prototype.navMenu1 = function() {
	this.navMenu('Football_1_1', 'Football_1_2', 1);
}


Football.prototype.navMenu2 = function() {
	this.navMenu('Football_2_1', 'Football_2_2', 2);
}

Football.prototype.navMenu3 = function() {
	this.navMenu('Football_3_1', 'Football_3_2', 3);
}


Football.prototype.pushStuffToHistory = function(key) {

	if (Device.ieVersion != 9) {
		var exHistory = new History();
		exHistory.PushState({
			scene: key
		});
		exHistory = null;
	}
}

Football.prototype.cleanMemory = function() {
	this.bkg1 = null;
	main.eventRunning = false;
}