function Football_2_2() {
    cl.show()

    PIXI.DisplayObjectContainer.call(this);

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position.set(77, 88);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();
    this.feedbackContainer = new PIXI.DisplayObjectContainer();
    this.animationContainer = new PIXI.DisplayObjectContainer();
    this.helpContainer = new PIXI.DisplayObjectContainer();

    localStorage.setItem('football_2_2', 0);
    main.CurrentExercise = "football_2_2";

    main.eventRunning = false;

    this.introMode = true;
    this.fromOutro = 0;

    main.texturesToDestroy = [];
    main.soundToDestroy = [];

    this.common = new Common();
    this.loadSpriteSheet();


    // everything connected to this is rendered.
    this.addChild(this.sceneContainer);
}

Football_2_2.constructor = Football_2_2;
Football_2_2.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Football_2_2.imagePath = 'build/exercises/football/football_2_2/images/';
Football_2_2.audioPath = 'build/exercises/football/football_2_2/audios/';
Football_2_2.totalFeedback = 10; //Main.TOTAL_FEEDBACK; // default 10

/**
 * load array of assets of different format
 */
Football_2_2.prototype.loadSpriteSheet = function() {

    this.introId_A = loadSound(Football_2_2.audioPath + 'intro_a');
    this.introId_B = loadSound(Football_2_2.audioPath + 'intro_b');
    this.outroId = loadSound(Football_2_2.audioPath + 'outro');
    // create an array of assets to load
    this.assetsToLoad = [
        Football_2_2.imagePath + "sprite_football_2_2_intro.json",
        Football_2_2.imagePath + "sprite_football_2_2_intro.png",
        Football_2_2.imagePath + "sprite_football_2_2_outro.json",
        Football_2_2.imagePath + "sprite_football_2_2_outro.png",
        Football_2_2.imagePath + "sprite_football_2_2_feedback.json",
        Football_2_2.imagePath + "sprite_football_2_2_feedback.png",
        Football_2_2.imagePath + "all_players.png",
        Football_2_2.imagePath + "groundwithallbg.png",
        Football_2_2.imagePath + "goalpost_outro.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
Football_2_2.prototype.spriteSheetLoaded = function() {

    // hide loader
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    // fill exercise background
    this.sceneBackground = new PIXI.Graphics();

    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0x8dc0d4);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);

    this.lightGreenBackground = createRect({
        w: 1838,
        h: 384,
        color: 0xD7E5AF
    });
    this.lightGreenBackground.position.set(3, 3);
    this.sceneContainer.addChild(this.lightGreenBackground);


    //grey background

    this.greenBackground = createRect({
        w: 1838,
        h: 814,
        color: 0x0D8F36
    });
    this.greenBackground.position.set(3, 387);
    this.sceneContainer.addChild(this.greenBackground);

    this.lightGreenBackgroundOutro = createRect({
        w: 1838,
        h: 604,
        color: 0xD7E5AF
    });
    this.lightGreenBackgroundOutro.position.set(3, 3);
    this.sceneContainer.addChild(this.lightGreenBackgroundOutro);
    this.lightGreenBackgroundOutro.visible = false;

    //grey background

    this.greenBackgroundOutro = createRect({
        w: 1838,
        h: 596,
        color: 0x0D8F36
    });
    this.greenBackgroundOutro.position.set(3, 604);
    this.sceneContainer.addChild(this.greenBackgroundOutro);
    this.greenBackgroundOutro.visible = false;

    // exercise instruction
    this.help = PIXI.Sprite.fromFrame("topcorner.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);
    this.helpContainer.addChild(this.help);

    this.introScene();
    this.loadExercise();
    this.outroScene();

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.outroContainer);
    this.sceneContainer.addChild(this.feedbackContainer);
    this.sceneContainer.addChild(this.animationContainer);
    this.sceneContainer.addChild(this.helpContainer);
}

Football_2_2.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    }
    if (isNumeric(number))
        this.hands[number].visible = true;
};

/**
 * play intro animation
 */
Football_2_2.prototype.introAnimation = function() {

    createjs.Sound.stop();

    if (!this.introMode)
        this.restartExercise();

    main.overlay.visible = true;

    if (this.fromOutro == 1) {
        this.fromOutro = 0;

        this.resetFeedback();
        this.restartExercise();
    }

    this.footballTrainer.visible = false;
    this.trainerMouth.visible = false;

    this.help.bringToFront();

    this.outroContainer.visible = false;
    this.introContainer.visible = true;

    this.help.interactive = false;
    this.help.buttonMode = false;

    this.animationContainer.visible = true;

    this.captainMouth.show();

    this.introInstance = createjs.Sound.play(this.introId_A, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.firstIntroCompleteHandler = this.playSecondIntro.bind(this);
    this.introInstance.addEventListener("complete", this.firstIntroCompleteHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
 0-2 sec arms_0
2-5 sec arms 1
stop at arms_0 + stop mouth animation also.

 */
Football_2_2.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    if (currentPostion >= 1000 && currentPostion < 2000) {
        this.showHandAt(0);
    } else if (currentPostion >= 2000 && currentPostion < 5000) {
        this.showHandAt(1);
    } else {
        this.showHandAt(0);
    }
};

Football_2_2.prototype.cancelIntro = function() {

    createjs.Sound.stop();
    clearInterval(this.timerId);

    this.captainMouth.hide(0);
    this.showHandAt(1);

    console.log("cancelIntro");
    this.trainerMouth.hide(6);
    this.animationContainer.visible = false;


    this.help.interactive = true;
    this.help.buttonMode = true;

    this.ovaigen.visible = false;
    main.overlay.visible = false;

};

Football_2_2.prototype.playSecondIntro = function() {

    var self = this;

    this.captainMouth.hide(0);
    this.showHandAt(1);

    clearInterval(this.timerId);

    var secondIntroInstance = createjs.Sound.play(this.introId_B);
    if (createjs.Sound.isReady()) {
        self.footballTrainer.visible = true;
        self.trainerMouth.show();
    }

    this.secondIntroCompleteHandler = this.cancelIntro.bind(this);
    secondIntroInstance.addEventListener("complete", this.secondIntroCompleteHandler);

}

Football_2_2.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.goalKeeperInPosition.show();
        //self.mouthOutroSayingGoal.visible = true;
        self.goalKeeperInPosition.visible = false;
        self.goalKeeperDiving.show();
        var outromouth = setTimeout(function() {
            self.captainMouthOutro.show();
            // self.mouthOutroSayingGoal.visible = false;
        }, 2000);

        var ball = setTimeout(function() {
            self.ballOutro.showOnce();
            self.goalKeeperDiving.hide(2);
        }, 300)
    }
    this.cancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.cancelHandler);
}

Football_2_2.prototype.cancelOutro = function() {

    var self = this;

    createjs.Sound.stop();

    self.defaultFootball.visible = true;
    //  self.mouthOutroSayingGoal.visible = false;
    self.ovaigen.visible = true;
    self.help.interactive = true;
    self.help.buttonMode = true;
    self.captainMouthOutro.hide(0);

    main.overlay.visible = false;


};

Football_2_2.prototype.resetFeedback = function() {
    var self = this;
    localStorage.setItem('football_2_2', 0);
    for (var k = 0; k < self.feedbackContainer.children.length; k++) {
        self.feedbackContainer.children.visible = false;
    }
}



/**
 * create sprites,animation,sound related to intro scene
 */
Football_2_2.prototype.introScene = function() {

    var self = this;
    this.animationContainer.visible = false;

    //football ground with banner
    this.groundWithBanners = PIXI.Sprite.fromImage(Football_2_2.imagePath + "groundwithallbg.png");
    this.groundWithBanners.position.set(3, 3);
    this.introContainer.addChild(this.groundWithBanners);

    //all players
    this.allPlayers = PIXI.Sprite.fromImage(Football_2_2.imagePath + "all_players.png");
    this.allPlayers.position.set(104, 458);
    this.introContainer.addChild(this.allPlayers);



    //intro football captain
    this.footballCaptain = PIXI.Sprite.fromFrame("captainbody.png");
    this.footballCaptain.position.set(320, 168);
    this.animationContainer.addChild(this.footballCaptain);

    this.captainMouth = new Animation({
        "image": "captainmouth",
        "length": 5,
        "x": 557,
        "y": 426,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 5, 2, 4, 1, 5, 3],
        "frameId": 0
    });

    this.animationContainer.addChild(this.captainMouth);

    //intro trainer.
    this.footballTrainer = PIXI.Sprite.fromFrame("trainer.png");
    this.footballTrainer.position.set(1315, 314);
    this.animationContainer.addChild(this.footballTrainer);
    this.trainerMouth = new Animation({
        "image": "trainermouth",
        "length": 4,
        "x": 1484,
        "y": 609,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 2, 4, 1, 3],
        "frameId": 0
    });
    this.footballTrainer.visible = false;
    this.trainerMouth.visible = false;

    this.animationContainer.addChild(this.trainerMouth);

    this.hands = [
        new PIXI.Sprite.fromFrame('arm1.png'),
        new PIXI.Sprite.fromFrame('arm2.png')
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(203, 572);
        this.animationContainer.addChild(this.hands[i]);
        if (i != 0) this.hands[i].visible = false;
    };

    this.showHandAt(0);
    //this.animationContainer.visible = false;
}

/**
 * create sprites,animation,sound related to outro scene
 */
Football_2_2.prototype.outroScene = function() {

    var self = this;

    this.outroContainer.visible = false;

    this.goalPost = PIXI.Sprite.fromImage(Football_2_2.imagePath + "goalpost_outro.png");
    this.goalPost.position.set(3, 96);
    this.outroContainer.addChild(this.goalPost);

    this.goalKeeperInPosition = new Animation({
        "image": "gk_",
        "length": 2,
        "x": 817,
        "y": 396,
        "speed": 3,
        "frameId": 0
    });

    this.outroContainer.addChild(this.goalKeeperInPosition);

    this.goalKeeperDiving = new Animation({
        "image": "gp_side_",
        "length": 3,
        "x": -10,
        "y": 178,
        "speed": 3,
        "frameId": 0
    });

    this.outroContainer.addChild(this.goalKeeperDiving);
    this.goalKeeperDiving.visible = false;

    //outro captain
    this.captainOutro = PIXI.Sprite.fromFrame("captainoutro.png");
    this.captainOutro.position.set(1488, 199);
    this.outroContainer.addChild(this.captainOutro);

    this.captainMouthOutro = new Animation({
        "image": "mouthoutro",
        "length": 5,
        "x": 1592,
        "y": 456,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 5, 2, 4, 1, 5, 3],
        "frameId": 0
    });

    this.outroContainer.addChild(this.captainMouthOutro);
    //this.captainMouthOutro.visible = false;



    /*this.mouthOutroSayingGoal = PIXI.Sprite.fromFrame("mouthoutro5.png");
    this.mouthOutroSayingGoal.position = {
        x: 1592,
        y: 456
    };
    this.outroContainer.addChild(this.mouthOutroSayingGoal);*/



    this.ballOutro = new Animation({
        "image": "outroball",
        "length": 6,
        "x": 667,
        "y": 220,
        "speed": 4,
        "frameId": 5
    });
    this.outroContainer.addChild(this.ballOutro);
    this.ballOutro.visible = false;


    // back to intro page
    this.ovaigen = PIXI.Sprite.fromFrame('ovaigen.png');
    this.ovaigen.position.set(1090, 746);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.ovaigen.visible = false;
    this.outroContainer.addChild(this.ovaigen);
}

/**
 * load exercise related stuffs
 */
Football_2_2.prototype.loadExercise = function() {

    //render green and red feedback folders
    this.redButton = createRect({
        w: 1243,
        h: 148,
        color: 0xff0000
    });

    this.greenButton = createRect({
        w: 1243,
        h: 148,
        color: 0x00ff00
    });

    this.defaultFootball = PIXI.Sprite.fromFrame("football.png");
    this.defaultFootball.position.set(172, 1020);
    this.introContainer.addChild(this.defaultFootball);



    this.redButton.alpha = this.greenButton.alpha = 0.6;
    this.redButton.visible = false;
    this.greenButton.visible = false;
    this.introContainer.addChild(this.redButton);
    this.introContainer.addChild(this.greenButton);

    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.exerciseContainer);

    this.createFeedBack();
    this.initObjectPooling();
}

/**
 * Render feedback to the container
 */
Football_2_2.prototype.createFeedBack = function() {
    var self = this;
    var feedbackPositions = [
        286, 833,
        481, 966,
        631, 795,
        730, 702,
        802, 1010,
        1004, 824,
        1121, 1039,
        1313, 763,
        1403, 973,
        0, 0
    ];
    for (var k = 0; k < 10; k++) {
        var feedbackSprite = PIXI.Sprite.fromFrame("football.png");

        feedbackSprite.position = {
            x: feedbackPositions[k * 2],
            y: feedbackPositions[k * 2 + 1]
        };
        main.texturesToDestroy.push(feedbackSprite);
        this.feedbackContainer.addChild(feedbackSprite);

        feedbackSprite.visible = false;

    }
};

/**
 * manage exercise with new sets randomized
 */
Football_2_2.prototype.runExercise = function() {
    var self = this;
    self.ballOutro.gotoAndStop(0);
    self.ballOutro.visible = false;

    this.jsonData = this.wallSlices[0];
    this.itemsInBanner = [];



    this.whiteGraphics = createRect({
        w: 290,
        h: 98,
        color: 0xffffff,
        lineStyle: 1
    });
    this.whiteRect = this.whiteGraphics.generateTexture();
    this.whiteBoxPositions = [
        375, 335,
        686, 223,
        930, 351,
        1242, 267
    ];
    //x = 740
    var noOfClicked = 0;
    var totalTextWidth = 0;
    var positionArray = [];
    this.textBoxes = [];



    for (var k = 0; k < 4; k++) {
        var drawWhiteBox = new PIXI.Sprite(this.whiteRect);
        drawWhiteBox.position = {
            x: this.whiteBoxPositions[k * 2],
            y: this.whiteBoxPositions[k * 2 + 1]
        };
        //drawWhiteBox.width = drawWhiteBox.height = 335;
        drawWhiteBox.buttonMode = true;
        drawWhiteBox.interactive = true;
        self.exerciseContainer.addChild(drawWhiteBox);


        var words = new PIXI.Text(this.jsonData.words[k], {
            font: "60px Arial"
        });
        words.position = {
            x: 30,
            y: (98 - words.height) / 2 + 10
        };

        drawWhiteBox.addChild(words);
        drawWhiteBox.hitIndex = k;
        drawWhiteBox.itemWidth = drawWhiteBox.width;
        drawWhiteBox.words_positions = this.jsonData.words_positions[k];
        drawWhiteBox.oldPositions = {
            x: this.whiteBoxPositions[k * 2],
            y: this.whiteBoxPositions[k * 2 + 1]
        };

        this.textBoxes.push(drawWhiteBox);
    }

    this.textBoxes[0].click = this.textBoxes[0].tap = this.textBoxes[1].click = this.textBoxes[1].tap = this.textBoxes[2].click = this.textBoxes[2].tap = this.textBoxes[3].click = this.textBoxes[3].tap = function() {
        if (addedListeners) return false;

        //check if user is trying to undo the clicked item - START
        var chk = positionArray.indexOf(this.words_positions);
        if (chk > -1) { //its already clicked item, user just clicked to do undo.
            noOfClicked--;
            if (noOfClicked < 0) noOfClicked = 0;

            totalTextWidth = totalTextWidth - this.itemWidth - 15;
            if (totalTextWidth < 0) totalTextWidth = 0;
            positionArray.splice(chk, 1);
            self.itemsInBanner.splice(chk, 1);

            //move back the textbox to old position
            self.textBoxes[this.hitIndex].position = {
                x: self.textBoxes[this.hitIndex].oldPositions.x,
                y: self.textBoxes[this.hitIndex].oldPositions.y
            };
            self.rearrangeItems();
            return false;
        }
        //check if user is trying to undo the clicked item - END

        var clickedButton = this;


        this.position = {
            x: 358 + totalTextWidth,
            y: 55
        };

        //this.interactive = false;
        noOfClicked++;
        totalTextWidth = totalTextWidth + this.itemWidth + 15;
        positionArray.push(this.words_positions);
        self.itemsInBanner.push(this.hitIndex);


        if (noOfClicked >= 4) {
            var checkArray = self.jsonData.answers.join() === positionArray.join()

            if (checkArray) { //correct answer
                var fb = setTimeout(function() {
                    correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                    self.showCorrectSentence(self.jsonData.sentence);
                    self.common.showRedOrGreenBox(358, 37, self.greenButton);
                }, 800);

                var a = setTimeout(function() {
                    self.correctFeedback();
                }, 2200);

            } else { //wrong answer
                var fb = setTimeout(function() {
                    wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                    self.common.showRedOrGreenBox(358, 37, self.redButton);
                }, 800);

                var a = setTimeout(function() {
                    for (var k = 0; k < 4; k++) {
                        self.textBoxes[k].position = {
                            x: self.textBoxes[k].oldPositions.x,
                            y: self.textBoxes[k].oldPositions.y
                        };
                        self.textBoxes[k].interactive = true;
                    }
                    self.wrongFeedback();
                }, 2000);

            }
            noOfClicked = 0;
            totalTextWidth = 0;
            positionArray = [];
            self.itemsInBanner = [];
        }


    }
}

Football_2_2.prototype.rearrangeItems = function() {
    var idx, idx2;
    for (var k = 0; k < this.itemsInBanner.length; k++) {
        idx = this.itemsInBanner[k];
        if (k == 0) {
            this.textBoxes[idx].position = {
                x: 358,
                y: 55
            };

        } else {
            idx2 = this.itemsInBanner[k - 1];
            this.textBoxes[idx].position = {
                x: this.textBoxes[idx2].position.x + this.textBoxes[idx2].width + 15,
                y: 55
            };
        }
    }
};

Football_2_2.prototype.showCorrectSentence = function(txt) {
    for (var i = this.exerciseContainer.children.length - 1; i >= 0; i--) {
        this.exerciseContainer.removeChild(this.exerciseContainer.children[i]);
    };

    var correctSentence = new PIXI.Text(txt, {
        font: "60px Arial"
    });

    correctSentence.position = {
        x: 344 + (1243 - correctSentence.width) / 2,
        y: 36 + (148 - correctSentence.height) / 2
    };

    this.exerciseContainer.addChild(correctSentence);
};


/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
Football_2_2.prototype.correctFeedback = function() {
    var self = this;
    var feedback = localStorage.getItem('football_2_2');
    feedback++;

    localStorage.setItem('football_2_2', feedback);
    this.updateFeedback();

    if (feedback >= Football_2_2.totalFeedback) {
        setTimeout(this.onComplete.bind(this), 100);
    } else {
        setTimeout(function() {
            self.getNewExerciseSet();
            main.eventRunning = false;
        }, 400);
    }
}

Football_2_2.prototype.wrongFeedback = function() {

    var feedback = localStorage.getItem('football_2_2');
    feedback--;

    if (feedback < 0)
        feedback = 0;

    localStorage.setItem('football_2_2', feedback);
    this.updateFeedback();
}


Football_2_2.prototype.updateFeedback = function() {

    var self = this;

    var counter = localStorage.getItem('football_2_2');

    for (var i = 0; i < self.feedbackContainer.children.length; i++) {
        self.feedbackContainer.children[i].visible = false;
    }

    if (counter > 0) {
        self.feedbackContainer.children[counter - 1].visible = true;
        self.defaultFootball.visible = false;
    } else if (counter == 10) {
        self.feedbackContainer.children[9].visible = false;
    } else {
        self.defaultFootball.visible = true;
    }

}

/**
 * prefetch exercise data and cache number of sets defined only.
 */
Football_2_2.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/football/football_2_2.json?nocache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        self.pool = new ExerciseSpritesPool(loader.json);
        //borrow two sprites once
        self.borrowWallSprites(2);
        self.runExercise();
    });
    loader.load();
}

Football_2_2.prototype.borrowWallSprites = function(num) {

    var self = this;

    for (var i = 0; i < num; i++) {

        var sprite = this.pool.borrowSprite(),
            spriteObj = sprite[0],
            wordsArray = [],
            answerArray = [];

        this.poolSlices.push(sprite);

        for (var k = 0; k < spriteObj.sentence_word.length; k++) {
            wordsArray.push(spriteObj.sentence_word[k]);
            answerArray.push(spriteObj.word_position[k]);
        }

        shuffle(wordsArray, answerArray);

        this.wallSlices.push({
            words: wordsArray,
            words_positions: answerArray,
            answers: spriteObj.word_position,
            sentence: spriteObj.sentence
        });
    }
};

Football_2_2.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];

    this.pool.returnSprite(sprite);

    this.poolSlices = [];
    this.wallSlices.shift();
};

Football_2_2.prototype.getNewExerciseSet = function() {

    this.returnWallSprites();
    this.borrowWallSprites(1);
    for (var i = this.exerciseContainer.children.length - 1; i >= 0; i--) {
        this.exerciseContainer.removeChild(this.exerciseContainer.children[i]);
    };
    this.runExercise();
}

Football_2_2.prototype.restartExercise = function() {
    this.feedbackContainer.visible = true;
    for (var i = this.feedbackContainer.children.length - 1; i >= 0; i--) {
        this.feedbackContainer.children[i].visible = false;
    };

    this.greenBackgroundOutro.visible = false;
    this.lightGreenBackgroundOutro.visible = false;

    this.greenBackground.visible = true;
    this.lightGreenBackground.visible = true;

    localStorage.setItem('football_2_2', 0);
    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    this.ovaigen.visible = false;
    this.introMode = true;

    this.getNewExerciseSet();
};

/**
 * the outro scene is displayed
 */
Football_2_2.prototype.onComplete = function() {
    var self = this;

    this.introMode = false;

    this.help.interactive = false;
    this.help.buttonMode = false;
    this.introContainer.visible = false;
    this.outroContainer.visible = true;
    this.animationContainer.visible = false;
    this.feedbackContainer.visible = false;
    this.goalKeeperInPosition.visible = true;
    this.goalKeeperDiving.visible = false;
    this.fromOutro = 1;

    this.greenBackgroundOutro.visible = true;
    this.lightGreenBackgroundOutro.visible = true;

    this.greenBackground.visible = false;
    this.lightGreenBackground.visible = false;

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        this.visible = false;
        self.fromOutro = 0;
        main.eventRunning = false;
        self.cancelOutro();
        self.restartExercise();
    }

    this.outroAnimation();

}

/**
 * clean memory by clearing reference of object
 */
Football_2_2.prototype.cleanMemory = function() {

    this.introMode = null;

    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    if (this.assetsToLoads)
        this.assetsToLoad.length = 0;

    this.fromOutro = null;

    this.help = null;


    this.ovaigen = null;

    if (this.jsonData)
        this.jsonData.length = 0;

    this.counter = null;

    if (this.wallSlices)
        this.wallSlices.length = 0;

    if (this.poolSlices)
        this.poolSlices.length = 0;

    this.pool = null;
    this.num = null;

    this.introContainer = null;
    this.outroContainer = null;
    this.feedbackContainer = null;
    this.animationContainer = null;
    this.helpContainer = null;

    this.common = null;

    this.redButton = null;
    this.greenButton = null;

    this.exerciseContainer = null;


    if (this.whiteBoxPositions)
        this.whiteBoxPositions.length = 0;


    this.outroId = null;

    this.introId_A = null;
    this.introId_B = null;

    if (this.lightGreenBackground)
        this.lightGreenBackground.clear();
    this.lightGreenBackground = null;

    if (this.greenBackground)
        this.greenBackground.clear();
    this.greenBackground = null;

    if (this.lightGreenBackgroundOutro)
        this.lightGreenBackgroundOutro.clear();
    this.lightGreenBackgroundOutro = null;

    if (this.greenBackgroundOutro)
        this.greenBackgroundOutro.clear();
    this.greenBackgroundOutro = null;

    this.groundWithBanners = null;
    this.allPlayers = null;

    if (this.introBackground)
        this.introBackground.clear();
    this.introBackground = null;

    this.footballCaptain = null;
    this.captainMouth = null;

    this.footballTrainer = null;
    this.trainerMouth = null;

    this.goalPost = null;
    this.goalKeeperInPosition = null;
    this.goalKeeperDiving = null;

    this.captainOutro = null;
    this.captainMouthOutro = null;
    this.ballOutro = null;

    if (this.textBoxes)
        this.textBoxes.length = 0;


    if (this.whiteGraphics)
        this.whiteGraphics.clear();
    this.whiteGraphics = null;

    this.whiteRect = null;

    if (this.whiteBoxPositions)
        this.whiteBoxPositions.length = 0;


    for (var i = this.sceneContainer.children.length - 1; i >= 0; i--) {
        this.sceneContainer.removeChild(this.sceneContainer.children[i]);
    };
    this.sceneContainer = null;

}