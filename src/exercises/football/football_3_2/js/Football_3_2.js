function Football_3_2() {
    cl.show()

    PIXI.DisplayObjectContainer.call(this);

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position.set(77, 88);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();
    this.feedbackContainer = new PIXI.DisplayObjectContainer();
    this.animationContainer = new PIXI.DisplayObjectContainer();
    this.helpContainer = new PIXI.DisplayObjectContainer();

    localStorage.setItem('football_3_2', 0);
    main.CurrentExercise = "football_3_2";

    this.fromOutro = 0;

    main.eventRunning = false;

    this.introMode = true;

    main.texturesToDestroy = [];
    main.soundToDestroy = [];

    this.common = new Common();
    this.loadSpriteSheet();

    // everything connected to this is rendered.
    this.addChild(this.sceneContainer);
}

Football_3_2.constructor = Football_3_2;
Football_3_2.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Football_3_2.imagePath = 'build/exercises/football/football_3_2/images/';
Football_3_2.audioPath = 'build/exercises/football/football_3_2/audios/';
Football_3_2.totalFeedback = 5;

/**
 * load array of assets of different format
 */
Football_3_2.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(Football_3_2.audioPath + 'intro');
    this.outroId = loadSound(Football_3_2.audioPath + 'outro');
    // create an array of assets to load
    this.assetsToLoad = [
        Football_3_2.imagePath + "sprite_football_3_2.json",
        Football_3_2.imagePath + "sprite_football_3_2.png",
        Football_3_2.imagePath + "backgroundoutro.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
Football_3_2.prototype.spriteSheetLoaded = function() {

    // hide loader
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    // fill exercise background
    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0x94C11F);
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0x8dc0d4);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);


    //add goalkeeper
    this.goalKeeper = new Animation({
        "image": "goalie",
        "length": 2,
        "x": 118,
        "y": 596,
        "speed": 0.4,
        "frameId": 0
    });
    this.introContainer.addChild(this.goalKeeper);
    this.goalKeeper.show();

    // exercise instruction
    this.help = PIXI.Sprite.fromFrame("topcorner.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);
    this.helpContainer.addChild(this.help);

    this.loadExercise();
    this.introScene();
    this.outroScene();

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.outroContainer);
    this.sceneContainer.addChild(this.animationContainer);
    this.sceneContainer.addChild(this.feedbackContainer);
    this.sceneContainer.addChild(this.helpContainer);
}

Football_3_2.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    }
    if (isNumeric(number))
        this.hands[number].visible = true;
};

/**
 * play intro animation
 */
Football_3_2.prototype.introAnimation = function() {

    createjs.Sound.stop();

    main.overlay.visible = true;

    if (this.fromOutro == 1) {
        this.fromOutro = 0;
        this.resetFeedback();
        this.restartExercise();
    }

    this.outroContainer.visible = false;
    this.introContainer.visible = true;

    this.help.interactive = false;
    this.help.buttonMode = false;

    this.animationContainer.visible = true;
    this.goalKeeper.visible = false;
    this.mouth.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.

 0-2 sec arm_rest => 3
2-4 sec arm_0 => 0
4-6 sec arm_up => 2
6-8 sec arm_down => 1
8-10 sec arm_rest => 3

 */
Football_3_2.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    if (currentPostion >= 1000 && currentPostion < 2000) {
        this.showHandAt(3);
    } else if (currentPostion >= 2000 && currentPostion < 4000) {
        this.showHandAt(0);
    } else if (currentPostion >= 4000 && currentPostion < 6000) {
        this.showHandAt(2);
    } else if (currentPostion >= 6000 && currentPostion < 8000) {
        this.showHandAt(1);
    } else {
        this.showHandAt(3);
    }
};

/**
 * cancel running intro animation
 */
Football_3_2.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    this.animationContainer.visible = false;
    clearInterval(this.timerId);
    this.mouth.hide(2);
    this.showHandAt(3);

    this.help.interactive = true;
    this.help.buttonMode = true;
    this.goalKeeper.visible = true;

    this.ovaigen.visible = false;
    main.overlay.visible = false;
}

Football_3_2.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.mouthOutro.show();
    }

    this.cancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.cancelHandler);
}

Football_3_2.prototype.cancelOutro = function() {

    var self = this;

    createjs.Sound.stop();
    self.mouthOutro.hide(0);
    self.ovaigen.visible = true;
    self.help.interactive = true;
    self.help.buttonMode = true;
    main.overlay.visible = false;

};

Football_3_2.prototype.resetFeedback = function() {
    var self = this;
    localStorage.setItem('football_3_2', 0);
    for (var k = 0; k < self.feedbackContainer.children.length; k++) {
        self.feedbackContainer.children.alpha = 0.1;
    }
}



/**
 * create sprites,animation,sound related to intro scene
 */
Football_3_2.prototype.introScene = function() {

    var self = this;

    this.blueRoundBackground = PIXI.Texture.fromFrame("bluebox.png");
    //main.texturesToDestroy.push(this.blueRoundBackground);

    this.goalieHandLeft = new PIXI.Sprite.fromFrame("arm_left.png");
    this.goalieHandLeft.position.set(964, 387);
    this.animationContainer.addChild(this.goalieHandLeft);
    main.texturesToDestroy.push(this.goalieHandLeft);



    this.goalKeeperIntro = new PIXI.Sprite.fromFrame('goalieintro.png');
    this.goalKeeperIntro.position.set(945, 127 + 8);
    main.texturesToDestroy.push(this.goalKeeperIntro);
    this.animationContainer.addChild(this.goalKeeperIntro);
    this.animationContainer.visible = false;

    this.hands = [
        new PIXI.Sprite.fromFrame('arm_1.png'),
        new PIXI.Sprite.fromFrame('arm_2.png'),
        new PIXI.Sprite.fromFrame('arm_3.png'),
        new PIXI.Sprite.fromFrame('arm_0.png')
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(586, 284);
        this.animationContainer.addChild(this.hands[i]);
        if (i != 0) this.hands[i].visible = false;
    };

    this.showHandAt(0);

    this.mouth = new Animation({
        "image": "mouth",
        "length": 5,
        "x": 980,
        "y": 389 + 15,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 5, 2, 4, 1, 5, 3],
        "frameId": 0
    });

    this.animationContainer.addChild(this.mouth);

    //eyes

    this.eyeParams = new Animation({
        "image": "eyes_intro_",
        "length": 2,
        "x": 992,
        "y": 289 + 15,
        "speed": 2,
        "pattern": true,
        "sequence": [1, 2, 2, 1, 1, 2, 2, 1, 2, 1, 1, 2],
        "frameId": 2
    });
    this.animationContainer.addChild(this.eyeParams);
    //this.animationContainer.visible = false;

    //4 textboxes for 4 sentences
    this.whiteGraphics = createRect({
        w: 1203,
        h: 102,
        color: 0xffffff,
        lineStyle: 1
    });
    this.whiteRect = this.whiteGraphics.generateTexture();
    this.whiteBoxPositions = [644, 759, 874, 989]; //x=375

    this.sentenceBox = [];

    for (var k = 0; k < 4; k++) {
        var textBox = new PIXI.Sprite(this.whiteRect);
        textBox.position = {
            x: 375,
            y: this.whiteBoxPositions[k]
        };
        self.introContainer.addChild(textBox);

        var charName = new PIXI.Text("", {
            font: "60px Arial",
            fill: "black"
        });

        charName.position = {
            x: 40,
            y: (textBox.height - charName.height) / 2 + 5
        };
        textBox.chars = charName;
        textBox.addChild(charName);
        textBox.alpha = 0.2;
        this.sentenceBox.push(textBox);
    }


}

/**
 * create sprites,animation,sound related to outro scene
 */
Football_3_2.prototype.outroScene = function() {

    var self = this;

    this.outroContainer.visible = false;
    this.footballGround = PIXI.Sprite.fromImage(Football_3_2.imagePath + "backgroundoutro.png");
    this.footballGround.position.set(3, 3);
    this.outroContainer.addChild(this.footballGround);
    main.texturesToDestroy.push(this.footballGround);

    this.outroGoalie = PIXI.Sprite.fromFrame("goalie.png");
    this.outroGoalie.position.set(153, 106);
    this.outroContainer.addChild(this.outroGoalie);
    main.texturesToDestroy.push(this.outroGoalie);

    this.mouthOutro = new Animation({
        "image": "mouthoutro",
        "length": 6,
        "x": 563,
        "y": 257,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 5, 6, 2, 4, 1, 5, 3, 6],
        "frameId": 0
    });

    this.outroContainer.addChild(this.mouthOutro);

    // back to intro page
    this.ovaigen = PIXI.Sprite.fromFrame('ovaigen.png');
    this.ovaigen.position.set(1135, 660);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.ovaigen.visible = false;
    this.outroContainer.addChild(this.ovaigen);
}

/**
 * load exercise related stuffs
 */
Football_3_2.prototype.loadExercise = function() {

    //render green and red feedback folders
    this.redButton = createRect({
        w: 1201,
        h: 100,
        color: 0xff0000
    });
    this.greenButton = createRect({
        w: 1201,
        h: 100,
        color: 0x00ff00
    });

    this.redButton.alpha = this.greenButton.alpha = 0.6;
    this.redButton.visible = false;
    this.greenButton.visible = false;
    this.introContainer.addChild(this.redButton);
    this.introContainer.addChild(this.greenButton);



    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.exerciseContainer);

    this.createFeedBack();
    this.initObjectPooling();


}

/**
 * Render feedback to the container
 */
Football_3_2.prototype.createFeedBack = function() {
    var self = this;
    for (var k = 0; k < 5; k++) {
        var feedbackSprite = PIXI.Sprite.fromFrame("feedback.png");

        feedbackSprite.position = {
            x: 1708,
            y: 1042 - (k * 121)
        };
        main.texturesToDestroy.push(feedbackSprite);
        this.feedbackContainer.addChild(feedbackSprite);
        feedbackSprite.alpha = 0.1;
    }
};

/**
 * manage exercise with new sets randomized
 */
Football_3_2.prototype.runExercise = function() {

    var self = this;

    this.jsonData = this.wallSlices[0];
    this.debugAnswers = this.jsonData.answerSentences;

    this.clickedButtons = [];
    this.currentSentenceIndex = [];
    this.clickedText = [];
    for (var k = 0; k < 4; k++) {
        if (k == 0) {
            self.sentenceBox[0].alpha = 1;
            this.sentenceBox[k].chars.setText("" + this.jsonData.character_names[k] + " __");
        } else {
            this.sentenceBox[k].chars.setText("" + this.jsonData.character_names[k]);
        }
        this.clickedText.push(this.jsonData.character_names[k]);
    }

    var blueBoxPositions = [
        5, 5,
        308, 5,
        615, 5,
        917, 5,
        1219, 5,
        1526, 5,
        5, 264,
        308, 264,
        615, 264,
        917, 264,
        1219, 264,
        1526, 264

    ];
    var currentSentence = 0;
    var noOfClicked = 0;
    var noOfCorrectAnswer = 0;
    for (var k = 0; k < this.jsonData.broken_words.length; k++) {
        var drawBox = new PIXI.Sprite(this.blueRoundBackground);
        drawBox.position = {
            x: blueBoxPositions[k * 2],
            y: blueBoxPositions[k * 2 + 1]
        };
        this.exerciseContainer.addChild(drawBox);
        drawBox.interactive = true;
        drawBox.buttonMode = true;

        //add words
        var words = new PIXI.Text(this.jsonData.broken_words[k], {
            font: "50px Arial"
        });
        words.position = {
            x: (drawBox.width - words.width) / 2,
            y: (drawBox.height - words.height) / 2
        };
        drawBox.addChild(words);
        drawBox.itemClicked = this.jsonData.broken_words[k];
        drawBox.hitIndex = k;
        this.clickedButtons.push(drawBox);

    }


    for (var k = 0; k < this.clickedButtons.length; k++) {
        this.clickedButtons[k].click = this.clickedButtons[k].tap = function() {
            if (addedListeners) return false;

            if (main.eventRunning) {
                return false;
            }

            if (noOfClicked >= 2) {
                main.eventRunning = true;
            }
           // console.log("NO OF CLICKED = " + noOfClicked);

            var btn = this;
            self.currentSentenceIndex.push(this.hitIndex);
            self.clickedText[currentSentence] = (this.itemClicked == ".") ? self.clickedText[currentSentence] + this.itemClicked : self.clickedText[currentSentence] + " " + this.itemClicked;

            if (this.itemClicked == ".") {
                self.sentenceBox[currentSentence].chars.setText("" + self.clickedText[currentSentence]);
            } else {
                self.sentenceBox[currentSentence].chars.setText("" + self.clickedText[currentSentence] + " __");
            }
            btn.alpha = 0.4;
            btn.interactive = false;
            noOfClicked++;
            if (noOfClicked >= 3) {

                var finalSentence = self.clickedText[currentSentence];
                finalSentence = finalSentence.replace(self.jsonData.character_names[currentSentence] + " ", "");
                finalSentence = finalSentence.replace(" .", ".");

                if (finalSentence == self.jsonData.answerSentences[0] || finalSentence == self.jsonData.answerSentences[1] || finalSentence == self.jsonData.answerSentences[2] || finalSentence == self.jsonData.answerSentences[3]) { //CORRECT ANSWER
                    var a = setTimeout(function() {
                        correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                        self.common.showRedOrGreenBox(self.sentenceBox[currentSentence].position.x, self.sentenceBox[currentSentence].position.y, self.greenButton);
                        currentSentence++;
                        noOfCorrectAnswer++;
                        main.eventRunning = false;
                        noOfClicked = 0;
                        main.eventRunning = false;
                        if (noOfCorrectAnswer >= 4) {
                            var b = setTimeout(function() {
                                self.correctFeedback();
                            }, 2000);
                            //update feedback here
                        } else {
                            self.currentSentenceIndex = [];
                            self.sentenceBox[currentSentence].alpha = 1;
                            self.sentenceBox[currentSentence].chars.setText("" + self.jsonData.character_names[currentSentence] + " __");
                        }
                    }, 1200);

                } else { //WRONG ANSWER
                    var a = setTimeout(function() {
                        wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                        self.common.showRedOrGreenBox(self.sentenceBox[currentSentence].position.x, self.sentenceBox[currentSentence].position.y, self.redButton);
                    }, 1200);

                    var b = setTimeout(function() {
                        self.sentenceBox[currentSentence].chars.setText("" + self.jsonData.character_names[currentSentence] + " __");
                        self.clickedText[currentSentence] = self.jsonData.character_names[currentSentence];
                        main.eventRunning = false;
                        self.wrongFeedback();
                    }, 2000);

                    noOfClicked = 0;
                    //main.eventRunning = false;
                }

            }


        }
    }
}



/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
Football_3_2.prototype.correctFeedback = function() {
    var self = this;
    var feedback = localStorage.getItem('football_3_2');
    feedback++;

    localStorage.setItem('football_3_2', feedback);
    this.updateFeedback();

    if (feedback >= Football_3_2.totalFeedback) {
        setTimeout(this.onComplete.bind(this), 2000);
    } else {
        setTimeout(function() {
            self.getNewExerciseSet();
            main.eventRunning = false;
        }, 400);
    }
}

Football_3_2.prototype.wrongFeedback = function() {
    console.log("test");
    for (var j = 0; j < this.currentSentenceIndex.length; j++) {
        this.clickedButtons[this.currentSentenceIndex[j]].alpha = 1;
        this.clickedButtons[this.currentSentenceIndex[j]].interactive = true;
    }

}


Football_3_2.prototype.updateFeedback = function() {

    var self = this;

    var counter = localStorage.getItem('football_3_2');

    for (var i = 0; i < self.feedbackContainer.children.length; i++) {
        self.feedbackContainer.children[i].alpha = 0.1;
    }

    for (var j = 0; j < counter; j++) {
        self.feedbackContainer.children[j].alpha = 1;
    }
}

/**
 * prefetch exercise data and cache number of sets defined only.
 */
Football_3_2.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/football/football_3_2.json?nocache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        self.pool = new ExerciseSpritesPool(loader.json);
        //borrow two sprites once
        self.borrowWallSprites(2);
        self.runExercise();
    });
    loader.load();
}

Football_3_2.prototype.borrowWallSprites = function(num) {

    var self = this;

    for (var i = 0; i < num; i++) {

        var sprite = this.pool.borrowSprite(),
            spriteObj = sprite[0],
            brokenWordsArr = [];
            //charNamesArr = [];

        this.poolSlices.push(sprite);

        for (var k = 0; k < spriteObj.broken_words.length; k++) {
            brokenWordsArr.push(spriteObj.broken_words[k]);
        }

       /* for (var k = 0; k < 4; k++) {
            charNamesArr.push(spriteObj.character_names[k]);
        }*/

        shuffle(brokenWordsArr);
        //shuffle(charNamesArr);

        this.wallSlices.push({
            answerSentences: spriteObj.exercise_sentences,
            broken_words: brokenWordsArr,
            character_names: this.getRandomNames()
        });
    }
};

Football_3_2.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];

    this.pool.returnSprite(sprite);

    this.poolSlices = [];
    this.wallSlices.shift();
};

Football_3_2.prototype.getNewExerciseSet = function() {

    this.returnWallSprites();
    this.borrowWallSprites(1);
    for (var i = this.exerciseContainer.children.length - 1; i >= 0; i--) {
        this.exerciseContainer.removeChild(this.exerciseContainer.children[i]);
    };
    this.runExercise();
}

Football_3_2.prototype.restartExercise = function() {
    this.feedbackContainer.visible = true;
    for (var i = this.feedbackContainer.children.length - 1; i >= 0; i--) {
        this.feedbackContainer.children[i].alpha = 0.1;
    };

    localStorage.setItem('football_3_2', 0);
    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    this.ovaigen.visible = false;
    this.introMode = true;

    this.getNewExerciseSet();
};

/**
 * the outro scene is displayed
 */
Football_3_2.prototype.onComplete = function() {
    var self = this;

    this.introMode = false;

    this.help.interactive = false;
    this.help.buttonMode = false;
    this.introContainer.visible = false;
    this.outroContainer.visible = true;
    this.animationContainer.visible = false;
    this.feedbackContainer.visible = false;
    this.fromOutro = 1;

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        this.visible = false;
        main.eventRunning = false;
        self.cancelOutro();
        self.fromOutro = 0;
        self.restartExercise();
    }

    this.outroAnimation();

}

Football_3_2.prototype.getRandomNames = function(){
    var arrayNames = ["Ada","Adam","Ali","Amira","Ann","Anton","Bea","Cesar","David","Dina","Dora","Elsa","Eva","Fabian","Fatima","Fia","Hanna","Hassan","Ida","Ines","Jan","Jennifer","Jesper","Jim","Josef","Karin","Kia","Kim","Lea","Leila","Leo","Liam","Lisa","Maria","Mia","Moa","Måns","Nona","Nora","Ola","Oskar","Pia","Pål","Pär","Rafael","Rod","Rosy","Rune","Sam","Sara","Soran","Sune","Tanja","Tim","Tina","Tom","Tore","Tyra","Ulf","Viktor"];
    shuffle(arrayNames);
    var retArr = [];

    for(var k=0;k<4;k++){
        retArr.push(arrayNames[k]);
    }

    return retArr;
}

/**
 * clean memory by clearing reference of object
 */
Football_3_2.prototype.cleanMemory = function() {

    this.introMode = null;

    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    if (this.assetsToLoads)
        this.assetsToLoad.length = 0;


    this.help = null;
    this.fromOutro = null;

    this.ovaigen = null;

    if (this.jsonData)
        this.jsonData.length = 0;

    this.counter = null;

    if (this.wallSlices)
        this.wallSlices.length = 0;

    if (this.poolSlices)
        this.poolSlices.length = 0;

    this.pool = null;
    this.num = null;

    this.introContainer = null;
    this.outroContainer = null;
    this.feedbackContainer = null;
    this.animationContainer = null;

    if (this.hands)
        this.hands.length = 0;

    this.common = null;

    this.redButton = null;
    this.greenButton = null;
    this.exerciseContainer = null;

    this.introId = null;
    this.outroId = null;

    this.helpContainer = null;
    this.goalKeeper = null;
    this.blueRoundBackground = null;
    this.goalKeeperIntro = null;
    this.mouth = null;
    this.eyeParams = null;

    if (this.whiteGraphics)
        this.whiteGraphics.clear();
    this.whiteGraphics = null;

    this.whiteRect = null;

    if (this.sentenceBox) this.sentenceBox.length = 0;
    if (this.whiteBoxPositions) this.whiteBoxPositions.length = 0;
    if (this.clickedButtons) this.clickedButtons.length = 0;
    if (this.currentSentenceIndex) this.currentSentenceIndex.length = 0;
    if (this.clickedText) this.clickedText.length = 0;

    this.footballGround = null;
    this.outroGoalie = null;
    this.mouthOutro = null;

    for (var i = this.sceneContainer.children.length - 1; i >= 0; i--) {
        this.sceneContainer.removeChild(this.sceneContainer.children[i]);
    };
    this.sceneContainer = null;

}