function Football_1_1() {
    cl.show()

    PIXI.DisplayObjectContainer.call(this);

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position.set(77, 88);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();
    this.feedbackContainer = new PIXI.DisplayObjectContainer();
    this.animationContainer = new PIXI.DisplayObjectContainer();

    localStorage.setItem('football_1_1', 0);
    main.CurrentExercise = "football_1_1";

    this.fromOutro = 0;

    this.introMode = true;

    main.texturesToDestroy = [];
    main.soundToDestroy = [];

    this.common = new Common();
    this.loadSpriteSheet();


    // everything connected to this is rendered.
    this.addChild(this.sceneContainer);
}

Football_1_1.constructor = Football_1_1;
Football_1_1.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Football_1_1.imagePath = 'build/exercises/football/football_1_1/images/';
Football_1_1.audioPath = 'build/exercises/football/football_1_1/audios/';
Football_1_1.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

/**
 * load array of assets of different format
 */
Football_1_1.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(Football_1_1.audioPath + 'intro');
    this.outroId = loadSound(Football_1_1.audioPath + 'outro');
    // create an array of assets to load
    this.assetsToLoad = [
        Football_1_1.imagePath + "sprite_football_1_1.json",
        Football_1_1.imagePath + "sprite_football_1_1.png",
        Football_1_1.imagePath + "ground.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
Football_1_1.prototype.spriteSheetLoaded = function() {

    // hide loader
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    // fill exercise background
    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0xCBBB9F);
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0x8dc0d4);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);

    this.footballGround = new PIXI.Sprite.fromImage(Football_1_1.imagePath + "ground.png");
    this.footballGround.position = {
        x: 4,
        y: 104
    };
    main.texturesToDestroy.push(this.footballGround);
    this.sceneContainer.addChild(this.footballGround);

    //add goalkeeper
    this.goalKeeper = PIXI.Sprite.fromFrame("goalkeeper.png");
    this.goalKeeper.position = {
        x: 1343,
        y: 212
    };
    main.texturesToDestroy.push(this.goalKeeper);
    this.introContainer.addChild(this.goalKeeper);

    // exercise instruction
    this.help = PIXI.Sprite.fromFrame("topcorner.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);
    this.sceneContainer.addChild(this.help);

    this.loadExercise();
    this.introScene();
    this.outroScene();

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.outroContainer);
    this.sceneContainer.addChild(this.feedbackContainer);
    this.sceneContainer.addChild(this.animationContainer);
}

Football_1_1.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    }
    if (isNumeric(number))
        this.hands[number].visible = true;
};

/**
 * play intro animation
 */
Football_1_1.prototype.introAnimation = function() {

    createjs.Sound.stop();

    main.overlay.visible = true;

    if (this.fromOutro == 1) {
        this.fromOutro = 0;
        this.resetFeedback();
        this.restartExercise();
    }

    this.outroContainer.visible = false;
    this.introContainer.visible = true;

    this.help.interactive = false;
    this.help.buttonMode = false;

    this.animationContainer.visible = true;
    this.goalKeeper.visible = false;
    this.mouth.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
 0-2 sec    ARM_1  => 0
2-3 SEC ARM_POINTING_AT_WORDS => 2
3-4 SEC ARM_POINTING_AT_IMAGE => 1
STOP AT ARM_1 => 0


 */
Football_1_1.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    if (currentPostion >= 2000 && currentPostion < 3000) {
        this.showHandAt(0);
    } else if (currentPostion >= 3000 && currentPostion < 4000) {
        this.showHandAt(2);
    } else if (currentPostion >= 4000 && currentPostion < 5000) {
        this.showHandAt(1);
    } else {
        this.showHandAt(0);
    }
};

/**
 * cancel running intro animation
 */
Football_1_1.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);

    this.animationContainer.visible = false;
    this.mouth.hide(2);
    this.showHandAt(2);
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.goalKeeper.visible = true;


    this.ovaigen.visible = false;
    main.overlay.visible = false;
    addedListeners = false;
}

Football_1_1.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.playersGroup.show();
    }
    this.cancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.cancelHandler);
}

Football_1_1.prototype.cancelOutro = function() {

    var self = this;

    createjs.Sound.stop();
    self.ovaigen.visible = true;
    self.help.interactive = true;
    self.help.buttonMode = true;
    main.overlay.visible = false;

};

Football_1_1.prototype.resetFeedback = function() {
    var self = this;
    localStorage.setItem('football_1_1', 0);
    for (var k = 0; k < self.feedbackContainer.children.length; k++) {
        self.feedbackContainer.children.visible = false;
    }
}



/**
 * create sprites,animation,sound related to intro scene
 */
Football_1_1.prototype.introScene = function() {

    var self = this;

    this.goalKeeperIntro = new PIXI.Sprite.fromFrame('goalkeeper_intro.png');
    this.goalKeeperIntro.position.set(1515, 202);
    main.texturesToDestroy.push(this.goalKeeperIntro);
    this.animationContainer.addChild(this.goalKeeperIntro);
    this.animationContainer.visible = false;

    this.mouth = new Animation({
        "image": "mouth",
        "length": 4,
        "x": 1559,
        "y": 366,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 2, 4, 1, 3],
        "frameId": 0
    });

    this.animationContainer.addChild(this.mouth);

    this.hands = [
        new PIXI.Sprite.fromFrame('arm1.png'),
        new PIXI.Sprite.fromFrame('arm2.png'),
        new PIXI.Sprite.fromFrame('arm3.png'),
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(941, 456);
        this.animationContainer.addChild(this.hands[i]);
        if (i != 0) this.hands[i].visible = false;
    };

    this.showHandAt(0);
    //this.animationContainer.visible = false;
}

/**
 * create sprites,animation,sound related to outro scene
 */
Football_1_1.prototype.outroScene = function() {

    var self = this;

    this.outroContainer.visible = false;
    this.playersGroup = new Animation({
        "image": "pile",
        "length": 4,
        "x": 348,
        "y": 356,
        "speed": 3,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 2, 4, 1, 3],
        "frameId": 0
    });

    this.outroContainer.addChild(this.playersGroup);

    // back to intro page
    this.ovaigen = PIXI.Sprite.fromFrame('ovaigen.png');
    this.ovaigen.position.set(1153, 719);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.ovaigen.visible = false;
    this.outroContainer.addChild(this.ovaigen);
}

/**
 * load exercise related stuffs
 */
Football_1_1.prototype.loadExercise = function() {

    //render green and red feedback folders
    this.redButton = createRect({
        w: 507,
        h: 94,
        color: 0xff0000
    });
    this.greenButton = createRect({
        w: 507,
        h: 94,
        color: 0x00ff00
    });

    this.redButton.alpha = this.greenButton.alpha = 0.6;
    this.redButton.visible = false;
    this.greenButton.visible = false;
    this.introContainer.addChild(this.redButton);
    this.introContainer.addChild(this.greenButton);

    this.soundTexture = PIXI.Texture.fromFrame("sound_grey.png");

    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.exerciseContainer);

    this.createFeedBack();
    this.initObjectPooling();
}

/**
 * Render feedback to the container
 */
Football_1_1.prototype.createFeedBack = function() {
    var self = this;
    var feedbackPositions = [
        1292, 216,
        1199, 181,
        1110, 239,
        1018, 244,
        925, 247,
        832, 255,
        732, 186,
        652, 239,
        559, 256,
        467, 241
    ];
    for (var k = 0; k < 10; k++) {
        var feedbackSprite = PIXI.Sprite.fromFrame("fb" + (k + 1) + ".png");

        feedbackSprite.position = {
            x: feedbackPositions[k * 2],
            y: feedbackPositions[k * 2 + 1]
        };
        main.texturesToDestroy.push(feedbackSprite);
        this.feedbackContainer.addChild(feedbackSprite);
        feedbackSprite.visible = false;
    }
};

/**
 * manage exercise with new sets randomized
 */
Football_1_1.prototype.runExercise = function() {

    var self = this;

    this.jsonData = this.wallSlices[0];
    console.info('answers', this.jsonData.answers);

    this.whiteImageBackground = createRect({
        w: 241,
        h: 241,
        color: 0xffffff,
        lineStyle: 1
    });
    this.whiteImageBackground.position = {
        x: 502,
        y: 685
    };
    this.questionImage = this.jsonData.image;
    this.questionImage.position = {
        x: 0,
        y: 0
    };
    this.questionImage.width = this.questionImage.height = 241;

    this.whiteImageBackground.addChild(this.questionImage);

    var soundSprite = new PIXI.Sprite(this.soundTexture);
    soundSprite.position.set(0, 191);

    soundSprite.howl = this.jsonData.sound;
    soundSprite.interactive = true;
    soundSprite.buttonMode = true;

    soundSprite.click = soundSprite.tap = this.onSoundPressed.bind(soundSprite);

    this.whiteImageBackground.addChild(soundSprite);

    this.exerciseContainer.addChild(this.whiteImageBackground);

    this.whiteGraphics = createRect({
        w: 507,
        h: 94,
        color: 0xffffff,
        lineStyle: 1
    });
    this.whiteRect = this.whiteGraphics.generateTexture();
    this.whiteBoxPositions = [655, 761, 867];

    for (var k = 0; k < 3; k++) {
        var drawWhiteBox = new PIXI.Sprite(this.whiteRect);
        drawWhiteBox.position = {
            x: 817,
            y: this.whiteBoxPositions[k]
        };
        //drawWhiteBox.width = drawWhiteBox.height = 335;
        drawWhiteBox.buttonMode = true;
        drawWhiteBox.interactive = true;
        self.exerciseContainer.addChild(drawWhiteBox);

        var sentences = new PIXI.Text(this.jsonData.sentences[k], {
            font: "55px Arial"
        });
        sentences.position = {
            x: 30,
            y: (94 - sentences.height) / 2 + 10
        };

        drawWhiteBox.addChild(sentences);
        drawWhiteBox.hitIndex = k;
        drawWhiteBox.answer = this.jsonData.answers[k];
        drawWhiteBox.pos = {
            x: 817 + 10,
            y: this.whiteBoxPositions[k] + 10
        };

        drawWhiteBox.click = drawWhiteBox.tap = function() {
            if (addedListeners) return false;

            addedListeners = true;
            if (this.answer == 1) { //correct sentence clicked
                correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                self.common.showRedOrGreenBox(this.pos.x - 10, this.pos.y - 10, self.greenButton);
                setTimeout(function() {
                    self.correctFeedback();
                }, 800);
            } else {
                wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                self.common.showRedOrGreenBox(this.pos.x - 10, this.pos.y - 10, self.redButton);
                setTimeout(function() {
                    self.wrongFeedback();
                }, 800);
            }
        }

    }
}


Football_1_1.prototype.onSoundPressed = function() {
    this.soundInstance = createjs.Sound.play(this.howl, {
        interrupt: createjs.Sound.INTERRUPT_NONE
    });
    this.soundInstance.addEventListener("failed", handleFailed);
    this.soundInstance.addEventListener("complete", handleComplete);
};



/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
Football_1_1.prototype.correctFeedback = function() {
    var self = this;
    var feedback = localStorage.getItem('football_1_1');
    feedback++;

    localStorage.setItem('football_1_1', feedback);
    this.updateFeedback();

    if (feedback >= Football_1_1.totalFeedback) {
        setTimeout(this.onComplete.bind(this), 2000);
    } else {
        self.getNewExerciseSet();
        addedListeners = false;
    }
}

Football_1_1.prototype.wrongFeedback = function() {
    var feedback = localStorage.getItem('football_1_1');
    feedback--;

    if (feedback < 0)
        feedback = 0;

    localStorage.setItem('football_1_1', feedback);
    addedListeners = false;
    this.updateFeedback();

}


Football_1_1.prototype.updateFeedback = function() {

    var self = this;

    var counter = localStorage.getItem('football_1_1');

    for (var i = 0; i < self.feedbackContainer.children.length; i++) {
        self.feedbackContainer.children[i].visible = false;
    }

    for (var j = 0; j < counter; j++) {
        self.feedbackContainer.children[j].visible = true;
    }
}

/**
 * prefetch exercise data and cache number of sets defined only.
 */
Football_1_1.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/football/football_1_1.json?nocache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        self.pool = new ExerciseSpritesPool(loader.json);
        //borrow two sprites once
        self.generatePoolCategory();
        self.borrowWallSprites(2);
        self.runExercise();
    });
    loader.load();
}

Football_1_1.prototype.generatePoolCategory = function() {

    var keyArr = [];

    var spriteArr = this.pool.sprites,
        spriteLen = spriteArr.length;

    for (var i = 0; i < spriteLen; i++) {
        keyArr.push(spriteArr[i][0].word[0]);
    };

    this.poolCategory = keyArr.filter(onlyUnique);

}

Football_1_1.prototype.borrowWallSprites = function(num) {

    var rand, category, sprite, spriteObj;

    for (var i = 0; i < num; i++) {

        rand = makeUniqueRandom(this.poolCategory.length);
        this.borrowCategory = this.poolCategory[rand];

        console.log('rand', rand)
        console.log('borrow category', this.borrowCategory)

        sprite = this.pool.borrowUniqueSprite(this.borrowCategory, this.lastBorrowSprite);

        spriteObj = sprite[0];

        this.lastBorrowSprite = spriteObj.word[0];

        var imageSpriteArr = [],
            wordArr = [],
            sentenceArr = [],
            answerArr = [],
            soundHowlArr = [];

        this.poolSlices.push(sprite);

        for (var k = 0; k < 3; k++) {
            sentenceArr.push(spriteObj.sentence[k]);
            answerArr.push(spriteObj.answer[k]);
        }

        shuffle(sentenceArr, answerArr);

        this.wallSlices.push({
            sentences: sentenceArr,
            answers: answerArr,
            image: PIXI.Sprite.fromImage('uploads/images/' + spriteObj.word[0]),
            sound: loadSound('uploads/audios/' + StripExtFromFile(spriteObj.word[1]))
        });
    }
};

Football_1_1.prototype.returnWallSprites = function() {
    this.wallSlices.shift();
};

Football_1_1.prototype.getNewExerciseSet = function() {

    this.returnWallSprites();
    this.borrowWallSprites(1);
    this.exerciseContainer.removeChildren();
    this.runExercise();
}

Football_1_1.prototype.restartExercise = function() {
    this.feedbackContainer.visible = true;

    for (var k = 0; k < this.feedbackContainer.children.length; k++) {
        this.feedbackContainer.children[k].visible = false;
    }
    //  this.feedbackContainer.removeChildren();

    localStorage.setItem('football_1_1', 0);
    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    this.ovaigen.visible = false;
    this.introMode = true;

    this.getNewExerciseSet();
};

/**
 * the outro scene is displayed
 */
Football_1_1.prototype.onComplete = function() {
    var self = this;

    this.introMode = false;

    this.help.interactive = false;
    this.help.buttonMode = false;
    this.introContainer.visible = false;
    this.outroContainer.visible = true;
    this.animationContainer.visible = false;
    this.feedbackContainer.visible = false;
    this.fromOutro = 1;

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        self.fromOutro = 0;
        this.visible = false;
        addedListeners = false;
        self.cancelOutro();
        self.restartExercise();
    }

    this.outroAnimation();
}

/**
 * clean memory by clearing reference of object
 */
Football_1_1.prototype.cleanMemory = function() {

    this.introMode = null;

    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    if (this.assetsToLoads)
        this.assetsToLoad.length = 0;


    this.help = null;
    this.fromOutro = null;

    this.goalKeeperIntro = null;
    this.mouth = null;

    this.ovaigen = null;

    if (this.jsonData)
        this.jsonData.length = 0;

    this.counter = null;

    if (this.wallSlices)
        this.wallSlices.length = 0;

    if (this.poolSlices)
        this.poolSlices.length = 0;

    this.pool = null;
    this.num = null;

    this.introContainer = null;
    this.outroContainer = null;
    this.feedbackContainer = null;
    this.animationContainer = null;

    this.common = null;

    this.redButton = null;
    this.greenButton = null;
    this.soundTexture = null;
    this.exerciseContainer = null;
    this.whiteRect = null;
    this.sound = null;


    if (this.whiteBoxPositions)
        this.whiteBoxPositions.length = 0;

    this.whiteImageBackground = null;
    this.introId = null;
    this.outroId = null;
    this.footballGround = null;
    this.goalKeeper = null;
    this.playersGroup = null;
    this.questionImage = null;
    if (this.whiteGraphics)
        this.whiteGraphics.clear();

    this.whiteGraphics = null;

    this.sceneContainer.removeChildren();
    this.sceneContainer = null;

}