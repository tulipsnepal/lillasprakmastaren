function Football_2_1() {
    cl.show()

    PIXI.DisplayObjectContainer.call(this);

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position.set(77, 88);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();
    this.feedbackContainer = new PIXI.DisplayObjectContainer();
    this.animationContainer = new PIXI.DisplayObjectContainer();

    localStorage.setItem('football_2_1', 0);
    main.CurrentExercise = "football_2_1";

    this.fromOutro = 0;

    main.eventRunning = false;

    this.introMode = true;

    main.texturesToDestroy = [];
    main.soundToDestroy = [];

    this.common = new Common();
    this.loadSpriteSheet();


    // everything connected to this is rendered.
    this.addChild(this.sceneContainer);
}

Football_2_1.constructor = Football_2_1;
Football_2_1.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Football_2_1.imagePath = 'build/exercises/football/football_2_1/images/';
Football_2_1.audioPath = 'build/exercises/football/football_2_1/audios/';
Football_2_1.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

/**
 * load array of assets of different format
 */
Football_2_1.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(Football_2_1.audioPath + 'intro');
    this.outroId = loadSound(Football_2_1.audioPath + 'outro');
    // create an array of assets to load
    this.assetsToLoad = [
        Football_2_1.imagePath + "sprite_football_2_1a.json",
        Football_2_1.imagePath + "sprite_football_2_1a.png",
        Football_2_1.imagePath + "sprite_football_2_1b.json",
        Football_2_1.imagePath + "sprite_football_2_1b.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
Football_2_1.prototype.spriteSheetLoaded = function() {

    // hide loader
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    // fill exercise background
    this.sceneBackground = new PIXI.Graphics();

    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0x8dc0d4);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);

    this.lightGreenBackground = createRect({
        w: 1838,
        h: 384,
        color: 0xD7E5AF
    });
    this.lightGreenBackground.position.set(3, 3);
    this.sceneContainer.addChild(this.lightGreenBackground);

    //grey background

    this.greenBackground = createRect({
        w: 1838,
        h: 814,
        color: 0x0D8F36
    });
    this.greenBackground.position.set(3, 387);
    this.sceneContainer.addChild(this.greenBackground);

    // exercise instruction
    this.help = PIXI.Sprite.fromFrame("topcorner.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);
    this.sceneContainer.addChild(this.help);

    this.loadExercise();
    this.introScene();
    this.outroScene();

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.outroContainer);
    this.sceneContainer.addChild(this.animationContainer);
    this.sceneContainer.addChild(this.feedbackContainer);
}

Football_2_1.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    }
    if (isNumeric(number))
        this.hands[number].visible = true;
};

/**
 * play intro animation
 */
Football_2_1.prototype.introAnimation = function() {

    createjs.Sound.stop();

    main.overlay.visible = true;

    if (this.fromOutro == 1) {
        this.fromOutro = 0;
        this.resetFeedback();
        this.restartExercise();
        this.defaultDeflatedBall.visible = true;
    }

    this.outroContainer.visible = false;
    this.introContainer.visible = true;

    this.help.interactive = false;
    this.help.buttonMode = false;

    this.animationContainer.visible = true;
    this.mouth.show();
    // this.showHandAt(2);

    this.animationContainer.visible = true;
    this.mouth.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.

0-1 sec ars_resting => arm 1
1-3 sec arm_side_left + arm_side_right => arm 2
3-5 sec arm_side_right + arm_pointing => arm 3

 new PIXI.Sprite.fromFrame('arm1.png'),
        new PIXI.Sprite.fromFrame('arm2.png'),
        new PIXI.Sprite.fromFrame('arm3.png'),
 */
Football_2_1.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    if (currentPostion >= 0 && currentPostion < 1000) {
        this.showHandAt(0);
    } else if (currentPostion >= 1000 && currentPostion < 3000) {
        this.showHandAt(1);
    } else if (currentPostion >= 3000 && currentPostion < 5000) {
        this.showHandAt(2);
    } else {
        this.showHandAt(0);
    }
};

/**
 * cancel running intro animation
 */
Football_2_1.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);
    this.mouth.hide(0);
    this.showHandAt(0);

    this.help.interactive = true;
    this.help.buttonMode = true;

    this.ovaigen.visible = false;
    main.overlay.visible = false;
}

Football_2_1.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.jugglingBall.show();

        self.mouthOutro.show();
        setTimeout(function() {
            self.jugglingBall.visible = false;
            self.spinBall.show();
        }, 3000);
    }
    this.cancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.cancelHandler);
}

Football_2_1.prototype.cancelOutro = function() {

    var self = this;

    createjs.Sound.stop();
    self.mouthOutro.hide(0);

    self.help.interactive = true;
    self.help.buttonMode = true;

    self.ovaigen.visible = true;
    main.overlay.visible = false;

};

Football_2_1.prototype.resetFeedback = function() {
    var self = this;
    localStorage.setItem('football_2_1', 0);
    for (var k = 0; k < self.feedbackContainer.children.length; k++) {
        self.feedbackContainer.children.visible = false;
    }
}

/**
 * create sprites,animation,sound related to intro scene
 */
Football_2_1.prototype.introScene = function() {

    var self = this;

    this.playerTorso = new PIXI.Sprite.fromFrame('captain_body.png');
    this.playerTorso.position.set(222, 233);
    main.texturesToDestroy.push(this.playerTorso);
    this.animationContainer.addChild(this.playerTorso);


    this.headIntro = new PIXI.Sprite.fromFrame('head_intro.png');
    this.headIntro.position.set(275, 11);
    main.texturesToDestroy.push(this.headIntro);
    this.animationContainer.addChild(this.headIntro);


    this.airPump = new PIXI.Sprite.fromFrame('airpump.png');
    this.airPump.position.set(484, 991);
    main.texturesToDestroy.push(this.airPump);
    this.introContainer.addChild(this.airPump);

    this.defaultDeflatedBall = new PIXI.Sprite.fromFrame('fb0.png');
    this.defaultDeflatedBall.position.set(1324, 859);
    main.texturesToDestroy.push(this.defaultDeflatedBall);
    this.introContainer.addChild(this.defaultDeflatedBall);

    //pumping animation
    this.pumpingFoot = new Animation({
        "image": "foot_",
        "length": 3,
        "x": 355,
        "y": 679,
        "speed": 2,
        "frameId": 0
    });

    this.pumpingFoot.gotoAndStop(0);

    this.animationContainer.addChild(this.pumpingFoot);


    this.mouth = new Animation({
        "image": "mouth",
        "length": 5,
        "x": 394,
        "y": 196,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 5, 2, 4, 1, 5, 3],
        "frameId": 0
    });

    this.animationContainer.addChild(this.mouth);

    this.hands = [
        new PIXI.Sprite.fromFrame('arm1.png'),
        new PIXI.Sprite.fromFrame('arm2.png'),
        new PIXI.Sprite.fromFrame('arm3.png'),
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(160, 293);
        this.animationContainer.addChild(this.hands[i]);
        if (i != 0) this.hands[i].visible = false;
    };

    this.showHandAt(0);
    //this.animationContainer.visible = false;
}

/**
 * create sprites,animation,sound related to outro scene
 */
Football_2_1.prototype.outroScene = function() {

    var self = this;

    this.outroContainer.visible = false;

    this.outroBody = new PIXI.Sprite.fromFrame('captain_outer_body.png');
    this.outroBody.position.set(412, 183);
    main.texturesToDestroy.push(this.outroBody);
    this.outroContainer.addChild(this.outroBody);



    this.jugglingBall = new Animation({
        "image": "arms_outro_",
        "length": 2,
        "x": 341,
        "y": 227,
        "speed": 3,
        "frameId": 0
    });

    this.outroContainer.addChild(this.jugglingBall);


    this.spinBall = new Animation({
        "image": "spin_outro_",
        "length": 2,
        "x": 346,
        "y": 227,
        "speed": 6,
        "frameId": 0
    });

    this.outroContainer.addChild(this.spinBall);
    this.spinBall.visible = false;

    this.mouthOutro = new Animation({
        "image": "mouthoutro",
        "length": 5,
        "x": 569,
        "y": 454,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 5, 2, 4, 1, 5, 3],
        "frameId": 0
    });

    this.outroContainer.addChild(this.mouthOutro);


    // back to intro page
    this.ovaigen = PIXI.Sprite.fromFrame('ovaigen.png');
    this.ovaigen.position.set(1153, 719);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.ovaigen.visible = false;
    this.outroContainer.addChild(this.ovaigen);
}

/**
 * load exercise related stuffs
 */
Football_2_1.prototype.loadExercise = function() {

    //render green and red feedback folders
    this.redButton = createRect({
        w: 662,
        h: 127,
        color: 0xff0000
    });
    this.greenButton = createRect({
        w: 662,
        h: 127,
        color: 0x00ff00
    });

    this.redButton.alpha = this.greenButton.alpha = 0.6;
    this.redButton.visible = false;
    this.greenButton.visible = false;
    this.introContainer.addChild(this.redButton);
    this.introContainer.addChild(this.greenButton);

    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.exerciseContainer);

    this.createFeedBack();
    this.initObjectPooling();
}

/**
 * Render feedback to the container
 */
Football_2_1.prototype.createFeedBack = function() {
    var self = this;

    for (var k = 0; k < 10; k++) {
        var feedbackSprite = PIXI.Sprite.fromFrame("fb" + (k + 1) + ".png");

        feedbackSprite.position = {
            x: 1324,
            y: 859
        };

        main.texturesToDestroy.push(feedbackSprite);
        this.feedbackContainer.addChild(feedbackSprite);
        feedbackSprite.visible = false;
    }
};

/**
 * manage exercise with new sets randomized
 */
Football_2_1.prototype.runExercise = function() {

    var self = this;

    this.jsonData = this.wallSlices[0];



    this.whiteGraphics = createRect({
        w: 662,
        h: 127,
        color: 0xffffff,
        lineStyle: 1
    });
    this.whiteRect = this.whiteGraphics.generateTexture();
    this.whiteBoxPositions = [273, 445, 617];
    //x = 740

    for (var k = 0; k < 3; k++) {
        var drawWhiteBox = new PIXI.Sprite(this.whiteRect);
        drawWhiteBox.position = {
            x: 740,
            y: this.whiteBoxPositions[k]
        };
        //drawWhiteBox.width = drawWhiteBox.height = 335;
        drawWhiteBox.buttonMode = true;
        drawWhiteBox.interactive = true;
        self.exerciseContainer.addChild(drawWhiteBox);

        var sentences = new PIXI.Text(this.jsonData.sentences[k], {
            font: "75px Arial"
        });
        sentences.position = {
            x: 30,
            y: (127 - sentences.height) / 2 + 10
        };

        drawWhiteBox.addChild(sentences);
        drawWhiteBox.hitIndex = k;
        drawWhiteBox.answer = this.jsonData.answers[k];
        drawWhiteBox.pos = {
            x: 740 + 10,
            y: this.whiteBoxPositions[k] + 10
        };

        drawWhiteBox.click = drawWhiteBox.tap = function() {
            if (addedListeners) return false;
            if (main.eventRunning === true) {
                return false;
            }

            main.eventRunning = true;

            if (this.answer == 1) { //correct sentence clicked
                correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                self.common.showRedOrGreenBox(this.pos.x - 10, this.pos.y - 10, self.greenButton);
                self.pumpingFoot.show();
                setTimeout(function() {
                    self.pumpingFoot.hide(0);
                    self.correctFeedback();
                }, 1500);
            } else {
                wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                self.common.showRedOrGreenBox(this.pos.x - 10, this.pos.y - 10, self.redButton);
                setTimeout(function() {
                    self.wrongFeedback();
                }, 400);
            }
        }

    }
}



/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
Football_2_1.prototype.correctFeedback = function() {
    var self = this;
    var feedback = localStorage.getItem('football_2_1');
    feedback++;

    localStorage.setItem('football_2_1', feedback);
    this.updateFeedback();

    if (feedback >= Football_2_1.totalFeedback) {
        setTimeout(this.onComplete.bind(this), 1000);
    } else {
        setTimeout(function() {
            self.getNewExerciseSet();
            main.eventRunning = false;
        }, 400);
    }
}

Football_2_1.prototype.wrongFeedback = function() {
    var feedback = localStorage.getItem('football_2_1');
    feedback--;

    if (feedback < 0)
        feedback = 0;

    localStorage.setItem('football_2_1', feedback);
    this.updateFeedback();

}


Football_2_1.prototype.updateFeedback = function() {

    var self = this;
    this.defaultDeflatedBall.visible = false;
    var counter = localStorage.getItem('football_2_1');

    main.eventRunning = false;

    for (var i = 0; i < self.feedbackContainer.children.length; i++) {
        self.feedbackContainer.children[i].visible = false;
    }

    if (counter == 0) {
        this.defaultDeflatedBall.visible = true;
    } else {
        self.feedbackContainer.children[counter - 1].visible = true;
    }

}

/**
 * prefetch exercise data and cache number of sets defined only.
 */
Football_2_1.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/football/football_2_1.json?nocache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        self.pool = new ExerciseSpritesPool(loader.json);
        //borrow two sprites once
        self.borrowWallSprites(2);
        self.runExercise();
    });
    loader.load();
}

Football_2_1.prototype.borrowWallSprites = function(num) {

    var self = this;

    for (var i = 0; i < num; i++) {

        var sprite = this.pool.borrowSprite(),
            spriteObj = sprite[0],
            sentenceArr = [],
            answerArr = [];

        this.poolSlices.push(sprite);

        for (var k = 0; k < 3; k++) {
            sentenceArr.push(spriteObj.sentence[k]);
            answerArr.push(spriteObj.answer[k]);
        }

        shuffle(sentenceArr, answerArr);

        this.wallSlices.push({
            sentences: sentenceArr,
            answers: answerArr
        });
    }
};

Football_2_1.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];

    this.pool.returnSprite(sprite);

    this.poolSlices = [];
    this.wallSlices.shift();
};

Football_2_1.prototype.getNewExerciseSet = function() {

    this.returnWallSprites();
    this.borrowWallSprites(1);
    for (var i = this.exerciseContainer.children.length - 1; i >= 0; i--) {
        this.exerciseContainer.removeChild(this.exerciseContainer.children[i]);
    };
    this.runExercise();
}

Football_2_1.prototype.restartExercise = function() {
    this.feedbackContainer.visible = true;
    for (var i = this.feedbackContainer.children.length - 1; i >= 0; i--) {
        this.feedbackContainer.children[i].visible = false;
    };
    this.defaultDeflatedBall.visible = true;
    this.jugglingBall.visible = true;
    this.spinBall.visible = false;
    localStorage.setItem('football_2_1', 0);
    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    this.animationContainer.visible = true;
    this.introMode = true;
    this.ovaigen.visible = false;

    this.getNewExerciseSet();
};

/**
 * the outro scene is displayed
 */
Football_2_1.prototype.onComplete = function() {
    var self = this;

    this.introMode = false;
    self.spinBall.visible = false;
    self.jugglingBall.visible = true;
    this.help.interactive = false;
    this.help.buttonMode = false;
    this.introContainer.visible = false;
    this.outroContainer.visible = true;
    this.fromOutro = 1;
    this.animationContainer.visible = false;
    this.feedbackContainer.visible = false;

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        self.fromOutro = 0;
        main.eventRunning = false;
        self.cancelOutro();
        self.restartExercise();
    }

    this.outroAnimation();

}

/**
 * clean memory by clearing reference of object
 */
Football_2_1.prototype.cleanMemory = function() {

    this.introMode = null;

    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    if (this.assetsToLoads)
        this.assetsToLoad.length = 0;


    if (this.lightGreenBackground)
        this.lightGreenBackground.clear();

    this.lightGreenBackground = null;


    if (this.greenBackground)
        this.greenBackground.clear();

    this.greenBackground = null;

    this.fromOutro = null;

    this.help = null;

    this.headIntro = null;
    this.mouth = null;
    this.playerTorso = null;



    this.ovaigen = null;

    if (this.jsonData)
        this.jsonData.length = 0;

    this.counter = null;

    if (this.wallSlices)
        this.wallSlices.length = 0;

    if (this.poolSlices)
        this.poolSlices.length = 0;

    this.pool = null;
    this.num = null;

    this.introContainer = null;
    this.outroContainer = null;
    this.feedbackContainer = null;
    this.animationContainer = null;

    this.common = null;

    this.redButton = null;
    this.greenButton = null;
    this.soundTexture = null;
    this.exerciseContainer = null;
    this.whiteRect = null;
    this.sound = null;


    if (this.whiteBoxPositions)
        this.whiteBoxPositions.length = 0;

    this.airPump = null;
    this.introId = null;
    this.outroId = null;
    this.defaultDeflatedBall = null;
    this.pumpingFoot = null;
    this.outroBody = null;
    this.jugglingBall = null;
    this.spinBall = null;
    this.mouthOutro = null;

    if (this.whiteGraphics)
        this.whiteGraphics.clear();

    this.whiteGraphics = null;


    for (var i = this.sceneContainer.children.length - 1; i >= 0; i--) {
        this.sceneContainer.removeChild(this.sceneContainer.children[i]);
    };
    this.sceneContainer = null;

}