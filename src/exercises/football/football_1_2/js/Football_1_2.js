function Football_1_2() {
    cl.show();
    PIXI.DisplayObjectContainer.call(this);

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position.set(77, 88);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();
    this.feedbackContainer = new PIXI.DisplayObjectContainer();
    this.animationContainer = new PIXI.DisplayObjectContainer();
    this.wrongRightContainer = new PIXI.SpriteBatch();

    localStorage.setItem('football_1_2', 0);
    main.CurrentExercise = "football_1_2";

    main.eventRunning = false;

    this.fromOutro = 0;

    this.introMode = true;

    main.texturesToDestroy = [];
    main.soundToDestroy = [];

    this.common = new Common();
    this.loadSpriteSheet();
    this.alreadyPlayedWords = [];


    // everything connected to this is rendered.
    this.addChild(this.sceneContainer);
}

Football_1_2.constructor = Football_1_2;
Football_1_2.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Football_1_2.imagePath = 'build/exercises/football/football_1_2/images/';
Football_1_2.audioPath = 'build/exercises/football/football_1_2/audios/';
Football_1_2.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

/**
 * load array of assets of different format
 */
Football_1_2.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(Football_1_2.audioPath + 'intro');
    this.outroId = loadSound(Football_1_2.audioPath + 'outro');
    // create an array of assets to load
    this.assetsToLoad = [
        Football_1_2.imagePath + "sprite_football_1_2.json",
        Football_1_2.imagePath + "sprite_football_1_2.png",
        Football_1_2.imagePath + "smip.png",
        Football_1_2.imagePath + "trainer_outro.png",
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
Football_1_2.prototype.spriteSheetLoaded = function() {

    // hide loader
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    // fill exercise background
    this.sceneBackground = new PIXI.Graphics();

    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0x8dc0d4);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);

    //blue background
    this.blueBackground = createRect({
        w: 1838,
        h: 153,
        color: 0x2DAAE1
    });
    this.blueBackground.position.set(3, 3);
    this.sceneContainer.addChild(this.blueBackground);

    //grey background

    this.greyBackground = createRect({
        w: 1838,
        h: 453,
        color: 0xF6F6F6,
        lineStyle: true,
        lineWidth: 1,
        lineColor: 0x5E5F5C
    });
    this.greyBackground.position.set(3, 155);
    this.sceneContainer.addChild(this.greyBackground);


    //blue background
    this.greenBackground = createRect({
        w: 1838,
        h: 593,
        color: 0x94C11F
    });
    this.greenBackground.position.set(3, 608);
    this.sceneContainer.addChild(this.greenBackground);

    this.bench = PIXI.Sprite.fromFrame("bench.png");
    this.bench.position = {
        x: 476,
        y: 809
    };
    main.texturesToDestroy.push(this.bench);
    this.sceneContainer.addChild(this.bench);


    // exercise instruction
    this.help = PIXI.Sprite.fromFrame("topcorner.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);
    this.sceneContainer.addChild(this.help);

    this.loadExercise();
    this.introScene();
    this.outroScene();

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.outroContainer);
    this.sceneContainer.addChild(this.animationContainer);
    this.sceneContainer.addChild(this.feedbackContainer);
    this.sceneContainer.addChild(this.wrongRightContainer);
}

Football_1_2.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    }
    if (isNumeric(number))
        this.hands[number].visible = true;
};


/**
 * play intro animation
 */
Football_1_2.prototype.introAnimation = function() {

    createjs.Sound.stop();

    main.overlay.visible = true;

    if (this.fromOutro == 1) {
        this.fromOutro = 0;
        this.resetFeedback();
        this.restartExercise();
    }

    this.outroContainer.visible = false;
    this.introContainer.visible = true;

    this.help.interactive = false;
    this.help.buttonMode = false;

    this.animationContainer.visible = true;
    this.eyeParams.show();
    this.mouth.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
 0-4 SEC ARM_0 => 0
4-5 SEC ARM_1 =>1
5-6 SEC ARM_2 =>2
6-7 SEC ARM_3 =>3
STOP AT ARM_0 =>0

 */
Football_1_2.prototype.animateIntro = function() {

    var currentPosition = this.introInstance.getPosition();
    if (currentPosition >= 1000 && currentPosition < 4000) {
        this.showHandAt(0);
    } else if (currentPosition >= 4000 && currentPosition < 5000) {
        this.showHandAt(1);
    } else if (currentPosition >= 5000 && currentPosition < 6000) {
        this.showHandAt(2);
    } else if (currentPosition >= 6000 && currentPosition < 7000) {
        this.showHandAt(3);
    } else {
        this.showHandAt(0);
    }
};

/**
 * cancel running intro animation
 */
Football_1_2.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    this.animationContainer.visible = false;
    clearInterval(this.timerId);
    this.mouth.hide(2);
    this.showHandAt(2);
    this.eyeParams.show(0);

    this.help.interactive = true;
    this.help.buttonMode = true;

    this.ovaigen.visible = false;
    main.overlay.visible = false;
}
Football_1_2.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.eyeParamOutro.show();
        self.mouthOutro.show();
    }
    this.cancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.cancelHandler);

}

Football_1_2.prototype.cancelOutro = function() {

    var self = this;

    createjs.Sound.stop();
    self.ovaigen.visible = true;
    self.help.interactive = true;
    self.help.buttonMode = true;
    self.mouthOutro.hide(0);
    main.overlay.visible = false;

};

Football_1_2.prototype.resetFeedback = function() {

    for (var i = this.feedbackContainer.children.length - 1; i >= 0; i--) {
        this.feedbackContainer.removeChild(this.feedbackContainer.children[i]);
    };
    localStorage.setItem('football_1_2', 0);
    this.createFeedBack();
}



/**
 * create sprites,animation,sound related to intro scene
 */
Football_1_2.prototype.introScene = function() {

    var self = this;

    this.trainerIntro = new PIXI.Sprite.fromFrame('trainer.png');
    this.trainerIntro.position.set(3, 565);
    main.texturesToDestroy.push(this.trainerIntro);
    this.animationContainer.addChild(this.trainerIntro);

    this.animationContainer.visible = false;

    this.mouth = new Animation({
        "image": "mouth",
        "length": 5,
        "x": 201,
        "y": 806,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 5, 2, 5, 1, 4, 3],
        "frameId": 0
    });

    this.eyeParams = new Animation({
        "image": "eyes_",
        "length": 2,
        "x": 174,
        "y": 727,
        "speed": 2,
        "pattern": true,
        "sequence": [1, 2, 2, 1, 1, 2, 2, 1, 2, 1, 1, 2],
        "frameId": 2
    });
    this.animationContainer.addChild(this.eyeParams);

    this.animationContainer.addChild(this.mouth);

    this.hands = [
        new PIXI.Sprite.fromFrame('arm1.png'),
        new PIXI.Sprite.fromFrame('arm2.png'),
        new PIXI.Sprite.fromFrame('arm3.png'),
        new PIXI.Sprite.fromFrame('arm4.png'),
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(323, 499);
        this.animationContainer.addChild(this.hands[i]);
        if (i != 0) this.hands[i].visible = false;
    };

    this.showHandAt(0);
    //this.animationContainer.visible = false;
}

/**
 * create sprites,animation,sound related to outro scene
 */
Football_1_2.prototype.outroScene = function() {

    var self = this;

    this.outroContainer.visible = false;

    this.smipBanner = PIXI.Sprite.fromImage(Football_1_2.imagePath + "smip.png");
    this.smipBanner.position = {
        x: 3,
        y: 264
    };
    this.outroContainer.addChild(this.smipBanner);


    this.tidyPiledupShirts = PIXI.Sprite.fromFrame("piled_jersies.png");
    this.tidyPiledupShirts.position = {
        x: 493,
        y: 362
    }
    this.outroContainer.addChild(this.tidyPiledupShirts);

    this.trainerOutro = PIXI.Sprite.fromImage(Football_1_2.imagePath + "trainer_outro.png");
    this.trainerOutro.position = {
        x: 2,
        y: 201
    };
    this.outroContainer.addChild(this.trainerOutro);



    this.eyeParamOutro = new Animation({
        "image": "eyes_outro_",
        "length": 2,
        "x": 1048,
        "y": 319,
        "speed": 2,
        "pattern": true,
        "sequence": [1, 2, 2, 1, 1, 2, 2, 1, 2, 1, 1, 2],
        "frameId": 2
    });

    this.mouthOutro = new Animation({
        "image": "mouth",
        "length": 5,
        "x": 1067,
        "y": 388,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 5, 2, 5, 1, 4, 3],
        "frameId": 0
    });

    this.outroContainer.addChild(this.eyeParamOutro);
    this.outroContainer.addChild(this.mouthOutro);

    // back to intro page
    this.ovaigen = PIXI.Sprite.fromFrame('ovaigen.png');
    this.ovaigen.position.set(494, 682);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.ovaigen.visible = false;
    this.outroContainer.addChild(this.ovaigen);
}

/**
 * load exercise related stuffs
 */
Football_1_2.prototype.loadExercise = function() {

    //render green and red feedback folders

    var feedBackRect = createRect({
        w: 318,
        h: 126,
        color: 0xffffff
    });

    var feedbackTexture = feedBackRect.generateTexture();
    this.feedBackIndicator = new PIXI.Sprite(feedbackTexture);

    this.dottedBoxTexture = PIXI.Texture.fromFrame("dottedbox.png");

    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.exerciseContainer);

    this.createFeedBack();
    this.initObjectPooling();
}

Football_1_2.prototype.showRedGreenBox = function(xPos, yPos, obj) {
    var self = this;

    this.feedTexture = obj.generateTexture();
    var ft = new PIXI.Sprite(this.feedTexture);
    this.wrongRightContainer.addChild(ft);
    ft.position = {
        x: xPos,
        y: yPos
    };
    ft.alpha = 0.6;
    ft.visible = true;

    setTimeout(function() {
        ft.visible = false;
    }, 800);

}

/**
 * Render feedback to the container
 */
Football_1_2.prototype.createFeedBack = function() {
    var self = this;

    for (var k = 0; k < 10; k++) {
        var feedbackSprite = PIXI.Sprite.fromFrame("fb.png");

        feedbackSprite.position = {
            x: 1685,
            y: 1049 - (k * 110)
        };

        var jerseyNumber = new PIXI.Text(k + 1, {
            font: "35px Arial",
            fill: "red"
        });

        jerseyNumber.position = {
            x: (feedbackSprite.width - jerseyNumber.width) / 2 + 4,
            y: (feedbackSprite.height - jerseyNumber.height) / 2 + 10
        };
        feedbackSprite.addChild(jerseyNumber);

        main.texturesToDestroy.push(feedbackSprite);
        this.feedbackContainer.addChild(feedbackSprite);
        feedbackSprite.alpha = 0.2;
    }
};

/**
 * manage exercise with new sets randomized
 */
Football_1_2.prototype.runExercise = function() {

    var self = this;

    this.dropArea = [];
    this.itemsDroppedIn = [];
    this.correctAnswer = [];

    this.jsonData = this.wallSlices[0];

    this.dragImage = [];
    this.itemsDragged = [-1, -1, -1];
    this.droppedNames = ["", "", ""];
    this.rightAnswer = this.jsonData.answer.join();

    this.chosenOne = {
        "0": '',
        "1": '',
        "2": ''
    };

    this.dropZone = [0, 0, 0];
    this.itmDrg = [];
    this.drpNames = [];


    this.itemPositions = {
        sentence: [
            50, 308,
            50, 377,
            50, 445
        ],
        image: [
            528, 135,
            890, 135,
            1252, 135
        ],
        dottedbox: [
            537, 474,
            899, 474,
            1261, 474
        ],
        drop_positions: [
            528, 463,
            890, 463,
            1252, 463
        ],
        textbox: [
            528, 671,
            890, 671,
            1252, 671
        ]
    };

    this.whiteBackgroundGraphics = createRect({
        w: 318,
        h: 318,
        color: 0xffffff,
        lineStyle: 1
    });
    this.whiteBackgroundRect = this.whiteBackgroundGraphics.generateTexture();

    this.whiteTextBackground = createRect({
        w: 318,
        h: 126,
        color: 0xffffff,
        lineStyle: 1
    });
    this.whiteTextRect = this.whiteTextBackground.generateTexture();

    for (var k = 0; k < 3; k++) {
        //ADD IMAGE AND IMAGE BACKGROUND
        var imageWhiteBox = new PIXI.Sprite(this.whiteBackgroundRect);
        imageWhiteBox.position = {
            x: this.itemPositions.image[k * 2],
            y: this.itemPositions.image[k * 2 + 1]
        };

        imageWhiteBox.width = imageWhiteBox.height = 318;

        var image = this.jsonData.image[k];
        // image.position = {x: 9, y: 9 };
        image.width = image.height = 318;
        imageWhiteBox.addChild(image);
        main.texturesToDestroy.push(image);
        self.exerciseContainer.addChild(imageWhiteBox);

        //ADD SENTENCES
        var sentence = new PIXI.Text(this.jsonData.sentences[k], {
            font: "55px Arial"
        });
        sentence.position = {
            x: this.itemPositions.sentence[k * 2],
            y: this.itemPositions.sentence[k * 2 + 1]
        };
        self.exerciseContainer.addChild(sentence);

        //ADD DOTTED SPACE
        var dottedSpace = new PIXI.Sprite(this.dottedBoxTexture);
        dottedSpace.position = {
            x: this.itemPositions.dottedbox[k * 2] - 10,
            y: this.itemPositions.dottedbox[k * 2 + 1] - 10
        };

        self.exerciseContainer.addChild(dottedSpace);

        //ADD CHARACTER NAMES
        var nameWhiteBox = new PIXI.Sprite(this.whiteTextRect);
        nameWhiteBox.interactive = true;
        nameWhiteBox.buttonMode = true;
        nameWhiteBox.position = {
            x: this.itemPositions.textbox[k * 2],
            y: this.itemPositions.textbox[k * 2 + 1]
        };

        nameWhiteBox.oldPosition = {
            x: this.itemPositions.textbox[k * 2],
            y: this.itemPositions.textbox[k * 2 + 1]
        };

        var character_name = new PIXI.Text(this.jsonData.character[k], {
            font: "55px Arial"
        });
        character_name.position = {
            x: (318 - character_name.width) / 2,
            y: (126 - character_name.height) / 2 + 10
        };
        nameWhiteBox.addChild(character_name);
        nameWhiteBox.dropPosition = this.itemPositions.drop_positions;
        nameWhiteBox.chosenText = this.jsonData.character[k];
        nameWhiteBox.dragIndex = k;
        this.correctAnswer.push(this.jsonData.answer[k]);
        self.exerciseContainer.addChild(nameWhiteBox);

        this.dragImage.push(nameWhiteBox);
    }

    for (var i = 0; i < this.dragImage.length; i++) {
        this.onDragging(i);
    };
}

/**
 * drag drop sprite
 * on correct drop, feedback is updated
 */
Football_1_2.prototype.onDragging = function(i) {

    var self = this;

    // use the mousedown and touchstart
    this.dragImage[i].mousedown = this.dragImage[i].touchstart = function(data) {
        if (addedListeners) return false;
        data.originalEvent.preventDefault()

        // store a refference to the data
        // The reason for this is because of multitouch
        // we want to track the movement of this particular touch
        this.data = data;
        this.alpha = 0.9;
        this.dragging = true;
        this.sx = this.data.getLocalPosition(this).x;
        this.sy = this.data.getLocalPosition(this).y;
        this.bringToFront();
    }

    this.dragImage[i].mouseup = this.dragImage[i].mouseupoutside = this.dragImage[i].touchend = this.dragImage[i].touchendoutside = function(data) {
        if (addedListeners) return false;
        var dragSelf = this;

        this.dragging = false;
        // set the interaction data to null
        this.data = null;
        this.alpha = 1;

        var dropArea = [];

        dropArea[0] = isInsideRectangle(this.dx, this.dy, this.dropPosition[0], this.dropPosition[1], this.dropPosition[0] + 318, this.dropPosition[1] + 126);
        dropArea[1] = isInsideRectangle(this.dx, this.dy, this.dropPosition[2], this.dropPosition[3], this.dropPosition[2] + 318, this.dropPosition[3] + 126);
        dropArea[2] = isInsideRectangle(this.dx, this.dy, this.dropPosition[4], this.dropPosition[5], this.dropPosition[4] + 318, this.dropPosition[5] + 126);


        var idxcheck = self.itemsDragged.indexOf(this.dragIndex);
        if (idxcheck > -1) {
            self.dropZone[idxcheck] = 0;
            self.itemsDragged[idxcheck] = -1;
            self.droppedNames[idxcheck] = "";
        }

        /* if (self.dropZone[0] === 0) dropArea[0] = false;
         if (self.dropZone[1] === 1) dropArea[1] = false;
         if (self.dropZone[2] === 2) dropArea[2] = false;*/

        if (self.dropZone[0] == 1) dropArea[0] = false;
        if (self.dropZone[1] == 1) dropArea[1] = false;
        if (self.dropZone[2] == 1) dropArea[2] = false;

        //   console.log("DROP ZONE  = " + self.dropZone);

        if (dropArea[0] || dropArea[1] || dropArea[2]) {
            if (dropArea[0]) {
                this.position = {
                    x: this.dropPosition[0],
                    y: this.dropPosition[1]
                };
                // self.dropArea[0] = true;
                self.itemsDragged[0] = this.dragIndex;
                self.chosenOne[0] = this.chosenText;
                self.droppedNames[0] = this.chosenText;
                self.dropZone[0] = 1;
                if (self.itemsDragged[1] == this.dragIndex) {
                    self.dropZone[1] = 0;
                }
                if (self.itemsDragged[2] == this.dragIndex) {
                    self.dropZone[2] = 0;
                }
                //self.itemsDroppedIn.push(0);
            }
            if (dropArea[1]) {
                this.position = {
                    x: this.dropPosition[2],
                    y: this.dropPosition[3]
                };
                //self.dropArea[1] = true;
                self.itemsDragged[1] = this.dragIndex;
                self.chosenOne[1] = this.chosenText;
                self.droppedNames[1] = this.chosenText;
                self.dropZone[1] = 1;
                if (self.itemsDragged[0] == this.dragIndex) {
                    self.dropZone[0] = 0;
                }
                if (self.itemsDragged[2] == this.dragIndex) {
                    self.dropZone[2] = 0;
                }
                //self.itemsDroppedIn.push(1);
            }
            if (dropArea[2]) {
                this.position = {
                    x: this.dropPosition[4],
                    y: this.dropPosition[5]
                };
                //  self.dropArea[2] = true;
                self.itemsDragged[2] = this.dragIndex;
                self.chosenOne[2] = this.chosenText;
                self.droppedNames[2] = this.chosenText;
                self.dropZone[2] = 1;
                if (self.itemsDragged[1] == this.dragIndex) {
                    self.dropZone[1] = 0;
                }
                if (self.itemsDragged[0] == this.dragIndex) {
                    self.dropZone[0] = 0;
                }
                //self.itemsDroppedIn.push(2);
            }

            self.itmDrg = self.itemsDragged.removeDuplicates();
            self.drpNames = self.droppedNames.removeDuplicates();
            // this.position.x = 0;
            // this.position.y = 0;
            this.sx = 0;
            this.sy = 0;
            this.dx = 0;
            this.dy = 0;
            // this.interactive = false;
            console.log("ITEMS DRAGGED = " + self.itemsDragged);
            //console.log("DROPPED NAMES  = " + self.drpNames);
            console.log("DROP ZONE  = " + self.dropZone);

            // check if all dragged correctly or not
            // if (self.itmDrg.length == 3) { //3 items dragged, evaluate answer
            if ((self.itemsDragged.length == 3) && (self.itemsDragged[0] != -1 && self.itemsDragged[1] != -1 && self.itemsDragged[2] != -1)) { //3 items dragged, evaluate answer
                console.log("FINAL CHOSEN ANSWERS = " + self.itmDrg.join());
                if (self.droppedNames.join() == self.rightAnswer) { //correct answer
                    var a = setTimeout(function() {
                        correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                        self.feedBackIndicator.tint = 0x00ff00;
                        for (var j = 0; j < 3; j++) {
                            self.showRedGreenBox(self.dragImage[j].dropPosition[j * 2], self.dragImage[j].dropPosition[j * 2 + 1], self.feedBackIndicator);
                        }
                    }, 1000);

                    var b = setTimeout(function() {
                        self.correctFeedback();
                        self.itemsDragged = [-1, -1, -1];
                        self.dropZone = [0, 0, 0];
                        self.droppedNames = ["", "", ""];
                        self.itmDrg = [];
                        self.drpNames = [];
                    }, 1800);

                } else { //wrong answer
                    var itemsToReset = [];
                    var a = setTimeout(function() {
                        wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);

                        for (var m = 0; m < 3; m++) {
                            self.dragImage[m].interactive = false;
                            if (self.chosenOne[m] != self.correctAnswer[m]) {
                                self.feedBackIndicator.tint = 0xff0000;
                                itemsToReset.push(self.itemsDragged[m]);
                                self.dropZone[m] = 0;
                                self.showRedGreenBox(self.dragImage[m].dropPosition[m * 2], self.dragImage[m].dropPosition[m * 2 + 1], self.feedBackIndicator);
                                //self.dropArea[m] = false;
                            } else {
                                self.feedBackIndicator.tint = 0x00ff00;
                                self.dropZone[m] = 1;
                                self.showRedGreenBox(self.dragImage[m].dropPosition[m * 2], self.dragImage[m].dropPosition[m * 2 + 1], self.feedBackIndicator);
                            }
                        }
                    }, 1000);



                    var b = setTimeout(function() {
                        for (var n = 0; n < itemsToReset.length; n++) {
                            self.dragImage[itemsToReset[n]].interactive = true;
                            self.dragImage[itemsToReset[n]].position.x = self.dragImage[itemsToReset[n]].oldPosition.x;
                            self.dragImage[itemsToReset[n]].position.y = self.dragImage[itemsToReset[n]].oldPosition.y;
                            var idx = self.itemsDragged.indexOf(itemsToReset[n]);
                            if (idx > -1) {
                                //self.itemsDragged.splice(idx, 1);
                                //self.droppedNames.splice(idx, 1);
                                self.itemsDragged[idx] = -1;
                                self.droppedNames[idx] = "";
                                //self.dropZone[idx] = 0;
                            }

                            var idx2 = self.itmDrg.indexOf(itemsToReset[n]);
                            if (idx2 > -1) {
                                self.itmDrg.splice(idx2, 1);
                                self.drpNames.splice(idx2, 1);
                                // self.dropZone[idx2] = 0;
                            }
                        }
                        self.wrongFeedback();
                    }, 1800);
                }
            }

        } else {
            var idx = self.itemsDragged.indexOf(this.dragIndex);
            if (idx > -1) {
                //self.itemsDragged.splice(idx, 1);
                //  self.droppedNames.splice(idx, 1);
                self.itemsDragged[idx] = -1;
                self.droppedNames[idx] = "";
                self.itmDrg.splice(idx, 1);
                self.drpNames.splice(idx, 1);
                self.dropZone[idx] = 0;

                if (self.itemsDragged[0] == -1 && self.itemsDragged[1] == -1 && self.itemsDragged[2] == -1) {
                    self.dropZone[0] = 0;
                    self.dropZone[1] = 0;
                    self.dropZone[2] = 0;
                }
                // self.dropArea[this.dragIndex] = false;
            }

            // reset dragged element position
            this.position.x = this.oldPosition.x;
            this.position.y = this.oldPosition.y;
        }

    }

    // set the callbacks for when the mouse or a touch moves
    this.dragImage[i].mousemove = this.dragImage[i].touchmove = function(data) {
        // set the callbacks for when the mouse or a touch moves
        if (this.dragging) {
            // need to get parent coords..
            var newPosition = this.data.getLocalPosition(this.parent);
            this.position.x = newPosition.x - this.sx;
            this.position.y = newPosition.y - this.sy;
            this.dx = newPosition.x;
            this.dy = newPosition.y;
        }
    };

}


/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
Football_1_2.prototype.correctFeedback = function() {
    var self = this;
    var feedback = localStorage.getItem('football_1_2');
    feedback++;

    localStorage.setItem('football_1_2', feedback);
    this.updateFeedback();

    if (feedback >= Football_1_2.totalFeedback) {
        setTimeout(this.onComplete.bind(this), 2000);
    } else {
        setTimeout(function() {
            self.getNewExerciseSet();
            main.eventRunning = false;
        }, 400);
    }
}

Football_1_2.prototype.wrongFeedback = function() {
    var feedback = localStorage.getItem('football_1_2');
    feedback--;

    if (feedback < 0)
        feedback = 0;

    localStorage.setItem('football_1_2', feedback);
    this.updateFeedback();

}


Football_1_2.prototype.updateFeedback = function() {

    var self = this;

    var counter = localStorage.getItem('football_1_2');

    for (var i = 0; i < self.feedbackContainer.children.length; i++) {
        self.feedbackContainer.children[i].alpha = 0.2;
    }

    for (var j = 0; j < counter; j++) {
        self.feedbackContainer.children[j].alpha = 1;
    }

    for (var i = self.wrongRightContainer.children.length - 1; i >= 0; i--) {
        self.wrongRightContainer.removeChild(self.wrongRightContainer.children[i]);
    };
}

/**
 * prefetch exercise data and cache number of sets defined only.
 */
Football_1_2.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/football/football_1_2.json?nocache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in evt.content.json
        self.pool = new ExerciseSpritesPool(loader.json);
        //borrow two sprites once
        self.generatePoolCategory();
        self.borrowWallSprites(2);
        self.runExercise();
    });
    loader.load();
}

Football_1_2.prototype.generatePoolCategory = function() {

    var keyArr = [];

    var spriteArr = this.pool.sprites,
        spriteLen = spriteArr.length;

    for (var i = 0; i < spriteLen; i++) {
        var ab = spriteArr[i][0].sentences;
        for (var k = 0; k < ab.length; k++) {
            var rw = ab[k];
            rw = rw.split(" ");
            var cw = rw[1].replace(".", "");
            keyArr.push(cw);
        }

    };

    this.poolCategory = keyArr.filter(onlyUnique).sort();


    this.poolCategoryType = "sentences";
}

Football_1_2.prototype.borrowWallSprites = function(num) {

    var rand, category, sprite, spriteObj;

    var self = this;
    for (var i = 0; i < num; i++) {

        /*  rand = makeUniqueRandom(this.poolCategory.length);
         
          this.borrowCategory = this.poolCategory[rand];*/

        sprite = this.pool.borrowUniqueSpriteMultipleWords(this.alreadyPlayedWords, this.poolCategory, true, this.poolCategoryType);
        //sprite = this.pool.borrowSprite();

        spriteObj = sprite[0];
        var imageSpriteArr = [],
            sentenceArr = [],
            characterArr = [],
            answerArr = [];
        this.poolSlices.push(sprite);

        for (var k = 0; k < 3; k++) {

            var rw = spriteObj.sentences[k];
            rw = rw.split(" ");
            var cw = rw[1].replace(".", "");
            this.alreadyPlayedWords.push(cw);

            sentenceArr.push(spriteObj.sentences[k]);
            characterArr.push(spriteObj.character_name[k]);
            answerArr.push(spriteObj.character_name[k]);
            imageSpriteArr.push(PIXI.Sprite.fromImage('uploads/images/' + spriteObj.image[k]));
        }



        //console.warn("MY TEST AGAIN = "+uwords);

        shuffle(sentenceArr);
        shuffle(imageSpriteArr, answerArr);

        this.wallSlices.push({
            sentences: sentenceArr,
            character: characterArr,
            image: imageSpriteArr,
            answer: answerArr
        });
    }
};

Football_1_2.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];

    this.pool.returnSprite(sprite);

    this.poolSlices = [];
    this.wallSlices.shift();
};

Football_1_2.prototype.getNewExerciseSet = function() {
    this.returnWallSprites();
    this.borrowWallSprites(1);

    for (var i = this.exerciseContainer.children.length - 1; i >= 0; i--) {
        this.exerciseContainer.removeChild(this.exerciseContainer.children[i]);
    };

    this.itemsDragged = [];
    this.dropArea = [];

    this.runExercise();
}

Football_1_2.prototype.restartExercise = function() {

    this.resetFeedback();
    this.feedbackContainer.visible = true;

    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    this.introMode = true;

    this.getNewExerciseSet();
};

/**
 * the outro scene is displayed
 */
Football_1_2.prototype.onComplete = function() {
    var self = this;

    this.introMode = false;

    this.help.interactive = false;
    this.help.buttonMode = false;
    this.introContainer.visible = false;
    this.outroContainer.visible = true;
    this.animationContainer.visible = false;
    this.feedbackContainer.visible = false;
    this.fromOutro = 1;

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        this.visible = false;
        main.eventRunning = false;
        self.fromOutro = 0;
        self.cancelOutro();
        self.restartExercise();
    }

    this.outroAnimation();

}

/**
 * clean memory by clearing reference of object
 */
Football_1_2.prototype.cleanMemory = function() {

    this.introMode = null;

    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    if (this.assetsToLoads)
        this.assetsToLoad.length = 0;

    this.help = null;

    this.trainerIntro = null;
    this.mouth = null;

    this.fromOutro = null;

    this.ovaigen = null;

    if (this.jsonData)
        this.jsonData.length = 0;

    this.counter = null;

    if (this.wallSlices)
        this.wallSlices.length = 0;

    if (this.poolSlices)
        this.poolSlices.length = 0;

    if (this.alreadyPlayedWords)
        this.alreadyPlayedWords.length = 0;

    this.pool = null;
    this.num = null;

    this.introContainer = null;
    this.outroContainer = null;
    this.feedbackContainer = null;
    this.animationContainer = null;

    this.common = null;

    this.feedBackIndicator = null;
    this.feedBackIndicator = null;

    this.exerciseContainer = null;


    if (this.itemPositions)
        this.itemPositions.length = 0;

    if (this.itemsDragged)
        this.itemsDragged.length = 0;


    this.introId = null;
    this.outroId = null;
    this.bench = null;

    this.eyeParamOutro = null;

    if (this.whiteBackgroundGraphics)
        this.whiteBackgroundGraphics.clear();

    this.whiteBackgroundGraphics = null;

    if (this.whiteTextBackground)
        this.whiteTextBackground.clear();

    this.whiteTextBackground = null;

    this.whiteBackgroundRect = null;
    this.whiteTextRect = null;
    this.dropArea1 = null;
    this.dropArea2 = null;
    this.dropArea3 = null;
    if (this.chosenOne) this.chosenOne.length = 0;
    if (this.correctAnswer) this.correctAnswer.length = 0;
    if (this.dragImage) this.dragImage.length = 0;
    this.feedTexture = null;
    this.dottedBoxTexture = null;
    this.mouthOutro = null;
    if (this.blueBackground)
        this.blueBackground.clear();

    this.blueBackground = null;

    if (this.greyBackground)
        this.greyBackground.clear();

    this.greyBackground = null;


    if (this.greenBackground)
        this.greenBackground.clear();

    this.greenBackground = null;

    this.wrongRightContainer = null;
    this.eyeParams = null;
    this.tidyPiledupShirts = null;
    this.smipBanner = null;

    for (var i = this.sceneContainer.children.length - 1; i >= 0; i--) {
        this.sceneContainer.removeChild(this.sceneContainer.children[i]);
    };
    this.sceneContainer = null;

}