function Football_3_1() {
    cl.show()

    PIXI.DisplayObjectContainer.call(this);

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position.set(77, 88);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();
    this.animationContainer = new PIXI.DisplayObjectContainer();
    this.helpContainer = new PIXI.DisplayObjectContainer();
    this.motiveContainer = new PIXI.DisplayObjectContainer();

    main.CurrentExercise = "football_3_1";

    this.fromOutro = 0;

    main.texturesToDestroy = [];
    main.soundToDestroy = [];

    this.common = new Common();
    this.loadSpriteSheet();

    this.randomIndex = makeUniqueRandom(6) + 1;
    // everything connected to this is rendered.
    this.addChild(this.sceneContainer);
}

Football_3_1.constructor = Football_3_1;
Football_3_1.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
Football_3_1.imagePath = 'build/exercises/football/football_3_1/images/';
Football_3_1.audioPath = 'build/exercises/football/football_3_1/audios/';

/**
 * load array of assets of different format
 */
Football_3_1.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(Football_3_1.audioPath + 'intro');

    // create an array of assets to load
    this.assetsToLoad = [
        Football_3_1.imagePath + "sprite_football_3_1.json",
        Football_3_1.imagePath + "sprite_football_3_1.png",
        Football_3_1.imagePath + "feedback1.png",
        Football_3_1.imagePath + "feedback2.png",
        Football_3_1.imagePath + "feedback3.png",
        Football_3_1.imagePath + "feedback4.png",
        Football_3_1.imagePath + "feedback5.png",
        Football_3_1.imagePath + "feedback6.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
Football_3_1.prototype.spriteSheetLoaded = function() {

    // hide loader
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    // fill exercise background
    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0xffffff);
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0x8dc0d4);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);


    // exercise instruction
    this.help = PIXI.Sprite.fromFrame("topcorner.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);
    this.helpContainer.addChild(this.help);

    this.loadExercise();
    this.introScene();
    this.outroScene();

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.motiveContainer);
    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.outroContainer);
    this.sceneContainer.addChild(this.animationContainer);
    this.sceneContainer.addChild(this.helpContainer);
}

Football_3_1.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    }
    if (isNumeric(number))
        this.hands[number].visible = true;
};

/**
 * play intro animation
 */
Football_3_1.prototype.introAnimation = function() {

    createjs.Sound.stop();

    main.overlay.visible = true;

    this.outroContainer.visible = false;
    this.introContainer.visible = true;

    this.help.interactive = false;
    this.help.buttonMode = false;

    if (this.fromOutro == 1) {
        this.fromOutro = 0;
        this.restartExercise();
    }

    this.animationContainer.visible = true;
    this.mouth.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
 0-2 sec arm_1 => 0
2-5 sec arm_2 => 1
5-9 sec arm_1 => 0
9-10 sec arm_3 =>2
stop at arm_1 => 0

 */
Football_3_1.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    if (currentPostion >= 1000 && currentPostion < 2000) {
        this.showHandAt(0);
    } else if (currentPostion >= 2000 && currentPostion < 5000) {
        this.showHandAt(1);
    } else if (currentPostion >= 5000 && currentPostion < 9000) {
        this.showHandAt(0);
    } else if (currentPostion >= 9000 && currentPostion < 10000) {
        this.showHandAt(2);
    } else {
        this.showHandAt(0);
    }
};

/**
 * cancel running intro animation
 */
Football_3_1.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    this.animationContainer.visible = false;
    clearInterval(this.timerId);
    this.mouth.hide(2);
    this.showHandAt(2);

    this.help.interactive = true;
    this.help.buttonMode = true;
    this.ovaigen.visible = false;
    main.overlay.visible = false;
}

Football_3_1.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        var to = setTimeout(function() {
            self.mouthOutro.show();
        }, this.outroSoundTimeout);
    }

    this.cancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.cancelHandler);
}

Football_3_1.prototype.cancelOutro = function() {

    var self = this;

    createjs.Sound.stop();
    self.mouthOutro.hide(0);
    self.ovaigen.visible = true;
    self.help.interactive = true;
    self.help.buttonMode = true;
    main.overlay.visible = false;


};


/**
 * create sprites,animation,sound related to intro scene
 */
Football_3_1.prototype.introScene = function() {

    var self = this;

    this.goalKeeperIntro = new PIXI.Sprite.fromFrame('goalkeeperbody.png');
    this.goalKeeperIntro.position.set(1152, 85);
    main.texturesToDestroy.push(this.goalKeeperIntro);
    this.animationContainer.addChild(this.goalKeeperIntro);
    this.animationContainer.visible = false;

    this.mouth = new Animation({
        "image": "mouth",
        "length": 5,
        "x": 1216,
        "y": 262,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 5, 2, 4, 1, 5, 3],
        "frameId": 0
    });

    this.animationContainer.addChild(this.mouth);

    this.hands = [
        new PIXI.Sprite.fromFrame('arms1.png'),
        new PIXI.Sprite.fromFrame('arms2.png'),
        new PIXI.Sprite.fromFrame('arms3.png')
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(836, 139);
        this.animationContainer.addChild(this.hands[i]);
        if (i != 0) this.hands[i].visible = false;
    };

    this.showHandAt(0);
    //this.animationContainer.visible = false;


}

/**
 * create sprites,animation,sound related to outro scene
 */
Football_3_1.prototype.outroScene = function() {

    var self = this;

    this.outroContainer.visible = false;

    this.keeperHand = PIXI.Sprite.fromFrame("arms1.png");
    this.keeperHand.position.set(1203, 139);
    main.texturesToDestroy.push(this.keeperHand);
    this.outroContainer.addChild(this.keeperHand);

    this.goalKeeperOutro = PIXI.Sprite.fromFrame("goalkeeperbodyoutro.png");
    this.goalKeeperOutro.position.set(1526, 86);
    main.texturesToDestroy.push(this.goalKeeperOutro);
    this.outroContainer.addChild(this.goalKeeperOutro);

    this.mouthOutro = new Animation({
        "image": "mouth",
        "length": 5,
        "x": 1583,
        "y": 262,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 5, 2, 4, 1, 5, 3],
        "frameId": 0
    });

    this.outroContainer.addChild(this.mouthOutro);

    // back to intro page
    this.ovaigen = PIXI.Sprite.fromFrame('ovaigen.png');
    this.ovaigen.position.set(1153, 225);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.ovaigen.visible = false;
    this.outroContainer.addChild(this.ovaigen);
}

/**
 * load exercise related stuffs
 */
Football_3_1.prototype.loadExercise = function() {

    //render green and red feedback folders

    this.redButton = createRect({
        w: 730,
        h: 85,
        color: 0xff0000
    });
    this.greenButton = createRect({
        w: 730,
        h: 85,
        color: 0x00ff00
    });

    this.redButton.alpha = this.greenButton.alpha = 0.6;
    this.redButton.visible = false;
    this.greenButton.visible = false;
    this.introContainer.addChild(this.redButton);
    this.introContainer.addChild(this.greenButton);

    /*this.soundTexture = PIXI.Texture.fromFrame("sound_grey.png");
    main.texturesToDestroy.push(this.soundTexture);*/

    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.exerciseContainer);
    this.initObjectPooling();
}

/**
 * Set hidden motive images
 */
Football_3_1.prototype.setHiddenMotiveImge = function(first_argument) {
    for (var i = this.motiveContainer.children.length - 1; i >= 0; i--) {
        this.motiveContainer.removeChild(this.motiveContainer.children[i]);
    };
    // var feedback_item = [1, 2, 3, 4, 5, 6];

    var random_index = this.randomIndex;

    var hiddenMotive = PIXI.Sprite.fromImage(Football_3_1.imagePath + "feedback" + random_index + ".png");
    this.outroId = loadSound(Football_3_1.audioPath + 'outro_' + random_index);

    if (random_index == 1) {
        this.outroSoundTimeout = 2000; //These are the times in millisecond after what time the outro audio is played show the mouth of the outro person.
    } else if (random_index == 2) {
        this.outroSoundTimeout = 5000;
    } else if (random_index == 4) {
        this.outroSoundTimeout = 1500;
    } else if (random_index == 6) {
        this.outroSoundTimeout = 6000;
    } else {
        this.outroSoundTimeout = 1000;
    }
    hiddenMotive.position = {
        x: 3,
        y: 3
    };
    hiddenMotive.width = 1060;
    main.texturesToDestroy.push(hiddenMotive);
    this.motiveContainer.addChild(hiddenMotive);
};

/**
 * manage exercise with new sets randomized
 */
Football_3_1.prototype.runExercise = function() {
    var self = this;
    this.setHiddenMotiveImge();
    //now add sentence place holder to the white part right side.
    this.sentence = [];
    for (var k = 0; k < 4; k++) {
        var sentenceHolder = new PIXI.Text("", {
            font: "60px Arial"
        })

        sentenceHolder.position = {
            x: 1150,
            y: 300 + (k * 80)
        };

        this.sentence.push(sentenceHolder);
        self.exerciseContainer.addChild(sentenceHolder);
    }

    this.greenButtonClicked = [];
    this.imageButtonClicked = [];
    this.jsonData = this.wallSlices[0];

    //render exercise words, images and boxes
    this.boxPositions = [
        3, 0,
        354, 0,
        708, 0,
        3, 301,
        354, 301,
        708, 301,
        3, 602,
        354, 602,
        708, 602,
        3, 903,
        354, 903,
        708, 903,
    ];
    for (var k = 0; k < this.jsonData.exercise_words.length; k++) {
        //WHITE BOX WITH WORDS AND IMAGES

        var whiteBoxImages = new PIXI.Sprite.fromFrame("commonwhiteblock.png");
        whiteBoxImages.position = {
            x: this.boxPositions[k * 2],
            y: this.boxPositions[k * 2 + 1]
        };
        whiteBoxImages.interactive = false;
        whiteBoxImages.buttonMode = true;
        //add image to white box
        var exerciseImage = this.jsonData.images[k];
        exerciseImage.position = {
            x: (whiteBoxImages.width - 241) / 2,
            y: 10
        };
        main.texturesToDestroy.push(whiteBoxImages);
        whiteBoxImages.addChild(exerciseImage);
        whiteBoxImages.part = this.jsonData.parts[k];
        whiteBoxImages.clickedText = this.jsonData.exercise_words[k];
        whiteBoxImages.hitIndex = k;
        //now add text
        if (this.jsonData.parts[k] != 1) { //Person names has already name written in the image so we don't need caption here
            var wordText = new PIXI.Text(this.jsonData.exercise_words[k], {
                font: "40px Arial"
            });
            wordText.position = {
                x: (whiteBoxImages.width - wordText.width) / 2,
                y: 240
            };
            whiteBoxImages.addChild(wordText);
        }
        this.imageButtonClicked.push(whiteBoxImages);
        self.exerciseContainer.addChild(whiteBoxImages);

        //now add green football ground pieces
        var greenPieces = new PIXI.Sprite.fromFrame("piece" + (k + 1) + ".png");
        greenPieces.position = {
            x: this.boxPositions[k * 2],
            y: this.boxPositions[k * 2 + 1]
        };
        greenPieces.interactive = true;
        greenPieces.buttonMode = true;
        greenPieces.part = this.jsonData.parts[k];
        greenPieces.hitIndex = k;
        main.texturesToDestroy.push(greenPieces);
        this.greenButtonClicked.push(greenPieces);
        self.exerciseContainer.addChild(greenPieces);
    }


    var arrayOpenedCardsParts = []; //this array will contain the part(1,2 or 3) from which the clicked words come
    var arrayOpenedCards = []; //this will hold the index of the opened cards
    var arrayClickedImages = [];
    var arrayClickedText = [];
    var arrayOpenedImage = [];
    var currentSentence = 0;

    for (var k = 0; k < this.greenButtonClicked.length; k++) {

        //CLICK FUNCTION FOR GREEN FOOTBALL GROUND PIECES.
        this.greenButtonClicked[k].click = this.greenButtonClicked[k].tap = function() {
            if (addedListeners) return false;
            this.visible = false;
            arrayOpenedCardsParts.push(this.part);
            arrayOpenedCards.push(this.hitIndex);
            if (arrayOpenedCardsParts.length == 3) {
                self.toggleInteractive(false);
                var hasDuplicates = arrayOpenedCardsParts.hasDuplicates();

                if (hasDuplicates === true) { //three opened cards cannot make a sentence. Flip back the opened cards. The 3 opened cards must contain part 1,2,3
                    var a = setTimeout(function() {
                        wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                        //As opened cards doesn't form a sentence close it again.
                        self.greenButtonClicked[arrayOpenedCards[0]].visible = true;
                        self.greenButtonClicked[arrayOpenedCards[1]].visible = true;
                        self.greenButtonClicked[arrayOpenedCards[2]].visible = true;
                        self.toggleInteractive(true); // 3 opened cards doesn't form the sentence, so set interactivity of all green pieces to TRUE

                        arrayOpenedCardsParts = [];
                        arrayOpenedCards = [];
                    }, 2000);
                } else { //The opened cards can form the sentence. Just set the interactivity of visible image box to TRUE
                    self.imageButtonClicked[arrayOpenedCards[0]].interactive = true;
                    self.imageButtonClicked[arrayOpenedCards[1]].interactive = true;
                    self.imageButtonClicked[arrayOpenedCards[2]].interactive = true;
                    arrayOpenedCardsParts = [];
                    arrayOpenedCards = [];
                }
            }
        }

        //CLICK FUNCTION FOR WHITE BOX WITH IMAGES.

        this.imageButtonClicked[k].click = this.imageButtonClicked[k].tap = function() {
            if (addedListeners) return false;
            arrayClickedText[currentSentence] = (arrayClickedText.length == 0) ? this.clickedText : arrayClickedText[currentSentence] + " " + this.clickedText;
            self.sentence[currentSentence].setText("" + arrayClickedText[currentSentence]);
            arrayClickedImages.push(this.part);
            arrayOpenedImage.push(this.hitIndex);
            this.interactive = false;
            if (arrayClickedImages.length == 3) {
                var check = arrayClickedImages.join() === "1,2,3"; //to form a valid sentence the, parts from which each word comes should be in sequence.
                if (check) { //correct answer
                    var b = setTimeout(function() {
                        correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                        self.common.showRedOrGreenBox(1079, 305 + (currentSentence * 80), self.greenButton);
                        currentSentence++;
                        if (currentSentence >= 4) {
                            self.correctFeedback();
                        }
                        self.imageButtonClicked[arrayOpenedImage[0]].visible = false;
                        self.imageButtonClicked[arrayOpenedImage[1]].visible = false;
                        self.imageButtonClicked[arrayOpenedImage[2]].visible = false;
                        arrayOpenedImage = [];
                        arrayClickedText = [];
                        arrayClickedImages = [];
                        self.toggleInteractive(true);

                    }, 1500);

                } else { //wrong answer
                    var b = setTimeout(function() {
                        wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                        self.common.showRedOrGreenBox(1079, 305 + (currentSentence * 80), self.redButton);
                        self.imageButtonClicked[arrayOpenedImage[0]].interactive = true;
                        self.imageButtonClicked[arrayOpenedImage[1]].interactive = true;
                        self.imageButtonClicked[arrayOpenedImage[2]].interactive = true;
                        arrayOpenedImage = [];
                        arrayClickedText = [];
                        arrayClickedImages = [];
                    }, 1500);

                    var c = setTimeout(function() {
                        self.sentence[currentSentence].setText("");
                    }, 2500)
                }


            }
        }
    }



}

Football_3_1.prototype.showHideGreenOverlay = function(doWhat) {
    for (var k = 0; k < this.greenButtonClicked.length; k++) {
        this.greenButtonClicked[k].visible = doWhat || false;
    }
};

/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
Football_3_1.prototype.correctFeedback = function() {
    var self = this;
    setTimeout(this.onComplete.bind(this), 2000);
}

Football_3_1.prototype.toggleInteractive = function(doWhat) {
    for (var k = 0; k < this.greenButtonClicked.length; k++) {
        this.greenButtonClicked[k].interactive = doWhat;
    }
};


/**
 * custom method of an array to check whether the given array has duplicate elements. If there is duplicate the method returns true.
 */
Array.prototype.hasDuplicates = function() {
    var r = new Array();
    var dupCount = 0;
    o: for (var i = 0, n = this.length; i < n; i++) {
        for (var x = 0, y = r.length; x < y; x++) {
            if (r[x] == this[i]) {
                dupCount++;
                continue o;
            }
        }
        r[r.length] = this[i];
    }

    return dupCount > 0;
}

/**
 * prefetch exercise data and cache number of sets defined only.
 */
Football_3_1.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/football/football_3_1.json?nocache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        self.pool = new ExerciseSpritesPool(loader.json);
        //borrow two sprites once
        self.borrowWallSprites(1);
        self.runExercise();
    });
    loader.load();
}

Football_3_1.prototype.borrowWallSprites = function(num) {

    var self = this;

    for (var i = 0; i < num; i++) {

        var sprite = this.pool.borrowSprite(),
            spriteObj = sprite[0],
            imageSpriteArr = [],
            exerciseWordsArr = [],
            partsArray = [];

        this.poolSlices.push(sprite);

        for (var k = 0; k < spriteObj.exercise_words.length; k++) {
            exerciseWordsArr.push(spriteObj.exercise_words[k]);
            partsArray.push(spriteObj.parts[k]);
            if (spriteObj.parts[k] == 1) //Person names
            {
                imageSpriteArr.push(PIXI.Sprite.fromImage('uploads/images/names/' + spriteObj.images[k]));
            } else {
                imageSpriteArr.push(PIXI.Sprite.fromImage('uploads/images/' + spriteObj.images[k]));
            }

        }

        shuffle(exerciseWordsArr, partsArray, imageSpriteArr);

        this.wallSlices.push({
            exercise_words: exerciseWordsArr,
            parts: partsArray,
            images: imageSpriteArr
        });
    }
};

Football_3_1.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];

    this.pool.returnSprite(sprite);

    this.poolSlices = [];
    this.wallSlices.shift();
};

Football_3_1.prototype.getNewExerciseSet = function() {

    this.returnWallSprites();
    this.borrowWallSprites(1);
    for (var i = this.exerciseContainer.children.length - 1; i >= 0; i--) {
        this.exerciseContainer.removeChild(this.exerciseContainer.children[i]);
    };
    this.runExercise();
}

Football_3_1.prototype.restartExercise = function() {
    this.randomIndex = makeUniqueRandom(6) + 1;
    this.outroContainer.visible = false;
    this.introContainer.visible = true;

    this.setHiddenMotiveImge();
    this.ovaigen.visible = false;
    this.introMode = true;

    this.getNewExerciseSet();
};

/**
 * the outro scene is displayed
 */
Football_3_1.prototype.onComplete = function() {
    var self = this;

    this.introMode = false;

    this.help.interactive = false;
    this.help.buttonMode = false;
    this.introContainer.visible = false;
    this.outroContainer.visible = true;
    this.animationContainer.visible = false;
    this.fromOutro = 1;

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        this.visible = false;
        self.fromOutro = 0;
        main.eventRunning = false;
        self.cancelOutro();
        self.restartExercise();
    }

    this.outroAnimation();

}

/**
 * clean memory by clearing reference of object
 */
Football_3_1.prototype.cleanMemory = function() {

    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    if (this.assetsToLoads)
        this.assetsToLoad.length = 0;


    this.help = null;

    this.fromOutro = null;


    this.ovaigen = null;

    if (this.jsonData)
        this.jsonData.length = 0;


    if (this.wallSlices)
        this.wallSlices.length = 0;

    if (this.poolSlices)
        this.poolSlices.length = 0;

    this.pool = null;
    this.num = null;

    this.introContainer = null;
    this.outroContainer = null;
    this.animationContainer = null;

    this.common = null;

    if (this.hands)
        this.hands.length = 0;

    if (this.redButton)
        this.redButton.clear();
    this.redButton = null;

    if (this.greenButton)
        this.greenButton.clear();

    this.greenButton = null;
    this.randomIndex = null;

    this.exerciseContainer = null;

    this.introId = null;
    this.outroId = null;

    this.helpContainer = null;
    this.helpContainer = null;
    this.motiveContainer = null;
    this.goalKeeperIntro = null;
    this.mouth = null;
    this.sentence = null;


    this.keeperHand = null;
    this.goalKeeperOutro = null;
    this.mouthOutro = null;


    if (this.greenButtonClicked)
        this.greenButtonClicked.length = 0;

    if (this.imageButtonClicked)
        this.imageButtonClicked.length = 0;


    if (this.boxPositions)
        this.boxPositions.length = 0;


    for (var i = this.sceneContainer.children.length - 1; i >= 0; i--) {
        this.sceneContainer.removeChild(this.sceneContainer.children[i]);
    };
    this.sceneContainer = null;

}