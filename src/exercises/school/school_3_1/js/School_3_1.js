function School_3_1() {
    cl.show()

    PIXI.DisplayObjectContainer.call(this);

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position.set(77, 88);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();
    this.feedbackContainer = new PIXI.DisplayObjectContainer();
    this.answerBoxContainer = new PIXI.DisplayObjectContainer();
    this.animationContainer = new PIXI.DisplayObjectContainer();

    localStorage.setItem('school_3_1', 0);
    main.CurrentExercise = "school_3_1";

    main.texturesToDestroy = [];
    this.buttonClicked = [];

    this.common = new Common();
    this.loadSpriteSheet();


    // everything connected to this is rendered.
    this.addChild(this.sceneContainer);
}

School_3_1.constructor = School_3_1;
School_3_1.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
School_3_1.imagePath = 'build/exercises/school/school_3_1/images/';
School_3_1.audioPath = 'build/exercises/school/school_3_1/audios/';
School_3_1.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

/**
 * load array of assets of different format
 */
School_3_1.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(School_3_1.audioPath + 'intro');
    this.outroId = loadSound(School_3_1.audioPath + 'outro');
    // create an array of assets to load
    this.assetsToLoad = [
        School_3_1.imagePath + "sprite_school_3_1.json",
        School_3_1.imagePath + "sprite_school_3_1.png",
        School_3_1.imagePath + "basebg.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
School_3_1.prototype.spriteSheetLoaded = function() {

    // hide loader
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    this.sceneBackground = new PIXI.Sprite.fromImage(School_3_1.imagePath + "basebg.png");
    this.sceneBackground.position = {
        x: 0,
        y: 0
    };
    main.texturesToDestroy.push(this.sceneBackground);
    this.sceneContainer.addChild(this.sceneBackground);

    // exercise instruction
    this.help = PIXI.Sprite.fromFrame("topcorner.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);
    this.sceneContainer.addChild(this.help);

    this.loadExercise();
    this.introScene();
    this.outroScene();

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.outroContainer);
    this.introContainer.addChild(this.answerBoxContainer);
    this.sceneContainer.addChild(this.animationContainer);
    this.sceneContainer.addChild(this.feedbackContainer);
}

/**
 * hand visibility
 * @param  {int} number
 */
School_3_1.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    };
    if (isNumeric(number))
        this.hands[number].visible = true;
}

/**
 * play intro animation
 */
School_3_1.prototype.introAnimation = function() {

    createjs.Sound.stop();

    if (!this.introMode)
        this.restartExercise();

    this.help.interactive = false;
    this.help.buttonMode = false;

    main.overlay.visible = true;

    this.mouthParams.show();
    this.answerBoxContainer.removeChildren();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
0-1 sec arm_1
1-4 sec arm_2
4-6.5 arm_3
stop at arm_1
 */
School_3_1.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    if (currentPostion >= 1000 && currentPostion < 4000) {
        this.showHandAt(1);
    } else if (currentPostion >= 4000 && currentPostion < 6500) {
        this.showHandAt(2);
    } else {
        this.showHandAt(0);
    }
};

/**
 * cancel running intro animation
 */
School_3_1.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);

    this.showHandAt(0);
    this.mouthParams.hide();

    this.help.interactive = true;
    this.help.buttonMode = true;

    this.ovaigen.visible = false;
    main.overlay.visible = false;
}

/**
 * play outro animation
 */
School_3_1.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.mouthParams.show();
        self.eyeParams.show();
    }
    this.outroCancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.outroCancelHandler);

}

/**
 * cancel outro animation
 */
School_3_1.prototype.cancelOutro = function() {

    var self = this;

    createjs.Sound.stop();
    self.mouthParams.hide();
    self.ovaigen.visible = true;
    self.help.interactive = true;
    self.help.buttonMode = true;
    main.overlay.visible = false;
};

/**
 * create sprites,animation,sound related to intro scene
 */
School_3_1.prototype.introScene = function() {

    var self = this;

    this.kitchen_lady = new PIXI.Sprite.fromFrame('lady_body.png');
    this.kitchen_lady.position.set(989, 86);
    main.texturesToDestroy.push(this.kitchen_lady);
    this.animationContainer.addChild(this.kitchen_lady);

    this.mouthParams = new Animation({
        "image": "mouth",
        "length": 4,
        "x": 1089,
        "y": 329,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 2, 4, 1, 3],
        "frameId": 0
    });

    this.feedBackLadyFirst = new PIXI.Sprite.fromImage("lady_feedback_1.png");
    this.feedBackLadyFirst.position = {
        "x": 900,
        "y": 87
    };

    this.feedBackLadyFirst.visible = false;
    this.introContainer.addChild(this.feedBackLadyFirst);


    this.feedBackLadySecond = new PIXI.Sprite.fromImage("lady_feedback_2.png");
    this.feedBackLadySecond.position = {
        "x": 900,
        "y": 87
    };

    this.feedBackLadySecond.visible = false;
    this.introContainer.addChild(this.feedBackLadySecond);


    this.animationContainer.addChild(this.mouthParams);

    this.eyeParams = new Animation({
        "image": "eyes",
        "length": 2,
        "x": 1049,
        "y": 248,
        "speed": 2,
        "pattern": true,
        "sequence": [1, 2, 2, 1, 1, 2, 2, 1, 2, 1, 1, 2],
        "frameId": 2
    });
    self.eyeParams.show();
    this.animationContainer.addChild(this.eyeParams);

    this.hands = [
        new PIXI.Sprite.fromFrame('arm1.png'),
        new PIXI.Sprite.fromFrame('arm2.png'),
        new PIXI.Sprite.fromFrame('arm3.png'),
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(609, 253);
        this.animationContainer.addChild(this.hands[i]);
        if (i != 0) this.hands[i].visible = false;
    };
    //this.animationContainer.visible = false;
}

/**
 * create sprites,animation,sound related to outro scene
 */
School_3_1.prototype.outroScene = function() {

    var self = this;

    this.outroContainer.visible = false;

    // back to intro page
    this.ovaigen = PIXI.Sprite.fromFrame('ovaigen.png');
    this.ovaigen.position.set(360, 369);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.ovaigen.visible = false;
    this.outroContainer.addChild(this.ovaigen);
}

/**
 * load exercise related stuffs
 */
School_3_1.prototype.loadExercise = function() {

    this.buttonTexture = PIXI.Texture.fromFrame("answer_button.png");

    this.kitchen_table = new PIXI.Sprite.fromFrame("table_desk.png");
    this.kitchen_table.position = {
        x: 784,
        y: 698
    };
    main.texturesToDestroy.push(this.kitchen_table);
    this.sceneContainer.addChild(this.kitchen_table);


    this.vessel_lid = new PIXI.Sprite.fromFrame("lid.png");
    this.vessel_lid.position = {
        x: 1405,
        y: 640
    };

    this.vessel_lid.old_position = this.vessel_lid.position;

    main.texturesToDestroy.push(this.vessel_lid);
    this.sceneContainer.addChild(this.vessel_lid);


    //render folders
    this.buttonPositions = [659, 807];
    this.buttonLabels = ["Slutar lika", "Slutar olika"];



    for (var k = 0; k < 2; k++) {

        var buttonSprite = new PIXI.Sprite(this.buttonTexture);

        buttonSprite.position = {
            x: 247,
            y: this.buttonPositions[k]
        };
        this.introContainer.addChild(buttonSprite);
        var btnLabel = new PIXI.Text(this.buttonLabels[k], {
            font: "bold 55px Arial"
        });
        btnLabel.position = {
            x: k == 1 ? 55 : 69,
            y: 36
        };
        buttonSprite.interactive = true;
        buttonSprite.buttonMode = true;
        buttonSprite.endsSame = k == 0 ? 1 : 0;
        buttonSprite.x = 247;
        buttonSprite.y = this.buttonPositions[k];
        buttonSprite.hitIndex = k;
        buttonSprite.addChild(btnLabel);
        this.buttonClicked.push(buttonSprite);
    }

    //render green and red feedback folders
    this.redButton = new PIXI.Sprite.fromFrame("answer_red.png");
    this.greenButton = new PIXI.Sprite.fromFrame("answer_green.png");
    this.redButton.visible = false;
    this.greenButton.visible = false;
    this.introContainer.addChild(this.redButton);
    this.introContainer.addChild(this.greenButton);

    this.soundTexture = PIXI.Texture.fromFrame("sound_grey.png");
    main.texturesToDestroy.push(this.redButton);
    main.texturesToDestroy.push(this.greenButton);

    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.exerciseContainer);

    this.createFeedBack();
    this.initObjectPooling();

}

/**
 * Render feedback to the container
 */
School_3_1.prototype.createFeedBack = function() {
    var self = this;
    var feedback = new PIXI.Texture.fromFrame("fb.png");
    var feedbackPositions = [
        1048, 782,
        1072, 839,
        1006, 828,
        1138, 828,
        1207, 812,
        1102, 773,
        1162, 769,
        1130, 711,
        1072, 718,
        1102, 660
    ];
    for (var k = 0; k < 10; k++) {
        var feedbackSprite = new PIXI.Sprite(feedback);

        feedbackSprite.position = {
            x: feedbackPositions[k * 2],
            y: feedbackPositions[k * 2 + 1]
        };
        main.texturesToDestroy.push(feedbackSprite);
        this.feedbackContainer.addChild(feedbackSprite);
        feedbackSprite.visible = false;
    }
};

/**
 * manage exercise with new sets randomized
 */
School_3_1.prototype.runExercise = function() {

    var self = this;

    this.jsonData = this.wallSlices[0];
    this.whiteGraphics = createRect({
        w: 335,
        h: 335,
        color: 0xffffff,
        lineStyle: 1
    });
    this.whiteRect = this.whiteGraphics.generateTexture();



    for (var k = 0; k < 2; k++) {
        var drawWhiteBox = new PIXI.Sprite(this.whiteRect);
        drawWhiteBox.position = {
            x: k == 0 ? 106 : 467,
            y: 110
        };
        //drawWhiteBox.width = drawWhiteBox.height = 335;
        drawWhiteBox.buttonMode = true;
        drawWhiteBox.interactive = true;
        self.exerciseContainer.addChild(drawWhiteBox);

        var imageSprite = this.jsonData.image[k];
        imageSprite.width = imageSprite.height = 335;
        /*imageSprite.position = {
            x: 28,
            y: 16
        };*/
        drawWhiteBox.addChild(imageSprite);

        var sound = new PIXI.Sprite(this.soundTexture);
        sound.interactive = true;
        sound.buttonMode = true;
        sound.position = {
            x: 10,
            y: 284
        };
        sound.howl = this.jsonData.sound[k];
        sound.click = sound.tap = this.onSoundPressed.bind(sound);

        drawWhiteBox.addChild(sound);
    }

    this.buttonClicked[0].click = this.buttonClicked[0].tap = this.buttonClicked[1].click = this.buttonClicked[1].tap = function() {
        if (addedListeners) return false;
        addedListeners = true;
        self.buttonClicked[0].interactive = false;
        self.buttonClicked[1].interactive = false;

        var clickObj = this;
        if (clickObj.endsSame == self.jsonData.answer) { //Correct answer
            correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
            self.common.showRedOrGreenBox(this.x, this.y, self.greenButton);

            var a = setTimeout(function() {
                self.showCorrectWords(self.jsonData.word);
            }, 400);

            var b = setTimeout(function() {
                self.animationContainer.visible = false;
                self.vessel_lid.visible = false;
                self.feedBackLadyFirst.visible = true;
            }, 1600);

            var c = setTimeout(function() {
                self.feedBackLadyFirst.visible = false;
                self.feedBackLadySecond.visible = true;
                self.vessel_lid.visible = true;
                self.vessel_lid.position = {
                    x: 800,
                    y: 700
                };
            }, 2000)

            var d = setTimeout(function() {
                self.feedBackLadySecond.visible = false;
                self.animationContainer.visible = true;
                self.vessel_lid.visible = true;
                self.vessel_lid.position = self.vessel_lid.old_position;
                self.correctFeedback();
            }, 3000);
        } else {
            wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
            addedListeners = false;
            self.common.showRedOrGreenBox(this.x, this.y, self.redButton);
            var a = setTimeout(function() {
                self.wrongFeedback()
            }, 800);
        }
    }
}


School_3_1.prototype.onSoundPressed = function() {
    createjs.Sound.stop();
    this.soundInstance = createjs.Sound.play(this.howl, {
        interrupt: createjs.Sound.INTERRUPT_NONE
    });
    this.soundInstance.addEventListener("failed", handleFailed);
    this.soundInstance.addEventListener("complete", handleComplete);
};



/**
 * show correct words
 * @param  {array} wordArray
 */
School_3_1.prototype.showCorrectWords = function(wordArray) {

    var textBox = createRect({
        w: 335,
        h: 76,
        color: 0xffffff,
        lineStyle: 0
    });

    var whiteRect = textBox.generateTexture();
    for (var k = 0; k < 2; k++) {
        var drawWhiteBox = new PIXI.Sprite(whiteRect);
        drawWhiteBox.position = {
            x: k == 0 ? 106 : 473,
            y: 462
        };

        var firstPartText = wordArray[k].substring(0, wordArray[k].length - 2);
        var secondPartText = wordArray[k].slice(-2);

        var alphabetsFirst = new PIXI.Text("" + firstPartText, {
            font: "55px Arial",
            fill: "black"
        });

        var alphabetsSecond = new PIXI.Text("" + secondPartText, {
            font: "bold 55px Arial",
            fill: "black"
        });

        var combinedWidth = parseInt(alphabetsFirst.width) + parseInt(alphabetsSecond.width);
        var xpos = (360 - combinedWidth) / 2;

        alphabetsFirst.position = {
            x: xpos,
            y: 5
        };

        alphabetsSecond.position = {
            x: xpos + parseInt(alphabetsFirst.width),
            y: 5
        };

        drawWhiteBox.addChild(alphabetsFirst);
        drawWhiteBox.addChild(alphabetsSecond);


        this.answerBoxContainer.addChild(drawWhiteBox);

    }
};

/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
School_3_1.prototype.correctFeedback = function() {
    var self = this;
    var feedback = localStorage.getItem('school_3_1');
    feedback++;

    localStorage.setItem('school_3_1', feedback);
    this.updateFeedback();

    if (feedback >= School_3_1.totalFeedback) {
        setTimeout(this.onComplete.bind(this), 1000);
    } else {
        setTimeout(function() {
            self.getNewExerciseSet();
            addedListeners = false;
        }, 1600);
    }
}

/**
 * on wrong feedback, feedback decrement by 1
 */
School_3_1.prototype.wrongFeedback = function() {
    var feedback = localStorage.getItem('school_3_1');
    feedback--;

    if (feedback < 0)
        feedback = 0;

    localStorage.setItem('school_3_1', feedback);
    this.updateFeedback();

}


/**
 * update feedback
 */
School_3_1.prototype.updateFeedback = function() {

    var self = this;

    self.buttonClicked[0].interactive = true;
    self.buttonClicked[1].interactive = true;

    var counter = localStorage.getItem('school_3_1');

    for (var i = 0; i < self.feedbackContainer.children.length; i++) {
        self.feedbackContainer.children[i].visible = false;
    }

    for (var j = 0; j < counter; j++) {
        self.feedbackContainer.children[j].visible = true;
    }
}

/**
 * prefetch exercise data and cache number of sets defined only.
 */
School_3_1.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/school/school_3_1.json?nocache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        self.pool = new ExerciseSpritesPool(loader.json);
        //borrow two sprites once
        self.borrowWallSprites(2);
        self.runExercise();
    });
    loader.load();
}

/**
 * borrow objects from pool
 */
School_3_1.prototype.borrowWallSprites = function(num) {

    var self = this;

    for (var i = 0; i < num; i++) {

        var sprite = this.pool.borrowSprite(),
            spriteObj = sprite[0],
            imageSpriteArr = [],
            wordArr = [],
            soundHowlArr = [];

        this.poolSlices.push(sprite);
        for (var k = 0; k < 2; k++) {
            var imgSprite = PIXI.Sprite.fromImage('uploads/images/' + spriteObj.image[k]);
            imageSpriteArr.push(imgSprite);

            var soundHowl = loadSound('uploads/audios/' + StripExtFromFile(spriteObj.sound[k]));

            soundHowlArr.push(soundHowl);

            wordArr.push(spriteObj.word[k]);
        }

        shuffle(imageSpriteArr, soundHowlArr, wordArr);

        this.wallSlices.push({
            image: imageSpriteArr,
            sound: soundHowlArr,
            word: wordArr,
            answer: spriteObj.answer
        });
    }
};

/**
 * retrun objects to pool
 */
School_3_1.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];

    this.pool.returnSprite(sprite);

    this.poolSlices = [];
    this.wallSlices.shift();
};

/**
 * get next set
 */
School_3_1.prototype.getNewExerciseSet = function() {

    this.returnWallSprites();
    this.borrowWallSprites(1);
    addedListeners = false;
    this.answerBoxContainer.removeChildren();
    this.runExercise();
}

/**
 * restart exercise by resetting default values
 */
School_3_1.prototype.restartExercise = function() {
    this.resetExercise();

    localStorage.setItem('school_3_1', 0);
    this.updateFeedback();

    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    this.introMode = true;

    this.getNewExerciseSet();
};

/**
 * the outro scene is displayed
 */
School_3_1.prototype.onComplete = function() {
    var self = this;

    this.introMode = false;

    this.help.interactive = false;
    this.help.buttonMode = false;
    this.introContainer.visible = false;
    this.outroContainer.visible = true;
    this.animationContainer.visible = true;

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        self.cancelOutro();
        self.restartExercise();
    }

    this.outroAnimation();

}

/**
 * reset exercise to default state
 */
School_3_1.prototype.resetExercise = function() {

    this.ovaigen.visible = false;
    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    this.introMode = true;
};

/**
 * clean memory by clearing reference of object
 */
School_3_1.prototype.cleanMemory = function() {

    this.introMode = null;

    if (this.assetsToLoads)
        this.assetsToLoad.length = 0;

    this.sceneBackground = null;
    this.help = null;

    this.kitchen_lady = null;
    this.mouthParams = null;


    this.figure_outro = null;
    this.ovaigen = null;

    if (this.jsonData)
        this.jsonData.length = 0;

    this.counter = null;

    if (this.wallSlices)
        this.wallSlices.length = 0;

    if (this.poolSlices)
        this.poolSlices.length = 0;

    this.pool = null;
    this.num = null;

    this.introContainer = null;
    this.outroContainer = null;
    this.feedbackContainer = null;
    this.animationContainer = null;

    this.common = null;

    if (this.buttonLabels)
        this.buttonLabels.length = 0;

    this.eyeParams = null;
    this.buttonTexture = null;
    this.kitchen_table = null;
    this.vessel_lid = null;
    this.redButton = null;
    this.greenButton = null;
    this.soundTexture = null;
    this.exerciseContainer = null;
    this.whiteRect = null;
    this.sound = null;
    this.animateFolder = null;

    this.answerBoxContainer = null;

    if (this.buttonClicked)
        this.buttonClicked.length = 0;

    if (this.buttonPositions)
        this.buttonPositions.length = 0;


    this.sceneContainer.removeChildren();
    this.sceneContainer = null;

}