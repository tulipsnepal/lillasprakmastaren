function School_4_1() {

	cl.show();

	PIXI.DisplayObjectContainer.call(this);

	this.sceneContainer = new PIXI.DisplayObjectContainer();
	this.sceneContainer.position.set(77, 88);

	this.introContainer = new PIXI.DisplayObjectContainer();
	this.outroContainer = new PIXI.DisplayObjectContainer();
	this.feedbackContainer = new PIXI.DisplayObjectContainer();

	localStorage.setItem('school_4_1', 0);
	main.CurrentExercise = "school_4_1";

	this.currentBoxLeft = 0;
	this.currentBoxRight = 0;
	this.noOfBoxDragged = 0;
	this.noOfCorrectAnswer = 0;
	this.itemsInLeftColumn = [];
	this.itemsInRightColumn = [];

	main.texturesToDestroy = [];
	this.jsonContents = {};
	this.defaultGroup = 0;
	this.loadJsonData();
	this.alreadyPlayedWords = [];

	// everything connected to this is rendered.
	this.addChild(this.sceneContainer);
}

School_4_1.constructor = School_4_1;
School_4_1.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
School_4_1.imagePath = 'build/exercises/school/school_4_1/images/';
School_4_1.audioPath = 'build/exercises/school/school_4_1/audios/';
School_4_1.totalFeedback = 5; // default 10

/**
 *Load Json Contents from json file
 */
School_4_1.prototype.loadJsonData = function() {
		var self = this;
		var jsonloader = new PIXI.JsonLoader('uploads/exercises/school/school_4_1.json?noache=' + (new Date()).getTime());
		jsonloader.on('loaded', function(evt) {
			self.jsonContents = jsonloader;
			self.loadSpriteSheet();
		});
		jsonloader.load();
	}
	/**
	 * load array of assets of different format
	 */
School_4_1.prototype.loadSpriteSheet = function() {
	this.introId = loadSound(School_4_1.audioPath + 'intro');
	this.outroId = loadSound(School_4_1.audioPath + 'outro');
	// create an array of assets to load
	this.assetsToLoad = [
		School_4_1.imagePath + "sprite_school_4_1.json",
		School_4_1.imagePath + "sprite_school_4_1.png",

	];
	// create a new loader
	loader = new PIXI.AssetLoader(this.assetsToLoad);
	// use callback
	loader.onComplete = this.spriteSheetLoaded.bind(this);
	//begin load
	loader.load();

	var feedBackIndicator = createRect({
		w: 228,
		h: 228,
		color: 0xffffff,
		alpha: 0.2,
		lineStyle: 0
	});

	var feedBackTexture = feedBackIndicator.generateTexture();
	this.feedBackRect = new PIXI.Sprite(feedBackTexture);
	main.texturesToDestroy.push(this.feedBackRect);
}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
School_4_1.prototype.spriteSheetLoaded = function() {

	// hide loader
	cl.hide();

	if (this.sceneContainer.stage)
		this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

	// fill exercise background
	this.sceneBackground = new PIXI.Graphics();
	this.sceneBackground.beginFill(0xcbe3c2);
	// set the line style to have a width of 5 and set the color to red
	this.sceneBackground.lineStyle(3, 0xec6567);
	// draw a rectangle
	this.sceneBackground.drawRect(0, 0, 1843, 1202);
	this.sceneContainer.addChild(this.sceneBackground);

	this.sceneBackgroundFloor = new PIXI.Graphics();
	this.sceneBackgroundFloor.beginFill(0xcbbb9f);
	this.sceneBackgroundFloor.drawRect(2, 882, 1839, 318);
	this.sceneContainer.addChild(this.sceneBackgroundFloor);

	this.sceneBackgroundSeparator = new PIXI.Graphics();
	this.sceneBackgroundSeparator.lineStyle(1, 0x000000);
	this.sceneBackgroundSeparator.moveTo(2, 884);
	this.sceneBackgroundSeparator.lineTo(1839, 884);
	this.sceneContainer.addChild(this.sceneBackgroundSeparator);

	// exercise instruction
	this.help = PIXI.Sprite.fromFrame("topcorner.png");
	this.help.interactive = true;
	this.help.buttonMode = true;
	this.help.position.set(-77, -88);
	this.sceneContainer.addChild(this.help);

	this.introScene();
	this.outroScene();
	this.loadExercise();

	this.initObjectPooling(this.defaultGroup);
	this.help.click = this.help.tap = this.introAnimation.bind(this);

	this.sceneContainer.addChild(this.introContainer);
	this.sceneContainer.addChild(this.outroContainer);
	this.sceneContainer.addChild(this.feedbackContainer);
}

/**
 * hand visibility
 * @param  {int} number
 */
School_4_1.prototype.showHandAt = function(number) {
	for (var i = 0; i < this.hands.length; i++) {
		this.hands[i].visible = false;
	};
	if (isNumeric(number))
		this.hands[number].visible = true;
}

/**
 * play intro animation
 */
School_4_1.prototype.introAnimation = function() {

	createjs.Sound.stop();

	if (!this.introMode)
		this.restartExercise();

	this.help.interactive = false;
	this.help.buttonMode = false;

	main.overlay.visible = true;

	this.animationContainer.bringToFront();

	this.mouthParam.show();

	this.introInstance = createjs.Sound.play(this.introId, {
		interrupt: createjs.Sound.INTERRUPT_ANY
	});

	if (createjs.Sound.isReady())
		this.timerId = setInterval(this.animateIntro.bind(this), 1);

	this.introCancelHandler = this.cancelIntro.bind(this);
	this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
0-2 sec arm_1
2-5.5 sec arm_3
5.5-8 sec arm_2
stop at arm_1
 */
School_4_1.prototype.animateIntro = function() {

	var currentPostion = this.introInstance.getPosition();
	if (currentPostion >= 2000 && currentPostion < 5500) {
		this.showHandAt(2);
	} else if (currentPostion >= 5500 && currentPostion < 8000) {
		this.showHandAt(1);
	} else {
		this.showHandAt(0);
	}
};

/**
 * cancel running intro animation
 */
School_4_1.prototype.cancelIntro = function() {

	createjs.Sound.stop();

	clearInterval(this.timerId);

	this.mouthParam.hide();
	this.showHandAt(0);
	this.help.interactive = true;
	this.help.buttonMode = true;

	this.ovaigen.visible = false;
	main.overlay.visible = false;
}

/**
 * play running outro animation
 */
School_4_1.prototype.outroAnimation = function() {

	var self = this;

	var outroInstance = createjs.Sound.play(this.outroId);
	if (createjs.Sound.isReady()) {
		main.overlay.visible = true;
		self.mouthOutro.show();
	}
	this.outroCancelHandler = this.cancelOutro.bind(this);
	outroInstance.addEventListener("complete", this.outroCancelHandler);
}

/**
 * cancel running outro animation
 */
School_4_1.prototype.cancelOutro = function() {

	var self = this;

	createjs.Sound.stop();
	self.mouthOutro.hide(0);
	self.ovaigen.visible = true;
	self.help.interactive = true;
	self.help.buttonMode = true;
	main.overlay.visible = false;

};

/**
 * Render feedback to the container
 */
School_4_1.prototype.createFeedBack = function() {
	var self = this;
	var feedback = new PIXI.Texture.fromFrame("fb.png");
	for (var k = 0; k < 5; k++) {
		var feedbackSprite = new PIXI.Sprite(feedback);

		feedbackSprite.position = {
			x: 1675,
			y: 841 - (k * 173)
		};
		main.texturesToDestroy.push(feedbackSprite);
		self.feedbackContainer.addChild(feedbackSprite);
		feedbackSprite.alpha = 0.2;
	}
};


/**
 *render the exercise letter group
 */
School_4_1.prototype.renderExerciseGroup = function() {
	var self = this;
	var groupsToShow = ["br-pr", "dr-tr", "gr-kr"];

	var groupPositions = [
		143, 517,
		143, 581,
		143, 651
	];

	var greenButton = PIXI.Sprite.fromFrame("optionselected.png");
	greenButton.position.set(96, 501);
	main.texturesToDestroy.push(greenButton);
	self.introContainer.addChild(greenButton);


	var groupButtons = [];
	for (var k = 0; k < 3; k++) {
		var alphabets = new PIXI.Text(groupsToShow[k], {
			font: "40px Arial",
			fill: "black"
		});

		alphabets.position = {
			x: groupPositions[k * 2],
			y: groupPositions[k * 2 + 1]
		};
		alphabets.interactive = true;
		alphabets.buttonMode = true;
		alphabets.hitIndex = k;
		self.introContainer.addChild(alphabets);
		groupButtons.push(alphabets);
	}
	groupButtons[0].interactive = false; //first group selected by default so set interactive to false of first group
	groupButtons[0].click = groupButtons[0].tap = groupButtons[1].click = groupButtons[1].tap = groupButtons[2].click = groupButtons[2].tap = function(data) {

		greenButton.position = {
			x: groupButtons[this.hitIndex].position.x - 48,
			y: groupButtons[this.hitIndex].position.y - 14
		};

		for (var k = 0; k < 3; k++) {
			if (this.hitIndex == k) {
				groupButtons[k].interactive = false;
			} else {
				groupButtons[k].interactive = true;
			}
		}


		// localStorage.setItem('school_4_1', 0);
		// self.showHideChildrenInContainer(self.feedbackContainer, false);
		self.defaultGroup = groupsToShow[this.hitIndex];
		//NOW RENDER GROUP TEXT IN THE SHELF
		var txtGroups = self.defaultGroup.split("-");


		self.firstShelfLabel.setText(txtGroups[0]);
		self.secondShelfLabel.setText(txtGroups[1]);

		self.exerciseBaseContainer.removeChildren();

		self.currentBoxLeft = 0;
		self.currentBoxRight = 0;
		self.noOfBoxDragged = 0;
		self.itemsInLeftColumn = [];
		self.itemsInRightColumn = [];

		if (self.wallSlices)
			self.wallSlices.length = 0;

		if (self.set)
			self.set.length = 0;

		self.initObjectPooling(this.hitIndex);
	}

}

/**
 * create sprites,animation,sound related to intro scene
 */
School_4_1.prototype.introScene = function() {

	var self = this;

	this.brownLetterBox = new PIXI.Texture.fromFrame("shelf_textbox.png");

	this.brownBox1 = new PIXI.Sprite(this.brownLetterBox);
	this.brownBox1.position.set(387, 118);
	this.sceneContainer.addChild(this.brownBox1);

	this.brownBox2 = new PIXI.Sprite(this.brownLetterBox);
	this.brownBox2.position.set(1377, 118);
	this.sceneContainer.addChild(this.brownBox2);

	this.shelf_one = new PIXI.Sprite.fromFrame('bookshelf.png');
	this.shelf_one.position.set(477, 118);
	main.texturesToDestroy.push(this.shelf_one);
	this.sceneContainer.addChild(this.shelf_one);

	this.shelf_two = new PIXI.Sprite.fromFrame('bookshelf.png');
	this.shelf_two.position.set(1145, 118);
	main.texturesToDestroy.push(this.shelf_two);
	this.sceneContainer.addChild(this.shelf_two);

	this.figure_intro = new PIXI.Sprite.fromFrame('bookman.png');
	this.figure_intro.position.set(54, 225);
	main.texturesToDestroy.push(this.figure_intro);
	this.introContainer.addChild(this.figure_intro);

	this.mouthParam = new Animation({
		"image": "mouth",
		"length": 3,
		"x": 184,
		"y": 383,
		"speed": 6,
		"pattern": true,
		"frameId": 0
	});

	this.animationContainer = new PIXI.DisplayObjectContainer();
	this.introContainer.addChild(this.animationContainer);

	this.animationContainer.addChild(this.mouthParam);

	this.hands = [
		new PIXI.Sprite.fromFrame('arm1.png'),
		new PIXI.Sprite.fromFrame('arm2.png'),
		new PIXI.Sprite.fromFrame('arm3.png'),
	];
	for (var i = 0; i < this.hands.length; i++) {
		this.hands[i].position.set(239, 431);
		this.animationContainer.addChild(this.hands[i]);
		if (i != 0) this.hands[i].visible = false;
	};
	this.animationContainer.visible = true;

	//add exercise group

	this.groupBoxPositions = [501, 568, 636];
	for (var i = 0; i < 3; i++) {
		var groupBox = new PIXI.Sprite.fromFrame("optionbox.png");
		groupBox.position.set(96, this.groupBoxPositions[i]);
		this.introContainer.addChild(groupBox);
	}

	this.rightHand = new PIXI.Sprite.fromFrame('handover.png');
	this.rightHand.position.set(75, 676);
	main.texturesToDestroy.push(this.rightHand);
	this.introContainer.addChild(this.rightHand);
	this.renderExerciseGroup();

	var defaultGroupLabel = "br-pr";
	var splittedGroup = defaultGroupLabel.split("-");
	this.firstShelfLabel = new PIXI.Text(splittedGroup[0], {
		font: "60px Arial",
		fill: "black"
	});
	this.firstShelfLabel.position.set(411, 147)
	this.sceneContainer.addChild(this.firstShelfLabel);

	this.secondShelfLabel = new PIXI.Text(splittedGroup[1], {
		font: "60px Arial",
		fill: "black"
	});
	this.secondShelfLabel.position.set(1401, 147)
	this.sceneContainer.addChild(this.secondShelfLabel);
}

/**
 * create sprites,animation,sound related to outro scene
 */
School_4_1.prototype.outroScene = function() {

	var self = this;

	this.outroContainer.visible = false;

	this.figure_outro = new PIXI.Sprite.fromFrame('bookmanoutro.png');
	this.figure_outro.position.set(34, 226);
	main.texturesToDestroy.push(this.figure_outro);
	this.outroContainer.addChild(this.figure_outro);

	this.mouthOutro = new Animation({
		"image": "mouthoutro",
		"length": 3,
		"x": 328,
		"y": 536,
		"speed": 6,
		"pattern": true,
		"sequence": [1, 2, 3, 3, 2, 1],
		"frameId": 0
	});

	this.outroContainer.addChild(this.mouthOutro);

	// back to intro page
	this.ovaigen = PIXI.Sprite.fromFrame('ovaigen.png');
	this.ovaigen.position.set(766, 566);
	this.ovaigen.interactive = true;
	this.ovaigen.buttonMode = true;
	this.ovaigen.visible = false;
	this.outroContainer.addChild(this.ovaigen);


}

/**
 * load exercise related stuffs
 */
School_4_1.prototype.loadExercise = function() {

	this.exerciseBaseContainer = new PIXI.DisplayObjectContainer();
	this.sceneContainer.addChild(this.exerciseBaseContainer);

	this.dropPositionsLeft = [
		488, 124,
		488, 348,
		488, 573,
		488, 797
	];

	this.dropPositionsRight = [
		1156, 124,
		1156, 348,
		1156, 573,
		1156, 797
	];


	// this.exerciseContainer = new PIXI.DisplayObjectContainer();
	//this.introContainer.addChild(this.exerciseContainer);

	this.soundTexture = PIXI.Texture.fromFrame("sound_grey.png");

	// this.dotBoxTextrue = PIXI.Texture.fromFrame('small_dotted_box.png');
	this.createFeedBack();

}

/**
 * manage exercise with new sets randomized
 */
School_4_1.prototype.runExercise = function() {

	var self = this;

	this.jsonData = this.wallSlices[0];

	this.dragImage = [];
	//  this.dropPosition = null;
	this.whiteGraphics = createRect({
		lineStyle: 1
	});
	this.whiteTexture = this.whiteGraphics.generateTexture();


	this.exerciseParams = [107, 343, 579, 815];
	//this.soundTexture = new PIXI.Texture.fromFrame("sound_grey.png");
	for (var i = 0; i < 4; i++) {
		var drawWhiteBox = new PIXI.Sprite(this.whiteTexture);
		drawWhiteBox.position.set(822, this.exerciseParams[i]);
		drawWhiteBox.width = drawWhiteBox.height = 225;
		drawWhiteBox.buttonMode = true;
		drawWhiteBox.interactive = true;
		drawWhiteBox.hitIndex = i;
		self.exerciseBaseContainer.addChild(drawWhiteBox);

		var imageSprite = this.jsonData.image[i];
		// imageSprite.width = imageSprite.height = 225;
		/*imageSprite.position = {
		    x: 10,
		    y: 20
		};*/
		drawWhiteBox.addChild(imageSprite);
		var sw = this.jsonData.startsWith[i];

		if (sw == "br" || sw == "dr" || sw == "gr") {
			drawWhiteBox.X1 = 385;
			drawWhiteBox.Y1 = 120;
			drawWhiteBox.X2 = 719;
			drawWhiteBox.Y2 = 1028;
		} else {
			drawWhiteBox.X1 = 1149;
			drawWhiteBox.Y1 = 120;
			drawWhiteBox.X2 = 1483;
			drawWhiteBox.Y2 = 1028;
		}

		//add audios
		var sound = new PIXI.Sprite(this.soundTexture);
		// sound.buttonMode = true;
		sound.position.x = 0;
		sound.position.y = 190;
		drawWhiteBox.addChild(sound);
		drawWhiteBox.oldPosition = [822, this.exerciseParams[i]];
		drawWhiteBox.howl = this.jsonData.sound[i];
		drawWhiteBox.howlPosition = sound.position;

		this.dragImage.push(drawWhiteBox);
	}

	for (var i = 0; i < this.dragImage.length; i++) {
		this.onDragging(i);
	};
}



/**
 * drag drop sprite
 * on correct drop, feedback is updated
 */
School_4_1.prototype.onDragging = function(i) {

	var self = this;

	// use the mousedown and touchstart
	this.dragImage[i].mousedown = this.dragImage[i].touchstart = function(data) {
		if (addedListeners) return false;
		data.originalEvent.preventDefault()
			// store a refference to the data
			// The reason for this is because of multitouch
			// we want to track the movement of this particular touch
		this.data = data;
		this.alpha = 0.9;
		this.dragging = true;
		this.sx = this.data.getLocalPosition(this).x;
		this.sy = this.data.getLocalPosition(this).y;
		this.bringToFront();

		if (isInsideRectangle(this.sx, this.sy, this.howlPosition.x, this.howlPosition.y, this.howlPosition.x + 283, this.howlPosition.y + 200)) {

			createjs.Sound.stop();
			createjs.Sound.play(this.howl);

			this.dragging = false;

		}

	}

	this.dragImage[i].mouseup = this.dragImage[i].mouseupoutside = this.dragImage[i].touchend = this.dragImage[i].touchendoutside = function(data) {
		if (addedListeners) return false;
		var dragSelf = this;

		this.dragging = false;
		// set the interaction data to null
		this.data = null;
		this.alpha = 1;

		var dropResult = self.isDropArea(this.dx, this.dy, this.hitIndex);

		var testIfDraggedOnSameColumn = self.checkDraggedInSameColumn(this.hitIndex, dropResult, dragSelf.oldPosition);
		if (testIfDraggedOnSameColumn === false) {
			self.noOfBoxDragged--;
			if (self.noOfBoxDragged < 0) self.noOfBoxDragged = 0;
			dropResult = false;
		}
		if (dropResult !== false) {

			dragSelf.position = {
				x: self.boxPlacement[0],
				y: self.boxPlacement[1]
			};

			/*console.log("Left column items = " + self.itemsInLeftColumn);
			console.log("Right column items = " + self.itemsInRightColumn);*/

			if (dropResult == "left") {
				self.currentBoxLeft++;
				self.pushItemToColumn(this.hitIndex, "left");
			} else {
				self.currentBoxRight++;
				self.pushItemToColumn(this.hitIndex, "right");
			}
			self.reArrangeItems();

			//  dragSelf.interactive = false;

			//IF ALL 4 BOXES ARE DRAGGED, THEN EVALUATE THE RESULT

			var dropped = [];
			if (self.noOfBoxDragged == 4) {
				dropped[0] = isInsideRectangle(self.dragImage[0].position.x, self.dragImage[0].position.y, self.dragImage[0].X1, self.dragImage[0].Y1, self.dragImage[0].X2, self.dragImage[0].Y2);
				dropped[1] = isInsideRectangle(self.dragImage[1].position.x, self.dragImage[1].position.y, self.dragImage[1].X1, self.dragImage[1].Y1, self.dragImage[1].X2, self.dragImage[1].Y2);
				dropped[2] = isInsideRectangle(self.dragImage[2].position.x, self.dragImage[2].position.y, self.dragImage[2].X1, self.dragImage[2].Y1, self.dragImage[2].X2, self.dragImage[2].Y2);
				dropped[3] = isInsideRectangle(self.dragImage[3].position.x, self.dragImage[3].position.y, self.dragImage[3].X1, self.dragImage[3].Y1, self.dragImage[3].X2, self.dragImage[3].Y2);


				if (dropped[0] && dropped[1] && dropped[2] && dropped[3]) //CORRECT ANSWER
				{
					var a = setTimeout(function() {
						correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
						self.feedBackRect.tint = 0x00ff00;
						for (var k = 0; k < 4; k++) {
							self.showRedGreenBox(self.dragImage[k].position.x, self.dragImage[k].position.y, self.feedBackRect);
						}
					}, 1000);

					var b = setTimeout(function() {
						dragSelf.interactive = false;
						self.correctFeedback();
					}, 1800);
				} else { //WRONG ANSWER

					var removeIndex = []
					var a = setTimeout(function() {
						wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
						for (var k = 0; k < 4; k++) {
							if (dropped[k]) {
								self.feedBackRect.tint = 0x00ff00;
								self.dragImage[k].interactive = false;
								self.showRedGreenBox(self.dragImage[k].position.x, self.dragImage[k].position.y, self.feedBackRect);
							} else {
								self.feedBackRect.tint = 0xff0000;
								self.showRedGreenBox(self.dragImage[k].position.x, self.dragImage[k].position.y, self.feedBackRect);
								removeIndex.push(k);
							}

						}
					}, 1000);

					var b = setTimeout(function() {
						for (var j = 0; j < removeIndex.length; j++) {
							var idx = removeIndex[j];
							self.dragImage[idx].interactive = true;
							self.dragImage[idx].position = {
								x: self.dragImage[idx].oldPosition[0],
								y: self.dragImage[idx].oldPosition[1]
							};
							//now remove wrong items from two columns
							var idleft = self.itemsInLeftColumn.indexOf(idx);
							if (idleft > -1) {
								self.itemsInLeftColumn.splice(idleft, 1);
								self.currentBoxLeft--;
							} else {
								var idright = self.itemsInRightColumn.indexOf(idx);
								if (idright > -1) {
									self.itemsInRightColumn.splice(idright, 1);
									self.currentBoxRight--;
								}
							}
							self.reArrangeItems();
							self.noOfBoxDragged--;
						}
						self.wrongFeedback();

						// console.log("ITEMS IN LEFT = " + self.itemsInLeftColumn);
						// console.log("ITEMS IN RIGHT = " + self.itemsInRightColumn);
					}, 1800);



				}

			}

			this.dx = 0;
			this.dy = 0;
			this.sx = 0;
			this.sy = 0;
		} else {
			var idx = self.itemsInLeftColumn.indexOf(dragSelf.hitIndex);
			if (idx > -1) {
				self.itemsInLeftColumn.splice(idx, 1);
				self.currentBoxLeft--;
				self.noOfBoxDragged--;
				if (self.currentBoxLeft < 0) self.currentBoxLeft = 0;
				if (self.noOfBoxDragged < 0) self.noOfBoxDragged = 0;
			} else {
				var idx = self.itemsInRightColumn.indexOf(dragSelf.hitIndex);
				if (idx > -1) {
					self.itemsInRightColumn.splice(idx, 1);
					self.currentBoxRight--;
					self.noOfBoxDragged--;
					if (self.currentBoxRight < 0) self.currentBoxRight = 0;
					if (self.noOfBoxDragged < 0) self.noOfBoxDragged = 0;
				}
			}
			// reset dragged element position
			dragSelf.position = {
				x: dragSelf.oldPosition[0],
				y: dragSelf.oldPosition[1]
			};

			self.reArrangeItems();
		}
	}

	// set the callbacks for when the mouse or a touch moves
	this.dragImage[i].mousemove = this.dragImage[i].touchmove = function(data) {
		if (addedListeners) return false;
		// set the callbacks for when the mouse or a touch moves
		if (this.dragging) {
			// need to get parent coords..
			var newPosition = this.data.getLocalPosition(this.parent);
			this.position.x = newPosition.x - this.sx;
			this.position.y = newPosition.y - this.sy;
			this.dx = newPosition.x;
			this.dy = newPosition.y;
		}
	};

}

/**
 * check dragged in same column
 * @param  {int} index
 * @param  {string} column
 */
School_4_1.prototype.checkDraggedInSameColumn = function(index, column, pos) {
	var self = this;
	var idx = self.itemsInLeftColumn.indexOf(index);
	var idx2 = self.itemsInRightColumn.indexOf(index);

	if ((idx > -1 && column == "left") || (idx2 > -1 && column == "right")) {
		return false;
	}

	return true;

}

/**
 * push item to column
 * @param  {int} index
 * @param  {string} column
 */
School_4_1.prototype.pushItemToColumn = function(index, column) {
	var self = this;
	if (column == "left") {
		self.itemsInLeftColumn.push(index);

		self.itemsInLeftColumn = self.itemsInLeftColumn.removeDuplicates();
		//if "index" already exist in right column, then remove it from there
		var idx = self.itemsInRightColumn.indexOf(index);
		if (idx > -1) {
			self.itemsInRightColumn.splice(idx, 1);
			self.currentBoxRight--;
			self.noOfBoxDragged--;
			if (self.currentBoxRight < 0) self.currentBoxRight = 0;
			if (self.noOfBoxDragged < 0) self.noOfBoxDragged = 0;
		}
	} else {
		self.itemsInRightColumn.push(index);
		self.itemsInRightColumn = self.itemsInRightColumn.removeDuplicates();

		//if "index" already exist in left column, then remove it from there
		var idx = self.itemsInLeftColumn.indexOf(index);
		if (idx > -1) {
			self.itemsInLeftColumn.splice(idx, 1);
			self.currentBoxLeft--;
			self.noOfBoxDragged--;
			if (self.currentBoxLeft < 0) self.currentBoxLeft = 0;
			if (self.noOfBoxDragged < 0) self.noOfBoxDragged = 0;
		}
	}


}

/**
 * re-arrange items
 */
School_4_1.prototype.reArrangeItems = function() {
	for (var k = 0; k < this.itemsInLeftColumn.length; k++) {
		var idx = this.itemsInLeftColumn[k];
		this.dragImage[idx].position = {
			x: this.dropPositionsLeft[k * 2],
			y: this.dropPositionsLeft[k * 2 + 1]
		};
	}

	for (var k = 0; k < this.itemsInRightColumn.length; k++) {
		var idx = this.itemsInRightColumn[k];
		this.dragImage[idx].position = {
			x: this.dropPositionsRight[k * 2],
			y: this.dropPositionsRight[k * 2 + 1]
		};
	}
};

/**
 * red/green sprite visibility on correct/wrong answer
 * @param  {int} xPos
 * @param  {int} yPos
 * @param  {object} obj
 */
School_4_1.prototype.showRedGreenBox = function(xPos, yPos, obj) {
	var self = this;
	this.feedTexture = obj.generateTexture();
	var ft = new PIXI.Sprite(this.feedTexture);
	this.sceneContainer.addChild(ft);
	ft.position = {
		x: xPos,
		y: yPos
	};
	ft.alpha = 0.8;
	ft.visible = true;

	setTimeout(function() {
		ft.visible = false;
	}, 400);

}

/**
 * check drag position is inside rectangular drop position
 * @param  {float} dx    drag positon x
 * @param  {float} dy    drag position y
 * @param  {integer} index
 * @return {boolean}       true/false
 */
School_4_1.prototype.isDropArea = function(dx, dy) {

	var x1 = 385,
		y1 = 120,
		x2 = x1 + 334,
		y2 = y1 + 908;

	var p1 = 1149,
		q1 = 120,
		p2 = p1 + 334,
		q2 = q1 + 908;

	if (isInsideRectangle(dx, dy, x1, y1, x2, y2)) {
		// console.log('in');
		this.boxPlacement = [this.dropPositionsLeft[this.currentBoxLeft * 2], this.dropPositionsLeft[this.currentBoxLeft * 2 + 1]];
		this.noOfBoxDragged++;
		return "left";
	} else if (isInsideRectangle(dx, dy, p1, q1, p2, q2)) {
		this.boxPlacement = [this.dropPositionsRight[this.currentBoxRight * 2], this.dropPositionsRight[this.currentBoxRight * 2 + 1]];
		this.noOfBoxDragged++;
		return "right";
	} else {
		// console.log('out');
		return false;
	}

}

/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
School_4_1.prototype.correctFeedback = function() {
	var feedback = localStorage.getItem('school_4_1');
	feedback++;

	localStorage.setItem('school_4_1', feedback);
	this.updateFeedback();
	if (feedback >= School_4_1.totalFeedback) {
		setTimeout(this.onComplete.bind(this), 600);
	} else {
		this.getNewExerciseSet();
	}
}

/**
 * on wrong answer, feedback decrease
 */
School_4_1.prototype.wrongFeedback = function() {
	var feedback = localStorage.getItem('school_4_1');
	feedback--;

	if (feedback < 0)
		feedback = 0;

	localStorage.setItem('school_4_1', feedback);
	this.updateFeedback();

}

/**
 * update feedback by 1
 */
School_4_1.prototype.updateFeedback = function() {

	var self = this;
	addedListeners = false;
	var counter = localStorage.getItem('school_4_1');

	self.showHideChildrenInContainer(self.feedbackContainer, false);

	for (var j = 0; j < counter; j++) {
		self.feedbackContainer.children[j].alpha = 1;
	}
}

School_4_1.prototype.setPool = function() {

	this.pool = {
		"0": {
			"0": ["brandbil", "brandman", "brev", "brevbärare", "brevlåda", "bricka", "bro", "brud", "brun", "brygga", "bräda", "bröd"],
			"1": ["pratbubbla", "prickar", "prins", "prinsessa", "prinskorv", "pris", "prislapp", "propeller", "propp", "präst"]
		},
		"1": {
			"0": ["drag", "dragspel", "drake", "dromedar", "droppe", "drottning", "druva"],
			"1": ["trasa", "tratt", "trea", "tron", "trosa", "trumpet", "tryne", "tråd", "trådrulle", "träd", "tröja"]
		},
		"2": {
			"0": ["gran", "gren", "grill", "grind", "gris", "groda", "grodd", "grop", "grus", "gryn", "gryta", "grå", "gräs", "gräshoppa", "gräsmatta", "grävling", "grön", "gröt"],
			"1": ["krabba", "kran", "krans", "kratta", "kringla", "krok", "krokodil", "krona", "kruka", "krycka", "kråka", "kräfta"]
		}
	}

	this.setsLogic = [
		[1, 3],
		[2, 2],
		[3, 1]
	];

	// cloning array
	this.tempPool = [];

	this.tempPool[0] = this.pool[this.defaultGroup][0].slice(0);
	this.tempPool[1] = this.pool[this.defaultGroup][1].slice(0);

}

/**
 * load exercise json using jsonloader and grab 2 sets
 */
School_4_1.prototype.initObjectPooling = function(defaultGroup) {

	this.defaultGroup = defaultGroup || 0;

	this.wallSlices = [];

	this.setPool();

	this.borrowCount = 2;

	this.generateNewSet();

	this.borrowWallSprites();

	this.runExercise();

}

School_4_1.prototype.generateNewSet = function() {

	this.randomPool = uniqueRandomNumbersInRange(3, 1)[0];

	console.log('tempPool', JSON.stringify(this.tempPool));

	if (this.tempPool[0].length < this.setsLogic[this.randomPool][0] || this.tempPool[1].length < this.setsLogic[this.randomPool][1]) {

		this.tempPool.length = 0;
		this.tempPool[0] = this.pool[this.defaultGroup][0].slice(0);
		this.tempPool[1] = this.pool[this.defaultGroup][1].slice(0);
		console.log('new tempPool', JSON.stringify(this.tempPool))

	}

	this.set = [];

	for (var k = 0; k < this.borrowCount; k++) {

		var tempSet = {
			"word": [],
			"startswith": []
		};

		shuffle(this.tempPool[0]);
		shuffle(this.tempPool[1]);

		console.log(this.tempPool[0].length, this.setsLogic[this.randomPool][0]);
		console.log(this.tempPool[1].length, this.setsLogic[this.randomPool][1]);

		var zeroUniqueRangeArr = uniqueRandomNumbersInRange(this.tempPool[0].length, this.setsLogic[this.randomPool][0]);
		var oneUniqueRangeArr = uniqueRandomNumbersInRange(this.tempPool[1].length, this.setsLogic[this.randomPool][1]);
		console.log(zeroUniqueRangeArr, oneUniqueRangeArr)

		for (var i = 0; i < zeroUniqueRangeArr.length; i++) {
			var tempIndex = zeroUniqueRangeArr[i];
			tempSet.word.push(this.tempPool[0][tempIndex]);
			tempSet.startswith.push('br');
		}

		for (var i = zeroUniqueRangeArr.length - 1; i >= 0; i--) {
			this.tempPool[0].splice(zeroUniqueRangeArr[i], 1);
		}

		for (var i = 0; i < oneUniqueRangeArr.length; i++) {
			var tempIndex = oneUniqueRangeArr[i];
			tempSet.word.push(this.tempPool[1][tempIndex]);
			tempSet.startswith.push('pr');
		}

		for (var i = oneUniqueRangeArr.length - 1; i >= 0; i--) {
			this.tempPool[1].splice(oneUniqueRangeArr[i], 1);
		}

		this.set.push(tempSet);

	}

	console.log(JSON.stringify(this.set));
}


/**
 * borrow objects
 */
School_4_1.prototype.borrowWallSprites = function() {

	var self = this;
	for (var i = 0; i < this.borrowCount; i++) {

		var imageArr = [],
			soundArr = [],
			startWithArray = [];

		var spriteObj = this.set[i];

		for (var j = 0; j < 4; j++) {

			var answerName = getNonUnicodeName(spriteObj.word[j]);

			var poolImageSprite = PIXI.Sprite.fromImage('uploads/images/' + answerName + '.png');

			imageArr.push(poolImageSprite);
			main.texturesToDestroy.push(poolImageSprite);

			var howl = loadSound('uploads/audios/' + answerName);
			soundArr.push(howl);
			startWithArray.push(spriteObj.startswith[j]);
		};

		shuffle(imageArr, soundArr, startWithArray);

		this.wallSlices.push({
			image: imageArr,
			sound: soundArr,
			startsWith: startWithArray
		});
	}
};

/**
 * get next exercise set
 */
School_4_1.prototype.getNewExerciseSet = function() {

	this.exerciseBaseContainer.removeChildren();

	this.currentBoxLeft = 0;
	this.currentBoxRight = 0;
	this.noOfBoxDragged = 0;
	this.itemsInLeftColumn = [];
	this.itemsInRightColumn = [];

	this.wallSlices.shift();

	this.set.length = 0;

	this.borrowCount = 1;

	this.generateNewSet();

	this.borrowWallSprites();

	this.runExercise();
}

/**
 * restart exercise
 * get next set
 */
School_4_1.prototype.restartExercise = function() {

	this.resetExercise();

	this.showHideChildrenInContainer(this.feedbackContainer, false);
	localStorage.setItem('school_4_1', 0);

	this.outroContainer.visible = false;
	this.introContainer.visible = true;

	this.introMode = true;

	this.getNewExerciseSet();
};

/**
 * the outro scene is displayed
 */
School_4_1.prototype.onComplete = function() {
	var self = this;

	this.introMode = false;
	this.introContainer.visible = false;
	this.outroContainer.visible = true;

	this.help.interactive = false;
	this.help.buttonMode = false;

	this.ovaigen.click = this.ovaigen.tap = function(data) {
		self.help.interactive = true;
		self.help.buttonMode = true;
		self.cancelOutro();
		self.restartExercise();
	}

	this.outroAnimation();
}


/**
 * reset exercise to default state
 */
School_4_1.prototype.resetExercise = function() {

	this.ovaigen.visible = false;
	this.outroContainer.visible = false;

	this.introContainer.visible = true;

	this.introMode = true;

};

/**
 *Show hide elements in the container
 */
School_4_1.prototype.showHideChildrenInContainer = function(obj, show) {

	for (var j = 0; j < obj.children.length; j++) {
		obj.children[j].alpha = (show === true) ? 1 : 0.2;
	}
}

/**
 * clean memory by clearing reference of object
 */
School_4_1.prototype.cleanMemory = function() {

	if (this.assetsToLoad)
		this.assetsToLoad.length = 0;

	if (this.sceneBackground)
		this.sceneBackground.clear();
	this.sceneBackground = null;
	this.counter = null;

	if (this.wallSlices)
		this.wallSlices.length = 0;

	if (this.itemsInLeftColumn)
		this.itemsInLeftColumn.length = 0;

	if (this.itemsInRightColumn)
		this.itemsInRightColumn.length = 0;

	this.pool = null;
	this.num = null;

	if (this.jsonData)
		this.jsonData.length = 0;

	this.figure_intro = null;
	this.figure_outro = null;
	this.mouthParam = null;
	this.introContainer = null;
	this.outroContainer = null;
	this.feedbackContainer = null;
	this.currentBoxLeft = null;
	this.currentBoxRight = null;
	this.noOfBoxDragged = null;
	this.noOfCorrectAnswer = null;
	this.defaultGroup = null;
	this.jsonContents = null;
	this.feedBackRect = null;
	this.sceneBackgroundFloor = null;
	this.sceneBackgroundSeparator = null;
	this.brownLetterBox = null;
	this.brownBox1 = null;
	this.brownBox2 = null;
	this.shelf_one = null;
	this.shelf_two = null;

	if (this.groupBoxPositions)
		this.groupBoxPositions.length = 0;

	if (this.alreadyPlayedWords)
		this.alreadyPlayedWords.length = 0;

	this.rightHand = null;
	this.firstShelfLabel = null;
	this.secondShelfLabel = null;
	this.mouthOutro = null;

	if (this.dragImage)
		this.dragImage.length = 0;

	this.whiteGraphics = null;
	this.whiteTexture = null;

	if (this.exerciseParams)
		this.exerciseParams.length = 0;

	this.soundTexture = null;
	this.feedTexture = null;
	this.boxPlacement = null;

	this.sceneContainer.removeChildren();
	this.sceneContainer = null;

}