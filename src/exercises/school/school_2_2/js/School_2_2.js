function School_2_2() {
    cl.show()

    PIXI.DisplayObjectContainer.call(this);

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position.set(77, 88);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();
    this.feedbackContainer = new PIXI.DisplayObjectContainer();
    this.animationContainer = new PIXI.DisplayObjectContainer();
    this.exerciseBaseContainer = new PIXI.DisplayObjectContainer();

    localStorage.setItem('school_2_2', 0);
    main.CurrentExercise = "school_2_2";

    main.eventRunning = false;

    this.introMode = true;

    main.texturesToDestroy = [];

    this.common = new Common();

    this.boxPlacement = [];
    this.currentBox = 0;

    this.loadSpriteSheet();

    this.noOfItemsDragged = 0;
    this.draggedIndex = [];
    this.correctAnswerIndex = [];
    this.wrongAnswerIndex = [];
    this.draggedCorrectIndex = [];
    this.draggedWrongIndex = [];

    // everything connected to this is rendered.
    this.addChild(this.sceneContainer);
}

School_2_2.constructor = School_2_2;
School_2_2.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
School_2_2.imagePath = 'build/exercises/school/school_2_2/images/';
School_2_2.audioPath = 'build/exercises/school/school_2_2/audios/';
School_2_2.totalFeedback = 5;
/**
 * load array of assets of different format
 */
School_2_2.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(School_2_2.audioPath + 'intro');
    this.outroId = loadSound(School_2_2.audioPath + 'outro');

    // create an array of assets to load
    this.assetsToLoad = [
        School_2_2.imagePath + "sprite_school_2_2.json",
        School_2_2.imagePath + "sprite_school_2_2.png",
        School_2_2.imagePath + "dotted_border.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
School_2_2.prototype.spriteSheetLoaded = function() {

    // hide loader
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    // fill exercise background
    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0xcae2c2);
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xec6567);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);

    // exercise instruction
    this.help = PIXI.Sprite.fromFrame("topcorner.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);

    this.loadExercise();
    this.introScene();

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.outroContainer);
    this.introContainer.addChild(this.feedbackContainer);
    this.introContainer.addChild(this.exerciseBaseContainer);
    this.sceneContainer.addChild(this.animationContainer);
}

/**
 * hand visibility
 * @param  {int} number
 */
School_2_2.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    };
    if (isNumeric(number))
        this.hands[number].visible = true;
}

/**
 * play intro animation
 */
School_2_2.prototype.introAnimation = function() {

    createjs.Sound.stop();

    if (!this.introMode)
        this.restartExercise();

    this.help.interactive = false;
    this.help.buttonMode = false;

    main.overlay.visible = true;

    this.arm_rest_outro.visible = false;
    this.introContainer.visible = true;
    this.outroContainer.visible = false;

    this.animationContainer.visible = true;
    this.mouths.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
0-1 sec arm_1
1-5 sec arm_3
5-7 sec arm_1
7-11 sec arm_2
stop at arm_3
 */
School_2_2.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    console.log(currentPostion);
    if (currentPostion >= 0 && currentPostion < 4000) {
        /*  this.showHandAt(0); //cross hand
    } else if (currentPostion >= 1000 && currentPostion < 4000) {*/
        this.showHandAt(1); //short hand 1
    } else if (currentPostion >= 4000 && currentPostion < 6000) {
        this.showHandAt(2); //long hand 1
        /*} else if (currentPostion >= 5500 && currentPostion < 6500) {
            this.showHandAt(0); //cross hand*/
    } else if (currentPostion >= 6000 && currentPostion < 7500) {
        this.showHandAt(1); //short hand 1
    } else if (currentPostion >= 7500 && currentPostion < 10843) {
        this.showHandAt(2); //long hand 1
    }

};

/**
 * cancel running intro animation
 */
School_2_2.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);

    this.mouths.hide();
    this.showHandAt(0);

    var self = this;
    setTimeout(function() {
        self.animationContainer.visible = false;
        self.help.interactive = true;
        self.help.buttonMode = true;

        self.ovaigen.visible = false;
        main.overlay.visible = false;
    }, 1000);



}

/**
 * play outro animation
 */
School_2_2.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.outroStars.show();
        self.showHandAt();
        self.animationContainer.visible = true;
        self.arm_rest_outro.visible = true;
        self.mouths.show();
    }
    this.outroCancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.outroCancelHandler);
}

/**
 * cancel running outro animation
 */
School_2_2.prototype.cancelOutro = function() {

    var self = this;

    createjs.Sound.stop();
    self.mouths.hide(0);
    self.ovaigen.visible = true;
    self.help.interactive = true;
    self.help.buttonMode = true;
    main.overlay.visible = false;

};

/**
 * create sprites,animation,sound related to intro scene
 */
School_2_2.prototype.introScene = function() {

    var self = this;


    this.sceneContainer.addChild(this.help);

    this.figure_intro = new PIXI.Sprite.fromFrame('girl-body.png');
    this.figure_intro.position.set(3, 269);
    main.texturesToDestroy.push(this.figure_intro);
    this.animationContainer.addChild(this.figure_intro);

    this.mouths = new Animation({
        "image": "mouth",
        "length": 3,
        "x": 146,
        "y": 520,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 2, 3, 1],
        "frameId": 0
    });

    this.animationContainer.addChild(this.mouths);


    this.hands = [
        new PIXI.Sprite.fromFrame('arm3.png'),
        new PIXI.Sprite.fromFrame('arm1.png'),
        new PIXI.Sprite.fromFrame('arm2.png'),
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(-7, 471);
        this.animationContainer.addChild(this.hands[i]);
        this.hands[i].visible = false;
    };
    this.hands[2].visible = true;
    this.animationContainer.visible = false;

    this.outroScene();
    this.createFeedBack();
}

/**
 * create sprites,animation,sound related to outro scene
 */
School_2_2.prototype.outroScene = function() {

    var self = this;

    this.outroContainer.visible = false;

    this.arm_rest_outro = new PIXI.Sprite.fromFrame("arm_rest.png");
    this.arm_rest_outro.position = {
        x: -7,
        y: 626
    };
    this.arm_rest_outro.visible = false;
    this.animationContainer.addChild(this.arm_rest_outro);

    //add stars in outro
    this.outroStars = new Animation({
        "image": "stars",
        "length": 5,
        "x": 350,
        "y": 200,
        "speed": 8,
        "fromImage": true,
        "imageFolder": "build/exercises/school/school_2_2/images/",
        "pattern": false,
        "frameId": 0
    });

    this.outroContainer.addChild(this.outroStars);


    // back to intro page
    this.ovaigen = PIXI.Sprite.fromFrame('ovaigen.png');
    this.ovaigen.position.set(910, 471);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.ovaigen.visible = false;
    this.outroContainer.addChild(this.ovaigen);
}

/**
 * create feedback sprite
 */
School_2_2.prototype.createFeedBack = function() {
    var self = this;
    var feedback = new PIXI.Texture.fromFrame("star.png");
    for (var k = 0; k < 5; k++) {
        var feedbackSprite = new PIXI.Sprite(feedback);

        feedbackSprite.position = {
            x: 1669,
            y: 837 - (k * 158)
        };
        main.texturesToDestroy.push(feedbackSprite);
        self.feedbackContainer.addChild(feedbackSprite);
        feedbackSprite.alpha = 0.2;
    }
};

/**
 * load exercise related stuffs
 */
School_2_2.prototype.loadExercise = function() {



    //ADD WHITE BOARD AND ITS RACK
    this.whiteBoard = createRect({
        w: 1424,
        h: 893,
        color: 0xffffff,
        lineStyle: 1,
        lineWidth: 10,
        lineColor: 0x624E42
    });



    this.whiteBoard.position = {
        x: 188,
        y: 136
    };

    this.sceneContainer.addChild(this.whiteBoard);

    //now add board's rack

    this.board_rack = new PIXI.Sprite.fromFrame("board-rack.png");
    this.board_rack.position = {
        x: 83,
        y: 1016
    };
    this.sceneContainer.addChild(this.board_rack);

    //now add markers
    this.blue_marker = new PIXI.Sprite.fromFrame("marker-blue.png");
    this.blue_marker.position = {
        x: 1185,
        y: 1015
    };
    this.sceneContainer.addChild(this.blue_marker);

    this.green_marker = new PIXI.Sprite.fromFrame("marker-green.png");
    this.green_marker.position = {
        x: 912,
        y: 1028
    };
    this.sceneContainer.addChild(this.green_marker);

    //NOW ADD TWO BOXES IN THE WHITE BOARD PINK AND LIGHT PURPLE BOX SO THAT THEY CAN CONTAIN EXERCISE STUFFS
    this.dottedBorder = new PIXI.Texture.fromImage(School_2_2.imagePath + "dotted_border.png");
    var boxColors = [0xf9cac2, 0xd6def2];
    var boxPositions = [225, 930];

    for (var k = 0; k < 2; k++) {
        var boxSprite = createRect({
            w: 642,
            h: 811,
            color: boxColors[k],
            lineStyle: 0
        });
        boxSprite.position = {
            x: boxPositions[k],
            y: 172
        };
        var borderSprite = new PIXI.Sprite(this.dottedBorder);
        boxSprite.addChild(borderSprite);
        this.exerciseBaseContainer.addChild(boxSprite);
    }

    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.exerciseBaseContainer.addChild(this.exerciseContainer);

    this.greenRect = createRect({
        w: 270,
        h: 80,
        color: 0x00ff00,
        alpha: 0.6,
        lineStyle: 0
    });
    this.redRect = createRect({
        w: 270,
        h: 80,
        color: 0xff0000,
        alpha: 0.6,
        lineStyle: 0
    });
    this.greenRect.visible = false;
    this.redRect.visible = false;

    this.initObjectPooling();
}

/**
 * manage exercise with new sets randomized
 */
School_2_2.prototype.runExercise = function() {

    var self = this;
    this.dragImage = [];
    this.dropPosition = [];
    this.dropArea = [];

    this.dropZone = [0, 0, 0, 0, 0, 0, 0, 0];
    this.itemsDragged = [-1, -1, -1, -1, -1, -1, -1, -1];



    this.jsonData = this.wallSlices[0];

    //stuffs for short words start
    this.whiteShortGraphics = createRect({
        w: 148,
        h: 80,
        color: 0xffffff,
        lineStyle: 1
    });
    this.whiteShortTexture = this.whiteShortGraphics.generateTexture();
    var shortBoxPositions = [
        572, 197,
        268, 285,
        572, 378,
        268, 468,
        572, 568,
        268, 673,
        572, 767,
        268, 878
    ];

    this.longBoxPositions = [
        1255, 197,
        958, 294,
        1255, 392,
        958, 491,
        1255, 587,
        958, 684,
        1255, 783,
        958, 883

    ];

    for (var k = 0; k < 8; k++) {
        this.dropArea.push(false);
        var shortWhiteBox = new PIXI.Sprite(this.whiteShortTexture);
        shortWhiteBox.position = {
            x: shortBoxPositions[k * 2],
            y: shortBoxPositions[k * 2 + 1]
        };
        shortWhiteBox.interactive = true;
        shortWhiteBox.buttonMode = true;

        shortWhiteBox.shortText = new PIXI.Text("" + this.jsonData.short_word[k], {
            'font': '45px Arial',
            'fill': 'black'
        });
        shortWhiteBox.shortText.position = {
            x: 30,
            y: (shortWhiteBox.height - shortWhiteBox.shortText.height) / 2
        };

        shortWhiteBox.addChild(shortWhiteBox.shortText);

        shortWhiteBox.oldPosition = {
            x: shortBoxPositions[k * 2],
            y: shortBoxPositions[k * 2 + 1]
        };
        shortWhiteBox.itemIndex = k;
        shortWhiteBox.selectedShortWord = this.jsonData.short_word[k];

        shortWhiteBox.dropPosition = this.longBoxPositions;
        this.exerciseContainer.addChild(shortWhiteBox);
        this.dragImage.push(shortWhiteBox);
    }
    //stuffs for short words end

    //stuffs for long words start
    this.whiteLongGraphics = createRect({
        w: 270,
        h: 80,
        color: 0xffffff,
        lineStyle: 1
    });
    this.whiteLongTexture = this.whiteLongGraphics.generateTexture();

    this.textLongChange = [];
    for (var k = 0; k < 8; k++) {
        var longWhiteBox = new PIXI.Sprite(this.whiteLongTexture);
        longWhiteBox.position = {
            x: this.longBoxPositions[k * 2],
            y: this.longBoxPositions[k * 2 + 1]
        };

        var longText = new PIXI.Text("" + this.jsonData.long_word[k], {
            'font': '45px Arial',
            'fill': 'black'
        });


        longText.position = {
            x: 30,
            y: (longWhiteBox.height - longText.height) / 2
        };
        this.textLongChange.push(longWhiteBox);
        longWhiteBox.addChild(longText);
        this.exerciseContainer.addChild(longWhiteBox);

    }
    //stuffs for long words end


    for (var i = 0; i < this.dragImage.length; i++) {
        this.onDragging(i);
    };

    this.exerciseContainer.addChild(this.greenRect);
    this.exerciseContainer.addChild(this.redRect);
    // this.greenRect.visible = false;
    // this.redRect.visible = false;
}

/**
 * drag drop sprite
 * on correct drop, feedback is updated
 */
School_2_2.prototype.onDragging = function(i) {

    var self = this;

    // use the mousedown and touchstart
    this.dragImage[i].mousedown = this.dragImage[i].touchstart = function(data) {
        if (addedListeners) return false;
        if (main.eventRunning) return false;
        data.originalEvent.preventDefault()
            // store a refference to the data
            // The reason for this is because of multitouch
            // we want to track the movement of this particular touch
        this.data = data;
        this.alpha = 0.9;
        this.dragging = true;
        this.sx = this.data.getLocalPosition(this).x;
        this.sy = this.data.getLocalPosition(this).y;
        this.bringToFront();
        for (var j = 0; j < self.itemsDragged.length; j++) {
            var idx = self.itemsDragged.indexOf(this.itemIndex);
            if (idx > -1) {
                self.dropZone[idx] = 0;
                self.revertLongText(self.jsonData.long_word[idx], idx);
                self.itemsDragged[idx] = -1;
            }
        }

    }

    this.dragImage[i].mouseup = this.dragImage[i].mouseupoutside = this.dragImage[i].touchend = this.dragImage[i].touchendoutside = function(data) {
        if (addedListeners) return false;
        if (main.eventRunning) return false;
        var dragSelf = this;

        this.alpha = 1
        this.dragging = false;
        // set the interaction data to null
        this.data = null;

        var dropArea = [];

        for (var j = 0; j < self.itemsDragged.length; j++) {
            var idx = self.itemsDragged.indexOf(this.itemIndex);
            if (idx > -1) {
                self.dropZone[idx] = 0;
                self.revertLongText(self.jsonData.long_word[idx], idx);
                self.itemsDragged[idx] = -1;
            }
        }

        for (var k = 0; k < 8; k++) {

            if (self.dropZone[k] == 1) {
                dropArea[k] = false;
            } else {
                dropArea[k] = isInsideRectangle(this.dx, this.dy, this.dropPosition[k * 2], this.dropPosition[k * 2 + 1], this.dropPosition[k * 2] + 270, this.dropPosition[k * 2 + 1] + 80);
            }
            if (dropArea[k]) {
                self.draggedIndex.push(k);
                self.dropZone[k] = 1;
                self.itemsDragged[k] = this.itemIndex;
            } else {
                self.draggedIndex = self.arraySplice(self.draggedIndex, k);

            }

        }

        if (dropArea[0] || dropArea[1] || dropArea[2] || dropArea[3] || dropArea[4] || dropArea[5] || dropArea[6] || dropArea[7]) {

            if (dropArea[0]) {
                this.position.x = this.dropPosition[0];
                this.position.y = this.dropPosition[1];
                self.meltWords(self.jsonData.long_word[0], self.dragImage[this.itemIndex], 0);
                if (this.selectedShortWord === self.jsonData.answer[0]) {
                    self.correctAnswerIndex.push(0);
                    self.correctAnswerIndex = self.correctAnswerIndex.removeDuplicates();
                    self.wrongAnswerIndex = self.arraySplice(self.wrongAnswerIndex, 0);

                    self.draggedCorrectIndex.push(this.itemIndex);
                    self.draggedCorrectIndex = self.draggedCorrectIndex.removeDuplicates();
                    self.draggedWrongIndex = self.arraySplice(self.draggedWrongIndex, this.itemIndex);
                } else {
                    self.wrongAnswerIndex.push(0);
                    self.wrongAnswerIndex = self.wrongAnswerIndex.removeDuplicates();
                    self.correctAnswerIndex = self.arraySplice(self.correctAnswerIndex, 0);

                    self.draggedWrongIndex.push(this.itemIndex);
                    self.draggedWrongIndex = self.draggedWrongIndex.removeDuplicates();
                    self.draggedCorrectIndex = self.arraySplice(self.draggedCorrectIndex, this.itemIndex);
                }

            }

            if (dropArea[1]) {
                this.position.x = this.dropPosition[2];
                this.position.y = this.dropPosition[3];
                self.meltWords(self.jsonData.long_word[1], self.dragImage[this.itemIndex], 1);

                if (this.selectedShortWord === self.jsonData.answer[1]) {
                    self.correctAnswerIndex.push(1);
                    self.correctAnswerIndex = self.correctAnswerIndex.removeDuplicates();
                    self.wrongAnswerIndex = self.arraySplice(self.wrongAnswerIndex, 1);

                    self.draggedCorrectIndex.push(this.itemIndex);
                    self.draggedCorrectIndex = self.draggedCorrectIndex.removeDuplicates();
                    self.draggedWrongIndex = self.arraySplice(self.draggedWrongIndex, this.itemIndex);
                } else {
                    self.wrongAnswerIndex.push(1);
                    self.wrongAnswerIndex = self.wrongAnswerIndex.removeDuplicates();
                    self.correctAnswerIndex = self.arraySplice(self.correctAnswerIndex, 1);

                    self.draggedWrongIndex.push(this.itemIndex);
                    self.draggedWrongIndex = self.draggedWrongIndex.removeDuplicates();
                    self.draggedCorrectIndex = self.arraySplice(self.draggedCorrectIndex, this.itemIndex);
                }

            }

            if (dropArea[2]) {
                this.position.x = this.dropPosition[4];
                this.position.y = this.dropPosition[5];
                self.meltWords(self.jsonData.long_word[2], self.dragImage[this.itemIndex], 2);

                if (this.selectedShortWord === self.jsonData.answer[2]) {
                    self.correctAnswerIndex.push(2);
                    self.correctAnswerIndex = self.correctAnswerIndex.removeDuplicates();
                    self.wrongAnswerIndex = self.arraySplice(self.wrongAnswerIndex, 2);

                    self.draggedCorrectIndex.push(this.itemIndex);
                    self.draggedCorrectIndex = self.draggedCorrectIndex.removeDuplicates();
                    self.draggedWrongIndex = self.arraySplice(self.draggedWrongIndex, this.itemIndex);
                } else {
                    self.wrongAnswerIndex.push(2);
                    self.wrongAnswerIndex = self.wrongAnswerIndex.removeDuplicates();
                    self.correctAnswerIndex = self.arraySplice(self.correctAnswerIndex, 2);

                    self.draggedWrongIndex.push(this.itemIndex);
                    self.draggedWrongIndex = self.draggedWrongIndex.removeDuplicates();
                    self.draggedCorrectIndex = self.arraySplice(self.draggedCorrectIndex, this.itemIndex);
                }

            }

            if (dropArea[3]) {
                this.position.x = this.dropPosition[6];
                this.position.y = this.dropPosition[7];
                self.meltWords(self.jsonData.long_word[3], self.dragImage[this.itemIndex], 3);

                if (this.selectedShortWord === self.jsonData.answer[3]) {
                    self.correctAnswerIndex.push(3);
                    self.correctAnswerIndex = self.correctAnswerIndex.removeDuplicates();
                    self.wrongAnswerIndex = self.arraySplice(self.wrongAnswerIndex, 3);

                    self.draggedCorrectIndex.push(this.itemIndex);
                    self.draggedCorrectIndex = self.draggedCorrectIndex.removeDuplicates();
                    self.draggedWrongIndex = self.arraySplice(self.draggedWrongIndex, this.itemIndex);
                } else {
                    self.wrongAnswerIndex.push(3);
                    self.wrongAnswerIndex = self.wrongAnswerIndex.removeDuplicates();
                    self.correctAnswerIndex = self.arraySplice(self.correctAnswerIndex, 3);

                    self.draggedWrongIndex.push(this.itemIndex);
                    self.draggedWrongIndex = self.draggedWrongIndex.removeDuplicates();
                    self.draggedCorrectIndex = self.arraySplice(self.draggedCorrectIndex, this.itemIndex);
                }

            }

            if (dropArea[4]) {
                this.position.x = this.dropPosition[8];
                this.position.y = this.dropPosition[9];
                self.meltWords(self.jsonData.long_word[4], self.dragImage[this.itemIndex], 4);

                if (this.selectedShortWord === self.jsonData.answer[4]) {
                    self.correctAnswerIndex.push(4);
                    self.correctAnswerIndex = self.correctAnswerIndex.removeDuplicates();
                    self.wrongAnswerIndex = self.arraySplice(self.wrongAnswerIndex, 4);

                    self.draggedCorrectIndex.push(this.itemIndex);
                    self.draggedCorrectIndex = self.draggedCorrectIndex.removeDuplicates();
                    self.draggedWrongIndex = self.arraySplice(self.draggedWrongIndex, this.itemIndex);
                } else {
                    self.wrongAnswerIndex.push(4);
                    self.wrongAnswerIndex = self.wrongAnswerIndex.removeDuplicates();
                    self.correctAnswerIndex = self.arraySplice(self.correctAnswerIndex, 4);

                    self.draggedWrongIndex.push(this.itemIndex);
                    self.draggedWrongIndex = self.draggedWrongIndex.removeDuplicates();
                    self.draggedCorrectIndex = self.arraySplice(self.draggedCorrectIndex, this.itemIndex);
                }

            }

            if (dropArea[5]) {
                this.position.x = this.dropPosition[10];
                this.position.y = this.dropPosition[11];
                self.meltWords(self.jsonData.long_word[5], self.dragImage[this.itemIndex], 5);

                if (this.selectedShortWord === self.jsonData.answer[5]) {
                    self.correctAnswerIndex.push(5);
                    self.correctAnswerIndex = self.correctAnswerIndex.removeDuplicates();
                    self.wrongAnswerIndex = self.arraySplice(self.wrongAnswerIndex, 5);

                    self.draggedCorrectIndex.push(this.itemIndex);
                    self.draggedCorrectIndex = self.draggedCorrectIndex.removeDuplicates();
                    self.draggedWrongIndex = self.arraySplice(self.draggedWrongIndex, this.itemIndex);
                } else {
                    self.wrongAnswerIndex.push(5);
                    self.wrongAnswerIndex = self.wrongAnswerIndex.removeDuplicates();
                    self.correctAnswerIndex = self.arraySplice(self.correctAnswerIndex, 5);

                    self.draggedWrongIndex.push(this.itemIndex);
                    self.draggedWrongIndex = self.draggedWrongIndex.removeDuplicates();
                    self.draggedCorrectIndex = self.arraySplice(self.draggedCorrectIndex, this.itemIndex);
                }

            }

            if (dropArea[6]) {
                this.position.x = this.dropPosition[12];
                this.position.y = this.dropPosition[13];
                self.meltWords(self.jsonData.long_word[6], self.dragImage[this.itemIndex], 6);

                if (this.selectedShortWord === self.jsonData.answer[6]) {
                    self.correctAnswerIndex.push(6);
                    self.correctAnswerIndex = self.correctAnswerIndex.removeDuplicates();
                    self.wrongAnswerIndex = self.arraySplice(self.wrongAnswerIndex, 6);

                    self.draggedCorrectIndex.push(this.itemIndex);
                    self.draggedCorrectIndex = self.draggedCorrectIndex.removeDuplicates();
                    self.draggedWrongIndex = self.arraySplice(self.draggedWrongIndex, this.itemIndex);
                } else {
                    self.wrongAnswerIndex.push(6);
                    self.wrongAnswerIndex = self.wrongAnswerIndex.removeDuplicates();
                    self.correctAnswerIndex = self.arraySplice(self.correctAnswerIndex, 6);

                    self.draggedWrongIndex.push(this.itemIndex);
                    self.draggedWrongIndex = self.draggedWrongIndex.removeDuplicates();
                    self.draggedCorrectIndex = self.arraySplice(self.draggedCorrectIndex, this.itemIndex);
                }

            }

            if (dropArea[7]) {
                this.position.x = this.dropPosition[14];
                this.position.y = this.dropPosition[15];
                self.meltWords(self.jsonData.long_word[7], self.dragImage[this.itemIndex], 7);

                if (this.selectedShortWord === self.jsonData.answer[7]) {
                    self.correctAnswerIndex.push(7);
                    self.correctAnswerIndex = self.correctAnswerIndex.removeDuplicates();
                    self.wrongAnswerIndex = self.arraySplice(self.wrongAnswerIndex, 7);

                    self.draggedCorrectIndex.push(this.itemIndex);
                    self.draggedCorrectIndex = self.draggedCorrectIndex.removeDuplicates();
                    self.draggedWrongIndex = self.arraySplice(self.draggedWrongIndex, this.itemIndex);
                } else {
                    self.wrongAnswerIndex.push(7);
                    self.wrongAnswerIndex = self.wrongAnswerIndex.removeDuplicates();
                    self.correctAnswerIndex = self.arraySplice(self.correctAnswerIndex, 7);

                    self.draggedWrongIndex.push(this.itemIndex);
                    self.draggedWrongIndex = self.draggedWrongIndex.removeDuplicates();
                    self.draggedCorrectIndex = self.arraySplice(self.draggedCorrectIndex, this.itemIndex);
                }

            }

            /*console.log("WRONG ANSWER INDEX = " + self.wrongAnswerIndex);
            console.log("CORRECT ANSWER INDEX = " + self.correctAnswerIndex);
            console.log("DRAGGED CORRECT ANSWER INDEX = " + self.draggedCorrectIndex);
            console.log("DRAGGED WRONG ANSWER INDEX = " + self.draggedWrongIndex);*/

            var totalDragged = self.draggedCorrectIndex.length + self.draggedWrongIndex.length;
            if (totalDragged == 8) { //user has already dragged 8 images so evaluate the answer
                // this.interactive = false;


                var a = setTimeout(function() {
                    if (self.draggedCorrectIndex.length == 8) {
                        correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                    } else {
                        wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                    }

                    for (var t = 0; t < self.correctAnswerIndex.length; t++) {
                        var idx = self.correctAnswerIndex[t];
                        self.showRedGreenBox(dragSelf.dropPosition[idx * 2], dragSelf.dropPosition[idx * 2 + 1], self.greenRect);
                        //self.meltWords(self.jsonData.long_word[idx], self.dragImage[self.draggedCorrectIndex[t]], idx);
                    }


                    for (var q = 0; q < self.wrongAnswerIndex.length; q++) {
                        var idx = self.wrongAnswerIndex[q];
                        self.showRedGreenBox(dragSelf.dropPosition[idx * 2], dragSelf.dropPosition[idx * 2 + 1], self.redRect);
                    }
                }, 1500);


                var b = setTimeout(function() {
                    if (self.draggedCorrectIndex.length >= 8) {
                        //update feedback
                        self.draggedIndex = [];
                        self.draggedCorrectIndex = [];
                        self.draggedWrongIndex = [];
                        self.correctAnswerIndex = [];
                        self.correctFeedback();
                    }

                    if (self.draggedWrongIndex.length > 0) {
                        for (var q = 0; q < self.draggedWrongIndex.length; q++) {
                            var idb = self.draggedWrongIndex[q];

                            self.dragImage[idb].shortText.setStyle({
                                font: "45px Arial"
                            });


                            self.dragImage[idb].position.x = self.dragImage[idb].oldPosition.x;
                            self.dragImage[idb].position.y = self.dragImage[idb].oldPosition.y;
                            self.dragImage[idb].alpha = 1;

                            var idcheck = self.itemsDragged.indexOf(idb);
                            if (idcheck > -1) {
                                self.itemsDragged[idcheck] = -1;
                                self.dropZone[idcheck] = 0;
                                self.revertLongText(self.jsonData.long_word[idcheck], idcheck);
                            }

                            //self.draggedIndex.splice(idb, 1);
                        }

                        self.draggedWrongIndex = [];
                        self.wrongAnswerIndex = [];
                    }

                }, 3000);

            }

        } else {
            this.shortText.setStyle({
                font: "45px Arial"
            });
            self.draggedCorrectIndex = self.arraySplice(self.draggedCorrectIndex, this.itemIndex);
            self.draggedWrongIndex = self.arraySplice(self.draggedWrongIndex, this.itemIndex);

            var idx = self.itemsDragged.indexOf(this.itemIndex);
            if (idx > -1) {
                self.dropZone.splice(idx, 1);
                self.itemsDragged.splice(idx, 1);
            }
            /* self.dropZone[this.itemIndex] = 0;
             self.itemsDragged[this.itemIndex] = -1;*/

            // reset dragged element position
            this.position.x = this.oldPosition.x;
            this.position.y = this.oldPosition.y;
        }

        /*console.log("DROP ZONE = " + self.dropZone);
        console.log("ITEMS DRAGGED = " + self.itemsDragged);
        console.log("===================================");
        console.log("CORRECT ANSWER INDEX = " + self.correctAnswerIndex);
        console.log("WRONG ANSWER INDEX = " + self.wrongAnswerIndex);
        console.log("-----------------------------------");
        console.log("DRAGGED CORRECT INDEX = " + self.draggedCorrectIndex);
        console.log("DRAGGED WRONG INDEX = " + self.draggedWrongIndex);*/


    }

    // set the callbacks for when the mouse or a touch moves
    this.dragImage[i].mousemove = this.dragImage[i].touchmove = function(data) {
        if (addedListeners) return false;
        if (main.eventRunning) return false;
        // set the callbacks for when the mouse or a touch moves
        if (this.dragging) {
            // need to get parent coords..
            var newPosition = this.data.getLocalPosition(this.parent);
            this.position.x = newPosition.x - this.sx;
            this.position.y = newPosition.y - this.sy;
            this.dx = newPosition.x;
            this.dy = newPosition.y;
        }
    };

}

/**
 * array splice
 * @param  {array} array
 * @param  {int} value
 */
School_2_2.prototype.arraySplice = function(array, value) {

    var index = array.indexOf(value);
    if (index > -1) {
        array.splice(index, 1);
    }
    return array;
}

/**
 * show red/green sprite on answer correct/wrong
 * @param  {int} xPos
 * @param  {int} yPos
 * @param  {object} obj
 */
School_2_2.prototype.showRedGreenBox = function(xPos, yPos, obj) {
    var self = this;
    this.feedTexture = obj.generateTexture();
    var ft = new PIXI.Sprite(this.feedTexture);
    this.sceneContainer.addChild(ft);
    ft.position = {
        x: xPos,
        y: yPos
    };
    ft.alpha = 0.6;
    ft.visible = true;

    setTimeout(function() {
        ft.visible = false;
    }, 400);

}

/**
 * check in array
 * @param  {int} needle
 * @param  {array} haystack
 */
School_2_2.prototype.inArray = function inArray(needle, haystack) {
    var length = haystack.length;
    for (var i = 0; i < length; i++) {
        if (haystack[i] == needle) return true;
    }
    return false;
};

/**
 * rever long text
 * @param  {string} word
 * @param  {int} idx
 */
School_2_2.prototype.revertLongText = function(word, idx) {
    var longword = new PIXI.Text("" + word, {
        font: "45px Arial"
    });

    longword.position = {
        x: 30,
        y: (80 - longword.height) / 2 + 10
    };

    for (var i = 0; i < this.textLongChange[idx].children.length; i++) {
        this.textLongChange[idx].removeChild(this.textLongChange[idx].children[i]);
    };
    this.textLongChange[idx].removeChildAt(0);
    this.textLongChange[idx].addChild(longword);

};

/**
 * melt words when dragged
 * @param  {array} longword
 * @param  {object} obj
 * @param  {int} index
 */
School_2_2.prototype.meltWords = function(longword, obj, index) {
    var alphabetsFirst = new PIXI.Text("" + obj.selectedShortWord, {
        'font': 'bold 45px Arial',
        'fill': 'black'
    });

    var lastpartChunk = longword.substring(obj.selectedShortWord.length, longword.length);
    var alphabetsSecond = new PIXI.Text("" + lastpartChunk, {
        'font': '45px Arial',
        'fill': 'black'
    });

    var combinedWidth = parseInt(alphabetsFirst.width) + parseInt(alphabetsSecond.width);
    var xpos = (270 - combinedWidth) / 2;

    alphabetsFirst.position = {
        x: 30,
        y: (80 - alphabetsFirst.height) / 2 + 10 - 2
    };

    alphabetsSecond.position = {
        x: 30 + parseInt(alphabetsFirst.width),
        y: (80 - alphabetsFirst.height) / 2 + 10
    };
    this.textLongChange[index].removeChildAt(0);
    this.textLongChange[index].addChild(alphabetsFirst);
    this.textLongChange[index].addChild(alphabetsSecond);
    obj.alpha = 0;

};

/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
School_2_2.prototype.correctFeedback = function() {
    var feedback = localStorage.getItem('school_2_2');
    feedback++;

    localStorage.setItem('school_2_2', feedback);
    this.updateFeedback();
    if (feedback >= School_2_2.totalFeedback) {
        setTimeout(this.onComplete.bind(this), 1000);
    } else {
        this.getNewExerciseSet();
    }
}


/**
 * update feedback
 */
School_2_2.prototype.updateFeedback = function() {

    var self = this;
    main.eventRunning = false;
    var counter = localStorage.getItem('school_2_2');

    for (var j = 0; j < self.feedbackContainer.children.length; j++) {
        self.feedbackContainer.children[j].alpha = 0.2;
    }

    for (var j = 0; j < counter; j++) {
        self.feedbackContainer.children[j].alpha = 1;
    }
}

/**
 * prefetch exercise data and cache number of sets defined only.
 */
School_2_2.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/school/school_2_2.json?nocache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        self.pool = new ExerciseSpritesPool(loader.json);
        //borrow two sprites once
        self.borrowWallSprites(2);
        self.runExercise();

    });
    loader.load();
}

/**
 * borrow and prelaod audios and sprites
 * @param  {int} num
 */
School_2_2.prototype.borrowWallSprites = function(num) {

    var self = this;

    for (var i = 0; i < num; i++) {

        var sprite = this.pool.borrowSprite(),
            spriteObj = sprite[0],
            shortWordArr = [],
            answerArr = [],
            longWordArr = [];

        this.poolSlices.push(sprite);

        for (var j = 0; j < 8; j++) {
            answerArr.push(spriteObj.short_word[j]);
            shortWordArr.push(spriteObj.short_word[j]);
            longWordArr.push(spriteObj.long_word[j]);
        };

        shuffle(answerArr, longWordArr);

        this.wallSlices.push({
            short_word: shortWordArr,
            long_word: longWordArr,
            answer: answerArr
        });
    }
};

/**
 * return objects too pool
 */
School_2_2.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];

    this.pool.returnSprite(sprite);

    this.poolSlices = [];
    this.wallSlices.shift();
};

/**
 * get new exercise sets
 */
School_2_2.prototype.getNewExerciseSet = function() {
    this.exerciseContainer.removeChildren();

    this.returnWallSprites();
    this.borrowWallSprites(1);
    this.runExercise();
}

/**
 * reset exercise and get new set
 */
School_2_2.prototype.restartExercise = function() {

    this.resetExercise();

    for (var i = 0; i < this.feedbackContainer.children.length; i++) {
        this.feedbackContainer.children[i].alpha = 0.2;
    };

    localStorage.setItem('school_2_2', 0);
    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    this.introMode = true;

    this.getNewExerciseSet();

};

/**
 * the outro scene is displayed
 */
School_2_2.prototype.onComplete = function() {

    var self = this;
    this.introMode = false;

    this.help.interactive = false;
    this.help.buttonMode = false;
    this.introContainer.visible = false;
    this.outroContainer.visible = true;
    this.animationContainer.visible = true;

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        self.animationContainer.visible = false;
        self.cancelOutro();
        self.restartExercise();
    }

    this.outroAnimation();

}


/**
 * reset exercise to default values
 */
School_2_2.prototype.resetExercise = function() {

    this.ovaigen.visible = false;
    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    this.introMode = true;
};

/**
 * clean memory by clearing reference of object
 */
School_2_2.prototype.cleanMemory = function() {
    this.introMode = null;

    if (this.sceneBackground)
        this.sceneBackground.clear();
    this.sceneBackground = null;

    if (this.assetsToLoads)
        this.assetsToLoad.length = 0;


    this.help = null;


    this.ovaigen = null;

    if (this.jsonData)
        this.jsonData.length = 0;

    this.counter = null;

    if (this.wallSlices)
        this.wallSlices.length = 0;

    if (this.poolSlices)
        this.poolSlices.length = 0;

    this.pool = null;

    this.introContainer = null;
    this.outroContainer = null;
    this.feedbackContainer = null;
    this.animationContainer = null;

    if (this.boxPlacement) this.boxPlacement.length = 0;
    this.currentBox = null;
    this.noOfItemsDragged = null;
    if (this.draggedIndex) this.draggedIndex.length = 0;
    if (this.correctAnswerIndex) this.correctAnswerIndex.length = 0;
    if (this.wrongAnswerIndex) this.wrongAnswerIndex.length = 0;
    if (this.draggedCorrectIndex) this.draggedCorrectIndex.length = 0;
    if (this.draggedWrongIndex) this.draggedWrongIndex.length = 0;

    this.common = null;


    this.exerciseContainer = null;

    this.outroId = null;

    this.introId = null;

    this.figure_intro = null;
    this.mouths = null;
    if (this.hands) this.hands.length = 0;

    this.whiteBoard = null;
    this.board_rack = null;
    this.blue_marker = null;
    this.green_marker = null;
    this.dottedBorder = null;

    this.greenRect = null;
    this.redRect = null;
    this.whiteShortGraphics = null;
    this.whiteShortTexture = null;

    if (this.longBoxPositions) this.longBoxPositions.length = 0;


    if (this.dragImage) this.dragImage.length = 0;
    if (this.dropPosition) this.dropPosition.length = 0;
    if (this.dropZone) this.dropZone.length = 0;

    this.arm_rest_outro = null;
    this.outroStars = null;



    this.sceneContainer.removeChildren();
    this.sceneContainer = null;
}