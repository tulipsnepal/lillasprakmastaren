function School_2_1() {
    cl.show()

    PIXI.DisplayObjectContainer.call(this);

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position.set(77, 88);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();

    localStorage.setItem('school_2_1', 0);
    main.CurrentExercise = "school_2_1";


    main.texturesToDestroy = [];

    this.common = new Common();

    this.loadSpriteSheet();

    // everything connected to this is rendered.
    this.addChild(this.sceneContainer);
}

School_2_1.constructor = School_2_1;
School_2_1.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
School_2_1.imagePath = 'build/exercises/school/school_2_1/images/';
School_2_1.audioPath = 'build/exercises/school/school_2_1/audios/';
School_2_1.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

/**
 * load array of assets of different format
 */
School_2_1.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(School_2_1.audioPath + 'intro');
    this.outroId = loadSound(School_2_1.audioPath + 'outro');

    // create an array of assets to load
    this.assetsToLoad = [
        School_2_1.imagePath + "sprite_2_1.json",
        School_2_1.imagePath + "sprite_2_1.png",
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
School_2_1.prototype.spriteSheetLoaded = function() {

    // hide loader
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    // fill exercise background
    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0xcbe3c2);
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xf4cde2);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);

    // exercise instruction
    this.help = PIXI.Sprite.fromFrame("topcorner.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);
    this.sceneContainer.addChild(this.help);

    this.loadExercise();
    this.introScene();

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.outroContainer);
}

/**
 * reset scene
 */
School_2_1.prototype.resetScene = function() {

    for (var i = 0; i < this.hiddenFeedback.length; i++) {
        this.hiddenFeedback[i].alpha = 0.4;
    };
    this.outroContainer.visible = false;

    this.exerciseBaseContainer.visible = true;
    this.exerciseContainer.visible = true;
    this.replaceContainer.visible = true;

    this.figure_head2.visible = false;
    this.figure_head1.visible = true;
}

/**
 * play intro animation
 */
School_2_1.prototype.introAnimation = function() {

    createjs.Sound.stop();

    if (!this.introMode)
        this.restartExercise();

    this.help.interactive = false;
    this.help.buttonMode = false;

    main.overlay.visible = true;

    this.figure_head1.visible = false;
    this.figure_head2.visible = true;
    this.mouths.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
0-2 sec arm_0
2-7 sec arm_1
7-9 secarm_0
9-14 sec arm_1
14-17 sec arm_2
stop at arm_0
 */
School_2_1.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    if (currentPostion >= 2000 && currentPostion < 7000) {
        this.showHandAt(1);
    } else if (currentPostion >= 7000 && currentPostion < 9000) {
        this.showHandAt(0);
    } else if (currentPostion >= 9000 && currentPostion < 14000) {
        this.showHandAt(1);
    } else if (currentPostion >= 14000 && currentPostion < 17000) {
        this.showHandAt(2);
    } else {
        this.showHandAt(0);
    }
};

/**
 * cancel running intro animation
 */
School_2_1.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);

    this.figure_head2.visible = false;
    this.figure_head1.visible = true;
    this.mouths.hide();
    this.mouths.visible = false;
    this.showHandAt(0);

    this.help.interactive = true;
    this.help.buttonMode = true;

    this.ovaigen.visible = false;
    main.overlay.visible = false;
}

/**
 * play outro animation
 */
School_2_1.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.mouths.show();
        self.figure_head1.visible = false;
        self.figure_head2.visible = true;
    }
    this.outroCancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.outroCancelHandler);
}

/**
 * cancel outro animation
 */
School_2_1.prototype.cancelOutro = function() {

    var self = this;

    createjs.Sound.stop();
    self.mouths.hide(6);
    self.mouths.visible = false;
    self.figure_head2.visible = false;
    self.figure_head1.visible = true;

    self.ovaigen.visible = true;
    self.help.interactive = true;
    self.help.buttonMode = true;
    main.overlay.visible = false;
};

/**
 * create sprites,animation,sound related to intro scene
 */
School_2_1.prototype.introScene = function() {

    var self = this;

    this.board = createRect({
        x: 319,
        y: 151,
        w: 1416,
        h: 898,
        color: 0x0b8e36,
        lineStyle: true,
        lineColor: 0x634e42,
        lineWidth: 12

    });
    this.sceneContainer.addChild(this.board);

    this.board_tray = new PIXI.Sprite.fromFrame('board_tray.png');
    this.board_tray.position.set(212, 1050);
    main.texturesToDestroy.push(this.board_tray);
    this.sceneContainer.addChild(this.board_tray);

    this.hands = [
        new PIXI.Sprite.fromFrame('arm0.png'),
        new PIXI.Sprite.fromFrame('arm1.png'),
        new PIXI.Sprite.fromFrame('arm2.png'),
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(210, 536);
        this.introContainer.addChild(this.hands[i]);
        this.hands[i].visible = false;
    };
    this.hands[2].position.y = 420;
    this.hands[0].visible = true;

    this.figure_body = new PIXI.Sprite.fromFrame('girl_body.png');
    this.figure_body.position.set(10, 297);
    main.texturesToDestroy.push(this.figure_body);
    this.introContainer.addChild(this.figure_body);

    this.figure_head1 = new PIXI.Sprite.fromFrame('head1.png');
    this.figure_head1.position.set(34, 187);
    main.texturesToDestroy.push(this.figure_head1);
    this.introContainer.addChild(this.figure_head1);

    this.figure_head2 = new PIXI.Sprite.fromFrame('head2.png');
    this.figure_head2.position.set(34, 187);
    main.texturesToDestroy.push(this.figure_head2);
    this.introContainer.addChild(this.figure_head2);
    this.figure_head2.visible = false;

    this.mouths = new Animation({
        "image": "mouth",
        "length": 4,
        "x": 168,
        "y": 450,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 2, 4, 1],
        "frameId": 2
    });
    this.mouths.visible = false;

    this.introContainer.addChild(this.mouths);

    this.outroScene();
}

/**
 * create sprites,animation,sound related to outro scene
 */
School_2_1.prototype.outroScene = function() {

    var self = this;

    this.outroContainer.visible = false;

    this.brajobbat = new PIXI.Sprite.fromFrame('brajobbat.png');
    this.brajobbat.position.set(577, 281);
    main.texturesToDestroy.push(this.brajobbat);
    this.outroContainer.addChild(this.brajobbat);

    // back to intro page
    this.ovaigen = PIXI.Sprite.fromFrame('ovaigen.png');
    this.ovaigen.position.set(866, 709);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.ovaigen.visible = false;
    this.outroContainer.addChild(this.ovaigen);
}

/**
 * show hand at
 * @param  {[type]} number [description]
 * @return {[type]}        [description]
 */
School_2_1.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    };
    if (isNumeric(number))
        this.hands[number].visible = true;
}

/**
 * load exercise related stuffs
 */
School_2_1.prototype.loadExercise = function() {

    this.exerciseBaseContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.exerciseBaseContainer);

    // position, size parameter for exercise
    this.params = {
        position: {
            white: {
                x: 547,
                y: 373
            },
            image: {
                x: 556,
                y: 400,
            },
            word: {
                0: {
                    x: 617,
                    y: 764,
                },
                1: {
                    x: 1091,
                    y: 252
                },
                2: {
                    x: 1091,
                    y: 500
                },
                3: {
                    x: 1091,
                    y: 763
                }
            },
            line: {
                0: {
                    x: 610 - 80,
                    y: 875
                },
                1: {
                    x: 1071,
                    y: 376
                },
                2: {
                    x: 1071,
                    y: 631
                },
                3: {
                    x: 1071,
                    y: 875
                }
            },
            wrongViewText: {
                x: 599,
                y: 480
            },
            wrongViewMark: {
                0: {
                    x: 1066,
                    y: 231
                },
                1: {
                    x: 1066,
                    y: 481
                },
                2: {
                    x: 1066,
                    y: 739
                }
            },
            feedback: {
                x: 514,
                y: 1028
            },
        },
        size: {
            white: {
                width: 345,
                height: 345
            },
            line: {
                width: 54,
                height: 11
            },
            wrongViewMark: {
                width: 401,
                height: 171
            }
        },
        relativeSize: {
            feedback: 95,
        }
    }

    this.whiteGraphics = createRect({
        x: this.params.position.white.x,
        y: this.params.position.white.y,
        w: this.params.size.white.width,
        h: this.params.size.white.height
    });

    this.exerciseBaseContainer.addChild(this.whiteGraphics);

    this.shadeGraphics = createRect({
        w: this.params.size.wrongViewMark.width,
        h: this.params.size.wrongViewMark.height,
        color: 0x000000
    });
    this.shadeTexture = this.shadeGraphics.generateTexture();

    this.shadeSpriteArr = [];
    for (var i = 0; i < 3; i++) {
        var shadeSprite = new PIXI.Sprite(this.shadeTexture);
        shadeSprite.position.x = this.params.position.wrongViewMark[i].x;
        shadeSprite.position.y = this.params.position.wrongViewMark[i].y;
        shadeSprite.alpha = 0.2;
        shadeSprite.visible = false;
        this.shadeSpriteArr.push(shadeSprite);
        this.exerciseBaseContainer.addChild(shadeSprite);
    };

    this.lineGraphics = createLine({
        x1: this.params.size.line.width,
        lineWidth: this.params.size.line.height,
        color: 0xFFFFFF
    });
    this.lineTexture = this.lineGraphics.generateTexture();

    this.greenBox = createRect({
        "alpha": 0.6,
        "color": 0x00ff00,

    });

    this.greenBox.visible = false;
    this.greenBox.width = this.params.size.white.width;
    this.greenBox.height = this.params.size.white.height;

    this.feedbackContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.feedbackContainer);

    this.generateFeedback();

    this.soundTexture = new PIXI.Texture.fromFrame('sound_grey.png');

    this.answerLineSpriteArr = [];
    this.lineSpriteArr = [];

    for (var i = 0; i < 4; i++) {
        for (var j = 0; j < 2; j++) {
            var lineSprite = new PIXI.Sprite(this.lineTexture);
            lineSprite.position.x = this.params.position.line[i].x + (j * this.params.size.line.width);
            lineSprite.position.y = this.params.position.line[i].y;
            if (i > 0)
                this.lineSpriteArr.push(lineSprite);
            else
                this.answerLineSpriteArr.push(lineSprite);

            this.exerciseBaseContainer.addChild(lineSprite);
        };
    };

    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.exerciseContainer);

    this.replaceContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.replaceContainer);

    this.initObjectPooling();
}

/**
 * manage exercise with new sets randomized
 */
School_2_1.prototype.runExercise = function() {

    var self = this;

    this.jsonData = this.wallSlices[0];
    console.info("word:", this.jsonData.word);
    //    console.info("options:", JSON.stringify(this.jsonData.options));

    this.imageSprite = this.jsonData.image;
    this.imageSprite.width = this.imageSprite.height = this.params.size.white.width;

    // this.imageSprite.position.set(this.params.position.image.x - 9, this.params.position.image.y - 26);
    this.imageSprite.position.set(this.params.position.white.x, this.params.position.white.y);

    this.soundSprite = new PIXI.Sprite(this.soundTexture);
    this.soundSprite.position.x = this.params.position.image.x;
    this.soundSprite.position.y = this.params.position.image.y + 252 + 15;

    this.exerciseContainer.addChild(this.imageSprite);
    this.exerciseContainer.addChild(this.soundSprite);
    this.exerciseContainer.addChild(this.greenBox);

    this.soundSprite.interactive = true;
    this.soundSprite.buttonMode = true;
    this.soundSprite.howl = this.jsonData.sound;

    this.soundSprite.click = this.soundSprite.tap = this.onSoundPressed.bind(this.soundSprite);

    this.answer = this.jsonData.word.substring(0, 2);

    this.answer1 = this.jsonData.word.replace(/^.{0,2}/, '');

    this.wordText1 = new PIXI.Text("" + this.answer1 + " ", {
        'font': "italic 123px Arial",
        'fill': "white",
    });

    this.wordText1.position.x = this.params.position.word[0].x + 50;
    this.wordText1.position.y = this.params.position.word[0].y;
    this.exerciseContainer.addChild(this.wordText1);

    this.optionTextArr = [];
    for (var i = 0; i < this.jsonData.options.length; i++) {
        var wordText3 = new PIXI.Text("" + this.jsonData.options[i] + " ", {
            'font': "italic 123px Arial",
            'fill': "white",
        });

        wordText3.replaceChars = this.jsonData.options[i].substring(0, 2);
        wordText3.position.x = this.params.position.word[i + 1].x;
        wordText3.position.y = this.params.position.word[i + 1].y;
        wordText3.interactive = true;
        wordText3.buttonMode = true;
        this.optionTextArr.push(wordText3);
        this.exerciseContainer.addChild(wordText3);
    };

    for (var i = 0; i < this.optionTextArr.length; i++) {
        this.onTextClick(i);
    };

}

School_2_1.prototype.onSoundPressed = function() {
    this.soundInstance = createjs.Sound.play(this.howl, {
        interrupt: createjs.Sound.INTERRUPT_NONE
    });
    this.soundInstance.addEventListener("failed", handleFailed);
    this.soundInstance.addEventListener("complete", handleComplete);
};


/**
 * on text click logic
 * @param  {int} index
 */
School_2_1.prototype.onTextClick = function(index) {

    var self = this;

    this.optionTextArr[index].click = this.optionTextArr[index].tap = function(data) {
        if (addedListeners) return false;
        addedListeners = true;
        var clickObj = this;

        var replaceText = new PIXI.Text("" + this.replaceChars + " ", {
            'font': "italic 123px Arial",
            'fill': "white",
        });
        replaceText.position.x = self.params.position.word[0].x - 10 - 70;
        replaceText.position.y = self.params.position.word[0].y;

        self.wordText1.position.x = replaceText.position.x + replaceText.width - 35;

        self.replaceContainer.removeChildren();
        self.replaceContainer.addChild(replaceText);
        setTimeout(function() {
            self.evaluate(index, clickObj);
        }, 1000)

    }
}

/**
 * evaluate exercise on text click for correct or wrong check
 * @param  {int} index
 * @param  {object} clickObj
 */
School_2_1.prototype.evaluate = function(index, clickObj) {
    var self = this;
    if (self.answer === clickObj.replaceChars) {
        correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
        self.common.showRedOrGreenBox(self.params.position.white.x, self.params.position.white.y, self.greenBox);
        self.correctFeedback();
    } else {
        // self.replaceContainer.removeChildren();
        var wrongText = new PIXI.Text("" + clickObj.replaceChars + self.answer1 + " ", {
            'font': "italic 80px Arial",
            'fill': "black",
        });
        clickObj.setStyle({
            'font': "italic 123px Arial",
            'fill': "red",
        });

        self.replaceContainer.getChildAt(0).setStyle({
            'font': "italic 123px Arial",
            'fill': "red",
        })

        self.wordText1.setStyle({
            'font': "italic 123px Arial",
            'fill': "red",
        })

        self.answerLineSpriteArr[0].visible = false;
        self.answerLineSpriteArr[1].visible = false;

        wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
        wrongText.position.x = self.params.position.wrongViewText.x - 20;
        wrongText.position.y = self.params.position.wrongViewText.y + 20

        // self.imageSprite.visible = false;
        // self.soundSprite.visible = false;
        // self.replaceContainer.addChild(wrongText);

        self.shadeSpriteArr[index].visible = true;
        self.lineSpriteArr[index * 2].visible = false;
        self.lineSpriteArr[index * 2 + 1].visible = false;
        setTimeout(function() {
            self.replaceContainer.removeChildren();
            // self.imageSprite.visible = true;
            // self.soundSprite.visible = true;
            clickObj.setStyle({
                'font': "italic 123px Arial",
                'fill': "white",
            });
            self.shadeSpriteArr[index].visible = false;
            self.lineSpriteArr[index * 2].visible = true;
            self.lineSpriteArr[index * 2 + 1].visible = true;

            self.answerLineSpriteArr[0].visible = true;
            self.answerLineSpriteArr[1].visible = true;
            self.wordText1.setStyle({
                'font': "italic 123px Arial",
                'fill': "white",
            })

            self.wrongFeedback();

        }, 2000)
    }
}

/**
 * generate output feedback
 */
School_2_1.prototype.generateFeedback = function(i) {

    this.hiddenFeedback = [];
    this.feedbackTexture = PIXI.Texture.fromFrame('feedback.png');

    for (var i = 0; i < 10; i++) {
        var feedbackSprite = new PIXI.Sprite(this.feedbackTexture);
        feedbackSprite.position = {
            x: this.params.position.feedback.x + (i * this.params.relativeSize.feedback),
            y: this.params.position.feedback.y,
        }
        feedbackSprite.alpha = 0.4;
        this.feedbackContainer.addChild(feedbackSprite);
        this.hiddenFeedback.push(feedbackSprite);
    };

}

/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
School_2_1.prototype.correctFeedback = function() {

    var self = this;

    addedListeners = false;

    setTimeout(function() {
        var feedback = localStorage.getItem('school_2_1');
        feedback++;

        self.hiddenFeedback[feedback - 1].alpha = 1;
        if (feedback >= School_2_1.totalFeedback) {
            setTimeout(self.onComplete.bind(self), 1000);
        } else {
            localStorage.setItem('school_2_1', feedback);
            self.getNewExerciseSet();
        }

    }, 2000);

}

/**
 * on wrong answer, feedback decrease
 */
School_2_1.prototype.wrongFeedback = function() {

    var self = this;

    addedListeners = false;

    setTimeout(function() {

        var feedback = localStorage.getItem('school_2_1');
        feedback--;
        if (feedback >= 0) {
            self.hiddenFeedback[feedback].alpha = 0.2;
            localStorage.setItem('school_2_1', feedback);
        };

    }, 2000);

}

/**
 * prefetch exercise data and cache number of sets defined only.
 */
School_2_1.prototype.initObjectPooling = function() {

    var self = this;

    this.lastBorrowSprite = null;

    this.wallSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/school/school_2_1.json?nocache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        self.pool = new ExerciseSpritesPool(loader.json);
        //borrow two sprites once
        self.generatePoolCategory();
        self.borrowWallSprites(2);
        self.runExercise();

    });
    loader.load();
}

School_2_1.prototype.generatePoolCategory = function() {

    var keyArr = [];

    var spriteArr = this.pool.sprites,
        spriteLen = spriteArr.length;

    for (var i = 0; i < spriteLen; i++) {
        keyArr.push(spriteArr[i][0].word[0]);
    };

    this.poolCategory = keyArr.filter(onlyUnique);

}

/**
 * borrow objects from pool
 * cache audios, sprites
 * @param  {int} num
 */
School_2_1.prototype.borrowWallSprites = function(num) {

    var rand, category, sprite, spriteObj, imageSprite, soundHowl;

    for (var i = 0; i < num; i++) {

        rand = makeUniqueRandom(this.poolCategory.length);
        this.borrowCategory = this.poolCategory[rand];

        console.log('rand', rand)
        console.log('borrow category', this.borrowCategory)

        sprite = this.pool.borrowUniqueSprite(this.borrowCategory, this.lastBorrowSprite);

        spriteObj = sprite[0];

        this.lastBorrowSprite = spriteObj.word[0];

        imageSprite = PIXI.Sprite.fromImage('uploads/images/' + spriteObj.word[1]);

        soundHowl = loadSound('uploads/audios/' + StripExtFromFile(spriteObj.word[2]));

        this.wallSlices.push({
            image: imageSprite,
            sound: soundHowl,
            word: spriteObj.word[0],
            options: spriteObj.options,
        });
    }
};

/**
 * return objects to pool
 */
School_2_1.prototype.returnWallSprites = function() {

    this.wallSlices.shift();

};

/**
 * get new sets from object pool
 */
School_2_1.prototype.getNewExerciseSet = function() {
    this.replaceContainer.removeChildren();
    this.exerciseContainer.removeChildren();

    this.returnWallSprites();
    this.borrowWallSprites(1);
    this.runExercise();
}

/**
 * reset exercise and get new set
 */
School_2_1.prototype.restartExercise = function() {

    this.resetExercise();

    this.feedbackContainer.removeChildren();

    this.exerciseBaseContainer.visible = true;

    localStorage.setItem('school_2_1', 0);
    this.outroContainer.visible = false;
    this.introContainer.visible = true;

    this.hiddenFeedback.length = 0;
    this.generateFeedback();

    this.getNewExerciseSet();

};

/**
 * the outro scene is displayed
 */
School_2_1.prototype.onComplete = function() {

    var self = this;
    this.introMode = false;

    this.help.interactive = false;
    this.help.buttonMode = false;

    self.exerciseBaseContainer.visible = false;
    self.exerciseContainer.visible = false;
    self.replaceContainer.visible = false;

    self.mouths.hide(6);
    self.showHandAt(0);
    self.figure_head1.visible = true;
    self.figure_head2.visible = false;
    self.figure_body.visible = true;

    this.outroContainer.visible = true;

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        self.cancelOutro();
        self.restartExercise();
    }

    this.outroAnimation();

}

/**
 * reset exercise to default values
 */
School_2_1.prototype.resetExercise = function() {

    this.ovaigen.visible = false;
    this.resetScene();
    this.introMode = true;
};

/**
 * clean memory by clearing reference of object
 */
School_2_1.prototype.cleanMemory = function() {

    this.common = null;
    this.introId = null;
    this.outroId = null;

    if (this.assetsToLoad) this.assetsToLoad.length = 0;

    if (this.sceneBackground) this.sceneBackground.clear();
    this.sceneBackground = null;

    this.help = null;

    if (this.hiddenFeedback) this.hiddenFeedback.length = 0;

    this.introInstance = null;
    this.timerId = null;
    this.introCancelHandler = null;
    this.outroCancelHandler = null;

    if (this.board) this.board.clear();
    this.board = null;

    this.board_tray = null;

    if (this.hands) this.hands.length = 0;

    this.figure_body = null;
    this.figure_head1 = null;
    this.figure_head2 = null;
    this.mouths = null;
    this.brajobbat = null;
    this.ovaigen = null;

    this.exerciseBaseContainer.removeChildren();
    this.exerciseBaseContainer = null;

    wipe(this.params);
    this.params = null;

    if (this.whiteGraphics) this.whiteGraphics.clear();
    this.whiteGraphics = null;

    if (this.shadeGraphics) this.shadeGraphics.clear();
    this.shadeGraphics = null;

    this.shadeTexture = null;

    if (this.shadeSpriteArr) this.shadeSpriteArr.length = 0;

    if (this.lineGrpahics) this.lineGrpahics.clear();
    this.lineGrpahics = null;
    this.lineTexture = null;

    this.feedbackContainer.removeChildren();
    this.feedbackContainer = null;

    this.soundTexture = null;

    if (this.lineSpriteArr) this.lineSpriteArr.length = 0;

    this.exerciseContainer.removeChildren();
    this.exerciseContainer = null;

    this.replaceContainer.removeChildren();
    this.replaceContainer = null;

    if (this.jsonData) this.jsonData = null;

    this.imageSprite = null;
    this.soundSprite = null;

    this.answer = null;
    this.answer1 = null;

    if (this.optionTextArr) this.optionTextArr.length = 0;

    this.soundInstance = null;

    this.lastBorrowSprite = null;

    if (this.wallSlices) this.wallSlices.length = 0;

    wipe(this.pool);
    this.pool = null;

    if (this.poolCategory) this.poolCategory.length = 0;
    this.borrowCategory = null;

    this.introMode = null;

    this.outroContainer.removeChildren();
    this.introContainer.removeChildren();
    this.sceneContainer.removeChildren();

    this.outroContainer = null;
    this.introContainer = null;
    this.sceneContainer = null;
}