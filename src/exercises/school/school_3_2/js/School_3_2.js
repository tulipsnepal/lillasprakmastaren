function School_3_2() {
    cl.show()

    PIXI.DisplayObjectContainer.call(this);

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position.set(77, 88);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.feedbackContainer = new PIXI.DisplayObjectContainer();
    this.animationContainer = new PIXI.DisplayObjectContainer();

    localStorage.setItem('school_3_2', 0);
    main.CurrentExercise = "school_3_2";

    this.currentBoxTop = 0;
    this.currentBoxBottom = 0;
    this.noOfBoxDragged = 0;

    main.texturesToDestroy = [];
    this.buttonClicked = [];
    this.firstShelfGroup = [];
    this.secondShelfGroup = [];
    this.itemsInFirstShelf = [];
    this.itemsInSecondShelf = [];
    this.itemIndexInFirstShelf = [];
    this.itemIndexInSecondShelf = [];
    this.itemsDragged = [];
    this.redItems = [];
    this.greenItems = [];

    this.common = new Common();
    this.loadSpriteSheet();

    // everything connected to this is rendered.
    this.addChild(this.sceneContainer);
}

School_3_2.constructor = School_3_2;
School_3_2.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
School_3_2.imagePath = 'build/exercises/school/school_3_2/images/';
School_3_2.audioPath = 'build/exercises/school/school_3_2/audios/';
School_3_2.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

/**
 * load array of assets of different format
 */
School_3_2.prototype.loadSpriteSheet = function() {
    this.introId = loadSound(School_3_2.audioPath + 'intro');
    this.outroId = loadSound(School_3_2.audioPath + 'outro');
    // create an array of assets to load
    this.assetsToLoad = [
        School_3_2.imagePath + "sprite_school_3_2.json",
        School_3_2.imagePath + "sprite_school_3_2.png",
        School_3_2.imagePath + "sprite_school_3_2_2.json",
        School_3_2.imagePath + "sprite_school_3_2_2.png",
        School_3_2.imagePath + "basebg.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
School_3_2.prototype.spriteSheetLoaded = function() {

    // hide loader
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    this.sceneBackground = new PIXI.Sprite.fromImage(School_3_2.imagePath + "basebg.png");
    this.sceneBackground.position = {
        x: 0,
        y: 0
    };
    main.texturesToDestroy.push(this.sceneBackground);
    this.sceneContainer.addChild(this.sceneBackground);

    // exercise instruction
    this.help = PIXI.Sprite.fromFrame("topcorner.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);
    this.sceneContainer.addChild(this.help);

    this.introScene();
    this.loadExercise();
    this.outroScene();

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.feedbackContainer);
    this.sceneContainer.addChild(this.animationContainer);
    this.sceneContainer.addChild(this.introContainer);
}

/**
 * hand visibility
 * @param  {int} number
 */
School_3_2.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    };
    if (isNumeric(number))
        this.hands[number].visible = true;
}

/**
 * play intro animation
 */
School_3_2.prototype.introAnimation = function() {

    createjs.Sound.stop();

    if (!this.introMode)
        this.restartExercise();

    this.help.interactive = false;
    this.help.buttonMode = false;

    main.overlay.visible = true;

    this.mouthParams.show();
    this.ovaigen.visible = false;
    this.kitchen_lady.visible = true;
    this.kitchen_lady_outro.visible = false;

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
 */
School_3_2.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    if (currentPostion >= 1000 && currentPostion < 2000) {
        this.showHandAt(1);
    } else if (currentPostion >= 2000 && currentPostion < 3000) {
        this.showHandAt(0);
    } else {
        this.showHandAt(0);
    }
};

/**
 * cancel running intro animation
 */
School_3_2.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);

    this.showHandAt(0);
    this.mouthParams.hide();
    this.help.interactive = true;
    this.help.buttonMode = true;

    this.ovaigen.visible = false;
    main.overlay.visible = false;
}

/**
 * play outro animation
 */
School_3_2.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.mouthParams.show();
        self.eyeParams.show();
        self.kitchen_lady.visible = false;
        self.kitchen_lady_outro.visible = true;
        self.hands[0].visible = false;
        self.hands[1].visible = false;
    }
    this.outroCancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.outroCancelHandler);
}


/**
 * cancel outro animation running
 */
School_3_2.prototype.cancelOutro = function() {

    var self = this;

    createjs.Sound.stop();
    self.mouthParams.hide();
    self.ovaigen.visible = true;
    self.help.interactive = true;
    self.help.buttonMode = true;
    main.overlay.visible = false;
};

/**
 * create sprites,animation,sound related to intro scene
 */
School_3_2.prototype.introScene = function() {

    var self = this;

    this.kitchen_lady = new PIXI.Sprite.fromFrame('kitchen_lady.png');
    this.kitchen_lady.position.set(30 + 76, 239);
    main.texturesToDestroy.push(this.kitchen_lady);
    this.animationContainer.addChild(this.kitchen_lady);

    this.kitchen_table = new PIXI.Sprite.fromFrame("table_desk.png");
    this.kitchen_table.position = {
        x: 39,
        y: 962
    };
    main.texturesToDestroy.push(this.kitchen_table);
    this.animationContainer.addChild(this.kitchen_table);

    this.kitchen_lady_outro = new PIXI.Sprite.fromFrame('kitchen_lady_outro.png');
    this.kitchen_lady_outro.position.set(35, 239);
    this.kitchen_lady_outro.visible = false;
    main.texturesToDestroy.push(this.kitchen_lady_outro);
    this.animationContainer.addChild(this.kitchen_lady_outro);

    this.mouthParams = new Animation({
        "image": "mouth",
        "length": 4,
        "x": 223 + 76,
        "y": 487,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 2, 4, 1, 3],
        "frameId": 0
    });

    this.animationContainer.addChild(this.mouthParams);

    this.eyeParams = new Animation({
        "image": "eyes",
        "length": 2,
        "x": 183 + 76,
        "y": 405,
        "speed": 2,
        "pattern": true,
        "sequence": [1, 2, 2, 1, 1, 2, 2, 1, 2, 1, 1, 2],
        "frameId": 2
    });
    self.eyeParams.show();
    this.animationContainer.addChild(this.eyeParams);

    this.hands = [
        new PIXI.Sprite.fromFrame('arm1.png'),
        new PIXI.Sprite.fromFrame('arm2.png')
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(262 + 76, 525);
        this.animationContainer.addChild(this.hands[i]);
        if (i != 0) this.hands[i].visible = false;
    };
    //this.animationContainer.visible = false;
}

/**
 * create sprites,animation,sound related to outro scene
 */
School_3_2.prototype.outroScene = function() {

    var self = this;

    // back to intro page
    this.ovaigen = PIXI.Sprite.fromFrame('ovaigen.png');
    this.ovaigen.position.set(943, 763);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.ovaigen.visible = false;
    this.introContainer.addChild(this.ovaigen);
}

/**
 * load exercise related stuffs
 */
School_3_2.prototype.loadExercise = function() {

    this.brownGraphics = createRect({
        w: 960,
        h: 35,
        color: 0x683c10,
        lineStyle: 0
    });
    this.brownRect = this.brownGraphics.generateTexture();
    for (var k = 0; k < 2; k++) {
        var drawBrownBox = new PIXI.Sprite(this.brownRect);
        drawBrownBox.position = {
            x: 637,
            y: k == 1 ? 338 : 607
        };
        main.texturesToDestroy.push(drawBrownBox);
        this.sceneContainer.addChild(drawBrownBox);
    }

    this.soundTexture = PIXI.Texture.fromFrame("sound_grey.png");

    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.exerciseContainer);

    this.createFeedBack();
    this.initObjectPooling();
}

/**
 * Render feedback to the container
 */
School_3_2.prototype.createFeedBack = function() {
    var self = this;
    var feedback = new PIXI.Texture.fromFrame("fb.png");

    for (var k = 0; k < 10; k++) {
        var feedbackSprite = new PIXI.Sprite(feedback);

        feedbackSprite.position = {
            x: 1712,
            y: 1067 - (k * 113)
        };
        main.texturesToDestroy.push(feedbackSprite);
        this.feedbackContainer.addChild(feedbackSprite);
        feedbackSprite.alpha = 0.3;
    }
};

/**
 * manage exercise with new sets randomized
 */
School_3_2.prototype.runExercise = function() {

    var self = this;
    this.dragImage = [];
    this.jsonData = this.wallSlices[0];
    this.whiteGraphics = createRect({
        w: 180,
        h: 64,
        color: 0xffffff,
        lineStyle: 1
    });
    this.whiteRect = this.whiteGraphics.generateTexture();

    //render folders
    this.utencilPositions = [
        79, 738,
        409, 747,
        759, 732,
        1087, 732,
        1285, 739
    ];

    this.boxPositions = [
        45, 152,
        56, 138,
        45, 140,
        5, 135,
        100, 150
    ];

    for (var k = 0; k < 5; k++) {
        var imgSprite = new PIXI.Sprite.fromFrame("item" + (k + 1) + ".png");
        imgSprite.position = {
            x: this.utencilPositions[k * 2],
            y: this.utencilPositions[k * 2 + 1]
        };

        imgSprite.buttonMode = true;
        imgSprite.interactive = true;

        imgSprite.oldPosition = [this.utencilPositions[k * 2], this.utencilPositions[k * 2 + 1]];

        imgSprite.group = this.jsonData.group[k];
        imgSprite.hitIndex = k;

        this.exerciseContainer.addChild(imgSprite);



        var drawWhiteBox = new PIXI.Sprite(this.whiteRect);
        drawWhiteBox.position = {
            x: this.boxPositions[k * 2],
            y: this.boxPositions[k * 2 + 1]
        };

        imgSprite.addChild(drawWhiteBox);

        //add text to it;
        var textLength = this.jsonData.word[k].length;
        if (textLength > 6) {
            var fontStyle = "40px Arial";
        } else {
            var fontStyle = "50px Arial";
        }

        var exerciseText = new PIXI.Text("" + this.jsonData.word[k], {
            font: fontStyle,
            fill: "black"
        });

        exerciseText.position = {
            x: (drawWhiteBox.width - exerciseText.width) / 2,
            y: (drawWhiteBox.height - exerciseText.height) / 2
        };

        drawWhiteBox.addChild(exerciseText);

        var redItem = new PIXI.Sprite.fromFrame("item_red" + (k + 1) + ".png");
        redItem.visible = false;
        this.redItems.push(redItem);
        imgSprite.addChild(redItem);

        var greenItem = new PIXI.Sprite.fromFrame("item_green" + (k + 1) + ".png");
        greenItem.visible = false;
        this.greenItems.push(greenItem);
        imgSprite.addChild(greenItem);

        main.texturesToDestroy.push(imgSprite);
        this.dragImage.push(imgSprite);

    }

    for (var i = 0; i < this.dragImage.length; i++) {
        this.onDragging(i);
    };

    //comment
}

/**
 * drag drop sprite
 * on correct drop, feedback is updated
 */
School_3_2.prototype.onDragging = function(i) {

    var self = this;

    // use the mousedown and touchstart
    this.dragImage[i].mousedown = this.dragImage[i].touchstart = function(data) {

        if (addedListeners) return false;
        data.originalEvent.preventDefault()
            // store a refference to the data
            // The reason for this is because of multitouch
            // we want to track the movement of this particular touch
        this.data = data;
        this.alpha = 0.9;
        this.dragging = true;
        this.sx = this.data.getLocalPosition(this).x;
        this.sy = this.data.getLocalPosition(this).y;
        this.bringToFront();

    }

    this.dragImage[i].mouseup = this.dragImage[i].mouseupoutside = this.dragImage[i].touchend = this.dragImage[i].touchendoutside = function(data) {
        if (addedListeners) return false;
        var dragSelf = this;

        this.dragging = false;
        // set the interaction data to null
        this.data = null;
        this.alpha = 1;

        var dropResult = self.isDropArea(this.dx, this.dy, self.jsonData.group[i], dragSelf);

        if (dropResult !== false) {

            self.positionItems("top");
            self.positionItems("bottom");


            if (dropResult == "top") {
                self.currentBoxTop++;
            } else {
                self.currentBoxBottom++;
            }


            //IF ALL 5 PANS ARE DRAGGED, THEN EVALUATE THE RESULT

            var checkIfCorrect = [];

            if (self.noOfBoxDragged == 5) {
                checkIfCorrect[0] = self.firstShelfGroup.AllValuesSame();
                checkIfCorrect[1] = self.secondShelfGroup.AllValuesSame();

                //set interactive to false for all dragged pans so that they can't be dragged further.
                for (var j = 0; j < 5; j++) {
                    self.dragImage[j].interactive = false;
                }

                if (checkIfCorrect[0] && checkIfCorrect[1]) //CORRECT ANSWER
                {
                    setTimeout(function() {
                        correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                        for (var k = 0; k < 5; k++) {
                            self.common.showRedOrGreenBox(0, 0, self.greenItems[k], 800);
                        }

                    }, 1000);

                    setTimeout(function() {
                        dragSelf.interactive = false;
                        self.correctFeedback();
                    }, 1400);
                } else { //WRONG ANSWER
                    var incorrectAnswers = self.getCorrectAnswerIndex();
                    self.itemsDragged = self.itemsDragged.removeDuplicates();
                    var gk = self.itemsDragged.sort();

                    setTimeout(function() {
                        wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                        for (var k = 0; k < incorrectAnswers.length; k++) {
                            //   var idx = gk.indexOf(incorrectAnswers[k]);
                            var idx = incorrectAnswers[k];
                            self.common.showRedOrGreenBox(0, 0, self.redItems[idx], 800);
                        }
                    }, 1000);

                    setTimeout(function() {
                        // var itemToRemoveFromDrag = [];
                        for (var k = 0; k < incorrectAnswers.length; k++) {
                            //var index = gk.indexOf(incorrectAnswers[k]);
                            var index = incorrectAnswers[k];
                            self.dragImage[index].interactive = true;
                            self.dragImage[index].position = {
                                x: self.dragImage[index].oldPosition[0],
                                y: self.dragImage[index].oldPosition[1]
                            };

                            self.dragImage[index].interactive = true; // set interactive to true again for incorrectly dragged pans
                            self.noOfBoxDragged--;

                            //  itemToRemoveFromDrag.push(index);
                            var idR = self.itemIndexInFirstShelf.indexOf(incorrectAnswers[k]);

                            if (idR > -1) {
                                self.currentBoxTop--;
                                self.itemIndexInFirstShelf.splice(idR, 1);
                                self.firstShelfGroup.splice(idR, 1);
                                self.itemsInFirstShelf.splice(idR, 1);
                            } else {
                                self.currentBoxBottom--;
                                var idRs = self.itemIndexInSecondShelf.indexOf(incorrectAnswers[k]);
                                self.itemIndexInSecondShelf.splice(idRs, 1);
                                self.secondShelfGroup.splice(idRs, 1);
                                self.itemsInSecondShelf.splice(idRs, 1);
                            }
                            var mIdx = self.itemsDragged.indexOf(incorrectAnswers[k]);
                            self.itemsDragged.splice(mIdx, 1);

                        }
                        self.noOfBoxDragged = self.noOfBoxDragged < 0 ? 0 : self.noOfBoxDragged;
                        self.currentBoxTop = self.currentBoxTop < 0 ? 0 : self.currentBoxTop;
                        self.currentBoxBottom = self.currentBoxBottom < 0 ? 0 : self.currentBoxBottom;

                        self.positionItems("top");
                        self.positionItems("bottom");

                        self.wrongFeedback();
                    }, 1800);
                }
            }
            this.dx = 0;
            this.dy = 0;
            this.sx = 0;
            this.sy = 0;
        } else {
            var removeFromTotalDragged = false;
            var remove_index = self.itemIndexInFirstShelf.indexOf(dragSelf.hitIndex);
            var remove_index_2 = self.itemIndexInSecondShelf.indexOf(dragSelf.hitIndex);
            if (remove_index > -1) {
                self.currentBoxTop--;
                self.itemIndexInFirstShelf.splice(remove_index, 1);
                self.firstShelfGroup.splice(remove_index, 1);
                self.itemsInFirstShelf.splice(remove_index, 1);
                self.noOfBoxDragged--;
                removeFromTotalDragged = true;
                self.positionItems("top");
                self.positionItems("bottom");
            } else if (remove_index_2 > -1) {
                self.currentBoxBottom--;

                self.itemIndexInSecondShelf.splice(remove_index_2, 1);
                self.secondShelfGroup.splice(remove_index_2, 1);
                self.itemsInSecondShelf.splice(remove_index_2, 1);
                self.noOfBoxDragged--;
                removeFromTotalDragged = true;
                self.positionItems("top");
                self.positionItems("bottom");

            }
            // reset dragged element position
            dragSelf.position = {
                x: dragSelf.oldPosition[0],
                y: dragSelf.oldPosition[1]
            };


            if (removeFromTotalDragged == true) {
                self.itemsDragged = self.itemsDragged.getUnique();
            }



        }



    }

    // set the callbacks for when the mouse or a touch moves
    this.dragImage[i].mousemove = this.dragImage[i].touchmove = function(data) {
        if (addedListeners) return false;
        // set the callbacks for when the mouse or a touch moves
        if (this.dragging) {
            // need to get parent coords..
            var newPosition = this.data.getLocalPosition(this.parent);
            this.position.x = newPosition.x - this.sx;
            this.position.y = newPosition.y - this.sy;
            this.dx = newPosition.x;
            this.dy = newPosition.y;
        }
    };

}

/**
 * position items
 */
School_3_2.prototype.positionItems = function(at) {
    var self = this;
    var widthArray;
    var itemsArray;
    var yPos;
    if (at == "top") {
        widthArray = self.itemsInFirstShelf;
        itemsArray = self.itemIndexInFirstShelf;
        yPos = 100;
    } else {
        widthArray = self.itemsInSecondShelf;
        itemsArray = self.itemIndexInSecondShelf;
        yPos = 370;
    }


    for (var it = 0; it < itemsArray.length; it++) {

        if (it == 0) {
            xPos = 676;
        } else if (it == 1) {
            xPos = 676 + self.dragImage[itemsArray[0]].width + 20;
        } else if (it == 2) {
            xPos = 676 + self.dragImage[itemsArray[0]].width + self.dragImage[itemsArray[1]].width + 20;
        }
        self.dragImage[itemsArray[it]].position = {
            x: xPos,
            y: yPos
        }
    }
}

/**
 * Method to find which items are wrongly being dragged and find their index so that they can be bounced back.
 */
School_3_2.prototype.getCorrectAnswerIndex = function() {
    var self = this;
    var firstArr, secondArr, secondArrTemp, fromFirst, fromSecond, firstArrTemp;
    var fa = self.firstShelfGroup;
    var sa = self.secondShelfGroup;

    var itemsInFirstShelf = self.itemIndexInFirstShelf;
    var itemsInSecondShelf = self.itemIndexInSecondShelf;

    if (fa.length > sa.length) {
        firstArr = fa;
        fromFirst = 1;
        firstArrTemp = firstArr.slice();
    } else {
        firstArr = sa;
        firstArrTemp = firstArr.slice();
        fromFirst = 2;
    }

    if (fa.length < sa.length) {
        secondArr = fa;
        secondArrTemp = secondArr.slice();
        fromSecond = 1;
    } else {
        secondArr = sa;
        secondArrTemp = secondArr.slice();
        fromSecond = 2;
    }

    var arrayToReturn = [];
    var unique_element = firstArrTemp.getUnique();
    unique_element = unique_element.join();

    var indexOfFirstUnique = firstArr.indexOf(unique_element);

    var idxToRemoveInSecondArr = secondArr.indexOf(unique_element);

    if (fromFirst == 1) {
        arrayToReturn.push(itemsInFirstShelf[indexOfFirstUnique]);
    } else {
        arrayToReturn.push(itemsInSecondShelf[indexOfFirstUnique]);
    }

    var unique_element_second = secondArrTemp.getUnique();
    unique_element_second = unique_element_second.join();

    if (unique_element_second != "") {
        if (idxToRemoveInSecondArr > -1) {
            secondArrTemp.splice(idxToRemoveInSecondArr, 1);
        }

        var indexOfSecond = secondArr.indexOf(secondArrTemp[0]);

        if (fromSecond == 1) {
            arrayToReturn.push(itemsInFirstShelf[indexOfSecond]);
        } else {
            arrayToReturn.push(itemsInSecondShelf[indexOfSecond]);
        }
    }
    //console.log("Return = " + arrayToReturn);
    return arrayToReturn;
}

/**
 * extend array to get unique items
 */
Array.prototype.getUnique = function() {
    var uniques = [];
    for (var i = 0, l = this.length; i < l; ++i) {
        if (this.lastIndexOf(this[i]) == this.indexOf(this[i])) {
            uniques.push(this[i]);
        }
    }
    return uniques;
}

/**
 * check drag position is inside rectangular drop position
 * @param  {float} dx    drag positon x
 * @param  {float} dy    drag position y
 * @param  {integer} index
 * @return {boolean}       true/false
 */
School_3_2.prototype.isDropArea = function(dx, dy, group, draggedItem) {

    dropPositionsTop = [
        676, 100,
        966, 100,
        1271, 100
    ];

    dropPositionBottom = [
        676, 370,
        966, 370,
        1271, 370
    ];

    var x1 = 622,
        y1 = 72,
        x2 = x1 + 955,
        y2 = y1 + 282;

    var p1 = 622,
        q1 = 348,
        p2 = p1 + 955,
        q2 = q1 + 282;

    this.itemsDragged.push(draggedItem.hitIndex);

    //To check whether same item is dragged within same shelf
    var dragged_first_to_first = this.itemIndexInFirstShelf.indexOf(draggedItem.hitIndex);
    var dragged_second_to_second = this.itemIndexInSecondShelf.indexOf(draggedItem.hitIndex);

    if (isInsideRectangle(dx, dy, x1, y1, x2, y2) && this.itemIndexInFirstShelf.length < 3) {

        if (dragged_first_to_first < 0) { //if not dragged within the same shelf, then only add index to arrays and increase the dragcount
            this.boxPlacement = [dropPositionsTop[this.currentBoxTop * 2], 100];
            this.noOfBoxDragged++;

            this.firstShelfGroup.push(group);
            this.itemsInFirstShelf.push(draggedItem.width);
            this.itemIndexInFirstShelf.push(draggedItem.hitIndex);
            this.itemIndexInFirstShelf = this.itemIndexInFirstShelf.removeDuplicates();
        }


        //CHECK IF IT IS DRAGGED FROM SECOND SHELF TO FIRST...IF SO THEN REMOVE FROM ARRAYS RELATED TO SECOND SHELF
        var dragged_from_second = this.itemIndexInSecondShelf.indexOf(draggedItem.hitIndex);
        if (dragged_from_second > -1) { //dragged from second shelf
            this.itemIndexInSecondShelf.splice(dragged_from_second, 1);
            this.itemsInSecondShelf.splice(dragged_from_second, 1);
            this.secondShelfGroup.splice(dragged_from_second, 1);
            this.noOfBoxDragged--;
            this.currentBoxBottom--;
        }

        return "top";
    } else if (isInsideRectangle(dx, dy, p1, q1, p2, q2) && this.itemIndexInSecondShelf.length < 3) {

        if (dragged_second_to_second < 0) {
            this.boxPlacement = [dropPositionBottom[this.currentBoxBottom * 2], 370];
            this.noOfBoxDragged++;

            this.secondShelfGroup.push(group);
            this.itemsInSecondShelf.push(draggedItem.width);
            this.itemIndexInSecondShelf.push(draggedItem.hitIndex);
            this.itemIndexInSecondShelf = this.itemIndexInSecondShelf.removeDuplicates();
        }

        //CHECK IF IT IS DRAGGED FROM FIRST SHELF TO SECOND...IF SO THEN REMOVE FROM ARRAYS RELATED TO FIRST SHELF
        var dragged_from_first = this.itemIndexInFirstShelf.indexOf(draggedItem.hitIndex);
        if (dragged_from_first > -1) { //indeed dragged from first
            this.itemIndexInFirstShelf.splice(dragged_from_first, 1);
            this.itemsInFirstShelf.splice(dragged_from_first, 1);
            this.firstShelfGroup.splice(dragged_from_first, 1);
            this.noOfBoxDragged--;
            this.currentBoxBottom--;
        }

        return "bottom";
    } else {
        // console.log('out');
        return false;
    }

}


/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
School_3_2.prototype.correctFeedback = function() {
    var self = this;
    var feedback = localStorage.getItem('school_3_2');
    feedback++;

    localStorage.setItem('school_3_2', feedback);
    this.updateFeedback();

    if (feedback >= School_3_2.totalFeedback) {
        setTimeout(this.onComplete.bind(this), 1000);
    } else {
        setTimeout(function() {
            self.getNewExerciseSet();
        }, 1600);
    }
}

/**
 * on wrong feedback , decrease feedback by 1
 */
School_3_2.prototype.wrongFeedback = function() {
    var feedback = localStorage.getItem('school_3_2');
    feedback--;

    if (feedback < 0)
        feedback = 0;

    localStorage.setItem('school_3_2', feedback);
    this.updateFeedback();

}


/**
 * update feedback
 */
School_3_2.prototype.updateFeedback = function() {

    var self = this;
    addedListeners = false;
    var counter = localStorage.getItem('school_3_2');

    for (var i = 0; i < self.feedbackContainer.children.length; i++) {
        self.feedbackContainer.children[i].alpha = 0.3;
    }

    for (var j = 0; j < counter; j++) {
        self.feedbackContainer.children[j].alpha = 1;
    }
}

/**
 * prefetch exercise data and cache number of sets defined only.
 */
School_3_2.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/school/school_3_2.json?nocache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        self.pool = new ExerciseSpritesPool(loader.json);
        //borrow two sprites once
        self.borrowWallSprites(2);
        self.runExercise();
    });
    loader.load();
}

/**
 * borrow objects from pool
 */
School_3_2.prototype.borrowWallSprites = function(num) {

    var self = this;

    for (var i = 0; i < num; i++) {

        var sprite = this.pool.borrowSprite(),
            spriteObj = sprite[0],
            imageSpriteArr = [],
            wordArr = [],
            groupArray = [];

        this.poolSlices.push(sprite);
        for (var k = 0; k < 5; k++) {
            var imgSprite = PIXI.Sprite.fromImage('uploads/images/' + spriteObj.image[k]);
            imageSpriteArr.push(imgSprite);
            wordArr.push(spriteObj.words[k]);
            groupArray.push(spriteObj.group[k]);
        }

        shuffle(imageSpriteArr, groupArray, wordArr);

        this.wallSlices.push({
            image: imageSpriteArr,
            group: groupArray,
            word: wordArr
        });
    }
};

/**
 * retrun objects to pool
 */
School_3_2.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];

    this.pool.returnSprite(sprite);

    this.poolSlices = [];
    this.wallSlices.shift();
};

/**
 * get next set on restart exercise
 */
School_3_2.prototype.getNewExerciseSet = function() {

    this.returnWallSprites();
    this.borrowWallSprites(1);

    this.exerciseContainer.removeChildren();

    this.currentBoxTop = 0;
    this.currentBoxBottom = 0;
    this.firstShelfGroup = [];
    this.secondShelfGroup = [];
    this.noOfBoxDragged = 0;
    this.newPositionTopX = 676;
    this.newPositionBottomX = 676;
    this.itemsInFirstShelf = [];
    this.itemsInSecondShelf = [];
    this.itemIndexInFirstShelf = [];
    this.itemIndexInSecondShelf = [];
    this.itemsInFirstShelf = [];
    this.itemsInSecondShelf = [];
    this.itemsDragged = [];

    this.greenItems = [];
    this.redItems = [];

    this.runExercise();
}

/**
 * reset exercise to default values
 * get new set
 */
School_3_2.prototype.restartExercise = function() {

    this.resetExercise();

    localStorage.setItem('school_3_2', 0);
    this.updateFeedback();
    this.introContainer.visible = true;
    this.introMode = true;

    this.getNewExerciseSet();
};

/**
 * the outro scene is displayed
 */
School_3_2.prototype.onComplete = function() {
    var self = this;

    this.introMode = false;

    this.help.interactive = false;
    this.help.buttonMode = false;
    this.animationContainer.visible = true;

    for (var k = 0; k < this.exerciseContainer.children.length; k++) {

        for (var j = 0; j < this.dragImage[k].children.length; j++) {
            this.dragImage[k].removeChild(this.dragImage[k].children[j]);
        }
    }

    this.ovaigen.click = this.ovaigen.tap = function() {
        self.cancelOutro();
        self.restartExercise();
    };

    this.outroAnimation();

}


/**
 * reset exercise to default values
 */
School_3_2.prototype.resetExercise = function() {
    var self = this;
    this.ovaigen.visible = false;
    self.kitchen_lady.visible = true;
    self.kitchen_lady_outro.visible = false;
    self.hands[0].visible = true;
    this.introMode = true;

};

/**
 * clean memory by clearing reference of object
 */
School_3_2.prototype.cleanMemory = function() {

    this.counter = null;

    if (this.wallSlices)
        this.wallSlices.length = 0;

    if (this.poolSlices)
        this.poolSlices.length = 0;

    this.pool = null;
    this.num = null;

    this.introContainer = null;
    this.feedbackContainer = null;
    this.animationContainer = null;
    this.currentBoxTop = null;
    this.currentBoxBottom = null;
    this.noOfBoxDragged = null;

    if (this.assetsToLoad)
        this.assetsToLoad.length = 0;

    if (this.buttonClicked)
        this.buttonClicked.length = 0;

    if (this.firstShelfGroup)
        this.firstShelfGroup.length = 0;

    if (this.secondShelfGroup)
        this.secondShelfGroup.length = 0;

    if (this.itemsInFirstShelf)
        this.itemsInFirstShelf.length = 0;

    if (this.itemsInSecondShelf)
        this.itemsInSecondShelf.length = 0;

    if (this.utencilPositions)
        this.utencilPositions.length = 0;

    if (this.boxPositions)
        this.boxPositions.length = 0;

    if (this.itemsDragged)
        this.itemsDragged.length = 0;

    if (this.redItems)
        this.redItems.length = 0;

    if (this.redItems)
        this.redItems.length = 0;

    if (this.dragImage)
        this.dragImage.length = 0;

    if (this.jsonData)
        this.jsonData.length = 0;

    this.common = null;
    this.sceneBackground = null;
    this.help = null;
    this.kitchen_lady = null;
    this.kitchen_table = null;
    this.kitchen_lady_outro = null;
    this.mouthParams = null;
    this.eyeParams = null;
    this.ovaigen = null;
    this.brownGraphics = null;
    this.brownRect = null;
    this.soundTexture = null;
    this.whiteGraphics = null;
    this.whiteRect = null;
    this.currentBoxTop = null;
    this.currentBoxBottom = null;

    this.sceneContainer.removeChildren();
    this.sceneContainer = null;
}

/**
 * all values same array property extend
 */
Array.prototype.AllValuesSame = function() {

    if (this.length > 0) {
        for (var i = 1; i < this.length; i++) {
            if (this[i] !== this[0])
                return false;
        }
    }
    return true;
}