function School_1_2() {
    cl.show();

    PIXI.DisplayObjectContainer.call(this);

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position.set(77, 88);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();

    localStorage.setItem('school_1_2', 0);
    main.CurrentExercise = "school_1_2";

    this.introMode = true;

    main.texturesToDestroy = [];

    this.loadSpriteSheet();

    // everything connected to this is rendered.
    this.addChild(this.sceneContainer);
}

School_1_2.constructor = School_1_2;
School_1_2.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
School_1_2.imagePath = 'build/exercises/school/school_1_2/images/';
School_1_2.audioPath = 'build/exercises/school/school_1_2/audios/';
School_1_2.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

/**
 * load array of assets of different format
 */
School_1_2.prototype.loadSpriteSheet = function() {

    this.introId = loadSound(School_1_2.audioPath + 'intro');
    this.outroId = loadSound(School_1_2.audioPath + 'outro');

    // create an array of assets to load
    this.assetsToLoad = [
        School_1_2.imagePath + "sprite_1_2.json",
        School_1_2.imagePath + "sprite_1_2.png",
        School_1_2.imagePath + "man_body_outro.png",

    ];
    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
School_1_2.prototype.spriteSheetLoaded = function() {

    // hide loader
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    // fill exercise background
    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0xcbe3c2);
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xf16c6c);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);

    // exercise instruction
    this.help = PIXI.Sprite.fromFrame("topcorner.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);

    this.loadExercise();
    this.introScene();

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.outroContainer);
}

/**
 * play intro animation
 */
School_1_2.prototype.introAnimation = function() {

    createjs.Sound.stop();

    if (!this.introMode)
        this.restartExercise();

    this.enableKeypad(false);

    this.help.interactive = false;
    this.help.buttonMode = false;

    main.overlay.visible = true;

    this.mouths.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
0-3 sec arm_1
3-7 sec arm_2
7-9 sec arm_pointingatkeypad
stop at arm_1
 */
School_1_2.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    // console.log(currentPostion)
    if (currentPostion >= 1000 && currentPostion < 5000) {
        this.showHandAt(1);
    } else if (currentPostion >= 5000 && currentPostion < 7500) {
        this.showHandAt(0);
    } else if (currentPostion >= 7500 && currentPostion < 10228) {
        this.showHandAt(2);
    } else {
        this.showHandAt(0);
    }
};

/**
 * cancel running intro animation
 */
School_1_2.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);

    this.mouths.hide(2);
    this.showHandAt(0);
    this.help.interactive = true;
    this.help.buttonMode = true;

    this.enableKeypad(true);

    this.ovaigen.visible = false;
    main.overlay.visible = false;
}

/**
 * outro animation play
 */
School_1_2.prototype.outroAnimation = function() {


    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.mouthOutro.show();
    }
    this.cancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.cancelHandler);
}

/**
 * cancel running outro animation
 */
School_1_2.prototype.cancelOutro = function() {

    var self = this;

    createjs.Sound.stop();
    self.mouthOutro.hide();
    self.ovaigen.visible = true;
    self.help.interactive = true;
    self.help.buttonMode = true;
    main.overlay.visible = false;

};

/**
 * create sprites,animation,sound related to intro scene
 */
School_1_2.prototype.introScene = function() {

    var self = this;

    this.barrier = new PIXI.Sprite.fromFrame('barrier.png');
    main.texturesToDestroy.push(this.barrier);
    this.sceneContainer.addChild(this.barrier);

    this.sceneContainer.addChild(this.help);

    this.animationContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.animationContainer);

    this.figure_intro = new PIXI.Sprite.fromFrame('man_body.png');
    this.figure_intro.position.set(1373, 304 - 140);
    main.texturesToDestroy.push(this.figure_intro);
    this.animationContainer.addChild(this.figure_intro);

    this.mouths = new Animation({
        "image": "mouth",
        "length": 4,
        "x": 1489,
        "y": 472 - 140,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 2, 4, 1],
        "frameId": 2
    });

    this.animationContainer.addChild(this.mouths);

    this.hands = [
        new PIXI.Sprite.fromFrame('arm1.png'),
        new PIXI.Sprite.fromFrame('arm2.png'),
        new PIXI.Sprite.fromFrame('arm3.png'),
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(1139, 422 - 140);
        this.animationContainer.addChild(this.hands[i]);
        this.hands[i].visible = false;
    };
    this.hands[0].visible = true;
    this.animationContainer.visible = true;

    this.outroScene();
}

/**
 * create sprites,animation,sound related to outro scene
 */
School_1_2.prototype.outroScene = function() {

    var self = this;

    this.outroContainer.visible = false;

    this.hangerTexture = PIXI.Texture.fromFrame('picturehanger.png');

    for (var i = 0; i < 3; i++) {
        this.hangerSprite = new PIXI.Sprite(this.hangerTexture);
        this.hangerSprite.position.set(this.params.position.hanger[i * 2], this.params.position.hanger[i * 2 + 1]);
        this.outroContainer.addChild(this.hangerSprite);
    };

    this.figure_outro = new PIXI.Sprite.fromImage(School_1_2.imagePath + "man_body_outro.png");
    this.figure_outro.position.set(999, 171);
    main.texturesToDestroy.push(this.figure_outro);
    this.outroContainer.addChild(this.figure_outro);

    this.mouthOutro = new Animation({
        "image": "mouthOutro",
        "length": 4,
        "x": 1537,
        "y": 367,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 3, 2, 4, 1],
        "frameId": 0
    });

    this.outroContainer.addChild(this.mouthOutro);

    // back to intro page
    this.ovaigen = PIXI.Sprite.fromFrame('ovaigen.png');
    this.ovaigen.position.set(563, 771);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.ovaigen.visible = false;
    this.outroContainer.addChild(this.ovaigen);

    this.hangerContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer.addChild(this.hangerContainer);
}

/**
 * load exercise related stuffs
 */
School_1_2.prototype.loadExercise = function() {

    this.keyboardContainer = new PIXI.DisplayObjectContainer();
    this.keyboardContainer.position = {
        x: 6,
        y: 896 - 40
    };
    this.sceneContainer.addChild(this.keyboardContainer);

    // position, size parameter for exercise
    this.params = {
        position: {
            hanger: [
                116, 159,
                517, 159,
                920, 159,
            ],
            white: [
                139, 225,
                540, 225,
                941, 225,
            ],
            image: [
                150, 236,
                552, 236,
                954, 236,

            ],
            miniWhite: [
                20, 519,
                441 + 10, 519,
                816 + 70, 519
            ],
            feedback: [1729, 1044],
            alphabets: [
                162, 910,
                257, 910,
                370, 910,
                473, 910,
                579, 910,
                686, 910,
                793, 910,
                907, 910,
                1022, 910,
                1131, 910,
                117, 998,
                235, 998,
                348, 998,
                474, 998,
                599, 998,
                715, 998,
                836, 998,
                956, 998,
                1071, 998,
                1187, 998,
                84, 1077,
                213, 1077,
                323, 1077,
                462, 1077,
                587, 1077,
                721, 1077,
                842, 1077,
                972, 1077,
                1097, 1077,
            ]
        },
        size: {
            white: {
                width: 233,
                height: 233
            },
            miniWhite: {
                width: 84,
                height: 84
            }
        },
        relativeSize: {
            feedback: 112,
            movesprite: [0, 0, 0, 40, 80],
        },
        letters: [
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'å', 'ä', 'ö',
        ]
    }

    this.whiteGraphics = createRect({
        w: this.params.size.white.width,
        h: this.params.size.white.height,
        color: 0xffffff,
        lineStyle: true,
        lineWidth: 2,
    });
    this.whiteTexture = this.whiteGraphics.generateTexture();

    this.redGraphics = createRect({
        alpha: 0.6,
        color: 0xff0000,
        w: this.params.size.miniWhite.width,
        h: this.params.size.miniWhite.height,
        lineStyle: true,
        lineWidth: 2,
    });
    this.redTexture = this.redGraphics.generateTexture();

    this.greenGraphics = createRect({
        alpha: 0.6,
        color: 0x00ff00,
        w: this.params.size.miniWhite.width,
        h: this.params.size.miniWhite.height,
        lineStyle: true,
        lineWidth: 2,
    });
    this.greenTexture = this.greenGraphics.generateTexture();

    this.generateKeyboard();

    this.feedbackContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.feedbackContainer);

    this.generateFeedback();

    this.soundTexture = new PIXI.Texture.fromFrame('sound_grey.png');

    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.exerciseContainer);

    this.markContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.markContainer);

    this.exerciseBaseContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.exerciseBaseContainer);

    this.markerGraphics = createRect({
        borderOnly: 'false',
        lineStyle: true,
        lineWidth: 10,
    });

    this.markerGraphics.width = this.params.size.miniWhite.width + 2;
    this.markerGraphics.height = this.params.size.miniWhite.height + 2;

    this.markerGraphics.visible = false;
    this.exerciseBaseContainer.addChild(this.markerGraphics);

    this.initObjectPooling();
    this.keyDownHandler = this.onKeyDown.bind(this);
    this.view.addEventListener("keydown", this.keyDownHandler, false);
}

/**
 * generate keypad using sprites and position
 */
School_1_2.prototype.generateKeyboard = function() {

    this.circleGraphics = createCircle({
        radius: 50,
        color: 0x616161
    });

    this.circleTexture = this.circleGraphics.generateTexture();

    this.keypad = new PIXI.Sprite.fromFrame('keyboard.png');
    main.texturesToDestroy.push(this.keypad);
    this.keyboardContainer.addChild(this.keypad);

    this.keypad = [];

    this.keyboardFirstRowStartPoint = [130, 0];
    this.keyboardFirstRowGap = 120;
    this.keyboardSecondRowStartPoint = [90, 95];
    this.keyboardSecondRowGap = 130;
    this.keyboardThirdRowStartPoint = [50, 182];
    this.keyboardThirdRowGap = 140;

    var alphabetsLen = this.params.letters.length + 1; //+1 for sudda
    for (var i = 0; i < alphabetsLen; i++) {
        var alphabetsSprite = new PIXI.Sprite(this.circleTexture);
        if (i >= 0 && i < 10) {
            alphabetsSprite.position.x = this.keyboardFirstRowStartPoint[0] + i * this.keyboardFirstRowGap;
            alphabetsSprite.position.y = this.keyboardFirstRowStartPoint[1];

        } else if (i >= 10 && i < 20) {
            alphabetsSprite.position.x = this.keyboardSecondRowStartPoint[0] + (i % 10) * this.keyboardSecondRowGap;
            alphabetsSprite.position.y = this.keyboardSecondRowStartPoint[1];

        } else if (i >= 20 && i < 30) {
            alphabetsSprite.position.x = this.keyboardThirdRowStartPoint[0] + (i % 20) * this.keyboardThirdRowGap;
            alphabetsSprite.position.y = this.keyboardThirdRowStartPoint[1];

        } else {
            alphabetsSprite.position.x = 0 + i * 10;
            alphabetsSprite.position.y = 0;
        }

        alphabetsSprite.alpha = 0;
        alphabetsSprite.interactive = true;
        alphabetsSprite.buttonMode = true;
        alphabetsSprite.index = i;
        this.keypad.push(alphabetsSprite);
        this.keyboardContainer.addChild(alphabetsSprite);
    };
};

/**
 * show hand at
 * @param  {int} number
 */
School_1_2.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    };
    if (isNumeric(number))
        this.hands[number].visible = true;
}

School_1_2.prototype.runExercise = function() {

    var self = this;

    this.jsonData = this.wallSlices[0];
    this.customButtons = {
        0: [],
        1: [],
        2: []
    };
    this.customSelection = false;
    this.customLetterIndex = 0;
    this.customCurrentIndex = 0;

    this.buttonsToEnable = {
        0: [],
        1: [],
        2: []
    }

    this.imageSpriteArr = [];

    this.textBoxPosition = [];
    this.dashLetterPosition = [];
    this.letterArr = [];

    this.pushedAnswerText = [];

    this.initialAnswer = this.jsonData.answer;
    console.log(this.initialAnswer);
    this.finalAnswer = [];
    this.answerStructure = [];
    this.matchIndex = [];
    this.noMatchIndex = [];

    //generate white sprite with image and sound
    for (var i = 0; i < 3; i++) {

        var words = this.jsonData.word[i].split(''),
            letters = this.jsonData.sentence[i].split(''),
            answers = this.jsonData.answer[i].split(''),
            whiteSprite = new PIXI.Sprite(this.whiteTexture),
            imageSprite = this.jsonData.image[i],
            soundSprite = new PIXI.Sprite(this.soundTexture);

        // imageSprite.width = 233 - 20;
        // imageSprite.height = 233 - 20;

        imageSprite.width = this.params.size.white.width,
            imageSprite.height = this.params.size.white.height,

            /*imageSprite.position.x = (whiteSprite.width - imageSprite.width) / 2;
            imageSprite.position.y = (whiteSprite.height - imageSprite.height) / 2;*/

            soundSprite.position.x = 10;
        soundSprite.position.y = 180;
        soundSprite.interactive = true;
        soundSprite.buttonMode = true;
        soundSprite.howl = this.jsonData.sound[i];

        whiteSprite.addChild(imageSprite);

        this.imageSpriteArr.push(imageSprite);

        whiteSprite.addChild(soundSprite);

        this.exerciseContainer.addChild(whiteSprite);
        whiteSprite.visible = false;

        this.generateSentenceWithDash(words, letters, i);

        whiteSprite.position.x = this.textBoxPosition[i] + this.params.relativeSize.movesprite[letters.length - 1];
        whiteSprite.position.y = this.params.position.white[i * 2 + 1];
        whiteSprite.visible = true;

        soundSprite.click = soundSprite.tap = this.onSoundPressed.bind(soundSprite);

    }

    this.navigateMarkerPos();

    for (var k = 0; k < 3; k++) {
        var boxButton = this.customButtons[k];

        for (var g = 0; g < boxButton.length; g++) {
            if (boxButton[g] != "") {
                boxButton[g].click = boxButton[g].tap = function() {

                    self.customSelection = true;
                    self.markerGraphics.visible = true;
                    self.markerGraphics.position = {
                        x: self.dashLetterPosition[this.blankIndex].x - 25,
                        y: self.dashLetterPosition[this.blankIndex].y - 11
                    }
                    self.customCurrentIndex = this.blankIndex;
                }
            }
        }
    }

    // key binding
    for (var i = 0; i < this.keypad.length; i++) {
        this.onKeypadHit(i);
    }

    this.keyPressHandler = this.onKeyPress.bind(this);
    this.view.addEventListener("keypress", this.keyPressHandler, false);
};

School_1_2.prototype.onSoundPressed = function() {
    createjs.Sound.stop();
    this.soundInstance = createjs.Sound.play(this.howl, {
        interrupt: createjs.Sound.INTERRUPT_NONE
    });
    this.soundInstance.addEventListener("failed", handleFailed);
    this.soundInstance.addEventListener("complete", handleComplete);
};


/**
 * generate sentence structure with dash
 * @param  {array} words
 * @param  {array} letters
 * @param  {int} index
 */
School_1_2.prototype.generateSentenceWithDash = function(words, letters, index) {
    var arr = new Array();
    var self = this;

    for (var j = 0; j < words.length; j++) {
        var letter = letters[j],
            word = words[j],
            textBox = new PIXI.Sprite(this.whiteTexture),
            letterText = new PIXI.Text("" + letter, {
                font: '50px Arial',
            });

        textBox.width = this.params.size.miniWhite.width;
        textBox.height = this.params.size.miniWhite.height;

        textBox.position.x = this.params.position.miniWhite[index * 2] + (j * textBox.width)

        textBox.position.y = this.params.position.miniWhite[index * 2 + 1];

        if (j === 0)
            this.textBoxPosition.push(textBox.position.x);

        letterText.position.x = textBox.position.x + 24;
        letterText.position.y = textBox.position.y + 14

        this.exerciseContainer.addChild(textBox);
        this.exerciseContainer.addChild(letterText);


        if (letter === '_') {
            self.buttonsToEnable[index].push(j);
            textBox.tint = "0xEAEAEA";
            letterText.visible = false;
            textBox.interactive = true;
            textBox.buttonMode = true;
            textBox.word = word;
            textBox.pos = letterText.position;

            this.dashLetterPosition.push(letterText.position);
            textBox.blankIndex = self.customLetterIndex;
            console.log("self.customLetterIndex = " + self.customLetterIndex);

            var redBox = new PIXI.Sprite(this.redTexture),
                greenBox = new PIXI.Sprite(this.greenTexture),
                wordText = new PIXI.Text("" + word, {
                    'font': '50px Arial'
                });

            greenBox.position = redBox.position = {
                x: textBox.position.x,
                y: textBox.position.y
            }

            var mText = new PIXI.Text("", {
                'font': '50px Arial'
            });
            mText.position = letterText.position;
            self.markContainer.addChild(mText);
            self.pushedAnswerText.push("");

            redBox.alpha = greenBox.alpha = 0.6;
            redBox.visible = false;
            greenBox.visible = false;

            wordText.position = letterText.position;
            wordText.visible = false;

            this.exerciseContainer.addChild(wordText);
            this.exerciseContainer.addChild(redBox);
            this.exerciseContainer.addChild(greenBox);
            arr.push({
                x: letterText.position.x,
                y: letterText.position.y,
                wrong: redBox,
                right: greenBox,
                word: wordText
            });

            self.customButtons[index].push(textBox);

            self.customLetterIndex++;
        } else {
            self.customButtons[index].push("");
        }
    };
    this.answerStructure.push(arr.length);
    this.letterArr.push(arr);
};

School_1_2.prototype.view = window;

/**
 * keydown listeners
 */
School_1_2.prototype.onKeyDown = function(e) {
    var code = e.charCode || e.keyCode;
    if (code === 8) {
        // keyIndex = self.keypad.length - 1;
        this.undoFeature();
        e.preventDefault();
    }
}

/**
 * perform on key press event
 */
School_1_2.prototype.onKeyPress = function(e) {

    if (addedListeners) return;

    var self = this;

    if (e.shiftKey) return false;

    var code = e.charCode || e.keyCode,
        character = String.fromCharCode(code),
        keyChar = character.toLowerCase(),
        keyIndex = self.params.letters.indexOf(keyChar);

    if (code === 8) {
        // keyIndex = self.keypad.length - 1;
        self.undoFeature();
        e.preventDefault();
    } else {

        self.keypad[keyIndex].alpha = 0.6;
        setTimeout(function() {
            self.keypad[keyIndex].alpha = 0;
        }, 100)

        if (isNumeric(keyIndex)) {
            self.onKeyClickTap(keyIndex);
        };
    }

}

/**
 * perform task on keypad on click/tap
 * @param  {integer} i
 */
School_1_2.prototype.onKeypadHit = function(i) {

    if (addedListeners) return;

    var self = this,
        key = this.keypad[i];

    key.click = key.tap = function(data) {
        if (this.index >= self.keypad.length - 1) {
            if (self.customSelection == true) return;
            self.undoFeature();
        } else {
            self.onKeyClickTap(this.index);
        }
    }

    key.mousedown = key.touchstart = function(data) {
        this.alpha = 0.6;
    }

    key.mouseup = key.mouseupoutside = key.touchend = key.touchendoutside = function(data) {
        this.alpha = 0;
    }

}

/**
 * push the tapped or clicked key and navigate to next marker position
 * evaluate if all empty index are type
 * @param  {[int]} index
 */
School_1_2.prototype.onKeyClickTap = function(index) {

    if (addedListeners) return;

    addedListeners = true;

    var answerText = new PIXI.Text("" + this.params.letters[index], {
        'font': '50px Arial',
        fill: 'black'
    });

    answerText.position.x = this.dashLetterPosition[this.customCurrentIndex].x;
    answerText.position.y = this.dashLetterPosition[this.customCurrentIndex].y;

    //since markContainer display object is prepopulated, we just need to update the index by removing item fromt he 'x' index and putting new on to same index.
    this.markContainer.removeChildAt(this.customCurrentIndex);
    this.markContainer.addChildAt(answerText, this.customCurrentIndex);
    this.pushedAnswerText[this.customCurrentIndex] = answerText.text;

    var chkIfPushedAll = cleanArray(this.pushedAnswerText);

    if (chkIfPushedAll.length === this.dashLetterPosition.length) { //check if typed letter array length and actual letter length is equal
        this.enableKeypad(false);
        this.evaluateRound();
    } else {
        addedListeners = false;
    }
    this.navigateMarkerPos(this.customCurrentIndex);
};

/*
 * Move marker position to the possible index.
 */
School_1_2.prototype.navigateMarkerPos = function(currentIndex) {

    //If user typed a letter and there is another blank text left, position marker to the next blank stuff
    if (this.markContainer.children[currentIndex + 1] !== undefined) {
        var indexPlsOneText = this.markContainer.children[currentIndex + 1].text;
        if (indexPlsOneText.replace(/(?:\r\n|\r|\n)/g, '').trim() == "") {
            this.customCurrentIndex = currentIndex + 1;

            this.markerGraphics.visible = true;
            this.markerGraphics.position = {
                x: this.dashLetterPosition[currentIndex + 1].x - 25,
                y: this.dashLetterPosition[currentIndex + 1].y - 11
            }
            return;
        }
    }
    for (var k = 0; k < this.markContainer.children.length; k++) {
        var txt = this.markContainer.children[k].text.replace(/(?:\r\n|\r|\n)/g, '').trim();
        if (txt == "") {
            this.customCurrentIndex = k;
            this.markerGraphics.visible = true;
            this.markerGraphics.position = {
                x: this.dashLetterPosition[k].x - 25,
                y: this.dashLetterPosition[k].y - 11
            };
            break;
        }

    }

};

/**
 * undo feature to remove typed key using delete key in custom keyboard
 */
School_1_2.prototype.undoFeature = function() {

    var newIndex = this.customCurrentIndex - 1;
    if (newIndex < 0) newIndex = 0;
    this.markContainer.children[newIndex].setText("");
    this.pushedAnswerText[newIndex] = "";
    this.customCurrentIndex = newIndex;
    this.navigateMarkerPos();
}

/**
 * reset sprite form red/green to white color
 */
School_1_2.prototype.resetHighlight = function() {
    for (var j = 0; j < this.answerStructure.length; j++) {
        for (var k = 0; k < this.answerStructure[j]; k++) {
            this.letterArr[j][k].right.visible = false;
            this.letterArr[j][k].wrong.visible = false;
        };
    };
    this.reInitExercise();
};


/**
 * reset exercise on evaluation complete to re-play for wrong position only
 */
School_1_2.prototype.reInitExercise = function() {
    this.reInit = true;

    this.markContainer.removeChildren();
    this.customCurrentIndex = 0;

    var cloneDashLetterPosition = this.dashLetterPosition.slice(0);

    var tempArr = [];

    for (var i = 0; i < this.answerStructure.length; i++) {
        var spliced = cloneDashLetterPosition.splice(0, this.answerStructure[i]);
        if (this.noMatchIndex.indexOf(i) > -1) {
            tempArr.push(spliced);
        };
    };


    for (var i = this.matchIndex.length - 1; i >= 0; i--) {
        this.letterArr.splice(this.matchIndex[i], 1);
        this.initialAnswer.splice(this.matchIndex[i], 1);
        this.answerStructure.splice(this.matchIndex[i], 1);
    };

    var newArr = []
    for (var i = 0; i < tempArr.length; i++) {
        for (var j = 0; j < tempArr[i].length; j++) {
            newArr.push(tempArr[i][j]);
        };
    };

    //CODE TO ENABLE REMAINING boxes so that students can click and answer them.
    var x = 0;
    for (var k = 0; k < 3; k++) {
        var enableButtons = this.buttonsToEnable[k];
        for (var c = 0; c < enableButtons.length; c++) {
            this.customButtons[k][enableButtons[c]].blankIndex = x;
            this.customButtons[k][enableButtons[c]].interactive = true;
            x++;
        }
    }


    this.dashLetterPosition = newArr.slice(0);
    for (var k = 0; k < this.dashLetterPosition.length; k++) {

        var t = new PIXI.Text("", {
            'font': '50px Arial'
        });
        this.markContainer.addChild(t);
        this.pushedAnswerText.push("");
    }

    cloneDashLetterPosition.length = 0;

    tempArr.length = 0;

    this.finalAnswer.length = 0;

    this.matchIndex.length = 0;
    this.noMatchIndex.length = 0;

    //this.keyhit = 0;
    this.enableKeypad(true)

    if (this.dashLetterPosition.length > 0) this.navigateMarkerPos();
    //this.navigateMarker();

    if (this.dashLetterPosition.length === 0) {
        correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
        setTimeout(this.correctFeedback.bind(this), 1000);
    };

    addedListeners = false;
};

/**
 * evaluate round by comparision with original answer with typed answer
 */
School_1_2.prototype.evaluateRound = function() {

    var self = this;

    this.enableKeypad(false);

    this.markerGraphics.visible = false;
    //create final answer to evaluate
    this.finalAnswer.length = 0;
    for (var i = 0; i < this.answerStructure.length; i++) {
        var slice = this.pushedAnswerText.splice(0, this.answerStructure[i]);
        var joinText = slice.join('').replace(/(?:\r\n|\r|\n)/g, '');
        this.finalAnswer.push(joinText);
        joinText = null;
    };

    for (var j = 0; j < this.finalAnswer.length; j++) {
        if (this.initialAnswer[j] === this.finalAnswer[j]) {
            this.matchIndex.push(j);
            for (var k = 0; k < this.finalAnswer[j].length; k++) {
                this.letterArr[j][k].right.visible = true;
                this.letterArr[j][k].word.visible = true;
                var chk = this.buttonsToEnable[j].indexOf(k);
                this.buttonsToEnable[j].splice(chk, 1);
                //this.customButtons[j].splice(chk, 1);
            };
        } else {
            this.noMatchIndex.push(j);
            wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
            for (var k = 0; k < this.finalAnswer[j].length; k++) {
                this.letterArr[j][k].wrong.visible = true;
            };
        }
    };

    for (var g = 0; g < 3; g++) { //set all boxes' interactive property to false. In method reinit, we'll enable interactivity of only remaining boxes.
        var cButton = this.customButtons[g];
        for (var m = 0; m < cButton.length; m++) {
            if (cButton[m] != "") {
                cButton[m].interactive = false;
            }
        }
    }

    setTimeout(this.resetHighlight.bind(this), 1000);

}

/**
 * toggle keypad enable/disable
 */
School_1_2.prototype.enableKeypad = function(bool) {

    if (bool) {
        for (var i = 0; i < this.keypad.length; i++) {
            this.keypad[i].interactive = true;
            this.keypad[i].buttonMode = true;
        }
        this.markerGraphics.visible = true;
    } else {
        for (var i = 0; i < this.keypad.length; i++) {
            this.keypad[i].interactive = false;
            this.keypad[i].buttonMode = false;
        }
        this.markerGraphics.visible = false;
    }


}

/**
 * generate output feedback
 */
School_1_2.prototype.generateFeedback = function(i) {

    this.hiddenFeedback = [];
    this.feedbackTexture = PIXI.Texture.fromFrame('feedback.png');

    for (var i = 0; i < 10; i++) {
        var feedbackSprite = new PIXI.Sprite(this.feedbackTexture);
        feedbackSprite.position = {
            x: this.params.position.feedback[0],
            y: this.params.position.feedback[1] - (i * this.params.relativeSize.feedback),
        }
        feedbackSprite.alpha = 0.2;
        this.feedbackContainer.addChild(feedbackSprite);
        this.hiddenFeedback.push(feedbackSprite);
    };
}

/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
School_1_2.prototype.correctFeedback = function() {

    this.enableKeypad(true);

    var feedback = localStorage.getItem('school_1_2');
    this.hiddenFeedback[feedback].alpha = 1;
    feedback++;
    localStorage.setItem('school_1_2', feedback);
    this.customCurrentIndex = 0;

    if (feedback >= School_1_2.totalFeedback) {
        setTimeout(this.onComplete.bind(this), 1000);
    } else {
        this.view.removeEventListener("keypress", this.keyPressHandler, false);
        this.getNewExerciseSet();
    }

}

/**
 * prefetch exercise data and cache number of sets defined only.
 */
School_1_2.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/school/school_1_2.json?nocache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        self.pool = new ExerciseSpritesPool(loader.json);
        //borrow two sprites once
        self.borrowWallSprites(2);
        self.runExercise();

    });
    loader.load();
}

/**
 * borrow object from pool
 * cache sprites and load audios on demand
 * @param  {int} num
 */
School_1_2.prototype.borrowWallSprites = function(num) {

    var self = this;

    for (var i = 0; i < num; i++) {

        var sprite = this.pool.borrowSprite(),
            spriteObj = sprite[0],
            wordArr = [],
            imageArr = [],
            soundArr = [],
            sentenceArr = [],
            answerArr = [];

        this.poolSlices.push(sprite);


        for (var j = 0; j < 3; j++) {
            var imageSprite = PIXI.Sprite.fromImage('uploads/images/' + spriteObj.image[j]);
            imageArr.push(imageSprite);

            var soundHowl = loadSound('uploads/audios/' + StripExtFromFile(spriteObj.sound[j]));

            soundArr.push(soundHowl);

            wordArr.push(spriteObj.word[j]);
            sentenceArr.push(spriteObj.sentence[j]);
            answerArr.push(spriteObj.answer[j]);
        };

        shuffle(imageArr, wordArr, soundArr, sentenceArr, answerArr);

        this.wallSlices.push({
            word: wordArr,
            image: imageArr,
            sound: soundArr,
            answer: answerArr,
            sentence: sentenceArr,
        });
    }
};

/**
 * return object to pool so it can be reused later if needed
 */
School_1_2.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];

    this.pool.returnSprite(sprite);

    this.poolSlices = [];
    this.wallSlices.shift();
};

/**
 * get new exercise sets
 */
School_1_2.prototype.getNewExerciseSet = function() {
    this.exerciseContainer.removeChildren();

    this.customButtons = {
        0: [],
        1: [],
        2: []
    };

    this.buttonsToEnable = {
        0: [],
        1: [],
        2: []
    };

    this.customSelection = false;
    this.customLetterIndex = 0;
    this.customCurrentIndex = 0;
    //  this.keyhit = 0;

    this.returnWallSprites();
    this.borrowWallSprites(1);
    this.runExercise();
}

/**
 * reset exercise and get next set
 */
School_1_2.prototype.restartExercise = function() {
    this.resetExercise();

    this.feedbackContainer.removeChildren();
    this.markContainer.removeChildren();
    this.hangerContainer.removeChildren();

    this.animationContainer.visible = true;

    localStorage.setItem('school_1_2', 0);
    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    this.introMode = true;

    this.getNewExerciseSet();

    this.hiddenFeedback.length = 0;
    this.generateFeedback();
};

/**
 * the outro scene is displayed
 */
School_1_2.prototype.onComplete = function() {

    var self = this;

    this.introMode = false;

    this.help.interactive = false;
    this.help.buttonMode = false;
    this.keyboardContainer.visible = false;
    this.introContainer.visible = false;
    this.outroContainer.visible = true;
    var xPos = [139, 540, 941];
    for (var i = 0; i < this.imageSpriteArr.length; i++) {
        /* this.imageSpriteArr[i].position.x = this.params.position.image[i * 2] - 16;
         this.imageSpriteArr[i].position.y = this.params.position.image[i * 2 + 1];*/

        this.imageSpriteArr[i].position.x = xPos[i] + (233 - this.imageSpriteArr[i].width) / 2 - 5;
        this.imageSpriteArr[i].position.y = 225 + (233 - this.imageSpriteArr[i].height) / 2 - 3;
        this.hangerContainer.addChild(this.imageSpriteArr[i]);
    };

    this.ovaigen.click = this.ovaigen.tap = function(data) {

        self.cancelOutro();
        self.restartExercise();
    }

    this.outroAnimation();

}

/**
 * reset exercise to default values
 */
School_1_2.prototype.resetExercise = function() {
    this.ovaigen.visible = false;
    this.outroContainer.visible = false;
    this.keyboardContainer.visible = true;
    this.introContainer.visible = true;
    this.introMode = true;
};

/**
 * clean memory by clearing reference of object
 */
School_1_2.prototype.cleanMemory = function() {

    this.introMode = null;
    this.introId = null;
    this.outroId = null;

    if (this.assetsToLoad)
        this.assetsToLoad.length = 0;

    if (this.sceneBackground) this.sceneBackground.clear();
    this.sceneBackground = null;

    this.help = null;

    this.introInstance = null;

    this.timerId = null;

    this.introCancelHandler = null;
    this.cancelHandler = null;

    this.barrier = null;
    this.figure_intro = null;
    this.figure_outro = null;

    this.mouths = null;

    if (this.hands) this.hands.length = 0;

    this.hangerTexture = null;
    this.hangerSprite = null;

    this.mouthOutro = null;

    this.ovaigen = null;

    this.hangerContainer.removeChildren();
    this.hangerContainer = null;

    this.keyboardContainer.removeChildren()
    this.keyboardContainer = null;

    wipe(this.params);
    this.params = null;

    if (this.whiteGraphics) this.whiteGraphics.clear();
    this.whiteGraphics = null;

    this.whiteTexture = null;

    if (this.redGraphics) this.redGraphics.clear();
    this.redGraphics = null;

    this.redTexture = null;

    if (this.greenGraphics) this.greenGraphics.clear();
    this.greenGraphics = null;

    this.greenTexture = null;

    this.soundTexture = null;

    this.markContainer.removeChildren();
    this.markContainer = null;

    if (this.markerGraphics) this.markerGraphics.clear();
    this.markerGraphics = null;

    this.keyDownHandler = null;

    if (this.circleGraphics) this.circleGraphics.clear();
    this.circleGraphics = null;

    this.circleTexture = null;

    this.keypad = null;

    if (this.keyboardFirstRowStartPoint) this.keyboardFirstRowStartPoint.length = 0;
    this.keyboardFirstRowGap = null;
    if (this.keyboardSecondRowStartPoint) this.keyboardSecondRowStartPoint.length = 0;
    this.keyboardSecondRowGap = null;
    if (this.keyboardThirdRowStartPoint) this.keyboardThirdRowStartPoint.length = 0;
    this.keyboardThirdRowGap = null;

    this.jsonData = null;
    wipe(this.customButtons);
    this.customButtons = null;

    this.customSelection = null;
    this.customCurrentIndex = null;

    wipe(this.buttonsToEnable);
    this.buttonsToEnable = null;

    if (this.imageSpriteArr) this.imageSpriteArr.length = 0;
    if (this.textBoxPosition) this.textBoxPosition.length = 0;
    if (this.dashLetterPosition) this.dashLetterPosition.length = 0;
    if (this.letterArr) this.letterArr.length = 0;
    if (this.pushedAnswerText) this.pushedAnswerText.length = 0;

    this.initialAnswer = null;

    if (this.finalAnswer) this.finalAnswer.length = 0;
    if (this.answerStructure) this.answerStructure.length = 0;
    if (this.matchIndex) this.matchIndex.length = 0;
    if (this.noMatchIndex) this.noMatchIndex.length = 0;

    this.keyPressHandler = null;

    this.soundInstance = null;

    this.customCurrentIndex = null;

    if (this.hiddenFeedback) this.hiddenFeedback.length = 0;
    this.feedbackTexture = null;

    if (this.wallSlices) this.wallSlices.length = 0;
    if (this.poolSlices) this.poolSlices.length = 0;

    wipe(this.pool);
    this.pool = null;

    this.customLetterIndex = null;

    this.exerciseBaseContainer.removeChildren();
    this.exerciseContainer.removeChildren();
    this.feedbackContainer.removeChildren();
    this.animationContainer.removeChildren();
    this.outroContainer.removeChildren();
    this.introContainer.removeChildren();
    this.sceneContainer.removeChildren();

    this.exerciseBaseContainer = null;
    this.exerciseContainer = null;
    this.feedbackContainer = null;
    this.animationContainer = null;
    this.outroContainer = null;
    this.introContainer = null;
    this.sceneContainer = null;

}

function cleanArray(actual) {
    var newArray = new Array();
    for (var i = 0; i < actual.length; i++) {
        if (actual[i]) {
            newArray.push(actual[i]);
        }
    }
    return newArray;
}