function School_4_2() {
    cl.show()

    PIXI.DisplayObjectContainer.call(this);

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position.set(77, 88);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();
    this.feedbackContainer = new PIXI.DisplayObjectContainer();

    localStorage.setItem('school_4_2', 0);
    main.CurrentExercise = "school_4_2";

    main.texturesToDestroy = [];

    this.common = new Common();
    this.loadSpriteSheet();
    this.folderLabels = ["kr", "gr", "pr", "br", "dr", "tr"];
    this.folderButton = [];

    // everything connected to this is rendered.
    this.addChild(this.sceneContainer);
}

School_4_2.constructor = School_4_2;
School_4_2.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
School_4_2.imagePath = 'build/exercises/school/school_4_2/images/';
School_4_2.audioPath = 'build/exercises/school/school_4_2/audios/';
School_4_2.totalFeedback = Main.TOTAL_FEEDBACK; // default 10

/**
 * load array of assets of different format
 */
School_4_2.prototype.loadSpriteSheet = function() {
    this.introId = loadSound(School_4_2.audioPath + 'intro');
    this.outroId = loadSound(School_4_2.audioPath + 'outro');

    // create an array of assets to load
    this.assetsToLoad = [
        School_4_2.imagePath + "sprite_school_4_2.json",
        School_4_2.imagePath + "sprite_school_4_2.png",
        School_4_2.imagePath + "monitor.png"
    ];
    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
School_4_2.prototype.spriteSheetLoaded = function() {

    // hide loader
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    // fill exercise background
    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0xcbe3c2);
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xec6567);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);

    // exercise instruction
    this.help = PIXI.Sprite.fromFrame("topcorner.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);
    this.sceneContainer.addChild(this.help);

    this.loadExercise();
    this.introScene();
    this.outroScene();

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.outroContainer);
    this.introContainer.addChild(this.feedbackContainer);
}

/**
 * hand visibility
 * @param  {int} number
 */
School_4_2.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    };
    if (isNumeric(number))
        this.hands[number].visible = true;
}

/**
 * play intro animation
 */
School_4_2.prototype.introAnimation = function() {

    createjs.Sound.stop();

    if (!this.introMode)
        this.restartExercise();

    this.help.interactive = false;
    this.help.buttonMode = false;

    main.overlay.visible = true;

    this.animationContainer.visible = true;
    this.eyeIntro.show();
    this.mouthIntro.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
0-2 sec arm_1
2-5 sec arm_holdingstick (arm_3)
5-10 sec arm_2
stop at arm_1
 */
School_4_2.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    if (currentPostion >= 2000 && currentPostion < 5000) {
        this.showHandAt(0);
    } else if (currentPostion >= 5000 && currentPostion < 10000) {
        this.showHandAt(1);
    } else {
        this.showHandAt(0);
    }
};

/**
 * cancel running intro animation
 */
School_4_2.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);

    this.mouthIntro.hide();
    this.animationContainer.visible = false;
    this.help.interactive = true;
    this.help.buttonMode = true;

    this.ovaigen.visible = false;
    main.overlay.visible = false;
}

/**
 * play outro animation
 */
School_4_2.prototype.outroAnimation = function() {

    var self = this;

    main.overlay.visible = true;

    self.mouthOutro.show();
    self.eyeOutro.show();

    this.outroInstance = createjs.Sound.play(this.outroId);

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateOutro.bind(this), 1);

    this.outroCancelHanlder = this.cancelOutro.bind(this);
    this.outroInstance.addEventListener("complete", this.outroCancelHanlder);
}

/**
 * cue points to animate outro
0-6 sec arm_1
6-stop arm_2
 */
School_4_2.prototype.animateOutro = function() {

    var currentPostion = this.outroInstance.getPosition();

    if (currentPostion >= 6000) {
        this.ovaigen.visible = true;
        this.outroHands[1].visible = true;
        this.outroHands[0].visible = false;

    } else {
        this.outroHands[1].visible = false;
        this.outroHands[0].visible = true;
    }

};

/**
 * cancel outro animation
 */
School_4_2.prototype.cancelOutro = function() {

    var self = this;

    clearInterval(this.timerId);

    this.outroHands[1].visible = false;
    this.outroHands[0].visible = true;
    self.ovaigen.visible = true;
    createjs.Sound.stop();
    self.mouthOutro.hide();
    self.help.interactive = true;
    self.help.buttonMode = true;
    main.overlay.visible = false;

};

/**
 * create sprites,animation,sound related to intro scene
 */
School_4_2.prototype.introScene = function() {

    var self = this;

    this.animationContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.animationContainer);

    this.figure_intro = new PIXI.Sprite.fromFrame('librarian.png');
    this.figure_intro.position.set(3, 214);
    main.texturesToDestroy.push(this.figure_intro);
    this.animationContainer.addChild(this.figure_intro);

    this.mouthIntro = new Animation({
        "image": "mouth_",
        "length": 4,
        "x": 230,
        "y": 546,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 2, 4, 1, 3],
        "frameId": 2
    });

    this.animationContainer.addChild(this.mouthIntro);

    this.eyeIntro = new Animation({
        "image": "eyes_",
        "length": 2,
        "x": 204,
        "y": 399,
        "speed": 2,
        "pattern": true,
        "sequence": [1, 2, 2, 1, 1, 2, 2, 1, 2, 1, 1, 2],
        "frameId": 2
    });

    this.animationContainer.addChild(this.eyeIntro);

    this.hands = [
        new PIXI.Sprite.fromFrame('arm1.png'),
        new PIXI.Sprite.fromFrame('arm2.png'),
        new PIXI.Sprite.fromFrame('arm3.png'),
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(215, 561);
        this.animationContainer.addChild(this.hands[i]);
        this.hands[i].visible = false;
    };
    this.animationContainer.visible = false;
}

/**
 * create sprites,animation,sound related to outro scene
 */
School_4_2.prototype.outroScene = function() {

    var self = this;

    this.outroContainer.visible = false;

    this.figure_outro = new PIXI.Sprite.fromFrame('librarianoutr.png');
    this.figure_outro.position.set(790, 180);
    main.texturesToDestroy.push(this.figure_outro);
    this.outroContainer.addChild(this.figure_outro);

    this.mouthOutro = new Animation({
        "image": "mouth_outro_",
        "length": 4,
        "x": 992,
        "y": 414,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 3, 2, 4, 1],
        "frameId": 0
    });

    this.outroContainer.addChild(this.mouthOutro);

    this.eyeOutro = new Animation({
        "image": "eyes_outro_",
        "length": 2,
        "x": 969,
        "y": 313,
        "speed": 2,
        "pattern": true,
        "sequence": [1, 2, 1, 1, 2, 2, 1, 2, 1, 1, 2],
        "frameId": 2
    });

    this.outroContainer.addChild(this.eyeOutro);

    this.outroHands = [
        new PIXI.Sprite.fromFrame('arm1outro.png'),
        new PIXI.Sprite.fromFrame('arm2outro.png'),
    ];
    for (var i = 0; i < this.outroHands.length; i++) {
        this.outroHands[i].position.set(710, 466);
        this.outroContainer.addChild(this.outroHands[i]);
    };
    this.outroHands[0].position.set(710, 466);
    this.outroHands[1].position.set(572, 388);
    this.outroHands[1].visible = false;

    // back to intro page
    this.ovaigen = PIXI.Sprite.fromFrame('ovaigen.png');
    this.ovaigen.position.set(420, 295);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.ovaigen.visible = false;
    this.outroContainer.addChild(this.ovaigen);
}

/**
 * load exercise related stuffs
 */
School_4_2.prototype.loadExercise = function() {
    this.folderTexture = PIXI.Texture.fromFrame("folder.png");

    this.computerDesk = new PIXI.Graphics();
    this.computerDesk.beginFill(0x7e4f25);
    this.computerDesk.drawRect(3, 485, 1837, 715);
    this.sceneContainer.addChild(this.computerDesk);

    this.monitor = new PIXI.Sprite.fromImage(School_4_2.imagePath + "monitor.png");
    this.monitor.position = {
        x: 161,
        y: 53
    };
    main.texturesToDestroy.push(this.monitor);
    this.sceneContainer.addChild(this.monitor);


    this.pendrive = new PIXI.Sprite.fromFrame("pendrive.png");
    this.pendrive.position = {
        x: 1421,
        y: 712
    };
    main.texturesToDestroy.push(this.pendrive);
    this.introContainer.addChild(this.pendrive);


    //render folders
    this.folderPositions = [
        266, 135,
        266, 367,
        523, 571,
        903, 571,
        1093, 385,
        1093, 153
    ];

    for (var k = 0; k < 6; k++) {

        var folderSprite = new PIXI.Sprite(this.folderTexture);

        folderSprite.position = {
            x: this.folderPositions[k * 2],
            y: this.folderPositions[k * 2 + 1]
        };
        this.introContainer.addChild(folderSprite);
        var flderLabel = new PIXI.Text(this.folderLabels[k], {
            font: "80px Arial"
        });
        flderLabel.position = {
            x: 70,
            y: 50
        };
        folderSprite.interactive = true;
        folderSprite.buttonMode = true;
        folderSprite.folderName = this.folderLabels[k];
        folderSprite.x = this.folderPositions[k * 2];
        folderSprite.y = this.folderPositions[k * 2 + 1];
        folderSprite.hitIndex = k;
        folderSprite.addChild(flderLabel);
        this.folderButton.push(folderSprite);
    }


    //render green and red feedback folders
    this.redFolder = new PIXI.Sprite.fromFrame("folder_red.png");
    this.greenFolder = new PIXI.Sprite.fromFrame("folder_green.png");
    this.redFolder.visible = false;
    this.greenFolder.visible = false;
    this.introContainer.addChild(this.redFolder);
    this.introContainer.addChild(this.greenFolder);

    this.soundTexture = PIXI.Texture.fromFrame("sound_grey.png");
    main.texturesToDestroy.push(this.redFolder);
    main.texturesToDestroy.push(this.greenFolder);

    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.exerciseContainer);

    this.createFeedBack();
    this.initObjectPooling();

}

/**
 * Render feedback to the container
 */
School_4_2.prototype.createFeedBack = function() {
    var self = this;
    var feedback = new PIXI.Texture.fromFrame("fb.png");
    for (var k = 0; k < 10; k++) {
        var feedbackSprite = new PIXI.Sprite(feedback);

        feedbackSprite.position = {
            x: 1482 + (k * 24),
            y: 759
        };
        main.texturesToDestroy.push(feedbackSprite);
        this.feedbackContainer.addChild(feedbackSprite);
        feedbackSprite.alpha = 0.1;
    }
};

/**
 * manage exercise with new sets randomized
 */
School_4_2.prototype.runExercise = function() {

    var self = this;

    this.jsonData = this.wallSlices[0];

    var whiteRectRaw = createRect({
        w: 360,
        h: 360,
        color: 0xffffff,
        lineStyle: 1,
        lineWidth: 2
    });
    var whiteRectTexture = whiteRectRaw.generateTexture();
    this.whiteRect = new PIXI.Sprite(whiteRectTexture);
    main.texturesToDestroy.push(this.whiteRect);

    this.whiteRect.position = {
        x: 609,
        y: 175
    };

    this.exerciseContainer.addChild(this.whiteRect);

    this.imageSprite = this.jsonData.image;
    this.imageSprite.width = this.imageSprite.height = 360;
    /*this.imageSprite.position = {
        x: 20,
        y: 20
    };
*/
    this.whiteRect.addChild(this.imageSprite);

    this.sound = new PIXI.Sprite(this.soundTexture);
    this.sound.interactive = true;
    this.sound.buttonMode = true;
    this.sound.position = {
        x: 2,
        y: 300
    };
    this.sound.howl = this.jsonData.sound;

    this.sound.click = this.sound.tap = this.onSoundPressed.bind(this.sound);

    this.whiteRect.addChild(this.sound);

    for (var k = 0; k < 6; k++) {
        this.folderButton[k].click = this.folderButton[k].tap = function(data) {
            if (addedListeners) return false;
            addedListeners = true;
            var d = this;
            if (this.folderName == self.jsonData.answer) {
                self.common.showRedOrGreenBox(this.x, this.y, self.greenFolder, 600);
                correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                setTimeout(function() {
                    self.sendImageIntoFolder(d.x, d.y, self.whiteRect);
                }, 800);
            } else {
                self.common.showRedOrGreenBox(this.x, this.y, self.redFolder, 600);
                wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                self.wrongFeedback();
            }

        }
    }



}


School_4_2.prototype.onSoundPressed = function() {
    this.soundInstance = createjs.Sound.play(this.howl, {
        interrupt: createjs.Sound.INTERRUPT_NONE
    });
    this.soundInstance.addEventListener("failed", handleFailed);
    this.soundInstance.addEventListener("complete", handleComplete);
};


/*
When user selects correct folder we have to animate image block into the correct folder.
*/
School_4_2.prototype.sendImageIntoFolder = function(xPos, yPos, obj) {
    // body...
    var self = this;
    /*obj.position = {
        x: xPos + 50,
        y: yPos + 50
    };*/
    xPos += 50;
    yPos += 50;


    this.animateFolder = true;
    //animate image block inside the folder
    requestAnimFrame(moveFolder);

    this.initialDimension = 360;
    this.alphaVal = 1;

    var startPosX = obj.position.x;
    var startPosY = obj.position.y;

    function moveFolder() {


        if (self.animateFolder == true) {
            obj.height = self.initialDimension;
            obj.width = self.initialDimension;
            obj.alpha = self.alphaVal;

            //we don't always need to loop the animation, once image width/height reaches 0px in size then break the loop
            obj.position.x = obj.position.x + (xPos - startPosX) / 20;
            obj.position.y = obj.position.y + (yPos - startPosY) / 20;

            if (parseInt(obj.height) <= 80 || parseInt(obj.width) <= 80) {
                self.exerciseContainer.removeChildAt(0);
                self.correctFeedback();
                self.animateFolder = false;
                self.initialDimension = 0
                obj.alpha = 0;
            }
            requestAnimFrame(moveFolder);
            self.initialDimension -= 15;
            self.alphaVal -= 0.03;
        }

    }
};

/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
School_4_2.prototype.correctFeedback = function() {
    var feedback = localStorage.getItem('school_4_2');
    feedback++;

    localStorage.setItem('school_4_2', feedback);
    this.updateFeedback();

    if (feedback >= School_4_2.totalFeedback) {
        setTimeout(this.onComplete.bind(this), 1000);
    } else {
        this.getNewExerciseSet();
    }
}

/**
 * on wrong anwer, feedback decrement
 */
School_4_2.prototype.wrongFeedback = function() {
    var feedback = localStorage.getItem('school_4_2');
    feedback--;

    if (feedback < 0)
        feedback = 0;

    localStorage.setItem('school_4_2', feedback);
    this.updateFeedback();

}


/**
 * update feedback
 */
School_4_2.prototype.updateFeedback = function() {

    var self = this;
    addedListeners = false;
    var counter = localStorage.getItem('school_4_2');

    for (var i = 0; i < self.feedbackContainer.children.length; i++) {
        self.feedbackContainer.children[i].alpha = 0.1
    }

    for (var j = 0; j < counter; j++) {
        self.feedbackContainer.children[j].alpha = 1;
    }
}

/**
 * prefetch exercise data and cache number of sets defined only.
 */
School_4_2.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/school/school_4_2.json?nocache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        self.pool = new ExerciseSpritesPool(loader.json);
        //borrow two sprites once
        self.generatePoolCategory();
        self.borrowWallSprites(2);
        self.runExercise();

    });
    loader.load();
}

School_4_2.prototype.generatePoolCategory = function() {

        var keyArr = [];

        var spriteArr = this.pool.sprites,
            spriteLen = spriteArr.length;

        for (var i = 0; i < spriteLen; i++) {
            keyArr.push(spriteArr[i][0].word[0]);
        };

        this.poolCategory = keyArr.filter(onlyUnique);

    }
    /**
     * borrow objects from pool
     * @param  {int} num
     */
School_4_2.prototype.borrowWallSprites = function(num) {
    var rand, category, sprite, spriteObj;

    var self = this;

    for (var i = 0; i < num; i++) {

        rand = makeUniqueRandom(this.poolCategory.length);
        this.borrowCategory = this.poolCategory[rand];

        sprite = this.pool.borrowUniqueSprite(this.borrowCategory, this.lastBorrowSprite);
        spriteObj = sprite[0];

        var imageSprite,
            soundHowl;

        this.poolSlices.push(sprite);

        this.lastBorrowSprite = spriteObj.word[0];

        imageSprite = PIXI.Sprite.fromImage('uploads/images/' + spriteObj.image);

        soundHowl = loadSound('uploads/audios/' + StripExtFromFile(spriteObj.sound));

        this.wallSlices.push({
            image: imageSprite,
            sound: soundHowl,
            answer: spriteObj.answer
        });
    }
};

/**
 * return objects back to pool
 */
School_4_2.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];

    this.pool.returnSprite(sprite);

    this.poolSlices = [];
    this.wallSlices.shift();
};

/**
 * get new exercise set
 */
School_4_2.prototype.getNewExerciseSet = function() {

    this.returnWallSprites();
    this.borrowWallSprites(1);
    this.runExercise();
}

/**
 * restart exercise
 */
School_4_2.prototype.restartExercise = function() {
    this.resetExercise();

    localStorage.setItem('school_4_2', 0);
    this.updateFeedback();
    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    this.introMode = true;

    this.getNewExerciseSet();
};

/**
 * the outro scene is displayed
 */
School_4_2.prototype.onComplete = function() {
    var self = this;

    this.introMode = false;

    this.help.interactive = false;
    this.help.buttonMode = false;
    this.introContainer.visible = false;
    this.outroContainer.visible = true;

    var b = setTimeout(function() {
        self.ovaigen.visible = true;
    }, 5500);

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        self.cancelOutro();
        self.restartExercise();
    }

    this.outroAnimation();

}

/**
 * reset exercise
 */
School_4_2.prototype.resetExercise = function() {
    this.ovaigen.visible = false;
    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    this.introMode = true;

};

/**
 * clean memory by clearing reference of object
 */
School_4_2.prototype.cleanMemory = function() {

    if (this.assetsToLoad)
        this.assetsToLoad.length = 0;

    if (this.sceneBackground)
        this.sceneBackground.clear();

    this.sceneBackground = null;
    this.help = null;

    this.figure_intro = null;
    this.mouthIntro = null;


    if (this.figure_outro)
        this.figure_outro = null;

    this.ovaigen = null;

    if (this.jsonData)
        this.jsonData.length = 0;

    this.counter = null;

    if (this.wallSlices)
        this.wallSlices.length = 0;

    if (this.poolSlices)
        this.poolSlices.length = 0;

    this.pool = null;
    this.num = null;

    this.introContainer = null;
    this.outroContainer = null;
    this.feedbackContainer = null;
    this.animationContainer = null;

    this.common = null;

    if (this.folderLabels)
        this.folderLabels.length = 0;

    if (this.folderButton)
        this.folderButton.length = 0;

    this.eyeIntro = null;
    this.figure_outro = null;
    this.mouthOutro = null;
    this.eyeOutro = null;
    this.folderTexture = null;
    this.computerDesk = null;
    this.monitor = null;
    this.pendrive = null;
    this.folderPositions = null;
    this.redFolder = null;
    this.greenFolder = null;
    this.soundTexture = null;
    this.exerciseContainer = null;
    this.whiteRect = null;
    this.sound = null;
    this.animateFolder = null;
    this.initialDimension = null;

    this.sceneContainer.removeChildren();
    this.sceneContainer = null;

}