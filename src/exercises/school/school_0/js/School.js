function School() {

	cl.show();

	PIXI.DisplayObjectContainer.call(this);

	this.tivoliContainer = new PIXI.DisplayObjectContainer();

	this.sceneContainer = new PIXI.DisplayObjectContainer();

	this.imageContainer = new PIXI.DisplayObjectContainer();

	this.menuContainer = new PIXI.DisplayObjectContainer();

	this.tivoliContainer.addChild(this.sceneContainer);
	this.tivoliContainer.addChild(this.menuContainer);

	//resize container
	this.imageContainer.position = {
		x: 0,
		y: -20
	}

	this.menuContainer.position = {
		x: 0,
		y: 1287 - 36
	}

	this.loadSpriteSheet();

	this.addChild(this.tivoliContainer);
}

School.constructor = School;
School.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

School.IMAGEPATH = "build/exercises/school/school_0/images/";
School.COMMONFOLDER = 'build/common/images/school/';

School.prototype.loadSpriteSheet = function() {

	// create an array of assets to load
	var assetsToLoad = [
		School.IMAGEPATH + "school_0.png",
		School.COMMONFOLDER + 'menuskeleton.png',
		School.COMMONFOLDER + 'sprite_school.json',
		School.COMMONFOLDER + 'sprite_school.png',
	];
	// create a new loader
	loader = new PIXI.AssetLoader(assetsToLoad);

	// use callback
	loader.onComplete = this.spriteSheetLoaded.bind(this);

	//begin load
	loader.load();

}

School.prototype.spriteSheetLoaded = function() {

	cl.hide();

	// get backgroudn for tivoli page
	this.bkg1 = new Background(School.IMAGEPATH + "school_0.png");
	this.sceneContainer.addChild(this.bkg1);

	this.loadMenus();

	this.imageButtons();

}

School.prototype.homePage = function() {
	UnloadScene.apply(this);
	localStorage.removeItem('exercise');
	// createjs.Sound.removeAllSounds();
	history.back();
}

School.prototype.sceneChange = function(scene) {

	var prevExercise = localStorage.getItem('exercise');

	if (scene === prevExercise)
		return false;

	UnloadScene.apply(this);

	main.CurrentExercise = scene;

	localStorage.setItem('exercise', scene);

	main.page = new window[scene];
	main.page.introMode = true;

	loadSound('build/common/audios/' + main.CurrentExercise + "_help");

	if (this.sceneContainer.stage)
		this.sceneContainer.stage.setBackgroundColor(0x000000);

	this.sceneContainer.addChild(main.page);

	this.cleanMemory();

};

School.prototype.loadMenus = function() {
	// position of 1 and 2 sub menu
	var x1 = 68,
		x2 = 191,
		y = 4;

	this.menuParams = {
		path: School.COMMONFOLDER,
		background: 'menuskeleton.png',
		images: [
			'tillbaka.png',
			'korta_ord.png',
			'ord_som.png',
			'slutar_lika.png',
			'hur_borjar.png'
		],
		positions: [
			18, 133,
			289, 49,
			549, 68,
			1002, 92,
			1402, 93
		],
		data: {
			1: {
				hover_image: 'menu_0.png',
				hover_position: [224, -14],
				submenu_image: [
					'sub_menu_1.png',
					'sub_menu_2.png',
				],
				submenu_position: [
					224 + x1, y,
					224 + x2, y
				]

			},
			2: {
				hover_image: 'menu_0.png',
				hover_position: [611, -16],
				submenu_image: [
					'sub_menu_1.png',
					'sub_menu_2.png',
				],
				submenu_position: [
					611 + x1, y,
					611 + x2, y
				]
			},
			3: {
				hover_image: 'menu_2.png',
				hover_position: [996, -20],
				submenu_image: [
					'sub_menu_1.png',
					'sub_menu_2.png',
				],
				submenu_position: [
					996 + x1, y,
					996 + x2, y
				]
			},
			4: {
				hover_image: 'menu_3.png',
				hover_position: [1383, -19],
				submenu_image: [
					'sub_menu_1.png',
					'sub_menu_2.png',
				],
				submenu_position: [
					1383 + x1, y,
					1383 + x2 + 50, y
				]
			},
			pink_image: 'circle.png',
			move_position: {
				x: 10,
				y: 32
			},
			chop: { // optional
				images: [
					'korta_ord.png',
					'ord_som.png',
					'slutar_lika.png',
					'hur_borjar.png'
				],
				positions: [
					289, 49,
					549, 68,
					1002, 92,
					1402, 93
				]
			}
		}

	};
	this.menu = new Menu(this.menuParams);

	this.menuContainer.addChild(this.menu);

	//tillbaka button
	this.menu.getChildAt(1).click = this.menu.getChildAt(1).tap = this.homePage.bind(this);

	//bokstavernai alfabet button
	this.menu.getChildAt(2).click = this.menu.getChildAt(2).tap = this.navMenu1.bind(this);

	//A till P
	this.menu.getChildAt(3).click = this.menu.getChildAt(3).tap = this.navMenu2.bind(this);

	//Q till Ö
	this.menu.getChildAt(4).click = this.menu.getChildAt(4).tap = this.navMenu3.bind(this);

	//Hela alfabetet
	this.menu.getChildAt(5).click = this.menu.getChildAt(5).tap = this.navMenu4.bind(this);


}

School.prototype.imageButtons = function() {

	this.navPositions = [
		1862, 382,
		1496, 646,
		340, 556,
		730, 486
	];

	this.navButtons = [];

	for (var i = 0; i < this.navPositions.length; i++) {
		var imageSprite = PIXI.Sprite.fromFrame('ova.png');

		imageSprite.buttonMode = true;
		imageSprite.position.x = this.navPositions[i * 2];
		imageSprite.position.y = this.navPositions[i * 2 + 1];
		imageSprite.interactive = true;

		this.imageContainer.addChild(imageSprite);

		this.navButtons.push(imageSprite);
		imageSprite = null;
	}

	//Hela alfabetet image button
	this.navButtons[3].click = this.navButtons[3].tap = this.navMenu4.bind(this);

	//A till P image button
	this.navButtons[1].click = this.navButtons[1].tap = this.navMenu2.bind(this);

	//Bokstäverna i alfabetet image button
	this.navButtons[0].click = this.navButtons[0].tap = this.navMenu1.bind(this);

	//Q till Ö  image button
	this.navButtons[2].click = this.navButtons[2].tap = this.navMenu3.bind(this);



	this.sceneContainer.addChild(this.imageContainer);

}

School.prototype.navMenu = function(submenu1, submenu2, index) {
	this.sceneChange(submenu1);

	var self = this;

	this.pushStuffToHistory("history_other_School");

	this.subMenu = self.menu.getActiveMenu(index);
	self.menu.moveSelectedExerciseCircle(this.subMenu[0].position);
	self.sceneContainer.addChild(self.menuContainer);

	this.subMenu[0].click = this.subMenu[0].tap = function(data) {
		self.sceneChange(submenu1);

		self.menu.moveSelectedExerciseCircle(this.position);
		self.sceneContainer.addChild(self.menuContainer);

	}
	this.subMenu[1].click = this.subMenu[1].tap = function(data) {
		self.sceneChange(submenu2);

		self.menu.moveSelectedExerciseCircle(this.position);
		self.sceneContainer.addChild(self.menuContainer);
	}
}

School.prototype.navMenu1 = function() {
	this.navMenu('School_1_1', 'School_1_2', 1)
}


School.prototype.navMenu2 = function() {
	this.navMenu('School_2_1', 'School_2_2', 2)
}

School.prototype.navMenu3 = function() {
	this.navMenu('School_3_1', 'School_3_2', 3)
}

School.prototype.navMenu4 = function() {
	this.navMenu('School_4_1', 'School_4_2', 4)
}


School.prototype.pushStuffToHistory = function(key) {

	if (Device.ieVersion != 9) {
		var exHistory = new History();
		exHistory.PushState({
			scene: key
		});
		exHistory = null;
	}
}

School.prototype.cleanMemory = function() {
	this.bkg1 = null;
	main.eventRunning = false;
}