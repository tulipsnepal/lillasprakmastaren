function School_1_1() {
    cl.show()

    PIXI.DisplayObjectContainer.call(this);

    this.sceneContainer = new PIXI.DisplayObjectContainer();
    this.sceneContainer.position.set(77, 88);

    this.introContainer = new PIXI.DisplayObjectContainer();
    this.outroContainer = new PIXI.DisplayObjectContainer();

    localStorage.setItem('school_1_1', 0);
    main.CurrentExercise = "school_1_1";

    main.texturesToDestroy = [];

    this.common = new Common();

    this.loadSpriteSheet();

    // everything connected to this is rendered.
    this.addChild(this.sceneContainer);
}

School_1_1.constructor = School_1_1;
School_1_1.prototype = Object.create(PIXI.DisplayObjectContainer.prototype);

//set constant properties
School_1_1.imagePath = 'build/exercises/school/school_1_1/images/';
School_1_1.audioPath = 'build/exercises/school/school_1_1/audios/';
School_1_1.totalFeedback = Main.TOTAL_FEEDBACK; // default 10


/**
 * load array of assets of different format
 */
School_1_1.prototype.loadSpriteSheet = function() {

    var self = this;

    this.introId = loadSound(School_1_1.audioPath + 'intro');
    this.outroId = loadSound(School_1_1.audioPath + 'outro');

    // create an array of assets to load
    this.assetsToLoad = [
        School_1_1.imagePath + "sprite_1_1.json",
        School_1_1.imagePath + "sprite_1_1.png",

    ];
    // create a new loader
    loader = new PIXI.AssetLoader(this.assetsToLoad);
    // use callback
    loader.onComplete = this.spriteSheetLoaded.bind(this);
    //begin load
    loader.load();

}

/**
 * load exercise background, help icon, intro and outro scene stuffs
 */
School_1_1.prototype.spriteSheetLoaded = function() {

    // hide loader
    cl.hide();

    if (this.sceneContainer.stage)
        this.sceneContainer.stage.setBackgroundColor(0xFFFFFF);

    // fill exercise background
    this.sceneBackground = new PIXI.Graphics();
    this.sceneBackground.beginFill(0xbfe0bf);
    // set the line style to have a width of 5 and set the color to red
    this.sceneBackground.lineStyle(3, 0xf16c6c);
    // draw a rectangle
    this.sceneBackground.drawRect(0, 0, 1843, 1202);
    this.sceneContainer.addChild(this.sceneBackground);

    // exercise instruction
    this.help = PIXI.Sprite.fromFrame("topcorner.png");
    this.help.interactive = true;
    this.help.buttonMode = true;
    this.help.position.set(-77, -88);
    this.sceneContainer.addChild(this.help);

    this.loadExercise();
    this.introScene();
    this.outroScene();

    this.help.click = this.help.tap = this.introAnimation.bind(this);

    this.sceneContainer.addChild(this.introContainer);
    this.sceneContainer.addChild(this.outroContainer);
}

/**
 * play intro animation
 */
School_1_1.prototype.introAnimation = function() {

    createjs.Sound.stop();

    if (!this.introMode)
        this.resetExercise();

    this.help.interactive = false;
    this.help.buttonMode = false;

    main.overlay.visible = true;

    this.animationContainer.visible = true;
    this.headmouth.show();

    this.introInstance = createjs.Sound.play(this.introId, {
        interrupt: createjs.Sound.INTERRUPT_ANY
    });

    if (createjs.Sound.isReady())
        this.timerId = setInterval(this.animateIntro.bind(this), 1);

    this.introCancelHandler = this.cancelIntro.bind(this);
    this.introInstance.addEventListener("complete", this.introCancelHandler);

};

/**
 * handle mouth, hand with specific sound positions or queue points.
 */
School_1_1.prototype.animateIntro = function() {

    var currentPostion = this.introInstance.getPosition();
    if (currentPostion >= 1000 && currentPostion < 2000) {
        this.showHandAt(1);
    } else if (currentPostion >= 2000 && currentPostion < 3000) {
        this.showHandAt(2);
    } else {
        this.showHandAt(0);
    }
};

/**
 * cancel running intro animation
 */
School_1_1.prototype.cancelIntro = function() {

    createjs.Sound.stop();

    clearInterval(this.timerId);

    this.headmouth.hide();
    this.animationContainer.visible = false;
    this.help.interactive = true;
    this.help.buttonMode = true;

    this.ovaigen.visible = false;
    main.overlay.visible = false;
}

/**
 * play outro animation
 */
School_1_1.prototype.outroAnimation = function() {

    var self = this;

    var outroInstance = createjs.Sound.play(this.outroId);
    if (createjs.Sound.isReady()) {
        main.overlay.visible = true;
        self.mouthOutro.show();
    }
    this.cancelHandler = this.cancelOutro.bind(this);
    outroInstance.addEventListener("complete", this.cancelHandler);

}

/**
 * cancel running outro animation with click or touch event anywhere
 */
School_1_1.prototype.cancelOutro = function() {

    var self = this;

    createjs.Sound.stop();
    self.mouthOutro.hide();
    self.ovaigen.visible = true;
    self.help.interactive = true;
    self.help.buttonMode = true;
    main.overlay.visible = false;

};

/**
 * hand visibility
 * @param  {int} number
 */
School_1_1.prototype.showHandAt = function(number) {
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].visible = false;
    };
    if (isNumeric(number))
        this.hands[number].visible = true;
}

/**
 * create sprites,animation,sound related to intro scene
 */
School_1_1.prototype.introScene = function() {

    var self = this;

    this.barrier = new PIXI.Sprite.fromFrame('barrier.png');
    this.barrier.position.set(3, 571);
    main.texturesToDestroy.push(this.barrier);
    this.sceneContainer.addChild(this.barrier);

    this.animationContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.animationContainer);

    this.figure_intro = new PIXI.Sprite.fromFrame('man_body.png');
    this.figure_intro.position.set(686, 700);
    main.texturesToDestroy.push(this.figure_intro);
    this.animationContainer.addChild(this.figure_intro);

    this.headmouth = new Animation({
        "image": "headmouth",
        "length": 6,
        "x": 732,
        "y": 523,
        "speed": 3,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 5, 6, 2, 4, 1, 5],
        "frameId": 2
    });

    this.animationContainer.addChild(this.headmouth);

    this.hands = [
        new PIXI.Sprite.fromFrame('arm_1.png'),
        new PIXI.Sprite.fromFrame('arm_2.png'),
        new PIXI.Sprite.fromFrame('arm_3.png'),
    ];
    for (var i = 0; i < this.hands.length; i++) {
        this.hands[i].position.set(534, 414);
        this.animationContainer.addChild(this.hands[i]);
        this.hands[i].visible = false;
    };
    this.animationContainer.visible = false;

    this.keyBase = new PIXI.Sprite.fromFrame('key_base.png');
    this.keyBase.position.set(1363, 280);
    main.texturesToDestroy.push(this.keyBase);
    this.introContainer.addChild(this.keyBase);



}

/**
 * create sprites,animation,sound related to outro scene
 */
School_1_1.prototype.outroScene = function() {

    var self = this;

    this.outroContainer.visible = false;

    this.figure_outro = new PIXI.Sprite.fromFrame('man_body_outro.png');
    this.figure_outro.position.set(611, 540);
    main.texturesToDestroy.push(this.figure_outro);
    this.outroContainer.addChild(this.figure_outro);

    this.headOutro = new PIXI.Sprite.fromFrame('head_outro.png')
    this.headOutro.position.set(789, 332);
    main.texturesToDestroy.push(this.headOutro);
    this.outroContainer.addChild(this.headOutro);

    this.mouthOutro = new Animation({
        "image": "mouthoutro",
        "length": 4,
        "x": 912,
        "y": 568,
        "speed": 6,
        "pattern": true,
        "sequence": [1, 2, 3, 4, 3, 2, 4, 1],
        "frameId": 0
    });

    this.outroContainer.addChild(this.mouthOutro);

    // back to intro page
    this.ovaigen = PIXI.Sprite.fromFrame('ovaigen.png');
    this.ovaigen.position.set(1263, 346);
    this.ovaigen.interactive = true;
    this.ovaigen.buttonMode = true;
    this.ovaigen.visible = false;
    this.outroContainer.addChild(this.ovaigen);
}

/**
 * load exercise related stuffs
 */
School_1_1.prototype.loadExercise = function() {
    this.brownGraphics = createRect({
        color: '0x683c10',
        w: 414,
        h: 30,

    });
    this.brownTexture = this.brownGraphics.generateTexture();

    this.brownBar1 = new PIXI.Sprite(this.brownTexture);
    this.brownBar1.position.set(40, 462 - 20);
    this.introContainer.addChild(this.brownBar1);

    this.brownBar2 = new PIXI.Sprite(this.brownTexture);
    this.brownBar2.position.set(495, 332 - 8);
    this.introContainer.addChild(this.brownBar2);
    this.exerciseBaseContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.exerciseBaseContainer);

    this.whiteGraphics = createRect();
    this.whiteTexture = this.whiteGraphics.generateTexture();

    this.exerciseParams = {
        big: {
            x: 113,
            y: 127,
            width: 316,
            height: 316
        },
        small: {
            width: 114,
            height: 114,
            position: {
                box: [
                    515, 223 - 6,
                    649, 223 - 6,
                    782, 223 - 6,
                    724, 460 - 6,
                    865, 460 - 6,
                    1009, 460 - 6,
                ],
                text: {
                    rx: 38,
                    ry: 16 + 4
                }
            }
        },
        feedback: {
            position: [
                1620, 622,
                1554, 629,
                1491, 626,
                1431, 607,
                1325, 561,
                1239, 496,
                1195, 432,
                1186, 365,
                1205, 302,
                1241, 250,
            ]
        }
    }

    this.bigBoxSprite = new PIXI.Sprite(this.whiteTexture);
    this.bigBoxSprite.width = this.exerciseParams.big.width;
    this.bigBoxSprite.height = this.exerciseParams.big.height;
    this.bigBoxSprite.position.set(this.exerciseParams.big.x, this.exerciseParams.big.y);
    this.exerciseBaseContainer.addChild(this.bigBoxSprite);

    this.topTextXY = [];
    this.bottomTextXY = [];

    for (var i = 0; i < 6; i++) {
        var smallBoxSprite = new PIXI.Sprite(this.whiteTexture);
        smallBoxSprite.width = this.exerciseParams.small.width;
        smallBoxSprite.height = this.exerciseParams.small.height;
        smallBoxSprite.position = {
            x: this.exerciseParams.small.position.box[i * 2] - 4,
            y: this.exerciseParams.small.position.box[i * 2 + 1],
        }
        this.exerciseBaseContainer.addChild(smallBoxSprite);

        if (i < 3) {
            this.topTextXY.push(smallBoxSprite);
        } else
            this.bottomTextXY.push(smallBoxSprite);

    };

    redBox = createRect({
        "alpha": 0.6,
        "color": 0xff0000,
        "w": this.exerciseParams.small.width - 4,
        "h": this.exerciseParams.small.height,
    });

    greenBox = createRect({
        "alpha": 0.6,
        "color": 0x00ff00,
        "w": this.exerciseParams.small.width - 4,
        "h": this.exerciseParams.small.height,
    });

    greenBox.visible = false;

    redBox.visible = false;
    this.exerciseBaseContainer.addChild(greenBox);
    this.exerciseBaseContainer.addChild(redBox);

    this.exerciseContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.exerciseContainer);

    this.feedbackContainer = new PIXI.DisplayObjectContainer();
    this.introContainer.addChild(this.feedbackContainer);

    this.soundTexture = PIXI.Texture.fromFrame("sound_grey.png");

    this.dotBoxTextrue = PIXI.Texture.fromFrame('small_dotted_box.png');

    this.generateFeedback();

    this.initObjectPooling();

}

/**
 * manage exercise with new sets randomized
 */
School_1_1.prototype.runExercise = function() {

    var self = this;

    this.jsonData = this.wallSlices[0];

    this.dragText = [];
    this.dropPosition = null;
    this.dropIndex = null;

    // this.jsonData.image.position.set(this.exerciseParams.big.x + 40, this.exerciseParams.big.y + 48);
    this.jsonData.image.position.set(this.exerciseParams.big.x, this.exerciseParams.big.y);

    this.jsonData.image.width = this.exerciseParams.big.width;
    this.jsonData.image.height = this.exerciseParams.big.height;

    this.exerciseContainer.addChild(this.jsonData.image);

    var soundSprite = new PIXI.Sprite(this.soundTexture);
    soundSprite.position.set(this.exerciseParams.big.x + 20, this.exerciseParams.big.y + 245);
    this.exerciseContainer.addChild(soundSprite);

    soundSprite.howl = this.jsonData.sound;
    soundSprite.interactive = true;
    soundSprite.buttonMode = true;

    soundSprite.click = soundSprite.tap = this.onSoundPressed.bind(soundSprite);


    for (var i = 0; i < 6; i++) {
        if (i < 3) {
            var letterT = this.jsonData.topText[i];
            if (typeof letterT !== 'undefined') {
                letterT.position = {
                    x: this.topTextXY[i].position.x + (this.topTextXY[i].width - letterT.width) / 2,
                    y: this.topTextXY[i].position.y + (this.topTextXY[i].height - letterT.height) / 2
                };

                this.exerciseContainer.addChild(letterT);
            } else {
                this.dropIndex = i;
                this.dropPosition = this.topTextXY[i];

                this.highlightPosition = {
                    x: this.topTextXY[i].position.x + 4,
                    y: this.topTextXY[i].position.y + 6
                };
                var dotSprite = new PIXI.Sprite(this.dotBoxTextrue);
                dotSprite.position = {
                    x: this.topTextXY[i].position.x, // - 1,
                    y: this.topTextXY[i].position.y // + 4
                };
                this.exerciseContainer.addChild(dotSprite);
            }
        } else {
            var j = i - 3;
            var letterB = this.jsonData.bottomText[j];
            letterB.position = {
                x: this.bottomTextXY[j].position.x + (this.bottomTextXY[j].width - letterB.width) / 2,
                y: this.bottomTextXY[j].position.y + (this.bottomTextXY[j].height - letterB.height) / 2
            }
            letterB.interactive = true;
            letterB.buttonMode = true;
            letterB.oldPosition = {
                x: this.bottomTextXY[j].position.x + (this.bottomTextXY[j].width - letterB.width) / 2,
                y: this.bottomTextXY[j].position.y + (this.bottomTextXY[j].height - letterB.height) / 2
            };
            letterB.dragIndex = j;
            letterB.hitArea = new PIXI.Rectangle(-32, 36, 112, 112);
            this.exerciseContainer.addChild(letterB);
            this.dragText.push(letterB);
        }

    };

    for (var i = 0; i < this.dragText.length; i++) {
        var posx = this.bottomTextXY[i].position.x + (this.bottomTextXY[i].width - this.dragText[i].width) / 2;
        var posy = this.bottomTextXY[i].position.y + (this.bottomTextXY[i].height - this.dragText[i].height) / 2
        this.onDragging(i, posx, posy);
    };
}

School_1_1.prototype.onSoundPressed = function() {
    this.soundInstance = createjs.Sound.play(this.howl, {
        interrupt: createjs.Sound.INTERRUPT_NONE
    });
    this.soundInstance.addEventListener("failed", handleFailed);
    this.soundInstance.addEventListener("complete", handleComplete);
};

/**
 * generate output feedback
 */
School_1_1.prototype.generateFeedback = function(i) {

    this.hiddenFeedback = [];
    for (var i = 0; i < 10; i++) {
        var feedbackSprite = new PIXI.Sprite.fromFrame('fb_' + (i + 1) + '.png');
        feedbackSprite.position = {
            x: this.exerciseParams.feedback.position[i * 2],
            y: this.exerciseParams.feedback.position[i * 2 + 1],
        }
        feedbackSprite.alpha = 0.2;
        this.feedbackContainer.addChild(feedbackSprite);
        this.hiddenFeedback.push(feedbackSprite);
    };

}


/**
 * drag drop sprite
 * on correct drop, feedback is updated
 */
School_1_1.prototype.onDragging = function(i, x, y) {

    var self = this;

    // use the mousedown and touchstart
    this.dragText[i].mousedown = this.dragText[i].touchstart = function(data) {
        if (addedListeners) return false;
        data.originalEvent.preventDefault()
            // store a refference to the data
            // The reason for this is because of multitouch
            // we want to track the movement of this particular touch
        this.data = data;
        this.alpha = 0.9;
        this.dragging = true;
        this.sx = this.data.getLocalPosition(this).x;
        this.sy = this.data.getLocalPosition(this).y;
        this.bringToFront();

    }

    this.dragText[i].mouseup = this.dragText[i].mouseupoutside = this.dragText[i].touchend = this.dragText[i].touchendoutside = function(data) {
        if (addedListeners) return false;
        var dragSelf = this;

        this.alpha = 1
        this.dragging = false;

        // set the interaction data to null
        this.data = null;

        redBox.position = greenBox.position = {
            x: self.highlightPosition.x,
            y: self.highlightPosition.y - 4
        }

        if (self.isDropArea(this.dx, this.dy)) {
            addedListeners = true;
            dragSelf.interactive = false;
            if (self.jsonData.answer === this.text.replace(/(?:\r\n|\r|\n)/g, '')) {
                dragSelf.position = {
                    x: self.dropPosition.position.x + (self.dropPosition.width - dragSelf.width) / 2,
                    y: self.dropPosition.position.y + (self.dropPosition.height - dragSelf.height) / 2
                };
                correctSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                greenBox.visible = true;
                setTimeout(function() {
                    greenBox.visible = false;
                    dragSelf.interactive = true;
                    self.correctFeedback();
                    self.bottomTextXY[dragSelf.dragIndex].visible = true;
                    addedListeners = false;
                }, 2200);

            } else {
                wrongSoundInstance.play(createjs.Sound.INTERRUPT_ANY);
                redBox.visible = true;
                dragSelf.position = {
                    x: self.dropPosition.position.x + (self.dropPosition.width - dragSelf.width) / 2,
                    y: self.dropPosition.position.y + (self.dropPosition.height - dragSelf.height) / 2
                };
                setTimeout(function() {
                    redBox.visible = false;
                    dragSelf.position.x = x;
                    dragSelf.position.y = y;
                    dragSelf.interactive = true;
                    self.wrongFeedback();
                    self.bottomTextXY[dragSelf.dragIndex].visible = true;
                    addedListeners = false;
                }, 2200);
            }
            this.sx = 0;
            this.sy = 0;
            this.dx = 0;
            this.dy = 0;

        } else {
            // reset dragged element position
            this.sx = 0;
            this.sy = 0;
            this.dx = 0;
            this.dy = 0;
            this.position.x = dragSelf.oldPosition.x;
            this.position.y = dragSelf.oldPosition.y;
            self.bottomTextXY[dragSelf.dragIndex].visible = true;

        }
    }

    // set the callbacks for when the mouse or a touch moves
    this.dragText[i].mousemove = this.dragText[i].touchmove = function(data) {
        // set the callbacks for when the mouse or a touch moves
        if (this.dragging) {
            self.bottomTextXY[this.dragIndex].visible = false;
            // need to get parent coords..
            var newPosition = this.data.getLocalPosition(this.parent);
            this.position.x = newPosition.x - this.sx;
            this.position.y = newPosition.y - this.sy;
            this.dx = newPosition.x;
            this.dy = newPosition.y;
        }
    };

}

/**
 * check drag position is inside rectangular drop position
 * @param  {float} dx    drag positon x
 * @param  {float} dy    drag position y
 * @param  {integer} index
 * @return {boolean}       true/false
 */
School_1_1.prototype.isDropArea = function(dx, dy) {
    var x1 = this.exerciseParams.small.position.box[this.dropIndex * 2],
        y1 = this.exerciseParams.small.position.box[this.dropIndex * 2 + 1],
        x2 = this.exerciseParams.small.position.box[this.dropIndex * 2] + this.exerciseParams.small.width,
        y2 = this.exerciseParams.small.position.box[this.dropIndex * 2 + 1] + this.exerciseParams.small.height;

    if (isInsideRectangle(dx, dy, x1, y1, x2, y2)) {
        return true;
    } else {
        return false;
    }
}

/**
 * on correct feedback increment feedback by 1
 * if feedback is greater than 10, go to outro scene
 */
School_1_1.prototype.correctFeedback = function() {
    addedListeners = false;
    var feedback = localStorage.getItem('school_1_1');
    this.hiddenFeedback[feedback].alpha = 1;
    feedback++;
    localStorage.setItem('school_1_1', feedback);

    if (feedback >= School_1_1.totalFeedback) {
        setTimeout(this.onComplete.bind(this), 1000);
    } else {
        this.getNewExerciseSet();
    }
}

/**
 * wrong answer , feedback decrease
 */
School_1_1.prototype.wrongFeedback = function() {
    addedListeners = false;
    var feedback = localStorage.getItem('school_1_1');
    feedback--;
    if (feedback >= 0) {
        this.hiddenFeedback[feedback].alpha = 0.2;
        localStorage.setItem('school_1_1', feedback);
    };
}

/**
 * prefetch exercise data and cache number of sets defined only.
 */
School_1_1.prototype.initObjectPooling = function() {

    var self = this;

    this.wallSlices = [];

    this.poolSlices = [];

    self.pool = {};

    var loader = new PIXI.JsonLoader('uploads/exercises/school/school_1_1.json?nocache=' + (new Date()).getTime());
    loader.on('loaded', function(evt) {
        //data is in loader.json
        self.pool = new ExerciseSpritesPool(loader.json);
        //borrow two sprites once
        self.generatePoolCategory();
        self.borrowWallSprites(2);
        self.runExercise();

    });
    loader.load();
}


School_1_1.prototype.generatePoolCategory = function() {

    var keyArr = [];

    var spriteArr = this.pool.sprites,
        spriteLen = spriteArr.length;

    for (var i = 0; i < spriteLen; i++) {
        keyArr.push(spriteArr[i][0].word[0]);
    };

    this.poolCategory = keyArr.filter(onlyUnique);

}

/**
 * borrow wall sprites and cache audio and sprites
 * @param  {int} num
 */
School_1_1.prototype.borrowWallSprites = function(num) {

    var rand, category, sprite, spriteObj;

    var self = this;

    for (var i = 0; i < num; i++) {

        // var sprite = this.pool.borrowSprite(),
        rand = makeUniqueRandom(this.poolCategory.length);
        this.borrowCategory = this.poolCategory[rand];

        sprite = this.pool.borrowUniqueSprite(this.borrowCategory, this.lastBorrowSprite);
        spriteObj = sprite[0],
            this.lastBorrowSprite = spriteObj.word[0];
        var imageSprite,
            soundHowl,
            answerArr = [],
            topLettersArr = [],
            bottomLettersArr = [];

        this.poolSlices.push(sprite);

        imageSprite = PIXI.Sprite.fromImage('uploads/images/' + spriteObj.image);

        soundHowl = loadSound('uploads/audios/' + StripExtFromFile(spriteObj.sound));

        var questionWordArr = spriteObj.question_word.split('');

        for (var j = 0; j < 3; j++) {
            var topLetters;
            if (questionWordArr[j] != '_') {
                topLetters = new PIXI.Text("" + questionWordArr[j], {
                    font: "80px Arial",
                });
            } else {

                topLetters = undefined;
            }
            topLettersArr.push(topLetters);

            var bottomLetters = new PIXI.Text("" + spriteObj.option_letters[j], {
                font: "80px Arial",
            });
            bottomLettersArr.push(bottomLetters);

        };

        this.wallSlices.push({
            image: imageSprite,
            sound: soundHowl,
            topText: topLettersArr,
            bottomText: bottomLettersArr,
            answer: spriteObj.answer,
        });
    }
};

/**
 * return sprites to object pool
 */
School_1_1.prototype.returnWallSprites = function() {
    var sprite = this.poolSlices[0];

    this.pool.returnSprite(sprite);

    this.poolSlices = [];
    this.wallSlices.shift();
};

/**
 * get new set on correct feedback
 */
School_1_1.prototype.getNewExerciseSet = function() {
    this.exerciseContainer.removeChildren();

    this.returnWallSprites();
    this.borrowWallSprites(1);
    this.runExercise();
}

/**
 * reset exercise and get new set
 */
School_1_1.prototype.restartExercise = function() {

    this.resetExercise();

    this.feedbackContainer.removeChildren();

    localStorage.setItem('school_1_1', 0);
    this.outroContainer.visible = false;
    this.introContainer.visible = true;

    this.getNewExerciseSet();

    this.hiddenFeedback.length = 0;
    this.generateFeedback();
};

/**
 * the outro scene is displayed
 */
School_1_1.prototype.onComplete = function() {
    var self = this;

    this.introMode = false;

    this.help.interactive = false;
    this.help.buttonMode = false;
    this.introContainer.visible = false;
    this.outroContainer.visible = true;

    this.ovaigen.click = this.ovaigen.tap = function(data) {
        self.cancelOutro();
        self.restartExercise();
    }

    this.outroAnimation();

}

/**
 * reset exercise to default values
 */
School_1_1.prototype.resetExercise = function() {
    this.ovaigen.visible = false;
    this.outroContainer.visible = false;
    this.introContainer.visible = true;
    this.introMode = true;
};

/**
 * clean memory by clearing reference of object
 */
School_1_1.prototype.cleanMemory = function() {

    this.common = null;

    this.introId = null;
    this.outroId = null;

    if (this.assetsToLoad)
        this.assetsToLoad.length = 0;

    if (this.sceneBackground)
        this.sceneBackground.clear();

    this.help = null;

    this.introMode = null;

    this.introInstance = null;

    this.timerId = null;

    this.introCancelHandler = null;
    this.cancelHandler = null;

    if (this.hands)
        this.hands.length = 0;

    this.barrier = null;

    this.figure_intro = null;
    this.figure_outro = null;

    this.headmouth = null;

    this.keyBase = null;

    this.headOutro = null;
    this.mouthOutro = null;
    this.ovaigen = null;

    if (this.brownGraphics)
        this.brownGraphics.clear();

    this.brownGraphics = null;

    this.brownTexture = null;

    this.brownBar1 = null;
    this.brownBar2 = null;

    if (this.whiteGraphics)
        this.whiteGraphics.clear();

    this.whiteGraphics = null;

    this.whiteTexture = null;

    wipe(this.exerciseParams);
    this.exerciseParams = null;

    this.bigBoxSprite = null;

    if (this.topTextXY)
        this.topTextXY.length = 0;

    if (this.bottomTextXY)
        this.bottomTextXY.length = 0;

    this.soundTexture = null;
    this.dotBoxTextrue = null;

    this.jsonData = null;

    if (this.dragText)
        this.dragText.length = 0;

    this.dropPosition = null;
    this.dropIndex = null;

    this.highlightPosition = null;

    this.soundInstance = null;

    if (this.hiddenFeedback)
        this.hiddenFeedback.length = 0;

    if (this.wallSlices)
        this.wallSlices.length = 0;

    if (this.poolSlices)
        this.poolSlices.length = 0;

    wipe(this.pool);
    this.pool = null;

    this.feedbackContainer.removeChildren();
    this.exerciseBaseContainer.removeChildren();
    this.animationContainer.removeChildren();
    this.outroContainer.removeChildren();
    this.introContainer.removeChildren();
    this.sceneContainer.removeChildren();

    this.feedbackContainer = null;
    this.exerciseBaseContainer = null;
    this.animationContainer = null;
    this.outroContainer = null;
    this.introContainer = null;
    this.sceneContainer = null;

}