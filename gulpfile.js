// Include gulp
var gulp = require('gulp');

// Include plugins
var plugins = require("gulp-load-plugins")({
    pattern: ['gulp-*', 'gulp.*', 'main-bower-files', 'browser-sync', 'imagemin-pngcrush', 'imagemin-optipng', 'imagemin-pngquant'],
    replaceString: /\bgulp[\-.]/,
    camelize: true, // if true, transforms hyphenated plugins names to camel case
    lazy: true, // whether the plugins should be lazy loaded on demand
});

var paths = {
    build: 'build/',
    htmlSources: 'src/index.html',
    jsSources: ['src/lib/*.js', 'src/common/js/*.js', 'src/exercises/**/*.js'],
    fntSources: ['src/**/*.fnt'],
    xmlSources: ['src/**/*.xml'],
    jsonSources: ['src/**/*.json'],
    audioSources: ['src/**/*.mp3', 'src/**/*.ogg'],
    imageSources: ['src/**/*.png'],
}

var env = process.env.NODE_ENV || 'development';

// Gulp plumber error handler
var onError = function(err) {
    console.log(err);
}

// start server
gulp.task('browser-sync', function() {
    plugins.browserSync({
        server: {
            baseDir: "./"
        },
        // Wait for 2 seconds before any browsers should try to inject/reload a file.
        // reloadDelay: 2000,
        // Stop the browser from automatically opening
        open: false,
        // Use a specific port (instead of the one auto-detected by BrowserSync)
        port: 8080,
        // Don't send any file-change events to browsers
        codeSync: false
    });
});

// Reload all Browsers
gulp.task('bs-reload', function() {
    plugins.browserSync.reload();
});

gulp.task('clean', function() {
    return gulp.src('build/js/*.js', {
            read: false
        }) // much faster
        .pipe(plugins.rimraf());
});

gulp.task('cleanall', function() {
    return gulp.src('build', {
            read: false
        }) // much faster
        .pipe(plugins.rimraf());
});

gulp.task('html', function() {
    var opts = {
        comments: false,
        spare: true
    };

    gulp.src(paths.htmlSources)
        .pipe(plugins.minifyHtml(opts))
        .pipe(gulp.dest('.'))
});

// Concatenate & Minify JS
gulp.task('js', function() {
    gulp.src(paths.jsSources)
        .pipe(plugins.concat('app.js'))
        .pipe(plugins.plumber({
            errorHandler: plugins.notify.onError("Error: <%= error.message %>")
        }))
        .pipe(gulp.dest(paths.build + 'js'))
        .pipe(plugins.notify("Javascript concatenated!"));
});

gulp.task('jsmin', function() {
    gulp.src(paths.jsSources)
        .pipe(plugins.concat('app.js'))
        /*.pipe(gulp.dest(paths.build + 'js'))
        .pipe(plugins.if(env === 'production', plugins.rename({
            suffix: '.min'
        })))*/
        .pipe(plugins.uglify({
            mangle: true
        }))
        .pipe(plugins.plumber({
            errorHandler: plugins.notify.onError("Error: <%= error.message %>")
        }))
        .pipe(plugins.stripDebug())
        .pipe(gulp.dest(paths.build + 'js'))
        .pipe(plugins.notify("Javascript uglify!"));
});

gulp.task('compile', function() {
    gulp.src(paths.jsSources)
        .pipe(plugins.closureCompiler({
            compilerPath: 'bower_components/compiler-latest/compiler.jar',
            fileName: 'app.min.js',
            compilerFlags: {
                language_in: 'ECMASCRIPT5'
            }
        }))
        .pipe(plugins.plumber({
            errorHandler: plugins.notify.onError("Error: <%= error.message %>")
        }))
        .pipe(gulp.dest(paths.build + 'js'))
        .pipe(plugins.notify("Javascript uglify!"));
});

gulp.task('json', function() {
    gulp.src(paths.jsonSources)
        .pipe(plugins.jsonminify())
        .pipe(gulp.dest(paths.build));
});

gulp.task('images', function() {
    /* return gulp.src(paths.imageSources)
         .pipe(plugins.newer(paths.build))
         .pipe(plugins.imagemin({
             interlaced: true,
             progressive: true,
             svgoPlugins: [{
                 removeViewBox: false
             }],
             use: [plugins.imageminPngcrush()]
         }))
         .pipe(gulp.dest(paths.build));*/
    gulp.src(paths.imageSources)
        .pipe(gulp.dest(paths.build));
});

gulp.task('imagemin', function() {
    return gulp.src(paths.imageSources)
        .pipe(plugins.newer(paths.build))
        .pipe(plugins.imageminPngquant({
            quality: '65-80',
            speed: 4
        }))
        .pipe(plugins.imageminOptipng({
            optimizationLevel: 3
        }))
        .pipe(gulp.dest(paths.build));
});

// grab libraries files from bower_components, minify and push in /public
gulp.task('bower', function() {

    var jsFilter = plugins.filter('*.js');

    return gulp.src(plugins.mainBowerFiles({
        debugging: true,
        includeDev: true
    }))

    // grab vendor js files from bower_components, minify and push in /public
    .pipe(jsFilter)
        .pipe(gulp.dest('src/vendor/lib'))
        .pipe(plugins.concat('third-party.js'))
        .pipe(gulp.dest('src/vendor'))
        .pipe(plugins.uglify())
        .pipe(plugins.rename({
            suffix: ".min"
        }))
        .pipe(gulp.dest(paths.build + '/js/vendor'))
        .pipe(jsFilter.restore())

});

gulp.task('copyaudio', function() {
    gulp.src(paths.audioSources)
        .pipe(gulp.dest(paths.build));
});

gulp.task('copybitmapfont', function() {
    gulp.src(paths.fntSources)
        .pipe(gulp.dest(paths.build));
});

gulp.task('copybitmapfontxml', function() {
    gulp.src(paths.xmlSources)
        .pipe(gulp.dest(paths.build));
});

// Rerun the task when a file changes
gulp.task('watch', function() {
    // watch for HTML changes
    gulp.watch(paths.htmlSources, ['html', 'bs-reload']);

    // watch for JS changes
    gulp.watch(paths.jsSources, ['clean', 'js', 'bs-reload']);

    // watch for JSON changes
    gulp.watch(paths.jsonSources, ['json', 'bs-reload']);

    // watch for Images changes
    //  gulp.watch(paths.imageSources, ['images']);

    // watch for audio changes
    //gulp.watch(paths.audioSources, ['copyaudio']);

    //watch for bitmap fonts
    // gulp.watch(paths.fntSources, ['copybitmapfont']);
    // gulp.watch(paths.fntSources, ['copybitmapfontxml']);

});

// default gulp task
//gulp.task('default', ['watch', 'clean', 'html', 'js', 'json', 'browser-sync', 'copybitmapfont', 'copybitmapfontxml', 'images', 'copyaudio']);
gulp.task('default', ['watch', 'clean', 'html', 'js', 'json', 'browser-sync']);
gulp.task('dist', ['watch', 'clean', 'html', 'jsmin', 'json', 'copybitmapfont', 'copybitmapfontxml', 'images', 'copyaudio', 'browser-sync']);