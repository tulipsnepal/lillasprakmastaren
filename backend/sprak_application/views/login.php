<!DOCTYPE html> 
<html lang="en-US">
  <head>
    <title><?php echo SITENAME?></title>
    <meta charset="utf-8">
    <link href="<?php echo base_url(); ?>assets/css/global.css" rel="stylesheet" type="text/css">
  </head>
  <body>
    <div class="container login">
      <?php 
      $attributes = array('class' => 'form-signin');
      echo form_open('login/validate_credentials', $attributes);
      echo '<h2 class="form-signin-heading">Login</h2>';
      echo form_input(array("name"=>"user_name","placeholder"=>"Username","style"=>"width:280px"));
      echo form_password(array("name"=>"password","type"=>"password","placeholder"=>"Password","style"=>"width:280px"));
      if(isset($message_error) && $message_error)
	  {
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo "<b>Error!</b> Invalid login credentials";
          echo '</div>';             
      }
      echo form_submit('submit', 'Login', 'class="btn btn-large btn-primary"');
      echo form_close();
      ?>      
    </div><!--container-->
    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
  </body>
</html>    
    