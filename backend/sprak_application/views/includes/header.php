<!DOCTYPE html>
<html lang="en-US">
<head>
  <title><?php echo SITENAME?></title>
  <meta charset="utf-8">
  <link href="<?php echo base_url(); ?>assets/css/global.css" rel="stylesheet" type="text/css">
  <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/admin.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/imagesloaded.pkgd.min.js"></script>
   <script src="<?php echo base_url(); ?>assets/js/fancybox/jquery.fancybox.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/functions.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/fancybox/jquery.fancybox.css" media="screen" />
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-select.min.js"></script>
     <link href="<?php echo base_url(); ?>assets/css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
    <script type="text/javascript">
  	var GlobalAjaxUrl,
     ajax_base_url = '<?php echo BASE_URL; ?>/',
     ajax_client_url = '<?php echo BASE_URL_CLIENT; ?>/';
      $(function(){
  		$('.fancybox').fancybox();
  		$('.selectpicker').selectpicker();
  	});
    </script>
</head>
<body>
	<div class="navbar navbar-fixed-top">
	  <div class="navbar-inner">
	    <div class="container">
        	<?php /*?><a class="brand" style="font-size:12px;color:#fff">Welcome! User</a><?php */?>

	      <ul class="nav" style="margin-top:10px">

                <li><a href="<?php echo base_url();?>">Home</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Global Action<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a tabindex="-1" href="<?php echo base_url()?>bulkupload">Bulk Uploads</a></li>
                        <li><a tabindex="-1" href="<?php echo base_url()?>words">Words & Phrases</a></li>
                        <li><a tabindex="-1" href="<?php echo base_url()?>tags">Add/Edit Tags</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Exercise Settings<b class="caret"></b></a>
                     <ul class="dropdown-menu">
                        <li><a tabindex="-1" href="<?php echo base_url()?>settings">Settings</a></li>
                     </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Exercises<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <?php echo $this->utilities->fetch_exercise_menu();?>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">System<b class="caret"></b></a>
                    <ul class="dropdown-menu">
	                    <li><a  href="<?php echo base_url()?>users">Users Management</a></li>
                        <li class="divider"></li>
                        <li><a  href="<?php echo base_url()?>logout">Log Out</a></li>
                    </ul>
                </li>
            </ul>
             <a class="brand" href="<?php echo base_url();?>"><img src="<?php echo base_url()?>assets/img/logo.png" style="height:40px;"/></a>
	    </div>
	  </div>
	</div>