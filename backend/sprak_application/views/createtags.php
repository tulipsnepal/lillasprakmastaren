<script type="text/javascript">

$(function(){
	loadWords();
		$('.selectpicker').selectpicker();
		
		$(document.body).on("click",".mainwords",function(){
			$current_check_val = $(this).val();
			$current_check_text = $.trim($(this).parent("label").text());
			
			//POPULATE IMAGE OF SELECTED WORDS
			if($(this).prop("checked")){
				$chkval = $(this).val();
				$chktext = $(this).parent("label").text();
				var exists = $("#selected_tags option[value='"+$chkval+"']").length !== 0;
				
				if(!exists)
				{
					$("#selected_tags").append("<option value='"+$chkval+"'>"+$chktext+"</option>");
				}
				$("#selected_tags").css("border","1px solid #C30");
				
			}else{
				$("#selected_tags option[value='"+$(this).val()+"']").remove();
			}
			
		});
	//WHEN SORT ALPHABETS ARE CLICKED
		$(".loadcharwords").on("click",function(){
			$(".loadcharwords").parent("li").removeClass("active");
			$(this).parent("li").addClass("active");
			loadWords("alpha",$(this).text());
		});
		
		//WHEN SEARCH IS CLICKED
		$(".loadcharwords_search").on("click",function(){
			$keyword = $(this).prev(".searchstring").val();
			loadWords("search",$keyword);
		});
		
		
		//WHEN FILTER BY TAGS IS SELECTED
		$(".loadcharwords_filter").on("change",function(){
			$keyword = $(this).val();
			loadWords("tags",$keyword);
		});
		
		//savetags
		$(".savetags").click(function(){
			var tag = $("#addnewtags option:selected").length;
			var checkedBoxlength=$("#selected_tags").children("option").length;
			var error = "";
			if(!tag>0)
			{
				error +="- Select the tag(s). \n";
			}
			if(checkedBoxlength==0){ 
				error +="- Select words to link tag with. \n";
			}
			if(error!=""){
				alert("Please fix the following error(s)\n"+error);
				return false;
			}else{
				$("#selected_tags option").prop("selected",true);
				$("#frmSaveTags").submit();
			}
		});
		
		
				
});
</script>
<style type="text/css">
.loadcharwords_search{
	margin-bottom: 11px !important;
}
</style>
<form name="frmSaveTags" id="frmSaveTags" method="post" action="<?php echo base_url();?>words/savetags">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
        <h4 class="modal-title" id="myModalLabel">Link tags to words</h4>
      </div>
      <div class="modal-body">
      <select style="display:none" name="selected_tags[]" id="selected_tags" multiple></select>
          <div>
                	
                    <div class="upl tivoli-1-sub-head">
                    <div>Select the tag(s)</div>
                    <div class="controls2 selecttags">
                    <select class='selectpicker show-menu-arrow' name='addnewtags[]' id="addnewtags" multiple title='' data-selected-text-format='count>6'>
				  <?php
                  if(isset($tags))
                  {
                      foreach($tags as $tag)
                      {
                          echo "<option value='".$tag["tag_id"]."'>".$tag["tag_name"]."</option>";
                      }
                  }
                  ?>
              </select><Br />
                    <div class="help-inline" style="font-weight:normal;">Already existing tag will be ignored.</div>
                    </div>
                    </div>
                     <div class="upl tivoli-1-sub-head">Select the words you want to link with specified tag</div>
                    <?php echo $gen_alphabets?>
                    <div  style="max-height:500px;overflow:auto">
                    	<ul class="list_words main-col"></ul>
                    </div>
                </div>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary savetags">Save changes</button>
      </div>
    </div>
  </div>
 </form>          