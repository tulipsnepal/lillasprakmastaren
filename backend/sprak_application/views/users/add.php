    <script type="text/javascript">
	$(document).ready(function(){
		$(".showpassword").click(function(){
			$attr = $("#pass_word").attr("type");
			if($attr=="password"){
				$("#pass_word").attr("type","text");
			}else{
				$("#pass_word").attr("type","password");
			}
		})
	});
	</script>
    <div class="container top">
      
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url().$this->uri->segment(1); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <?php echo $action?>
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          <?php echo $actioning?> <?php echo ucfirst($this->uri->segment(1));?>
        </h2>
      </div>
 
      <?php
      //flash messages
	  
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == "1")
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_SUCCESS_MSG;
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_ERROR_MSG;
          echo '</div>';          
        }
      }
	  if($this->session->flashdata('errors'))
	  {
		   echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			echo "<b>".$this->session->flashdata('errors')."</b>";
			 echo '</div>'; 
	  }
      ?>
      
      <?php
	  
	  $editmode = isset($users)?1:0;
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');
      
      //form validation
      echo validation_errors();
      if($action=="Add")
	  {
      	echo form_open('users/add', $attributes);
	  }else{
		echo form_open('users/update/'.$this->uri->segment(3).'', $attributes);
	  }
      ?>
        <fieldset>
          <div class="control-group">
            <label for="inputError" class="control-label">First Name</label>
            <div class="controls">
              <input type="text" id="first_name" name="first_name" value="<?php echo ($editmode==1)?$users["first_name"]:""?>">
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Last Name</label>
            <div class="controls">
              <input type="text" id="last_name" name="last_name" value="<?php echo ($editmode==1)?$users["last_name"]:""?>">
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Email Address</label>
            <div class="controls">
              <input type="text" id="email_address" name="email_address" value="<?php echo ($editmode==1)?$users["email_addres"]:""?>">
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Username</label>
            <div class="controls">
              <input type="text" id="user_name" name="user_name" value="<?php echo ($editmode==1)?$users["user_name"]:""?>" <?php echo ($action=="Update")?"readonly='readonly'":""?> autocomplete="off" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          
          <div class="control-group">
            <label for="inputError" class="control-label">Password</label>
            <div class="controls">
              <input type="password" id="pass_word" autocomplete="off" name="pass_word"><span style="margin-left:10px;cursor:pointer;" class="showpassword"><img src="<?php echo base_url()?>assets/img/eye.png" title="Toggle Password" width="22"  /></span>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">User Status</label>
            <div class="controls">
             <select class="span2" name="user_status">
             	<option value="1">Active</option>
                <option value="0">Inactive</option>
             </select>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          
         
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="button" onclick="document.location.href='<?php echo base_url()?>users'">Cancel</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>
     