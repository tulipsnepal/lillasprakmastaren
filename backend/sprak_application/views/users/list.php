  	
    
    <div class="container top">

      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <?php echo ucfirst($this->uri->segment(1));?>
        </li>
      </ul>

      <div class="page-header users-header">
        <h2>
          <?php echo ucfirst($this->uri->segment(1));?> 
          <a  href="<?php echo site_url().$this->uri->segment(1); ?>/add" class="btn btn-success">Add a new</a>
        </h2>
      </div>
      
      <div class="row">
      <?php 
	if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == "done")
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_SUCCESS_MSG;
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_ERROR_MSG;
          echo '</div>';          
        }
      }	
	?>
        <div class="span12 columns">
          <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
                <th class="header">#</th>
                <th class="yellow header headerSortDown">Full Name</th>
                <th class="red header">Email Address</th>
                <th class="red header">Username</th>
                <th class="red header">Status</th>
                <th class="red header">Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php
			  $i = 1;
              foreach($users as $row)
              {
                $class = "";
                if($row["user_status"]!=1)
                {
                  $class = "hide-deleted-rows";
                }
                echo '<tr class="'.$class.'">';
                echo '<td>'.$i.'</td>';
                echo '<td>'.$row['first_name']." ".$row['last_name'].'</td>';
				echo '<td>'.$row["email_addres"].'</td>';
                echo '<td>'.$row["user_name"].'</td>';
				if($row["user_status"]==1)
				{
					$status = "Active";
				}else{
					$status = "<span style='color:red'>Deleted</span>";
				}
                echo '<td>'.$status.'</td>';
				
                echo '<td class="crud-actions">
                  <a href="'.site_url().'users/update/'.$row['id'].'" class="btn btn-info">view & edit</a>  
                  
                </td>';
                echo '</tr>';
				$i++;
              }
              ?>

            </tbody>
          </table>
        <div style="text-align:right;"><a href="javascript:void(0)" id="toggle-deleted-users" class="btn btn-primary">Show Deleted Users</a></div>
      </div>
    </div>
    <script>
    $("#toggle-deleted-users").click(function(){
          var self = $(this);
          $(".hide-deleted-rows").each(function(){
              if($(this).is(":visible"))
              {
                  $(this).hide();
                  self.html("Show Deleted Users");
              }else{
                  $(this).show();
                  self.html("Hide Deleted Users");
              }
          });
    });
    </script>