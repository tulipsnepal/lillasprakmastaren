    <script type="text/javascript">
	//SET GLOBAL AJAX URL TO BE USED FOR AJAX CALLS
	GlobalAjaxUrl = '<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>';
	
	$(document).ready(function(){
		$(".li-alphabets").on("click",function(){
			$checkedlength = $(".li-alphabets :checked").length;
			$("#answer").empty();
			$(".li-alphabets :checked").each(function(){
				$("#answer").append("<option value='"+$(this).val()+"'>"+$(this).next("span").html()+"</option>");
			});
			if($checkedlength>4){
				alert("You can't select more than 4 alphabets");
				return false;
			}
		});
		
		
		
		
			
			
			
			$(document.body).on("click",".playAudio",function(){
				var audioSrc = $(this).data("src");
				$(".audiopreviewholder").empty().html("<audio class='audioFile' id='html5audio' src='"+audioSrc+"' ></audio>");
				document.getElementById("html5audio").play();
			});
			
			
	});
	function submitSet(){
		$checkedlength = $(".li-alphabets :checked").length;
		var checkedArray = new Array();
		$(".li-alphabets :checked").each(function(){
			checkedArray.push($(this).val());
		})
		
		
		var errors = "";
		
		if($checkedlength<4)
		{
			errors +="You must choose 4 alphabets from the list.\n";
		}
		
		$answer = $(".answer").val();
		
		if($answer=="")
		{
			errors +="You must select an answer alphabet .\n";
		}
		
		if($(".selectFile:checked").length==0){
			errors +="You must select an audio related to answer alphabet .\n";
		}
		
		if(errors!=""){
			alert("Please fix the following error(s)\n\n"+errors);
			return false;
		}
	
		//alert($unrhyming);
		return true;
	}
	</script>
    <div class="container top">
      
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url()."exercise"; ?>">
            <?php echo "Exercises";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
        <a href="<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>">
          <?php echo ucwords($this->uri->segment(1))?>
          </a>
           <span class="divider">/</span>
        </li>
        <li class="active">
        <?php echo "Toystore 1-1"?>
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          <?php echo "Toystore 1-1 - ".$exercise_desc[0]?> 
        </h2>
      </div>
 
      <?php
      //flash messages
	  
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == "done")
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_SUCCESS_MSG;
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_ERROR_MSG;
          echo '</div>';          
        }
      }
	  if($this->session->flashdata('errors'))
	  {
		   echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			echo "<b>".$this->session->flashdata('errors')."</b>";
			 echo '</div>'; 
	  }
      ?>
      
      <?php
	  
	 $editmode = isset($exercises)?1:0;
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '','onsubmit'=>'return submitSet();');
      
      //form validation
      echo validation_errors();
	 
      if($action=="Add")
	  {
      	echo form_open('toystore/toystore_1_1/add', $attributes);
	  }else{
		echo form_open('toystore/toystore_1_1/update/'.$this->uri->segment(4).'', $attributes);
	  }
	  
	  ?>
      
        <fieldset>
        <div class="audiopreviewholder"></div>
         <input type="hidden" id="exercise_id" name="exercise_id" value="<?php echo $exercise_desc[2]?>" />
      <div class="control-group">
            <label for="inputError" class="control-label">Select Alphabets</label>
            <div class="controls">
            <?php
            if(isset($set) && $set!="")
			{
			?>
            <div class="help-inline" style="padding:0 0 10px 0px">You can change the selected words by using the list below</div>
            <?php
			}
			?>
                <div class="tivoli-1-full" style="margin-left:12px;">
                	<div class="upl tivoli-1-sub-head">Select 4 alphabets and then select the audio answer for the alphabets</div>
                    
                    <div  style="max-height:500px;overflow:auto">
                    	<ul class="list_words main-col">
                        <?php
						$opts = "";
						$arrayalphabets = range("A","Z");
						$arrayextraalphabets = array("Å","Ä","Ö");
						$arrayalphabets = array_merge($arrayalphabets,$arrayextraalphabets);
                        foreach($arrayalphabets as $char)
                        {
							$checked = "";
							
							if(isset($set) && $set!="")
							{
								$setalphabets = $set["alphabets"];
								
								if(in_array($char,$setalphabets))
								{
									$checked = "checked='checked'";
								}
								
								
							}
							echo '<li data-char="'.$char.'" class="li-alphabets"><label><input type="checkbox" name="alphabets[]" '.$checked.' value="'.$char.'" /><span>'.$char.mb_strtolower($char).'</span></label></li>';
                        }
						
						if(isset($set) && is_array($set["alphabets"]))
						{
							foreach($set["alphabets"] as $alpha){
								$selected = "";
								if($alpha==$set["answer"])
								{
									$selected = "selected='selected'";
								}
								$opts .="<option value='".$alpha."' $selected>".$alpha.mb_strtolower($alpha)."</option>";
							}
						}
						?>
                        
                        </ul>
                    </div>
                </div>		
            </div>
          </div>
          <div class="control-group">
          	<label for="answer" class="control-label">Select Answer & Sound</label>
          	<div class="controls">
            	<div style="float:left">
            	<select id="answer" name="answer">
                <?php echo $opts?>
                </select>
                </div>
              
                <div style="float:left">
               
                <div class="jptdiv">
                <span class="aud">
                <div class="galleryimage audio-main">
				<?php 
				$alphabet_path = GLOBAL_AUDIO_DIR."alphabets/capital/";
				$checked = "";
				foreach (glob($alphabet_path."*.mp3") as $filename) {
					$basename = basename($filename,".mp3");
					if(isset($set["audio"]) && is_file(GLOBAL_AUDIO_DIR."alphabets/capital/".utf8_replace($set["audio"]).".mp3"))
					{
						if($set["audio"]==to_utf8($basename)){
							
							$checked = "checked='checked'";
						}else{
							$checked ="";
						}
					}
				?>
                <div class="selementholder soundholderdef" data-name="<?php echo $basename?>" data-category="audio">
                	<span><input type="radio" name="rdo_alphabet_audio" class="selectFile" data-category="audio" value="<?php echo to_utf8($basename)?>" <?php echo $checked?> data-file="<?php echo $basename?>"></span>
                   <span><img class="playAudio" data-src="<?php echo GLOBAL_AUDIO_URL?>alphabets/capital/<?php echo $basename?>.mp3" src="<?php echo base_url();?>assets/img/sound.png">
                    </span>
                    <span><?php echo to_utf8($basename)?></span>
                </div>        
             
                <?php	
				}
				?>
				</div>
				</span>
                </div>
                </div>
                <div style="clear:both"></div>
               <div class="audionounholder mediaholder"></div>
          	</div>
          </div>
          <div class="control-group">
            <label for="is_active" class="control-label">Set Status</label>
            <div class="controls">
            	<?php
				$selanswer = "";
                if(isset($set))
				{
					$selanswer = isset($set["is_active"])?$set["is_active"]:"";
				}
				?>
                <select id="is_active" name="is_active">
                <option value="">--Select--</option>
                <option value="1" selected="selected" <?php echo ($selanswer=="1")?"selected='selected'":""?>>Active</option>
                <option value="0" <?php echo ($selanswer=="0")?"selected='selected'":""?>>Inactive</option>
                </select>	
            </div>
          </div>
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="button" onclick="document.location.href='<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>'">Cancel</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>
     <div class="pvw_loader">Loading...</div> 