    <script type="text/javascript">
	//SET GLOBAL AJAX URL TO BE USED FOR AJAX CALLS
	GlobalAjaxUrl = '<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>';
	$(document).ready(function(){
		
		//REMOVE SELECTED ITEM
		$(document.body).on("click",".delete-absolute",function(){
			$type = $(this).data("type");
			$id = $(this).data("id");
			$("#selected_word option[value='"+$id+"']").remove();
			$(".mainwords[value='"+$id+"']").prop("checked",false);
			$(this).parent(".relative").remove();
		});
		
		$(".show-hide-selected-images").on("click",function(){
			if($(".tivoli-main-words-img").is(":visible"))
			{
				$(this).html("Show Images");
				$(".tivoli-main-words-img").hide();
			}else{
				$(this).html("Hide Images");
				$(".tivoli-main-words-img").show();
			}
		});
		
		//CLICK EVENT TO THE CHECKBOX/RADIO-BUTTON. UPON CLICKING DISPLAY SELECTED IMAGE AND SET THE SELECTED WORD VALUE TO HIDDEN FIELD OR THE HIDDEN COMBOBOX
		$(document.body).on("click",".mainwords",function(){
			$current_check_val = $(this).val();
			$current_check_text = $.trim($(this).parent("label").text());
			var checkedBoxlength=$(".mainwords:checked").length;
			
			//POPULATE IMAGE OF SELECTED WORDS
			
			if($(this).prop("checked")){
				if($("#selected_word option[value='"+$current_check_val+"']").length > 0){
					alert("This image already added. Please select different one");
					return false;
				}
				$("#selected_word").append("<option value='"+$current_check_val+"'>"+$current_check_text+"</option>");
				
				
				//FETCH "imgsrc" DATA ATTRIBUTE FROM CHECKBOXES AND SHOW THE IMAGE OF SELECTED WORDS
				if($(this).data("imgsrc")!="")
				{
					$imgsrc = "<?php echo GLOBAL_IMG_URL?>"+$(this).data("imgsrc")+".png";
					$(".tivoli-main-words-img").append("<div class='tivoli-words-image relative main"+$current_check_val+"'><img src='"+$imgsrc+"' title='"+$current_check_text+"' style='height:120px' /><div style='text-align:center;font-weight:normal;color:#000'>"+$current_check_text+"</div><div class='delete-absolute' data-id='"+$current_check_val+"' data-type='main'>x</div></div>").show();
				}
			}else{ //CHECKBOX UNCHECKED, REMOVE ALL THE STUFFS RELATED TO UNCHECKED CHECKBOX
				$("#selected_word option[value='"+$current_check_val+"']").remove();
				$(".tivoli-main-words-img").find(".main"+$current_check_val).remove();
			}
			
		});
		
		<?php
		if(isset($set) && $set!="") //in case of update show the words starting with the first character of the word that comes from database
		{
		?>
		hideAlphabets("<?php echo $set["alphabet_group"]?>");
		<?php
		}else{
		?>
		loadWords();
		hideAlphabets();
		<?php
		}
		?>
		
		//WHEN SORT ALPHABETS ARE CLICKED
		$(document.body).on("click",".loadcharwords",function(){
			$(".loadcharwords").parent("li").removeClass("active");
			$(this).parent("li").addClass("active");
			loadWords("alpha",$(this).text());
		});
		
		//WHEN SEARCH BUTTON IS CLICKED
		$(".loadcharwords_search").on("click",function(){
			$keyword = $(this).prev(".searchstring").val();
			$keyword = ($keyword!="")?$keyword:"A";
			loadWords("search",$keyword);
		});
		
		
		//WHEN FILTER BY TAGS IS SELECTED
		$(document.body).on("change",".loadcharwords_filter",function(){
			$keyword = $(this).val();
			loadWords("tags",$keyword);
		});
		
		$("#alphabet_group").on("change",function(){
			$(".tivoli-main-words-img").empty();
			$("#selected_word").empty();
			hideAlphabets($(this).val());
			
			preSelectCheckboxes();
		});
	});
	
	
	function hideAlphabets(except)
	{
		var ex = except || "ABCD";
		
		var explode = ex.split('');
		$(".pagination ul").find("li").remove();
		
		for(var i=0;i<explode.length;i++){
			$(".pagination ul").append('<li data-char="'+explode[i]+'"><a href="javascript:void(0)" class="loadcharwords">'+explode[i]+'</a></li>');
		}
		
		loadWords("alpha",ex[0]);
	}
	
	function preSelectCheckboxes(){
		$("#selected_word").children("option").each(function(){
			$(".mainwords[value='"+$(this).attr("value")+"']").prop("checked");
		});
	}
	
	function submitForm(){
		$mainlength = $("#selected_word").children("option").length;
		var errors = "";
		
		if(!$mainlength>0)
		{
			errors +="Choose a word from the list.\n";
		}
		
		if(errors!=""){
			alert("Please fix the following error(s)\n\n"+errors);
			return false;
		}
		$("#selected_word option").prop("selected",true);
		//alert($unrhyming);
		return true;
	}
	</script>
    <div class="container top">
      
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url()."exercise"; ?>">
            <?php echo "Exercises";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
        <a href="<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>">
          <?php echo ucwords($this->uri->segment(1))?>
          </a>
           <span class="divider">/</span>
        </li>
        <li class="active">
        <?php echo "Toystore 2-1"?> 
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          <?php echo "Toystore 2-1 - ".$exercise_desc[0]?> 
        </h2>
      </div>
 
      <?php
      //flash messages
	  
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == "done")
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_SUCCESS_MSG;
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_ERROR_MSG;
          echo '</div>';          
        }
      }
	  if($this->session->flashdata('errors'))
	  {
		   echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			echo "<b>".$this->session->flashdata('errors')."</b>";
			 echo '</div>'; 
	  }
      ?>
      
      <?php
	  
	
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '','onsubmit'=>'return submitForm()');
      
      //form validation
      echo validation_errors();
	 
      if($action=="Add")
	  {
      	echo form_open('toystore/toystore_2_1/add', $attributes);
	  }else{
		echo form_open('toystore/toystore_2_1/update/'.$this->uri->segment(4).'', $attributes);
	  }
	 	$selectedword = "";
	   $imagesforselectedword = "";
	   if(isset($set) && $set!="")
		{
			$group = $set["alphabet_group"];
			//$this->utilities->printr($set[$group],1);
			foreach($set[$group] as $word)
			{
				$selectedword .="<option value='".$word["word_id"]."'>".$word["word"]."</option>";
				$imagesforselectedword .= "<div class='tivoli-words-image relative main".$word["word_id"]."'><img src='".GLOBAL_IMG_URL.$word["imageRef"]."' title='".$word["word"]."' style='height:120px' />
			<div style='text-align:center;font-weight:normal;color:#000'>".$word["word"]."</div>
			<div class='delete-absolute' data-id='".$word["word_id"]."' data-type='main'>x</div>
			</div>";
			
			}
			
			
		}
      ?>
      	<div class="selected-words-images" style="margin-left:68px">
        <?php
		$displayimages = "display:block";
        if(trim($imagesforselectedword)!="")
		{
			echo "<a href='javascript:void(0)' class='show-hide-selected-images btn btn-warning' style='margin-left:32px'>Show Images</a>";
			$displayimages = "display:none";
		}
		?>
        <div class="tivoli-main-words-img" style="margin-top:15px;margin-left:27px;<?php echo $displayimages?>"><?php echo $imagesforselectedword?></div>
        </div>
        <div style="clear:both;"></div>
        <select id="selected_word" name="selected_word[]" style="display:none" multiple="multiple">
            <?php echo $selectedword;?>
            </select>
        <fieldset>
      
          <div class="control-group">
           <input type="hidden" id="exercise_id" name="exercise_id" value="<?php echo $exercise_desc[2]?>" />
            <label for="inputError" class="control-label">Select Word</label>
            
            <div class="controls">
                <div class="tivoli-1-full">
                	<div class="alphabet-groups">
	                    <div class="upl tivoli-1-sub-head fl">Select a group</div>
                        <div class="fl" style="margin-left:20px;">
                        	<select id="alphabet_group" class="alphabet-group" name="alphabet_group">
                            	<?php
                                	if(isset($set) && isset($set["alphabet_group"]))
									{
										echo "<option value='".$set["alphabet_group"]."'>".$set["alphabet_group"]."</option>";
									}else{
								?>
                                		<option value="ABCD">ABCD</option>
                                        <option value="EFGH">EFGH</option>
                                        <option value="IJKL">IJKL</option>
                                        <option value="MNOP">MNOP</option>
                                <?php
									}
								?>
                            </select>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                	<div class="upl tivoli-1-sub-head" style="padding-bottom:5px;">Select a word</div>
                    <?php echo $gen_alphabets?>
                    <div style="max-height:500px;overflow:auto;">
                    	<ul class="list_words main-col"></ul>
                    </div>
                </div>		
            </div>
          </div>
          <div class="control-group">
            <label for="is_active" class="control-label">Set Status</label>
            <div class="controls">
            	<?php
				$selanswer = "";
              	if(isset($set))
				{
					$selanswer = isset($set["is_active"])?$set["is_active"]:"";
				}
				?>
                <select id="is_active" name="is_active">
                <option value="">--Select--</option>
                <option value="1" selected="selected" <?php echo ($selanswer=="1")?"selected='selected'":""?>>Active</option>
                <option value="0" <?php echo ($selanswer=="0")?"selected='selected'":""?>>Inactive</option>
                </select>	
            </div>
          </div>
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="button" onclick="document.location.href='<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>'">Cancel</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>
     <div class="pvw_loader">Loading...</div>