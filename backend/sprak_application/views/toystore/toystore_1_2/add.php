    <script type="text/javascript">
	//SET GLOBAL AJAX URL TO BE USED FOR AJAX CALLS
	GlobalAjaxUrl = '<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>';
	
	$(document).ready(function(){
		$(".li-alphabets").on("click",function(){
			$checkedlength = $(".li-alphabets :checked").length;
			$("#answer").empty();
			$(".li-alphabets :checked").each(function(){
				$("#answer").append("<option value='"+$(this).val()+"'>"+$(this).next("span").html()+"</option>");
			});
			if($checkedlength>4){
				alert("You can't select more than 4 alphabets");
				return false;
			}
		});
		
		
		
			
			$(document.body).on("click",".playAudio",function(){
				var audioSrc = $(this).data("src");
				$(".audiopreviewholder").empty().html("<audio class='audioFile' id='html5audio' src='"+audioSrc+"' ></audio>");
				document.getElementById("html5audio").play();
			});
			
			
			$(document.body).on("click",".selectFile",function(){
				if($(this).prop("checked")){
					$len = $(".selected-stuffs").length;
					console.log("LENGTH = "+$len);
					if($len>2) { 
						alert("You can't select more than 3 sounds");
						$(this).prop("checked",false);
						return false;
					}
					$(".select-label").show();
					$(".selected-audios-div").append("<div class='selected-stuffs'><span class='chk'><input class='answer-radio' type='radio' value='"+$(this).val()+"'/></span><span class='audio-png'><img class='playAudio' data-src='"+$(this).data("src")+"' src='<?php echo base_url()?>assets/img/sound.png' /></span><span class='audio-name'>"+$(this).val()+"</span><input class='hidden-selected-audios' type='hidden' name='selected_audios[]' value='"+$(this).val()+"'><span class='remove-from-selected'>x</span></div>");
				}
			});
			
			$(document.body).on("click",".remove-from-selected",function(){
				$(this).parent(".selected-stuffs").remove();
			});
			
			$(document.body).on("click",".answer-radio",function(){
				if($(this).prop("checked")){
					$word = $(this).parent().parent(".selected-stuffs").find(".audio-name").html();
					$("#answer_alphabet").val($word.charAt(0));
					$("#answer").val($word);
				}
			});
			
			
	});
	
	
	function submitSet(){
		$selectedlength = $(".hidden-selected-audios").length;
		
		
		var errors = "";
		
		if($selectedlength<3)
		{
			errors +="- You must choose 3 audios.\n";
		}
		$answer_alphabet = $("#answer_alphabet").val();
		
		if($answer_alphabet=="")
		{
			errors +="- You must check the answer among the selected audios.\n";
		}
		
		var checkedArray = new Array();
		$(".audio-name").each(function(){
			checkedArray.push($(this).html().charAt(0));
		});
		
		if(hasDuplicates(checkedArray)){
			errors +=" - There are two ore more sounds that starts with same alphabet, they should be different";
		}
		
		
		if(errors!=""){
			alert("Please fix the following error(s)\n\n"+errors);
			return false;
		}
	
		//alert($unrhyming);
		return true;
	}
	
	
	</script>
    <div class="container top">
      
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url()."exercise"; ?>">
            <?php echo "Exercises";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
        <a href="<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>">
          <?php echo ucwords($this->uri->segment(1))?>
          </a>
           <span class="divider">/</span>
        </li>
        <li class="active">
        <?php echo "Toystore 1-2"?>
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          <?php echo "Toystore 1-2 - ".$exercise_desc[0]?> 
        </h2>
      </div>
 
      <?php
      //flash messages
	  
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == "done")
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_SUCCESS_MSG;
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_ERROR_MSG;
          echo '</div>';          
        }
      }
	  if($this->session->flashdata('errors'))
	  {
		   echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			echo "<b>".$this->session->flashdata('errors')."</b>";
			 echo '</div>'; 
	  }
      ?>
      
      <?php
	  
	 $editmode = isset($exercises)?1:0;
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '','onsubmit'=>'return submitSet();');
      
      //form validation
      echo validation_errors();
	 
      if($action=="Add")
	  {
      	echo form_open('toystore/toystore_1_2/add', $attributes);
	  }else{
		echo form_open('toystore/toystore_1_2/update/'.$this->uri->segment(4).'', $attributes);
	  }
	  
	  $selected_items = "";
	  $answer_audio = "";
	  $answer_alphabet = "";
	  if(isset($set))
	  {
		  $selected_sound = $set["sound_name"];
		  $answer_audio = $set["answer_audio_name"];
		  $answer_alphabet = $set["answer_alphabet"];
		  for($i=0;$i<3;$i++)
		  {
			  $checked ="";
			  if($set["answer_audio_name"]==$selected_sound[$i])
			  {
				  $checked = "checked='checked'";
			  }
			  $selected_items .= "<div class='selected-stuffs'><span class='chk'><input class='answer-radio' ".$checked." type='radio' value='".$selected_sound[$i]."'/></span><span class='audio-png'><img class='playAudio' data-src='".$set["audios"][$i]."' src='".base_url()."assets/img/sound.png' /></span><span class='audio-name'>".$selected_sound[$i]."</span><input class='hidden-selected-audios' type='hidden' name='selected_audios[]' value='".$selected_sound[$i]."'><span class='remove-from-selected'>x</span></div>";
		  }
	  }
      ?>
      
        <fieldset>
        <div class="audiopreviewholder"></div>
        <div class="loadmedia-div" style="display:none"><a href="javascript:void(0)" data-href="<?php echo site_url()?>words/medialoader/audio/noun?element=hid_selected_audio_file" class="btn btn-warning loadmedia" data-target="audionounholder">Select Audio</a></div>
         <input type="hidden" id="exercise_id" name="exercise_id" value="<?php echo $exercise_desc[2]?>" />
      <input type="hidden" name="answer_alphabet" id="answer_alphabet" value="<?php echo $answer_alphabet?>" />
      <input type="hidden" name="answer" id="answer" value="<?php echo $answer_audio?>" />
          <div class="control-group">
          	<label for="answer" class="control-label">Select Audios</label>
          	<div class="controls">
            	<div class="audionounholder mediaholder">
                <div class="galleryimage audio-main">
				<?php 
				$alphabet_path = GLOBAL_AUDIO_DIR."alphabets/capital/";
				$checked = "";
				foreach (glob($alphabet_path."*.mp3") as $filename) {
					$basename = basename($filename,".mp3");
					if(isset($set["audio"]) && is_file(GLOBAL_AUDIO_DIR."alphabets/capital/".utf8_replace($set["audio"]).".mp3"))
					{
						if($set["audio"]==to_utf8($basename)){
							
							$checked = "checked='checked'";
						}else{
							$checked ="";
						}
					}
				?>
                <div class="selementholder soundholderdef" data-name="<?php echo $basename?>" data-category="audio">
                	<span><input type="radio" name="rdo_alphabet_audio" class="selectFile" data-category="audio" value="<?php echo to_utf8($basename)?>" <?php echo $checked?> data-file="<?php echo $basename?>"></span>
                   <span><img class="playAudio" data-src="<?php echo GLOBAL_AUDIO_URL?>alphabets/capital/<?php echo $basename?>.mp3" src="<?php echo base_url();?>assets/img/sound.png">
                    </span>
                    <span><?php echo to_utf8($basename)?></span>
                </div>        
             
                <?php	
				}
				?>
				</div>
                </div>
                <div class="select-label" <?php echo (isset($set))?"style='display:block'":""?>>Selected Audios (Check one of the selected audios to mark as an answer audio)</div>
                <div class="selected-audios-div"><?php echo $selected_items?></div>
          	</div>
          </div>
          <div class="control-group">
            <label for="is_active" class="control-label">Set Status</label>
            <div class="controls">
            	<?php
				$selanswer = "";
                if(isset($set))
				{
					$selanswer = isset($set["is_active"])?$set["is_active"]:"";
				}
				?>
                <select id="is_active" name="is_active">
                <option value="">--Select--</option>
                <option value="1" selected="selected" <?php echo ($selanswer=="1")?"selected='selected'":""?>>Active</option>
                <option value="0" <?php echo ($selanswer=="0")?"selected='selected'":""?>>Inactive</option>
                </select>	
            </div>
          </div>
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="button" onclick="document.location.href='<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>'">Cancel</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>
     <div class="pvw_loader">Loading...</div> 