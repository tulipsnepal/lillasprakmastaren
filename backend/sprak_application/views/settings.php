 <div class="container top">
     
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        
        <li class="active">
         <?php echo "Settings"?>
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          Exercise Settings
        </h2>
      </div>
 
      <?php
      //flash messages
	  
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == "done")
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_SUCCESS_MSG;
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_ERROR_MSG;
          echo '</div>';          
        }
      }
	  if($this->session->flashdata('errors'))
	  {
		   echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			echo "<b>".$this->session->flashdata('errors')."</b>";
			 echo '</div>'; 
	  }
      ?>
      
      <?php
	  
	  $editmode = isset($words)?1:0;
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');
      
      //form validation
      echo validation_errors();
      
      echo form_open('settings/save', $attributes);
	  
      ?>
        <fieldset class="general-settings">
          <div class="control-group">
            <label for="inputError" class="control-label">Long/Short Word<div class="help-inline">Minimum length of the word to be a long word</div><br /></label>
            <div class="controls">
            
              <input type="text" id="longest_word_min_length" name="longest_word_min_length" value="<?php echo (isset($settings["longest_word_min_length"]))?$settings["longest_word_min_length"]:""?>" style="width:280px">
              <br />
              <span class="help-inline">If you change this settings, you will need to  create new JSON file.</span>
            </div>
          </div>
          
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Save changes</button>
          </div>
        </fieldset>
		
      <?php echo form_close(); ?>

    </div>