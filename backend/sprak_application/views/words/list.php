  	
    <script type="text/javascript">
	GlobalAjaxUrl = '<?php echo base_url()."game_words"?>';
		$(document).ready(function(){
			$(".bulkaction").click(function(e){
			var error ="";
			var action = $(this).parent("div").prev("div").find(".bulkactionselect").val();
			if($(".exercise_sets:checked").length==0)
			{
				error +=" - At least select something.\n";
			}
			if(action=="")
			{
				error +=" - Select the action.\n";
			}
			if(error!="")
			{
				alert("Please fix the following error(s):-\n"+error);
				return false;
			}else{
				
				$("#bulkaction").val(action);
				if(action==2)
				{
					if(confirm("This will permanently delete the selected words along with associated images and audios. Are you sure you want to delete?"))
					{
						document.frmList.action = "<?php echo base_url()?>words/bulkaction";
						document.frmList.submit();
					}
				}else{
					document.frmList.action = "<?php echo base_url()?>words/bulkaction";
					document.frmList.submit();
				}
				return false;
			}
		});
			//delete word permanently
			$(".delete-word").click(function(){
				if($("input[name='words_delete[]']:checked").length==0){
					alert("At least select one record to delete");
					return false;
				}else{
					if(confirm("This will permanently delete the selected words along with associated images and audios. Are you sure you want to delete?"))
					{
						document.frmList.action = "<?php echo base_url()?>words/bulkdelete";
						document.frmList.submit();
					}
				}
			});
			
			//delete audios permanently
			$(".delete-audios").click(function(){
				if($("input[name='audio_delete[]']:checked").length==0){
					alert("At least select one file to delete");
					return false;
				}else{
					if(confirm("This will permanently delete the selected audios. Are you sure you want to delete?"))
					{
						document.frmDeleteAudios.submit();
					}
				}
			});
			
			
			$(".playAudio").click(function(){
				var audioSrc = $(this).data("src");
				$(".audiopreviewholder").empty().html("<audio class='audioFile' id='html5audio' src='"+audioSrc+"' ></audio>");
				document.getElementById("html5audio").play();
			});
			
			if($("#showimages").prop("checked")){
				$("#showimages").trigger("click");
			}
			
			$("#showimages").on("click",function(){
				if($(this).prop("checked")){
					//add parameter to CI pagination show that image can be shown/hidden as per check status when user browses through pages
					/*$(".pagination").find("ul").find("li").each(function(){
						$href = $(this).find("a").attr("href");
						$newhref = $href+"?images="+$("#showimages").val();
						 $(this).find("a").attr("href",$newhref);
					});*/
					$(".imagecolumn").not("th").each(function(){
						var imagesrc = $(this).data("imgsrc");
						if(imagesrc!="-")
						{
							$obj = $(this);
							$(this).find(".imgholder").html("<a href='"+imagesrc+"' class='fancybox'><img src='"+imagesrc+"' class='words-image' width='80' /></a>");
							$(this).find(".imgholder").find("img").on("load",function(){
								$(this).parent("a").parent("div").parent("td").find(".image-loader").hide();
							})
						}else{
							$(this).find(".imgholder").html("-");
							$(this).find(".image-loader").hide();
						}
					});
					$(".imagecolumn").show();
				}else{
					/*$(".pagination").find("ul").find("li").each(function(){
						$href = $(this).find("a").attr("href");
						$newhref = $href.split("?");
						 $(this).find("a").attr("href",$newhref[0]);
					});*/
					$(".imagecolumn").hide();
				}
			});
			
			
			//OPEN MODEL WINDOW FOR CREATING TAGS
			$('[data-toggle="modal"]').click(function(e) {
			  e.preventDefault();
			  var url = $(this).attr('href');
			  //var modal_id = $(this).attr('data-target');
			  $.get(url, function(data) {
				  $(".modal").remove();
				  $("<div class='modal fade hide' role='dialog'></div>").html(data).modal();
			  });
			});
			$(".wordfilter").val("");
			$(".selectpicker").selectpicker("refresh");
		});
	</script>
    <div class="container top">
<div class="audiopreviewholder" style="display:none"></div>
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <?php echo ucfirst($this->uri->segment(1));?>
        </li>
      </ul>

      <div class="page-header users-header">
        <h2>
          <?php echo ucfirst($this->uri->segment(1));?> 
          <?php /*?><a  href="<?php echo site_url().$this->uri->segment(1); ?>/add" class="btn btn-success">Add a new</a><?php */?>
        </h2>
      </div>
      
      <div class="row">
      <?php 
	if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == "done")
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_SUCCESS_MSG;
          echo '</div>';       
        }elseif($this->session->flashdata('flash_message') == "not_done"){
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_ERROR_MSG;
          echo '</div>';          
        }else{
          echo '<div class="alert alert-info">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo $this->session->flashdata('flash_message');
          echo '</div>';          
        }
      }	
	?>
        <div class="span12 columns">
         <?php
          if($show!="orphaned_audios")
		  {
		  ?>
          <div class="well">
           
            <?php
            $attributes = array('class' => 'form-inline reset-margin', 'id' => 'myform');
           
            //save the columns names in a array that we will use as filter         
            $filter_tags = array();  
			//$this->utilities->printr($tags,1);
			$filter_tags["-1"]="--Select Filter--";  
			
            foreach ($tags as $value) {
              $filter_tags[$value["tag_id"]] = $value["tag_name"];
            }
			
            echo form_open('words/'.$show, $attributes);
     
              echo form_label('Search:', 'search_string');
              echo form_input('search_string', "", 'style="width: 170px;
height: 26px;"');

             

              echo form_label('Filter by:', 'filter_by');
              echo form_dropdown('filter_by', $filter_tags, $filter_by, 'class="span2 wordfilter selectpicker" data-style="btn-info" title="Filter by Tags"');

              $data_submit = array('name' => 'mysubmit', 'class' => 'btn btn-primary', 'value' => 'Go');

              $options_order_type = array('Asc' => 'Asc', 'Desc' => 'Desc');
			  echo "&nbsp;&nbsp;";
              echo form_dropdown('order_type', $options_order_type, $order_type_selected, 'class="span1 selectpicker" data-style="btn-info"');
			 echo "&nbsp;&nbsp;";
              echo form_submit($data_submit);
			 
			 
            echo form_close();
            ?>
			
          </div>
        <?php
		  }
		  
		   if((isset($search_string_selected) && trim($search_string_selected)!="") || (isset($filter_by) && trim($filter_by)!="")) 
		  {
			$displaytext = "Search/Filter results for ";
		  }else{
			$displaytext = "Displaying ";
		  }
		?>
          <div id="tag-creater-modal-holder">
    </div>
        
        <?php
          if($show!="orphaned_audios")
		  {
		?>
		 <?php /*?><div class="checkbox" style="padding:10px 0 10px 0; float:right">
            <label>
              <input type="checkbox" name="showimages" id="showimages" value="1" <?php echo (isset($_GET["images"]))?"checked='checked'":""?> /> Show Images
            </label>
          </div><div style="clear:both"></div><?php */?>
         <?php 
		  }
		 ?>
           
           <?php
          if($show!="orphaned_audios")
		  {
		  ?>
          <div style="float:left">
        <select class="selectpicker bulkactionselect" name="bulkaction">
        <option value="">- With Selected - </option>
        <option value="2">Delete</option>
        <option value="1">Make Active</option>
        <option value="0">Make Inactive</option>
        </select></div><div style="float:left;margin-left:15px;">
        <input type="button" class="btn btn-danger bulkaction"   value="  GO  "/></div>
           
          <?php
		  }else{
		?>
        <input type="button" class="btn btn-danger delete-audios" value="Delete selected audios"  />
        <?php
		  }
		  ?> 
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url();?>words/clean_audios" class='btn btn-warning'>Show words with missing audio</a> &nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url()?>words/orphaned_audios" class="btn btn-primary">Show Audios not linked with any words</a><br /><br />
            <div style="padding-bottom:20px;">
          <?php
          if($show=="all")
		  {
			  echo $displaytext."all words.";
		  }else if($show=="noaudio")
		  {
			  echo $displaytext."all words with missing audios only. Click <b><a href='".base_url()."words'>here</a></b> to display all words.";
		  }else if($show=="orphaned_audios")
		  {
			  echo $displaytext."all audios not associated with any words. Click <b><a href='".base_url()."words'>here</a></b> to display all words.";
		  }
		  ?>
          </div>
          <?php
          if($show!="orphaned_audios")
		  {
		  ?>
          <form name="frmList" id="frmList" method="post" action="<?php echo base_url();?>words/linktags">
          
          <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr class="tableheads">
                <th width="2%"><input type="checkbox" id="checkallsets" /></th>
                <th width="4%" height="41" class="header">#</th>
                <th class="red header imagecolumn" width="8%">Image</th>
                <th class="yellow header headerSortDown" width="28%">Words</th>
                 <th class="red header" width="30%"><span style="float:left">Tags</span><span style="float:right;padding-right:15px">
                 <a href="<?php echo base_url();?>words/createtags" class="btn btn-link addtags" id="fancybox-ajax" type="button" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus"></span> Link Tags with words</a></span></th>
                <th class="red header" width="8%">Audio</th>
                <th class="red header" width="8%">Status</th>
                <th class="red header" width="12%">Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php
			  $i = 1;
			  $currentpage = ($this->uri->segment(2)>0)?$this->uri->segment(2):1;
			  if($words)
			  {
				  foreach($words as $row)
				  {
					$recno = RECORDS_PER_PAGES * ($currentpage-1)+$i;
					$deleterow = $row["word_status"]!=1?'class="danger deleted-items"':"";
					echo '<tr '.$deleterow.'>';
					echo "<td>";
					if(!in_array($row["word_id"],$this->used_words))
					{
						echo '<input type="checkbox" name="words_delete[]" class="exercise_sets" value="'.$row['word_id'].'" />';
					}
					echo "</td>";
					echo '<td>'.$recno.'</td>';
					if(trim($row["imageRef"])!="")
					{
						$img = GLOBAL_IMG_URL.urldecode($row["imageRef"]).".png";
					}else{
						$img = "-";
					}
					echo '<td class="imagecolumn" data-imgsrc="'.$img.'" style="text-align:center;position:relative"><div class="image-loader"></div><div class="imgholder"></div></td>';
					echo '<td>'.$row['word'].'</td>';
					
					$tagoptions ="<select class='selectpicker show-menu-arrow' name='selectedtags[".$row["word_id"]."][]' multiple title='' data-selected-text-format='count>4'>";
					$sel = "";
					foreach ($tags as $value) {
					  $sel = (in_array($value["tag_id"],explode(",",$row["tags"])))?" selected='selected'":"";
					  $tagoptions .= "<option  value='".$value["tag_id"]."' $sel >".$value["tag_name"]."</option>";
					}
					$tagoptions .="</select>";
					
					echo '<td class="selecttags">'.$tagoptions.'</td>';
					
					
					if(trim($row["audioRef"])!="" && is_file(GLOBAL_AUDIO_DIR.$row["audioRef"].".mp3"))
					{
						$audio = "<a href='javascript:void(null)' class='playAudio' data-src='".GLOBAL_AUDIO_URL.$row["audioRef"].".mp3'><img src='".base_url()."assets/img/sound.png' width='22' /></a>";
						
					}else{
						$audio = "-";
					}
					
					
					
					echo '<td>'.$audio.'</td>';
				
					echo '<td>'.($row["word_status"]==1?"Active":"<span style='color:red'><i>Inactive</i></span>").'</td>';
					 echo '<td class="crud-actions">
					  <a href="'.site_url().'words/update/'.$row['word_id'].'" class="btn btn-info">view & edit</a> '; 
					/*if($row["word_status"]==0)
					{
						echo  '<a href="'.site_url().'words/undelete/'.$row['word_id'].'" class="btn btn-success">&nbsp;&nbsp;Activate&nbsp;&nbsp;&nbsp;</a>';
					}else{
						echo  '<a href="'.site_url().'words/delete/'.$row['word_id'].'" class="btn btn-danger">Deactivate</a>';
					}*/
					
					echo '</td>';
					echo '</tr>';
					$i++;
				  }
				  ?>
                  <tr>
                <td class="imagecolumn">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td style="text-align:right">
                <input type="submit" class="btn btn-info" value="Save Tags" />
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>  
                  <?php
			  }else{
				  echo "<tr><td colspan='7' style='text-align:center'>Records not available</td></tr>";
			  }
              ?>
                   
            </tbody>
          </table>
          <div style="float:left">
        <select class="selectpicker bulkactionselect" name="bulkaction">
        <option value="">- With Selected - </option>
        <option value="2">Delete</option>
        <option value="1">Make Active</option>
        <option value="0">Make Inactive</option>
        </select></div><div style="float:left;margin-left:15px;">
        <input type="button" class="btn btn-danger bulkaction"   value="  GO  "/></div><div style="clear:both"></div>
        <input type="hidden" name="bulkaction" id="bulkaction" />
			</form>
          <?php echo '<div class="pagination">'.$this->pagination->create_links().'</div>'; ?>
		  <?php
		  }else{
		?>
        <form method="post" id="frmDeleteAudios" name="frmDeleteAudios" action="<?php echo base_url()?>words/delete_audios">
        <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr class="tableheads">
                <th width="6%"><input type="checkbox" id="checkallsets" /></th>
                <th width="6%" height="41" class="header">#</th>
                <th class="yellow header headerSortDown" width="20%">Audios</th>
                <th class="header" width="10%">Listen</th>
                <th class="header">Similar words available</th>
              </tr>
            </thead>
            <tbody>
            <?php
			//$this->utilities->printr($orphaned_audio_list,1);
            if(count($orphaned_audio_list)>0)
			{
				$k=1;
				foreach($orphaned_audio_list as $alist)
				{
				  	echo "<tr>";
				 	echo '<td><input type="checkbox" name="audio_delete[]" class="exercise_sets" value="'.$alist["filename"].'" /></td>';
					echo "<td>$k</td>";
					echo "<td>".$alist["displayname"]."</td>";
					echo "<td><a href='javascript:void(null)' class='playAudio' data-src='".GLOBAL_AUDIO_URL.$alist["filename"]."'><img src='".base_url()."assets/img/sound.png' width='22' /></a></td>";
					echo "<td>".$alist["similar_word"]."</td>";
					echo "</tr>";
					$k++;
				}
			}else{
				echo "<tr><td colspan='4' style='text-align:center;'>Audios unassociated with any words is not available.</td></tr>";
			}
			?>
            </tbody>
         </table>
         <input type="button" class="btn btn-danger delete-audios" value="Delete selected audios"  />
         </form>
        <?php
		  }
		  ?>
      </div>
    </div>
    <div class="pvw_loader">Loading...</div>