	<script type="text/javascript">
		$(document).ready(function(){
			
			$(document.body).on("click",".loadmedia",function(){
				var obj = $(this);
				var searchstring = obj.data("searchfor");
				if(typeof searchstring ==="undefined")
				{
					searchstring = "A";
				}
				$.ajax({
					url: obj.data("href"),
					data: "url="+obj.data("href")+"&searchfor="+searchstring+"&target="+obj.data("target")+"&criteria=startingwith",
					type :"POST",
					beforeSend:function(){
						//$("."+obj.data("target")).html("<em>Loading medias...</em>");
						$(".pvw_loader").show();
					},
					success:function(data){
						$("."+obj.data("target")).html(data);
						obj.removeClass("loadmedia").addClass("togglemedialoader");
						$(".pvw_loader").hide();
					}
				});
			});
			
			$(document.body).on("click",".searchmedia",function(){
				var keyword = $(this).closest(".searchstring").val();
				var obj = $(this);
					var searchstring = $(this).prev(".searchstring").val();
					
					$.ajax({
						url: obj.data("href"),
						data: "url="+obj.data("href")+"&searchfor="+searchstring+"&target="+obj.data("target")+"&criteria=containing",
						type :"POST",
						beforeSend:function(){
							$(".pvw_loader").show();
						},
						success:function(data){
							$("."+obj.data("target")).html(data);
							obj.removeClass("loadmedia").addClass("togglemedialoader");
							$(".pvw_loader").hide();
						}
					});
			});
			
			$(document.body).on("click",".togglemedialoader",function(){
				$("."+$(this).data("target")).toggle();
			});
			
			$(document.body).on("click",".playAudio",function(){
				var audioSrc = $(this).data("src");
				$(".audiopreviewholder").empty().html("<audio class='audioFile' id='html5audio' src='"+audioSrc+"' ></audio>");
				document.getElementById("html5audio").play();
			});
			
			$(document.body).on("click",".selectFile",function(){
				var category = $(this).data("category");
				if($(this).prop("checked")){
					
					if(category=="image")
					{
						$(".hid_"+category).val($(this).val());
						$(".div_"+category).html("<div class='tivoli-words-image main2'><img src='<?php echo GLOBAL_IMG_URL?>"+$(this).data("file")+".png' style='height:126px' /></div>").show();
					}else{
						$(".hid_"+category).val($(this).val());
						$(".div_"+category).find(".aud").html("<img class='playAudio' data-src='<?php echo GLOBAL_AUDIO_URL?>"+$(this).data("file")+".mp3' src='<?php echo base_url()."assets/img/sound.png"?>' />");
					}
					//return false;
				}
			});
			
			$(document.body).on("click",".selementholder",function(event){
				var category = $(this).data("category");
				$chkbox = $(this).find(".selectFile[data-category='"+category+"']");
				
				$chkbox.prop("checked",true);
				if (!$(event.target).is(".selectFile[data-category='"+category+"']")) {
					console.log($chkbox.val());
					$chkbox.trigger("click");
				}
			});
			
			$(document.body).on("keypress",".searchstring",function(e){
				if(e.keyCode=="13")
				{
					e.preventDefault();
					$(this).next(".searchmedia").trigger("click");
				}
			})
			
			
			
			//show loader for images
		/*	$(".words-image").on("load",function(){
				$(this).parent(".img").parent().find(".image-loader").hide();
			});*/
			
			
		});
	
	
	</script>
    <div class="container top">
      <div class="audiopreviewholder"></div>
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url().$this->uri->segment(1); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
         <?php echo $action?>
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          Manage words & sounds
        </h2>
      </div>
 
      <?php
      //flash messages
	  
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == "done")
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_SUCCESS_MSG;
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_ERROR_MSG;
          echo '</div>';          
        }
      }
	  if($this->session->flashdata('errors'))
	  {
		   echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			echo "<b>".$this->session->flashdata('errors')."</b>";
			 echo '</div>'; 
	  }
      ?>
      
      <?php
	  
	  $editmode = isset($words)?1:0;
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');
      
      //form validation
      echo validation_errors();
      if(strtolower($action)=="add")
	  {
      	echo form_open_multipart('words/add', $attributes);
	  }else{
		echo form_open_multipart('words/update/'.$this->uri->segment(3).'', $attributes);
	  }
      ?>
        <fieldset>
        <div id="audiopreviewholder"></div>
          <div class="control-group">
            <label for="inputError" class="control-label">Word/Phrase</label>
            <div class="controls">
              <input type="text" id="word" name="word" value="<?php echo ($editmode==1)?$words["word"]:""?>" style="width:280px">
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Select Image File</label>
            <div class="controls">
            	
            	<div><a href="javascript:void(0)" data-href="<?php echo site_url()?>words/medialoader/image?element=hid_selected_image_file" data-target="imgholder" class="btn btn-info loadmedia">Select Image</a>
                <input type="text" name="hid_selected_image_file" id="hid_selected_image_file" class="hid_image selectedmediafile" value="<?php echo ($editmode==1)?to_utf8($words["imageRef"]):""?>" readonly="readonly" />
                </div>
                <?php
				$impath="";
				$style="display:none;";
				if(isset($words["imageRef"]) && is_file(GLOBAL_IMG_DIR.utf8_decode($words["imageRef"]).".png"))
				{
					$style="display:block;";
					$impath = "<img src='".GLOBAL_IMG_URL.$words["imageRef"].".png' style='height:120px' />";
				}
				?>
                 <div style="float:left;margin-top:10px;margin-left:12px;<?php echo $style?>" class="tivoli-words-image hid_selected_image_file_div div_image"><div class="tivoli-words-image main2">
                <?php echo $impath?>
                </div></div>
                
                
                <div class="imgholder mediaholder" style="float:left;margin-top:9px;width:850px;"></div>
               
                
               
            </div>
          </div>          
          <div class="control-group">
            <label for="inputError" class="control-label">Sound File</label>
            <div class="controls">
            	<div style="float:left"><a href="javascript:void(0)" data-href="<?php echo site_url()?>words/medialoader/audio/noun?element=hid_selected_audio_noun_file" class="btn btn-warning loadmedia" data-target="audionounholder">Select Noun Audio</a></div>
                <div style="float:left">
                <?php
				$audiopath = "";
				
				if(isset($words["audioRef"]) && is_file(GLOBAL_AUDIO_DIR.$words["audioRef"].".mp3"))
				{
					$audiopath = "<img class='playAudio' data-src='".GLOBAL_AUDIO_URL.$words["audioRef"].".mp3' src='".base_url()."assets/img/sound.png' />";
				}
				?>
                <div class="audio_noun_holder hid_selected_audio_noun_file_div selectedsound div_noun">
                <span class="aud"><?php echo $audiopath;?></span>
                <span class="fname">
                <input type='text' name='hid_selected_audio_noun_file' id='hid_selected_audio_noun_file' class='hid_audio selectedmediafile' value='<?php echo ($editmode==1)?to_utf8($words["audioRef"]):""?>' readonly='readonly' />
                </span>
                </div>
                </div>
                <div style="clear:both"></div>
               <div class="audionounholder mediaholder"></div>
               
                
            </div>
          </div>
          
         <?php /*?><div class="control-group">
            <label for="inputError" class="control-label">Sound File</label>
            <div class="controls">
            	<div style="float:left"><a href="javascript:void(0)" data-target="audionounverbholder" data-href="<?php echo site_url()?>words/medialoader/audio/noun_and_verbs?element=hid_selected_audio_noun_verb_file" class="btn btn-success loadmedia">Select Noun-Verb Audio</a></div>
                <div style="float:left">
                 <?php
				$audiopath = "";
				if(isset($words["audioNounVerbRef"]) && is_file(GLOBAL_AUDIO_DIR.$words["audioNounVerbRef"].".mp3"))
				{
					$audiopath = "<img class='playAudio' data-src='". GLOBAL_AUDIO_URL.$words["audioNounVerbRef"].".mp3' src='".base_url()."assets/img/sound.png' />";
				}
				?>
                <div class="audio_noun_verb_holder hid_selected_audio_noun_verb_file_div selectedsound div_noun_and_verbs">
                <span class="aud"><?php echo $audiopath;?></span>
                <span class="fname">
                 <input type="text" name="hid_selected_audio_noun_verb_file" id="hid_selected_audio_noun_verb_file" class="hid_noun_and_verbs selectedmediafile" value="<?php echo ($editmode==1)?to_utf8($words["audioNounVerbRef"]):""?>"readonly='readonly' />
                </span>
                </div>
                </div>
                <div style="clear:both"></div>
                <div class="audionounverbholder mediaholder"></div>
               
               
                
            </div>
          </div><?php */?>
          
          <?php /*?><div class="control-group">
            <label for="inputError" class="control-label">Sound File</label>
            <div class="controls">
            	<div style="float:left"><a href="javascript:void(0)" data-target="audioverbholder" data-href="<?php echo site_url()?>words/medialoader/audio/verbs?element=hid_selected_audio_verb_file" class="btn btn-danger loadmedia">Select Verbs Audio</a></div>
               <div style="float:left">
               <?php
				$audiopath = "";
				if(isset($words["audioVerbsRef"]) && is_file(GLOBAL_AUDIO_DIR.$words["audioVerbsRef"].".mp3"))
				{
					$audiopath = "<img class='playAudio' data-src='". GLOBAL_AUDIO_URL.$words["audioVerbsRef"].".mp3' src='".base_url()."assets/img/sound.png' />";
				}
				?>
                <div class="audio_noun_verb_holder hid_selected_audio_verb_file_div selectedsound div_verbs">
                 <span class="aud"><?php echo $audiopath;?></span>
                <span class="fname">
                 <input type="text" name="hid_selected_audio_verb_file" id="hid_selected_audio_verb_file" class="hid_verbs selectedmediafile" value="<?php echo ($editmode==1)?to_utf8($words["audioVerbsRef"]):""?>"  readonly='readonly' />
                </span>
                </div>
               </div>
               <div style="clear:both"></div>
               <div class="audioverbholder mediaholder"></div>
               
                
            </div>
          </div><?php */?>
          
          <div class="control-group">
            <label for="inputError" class="control-label">Tags</label>
            <div class="controls controls2 selecttags">
              <select class='selectpicker show-menu-arrow' name='selectedtags[]' multiple title='' data-selected-text-format='count>4'>
              <?php
			  if(isset($tags))
			  {
				  foreach($tags as $tag)
				  {
					   $sel = (in_array($tag["tag_id"],explode(",",$words["tags"])))?" selected='selected'":"";
					  echo "<option value='".$tag["tag_id"]."' $sel>".$tag["tag_name"]."</option>";
				  }
			  }
			  ?>
              </select>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="button" onclick="document.location.href='<?php echo base_url()?>words'">Cancel</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>
     <div class="pvw_loader">Loading...</div>