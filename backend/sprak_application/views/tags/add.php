 <div class="container top">
     
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        
        <li class="active">
         <?php echo "<a href='".base_url()."tags'>Tags</a>"?>
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          Manage Tags
        </h2>
      </div>
 
      <?php
      //flash messages
	  $editmode = isset($tags)?1:0;
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == "done")
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_SUCCESS_MSG;
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_ERROR_MSG;
          echo '</div>';          
        }
      }
	  if($this->session->flashdata('errors'))
	  {
		   echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			echo "<b>".$this->session->flashdata('errors')."</b>";
			 echo '</div>'; 
	  }
      ?>
      
      <?php
	  
	  $editmode = isset($words)?1:0;
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');
      
      //form validation
      echo validation_errors();
     
	  if($action=="Add")
	  {
      	echo form_open('tags/add', $attributes);
	  }else{
		echo form_open('tags/update/'.$this->uri->segment(3).'', $attributes);
	  }
	  
      ?>
        <fieldset class="tags-setting">
          <div class="control-group">
            <label for="inputError" class="control-label">Tag Name</label>
            <div class="controls">
            
              <input type="text" id="tag_name" name="tag_name" value="<?php echo (isset($tags["tag_name"]))?$tags["tag_name"]:""?>">
              <?php
              if($editmode==0) //Show only when user is adding new record
			  {
			  ?>
              <br /><div class="help-inline">You can add <b class='btn-success' style="padding:1px 2px 1px 2px;">multiple tags</b> by separating them with comma.</div>
              <?php
			  }
			  ?>
            </div>
          </div>
          <div class="control-group">
            <label for="is_active" class="control-label">Tag Status</label>
            <div class="controls">
            	<?php
				$selanswer = "";
                if($editmode==1)
				{
					$selanswer = $tags["is_active"];
				}
				?>
                <select id="is_active" name="is_active">
                <option value="">--Select--</option>
                <option value="1" selected="selected" <?php echo ($selanswer=="1")?"selected='selected'":""?>>Active</option>
                <option value="0" <?php echo ($selanswer=="0")?"selected='selected'":""?>>Inactive</option>
                </select>	
            </div>
          </div>
          
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Save changes</button>
          </div>
        </fieldset>
		
      <?php echo form_close(); ?>

    </div>