    <div class="container top">
      
       <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        
        <li class="active">
         <?php echo "Tags"?>
        </li>
      </ul>
      
      
 	<div class="page-header users-header">
        <h2>
           <?php echo "Manage Tags"?> 
          <span><a href="<?php echo site_url().$this->uri->segment(1); ?>/add" class="btn btn-success" style="margin-left:20px;">Add a new Tag</a></span>
          <?php /*?><span><a  href="<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>/json/<?php echo $exercise_desc[2]?>"  class="btn btn-warning">Create JSON of this Exercise</a></span><?php */?>
        </h2>
      </div>
    
    <div class="row">
    <?php 
	if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') != "not_done")
        {
          	echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			if($this->session->flashdata('flash_message')=="done")
			{
            	echo CONST_SUCCESS_MSG;
			}else{
				echo "<strong>".$this->session->flashdata('flash_message')."</strong>";
			}
          echo '</div>';       
        }else{
          	echo '<div class="alert alert-error">';
			echo '<a class="close" data-dismiss="alert">×</a>';
			
			if($this->session->flashdata('flash_message')=="not_done")
			{
				echo CONST_ERROR_MSG;
			}else{
				echo "<strong>".$this->session->flashdata('flash_message')."</strong>";
			}
          	echo '</div>';          
        }
      }	
	?>
        <div class="span12 columns">
              
           
         <form action="<?php echo base_url();?>tags/bulkaction" method="POST" id="frmTagList">
          <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
              <th width="8%"><input type="checkbox" id="checkallsets" /></th>
                <th class="header" width="8%">#</th>
                 <th class="red header" width="40%">Tag Name</th>
                 <th class="red header" width="24%">Tag Status</th> 
                <th class="red header" width="20%">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
			  $i = 1;
			  if($tags)
			  {
				  foreach($tags as $row)
				  {
					$danger = ($row["is_active"]==0)?"class='danger'":"";
					echo '<tr '.$danger.'>';
					if(!in_array($row["tag_id"],$used_tags))
					{
						echo '<td><input type="checkbox" name="selected_tags[]" class="exercise_sets" value="'.$row['tag_id'].'" /></td>';
					}else{
						echo "<td>&nbsp;</td>";
					}
					echo '<td>'.$i.'</td>';
					echo '<td>'.$row['tag_name'].'</td>';
					echo '<td>'.(($row["is_active"]==1)?"Active":"<span style='color:red'><i>Inactive</i></span>").'</td>';
					echo '<td class="crud-actions">
					  <a href="'.site_url().$this->uri->segment(1).'/update/'.$row['tag_id'].'" class="btn btn-info">view & edit</a> 
					</td>';
					echo '</tr>';
					$i++;
				  }
			  }else{
				  echo "<tr><td colspan='5' style='text-align:center;'>No records available</td></tr>";
			  }
              ?>      
            </tbody>
          </table>
		<div style="float:left">
        <select class="selectpicker" name="bulkaction" id="bulkactionselect">
        <option value="">- With Selected - </option>
        <option value="2">Delete</option>
        <option value="1">Make Active</option>
        <option value="0">Make Inactive</option>
        </select></div><div style="float:left;margin-left:15px;">
        <input type="button" class="btn btn-danger" id="bulkaction"  value="  GO  "/></div>
        </form>
      </div>
    </div>
    <script type="text/javascript">
	$(function(){
		$("#bulkaction").click(function(e){
			var error ="";
			
			if($(".exercise_sets:checked").length==0)
			{
				error +=" - At least select something.\n";
			}
			if($("#bulkactionselect").val()=="")
			{
				error +=" - Select the action.\n";
			}
			if(error!="")
			{
				alert("Please fix the following error(s):-\n"+error);
				return false;
			}else{
				$action = $("#bulkactionselect").val();
				if($action==2)
				{
					if(confirm("Are you sure you want to permanently delete selected tag(s)?"))
					{
						$("#frmTagList").submit();
					}
				}else{
					$("#frmTagList").submit();
				}
				return false;
			}
		})
	});
	</script>
     