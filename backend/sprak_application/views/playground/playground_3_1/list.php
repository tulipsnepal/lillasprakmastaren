    <div class="container top">
      
       <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url()."exercise"; ?>">
            <?php echo "Exercises";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <?php echo ucwords($this->uri->segment(1))?>
           <span class="divider">/</span>
        </li>
        <li class="active">
        <?php echo "Playground 3-1"?>
        </li>
      </ul>
      
      
 	<div class="page-header users-header">
        <h2>
           <?php echo "Playground 3-1 - ".$exercise_desc[0]?> 
          <span><a href="<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>/add" class="btn btn-success" style="margin-left:20px;">Add a new Set</a></span>
          <?php /*?><span><a  href="<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>/json/<?php echo $exercise_desc[2]?>"  class="btn btn-warning">Create JSON of this Exercise</a></span><?php */?>
        </h2>
      </div>
    
    <div class="row">
    <?php 
	if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') != "not_done")
        {
          	echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			if($this->session->flashdata('flash_message')=="done")
			{
            	echo CONST_SUCCESS_MSG;
			}else{
				echo "<strong>".$this->session->flashdata('flash_message')."</strong>";
			}
          echo '</div>';       
        }else{
          	echo '<div class="alert alert-error">';
			echo '<a class="close" data-dismiss="alert">×</a>';
			
			if($this->session->flashdata('flash_message')=="not_done")
			{
				echo CONST_ERROR_MSG;
			}else{
				echo "<strong>".$this->session->flashdata('flash_message')."</strong>";
			}
          	echo '</div>';          
        }
      }	
	?>
        <div class="span12 columns">
              <?php
             if(trim($exercise_desc[0])!=""){
			?>
            <div>
            <img src="<?php echo EXTRA_IMAGE_URL.$exercise_desc[1]?>" width="250" /> 
            </div>
            <br clear="all" />
            <?php
			 }
			 ?>
			
           
           <form action="<?php echo base_url();?>playground/playground_3_1/deleteselected" method="POST" id="frmSetList">
          <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
              	<th class="header" width="3%"><input type="checkbox" id="checkallsets" /></th>
                <th class="header" width="3%">#</th>
                <th class="red header" width="8%">Word 1</th>
                <th class="red header" width="8%">Word 2</th>
                <th class="red header" width="8%">Word 3</th>
                <th class="red header" width="8%">Word 4</th>
                <th class="red header" width="8%">Word 5</th>
                <th class="red header" width="8%">Word 6</th>
                <th class="red header" width="8%">Word 7</th>
                <th class="red header" width="8%">Word 8</th>
                <th class="red header" width="8%">Category</th>
                <th class="red header" width="10%">Set Status</th>
                <th class="red header" width="12%">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
			  $i = 1;
			 
              foreach($set as $row)
              {
                
               $danger = ($row["is_active"]==0)?"class='danger'":"";
                echo '<tr '.$danger.'>';
				echo '<td><input type="checkbox" name="sets[]" class="exercise_sets" value="'.$row['set_id'].'" /></td>';
        echo '<td>'.$i.'</td>';
        if(in_array($row["word_1"]["word_id"],$row["same_category"]))
        {

        }
				echo '<td><a href="'.GLOBAL_IMG_URL.$row["word_1"]["imageRef"].'" class="fancybox btn btn-link '.$row["word_1"]["bold_class"].'">'.$row["word_1"]["word"].'</a></td>';
        echo '<td><a href="'.GLOBAL_IMG_URL.$row["word_2"]["imageRef"].'" class="fancybox btn btn-link '.$row["word_2"]["bold_class"].'">'.$row["word_2"]["word"].'</a></td>';
				echo '<td><a href="'.GLOBAL_IMG_URL.$row["word_3"]["imageRef"].'" class="fancybox btn btn-link '.$row["word_3"]["bold_class"].'">'.$row["word_3"]["word"].'</a></td>';
				echo '<td><a href="'.GLOBAL_IMG_URL.$row["word_4"]["imageRef"].'" class="fancybox btn btn-link '.$row["word_4"]["bold_class"].'">'.$row["word_4"]["word"].'</a></td>';
        echo '<td><a href="'.GLOBAL_IMG_URL.$row["word_5"]["imageRef"].'" class="fancybox btn btn-link '.$row["word_5"]["bold_class"].'">'.$row["word_5"]["word"].'</a></td>';
        echo '<td><a href="'.GLOBAL_IMG_URL.$row["word_6"]["imageRef"].'" class="fancybox btn btn-link '.$row["word_6"]["bold_class"].'">'.$row["word_6"]["word"].'</a></td>';
        echo '<td><a href="'.GLOBAL_IMG_URL.$row["word_7"]["imageRef"].'" class="fancybox btn btn-link '.$row["word_7"]["bold_class"].'">'.$row["word_7"]["word"].'</a></td>';
        echo '<td><a href="'.GLOBAL_IMG_URL.$row["word_8"]["imageRef"].'" class="fancybox btn btn-link '.$row["word_8"]["bold_class"].'">'.$row["word_8"]["word"].'</a></td>';
        echo '<td>'.$row["category"].'</td>';
				echo '<td>'.(($row["is_active"]==1)?"Active":"<span style='color:red'><i>Inactive</i></span>").'</td>';
				echo '<td class="crud-actions">
                  <a href="'.site_url().$this->uri->segment(1)."/".$this->uri->segment(2).'/update/'.$row['set_id'].'" class="btn btn-info">view & edit</a> 
                </td>';
                echo '</tr>';
				$i++;
              }
              ?>      
            </tbody>
          </table>
			<div><input type="submit" class="btn btn-danger" id="submitlist"  value="Delete Selected Sets"/></div>
        </form>
      </div>
    </div>
     