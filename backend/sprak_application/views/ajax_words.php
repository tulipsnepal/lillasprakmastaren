<div class="well">
      	<div style="float:left">
      	<span><input type="text" id="search" name="search" /></span><span style="vertical-align:top;margin-left:15px;"><a href="javascript:void(0)" class="btn btn-primary" onclick="filterWords('search')">Search</a></span>
        </div>
        <div style="float:left;margin-left:25px">
        <select name="filter_words" id="filter_words" onchange="filterWords('filter')">
        <option value="">--Filter by Tags--</option>
        <?php
        foreach($tags as $tag)
		{
		?>
        <option value="<?php echo $tag?>"><?php echo $tag?></option>
        <?php
		}
		?>
        </select>
        </div>
        <div style="float:right">
        <a href="javascript:void(0)" class="btn btn-warning" onclick="loadWords();">View All</a>
        </div>
      </div>
<div class='tivoli-1-full' style='width:900px'><ul class='list_words main-col' data-setvalueto='<?php echo $type?>'>
<?php 
echo $results;
?>
</ul></div>
<script type="text/javascript">

$(function(){
	checkSelected();
});

function checkSelected(){
	$(".checkbox").each(function(){
		
		if($(this).val()==$("#<?php echo $type?>").val())
		{
			$(this).prop("checked",true);
			$(this).parent("label").addClass("btn btn-info no-padding");
		}
	});	
}

function filterWords(type,keyword){
		if(type=="search"){
			$type = "search";
			$keyword = $("#search").val();
			if($.trim($keyword)==""){
				loadWords();
				return false;
			}else{
				loadWords($type,$keyword);
			}
		}else{
			$type = "tags";
			$keyword = $("#filter_words").val();
			if($.trim($keyword)==""){
				loadWords();
				return false;
			}else{
				loadWords($type,$keyword);
			}
		}
		
		
}

function loadWords(){
	if(arguments.length>0)
	{
		var url = '<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>/filterwords/'+arguments[0]+"/"+arguments[1];
	}else{
		var url = '<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>/filterwords';
	}
	$.ajax({
		url: url,
		type: "POST",
		beforeSend:function(){
			$(".main-col").html("<li><em>Loading Words...</em></li>");
		},
		success:function(data){
			$(".main-col").html(data);
			checkSelected();
		}
	});
}
</script>