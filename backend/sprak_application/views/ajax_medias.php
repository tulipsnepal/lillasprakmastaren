<div class="atoz">
    <div class="pagination" style="float:left">
        <ul>
        <?php
        
        foreach(range("A","Z") as $char)
        {
			$class = "";
			if($char==$searchstr){
				$class = "class='active'";
			}
         ?>
        <li <?php echo $class?>><a href="javascript:void(0)"  data-href="<?php echo $url?>" class="loadmedia" data-target="<?php echo $target?>" data-searchfor="<?php echo $char?>"><?php echo $char?></a></li>
         <?php 
        }
        ?>
         <li <?php echo ($searchstr=="Ä")?"class='active'":""?>><a href="javascript:void(0)"  data-href="<?php echo $url?>" class="loadmedia" data-target="<?php echo $target?>" data-searchfor="Ä">Ä</a></li>
         <li <?php echo ($searchstr=="Å")?"class='active'":""?>><a href="javascript:void(0)"  data-href="<?php echo $url?>" class="loadmedia" data-target="<?php echo $target?>" data-searchfor="Å">Å</a></li>
         <li <?php echo ($searchstr=="Ö")?"class='active'":""?>><a href="javascript:void(0)"  data-href="<?php echo $url?>" class="loadmedia" data-target="<?php echo $target?>" data-searchfor="Ö">Ö</a></li>
        </ul>
    </div>
    <div class="ajaxinput">
    <input type="text" size="20" class="searchstring" /><input type="button" placeholder="Search File" class="btn btn-primary searchmedia" value="Search" data-href="<?php echo $url?>" class="loadmedia" data-target="<?php echo $target?>" data-searchfor="Ä" />
    </div>
    <div style="clear:both"></div>
</div>
<div class="galleryimage <?php echo $category."-main"?>">
                  
                  <?php
				  $checked="";
				 
				if($results){ 
				   foreach($results as $file)
                  {
					  $pathinfo = pathinfo($file->file_name_with_ext);
					  
					  if($type=="audio")
					  {
                  ?>
                  
                    <div class="selementholder soundholderdef"  data-name="<?php echo mb_strtolower($pathinfo["filename"])?>"  data-category="<?php echo $category?>">
                	<span><input type="radio" name="selectFile-<?php echo $category?>" class="selectFile" data-category="<?php echo $category?>"  value="<?php echo to_utf8($pathinfo["filename"]);?>" data-file="<?php echo $pathinfo["filename"];?>" /></span>
                   <span><img class="playAudio" data-src="<?php echo GLOBAL_AUDIO_URL.$file->file_name_with_ext?>" src="<?php echo base_url()."assets/img/sound.png"?>" />
                    </span>
                    <span><?php echo to_utf8($pathinfo["filename"]);?></span>
                </div>
                  <?php
					  }else{
				?>
              <div class="thumbholder" data-name="<?php echo mb_strtolower($pathinfo["filename"])?>">
                      <div class="selementholder simageholder"  data-category="<?php echo $category?>" style="position:relative">
                      		<div class="image-loader"></div>
                      		<div class="galleryradio"> 
                            	<input type="radio" name="selectFile"  class="selectFile" data-category="<?php echo $category?>" value="<?php echo to_utf8($pathinfo["filename"])?>"  data-file="<?php echo $pathinfo["filename"];?>"  <?php echo $checked?> />
                            </div>
                            <div class="img" title="<?php echo to_utf8($pathinfo["filename"])?>">
                            <img class="words-image" src="<?php echo GLOBAL_IMG_URL.$file->file_name_with_ext?>" height="80" alt="" />
                            </div>
                            <div class="simagecaption">
                            <?php echo to_utf8($pathinfo["filename"])?>
                            </div>
                      </div>
                  </div>
                <?php
					  }
                  }
				}else{
					echo "<div class='no-records'>No records available for search string <b>$searchstr</b></div>";
				}
                  ?>
              
             </div>

<script type="text/javascript">
/*var type = "<?php echo $type?>";
var element =  $("#<?php echo $_GET["element"]?>");
var elementdiv = $(".<?php echo $_GET["element"]?>_div");
var catelement =  $(".<?php echo $category."-div"?>");
var mainelement = "<?php echo $category."-main"?>";*/

$(function(){
	//search
	
	/*
	var selectedfile = $("#<?php echo $_GET["element"]?>").val();
	
	 $(".<?php echo $category."-div"?>").find("input[type=radio][value='"+selectedfile+"']").prop("checked",true);
	if(type=="image")
	{
		var ele =  $(".<?php echo $category."-div"?>").find("input[type=radio][value='"+selectedfile+"']").parent(".galleryradio").parent(".thumbholder");
	}else{
		var ele =  $(".<?php echo $category."-div"?>").find("input[type=radio][value='"+selectedfile+"']").parent("span").parent(".selementholder");
	}
	
	ele.prependTo(".<?php echo $category."-main"?>");*/
	
	
	
	if($('.words-image').length>0){
		$('.words-image').one('load', function() {
					 $(this).parent(".img").parent().find(".image-loader").remove();
				}).each(function() {
				  if(this.complete) {
					$(this).trigger('load');
				  }
		});	
	}
});




</script>