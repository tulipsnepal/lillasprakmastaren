    <script type="text/javascript">
	//SET GLOBAL AJAX URL TO BE USED FOR AJAX CALLS
	GlobalAjaxUrl = '<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>';
	
	var block = 1;
	$(document).ready(function(){
		
		$(".add-new-set").click(function(){
			if($(".optioncheck:checked").length>18){
				alert("You cannot create more than 6 sets");
				return false;
			}
			$(".set-block:empty").remove();
			$(".mainwords").prop("checked",false);
			$("#temp_selected").empty();
			
			var maxval = Number.MIN_VALUE;
			
			$(".set-block").each(function () {
				var num = parseInt($(this).data("blockid"), 10) || 0; // always use a radix
				maxval = Math.max(num, maxval)
			});
			
			block = maxval+1;
			
			
			$(".tivoli-main-words-img").prepend("<div class='set-block block-"+block+"' data-blockid='"+block+"'></div><div style='clear:both' class='cleardiv'></div>");
			
			
			$(document).scrollTop(0);
		});
		
		$(document.body).on("click",".remove-holder a",function(){
			$("#temp_selected").empty();
			$parent =$(this).parent().prev(".set-block");
			$(this).parent().next(".cleardiv").remove();
			$parent.remove();
			$(this).parent().remove();
			
			$(".add-new-set").trigger("click");
			if($(".set-block").length==0){
				$("#add-new-holder").hide();
			}
		})
		
		//CLICK EVENT TO THE CHECKBOX/RADIO-BUTTON. UPON CLICKING DISPLAY SELECTED IMAGE AND SET THE SELECTED WORD VALUE TO HIDDEN FIELD OR THE HIDDEN COMBOBOX
		$(document.body).on("click",".mainwords",function(){
			$current_check_val = $(this).val();
			$current_check_text = $.trim($(this).parent("label").text());
			var checkedBoxlength=$("#temp_selected").children("option").length+1;
			if($(".optioncheck:checked").length>=18){
				alert("You cannot create more than 6 sets");
				return false;
			}
			$chkval = $(this).val();
			$chktext = $(this).parent("label").text();
			var exists = $("#temp_selected option[value='"+$chkval+"']").length !== 0;
			var optlength = $("#temp_selected").children("option").length;
			console.log("BLOCK = "+block);
			if($(".set-block").length==0){
				$(".tivoli-main-words-img").prepend("<div class='set-block block-"+block+"' data-blockid='"+block+"'></div><div style='clear:both' class='cleardiv'></div>");
			}
			if(checkedBoxlength==0)
			{ 
				$startingdiv = "<div class";
				$equaltodiv = ""
			}else if(checkedBoxlength==3){
				$equaltodiv = "<div class='equalto-div'>=</div>";
			}else if(checkedBoxlength>3){
				return false;
			}else if(checkedBoxlength==2){
				$equaltodiv = "<div class='equalto-div'>+</div>";
			}else{
				$equaltodiv = "";
			}
			
			//POPULATE IMAGE OF SELECTED WORDS
			if($(this).prop("checked")){
				
				if(!exists && optlength<4)
				{
					$("#temp_selected").append("<option value='"+$chkval+"'>"+$chktext+"</option>");
				}else{
					return false;
				}
				
				
				
				//SHOW IMAGE OF THE SELECTED WORD WITH THE URL FETCHED FROM THE DATA ATTRIBUTE "imgsrc" OF THE SELECTED CHECKBOX
				if($(this).data("imgsrc")!="")
				{
					$imgsrc = "<?php echo GLOBAL_IMG_URL?>"+$(this).data("imgsrc")+".png";
					$(".block-"+block).append($equaltodiv+"<div class='tivoli-words-image relative tivoli4 main"+$current_check_val+"'><div class='absolute-topright'><input type='checkbox' value='"+$current_check_val+"' name='selectedoptionword[]' class='optioncheck' checked='checked' /></div><img src='"+$imgsrc+"' title='"+$current_check_text+"' style='height:120px' /><div style='text-align:center'>"+$current_check_text+"</div></div>").show();
					$(".tivoli-main-words-img ").prepend($(".block-"+block));
					if(checkedBoxlength>0 && $(".tivoli-main-words-img .block-"+block).next(".remove-holder").length==0){
						$(".tivoli-main-words-img .block-"+block).after("<div class='remove-holder'><a href='javascript:void(0)' class='btn btn-danger'>Remove Set</a></div>");
					}else if(checkedBoxlength==3){
						setTimeout(function(){$(".add-new-set").trigger("click");}, 500);
						$("#add-new-holder").show();
					}
				}else{
					alert("No image linked with this word.");
					$(this).prop("checked",false);
				}
			}else{
				
			}
		});
		
		
		
		loadWords();
		
		//WHEN SORT ALPHABETS ARE CLICKED
		$(".loadcharwords").on("click",function(){
			$(".loadcharwords").parent("li").removeClass("active");
			$(this).parent("li").addClass("active");
			loadWords("alpha",$(this).text());
		});
		
		//WHEN SEARCH IS CLICKED
		$(".loadcharwords_search").on("click",function(){
			$keyword = $(this).prev(".searchstring").val();
			loadWords("search",$keyword);
		});
		
		
		//WHEN FILTER BY TAGS IS SELECTED
		$(".loadcharwords_filter").on("change",function(){
			$keyword = $(this).val();
			loadWords("tags",$keyword);
		});
	});
	
		
	
	function submitWords(){
		$selectedlength = $(".optioncheck:checked").length;
		var errors = "";
		
		if($selectedlength<18)
		{
			errors +="You must create 6 sets, with two primary words and one composite word each .\n";
		}
		
		if(errors!=""){
			alert("Please fix the following error(s)\n\n"+errors);
			return false;
		}
		$("#selected_mainwords option").prop("selected",true);
		//alert($unrhyming);
		return true;
	}
	</script>
    <div class="container top">
      
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url()."exercise"; ?>">
            <?php echo "Exercises";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
        <a href="<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>">
          <?php echo ucwords($this->uri->segment(1))?>
          </a>
           <span class="divider">/</span>
        </li>
        <li class="active">
        <?php echo "Tivoli 4-1"?>
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          <?php echo "Tivoli 4-1 - ".$exercise_desc[0]?> 
        </h2>
      </div>
 
      <?php
      //flash messages
	  
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == "done")
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_SUCCESS_MSG;
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_ERROR_MSG;
          echo '</div>';          
        }
      }
	  if($this->session->flashdata('errors'))
	  {
		   echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			echo "<b>".$this->session->flashdata('errors')."</b>";
			 echo '</div>'; 
	  }
      ?>
      
      <?php
	  
	  $editmode = isset($exercises)?1:0;
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '','onsubmit'=>'return submitWords()');
      
      //form validation
      echo validation_errors();
	 
      if($action=="Add")
	  {
      	echo form_open('tivoli/tivoli_4_1/add', $attributes);
	  }else{
		echo form_open('tivoli/tivoli_4_1/update/'.$this->uri->segment(4).'', $attributes);
	  }
	  
	   $selectedwords = "";
	   $imagesforselectedwords = "";
	   if(isset($set) && $set!="")
		{
			$set_data = $set["set_data"]["answers"];
			//echo "<pre>".print_r($set_data,true)."</pre>"; exit;
			$a =1;
			foreach($set_data as $key=>$value)
			{
				$compositeword = $this->utilities->word_details_by_id($key);
				$firstword = $this->utilities->word_details_by_id($value["first"]);
				$secondword = $this->utilities->word_details_by_id($value["second"]);
				$imagesforselectedwords .= "<div class='set-block block-".$a."' data-blockid='".$a."'>";
				
				$imagesforselectedwords .= "<div class='tivoli-words-image relative tivoli4 main".$firstword["word_id"]."'><div class='absolute-topright'><input type='checkbox' value='".$firstword["word_id"]."' name='selectedoptionword[]' class='optioncheck' checked='checked' /></div><img src='".GLOBAL_IMG_URL.$firstword["imageRef"]."' title='".$firstword["word"]."' style='height:120px' /><div style='text-align:center'>".$firstword["word"]."</div></div>";
				$imagesforselectedwords .= "<div class='equalto-div'>+</div>";
				$imagesforselectedwords .= "<div class='tivoli-words-image relative tivoli4 main".$secondword["word_id"]."'><div class='absolute-topright'><input type='checkbox' value='".$secondword["word_id"]."' name='selectedoptionword[]' class='optioncheck' checked='checked' /></div><img src='".GLOBAL_IMG_URL.$secondword["imageRef"]."' title='".$secondword["word"]."' style='height:120px' /><div style='text-align:center'>".$secondword["word"]."</div></div>";
				$imagesforselectedwords .= "<div class='equalto-div'>=</div>";
				$imagesforselectedwords .= "<div class='tivoli-words-image relative tivoli4 main".$compositeword["word_id"]."'><div class='absolute-topright'><input type='checkbox' value='".$compositeword["word_id"]."' name='selectedoptionword[]' class='optioncheck' checked='checked' /></div><img src='".GLOBAL_IMG_URL.$compositeword["imageRef"]."' title='".$compositeword["word"]."' style='height:120px' /><div style='text-align:center'>".$compositeword["word"]."</div></div>";
				$imagesforselectedwords .="</div><div class='remove-holder'><a href='javascript:void(0)' class='btn btn-danger'>Remove Set</a></div><div style='clear:both'  class='cleardiv'></div>";
				$a++;
			}
		}
		
      ?>
      
        <fieldset>
          <div class="control-group">
          
            <label for="inputError" class="control-label">Select Words</label>
            <div class="controls">
            <?php
            if(isset($set) && $set!="")
			{
			?>
            <div class="help-inline" style="padding:0 0 10px 0px">You can change the selected words by using the list below</div>
            <?php
			}
			?>
                <div class="tivoli-1-full" style="margin-left:12px;">
                	<div class="upl tivoli-1-sub-head"><b>Select three words. The third word selected will be composite word of the first two. </b></div>
                    <?php echo $gen_alphabets?>
                    <div  style="max-height:500px;overflow:auto">
                    	<ul class="list_words main-col"></ul>
                    </div>
                </div>		
            </div>
          </div>
          
          <div class="control-group">
          	<div class="controls">
            <div style="margin-left:23px;">
            <div style="margin-bottom:20px;"><hr size="2" width="100%" color="#CCCCCC" /></div>
            <select name="temp_selected[]" id="temp_selected" style="display:none;" multiple="multiple"> </select>
        
        	<div class="tivoli-main-words-img" <?php echo ($imagesforselectedwords!="")?"style='display:block;'":""?>><?php echo $imagesforselectedwords?></div>
            <div style="display:none" id="add-new-holder"><a href='javascript:void(0)' class="btn btn-success add-new-set">Add New</a></div>
        </div>
            </div>
          </div>
          
          <div class="control-group">
            <label for="is_active" class="control-label">Set Status</label>
            <div class="controls">
            	<?php
				$selanswer = "";
                if(isset($set))
				{
					$selanswer = isset($set["is_active"])?$set["is_active"]:"";
				}
				?>
                <select id="is_active" name="is_active">
                <option value="">--Select--</option>
                <option value="1" selected="selected" <?php echo ($selanswer=="1")?"selected='selected'":""?>>Active</option>
                <option value="0" <?php echo ($selanswer=="0")?"selected='selected'":""?>>Inactive</option>
                </select>	
            </div>
          </div>
          <div class="form-actions">
          <input type="hidden" id="exercise_id" name="exercise_id" value="<?php echo $exercise_desc[2]?>" />
            <button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="button" onclick="document.location.href='<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>'">Cancel</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>
     <div class="pvw_loader">Loading...</div> 