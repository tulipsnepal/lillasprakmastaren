    <div class="container top">

       <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a>
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url()."exercise"; ?>">
            <?php echo "Exercises";?>
          </a>
          <span class="divider">/</span>
        </li>
        <li class="active">
          <?php echo ucwords($this->uri->segment(1))?>
           <span class="divider">/</span>
        </li>
        <li class="active">
        <?php echo "Tivoli 4-1"?>
        </li>
      </ul>


 	<div class="page-header users-header">
        <h2>
           <?php echo "Tivoli 4-1 - ".$exercise_desc[0]?>
          <span><a href="<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>/add" class="btn btn-success" style="margin-left:20px;">Add a new Set</a></span>
         <?php /*?> <span><a  href="<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>/json/<?php echo $exercise_desc[2]?>"  class="btn btn-warning">Create JSON of this Exercise</a></span><?php */?>
        </h2>
      </div>

    <div class="row">
    <?php
	if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') != "not_done")
        {
          	echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			if($this->session->flashdata('flash_message')=="done")
			{
            	echo CONST_SUCCESS_MSG;
			}else{
				echo "<strong>".$this->session->flashdata('flash_message')."</strong>";
			}
          echo '</div>';
        }else{
          	echo '<div class="alert alert-error">';
			echo '<a class="close" data-dismiss="alert">×</a>';

			if($this->session->flashdata('flash_message')=="not_done")
			{
				echo CONST_ERROR_MSG;
			}else{
				echo "<strong>".$this->session->flashdata('flash_message')."</strong>";
			}
          	echo '</div>';
        }
      }
	// $this->utilities->printr($set);
	?>
        <div class="span12 columns">
              <?php
             if(trim($exercise_desc[0])!=""){
			?>
            <div>
            <img src="<?php echo EXTRA_IMAGE_URL.$exercise_desc[1]?>" width="250" />
            </div>
            <br clear="all" />
            <?php
			 }
			 ?>


           <form action="<?php echo base_url();?>tivoli/tivoli_4_1/deleteselected" method="POST" id="frmSetList">
          <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
              	<th class="header" width="4%"><input type="checkbox" id="checkallsets" /></th>
                <th class="header" width="4%">#</th>
                <th class="red header" width="70%">Set </th>
                <th class="red header" width="10%">Set Status</th>
                <th class="red header" width="12%">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
			  $i = 1;

              foreach($set as $row)
              {

				  //<a href="'.GLOBAL_IMG_URL.$row["word_1"]["imageRef"].'" class="fancybox btn btn-link">'.$row["word_1"]["word"].'</a>
               $danger = ($row["is_active"]==0)?"class='danger'":"";
                echo '<tr '.$danger.'>';
				echo '<td><input type="checkbox" name="sets[]" class="exercise_sets" value="'.$row['set_id'].'" /></td>';
                echo '<td>'.$i.'</td>';
				echo '<td><div style="position:relative"><a href="javascript:void(0)" class="showdetails btn-link">Set '.$i.'</a>';
				$questions = $row["set_arrays"]["answers"];
				echo '<div class="sub-results-tivoli-4"><table class="showAudio table table-striped table-bordered table-condensed">';
				echo '<tr><th class="red header">First Word</th><th class="red header">Second Word</th><th class="red header">Composite Word</th></tr>';
				foreach($questions as $key=>$value){
					$compositeword = $this->utilities->word_details_by_id($key);
					$firstword = $this->utilities->word_details_by_id($value["first"]);
					$secondword = $this->utilities->word_details_by_id($value["second"]);
					echo "<tr>";
					echo '<td><a data-audio="'.$firstword['audioRef'].'" href="'.GLOBAL_IMG_URL.$firstword["imageRef"].'" class="fancybox btn-link">'.$firstword["word"].'</a></td>';
					echo '<td><a data-audio="'.$secondword['audioRef'].'" href="'.GLOBAL_IMG_URL.$secondword["imageRef"].'" class="fancybox btn-link">'.$secondword["word"].'</a></td>';
					echo '<td><a data-audio="'.$compositeword['audioRef'].'" href="'.GLOBAL_IMG_URL.$compositeword["imageRef"].'" class="fancybox btn-link">'.$compositeword["word"].'</a></td>';
					echo "</tr>";
				}
				echo '</table></div>';
				echo '</div></td>';
				echo '<td>'.(($row["is_active"]==1)?"Active":"<span style='color:red'><i>Inactive</i></span>").'</td>';
				echo '<td class="crud-actions">
                  <a href="'.site_url().$this->uri->segment(1)."/".$this->uri->segment(2).'/update/'.$row['set_id'].'" class="btn btn-info">view & edit</a>
                </td>';
                echo '</tr>';
				$i++;
              }
              ?>
            </tbody>
          </table>
			<div><input type="submit" class="btn btn-danger" id="submitlist"  value="Delete Selected Sets"/></div>
        </form>
      </div>
    </div>
     <script type="text/javascript">
	 $(function(){
		 $(".showdetails").click(function(){
			$(".sub-results-tivoli-4").not($(this).next(".sub-results-tivoli-4")).hide();
			$(".showdetails").not($(this)).removeClass("classtab");
			$(this).toggleClass("classtab");
		 	$(this).next(".sub-results-tivoli-4").toggle();
		 })
	 });
	 </script>