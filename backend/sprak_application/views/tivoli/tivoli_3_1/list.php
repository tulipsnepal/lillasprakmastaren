    <div class="container top">

       <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a>
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url()."exercise"; ?>">
            <?php echo "Exercises";?>
          </a>
          <span class="divider">/</span>
        </li>
        <li class="active">
          <?php echo ucwords($this->uri->segment(1))?>
           <span class="divider">/</span>
        </li>
        <li class="active">
        <?php echo "Tivoli 3-1"?>
        </li>
      </ul>


 	<div class="page-header users-header">
        <h2>
           <?php echo "Tivoli 3-1 - ".$exercise_desc[0]?>
          <span><a href="<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>/add" class="btn btn-success" style="margin-left:20px;">Add Words</a></span>
          <?php /*?><span><a  href="<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>/json/<?php echo $exercise_desc[2]?>"  class="btn btn-warning">Create JSON of this Exercise</a></span><?php */?>
        </h2>
      </div>

    <div class="row">
    <?php
	if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') != "not_done")
        {
          	echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			if($this->session->flashdata('flash_message')=="done")
			{
            	echo CONST_SUCCESS_MSG;
			}else{
				echo "<strong>".$this->session->flashdata('flash_message')."</strong>";
			}
          echo '</div>';
        }else{
          	echo '<div class="alert alert-error">';
			echo '<a class="close" data-dismiss="alert">×</a>';

			if($this->session->flashdata('flash_message')=="not_done")
			{
				echo CONST_ERROR_MSG;
			}else{
				echo "<strong>".$this->session->flashdata('flash_message')."</strong>";
			}
          	echo '</div>';
        }
      }
	?>
        <div class="span12 columns">
              <?php
             if(trim($exercise_desc[0])!=""){
			?>
            <div>
            <img src="<?php echo EXTRA_IMAGE_URL.$exercise_desc[1]?>" width="250" />
            </div>
            <br clear="all" />
            <?php
			 }
			 ?>


           <form action="<?php echo base_url();?>tivoli/tivoli_3_1/deleteselected" method="POST" id="frmSetList">
           <?php
		   //as we've fetched combination of both long and short words from database, we can't loop through all the recorset and separate them in different column of the table.
		   //For this we need to run separate loop for Long words and separate for Short words both in different tables.


		   $longwordcount = (isset($set["long_word"]))?count($set["long_word"]):0;
		   $shortwordcount = (isset($set["short_word"]))?count($set["short_word"]):0;
		   $difference = abs($longwordcount-$shortwordcount);
		   $shortwordextratds=""; $longwordextratds ="";
		   if($longwordcount<$shortwordcount)
		   {
			   $serialcount = $shortwordcount;
			   for($i=0;$i<$difference;$i++)
			   {
			   	$longwordextratds .="<tr><td>&nbsp;</td></tr>";
			   }
		   }else{
			   $serialcount = $longwordcount;
			   $longwordextratds = "";
		   }

		    if($shortwordcount<$longwordcount)
		   {
			   for($i=0;$i<$difference;$i++)
			   {
			   	$shortwordextratds .="<tr><td>&nbsp;</td></tr>";
			   }
		   }else{
			   $shortwordextratds = "";
		   }
		   //The above code block is used to populate extra rows in the table if any one of two has fewer record than other
		   ?>
           <div style="float:left;width:10%">
            <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
              	 <th class="red header">#</th>
              </tr>
            </thead>
            <tbody>
              <?php
			  for($k=1;$k<=$serialcount;$k++)
              {
				echo '<tr>';
				echo '<td>'.$k.'</td>';
                echo '</tr>';
              }
              ?>
            </tbody>
          </table>
           </div>
           <div style="float:left;width:45%">
          <table class="showAudio table table-striped table-bordered table-condensed concatedtable">
            <thead>
              <tr>
              	 <th class="red header">Long Word</th>
              </tr>
            </thead>
            <tbody>
              <?php
			  if(isset($set["long_word"]))
			  {
				  foreach($set["long_word"] as $lrow)
				  {
					echo '<tr>';
					echo '<td><a data-audio="'.$lrow["audioRef"].'" href="'.GLOBAL_IMG_URL.$lrow["imageRef"].'" class="fancybox btn-link">'.$lrow["word"].'</a></td>';
					echo '</tr>';
				  }

			  }
			  echo $longwordextratds;
              ?>
            </tbody>
          </table>
          </div>
          <div style="float:left;width:45%">
          <table class="table table-striped table-bordered table-condensed concatedtable">
            <thead>
              <tr>
              	 <th class="red header">Short Word</th>
              </tr>
            </thead>
            <tbody>
              <?php
			  if(isset($set["short_word"]))
			  {
				  foreach($set["short_word"] as $srow)
				  {
					echo '<tr>';
					echo '<td><a href="'.GLOBAL_IMG_URL.$srow["imageRef"].'" class="fancybox btn-link">'.$srow["word"].'</a></td>';
					echo '</tr>';
				  }

			  }
			  echo $shortwordextratds;
              ?>
            </tbody>
          </table>
          </div>
        </form>
      </div>
    </div>
