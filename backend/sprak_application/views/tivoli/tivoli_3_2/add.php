    <script type="text/javascript">
	
		//REMOVE SELECTED ITEM
		$(document.body).on("click",".delete-absolute",function(){
			$type = $(this).data("type");
			$id = $(this).data("id");
			$("#"+$type).val("");
			if($type=="first_word")
			{
				$(".mainwords-first[value='"+$id+"']").prop("checked",false);
			}else{
				$(".mainwords-second[value='"+$id+"']").prop("checked",false);
			}
			$(this).parent(".relative").remove();
		});
		
		
	//SET GLOBAL AJAX URL TO BE USED FOR AJAX CALLS
	GlobalAjaxUrl = '<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>';
	
	$(function(){
		//CLICK EVENT TO THE CHECKBOX/RADIO-BUTTON. UPON CLICKING DISPLAY SELECTED IMAGE AND SET THE SELECTED WORD VALUE TO HIDDEN FIELD OR THE HIDDEN COMBOBOX
		$(document.body).on("click",".mainwords-first,.mainwords-second",function(){
			$parent = $(this).parent("label").parent("li").parent("ul");
			var holder ="";
			
			//SET THE HOLDER VALUE DEPENDING UPON THE BLOCK OF WORDS
			if($parent.hasClass("main-col-first")) //FIRST BLOCK
			{
				holder = "first";
			}else if($parent.hasClass("main-col-second")) //SECOND BLOCK
			{
				holder = "second";
			}
			
			$current_check_val = $(this).val();
			$current_check_text = $.trim($(this).parent("label").text());
			var checkedBoxlength=$(".mainwords-"+holder+":checked").length;
			if(checkedBoxlength>1){
				return false;
			}
			
			//POPULATE IMAGE OF SELECTED WORDS
			if($(this).prop("checked")){
				$("#"+holder+"_word").val($(this).val());
				$("#"+holder+"_word").attr("data-text",$.trim($(this).parent("label").text()));
				$("."+holder).empty();
				if($(this).data("imgsrc")!="")
				{
					$imgsrc = "<?php echo GLOBAL_IMG_URL?>"+$(this).data("imgsrc")+".png";
					$("."+holder).append("<div class='tivoli-words-image relative main"+$current_check_val+"'><img src='"+$imgsrc+"' title='"+$current_check_text+"' style='height:120px' /><div style='text-align:center;font-weight:normal;color:#000'>"+$current_check_text+"</div><div class='delete-absolute' data-id='"+$current_check_val+"' data-type='"+holder+"_word'>x</div></div>").show();
				}
			}else{
				$("#"+holder+"_word").val("");
				$("."+holder).find(".main"+$current_check_val).remove();
			}
			
		});
		
		
		loadWordsTwoInstances("first","all","<?php echo (isset($set) && $set!="")?mb_substr($set["first_word"]["word"],0,1):"A"?>");
		loadWordsTwoInstances("second","all","<?php echo (isset($set) && $set!="")?mb_substr($set["second_word"]["word"],0,1):"A"?>");
		
		//SORT ALPHABETS ON FIRST BLOCK OF WORD IS CLICKED
		$(".loadcharwords_first").on("click",function(){
			$(".loadcharwords_first").parent("li").removeClass("active");
			$(this).parent("li").addClass("active");
			loadWordsTwoInstances("first","alpha",$(this).text());
		});
		
		//SORT ALPHABETS ON SECOND BLOCK OF WORD IS CLICKED
		$(".loadcharwords_second").on("click",function(){
			$(".loadcharwords_second").parent("li").removeClass("active");
			$(this).parent("li").addClass("active");
			loadWordsTwoInstances("second","alpha",$(this).text());
		});
		
		//WHEN SEARCH BUTTON IN FIRST AND SECOND BLOCK OF WORD IS CLICKED
		$(".loadcharwords_first_search,.loadcharwords_second_search").on("click",function(){
			$keyword = $(this).prev(".searchstring").val();
			$clickedclass = this.className.replace("btn btn-primary ","");
			if($clickedclass=="loadcharwords_first_search"){
				var holder = "first";
			}else{
				var holder = "second";
			}
			loadWordsTwoInstances(holder,"search",$keyword);
		});
		
		//FILTER OPTION IN EITHER FIRST OR SECOND BLOCK IS SELECTED
		$(".loadcharwords_first_filter,.loadcharwords_second_filter").on("change",function(){
			$keyword = $(this).val();
			
			
			if($(this).hasClass("loadcharwords_first_filter")){
				var holder = "first";
			}else{
				var holder = "second";
			}
			loadWordsTwoInstances(holder,"tags",$keyword);
		});
	});
	
	
	
	
	function submitForm(){
		
		$firsttext = $("#first_word").attr("data-text");
		$secondtext = $("#second_word").attr("data-text");
		
		var errors = "";
				
		if($.trim($("#first_word").val())=="")
		{
			errors +="-Select a first word .\n";
		}
		
		if($.trim($("#second_word").val())=="")
		{
			errors +="-Select a second word.\n";
		}
		
		$firststrlength  = parseInt($.trim($firsttext).length);
		$secondstrlength = parseInt($.trim($secondtext).length);
		
		if($firststrlength==$secondstrlength)
		{
			errors += "- Selected words must be of different length.";	
		}
		
		
		if(errors!=""){
			alert("Please fix the following error(s)\n\n"+errors);
			return false;
		}
		return true;
	}
	</script>
    <div class="container top">
      
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url()."exercise"; ?>">
            <?php echo "Exercises";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
        <a href="<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>">
          <?php echo ucwords($this->uri->segment(1))?>
          </a>
           <span class="divider">/</span>
        </li>
        <li class="active">
        <?php echo "Tivoli 3-2"?> 
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          <?php echo "Tivoli 3-2 - ".$exercise_desc[0]?>
        </h2>
      </div>
 
      <?php
      //flash messages
	  
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == "done")
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_SUCCESS_MSG;
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_ERROR_MSG;
          echo '</div>';          
        }
      }
	  if($this->session->flashdata('errors'))
	  {
		   echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			echo "<b>".$this->session->flashdata('errors')."</b>";
			 echo '</div>'; 
	  }
      ?>
      
      <?php
	  
	  $editmode = isset($set)?1:0;
     
      $attributes = array('class' => 'form-horizontal', 'id' => '','onsubmit'=>'return submitForm()');
     
      //form validation
      echo validation_errors();
	 
      if($action=="Add")
	  {
      	echo form_open('tivoli/tivoli_3_2/add', $attributes);
	  }else{
		echo form_open('tivoli/tivoli_3_2/update/'.$this->uri->segment(4).'', $attributes);
	  }
	  $selectedword_first = "";
	  $selectedword_first_text = "";
	   $imagesforselectedword_first = "";
	   
	   $selectedword_second= "";
	   $selectedword_second_text = "";
	   $imagesforselectedword_second= "";
	   if(isset($set) && $set!="")
		{
			$first_word = $set["first_word"];
			$second_word = $set["second_word"];
			
			$selectedword_first = $first_word["word_id"];
			$selectedword_first_text = $first_word["word"];
			$selectedword_second = $second_word["word_id"];
			$selectedword_second_text = $second_word["word"];
			
			$imagesforselectedword_first = "<div class='tivoli-words-image relative main".$first_word["word_id"]."'><img src='".GLOBAL_IMG_URL.$first_word["imageRef"]."' title='".$first_word["word"]."' style='height:120px' />
											<div style='text-align:center;font-weight:normal;color:#000'>".$first_word["word"]."</div>
											<div class='delete-absolute' data-id='".$first_word["word_id"]."' data-type='first_word'>x</div>
											</div>";
			$imagesforselectedword_second = "<div class='tivoli-words-image relative main".$second_word["word_id"]."'><img src='".GLOBAL_IMG_URL.$second_word["imageRef"]."' title='".$second_word["word"]."' style='height:120px' />
											<div style='text-align:center;font-weight:normal;color:#000'>".$second_word["word"]."</div>
											<div class='delete-absolute' data-id='".$second_word["word_id"]."' data-type='second_word'>x</div>
											</div>";
			
		}
      ?>
      
        <fieldset>
         <input type="hidden" id="exercise_id" name="exercise_id" value="<?php echo $exercise_desc[2]?>" />
        <div class="control-group">
            <label for="sentence" class="control-label">Select first word<div class="tivoli-main-words-img first" style="margin-top:15px;margin-left:27px;"><?php echo $imagesforselectedword_first?></div></label>
            <div class="controls">
              <input type="hidden" id="first_word" name="first_word" value="<?php echo $selectedword_first;?>"  data-text="<?php echo $selectedword_first_text?>">
              <div class="tivoli-1-full" style="padding-top:15px;">
              <?php echo $gen_alphabets_first?>
                    <div style="max-height:500px;overflow:auto;">
                    	<ul class="list_words main-col-first"></ul>
                    </div>
               </div>
            </div>
          </div>
          
          <div class="control-group">
            <label for="sentence" class="control-label">Select second word<div class="tivoli-main-words-img second" style="margin-top:15px;margin-left:27px;"><?php echo $imagesforselectedword_second?></div></label>
            <div class="controls">
            <input type="hidden" id="second_word" name="second_word" value="<?php echo $selectedword_second;?>" data-text="<?php echo $selectedword_second_text?>">
              <div class="tivoli-1-full" style="padding-top:15px;">
               <?php echo $gen_alphabets_second?>
                    <div style="max-height:500px;overflow:auto;">
                    	<ul class="list_words main-col-second"></ul>
                    </div>
               </div>
            </div>
          </div>
          
          <div class="control-group">
            <label for="is_active" class="control-label">Set Status</label>
            <div class="controls">
            	<?php
				$selanswer = "";
                if($editmode==1)
				{
					$selanswer = $set["is_active"];
				}
				?>
                <select id="is_active" name="is_active">
                <option value="">--Select--</option>
                <option value="1" selected="selected" <?php echo ($selanswer=="1")?"selected='selected'":""?>>Active</option>
                <option value="0" <?php echo ($selanswer=="0")?"selected='selected'":""?>>Inactive</option>
                </select>	
            </div>
          </div>
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="button" onclick="document.location.href='<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>'">Cancel</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>
        <div class="pvw_loader">Loading...</div>