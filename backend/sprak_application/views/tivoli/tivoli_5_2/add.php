    <script type="text/javascript">
	//SET GLOBAL AJAX URL TO BE USED FOR AJAX CALLS
	GlobalAjaxUrl = '<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>';
	$(document).ready(function(){
		
		$("#select-question").change(function(){
			$(".mainwords").prop("checked",false);
		});
		
		$(document.body).on("click",".delete-absolute",function(){
			$type = $(this).data("type");
			$id = $(this).data("id");
			$("#selected_word").val("");
			$(".mainwords[value='"+$id+"']").prop("checked",false);
			$(this).parent(".relative").remove();
			
		});
		
		//CLICK EVENT TO THE CHECKBOX/RADIO-BUTTON. UPON CLICKING DISPLAY SELECTED IMAGE AND SET THE SELECTED WORD VALUE TO HIDDEN FIELD OR THE HIDDEN COMBOBOX
		$(document.body).on("click",".mainwords",function(){
			$current_check_val = $(this).val();
			$current_check_text = $.trim($(this).parent("label").text());
			var checkedBoxlength=$(".mainwords:checked").length;
			if(checkedBoxlength>1){ //DON'T LET USER CHECK MORE THAN ONE CHECKBOX
				return false;
			}
			
			var forquestion = $("#select-question").val();
			//POPULATE IMAGE OF SELECTED WORDS
			
			if($(this).prop("checked")){
				$("#selected_word_"+forquestion).val($(this).val());
				$(".img"+forquestion).empty();
				
				//FETCH "imgsrc" DATA ATTRIBUTE FROM CHECKBOXES AND SHOW THE IMAGE OF SELECTED WORDS
				if($(this).data("imgsrc")!="")
				{
					$imgsrc = "<?php echo GLOBAL_IMG_URL?>"+$(this).data("imgsrc")+".png";
					$(".img"+forquestion).append("<div class='tivoli-words-image relative main"+$current_check_val+"'><img src='"+$imgsrc+"' title='"+$current_check_text+"' style='height:120px' /><div style='text-align:center;font-weight:normal;color:#000'>"+$current_check_text+"</div><div class='delete-absolute' data-id='"+$current_check_val+"' data-type='main'>x</div></div>").show();
				}
			}else{ //CHECKBOX UNCHECKED, REMOVE ALL THE STUFFS RELATED TO UNCHECKED CHECKBOX
				$("#selected_word_"+forquestion).val("");
				$(".img"+forquestion).find(".main"+$current_check_val).remove();
			}
			
		});
		
		
		loadWords();
		
		//WHEN SORT ALPHABETS ARE CLICKED
		$(".loadcharwords").on("click",function(){
			$(".loadcharwords").parent("li").removeClass("active");
			$(this).parent("li").addClass("active");
			loadWords("alpha",$(this).text());
		});
		
		//WHEN SEARCH BUTTON IS CLICKED
		$(".loadcharwords_search").on("click",function(){
			$keyword = $(this).prev(".searchstring").val();
			$keyword = ($keyword!="")?$keyword:"A";
			loadWords("search",$keyword);
		});
		
		
		//WHEN FILTER BY TAGS IS SELECTED
		$(".loadcharwords_filter").on("change",function(){
			$keyword = $(this).val();
			loadWords("tags",$keyword);
		});
	});
	
	
	function submitForm(){
		$mainlength = $.trim($("#selected_word").val());
		var errors = "";
		
		if($.trim($("#sentence1").val())=="")
		{
			errors +="Specify the first question.\n";
		}
		
		if($.trim($("#sentence2").val())=="")
		{
			errors +="Specify the second question.\n";
		}
		
		if($.trim($("#sentence3").val())=="")
		{
			errors +="Specify the third question.\n";
		}
		
		if($.trim($("#sentence4").val())=="")
		{
			errors +="Specify the fourth question.\n";
		}
		
		
		if($.trim($("#selected_word_1").val())=="")
		{
			errors +="Select the related word for the first sentence.\n";
		}
		
		if($.trim($("#selected_word_2").val())=="")
		{
			errors +="Select the related word for the second sentence.\n";
		}
		
		if($.trim($("#selected_word_3").val())=="")
		{
			errors +="Select the related word for the third sentence.\n";
		}
		
		if($.trim($("#selected_word_4").val())=="")
		{
			errors +="Select the related word for the fourth sentence.\n";
		}
		
		
		if($("#answer1").val()==""){
			errors +="Select the correct answer for the first question.\n";
		}
		
		if($("#answer2").val()==""){
			errors +="Select the correct answer for the second question.\n";
		}
		
		if($("#answer3").val()==""){
			errors +="Select the correct answer for the third question.\n";
		}
		
		if($("#answer4").val()==""){
			errors +="Select the correct answer for the fourth question.\n";
		}
		
		if(errors!=""){
			alert("Please fix the following error(s)\n\n"+errors);
			return false;
		}
		//alert($unrhyming);
		return true;
	}
	</script>
    <div class="container top">
      
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url()."exercise"; ?>">
            <?php echo "Exercises";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
        <a href="<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>">
          <?php echo ucwords($this->uri->segment(1))?>
          </a>
           <span class="divider">/</span>
        </li>
        <li class="active">
        <?php echo "Tivoli 5-2"?> 
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          <?php echo "Tivoli 5-2 - ".$exercise_desc[0]?> 
        </h2>
      </div>
 
      <?php
      //flash messages
	  
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == "done")
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_SUCCESS_MSG;
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_ERROR_MSG;
          echo '</div>';          
        }
      }
	  if($this->session->flashdata('errors'))
	  {
		   echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			echo "<b>".$this->session->flashdata('errors')."</b>";
			 echo '</div>'; 
	  }
      ?>
      
      <?php
	  
	
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '','onsubmit'=>'return submitForm()');
      
      //form validation
      echo validation_errors();
	 
      if($action=="Add")
	  {
      	echo form_open('tivoli/tivoli_5_2/add', $attributes);
	  }else{
		echo form_open('tivoli/tivoli_5_2/update/'.$this->uri->segment(4).'', $attributes);
	  }
	   $selectedword = array();
	   $imageselectedword = array();
	   if(isset($set) && $set!="")
		{
			//$this->utilities->printr($set,1);
			$value = $set["related_word"];
			
			foreach($value as $v)
			{
				$selectedword[] = $v["word_id"];
				$imageselectedword[] = "<div class='tivoli-words-image relative  main".$v["word_id"]."'><img src='".GLOBAL_IMG_URL.$v["imageRef"]."' title='".$v["word"]."' style='height:120px' />
			<div style='text-align:center;font-weight:normal;color:#000'>".$v["word"]."</div>
			<div class='delete-absolute' data-id='".$v["word_id"]."' data-type='main'>x</div>
			</div>";
			
			}
		}
      ?>
      
        <fieldset>
         <input type="hidden" id="exercise_id" name="exercise_id" value="<?php echo $exercise_desc[2]?>" />
      	<div class="control-group question-1">
            <label for="sentence" class="control-label">Exercise Sentence 1</label>
           
            <div class="controls">
            	 <div class="elements-block-5-2">
              			<input type="text" id="sentence1" name="sentence[]" value="<?php echo (isset($set))?$set["sentence"][0]:""?>" style="width:280px">
               			<br /><span class="help-inline">Type a sentence with a blank field. <b>For example:</b> ____ är god </span>
               			<div class="tivoli-5-2">
                        <?php
						$selanswer1 = "";
						if(isset($set))
						{
							$selanswer1 = isset($set["answer"][0])?$set["answer"][0]:"";
						}
						?>
						<select id="answer1" name="answer[]">
						<option value="">--Select an Answer--</option>
						<option value="den" <?php echo ($selanswer1=="den")?"selected='selected'":""?>>Den</option>
						<option value="det" <?php echo ($selanswer1=="det")?"selected='selected'":""?>>Det</option>
                        <option value="hon" <?php echo ($selanswer1=="hon")?"selected='selected'":""?>>Hon</option>
                        <option value="han" <?php echo ($selanswer1=="han")?"selected='selected'":""?>>Han</option>
						</select>	
                        </div>
               </div>
               <div class="image-block-5-2">
               		<input type="hidden" id="selected_word_1" name="related_word[]" value="<?php echo (isset($selectedword[0]))?$selectedword[0]:"";?>" />
               		<div class="tivoli-main-words-img img1" style="margin-top:15px;margin-left:27px"><?php echo (isset($imageselectedword[0]))?$imageselectedword[0]:""?></div>
              </div>
            </div>
              
              <div style="clear:both;"></div>
        </div>
        
        <div class="control-group question-2">
            <label for="sentence" class="control-label">Exercise Sentence 2</label>
           
            <div class="controls">
            	 <div class="elements-block-5-2">
              			<input type="text" id="sentence2" name="sentence[]" value="<?php echo (isset($set))?$set["sentence"][1]:""?>" style="width:280px">
               			<br /><span class="help-inline">Type a sentence with a blank field. <b>For example:</b> ____ är god </span>
               			<div class="tivoli-5-2">
                        <?php
						$selanswer2 = "";
						if(isset($set))
						{
							$selanswer2 = isset($set["answer"][1])?$set["answer"][1]:"";
						}
						?>
						<select id="answer2" name="answer[]">
						<option value="">--Select an Answer--</option>
						<option value="den" <?php echo ($selanswer2=="den")?"selected='selected'":""?>>Den</option>
						<option value="det" <?php echo ($selanswer2=="det")?"selected='selected'":""?>>Det</option>
                        <option value="hon" <?php echo ($selanswer2=="hon")?"selected='selected'":""?>>Hon</option>
                        <option value="han" <?php echo ($selanswer2=="han")?"selected='selected'":""?>>Han</option>
						</select>	
                        </div>
               </div>
               <div class="image-block-5-2">
               		<input type="hidden" id="selected_word_2" name="related_word[]" value="<?php echo (isset($selectedword[1]))?$selectedword[1]:"";?>" />
               		<div class="tivoli-main-words-img img2" style="margin-top:15px;margin-left:27px"><?php echo (isset($imageselectedword[1]))?$imageselectedword[1]:""?></div>
              </div>
            </div>
              
              <div style="clear:both;"></div>
        </div>
        
        <div class="control-group question-3">
            <label for="sentence" class="control-label">Exercise Sentence 3</label>
           
            <div class="controls">
            	 <div class="elements-block-5-2">
              			<input type="text" id="sentence3" name="sentence[]" value="<?php echo (isset($set))?$set["sentence"][2]:""?>" style="width:280px">
               			<br /><span class="help-inline">Type a sentence with a blank field. <b>For example:</b> ____ är god </span>
               			<div class="tivoli-5-2">
                        <?php
						$selanswer3 = "";
						if(isset($set))
						{
							$selanswer3 = isset($set["answer"][2])?$set["answer"][2]:"";
						}
						?>
						<select id="answer3" name="answer[]">
						<option value="">--Select an Answer--</option>
						<option value="den" <?php echo ($selanswer3=="den")?"selected='selected'":""?>>Den</option>
						<option value="det" <?php echo ($selanswer3=="det")?"selected='selected'":""?>>Det</option>
                        <option value="hon" <?php echo ($selanswer3=="hon")?"selected='selected'":""?>>Hon</option>
                        <option value="han" <?php echo ($selanswer3=="han")?"selected='selected'":""?>>Han</option>
						</select>	
                        </div>
               </div>
               <div class="image-block-5-2">
               		<input type="hidden" id="selected_word_3" name="related_word[]" value="<?php echo (isset($selectedword[2]))?$selectedword[2]:"";?>" />
               		<div class="tivoli-main-words-img img3" style="margin-top:15px;margin-left:27px"><?php echo (isset($imageselectedword[2]))?$imageselectedword[2]:""?></div>
              </div>
            </div>
              
              <div style="clear:both;"></div>
        </div>
        
        <div class="control-group question-4">
            <label for="sentence" class="control-label">Exercise Sentence 4</label>
           
            <div class="controls">
            	 <div class="elements-block-5-2">
              			<input type="text" id="sentence4" name="sentence[]" value="<?php echo (isset($set))?$set["sentence"][3]:""?>" style="width:280px">
               			<br /><span class="help-inline">Type a sentence with a blank field. <b>For example:</b> ____ är god </span>
               			<div class="tivoli-5-2">
                        <?php
						$selanswer4 = "";
						if(isset($set))
						{
							$selanswer4 = isset($set["answer"][3])?$set["answer"][3]:"";
						}
						?>
						<select id="answer4" name="answer[]">
						<option value="">--Select an Answer--</option>
						<option value="den" <?php echo ($selanswer4=="den")?"selected='selected'":""?>>Den</option>
						<option value="det" <?php echo ($selanswer4=="det")?"selected='selected'":""?>>Det</option>
                        <option value="hon" <?php echo ($selanswer4=="hon")?"selected='selected'":""?>>Hon</option>
                        <option value="han" <?php echo ($selanswer4=="han")?"selected='selected'":""?>>Han</option>
						</select>	
                        </div>
               </div>
               <div class="image-block-5-2">
               		<input type="hidden" id="selected_word_4" name="related_word[]" value="<?php echo (isset($selectedword[3]))?$selectedword[3]:"";?>" />
               		<div class="tivoli-main-words-img img4" style="margin-top:15px;margin-left:27px"><?php echo (isset($imageselectedword[3]))?$imageselectedword[3]:""?></div>
              </div>
            </div>
              
              <div style="clear:both;"></div>
        </div>
          
          <div class="control-group">
          
            <label for="inputError" class="control-label">Select Related Word</label>
            <div class="controls">
                <div class="tivoli-1-full">
                	<div class="select-question-5-2"><b>Select related word for</b>&nbsp;&nbsp;&nbsp;&nbsp;
                    <select id="select-question">
                    	<option value="1">Sentence 1</option>
                        <option value="2">Sentence 2</option>
                        <option value="3">Sentence 3</option>
                        <option value="4">Sentence 4</option>
                    </select>
                    </div>
                    <?php echo $gen_alphabets?>
                    <div style="max-height:500px;overflow:auto;">
                    	<ul class="list_words main-col"></ul>
                    </div>
                </div>		
            </div>
          </div>
          
          <div class="control-group">
            <label for="is_active" class="control-label">Set Status</label>
            <div class="controls">
            	<?php
				$selanswer = "";
              	if(isset($set))
				{
					$selanswer = isset($set["is_active"])?$set["is_active"]:"";
				}
				?>
                <select id="is_active" name="is_active">
                <option value="">--Select--</option>
                <option value="1" selected="selected" <?php echo ($selanswer=="1")?"selected='selected'":""?>>Active</option>
                <option value="0" <?php echo ($selanswer=="0")?"selected='selected'":""?>>Inactive</option>
                </select>	
            </div>
          </div>
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="button" onclick="document.location.href='<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>'">Cancel</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>
     <div class="pvw_loader">Loading...</div>