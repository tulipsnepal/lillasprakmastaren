    <div class="container top">

       <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a>
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url()."exercise"; ?>">
            <?php echo "Exercises";?>
          </a>
          <span class="divider">/</span>
        </li>
        <li class="active">
          <?php echo ucwords($this->uri->segment(1))?>
           <span class="divider">/</span>
        </li>
        <li class="active">
        <?php echo "Tivoli 5-2"?>
        </li>
      </ul>


 	<div class="page-header users-header">
        <h2>
           <?php echo "Tivoli 5-2 - ".$exercise_desc[0]?>
          <span><a href="<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>/add" class="btn btn-success" style="margin-left:20px;">Add a new Set</a></span>
          <?php /*?><span><a  href="<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>/json/<?php echo $exercise_desc[2]?>"  class="btn btn-warning">Create JSON of this Exercise</a></span><?php */?>
        </h2>
      </div>

    <div class="row">
    <?php
	if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') != "not_done")
        {
          	echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			if($this->session->flashdata('flash_message')=="done")
			{
            	echo CONST_SUCCESS_MSG;
			}else{
				echo "<strong>".$this->session->flashdata('flash_message')."</strong>";
			}
          echo '</div>';
        }else{
          	echo '<div class="alert alert-error">';
			echo '<a class="close" data-dismiss="alert">×</a>';

			if($this->session->flashdata('flash_message')=="not_done")
			{
				echo CONST_ERROR_MSG;
			}else{
				echo "<strong>".$this->session->flashdata('flash_message')."</strong>";
			}
          	echo '</div>';
        }
      }
	?>
        <div class="span12 columns">
              <?php
             if(trim($exercise_desc[0])!=""){
			?>
            <div>
            <img src="<?php echo EXTRA_IMAGE_URL.$exercise_desc[1]?>" width="250" />
            </div>
            <br clear="all" />
            <?php
			 }
			 ?>


         <form action="<?php echo base_url();?>tivoli/tivoli_5_2/deleteselected" method="POST" id="frmSetList">
          <table class="showAudio table table-striped table-bordered table-condensed table-border-bottom">
            <thead>
              <tr>
              <th width="4%"><input type="checkbox" id="checkallsets" /></th>
                <th class="header" width="4%">#</th>
                <th class="yellow header headerSortDown" width="24%">Sentence</th>
                <th class="red header" width="30%">Correct Answer (Den or Det)</th>
                 <th class="red header" width="18%">Related Image</th>
                  <th class="red header" width="10%">Set Status</th>
                <th class="red header" width="10%">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
			  $i = 1;

              foreach($set as $row)
              {
                $danger = ($row["is_active"]==0)?"class='danger'":"";
                echo '<tr '.$danger.'>';
				echo '<td><input type="checkbox" name="sets[]" class="exercise_sets" value="'.$row['set_id'].'" /></td>';
                echo '<td>'.$i.'</td>';
				echo '<td>';
				echo $row["sentence"][0]."<br>";
				echo $row["sentence"][1]."<br>";
				echo $row["sentence"][2]."<br>";
				echo $row["sentence"][3];
				echo '</td>';
                echo '<td>';
				echo strtoupper($row["answer"][0])."<br>";
				echo strtoupper($row["answer"][1])."<br>";
				echo strtoupper($row["answer"][2])."<br>";
				echo strtoupper($row["answer"][3]);
				echo '</td>';
				echo '<td>';
				echo '<a data-audio="'.$row['related_word'][0]['audioRef'].'" href="'.GLOBAL_IMG_URL.$row["related_word"][0]["imageRef"].'" class="fancybox btn-link">'.$row["related_word"][0]["word"].'</a><br>';
				echo '<a data-audio="'.$row['related_word'][1]['audioRef'].'" href="'.GLOBAL_IMG_URL.$row["related_word"][1]["imageRef"].'" class="fancybox btn-link">'.$row["related_word"][1]["word"].'</a><br>';
				echo '<a data-audio="'.$row['related_word'][2]['audioRef'].'" href="'.GLOBAL_IMG_URL.$row["related_word"][2]["imageRef"].'" class="fancybox btn-link">'.$row["related_word"][2]["word"].'</a><br>';
				echo '<a data-audio="'.$row['related_word'][3]['audioRef'].'" href="'.GLOBAL_IMG_URL.$row["related_word"][3]["imageRef"].'" class="fancybox btn-link">'.$row["related_word"][3]["word"].'</a><br>';
				echo '</td>';
				echo '<td>'.(($row["is_active"]==1)?"Active":"<span style='color:red'><i>Inactive</i></span>").'</td>';
				echo '<td class="crud-actions">
                  <a href="'.site_url().$this->uri->segment(1)."/".$this->uri->segment(2).'/update/'.$row['set_id'].'" class="btn btn-info">view & edit</a>
                </td>';
                echo '</tr>';
				$i++;
              }
              ?>
            </tbody>
          </table>
		<div><input type="submit" class="btn btn-danger" id="submitlist"  value="Delete Selected Sets"/></div>
        </form>
      </div>
    </div>
