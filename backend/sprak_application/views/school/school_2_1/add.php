    <script type="text/javascript">
	
		//REMOVE SELECTED ITEM
		$(document.body).on("click",".delete-absolute",function(){
			$type = $(this).data("type");
			$id = $(this).data("id");
			$("#"+$type).val("");
			if($type=="first_word")
			{
				$(".mainwords-first[value='"+$id+"']").prop("checked",false);
			}else{
				$(".mainwords-second[value='"+$id+"']").prop("checked",false);
			}
			$(this).parent(".relative").remove();
		});
		
		
	//SET GLOBAL AJAX URL TO BE USED FOR AJAX CALLS
	GlobalAjaxUrl = '<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>';
	
	$(function(){
		//CLICK EVENT TO THE CHECKBOX/RADIO-BUTTON. UPON CLICKING DISPLAY SELECTED IMAGE AND SET THE SELECTED WORD VALUE TO HIDDEN FIELD OR THE HIDDEN COMBOBOX
		$(document.body).on("click",".mainwords-first,.mainwords-second",function(){
			$parent = $(this).parent("label").parent("li").parent("ul");
			var holder ="";
			var words_needed = 0;
			//SET THE HOLDER VALUE DEPENDING UPON THE BLOCK OF WORDS
			if($parent.hasClass("main-col-first")) //FIRST BLOCK
			{
				holder = "first";
				words_needed = 1;
			}else if($parent.hasClass("main-col-second")) //SECOND BLOCK
			{
				holder = "second";
				words_needed = 3;
				
			}
			
			$current_check_val = $(this).val();
			$current_check_text = $.trim($(this).parent("label").text());
			var checkedBoxlength=$(".mainwords-"+holder+":checked").length;
			if(checkedBoxlength>words_needed){
				return false;
			}
			
			//POPULATE IMAGE OF SELECTED WORDS
			if($(this).prop("checked")){
				if(holder=="first")
				{
					$("#"+holder+"_word").val($(this).val());
					$("#"+holder+"_word").attr("data-text",$.trim($(this).parent("label").text()));
					$("."+holder).empty();
				}else{
					$chkval = $(this).val();
					$chktext = $(this).parent("label").text();
					var exists = $("#selected_options option[value='"+$chkval+"']").length !== 0;
					var optlength = $("#selected_options").children("option").length;
					// IF HIDDEN COMBO FOR OPTION WORDS DOESN'T CONTAINS SELETED WORD AND IT HAS LESS THAN 3 OPTIONS THEN APPEND OPTIONS OF THE SELECTED WORD.
					if(!exists && optlength<3)
					{
						$("#selected_options").append("<option value='"+$chkval+"'>"+$chktext+"</option>");
					}else{
						return false;
					}
				}
				
				if(typeof $(this).data("imgsrc") !=='undefined')
				{
					$imgsrc = "<?php echo GLOBAL_IMG_URL?>"+$(this).data("imgsrc")+".png";
					$("."+holder).append("<div class='tivoli-words-image relative main"+$current_check_val+"'><img src='"+$imgsrc+"' title='"+$current_check_text+"' style='height:120px' /><div style='text-align:center;font-weight:normal;color:#000'>"+$current_check_text+"</div><div class='delete-absolute' data-id='"+$current_check_val+"' data-type='"+holder+"_word'>x</div></div>").show();
				}
			}else{
				if(holder=="first"){
					$("#"+holder+"_word").val("");
					$("."+holder).find(".main"+$current_check_val).remove();
				}else{
					$("#selected_options option[value='"+$(this).val()+"']").remove();
				}
			}
			
		});
		
		
		loadWordsTwoInstances("first","all","A");
		loadWordsTwoInstances("second","all","A","no_image_as_well");
		
		
		//SORT ALPHABETS ON FIRST BLOCK OF WORD IS CLICKED
		$(".loadcharwords_first").on("click",function(){
			$(".loadcharwords_first").parent("li").removeClass("active");
			$(this).parent("li").addClass("active");
			loadWordsTwoInstances("first","alpha",$(this).text());
		});
		
		//SORT ALPHABETS ON SECOND BLOCK OF WORD IS CLICKED
		$(".loadcharwords_second").on("click",function(){
			$(".loadcharwords_second").parent("li").removeClass("active");
			$(this).parent("li").addClass("active");
			loadWordsTwoInstances("second","alpha",$(this).text(),"no_image_as_well");
		});
		
		//WHEN SEARCH BUTTON IN FIRST AND SECOND BLOCK OF WORD IS CLICKED
		$(".loadcharwords_first_search,.loadcharwords_second_search").on("click",function(){
			$keyword = $(this).prev(".searchstring").val();
			$clickedclass = this.className.replace("btn btn-primary ","");
			if($clickedclass=="loadcharwords_first_search"){
				var holder = "first";
				loadWordsTwoInstances(holder,"search",$keyword);
			}else{
				var holder = "second";
				loadWordsTwoInstances(holder,"search",$keyword,"no_image_as_well");
			}
			
		});
		
		//FILTER OPTION IN EITHER FIRST OR SECOND BLOCK IS SELECTED
		$(".loadcharwords_first_filter,.loadcharwords_second_filter").on("change",function(){
			$keyword = $(this).val();
			
			if($(this).hasClass("loadcharwords_first_filter")){
				var holder = "first";
				loadWordsTwoInstances(holder,"tags",$keyword);
			}else{
				var holder = "second";
				loadWordsTwoInstances(holder,"tags",$keyword,"no_image_as_well");
			}
			
		});
	});
	
	
	
	
	function submitForm(){
		
		$firsttext = $("#first_word").attr("data-text");
		$optionallength = $("#selected_options").children("option").length;
		
		var errors = "";
				
		if($.trim($("#first_word").val())=="")
		{
			errors +="-Select a first word .\n";
		}
		
		if($optionallength<3)
		{
			errors +="You must select 3 option words.\n";
		}
		
		$("#selected_options option").prop("selected",true);
		
		if(errors!=""){
			alert("Please fix the following error(s)\n\n"+errors);
			return false;
		}
		return true;
	}
	</script>
    <div class="container top">
      
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url()."exercise"; ?>">
            <?php echo "Exercises";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
        <a href="<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>">
          <?php echo ucwords($this->uri->segment(1))?>
          </a>
           <span class="divider">/</span>
        </li>
        <li class="active">
        <?php echo "School 2-1"?> 
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          <?php echo "School 2-1 - ".$exercise_desc[0]?>
        </h2>
      </div>
 
      <?php
      //flash messages
	  
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == "done")
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_SUCCESS_MSG;
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_ERROR_MSG;
          echo '</div>';          
        }
      }
	  if($this->session->flashdata('errors'))
	  {
		   echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			echo "<b>".$this->session->flashdata('errors')."</b>";
			 echo '</div>'; 
	  }
      ?>
      
      <?php
	  
	  $editmode = isset($set)?1:0;
     
      $attributes = array('class' => 'form-horizontal', 'id' => '','onsubmit'=>'return submitForm()');
     
      //form validation
      echo validation_errors();
	 
      if($action=="Add")
	  {
      	echo form_open('school/school_2_1/add', $attributes);
	  }else{
		echo form_open('school/school_2_1/update/'.$this->uri->segment(4).'', $attributes);
	  }
	  $selectedword_first = "";
	  $selectedword_first_text = "";
	   $imagesforselectedword_first = "";
	   $option_words = "";
	   
	   
	  
	   if(isset($set) && $set!="")
		{
			$first_word = $set["main"];
			foreach($set["optional"] as $opt_word)
			{
				$option_words .="<option value='".$opt_word["word_id"]."'>".$opt_word["word"]."</option>";
			}
			
			$selectedword_first = $first_word["word_id"];
			$selectedword_first_text = $first_word["word"];
			
			
			$imagesforselectedword_first = "<div class='tivoli-words-image relative main".$first_word["word_id"]."'><img src='".GLOBAL_IMG_URL.$first_word["imageRef"]."' title='".$first_word["word"]."' style='height:120px' />
											<div style='text-align:center;font-weight:normal;color:#000'>".$first_word["word"]."</div>
											<div class='delete-absolute' data-id='".$first_word["word_id"]."' data-type='first_word'>x</div>
											</div>";
			
			
		}
      ?>
      
        <fieldset>
         <input type="hidden" id="exercise_id" name="exercise_id" value="<?php echo $exercise_desc[2]?>" />
        <div class="control-group">
            <label for="sentence" class="control-label">Select Question word<div class="tivoli-main-words-img first" style="margin-top:15px;margin-left:27px;"><?php echo $imagesforselectedword_first?></div></label>
            <div class="controls">
              <input type="hidden" id="first_word" name="first_word" value="<?php echo $selectedword_first;?>"  data-text="<?php echo $selectedword_first_text?>">
              <div class="tivoli-1-full" style="padding-top:15px;">
              <?php echo $gen_alphabets_first?>
                    <div style="max-height:500px;overflow:auto;">
                    	<ul class="list_words main-col-first"></ul>
                    </div>
               </div>
            </div>
          </div>
          
          <div class="control-group">
            <label for="sentence" class="control-label">Select option words
            <div class="second" style="margin-top:15px;margin-left:27px;overflow:hidden;">
            <select id="selected_options" name="selected_options[]" multiple="multiple" style="width:150px;border:none;">
            <?php echo $option_words?>
            </select>
            </div></label>
            <div class="controls">
            	<div class="help-inline">Select 3 options</div>
              <div class="tivoli-1-full" style="padding-top:15px;">
               <?php echo $gen_alphabets_second?>
                    <div style="max-height:500px;overflow:auto;">
                    	<ul class="list_words main-col-second"></ul>
                    </div>
               </div>
            </div>
          </div>
          
          <div class="control-group">
            <label for="is_active" class="control-label">Set Status</label>
            <div class="controls">
            	<?php
				$selanswer = "";
              	if(isset($set))
				{
					$selanswer = isset($set["is_active"])?$set["is_active"]:"";
				}
				?>
                <select id="is_active" name="is_active">
                <option value="">--Select--</option>
                <option value="1" selected="selected" <?php echo ($selanswer=="1")?"selected='selected'":""?>>Active</option>
                <option value="0" <?php echo ($selanswer=="0")?"selected='selected'":""?>>Inactive</option>
                </select>	
            </div>
          </div>
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="button" onclick="document.location.href='<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>'">Cancel</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>
        <div class="pvw_loader">Loading...</div>