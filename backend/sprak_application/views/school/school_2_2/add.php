    <script type="text/javascript">
	//SET GLOBAL AJAX URL TO BE USED FOR AJAX CALLS
	GlobalAjaxUrl = '<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>';
	
	$(document).ready(function(){
		
		
		//CLICK EVENT TO THE CHECKBOX/RADIO-BUTTON. UPON CLICKING DISPLAY SELECTED IMAGE AND SET THE SELECTED WORD VALUE TO HIDDEN FIELD OR THE HIDDEN COMBOBOX
		$(document.body).on("click",".mainwords",function(){
			$current_check_val = $(this).val();
			$current_check_text = $.trim($(this).parent("label").text());
			var checkedBoxlength=$(".mainwords:checked").length;
			if(checkedBoxlength>8){ //DON'T LET CHECK MORE THAN 2 WORDS FOR MAIN & RHYMING WORDS
				return false;
			}
			
			//DON'T LET USER SELECT THE WORD THAT IS ALREADY BEEN SELECTED AS OPTIONAL WORD
			$chk = $("#selected_options option[value='"+$current_check_val+"']").length;
			if($chk>0){
				alert("The word '"+$current_check_text+"' is already checked as option. Please select different one or uncheck it first from option word");
				return false;
			}
			
			//POPULATE IMAGE OF SELECTED WORDS
			if($(this).prop("checked")){
				$chkval = $(this).val();
				$chktext = $(this).parent("label").text();
				var exists = $("#selected_mainwords option[value='"+$chkval+"']").length !== 0;
				var optlength = $("#selected_mainwords").children("option").length;
				
				
				// IF HIDDEN COMBO DOESN'T CONTAINS SELETED WORD AND IT HAS LESS THAN TWO OPTIONS THEN APPEND OPTIONS OF THE SELECTED WORD.
				if(!exists && optlength<8)
				{
					$("#selected_mainwords").append("<option value='"+$chkval+"'>"+$chktext+"</option>");
				}else{
					return false;
				}
				
				if($("#selected_mainwords").children("option").length>0){
					$(".display-selected-main").show();
				}else{
					$(".display-selected-main").hide();
				}
				showHideSelected();
			}else{ //THE CHECKBOX IS UNCHECKED SO REMOVE THE IMAGE FROM DISPLAY AND REMOVE THE OPTION WHICH VALUE EQUALS TO UNCHECKED CHECKBOX.
				$("#selected_mainwords option[value='"+$(this).val()+"']").remove();
				$(".tivoli-1-main-words-img").find(".main"+$current_check_val).remove();
				if($(".tivoli-1-main-words-img").find(".tivoli-words-image").length==0)
				{
					$(".tivoli-1-main-words-img").hide();
				}
				showHideSelected();
			}
			
		});
		
		//EVENT WHEN CHECKBOXES ON OPTIONAL WORD BLOCK ARE CLICKED
		$(document.body).on("click",".optionalwords",function(){
			$current_check_val = $(this).val();
			$current_check_text = $.trim($(this).parent("label").text());
			var checkedBoxlength=$(".optionalwords:checked").length;
			if(checkedBoxlength>8){ //DON'T LET ADD MORE THAN 3 OPTIONS. A SET HAVE 5 WORDS, 2 MAIN AND RHYMING WORDS AND  3 OPTION WORDS
				return false;
			}
			
			//DON'T LET USER SELECT THE WORD THAT IS ALREADY BEEN SELECTED AS MAIN/RHYMING WORD
			$chk = $("#selected_mainwords option[value='"+$current_check_val+"']").length;
			if($chk>0){
				alert("The word '"+$current_check_text+"' is already checked as main/option word. Please select different one or uncheck it first from main/option word");
				return false;
			}
			
			//POPULATE IMAGE OF SELECTED WORDS
			if($(this).prop("checked")){
				$chkval = $(this).val();
				$chktext = $(this).parent("label").text();
				var exists = $("#selected_options option[value='"+$chkval+"']").length !== 0;
				var optlength = $("#selected_options").children("option").length;
				
				// IF HIDDEN COMBO FOR OPTION WORDS DOESN'T CONTAINS SELETED WORD AND IT HAS LESS THAN 3 OPTIONS THEN APPEND OPTIONS OF THE SELECTED WORD.
				if(!exists && optlength<8)
				{
					$("#selected_options").append("<option value='"+$chkval+"'>"+$chktext+"</option>");
				}else{
					return false;
				}
				if($("#selected_options").children("option").length>0){
					$(".display-selected-option").show();
				}else{
					$(".display-selected-option").hide();
				}
				showHideSelected();
			}else{ //THE CHECKBOX IS UNCHECKED SO REMOVE THE IMAGE FROM DISPLAY AND REMOVE THE OPTION WHICH VALUE EQUALS TO UNCHECKED CHECKBOX.
				$("#selected_options option[value='"+$(this).val()+"']").remove();
				$(".tivoli-1-optional-words-img").find(".options"+$current_check_val).remove();
				if($(".tivoli-1-optional-words-img").find(".tivoli-words-image").length==0)
				{
					$(".tivoli-1-optional-words-img").hide();
				}
				showHideSelected();
			}
		});
		
		loadWordsAll();
		showHideSelected();
		
		//ALPHABETS TO SORT WORDS ARE CLICKED 
		$(".loadcharwords").on("click",function(){
			$(".loadcharwords").parent("li").removeClass("active");
			$(this).parent("li").addClass("active");
			loadWordsAll("alpha",$(this).text());
		});
		
		//SEARCH BUTTON IS CLICKED
		$(".loadcharwords_search").on("click",function(){
			$keyword = $(this).prev(".searchstring").val();
			$(".loadcharwords_filter").val("");
			loadWordsAll("search",$keyword);
		});
		
		//FILTER OPTION IS SELECTED
		$(".loadcharwords_filter").on("change",function(){
			$keyword = $(this).val();
			loadWordsAll("tags",$keyword);
		});
	});
	
	function showHideSelected(){
		$mainlength = $("#selected_mainwords").children("option").length;
		$optionallength = $("#selected_options").children("option").length;
		
		$mainlength>0?$(".selected-first").show():$(".selected-first").hide();
		$optionallength>0?$(".selected-second").show():$(".selected-second").hide();
	}
	
	function submitRhyme(){
		$mainlength = $("#selected_mainwords").children("option").length;
		$optionallength = $("#selected_options").children("option").length;
		
		var errors = "";
		
		if($mainlength<1)
		{
			errors +="You must choose the main word.\n";
		}
		
		if($optionallength<3)
		{
			errors +="You must choose 3 option words.\n";
		}
		
		if(errors!=""){
			alert("Please fix the following error(s)\n\n"+errors);
			return false;
		}
		$("#selected_mainwords option").prop("selected",true);
		$("#selected_options option").prop("selected",true);
		//return false;
	}
	</script>
    <div class="container top">
      
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url()."exercise"; ?>">
            <?php echo "Exercises";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
        <a href="<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>">
          <?php echo ucwords($this->uri->segment(1))?>
          </a>
           <span class="divider">/</span>
        </li>
        <li class="active">
        <?php echo "School 2-2"?> 
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
           <?php echo "School 2-2 - ".$exercise_desc[0]?> 
        </h2>
      </div>
 
      <?php
      //flash messages
	  
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == "done")
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_SUCCESS_MSG;
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_ERROR_MSG;
          echo '</div>';          
        }
      }
	  if($this->session->flashdata('errors'))
	  {
		   echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			echo "<b>".$this->session->flashdata('errors')."</b>";
			 echo '</div>'; 
	  }
      ?>
     
      <?php
	  
	  $editmode = isset($exercises)?1:0;
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '','onsubmit'=>'return submitRhyme()');
      
      //form validation
      echo validation_errors();
	 
      if($action=="Add")
	  {
      	echo form_open('school/school_2_2/add', $attributes);
	  }else{
		echo form_open('school/school_2_2/update/'.$this->uri->segment(4).'', $attributes);
	  }
	  
	   $optionformainword = "";
	   $optionforoptionalword = "";
	  
	  // $this->utilities->printr($set,1);
	   if(isset($set) && $set!="")
		{
			$mainword = $set["short_word"];
			$optionalwords = $set["long_word"];
			foreach($mainword as $value)
			{
				$optionformainword .= "<option value='".$value["word_id"]."'>".$value["word"]."</option>";
				
			}
			
			foreach($optionalwords as $optvalue)
			{
				$optionforoptionalword .= "<option value='".$optvalue["word_id"]."'>".$optvalue["word"]."</option>";
				
			}
		}
      ?>
      
        <fieldset>
         <input type="hidden" id="exercise_id" name="exercise_id" value="<?php echo $exercise_desc[2]?>" />
        
       
        
          <div class="control-group">
            <label for="inputError" class="control-label">Select Words</label>
            <div class="controls">
            <?php
            if(isset($set) && $set!="")
			{
			?>
            <div class="help-inline" style="padding:0 0 10px 10px">You can change the selected words by using the list below</div>
            <?php
			}
			?>
            <div class="upl tivoli-1-sub-head">Select total 16 words, 8 from each group</div>
            	<?php echo $gen_alphabets?>
            	<div class="upl tivoli-1-sub-head" style="float:left;width:42%">Short Word</div>
                <div class="upl tivoli-1-sub-head" style="float:left;width:40%">Long words</div>
                <div style="clear:both"></div>
                
            	<div style="max-height:500px;overflow:auto;width:98%;float:right">
                    <div class="tivoli-1-column1">
                        
                        <ul class="list_words main-col"></ul>
                      
                    </div>	
                    <div class="tivoli-1-column2">
                        
                        <ul class="list_words option-col"></ul>
                        
                    </div>	
                </div>
                <div style="overflow:auto;width:98%;float:right;margin-top:20px;">
                	<div class="tivoli-1-column1 selected-first">
                    	<div class="upl tivoli-1-sub-head" style="float:left;width:42%">Selected Short Word</div>
                    	<div style="clear:both"></div>
                        <div style="overflow:hidden">
                            <select name="selected_mainwords[]" id="selected_mainwords" multiple="multiple" style="border:none;height:145px;width:104%">
                           	 <?php echo $optionformainword;?>
                            </select>
                    	</div>
                    </div>
                     <div class="tivoli-1-column2 selected-second">
                     <div class="upl tivoli-1-sub-head" style="float:left;width:40%">Selected Long words</div>
                     <div style="clear:both"></div>
                     <div style="overflow:hidden">
                        <select name="selected_options[]" id="selected_options" multiple="multiple" style="border:none;height:145px;width:104%">
                        	<?php echo $optionforoptionalword;?>
                        </select>
                    </div>
                     </div>
                </div>
            </div>
          </div>
          <div class="control-group">
            <label for="is_active" class="control-label">Set Status</label>
            <div class="controls">
            	<?php
				$selanswer = "";
                if(isset($set))
				{
					$selanswer = isset($set["is_active"])?$set["is_active"]:"";
				}
				?>
                <select id="is_active" name="is_active">
                <option value="">--Select--</option>
                <option value="1" selected="selected" <?php echo ($selanswer=="1")?"selected='selected'":""?>>Active</option>
                <option value="0" <?php echo ($selanswer=="0")?"selected='selected'":""?>>Inactive</option>
                </select>	
            </div>
          </div>
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="button" onclick="document.location.href='<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>'">Cancel</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>
     <div class="pvw_loader">Loading...</div> 