    <script type="text/javascript">
	//SET GLOBAL AJAX URL TO BE USED FOR AJAX CALLS
	GlobalAjaxUrl = '<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>';
	$(document).ready(function(){
		
		//REMOVE SELECTED ITEM
		$(document.body).on("click",".delete-absolute",function(){
			$type = $(this).data("type");
			$id = $(this).data("id");
			$("#selected_mainwords option[value='"+$id+"']").remove();
			$(".mainwords[value='"+$id+"']").prop("checked",false);
			$(this).parent(".relative").remove();
		});
		
		
		//CLICK EVENT TO THE CHECKBOX/RADIO-BUTTON. UPON CLICKING DISPLAY SELECTED IMAGE AND SET THE SELECTED WORD VALUE TO HIDDEN FIELD OR THE HIDDEN COMBOBOX
		$(document.body).on("click",".mainwords",function(){
			$current_check_val = $(this).val();
			$current_check_text = $.trim($(this).parent("label").text());
			var checkedBoxlength=$(".mainwords:checked").length;
			if(checkedBoxlength>3){ //DON'T LET USER SELECT MORE THAN 4 WORDS
				return false;
			}
			
			//POPULATE IMAGE OF SELECTED WORDS
			if($(this).prop("checked")){
				$chkval = $(this).val();
				$chktext = $(this).parent("label").text();
				var exists = $("#selected_mainwords option[value='"+$chkval+"']").length !== 0;
				var optlength = $("#selected_mainwords").children("option").length;
				
				// IF HIDDEN COMBO DOESN'T CONTAINS SELETED WORD AND IT HAS LESS THAN 4 OPTIONS THEN APPEND OPTIONS OF THE SELECTED WORD.
				if(!exists && optlength<3)
				{
					$("#selected_mainwords").append("<option value='"+$chkval+"'>"+$chktext+"</option>");
				}else{
					return false;
				}
				
				//SHOW IMAGE OF THE SELECTED WORD WITH THE URL FETCHED FROM THE DATA ATTRIBUTE "imgsrc" OF THE SELECTED CHECKBOX
				if($(this).data("imgsrc")!="")
				{
					$imgsrc = "<?php echo GLOBAL_IMG_URL?>"+$(this).data("imgsrc")+".png";
					$(".tivoli-main-words-img").append("<div class='tivoli-words-image relative main"+$current_check_val+"'><img src='"+$imgsrc+"' title='"+$current_check_text+"' style='height:120px' /><div style='text-align:center'>"+$current_check_text+"</div><div class='delete-absolute left' data-id='"+$current_check_val+"' data-type='main'>x</div></div>").show();
				}else{
					alert("No image linked with this word.");
					$(this).prop("checked",false);
				}
			}else{
				//REMOVE IMAGE AND OPTIONS IF THE CHECKBOX IS UNCHECKED
				 $("#selected_mainwords option[value='"+$(this).val()+"']").remove();
				$(".tivoli-main-words-img").find(".main"+$current_check_val).remove();
				if($(".tivoli-main-words-img").find(".tivoli-words-image").length==0)
				{
					$(".tivoli-main-words-img").hide();
				}
			}
			
		});
		
		//AMONG 4 WORDS 1 IS UNRHYMING, SELECT THE UNRHYMING RADIO IF ITS VALUE EQUALS THE HIDDEN FIELD'S VALUE
		$(document.body).on("click",".tivoli-words-image",function(){
			$chkbox = $(this).find(".unrhymingword");
			$chkbox.prop("checked",true);
		});
		
		loadWords();
		
		//WHEN SORT ALPHABETS ARE CLICKED
		$(".loadcharwords").on("click",function(){
			$(".loadcharwords").parent("li").removeClass("active");
			$(this).parent("li").addClass("active");
			loadWords("alpha",$(this).text());
		});
		
		//WHEN SEARCH IS CLICKED
		$(".loadcharwords_search").on("click",function(){
			$keyword = $(this).prev(".searchstring").val();
			loadWords("search",$keyword);
		});
		
		
		//WHEN FILTER BY TAGS IS SELECTED
		$(".loadcharwords_filter").on("change",function(){
			$keyword = $(this).val();
			loadWords("tags",$keyword);
		});
	});
	
		
	
	function submitRhyme(){
		$mainlength = $("#selected_mainwords").children("option").length;
		var errors = "";
		var options = 0;
		
		if($mainlength<3)
		{
			errors +="You must choose 3 images for 3 sentences from the list.\n";
		}
		
		
		$(".option_sentences").each(function(){
			if($.trim($(this).val())==""){
				options++;
			}
		});

		if(options>0){
			errors +="You must specify all three option sentences.\n";
		}
		
		
		if(errors!=""){
			alert("Please fix the following error(s)\n\n"+errors);
			return false;
		}
		$("#selected_mainwords option").prop("selected",true);
		//alert($unrhyming);
		return true;
	}
	</script>
    <div class="container top">
      
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url()."exercise"; ?>">
            <?php echo "Exercises";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
        <a href="<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>">
          <?php echo ucwords($this->uri->segment(1))?>
          </a>
           <span class="divider">/</span>
        </li>
        <li class="active">
        <?php echo "Football 1-2"?>
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          <?php echo "Football 1-2 - ".$exercise_desc[0]?> 
        </h2>
      </div>
 
      <?php
      //flash messages
	  
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == "done")
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_SUCCESS_MSG;
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_ERROR_MSG;
          echo '</div>';          
        }
      }
	  if($this->session->flashdata('errors'))
	  {
		   echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			echo "<b>".$this->session->flashdata('errors')."</b>";
			 echo '</div>'; 
	  }
      ?>
      
      <?php
	  
	  $editmode = isset($exercises)?1:0;
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '','onsubmit'=>'return submitRhyme()');
      
      //form validation
      echo validation_errors();
	 
      if($action=="Add")
	  {
      	echo form_open('football/football_1_2/add', $attributes);
	  }else{
		echo form_open('football/football_1_2/update/'.$this->uri->segment(4).'', $attributes);
	  }
	  
	   $selectedwords = "";
	   $imagesforselectedwords = "";
	   if(isset($set) && $set!="")
		{
			$swords = $set["words"];
			
			foreach($swords as $value)
			{
				
				$selectedwords .= "<option value='".$value["word_id"]."'>".$value["word"]."</option>";
				//$imagesforselectedwords .="<div class='tivoli-words-image main".$value["word_id"]."'><img class='words-image' src='".GLOBAL_IMG_URL.$value["imageRef"]."' title='".$value["word"]."' style='height:120px' /></div>";
				$imagesforselectedwords .="<div class='tivoli-words-image relative main".$value["word_id"]."'><img src='".GLOBAL_IMG_URL.$value["imageRef"]."' title='".$value["word"]."' style='height:120px' />
				<div style='text-align:center'>".$value["word"]."</div>
				<div class='delete-absolute left' data-id='".$value["word_id"]."' data-type='main'>x</div>
				</div>";
			}
		}
      ?>
      
        <fieldset>
         <input type="hidden" id="exercise_id" name="exercise_id" value="<?php echo $exercise_desc[2]?>" />
        <select name="selected_mainwords[]" id="selected_mainwords" style="display:none;" multiple="multiple">
        <?php echo $selectedwords?>
        </select>
        <div class="control-group" style="margin-bottom:40px">
        	<div class="tivoli-main-words-img" <?php echo ($imagesforselectedwords!="")?"style='display:block;'":""?>><?php echo $imagesforselectedwords?></div>
        </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Select Words</label>
            <div class="controls">
            <?php
            if(isset($set) && $set!="")
			{
			?>
            <div class="help-inline" style="padding:0 0 10px 0px">You can change the selected words by using the list below</div>
            <?php
			}
			?>
                <div class="tivoli-1-full" style="margin-left:12px;">
                	<div class="upl tivoli-1-sub-head">Select 3 images and specify three sentences in same order accordingly to link three images</div>
                    <?php echo $gen_alphabets?>
                    <div  style="max-height:500px;overflow:auto">
                    	<ul class="list_words main-col"></ul>
                    </div>
                </div>		
            </div>
          </div>

          <div class="control-group">
          	<label for="option_sentences" class="control-label">Type Sentences</label>
          	<div class="controls">
          		<?php
          			for($k=0; $k<3; $k++)
          			{
          		?>
          			<div class="answer_options">
             	<span>Sentence <?php echo $k+1?></span><input type="text" class="option_sentences" name="option_sentences[]" style="width:250px" value="<?php echo isset($set["sentence"])?$set["sentence"][$k]:"";?>" />
             	</div>	
          		<?php
          			}
          		?>
          	</div>
          </div>
          <div class="control-group">
            <label for="is_active" class="control-label">Set Status</label>
            <div class="controls">
            	<?php
				$selanswer = "";
                if(isset($set))
				{
					$selanswer = isset($set["is_active"])?$set["is_active"]:"";
				}
				?>
                <select id="is_active" name="is_active">
                <option value="">--Select--</option>
                <option value="1" selected="selected" <?php echo ($selanswer=="1")?"selected='selected'":""?>>Active</option>
                <option value="0" <?php echo ($selanswer=="0")?"selected='selected'":""?>>Inactive</option>
                </select>	
            </div>
          </div>
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="button" onclick="document.location.href='<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>'">Cancel</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>
     <div class="pvw_loader">Loading...</div> 