    <script type="text/javascript">
	
		//REMOVE SELECTED ITEM
		/*$(document.body).on("click",".delete-absolute",function(){
			$type = $(this).data("type");
			$id = $(this).data("id");
			$("#"+$type).val("");
			if($type=="first_word")
			{
				$(".mainwords-first[value='"+$id+"']").prop("checked",false);
			}else{
				$(".mainwords-second[value='"+$id+"']").prop("checked",false);
			}
			$(this).parent(".relative").remove();
		});*/
		
		
	//SET GLOBAL AJAX URL TO BE USED FOR AJAX CALLS
	GlobalAjaxUrl = '<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>';
	
	$(function(){
		
		//REMOVE SELECTED ITEM
		$(document.body).on("click",".delete-absolute-first,.delete-absolute-second,.delete-absolute-names",function(){
			$type = $(this).data("type");
			//alert($type);
			$id = $(this).data("id");
			var holder = $type;
			$("#"+holder+"_word option[value='"+$id+"']").remove();
				$(".mainwords-"+holder+"[value='"+$id+"']").prop("checked",false);
				if(!$("#"+holder+"_word").children("option").length>0){
					$(".tivoli-1-main-words-img "+holder).hide();
				}			
			$(this).parent(".relative").remove();
			
		});
		
		//CLICK EVENT TO THE CHECKBOX/RADIO-BUTTON. UPON CLICKING DISPLAY SELECTED IMAGE AND SET THE SELECTED WORD VALUE TO HIDDEN FIELD OR THE HIDDEN COMBOBOX
		$(document.body).on("click",".mainwords-first,.mainwords-second,.mainwords-names",function(){
			$parent = $(this).parent("label").parent("li").parent("ul");
			var holder ="";
			
			//SET THE HOLDER VALUE DEPENDING UPON THE BLOCK OF WORDS
			if($parent.hasClass("main-col-first")) //FIRST BLOCK
			{
				holder = "first";
			}else if($parent.hasClass("main-col-second")) //SECOND BLOCK
			{
				holder = "second";
			}else{
				holder = "names";
			}
			
			$current_check_val = $(this).val();
			$current_check_text = $.trim($(this).parent("label").text());
			var checkedBoxlength=$(".mainwords-"+holder+":checked").length;
			if(checkedBoxlength>4){
				return false;
			}
			
			//POPULATE IMAGE OF SELECTED WORDS
			if($(this).prop("checked")){
				$chkval = $(this).val();
				$chktext = $(this).parent("label").text();
				//$("#"+holder+"_word").val($(this).val());
				//$("#"+holder+"_word").attr("data-text",$.trim($(this).parent("label").text()));
				var exists = $("#"+holder+"_word option[value='"+$chkval+"']").length !== 0;
				var optlength = $("#"+holder+"_word").children("option").length;
				
				if(!exists && optlength<4)
				{
					$("#"+holder+"_word").append("<option value='"+$chkval+"'>"+$chktext+"</option>");
				}else{
					return false;
				}
				//$("."+holder).empty();
				if($(this).data("imgsrc")!="")
				{
					if(holder=="second"){
						$selected_name = "<select id='article' name='word_article[]' style='width:120px;margin-left:6px'><option value='en'>en</option><option value='ett'>ett</option></select>";
					}else if(holder=="names"){
						$selected_name = "";
					}else{
						$selected_name = $current_check_text;
					}
					$imgsrc = (holder=="names")?"<?php echo GLOBAL_IMG_URL?>names/"+$(this).data("imgsrc")+".png":"<?php echo GLOBAL_IMG_URL?>"+$(this).data("imgsrc")+".png";
					$("."+holder).append("<div class='tivoli-words-image relative main"+$current_check_val+"'><img src='"+$imgsrc+"' title='"+$current_check_text+"' style='height:120px' /><div style='text-align:center;font-weight:normal;color:#000'>"+$selected_name+"</div><div class='delete-absolute delete-absolute-"+holder+"' data-id='"+$current_check_val+"' data-type='"+holder+"'>x</div></div>").show();
				}
			}else{
				//$("#"+holder+"_word").val("");
				$("#"+holder+"_word option[value='"+$(this).val()+"']").remove();
				$("."+holder).find(".main"+$current_check_val).remove();
			}
			
		});
		
		
		loadWordsTwoInstances("first","all","A");
		loadWordsTwoInstances("second","all","A");
		
		//SORT ALPHABETS ON FIRST BLOCK OF WORD IS CLICKED
		$(".loadcharwords_first").on("click",function(){
			$(".loadcharwords_first").parent("li").removeClass("active");
			$(this).parent("li").addClass("active");
			loadWordsTwoInstances("first","alpha",$(this).text());
		});
		
		//SORT ALPHABETS ON SECOND BLOCK OF WORD IS CLICKED
		$(".loadcharwords_second").on("click",function(){
			$(".loadcharwords_second").parent("li").removeClass("active");
			$(this).parent("li").addClass("active");
			loadWordsTwoInstances("second","alpha",$(this).text());
		});
		
		//WHEN SEARCH BUTTON IN FIRST AND SECOND BLOCK OF WORD IS CLICKED
		$(".loadcharwords_first_search,.loadcharwords_second_search").on("click",function(){
			$keyword = $(this).prev(".searchstring").val();
			$clickedclass = this.className.replace("btn btn-primary ","");
			if($clickedclass=="loadcharwords_first_search"){
				var holder = "first";
			}else{
				var holder = "second";
			}
			loadWordsTwoInstances(holder,"search",$keyword);
		});
		
		//FILTER OPTION IN EITHER FIRST OR SECOND BLOCK IS SELECTED
		$(".loadcharwords_first_filter,.loadcharwords_second_filter").on("change",function(){
			$keyword = $(this).val();
			
			
			if($(this).hasClass("loadcharwords_first_filter")){
				var holder = "first";
			}else{
				var holder = "second";
			}
			loadWordsTwoInstances(holder,"tags",$keyword);
		});
	});
	
	
	
	
	function submitForm(){
		
		$names_length = $("#names_word").children("option").length;
		$first_word_length = $("#first_word").children("option").length;
		$second_word_length = $("#second_word").children("option").length;
		
		var errors = "";
		if($names_length<4)
		{
			errors +="- You must select 4 names from part 1 .\n";
		}
				
		if($first_word_length<4)
		{
			errors +="- You must select 4 words from part 2 .\n";
		}
		
		if($second_word_length<4)
		{
			errors +="- You must select 4 words from part 3 .\n";
		}
		
		
		$("#names_word option").prop("selected",true);
		$("#first_word option").prop("selected",true);
		$("#second_word option").prop("selected",true);
		
		if(errors!=""){
			alert("Please fix the following error(s)\n\n"+errors);
			return false;
		}
		return true;
	}
	</script>
    <div class="container top">
      
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url()."exercise"; ?>">
            <?php echo "Exercises";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
        <a href="<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>">
          <?php echo ucwords($this->uri->segment(1))?>
          </a>
           <span class="divider">/</span>
        </li>
        <li class="active">
        <?php echo "Football 3-1"?> 
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          <?php echo "Football 3-1 - ".$exercise_desc[0]?>
        </h2>
      </div>
 
      <?php
      //flash messages
	  
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == "done")
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_SUCCESS_MSG;
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_ERROR_MSG;
          echo '</div>';          
        }
      }
	  if($this->session->flashdata('errors'))
	  {
		   echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			echo "<b>".$this->session->flashdata('errors')."</b>";
			 echo '</div>'; 
	  }
      ?>
      
      <?php
	  
	  $editmode = isset($set)?1:0;
     
      $attributes = array('class' => 'form-horizontal', 'id' => '','onsubmit'=>'return submitForm()');
     
      //form validation
      echo validation_errors();
	 
      if($action=="Add")
	  {
      	echo form_open('football/football_3_1/add', $attributes);
	  }else{
		echo form_open('football/football_3_1/update/'.$this->uri->segment(4).'', $attributes);
	  }
	 
	 	$characters = "";
		$name_word_options = "";
		$name_images = "";
		$first_word_items = "";
		$first_word_options="";
		$first_images="";;
		$second_word_items = "";
		$second_word_options="";
		$second_images="";
		$set_article = "";
	   if(isset($set) && $set!="")
		{
						
			$characters = $set["names_word"];
			foreach($characters as $name){
				$name_word_options .="<option value='".$name["word_id"]."'>".$name["word"]."</option>";
				$name_images .='<div class="tivoli-words-image relative main'.$name["word_id"].'"><img src="'.GLOBAL_IMG_URL."names/".$name["imageRef"].'" title="'.$name["word"].'" style="height:120px"><div style="text-align:center;font-weight:normal;color:#000"></div><div class="delete-absolute delete-absolute-names" data-id="'.$name["word_id"].'" data-type="names">x</div></div>';
			}
			
			$first_word_items = $set["first_word"];
			foreach($first_word_items as $first_w){
				$first_word_options .="<option value='".$first_w["word_id"]."'>".$first_w["word"]."</option>";
				$first_images .='<div class="tivoli-words-image relative main'.$first_w["word_id"].'"><img src="'.GLOBAL_IMG_URL.$first_w["imageRef"].'" title="'.$first_w["word"].'" style="height:120px"><div style="text-align:center;font-weight:normal;color:#000">'.$first_w["word"].'</div><div class="delete-absolute delete-absolute-first" data-id="'.$first_w["word_id"].'" data-type="first">x</div></div>';
			}
			
			$second_word_items = $set["second_word"];
			$k=0;
			$set_article = $set["word_article"];
			//$this->utilities->printr($set_article,1);
			foreach($second_word_items as $second_w){
				
				$select_en = ($set_article[$k]=="en")?' selected="selected "':"";
				$select_ett = ($set_article[$k]=="ett")?' selected="selected "':"";
				$selectarticle = '<select id="article" name="word_article[]" style="width:120px;margin-left:6px">
								<option value="en" '.$select_en.' data-test="'.$set_article[$k].'">en</option><option value="ett" '.$select_ett.'>ett</option>
								</select>';
				$second_word_options .="<option value='".$second_w["word_id"]."'>".$second_w["word"]."</option>";
				$second_images .='<div class="tivoli-words-image relative main'.$second_w["word_id"].'"><img src="'.GLOBAL_IMG_URL.$second_w["imageRef"].'" title="'.$second_w["word"].'" style="height:120px"><div style="text-align:center;font-weight:normal;color:#000">'.$selectarticle.'</div><div class="delete-absolute delete-absolute-second" data-id="'.$second_w["word_id"].'" data-type="second">x</div></div>';
				$k++;
			}
			
		}
      ?>
       <fieldset>
        
         <input type="hidden" id="exercise_id" name="exercise_id" value="<?php echo $exercise_desc[2]?>" />
         <div class="control-group">
         	<label for="sentence" class="control-label">Part 1 (Names)</label>
            <div class="controls">
            	<div class="help-inline">Select 4 names.</div>
                <div class="tivoli-main-words-img names" style="margin-top:15px;margin-left:27px;"><?php echo $name_images?></div>
                <select id="names_word" name="names_word[]" multiple="multiple" style="display:none;"><?php echo $name_word_options?></select>
                <div class="tivoli-1-full" style="padding-top:15px;margin-top:10px;">
                    <div style="max-height:500px;overflow:auto;">
                            <ul class="list_words">
                            <?php echo $character_names?>
                            </ul>
                    </div>
                </div>
            </div>
         </div>
        <div class="control-group">
            <label for="sentence" class="control-label">Part 2 (Words)</label>
            <div class="controls">
             <div class="help-inline">Select 4 words.</div>
            <div class="tivoli-main-words-img first" style="margin-top:15px;margin-left:27px;"><?php echo $first_images?></div>
             <?php /*?> <input type="hidden" id="first_word" name="first_word" value="<?php echo $selectedword_first;?>"  data-text="<?php echo $selectedword_first_text?>"><?php */?>
              <select id="first_word" name="first_word[]" multiple="multiple" style="display:none;"><?php echo $first_word_options?></select>
              <div class="tivoli-1-full" style="padding-top:15px;margin-top:10px;">
              <?php echo $gen_alphabets_first?>
                    <div style="max-height:500px;overflow:auto;">
                    	<ul class="list_words main-col-first"></ul>
                    </div>
               </div>
            </div>
          </div>
          
          <div class="control-group">
            <label for="sentence" class="control-label">Part 3 (Phrases)</label>
            <div class="controls">
            <div class="help-inline">Select 4 words and then select the article corresponding to it.</div>
            <div class="tivoli-main-words-img second" style="margin-top:15px;margin-left:27px;"><?php echo $second_images?></div>
           <?php /*?> <input type="hidden" id="second_word" name="second_word" value="<?php echo $selectedword_second;?>" data-text="<?php echo $selectedword_second_text?>"><?php */?>
            <select id="second_word" name="second_word[]" multiple="multiple"  style="display:none;"><?php echo $second_word_options?></select>
              <div class="tivoli-1-full" style="padding-top:15px;margin-top:10px;">
               <?php echo $gen_alphabets_second?>
                    <div style="max-height:500px;overflow:auto;">
                    	<ul class="list_words main-col-second"></ul>
                    </div>
               </div>
            </div>
          </div>
          
          <div class="control-group">
            <label for="is_active" class="control-label">Set Status</label>
            <div class="controls">
            	<?php
				$selanswer = "";
                if($editmode==1)
				{
					$selanswer = $set["is_active"];
				}
				?>
                <select id="is_active" name="is_active">
                <option value="">--Select--</option>
                <option value="1" selected="selected" <?php echo ($selanswer=="1")?"selected='selected'":""?>>Active</option>
                <option value="0" <?php echo ($selanswer=="0")?"selected='selected'":""?>>Inactive</option>
                </select>	
            </div>
          </div>
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="button" onclick="document.location.href='<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>'">Cancel</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>
        <div class="pvw_loader">Loading...</div>