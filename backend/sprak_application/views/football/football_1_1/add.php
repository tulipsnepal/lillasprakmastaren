    <script type="text/javascript">
	//SET GLOBAL AJAX URL TO BE USED FOR AJAX CALLS
	GlobalAjaxUrl = '<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>';
	$(document).ready(function(){
		
		//REMOVE SELECTED ITEM
		$(document.body).on("click",".delete-absolute",function(){
			$type = $(this).data("type");
			$id = $(this).data("id");
			$("#selected_word").val("");
			$(".mainwords[value='"+$id+"']").prop("checked",false);
			$(this).parent(".relative").remove();
			
		});
		
		//CLICK EVENT TO THE CHECKBOX/RADIO-BUTTON. UPON CLICKING DISPLAY SELECTED IMAGE AND SET THE SELECTED WORD VALUE TO HIDDEN FIELD OR THE HIDDEN COMBOBOX
		$(document.body).on("click",".mainwords",function(){
			$current_check_val = $(this).val();
			$current_check_text = $.trim($(this).parent("label").text());
			var checkedBoxlength=$(".mainwords:checked").length;
			if(checkedBoxlength>1){ //DON'T LET USER CHECK MORE THAN ONE CHECKBOX
				return false;
			}
			
			//POPULATE IMAGE OF SELECTED WORDS
			
			if($(this).prop("checked")){
				$("#selected_word").val($(this).val());
				$(".tivoli-main-words-img").empty();
				
				//FETCH "imgsrc" DATA ATTRIBUTE FROM CHECKBOXES AND SHOW THE IMAGE OF SELECTED WORDS
				if($(this).data("imgsrc")!="")
				{
					$imgsrc = "<?php echo GLOBAL_IMG_URL?>"+$(this).data("imgsrc")+".png";
					$(".tivoli-main-words-img").append("<div class='tivoli-words-image relative main"+$current_check_val+"'><img src='"+$imgsrc+"' title='"+$current_check_text+"' style='height:120px' /><div style='text-align:center;font-weight:normal;color:#000'>"+$current_check_text+"</div><div class='delete-absolute' data-id='"+$current_check_val+"' data-type='main'>x</div></div>").show();
				}
			}else{ //CHECKBOX UNCHECKED, REMOVE ALL THE STUFFS RELATED TO UNCHECKED CHECKBOX
				$("#selected_word").val("");
				$(".tivoli-main-words-img").find(".main"+$current_check_val).remove();
			}
			
		});
		
		<?php
		if(isset($set) && $set!="") //in case of update show the words starting with the first character of the word that comes from database
		{
		?>
		loadWords("alpha","<?php echo  mb_substr($set["selected_word"]["word"],0,1);?>")
		<?php
		}else{
		?>
		loadWords("alpha");
		<?php
		}
		?>
		
		//WHEN SORT ALPHABETS ARE CLICKED
		$(".loadcharwords").on("click",function(){
			$(".loadcharwords").parent("li").removeClass("active");
			$(this).parent("li").addClass("active");
			loadWords("alpha",$(this).text());
		});
		
		//WHEN SEARCH BUTTON IS CLICKED
		$(".loadcharwords_search").on("click",function(){
			$keyword = $(this).prev(".searchstring").val();
			$keyword = ($keyword!="")?$keyword:"A";
			loadWords("search",$keyword);
		});
		
		
		//WHEN FILTER BY TAGS IS SELECTED
		$(".loadcharwords_filter").on("change",function(){
			$keyword = $(this).val();
			loadWords("tags",$keyword);
		});
	});
	
	
	
	function submitForm(){
		$mainlength = $.trim($("#selected_word").val());
		var errors = "";
		var options = 0;
		
		if($.trim($("#selected_word").val())=="")
		{
			errors +="Select the question word/image.\n";
		}

		$(".option_sentences").each(function(){
			if($.trim($(this).val())==""){
				options++;
			}
		});

		if(options>0){
			errors +="You must specify all three option sentences.\n";
		}

		if($(".correct_sentence:checked").length==0){
			errors +="You must select one correct sentence.\n";	
		}

		
		
		
		
		
		if(errors!=""){
			alert("Please fix the following error(s)\n\n"+errors);
			return false;
		}
		//alert($unrhyming);
		return true;
	}
	</script>
    <div class="container top">
      
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url()."exercise"; ?>">
            <?php echo "Exercises";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
        <a href="<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>">
          <?php echo ucwords($this->uri->segment(1))?>
          </a>
           <span class="divider">/</span>
        </li>
        <li class="active">
        <?php echo "Football 1-1"?> 
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          <?php echo "Football 1-1 - ".$exercise_desc[0]?> 
        </h2>
      </div>
 
      <?php
      //flash messages
	  
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == "done")
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_SUCCESS_MSG;
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_ERROR_MSG;
          echo '</div>';          
        }
      }
	  if($this->session->flashdata('errors'))
	  {
		   echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			echo "<b>".$this->session->flashdata('errors')."</b>";
			 echo '</div>'; 
	  }
      ?>
      
      <?php
	  
	
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '','onsubmit'=>'return submitForm()');
      
      //form validation
      echo validation_errors();
	 
      if($action=="Add")
	  {
      	echo form_open('football/football_1_1/add', $attributes);
	  }else{
		echo form_open('football/football_1_1/update/'.$this->uri->segment(4).'', $attributes);
	  }
	 	$selectedword = "";
	   $imagesforselectedword = "";
	   if(isset($set) && $set!="")
		{
			$value = $set["selected_word"];
			$selectedword = $value["word_id"];
			$imagesforselectedword = "<div class='tivoli-words-image relative main".$value["word_id"]."'><img src='".GLOBAL_IMG_URL.$value["imageRef"]."' title='".$value["word"]."' style='height:120px' />
			<div style='text-align:center;font-weight:normal;color:#000'>".$value["word"]."</div>
			<div class='delete-absolute' data-id='".$value["word_id"]."' data-type='main'>x</div>
			</div>";
			
		}
      ?>
      
        <fieldset>
         <input type="hidden" id="exercise_id" name="exercise_id" value="<?php echo $exercise_desc[2]?>" />
     
          <div class="control-group">
          
            <label for="inputError" class="control-label">Select Word<div class="tivoli-main-words-img" style="margin-top:15px;margin-left:27px"><?php echo $imagesforselectedword?></div></label>
            <input type="hidden" id="selected_word" name="selected_word" value="<?php echo $selectedword;?>" />
            <div class="controls">
                <div class="tivoli-1-full">
                	<div class="upl tivoli-1-sub-head">Select a word/image related to exercise.</div>
                    <?php echo $gen_alphabets?>
                    <div style="max-height:500px;overflow:auto;">
                    	<ul class="list_words main-col"></ul>
                    </div>
                </div>		
            </div>
          </div>
          
           <div class="control-group">
            <label for="inputError" class="control-label">Specify the answer</label>
            <div class="controls">
            	<div class="help-inline">Specify the option sentences and select the correct one that matches with the selected image</div>
            	<?php
            	for($k=0;$k<3;$k++)
            	{
            		$checked="";
            		if(isset($set["answer"]) && $set["answer"]==$k){
            			$checked="checked='checked'";
            		}
            	?>
             	<div class="answer_options">
             	<span>Option <?php echo $k+1?></span><input type="text" class="option_sentences" name="options[]" style="width:250px" value="<?php echo isset($set["sentence"])?$set["sentence"][$k]:"";?>" />
             	<input type="radio" name="chkCorrectAnswer" class="correct_sentence" value="<?php echo $k?>" <?php echo $checked;?> />	
             	</div>	
             	<?php
             	}
             	?>
            </div>
          </div>
          <div class="control-group">
            <label for="is_active" class="control-label">Set Status</label>
            <div class="controls">
            	<?php
				$selanswer = "";
              	if(isset($set))
				{
					$selanswer = isset($set["is_active"])?$set["is_active"]:"";
				}
				?>
                <select id="is_active" name="is_active">
                <option value="">--Select--</option>
                <option value="1" selected="selected" <?php echo ($selanswer=="1")?"selected='selected'":""?>>Active</option>
                <option value="0" <?php echo ($selanswer=="0")?"selected='selected'":""?>>Inactive</option>
                </select>	
            </div>
          </div>
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="button" onclick="document.location.href='<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>'">Cancel</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>
     <div class="pvw_loader">Loading...</div>