    <script type="text/javascript">
	$(document).ready(function(){
		$(".showpassword").click(function(){
			$attr = $("#pass_word").attr("type");
			if($attr=="password"){
				$("#pass_word").attr("type","text");
			}else{
				$("#pass_word").attr("type","password");
			}
		})
	});
	</script>
    <div class="container top">
      
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url().$this->uri->segment(1); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <?php echo $action?>
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          <?php echo $actioning?> <?php echo ucfirst($this->uri->segment(1));?>
        </h2>
      </div>
 
      <?php
      //flash messages
	  
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == "done")
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_SUCCESS_MSG;
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_ERROR_MSG;
          echo '</div>';          
        }
      }
	  if($this->session->flashdata('errors'))
	  {
		   echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			echo "<b>".$this->session->flashdata('errors')."</b>";
			 echo '</div>'; 
	  }
      ?>
      
      <?php
	  
	  $editmode = isset($exercises)?1:0;
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');
      
      //form validation
      echo validation_errors();
      if($action=="Add")
	  {
      	echo form_open_multipart('exercise/add', $attributes);
	  }else{
		echo form_open_multipart('exercise/update/'.$this->uri->segment(3).'', $attributes);
	  }
      ?>
      <div class="help-inline" style="margin-bottom:15px;">To be added/edited by developers only. Please be careful playing with this section, the wrong entries might make exercise modules cease to work.</div>
        <fieldset>
          <div class="control-group">
            <label for="inputError" class="control-label">Main Exercise</label>
            <div class="controls">
              <input type="text" id="main_exercise" name="main_exercise" value="<?php echo ($editmode==1)?$exercises["main_exercise"]:set_value("main_exercise")?>">
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Sub Exercise</label>
            <div class="controls">
              <input type="text" id="sub_exercise" name="sub_exercise" value="<?php echo ($editmode==1)?$exercises["sub_exercise"]:set_value("sub_exercise")?>">
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Identifier</label>
            <div class="controls">
              <input type="text" id="exercise_identifier" name="exercise_identifier" value="<?php echo ($editmode==1)?$exercises["exercise_identifier"]:set_value("exercise_identifier")?>">
              <br /><span class="help-inline">A little text to identify the exercise. This will come in a main menu. 3 words max</span>
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Help Text:</label>
            <div class="controls">
              <textarea rows="3"  id="description" name="description"><?php echo ($editmode==1)?$exercises["description"]:set_value("description")?></textarea>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Help Text Identifier</label>
            <div class="controls">
              <input type="text" id="help_text_identifier" name="help_text_identifier" value="<?php echo ($editmode==1)?$exercises["help_text_identifier"]:set_value("help_text_identifier")?>">
              <br /><span class="help-inline">This will help to fetch appropriate help text and audios.</span>
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Url Segment</label>
            <div class="controls">
              <input type="text" id="url_link" name="url_link" value="<?php echo ($editmode==1)?$exercises["url_link"]:set_value("url_link")?>" >
             <br /><span class="help-inline">Note: Page should already exist. Be careful with this field</span>
            </div>
          </div>
          
          <div class="control-group">
            <label for="inputError" class="control-label">Exercise Screenshot</label>
            <div class="controls">
              <input type="file" name="exercise_screenshot" id="exercise_screenshot" />
               <input type="hidden" name="hid_exercise_screenshot" id="hid_exercise_screenshot" value="<?php echo ($editmode==1)?$exercises["exercise_screenshot"]:"";?>" />
             <br /><span class="help-inline">This will help to let know about exercise at a glance.</span>
             <?php
             if($editmode==1 && trim($exercises["exercise_screenshot"])!=""){
			?>
            <div>
            <img src="<?php echo EXTRA_IMAGE_URL.$exercises["exercise_screenshot"]?>" width="250" /> 
            </div>
            <?php
			 }
			 ?>
            </div>
          </div>
          
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="button" onclick="document.location.href='<?php echo base_url()?>exercise'">Cancel</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>
     