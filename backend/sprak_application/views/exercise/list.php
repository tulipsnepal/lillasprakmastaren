    <div class="container top">
      
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <?php echo "Exercises";?>
        </li>
      </ul>
      
      
 	<div class="page-header users-header">
        <h2>
          <?php echo "Exercises";?> 
          <a  href="<?php echo site_url().$this->uri->segment(1); ?>/add" class="btn btn-success">Add a new</a>
        </h2>
      </div>
    
    <div class="row">
    <?php 
	if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == "done")
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_SUCCESS_MSG;
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_ERROR_MSG;
          echo '</div>';          
        }
      }	
	?>
        <div class="span12 columns">
          <div class="help-inline">To be added/edited by developers only. Please be careful playing with this section, the wrong entries might make exercise modules cease to work.</div>
          <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
                <th class="header" width="5%">#</th>
                <th class="yellow header headerSortDown" width="12%">Main Excercises</th>
                <th class="red header" width="12%">Excercises</th>
                <th class="red header" width="51%">Description</th>
                <th class="red header" width="20%">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
			  $i = 1;
			  
              foreach($exercises as $row)
              {
                echo '<tr>';
                echo '<td>'.$i.'</td>';
				echo '<td>'.$row["main_exercise"].'</td>';
                echo '<td>'.$row['sub_exercise'].'</td>';
				echo '<td style="font-size:11px">'.$row['description'].'</td>';
				echo '<td class="crud-actions">
                  <a href="'.site_url().'exercise/update/'.$row['exercise_id'].'" class="btn btn-info">view & edit</a>
				  <a href="'.site_url().strtolower($row["main_exercise"]).'/'.$row['url_link'].'" class="btn btn-warning">Edit Sets</a> 
                </td>';
                echo '</tr>';
				$i++;
              }
              ?>      
            </tbody>
          </table>

      </div>
    </div>
     