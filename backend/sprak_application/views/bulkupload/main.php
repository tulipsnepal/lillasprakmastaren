     <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css">
     <script src="<?php echo base_url(); ?>assets/js/ajaxupload-min.js"></script>
    <div class="container top">

      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <?php echo "Bulk Upload"?>
        </li>
      </ul>

      <div class="page-header users-header">
        <h2>
          <?php echo "Bulk Upload";?> 
        </h2>
      </div>
      <?php
       //flash messages
	  
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == "done")
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_SUCCESS_MSG;
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_ERROR_MSG;
          echo '</div>';          
        }
      }
	  if($this->session->flashdata('errors'))
	  {
		   echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			echo "<b>".$this->session->flashdata('errors')."</b>";
			 echo '</div>'; 
	  }
      ?>
      <div class="row">
        <div class="span12 columns">
          <div class="well">
          		<div class="">
                 <div class="upl">Data Type:</div> <span class="help-inline">Whether you are uploading exercise related data or a person related data.</span>
                 <select name="set_image_type" id="set_image_type">
                    <option value="exercise_data">Exercise Data (Noun)</option>
                    <option value="exercise_data_verbs">Exercise Data (Verbs)</option>
                    <option value="character_names">Character Names</option>
                  </select><br />
                </div>
          		<div class="imageuploadholder">
                     <div class="upl">Upload New Images</div><span class="help-inline">Supported image png,jpg and gif. Existing file will be replaced. You can either select the file below or drop files from your computer. </span>
                     <div style="margin:10px 0 10px 0">
                    
                 <span>Tags:</span> 
                <input type="text" name="tags" id="tags" style="width:292px;" /><span class="help-inline">Separate multiple tags with comma.</span>
                  </div>
                    <div id="bulkimageuploader"></div>
                    
                </div>
                
                <div class="audiouploadholder">
                  <div class="upl">Upload Audios</div><span class="help-inline"><b>Supported audio mp3</b>. Existing file will be replaced. You can either select the file below or drop files from your computer.</span>
                  <?php /*?><div style="margin:10px 0 10px 0">
                 <span>Audio Type:</span> 
                 <select name="audio_type" id="audio_type">
                    <option value="" selected="selected">-- Select Audio Type --</option>
                  	<option value="noun">Noun</option>
                    <option value="noun_and_verbs">Noun & Verbs</option>
                    <option value="verbs">Verbs</option>
                  </select>
                   <select name="singular_plural_audio" id="singular_plural_audio">
                  <option value="">-- Singular or Plural --</option>
                  <option value="singular">Singular</option>
                  <option value="plural">Plural</option>
                  </select>
                  </div><?php */?>
                  <div id="bulkaudiouploader"></div>
                </div>
                <div class="upl" style="margin-top:30px;">Upload Words Without Images</div>
                <div>
                	<form id="frmWordNoImage" method="post" action="<?php echo site_url()."bulkupload/upload_no_image_words"?>">
                	<div class="help-inline">You can upload multiple words each separated by ##</div>
                	<div>
                		<textarea id="bulk_no_image_words" name="bulk_no_image_words" cols="6" rows="8"></textarea>
                	</div>
                	<div>
                	<input type="submit" value="Upload Words" class="btn btn-success" />
                	</div>
                	</form>
                </div>
          </div>
      </div>
    </div>
    <div class="pvw_loader">Loading...</div>
    <script type="text/javascript">
			
	$("#set_image_type").change(function(){
		if($(this).val()=="character_names"){
			$("#tags").attr("disabled","disabled");
		}else{
			$("#tags").removeAttr("disabled");
		}
	});
			$('#bulkimageuploader').ajaxupload({
					url:'<?php echo base_url()?>bulkupload/startupload/image',
					remotePath: "<?php echo GLOBAL_IMG_DIR?>", 
					data: function(){
						var tg = $("#tags").val();
						var set_img_type = $("#set_image_type").val();
						return {tags:tg,set_image_type:set_img_type};
					},
					allowExt:['png','jpg','gif'],
					error:function(err,fileName){
						alert(err+' - '+ fileName) //this will return the error that the file is not accepted because it has the not allowed extesion extension.
					},
					dropColor:"#ffcc00",
					removeOnSuccess: true,
					maxFileSize:'5M'
			});
			
			
			$('#bulkaudiouploader').ajaxupload({
					url:'<?php echo base_url()?>bulkupload/startupload/audio/',
					remotePath: "<?php echo GLOBAL_AUDIO_DIR?>",  
					data: function(){
						var set_img_type = $("#set_image_type").val();
						return {set_image_type:set_img_type};
					},
					allowExt:['mp3'],
					error:function(err,fileName){
						alert(err+' - '+ fileName) //this will return the error that the file is not accepted because it has the not allowed extesion extension.
					},
					removeOnSuccess: true,
					maxFileSize:'5M'
			});
		<?php /*?>$('#bulkimageuploader').ajaxupload({
				url:'<?php echo base_url()?>bulkupload/startupload/image',
				remotePath:function(){
						var base = "<?php echo GLOBAL_IMG_DIR?>";//get part of the path from some input
						var path = $('#image_type').val()+"/";
						return base+path; //MUST RETURN A STRING AS PATH
				},  
				data:function(){
					var catvalue = $("#image_type").val();
					var sing_pl = $("#singular_plural_image").val();
					return {image_type:catvalue,singular_plural:sing_pl};
				},
				beforeUploadAll:function(files){
					var err = "";
					if($("#image_type").val()==""){
						err += "Select the image type. \n";
					}
					
					if($("#image_type").val()=="noun")
					{
						if($("#singular_plural_image").val()==""){
							err += "Select whether the word is singular or plural. \n";
						}
					}
					
					if(err!=""){
						alert("Please fix the following error(s) \n"+err);
						return false;
					}else{
						return true;
					}
					
					
				},
				finish:	function(file_names, file_obj){
					$("#image_type").val("");
					$("#singular_plural_image").val("");
				},
				allowExt:['jpg', 'gif', 'png'],
				dropColor:"#ffcc00",
				removeOnSuccess: true,
				maxFileSize:'5M'
		});
			
			
			$('#bulkaudiouploader').ajaxupload({
					url:'<?php echo base_url()?>bulkupload/startupload/audio/',
					remotePath:function(){
						var base = "<?php echo GLOBAL_AUDIO_DIR?>";//get part of the path from some input
						var path = $('#audio_type').val()+"/";
						return base+path; //MUST RETURN A STRING AS PATH
					},  
					data:function(){
						var catvalue = $("#audio_type").val();
						var sing_pl = $("#singular_plural_audio").val();
						return {audio_type:catvalue,singular_plural:sing_pl};
					},
					beforeUploadAll:function(files){
						var err = "";
						if($("#audio_type").val()==""){
							err += "Select the image type. \n";
						}
						
						if($("#audio_type").val()=="noun")
						{
							if($("#singular_plural_audio").val()==""){
								err += "Select whether the word is singular or plural. \n";
							}
						}
						
						if(err!=""){
							alert("Please fix the following error(s) \n"+err);
							return false;
						}else{
							return true;
						}
					},
					finish:	function(file_names, file_obj){
						$("#audio_type").val("");
						$("#singular_plural_audio").val("");
					},
					allowExt:['mp3'],
					removeOnSuccess: true,
					maxFileSize:'5M'
			});<?php */?>
		
	</script>