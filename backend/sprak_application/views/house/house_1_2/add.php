    <script type="text/javascript">
	function ajaxChangeToLowerCase(val){

      $.ajax({
          type: "POST",
          data: "uc_alphabets="+val,
          url: "<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>/ajaxtolower",
          beforeSend: function(){
            $("#alphabet_lowercase").val("Converting...");
          },
          success: function(data){
            $("#alphabet_lowercase").val($.trim(data));
          }
      });
  }
	
	function submitForm(){
		
		

		if($.trim($("#alphabet_lowercase").val())){
			errors +="You must specify all three  sentences.\n";
		}

		if($(".correct_sentence:checked").length==0){
			errors +="You must select one correct sentence.\n";	
		}

		
		
		
		
		
		if(errors!=""){
			alert("Please fix the following error(s)\n\n"+errors);
			return false;
		}
		//alert($unrhyming);
		return true;
	}
	</script>
    <div class="container top">
      
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url()."exercise"; ?>">
            <?php echo "Exercises";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
        <a href="<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>">
          <?php echo ucwords($this->uri->segment(1))?>
          </a>
           <span class="divider">/</span>
        </li>
        <li class="active">
        <?php echo "House 1-2"?> 
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          <?php echo "House 1-2 - ".$exercise_desc[0]?> 
        </h2>
      </div>
 
      <?php
      //flash messages
	  
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == "done")
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_SUCCESS_MSG;
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_ERROR_MSG;
          echo '</div>';          
        }
      }
	  if($this->session->flashdata('errors'))
	  {
		   echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			echo "<b>".$this->session->flashdata('errors')."</b>";
			 echo '</div>'; 
	  }
      ?>
      
      <?php
	  
	
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '','onsubmit'=>'return submitForm()');
      
      //form validation
      echo validation_errors();
	 
      if($action=="Add")
	  {
      	echo form_open('house/house_1_2/add', $attributes);
	  }else{
		echo form_open('house/house_1_2/update/'.$this->uri->segment(4).'', $attributes);
	  }
	 	
      ?>
      
        <fieldset>
         <input type="hidden" id="exercise_id" name="exercise_id" value="<?php echo $exercise_desc[2]?>" />
       	<div class="control-group">
            <label for="inputError" class="control-label">Uppercase Alphabets</label>
            <div class="controls">
            	<div class="help-inline">Specify alphabets in capital letters, each separated by comma</div>
              <div>
            	<input type="text" name="alphabet_uppercase" id="alphabet_uppercase" value="<?php echo isset($set["alphabet_uppercase"])?$set["alphabet_uppercase"]:""?>" onblur="ajaxChangeToLowerCase(this.value)" />
              </div>
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Lowercase Alphabets</label>
            <div class="controls">
              <div class="help-inline">Will be automatically converted to lowercase, but please re-check if they're correct.</div>
              <div>
              <input type="text" name="alphabet_lowercase" id="alphabet_lowercase" value="<?php echo isset($set["alphabet_lowercase"])?$set["alphabet_lowercase"]:""?>" />
              </div>
            </div>
          </div>
          <div class="control-group">
            <label for="is_active" class="control-label">Set Status</label>
            <div class="controls">
            	<?php
				$selanswer = "";
              	if(isset($set))
				{
					$selanswer = isset($set["is_active"])?$set["is_active"]:"";
				}
				?>
                <select id="is_active" name="is_active">
                <option value="">--Select--</option>
                <option value="1" selected="selected" <?php echo ($selanswer=="1")?"selected='selected'":""?>>Active</option>
                <option value="0" <?php echo ($selanswer=="0")?"selected='selected'":""?>>Inactive</option>
                </select>	
            </div>
          </div>
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="button" onclick="document.location.href='<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>'">Cancel</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>
 