    <script type="text/javascript">
	//REMOVE SELECTED ITEM
		$(document.body).on("click",".delete-absolute",function(){
			$type = $(this).data("type");
			$id = $(this).data("id");
			$("#selected_mainwords option[value='"+$id+"']").remove();
			$(".mainwords[value='"+$id+"']").prop("checked",false);
			$(this).parent(".relative").remove();
		});
		
	//SET GLOBAL AJAX URL TO BE USED FOR AJAX CALLS
	GlobalAjaxUrl = '<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>';
	$(document).ready(function(){
		
		//CLICK EVENT TO THE CHECKBOX/RADIO-BUTTON. UPON CLICKING DISPLAY SELECTED IMAGE AND SET THE SELECTED WORD VALUE TO HIDDEN FIELD OR THE HIDDEN COMBOBOX
		$(document.body).on("click",".mainwords",function(){
			$current_check_val = $(this).val();
			$current_check_text = $.trim($(this).parent("label").text());
			var checkedBoxlength=$(".mainwords:checked").length;
			
			
			//POPULATE IMAGE OF SELECTED WORDS
			if($(this).prop("checked")){
				$chkval = $(this).val();
				$chktext = $(this).parent("label").text();
				var exists = $("#selected_mainwords option[value='"+$chkval+"']").length !== 0;
				var optlength = $("#selected_mainwords").children("option").length;
				
				// IF HIDDEN COMBO DOESN'T CONTAINS SELETED WORD AND IT HAS LESS THAN 4 OPTIONS THEN APPEND OPTIONS OF THE SELECTED WORD.
				if(!exists)
				{
					$("#selected_mainwords").append("<option value='"+$chkval+"'>"+$chktext+"</option>");
				}else{
					return false;
				}
				
				//SHOW IMAGE OF THE SELECTED WORD WITH THE URL FETCHED FROM THE DATA ATTRIBUTE "imgsrc" OF THE SELECTED CHECKBOX
				if($(this).data("imgsrc")!="")
				{
					$(".selected-heading").show();
					$imgsrc = "<?php echo GLOBAL_IMG_URL?>"+$(this).data("imgsrc")+".png";
					$(".tivoli-main-words-img").append("<div class='tivoli-words-image relative tivoli31 main"+$current_check_val+"'><img src='"+$imgsrc+"' title='"+$current_check_text+"' style='height:120px' /><div style='text-align:center'>"+$current_check_text+"</div><div class='delete-absolute' data-id='"+$current_check_val+"' data-type='main'>x</div></div>").show();
				}else{
					alert("No image linked with this word.");
					$(this).prop("checked",false);
				}
			}else{
				//REMOVE IMAGE AND OPTIONS IF THE CHECKBOX IS UNCHECKED
				 $("#selected_mainwords option[value='"+$(this).val()+"']").remove();
				$(".tivoli-main-words-img").find(".main"+$current_check_val).remove();
				if($(".tivoli-main-words-img").find(".tivoli-words-image").length==0)
				{
					$(".tivoli-main-words-img").hide();
				}
			}
			
		});
		
		//AMONG 4 WORDS 1 IS UNRHYMING, SELECT THE UNRHYMING RADIO IF ITS VALUE EQUALS THE HIDDEN FIELD'S VALUE
		$(document.body).on("click",".tivoli-words-image",function(){
			$chkbox = $(this).find(".unrhymingword");
			$chkbox.prop("checked",true);
		});
		
		loadWords();
		
		//WHEN SORT ALPHABETS ARE CLICKED
		$(".loadcharwords").on("click",function(){
			$(".loadcharwords").parent("li").removeClass("active");
			$(this).parent("li").addClass("active");
			loadWords("alpha",$(this).text());
		});
		
		//WHEN SEARCH IS CLICKED
		$(".loadcharwords_search").on("click",function(){
			$keyword = $(this).prev(".searchstring").val();
			loadWords("search",$keyword);
		});
		
		
		//WHEN FILTER BY TAGS IS SELECTED
		$(".loadcharwords_filter").on("change",function(){
			$keyword = $(this).val();
			loadWords("tags",$keyword);
		});
	});
	
		
	
	function submitRhyme(){
		/*$mainlength = $("#selected_mainwords").children("option").length;
		if($mainlength==0)
		{
			alert("Please select words before proceeding");
			return false;
		}*/
		$("#selected_mainwords option").prop("selected",true);
		
		//alert($unrhyming);
		return true;
	}
	</script>
    <div class="container top">
      
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url()."exercise"; ?>">
            <?php echo "Exercises";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
        <a href="<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>">
          <?php echo ucwords($this->uri->segment(1))?>
          </a>
           <span class="divider">/</span>
        </li>
        <li class="active">
        <?php echo "House 2-3"?>
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          <?php echo "House 2-3 - ".$exercise_desc[0]?> 
        </h2>
      </div>
 
      <?php
      //flash messages
	  
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == "done")
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_SUCCESS_MSG;
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_ERROR_MSG;
          echo '</div>';          
        }
      }
	  if($this->session->flashdata('errors'))
	  {
		   echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			echo "<b>".$this->session->flashdata('errors')."</b>";
			 echo '</div>'; 
	  }
      ?>
      
      <?php
	  
	  $editmode = isset($exercises)?1:0;
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '','onsubmit'=>'return submitRhyme()');
      
      //form validation
      echo validation_errors();
	 
     echo form_open('house/house_2_3/add', $attributes);
	  
	  
	   $selectedwords = "";
	   $imagesforselectedwords = "";
	   if(isset($set) && $set!="")
		{
			$swords = $set;
			
			foreach($swords as $value)
			{
				$selectedwords .= "<option value='".$value["word"]["word_id"]."'>".$value["word"]["word"]."</option>";
				
				$imagesforselectedwords .="<div class='tivoli-words-image relative tivoli31 main".$value["word"]["word_id"]."'><img src='".GLOBAL_IMG_URL.$value["word"]["imageRef"]."' title='".$value["word"]["word"]."' style='height:120px' />
				<div style='text-align:center'>".$value["word"]["word"]."</div>
				<div class='delete-absolute' data-id='".$value["word"]["word_id"]."' data-type='main'>x</div>
				</div>";
			}
		}
      ?>
      
        <fieldset>
         <input type="hidden" id="exercise_id" name="exercise_id" value="<?php echo $exercise_desc[2]?>" />
        <select name="selected_mainwords[]" id="selected_mainwords" style="display:none;" multiple="multiple">
        <?php echo $selectedwords?>
        </select>
        
         <div class="control-group">
            <label for="inputError" class="control-label">Select Words</label>
            <div class="controls">
            <?php
            if(isset($set) && $set!="")
			{
			?>
            <div class="help-inline" style="padding:0 0 10px 0px">You can change the selected words by using the list below</div>
            <?php
			}
			?>
                <div class="tivoli-1-full" style="margin-left:12px;">
                	<div class="upl tivoli-1-sub-head">Select multiple words (Answer will be evaluated and detected automatically while creating sets)</div>
                    <?php echo $gen_alphabets?>
                    <div  style="max-height:500px;overflow:auto">
                    	<ul class="list_words main-col"></ul>
                    </div>
                </div>		
            </div>
          </div>
          <div class="control-group">
          
          <div class="controls" style="padding-left:20px;">
          	<div class="selected-heading" <?php echo ($imagesforselectedwords!="")?"style='display:block;'":""?>><b>Selected words</b></div>
          	<div class="tivoli-main-words-img" <?php echo ($imagesforselectedwords!="")?"style='display:block;'":""?>><?php echo $imagesforselectedwords?></div>
          </div>
          </div>
          
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="button" onclick="document.location.href='<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>'">Cancel</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>
     <div class="pvw_loader">Loading...</div> 