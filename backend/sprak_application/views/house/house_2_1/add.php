    <script type="text/javascript">
    $(document).ready(function(){
        $(".exercise_sentence").on("blur",function(){
            var val = $(this).val();
            var target = $(this).next(".answer_word");
            if($.trim(val)!="")
            {
              $.ajax({
                    type: "POST",
                    data: "exercise_sentence="+val,
                    url: "<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>/ajax_extract_word",
                    beforeSend: function(){
                      target.val("Extracting Answer...");
                    },
                    success: function(data){
                      target.val($.trim(data));
                    }
              });
            }
        });
    });
	
	
	function submitForm(){
		
		var errSentence = 0, errorAnswer = 0, errors = "";

    $(".exercise_sentence").each(function(){
        if($.trim($(this).val())=="")
        {
          errSentence++;
        }
    });

    $(".answer_word").each(function(){
        if($.trim($(this).val())=="")
        {
          errorAnswer++;
        }
    });
  
		if(errSentence>0){
			errors +="You must specify all 10 sentences.\n";
		}

		if(errorAnswer>0){
			errors +="You must specify all 10 answers.\n";	
		}

		
		if(errors!=""){
			alert("Please fix the following error(s)\n\n"+errors);
			return false;
		}
		//alert($unrhyming);
		return true;
	}
	</script>
    <div class="container top">
      
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url(); ?>">
            <?php echo "Admin";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url()."exercise"; ?>">
            <?php echo "Exercises";?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
        <a href="<?php echo site_url().$this->uri->segment(1)."/".$this->uri->segment(2); ?>">
          <?php echo ucwords($this->uri->segment(1))?>
          </a>
           <span class="divider">/</span>
        </li>
        <li class="active">
        <?php echo "House 2-1"?> 
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          <?php echo "House 2-1 - ".$exercise_desc[0]?> 
        </h2>
      </div>
 
      <?php
      //flash messages
	  
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == "done")
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_SUCCESS_MSG;
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo CONST_ERROR_MSG;
          echo '</div>';          
        }
      }
	  if($this->session->flashdata('errors'))
	  {
		   echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
			echo "<b>".$this->session->flashdata('errors')."</b>";
			 echo '</div>'; 
	  }
      ?>
      
      <?php
	  
	
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '','onsubmit'=>'return submitForm()');
      
      //form validation
      echo validation_errors();
	 
      if($action=="Add")
	  {
      	echo form_open('house/house_2_1/add', $attributes);
	  }else{
		echo form_open('house/house_2_1/update/'.$this->uri->segment(4).'', $attributes);
	  }
	 	
      ?>
      
        <fieldset>
         <input type="hidden" id="exercise_id" name="exercise_id" value="<?php echo $exercise_desc[2]?>" />
         <div class="help-inline" style="padding-left:192px;padding-bottom:10px;">Please specify the exercise sentences. The answer will be detected automatically, but please do check if they're correct</div>
         <?php
          for($k = 0; $k<10; $k++)
          {
         ?>
       	<div class="control-group">
            <label for="inputError" class="control-label">Exercise Sentence <?php echo $k+1?></label>
            <div class="controls">
            	<input class="exercise_sentence" type="text" name="exercise_sentence[]" value="<?php echo isset($set["exercise_sentence"][$k])?$set["exercise_sentence"][$k]:""?>"  />
              <input class="answer_word" type="text" name="answer_word[]" style="width:150px;" value="<?php echo isset($set["answer_word"][$k])?$set["answer_word"][$k]:""?>">
            </div>
          </div>
        <?php
        }
        ?> 
          <div class="control-group">
            <label for="is_active" class="control-label">Set Status</label>
            <div class="controls">
            	<?php
				$selanswer = "";
              	if(isset($set))
				{
					$selanswer = isset($set["is_active"])?$set["is_active"]:"";
				}
				?>
                <select id="is_active" name="is_active">
                <option value="">--Select--</option>
                <option value="1" selected="selected" <?php echo ($selanswer=="1")?"selected='selected'":""?>>Active</option>
                <option value="0" <?php echo ($selanswer=="0")?"selected='selected'":""?>>Inactive</option>
                </select>	
            </div>
          </div>
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="button" onclick="document.location.href='<?php echo base_url().$this->uri->segment(1)."/".$this->uri->segment(2)?>'">Cancel</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>
 