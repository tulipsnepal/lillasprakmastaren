<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('utf8_replace'))
{
   /*
	method to replace utf8 characters in filename with normal character
	*/
	function utf8_replace($string)
	{
		$arrayUTF = array("å","ä","ö","Å","Ä","Ö");
		$arrayToReplace = array("_ao_","_ae_","_oe_","_AO_","_AE_","_OE_");
		return str_replace($arrayUTF,$arrayToReplace,$string);
	} 
}

if ( ! function_exists('to_utf8'))
{
   /*
	method to replace utf8 characters in filename with normal character
	*/
	function to_utf8($string)
	{
		$arrayUTF = array("å","ä","ö","Å","Ä","Ö");
		$arrayToReplace = array("_ao_","_ae_","_oe_","_AO_","_AE_","_OE_");
		return str_replace($arrayToReplace,$arrayUTF,$string);
	} 
}

if ( ! function_exists('mb_str_split'))
{
  	/*
  	Method to split word into array of characters. equivalent to str_split, but this is for unicode characters
  	*/
	function mb_str_split($string)
	{ 
    	return preg_split('/(?<!^)(?!$)/u', $string ); 
 	} 
}

