<?php
class Model_toystore_2_1 extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

   	public function get_set_by_id($id)
    {
		$this->db->select('set_json_data,is_active');
		$this->db->from('lsm_game_json_data');
		$this->db->where('set_id', $id);
		$query = $this->db->get();
		$t =  $query->result_array();
		$json_data = json_decode($t[0]["set_json_data"],true);

		$set = array();
		$set["alphabet_group"] = key($json_data);
		$a=0;
		foreach($json_data[$set["alphabet_group"]]["words"] as $words)
		{
			$word= $this->utilities->word_details_by_id($words);
			$set[$set["alphabet_group"]][$a]=$word;
			$a++;
		}
		
		$set["is_active"] = $t[0]["is_active"];
		//$this->utilities->printr($set,1);
		return $set;
    }
	
	public function get_items($exerciseid)
    {
	    $this->db->select("*");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->order_by('set_id', "DESC");
		$query = $this->db->get();
		
		
		$a = 0;
		$array = array();
		foreach($query->result_array() as $results)
		{
			$set_data = json_decode($results["set_json_data"],true);
			
			$group = key($set_data);
			$array[$a]["group"]  = $group;
			foreach($set_data[$group]["words"] as $words)
			{
				$array[$a][$group]["word"][]= $this->utilities->word_details_by_id($words);	
			}
			
			
			
			$array[$a]["is_active"] = $results["is_active"];
			$array[$a]["set_id"] = $results["set_id"];
			$a++;			
		}
		//$this->utilities->printr($array,1);
		return $array;
    }

    
	function store_set($data)
    {
    	//first check if the data for same group already exist if yes then don't let add new one
    	$countqry = $this->db->query("SELECT * FROM lsm_game_json_data WHERE extra_info='".$data["alphabet_group"]."'");
    	

    	if($countqry->num_rows()>0)
    	{
			return "duplicate";
    		exit;
    	}
		
		$array = array();
		$selectedwords = $data["selected_word"];
		shuffle($selectedwords);

		foreach($selectedwords as $words)
		{
			$array[$data["alphabet_group"]]["words"][] = $words;	
		}
		$jsontosave = $this->utilities->json($array); 
		
		$datatostore = array("exercise_id"=>$data["exercise_id"],"set_json_data"=>$jsontosave,"is_active"=>$data["is_active"],"extra_info"=>$data["alphabet_group"]);
		$insert = $this->db->insert('lsm_game_json_data', $datatostore);
	    return $insert;
		
	}

   
    function update_set($id, $data)
    {
		$array = array();
		$selectedwords = $data["selected_word"];
		shuffle($selectedwords);

		foreach($selectedwords as $words)
		{
			$array[$data["alphabet_group"]]["words"][] = $words;	
		}
		$jsontosave = $this->utilities->json($array); 
		
		$datatostore = array("set_json_data"=>$jsontosave,"is_active"=>$data["is_active"]);
		$this->db->where('set_id', $id);
		$update = $this->db->update('lsm_game_json_data', $datatostore);
		return $update;
	}

   	/*
	Method to create JSON 
	*/
 	function create_json($exerciseid){
		$file_to_create = UPLOADS_DIR."exercises/toystore/toystore_2_1.json";
		$this->db->select("set_json_data");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->where("is_active",1);
		$query = $this->db->get();
		$t =  $query->result_array();
		
		if($t)
		{
			foreach($t as $o){
				$r  = json_decode($o["set_json_data"],true);
				$group = key($r);
				$a = 0;
				
				shuffle($r[$group]["words"]);
				
				foreach($r[$group]["words"] as $wordid)
				{
					$word = $this->utilities->word_details_by_id($wordid);
					$array[$group][$a]["word"] = $word["word"];
					$array[$group][$a]["image"] = $word["imageRef"];
					$array[$group][$a]["sound"] = $word["audioRef"];
					$array[$group][$a]["answer"] = mb_strtoupper($word["word"][0]);
					$a++;
				}
				
				
				
			}
		
			$content_type = 'application/json; charset=utf-8';
			header('Content-type: '.$content_type); 
			
			$fp = fopen($file_to_create, 'w');
			$write = fwrite($fp, $this->utilities->json($array)); 
			fclose($fp);
			return $write;
		}else{
			@unlink($file_to_create);
		}
	}
	
	//delete selected sets
	function deleteselected($postvars){
		//$this->utilities->printr($postvars,1);
		$report = array();
		foreach($postvars["sets"] as $setid)
		{
			$this->db->where('set_id', $setid);
			$this->db->delete('lsm_game_json_data');
			array_push($report,$this->db->_error_number());
		}
		
		if(!empty($report)){
			return true;
		}else{
			return false;
		}
	}
}
?>	
