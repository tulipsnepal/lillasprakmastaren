<?php
class Model_Toystore_1_1 extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

   	public function get_set_by_id($id)
    {
		
		$this->db->select('set_json_data,is_active');
		$this->db->from('lsm_game_json_data');
		$this->db->where('set_id', $id);
		$query = $this->db->get();
		$t =  $query->result_array();
		$json_data = json_decode($t[0]["set_json_data"],true);
		
		$set = array();
		
		$set["alphabets"] = $json_data["alphabets"];
		$set["answer"] = $json_data["answer"]["alphabet"];
		$set["audio"] = $json_data["answer"]["audio"];
		$set["is_active"] = $t[0]["is_active"];
		return $set;
    }
	
	public function get_items($exerciseid)
    {
	    $this->db->select("*");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->order_by('set_id', "DESC");
		$query = $this->db->get();
		
		
		$a = 0;
		$array = array();
		foreach($query->result_array() as $results)
		{
			$set_data = json_decode($results["set_json_data"],true);
			
			$array[$a]["word"] = $set_data["alphabets"];
			$array[$a]["answer"] = $set_data["answer"]["alphabet"];
			$array[$a]["audio"] = $set_data["answer"]["audio"];
			$array[$a]["is_active"] = $results["is_active"];
			$array[$a]["set_id"] = $results["set_id"];
			$a++;			
		}
		return $array;
    }

    
	function store_set($data)
    {
    	$array = array();
		
		$array["alphabets"] = $data["alphabets"];
		$array["answer"]["alphabet"] = $data["answer"];
		$array["answer"]["audio"] = $data["rdo_alphabet_audio"];
		//$this->utilities->printr($data,1);
		$jsontosave = $this->utilities->json($array); 
		
		$datatostore = array("exercise_id"=>$data["exercise_id"],"set_json_data"=>$jsontosave,"is_active"=>$data["is_active"]);
		$insert = $this->db->insert('lsm_game_json_data', $datatostore);
	    return $insert;
	}

   
    function update_set($id, $data)
    {
		$array = array();
		$array["alphabets"] = $data["alphabets"];
		$array["answer"]["alphabet"] = $data["answer"];
		$array["answer"]["audio"] = $data["rdo_alphabet_audio"];
		$jsontosave = $this->utilities->json($array); 
		
		$datatostore = array("set_json_data"=>$jsontosave,"is_active"=>$data["is_active"]);
		$this->db->where('set_id', $id);
		$update = $this->db->update('lsm_game_json_data', $datatostore);
		return $update;
	}

   	/*
	Method to create JSON 
	*/
 	function create_json($exerciseid){
		$file_to_create = UPLOADS_DIR."exercises/toystore/toystore_1_1.json";
		$this->db->select("set_json_data");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->where("is_active",1);
		$query = $this->db->get();
		$t =  $query->result_array();
		$a = 0;
		if($t){
			foreach($t as $o){
				$r = json_decode($o["set_json_data"],true);
				$array[$a]["alphabets"] = $r["alphabets"];
				$al = $r["alphabets"];
				
				array_walk($al, function(&$value, &$key) {
    				$value = utf8_replace($value);
				});

				$array[$a]["utf8_safe_alphabets"] = $al;
				foreach($r["alphabets"] as $alphabets){
					$array[$a]["alphabets_to_show"][] = mb_strtoupper($alphabets).mb_strtolower($alphabets);	
				}
				
				$array[$a]["answer"]["alphabet"] = $r["answer"]["alphabet"];
				$array[$a]["answer"]["audio"] = utf8_replace($r["answer"]["audio"]).".mp3";
				
				$a++;
			}
			
			$content_type = 'application/json; charset=utf-8';
			header('Content-type: '.$content_type); 
			
			$fp = fopen($file_to_create, 'w');
			$write = fwrite($fp,$this->utilities->json($array)); 
			fclose($fp);
			return $write;
		}else{
			@unlink($file_to_create);
		}
	}
	
	//delete selected sets
	function deleteselected($postvars){
		$report = array();
		
		foreach($postvars["sets"] as $setid)
		{
			$this->db->where('set_id', $setid);
			$this->db->delete('lsm_game_json_data');
			array_push($report,$this->db->_error_number());
		}
		
		if(!empty($report)){
			return true;
		}else{
			return false;
		}
	}
	
}
?>	
