<?php
class Model_toystore_2_2 extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

   	public function get_set_by_id($id)
    {
		
		$this->db->select('set_json_data,is_active');
		$this->db->from('lsm_game_json_data');
		$this->db->where('set_id', $id);
		$query = $this->db->get();
		$t =  $query->result_array();
		$json_data = json_decode($t[0]["set_json_data"],true);
		
		$set = array();
		
		for($i=0;$i<count($json_data["options"]);$i++)
		{
			$set["words"][]= $this->utilities->word_details_by_id($json_data["options"][$i]);
		}
		$set["answer_word"] = $json_data["answer_word"];
		$set["is_active"] = $t[0]["is_active"];
		return $set;
    }
	
	public function get_items($exerciseid)
    {
	    $this->db->select("*");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->order_by('set_id', "DESC");
		$query = $this->db->get();
		
		
		$a = 0;
		$array = array();
		foreach($query->result_array() as $results)
		{
			$set_data = json_decode($results["set_json_data"],true);
			
			for($i=1;$i<=count($set_data["options"]);$i++)
			{
				$array[$a]["word_".$i]= $this->utilities->word_details_by_id($set_data["options"][$i-1]);
			}
			$array[$a]["answer_word"]= $this->utilities->word_details_by_id($set_data["answer_word"]);
			$array[$a]["is_active"] = $results["is_active"];
			$array[$a]["set_id"] = $results["set_id"];
			$a++;			
		}
		return $array;
    }

    
	function store_set($data)
    {
		$array = array();
		
		$array["options"] = $data["selected_mainwords"];
		$array["answer_word"] = $data["answer"];
		//$this->utilities->printr($data,1);
		$jsontosave = $this->utilities->json($array); 
		
		$datatostore = array("exercise_id"=>$data["exercise_id"],"set_json_data"=>$jsontosave,"is_active"=>$data["is_active"]);
		$insert = $this->db->insert('lsm_game_json_data', $datatostore);
	    return $insert;
	}

   
    function update_set($id, $data)
    {
		$array = array();
		$array["options"] = $data["selected_mainwords"];
		$array["answer_word"] = $data["answer"];
		$jsontosave = $this->utilities->json($array); 
		
		$datatostore = array("set_json_data"=>$jsontosave,"is_active"=>$data["is_active"]);
		$this->db->where('set_id', $id);
		$update = $this->db->update('lsm_game_json_data', $datatostore);
		return $update;
	}

   	/*
	Method to create JSON 
	*/
 	function create_json($exerciseid){
		$file_to_create = UPLOADS_DIR."exercises/toystore/toystore_2_2.json";
		$this->db->select("set_json_data");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->where("is_active",1);
		$query = $this->db->get();
		$t =  $query->result_array();
		$a = 0;
		if($t){
			foreach($t as $o){
				$r = json_decode($o["set_json_data"],true);
				
				for($i=1;$i<=count($r["options"]);$i++)
				{
					$w  = $this->utilities->word_details_by_id($r["options"][$i-1]);
					$array[$a]["word"][] = $w["word"];
					$array[$a]["image"][] = $w["imageRef"];
					$array[$a]["sound"][] = $w["audioRef"];
					$array[$a]["answer_options"][] = mb_strtoupper($w["word"][0]);
					
				}
				$ans = $w  = $this->utilities->word_details_by_id($r["answer_word"]);
				$array[$a]["answer"] = mb_strtoupper($ans["word"][0]);
				$a++;
			}
			
			$content_type = 'application/json; charset=utf-8';
			header('Content-type: '.$content_type); 
			
			$fp = fopen($file_to_create, 'w');
			$write = fwrite($fp,$this->utilities->json($array)); 
			fclose($fp);
			return $write;
		}else{
			@unlink($file_to_create);
		}
	}
	
	//delete selected sets
	function deleteselected($postvars){
		$report = array();
		
		foreach($postvars["sets"] as $setid)
		{
			$this->db->where('set_id', $setid);
			$this->db->delete('lsm_game_json_data');
			array_push($report,$this->db->_error_number());
		}
		
		if(!empty($report)){
			return true;
		}else{
			return false;
		}
	}
	
}
?>	
