<?php
class Settings_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

  	public function fetch_settings()
	{
		$this->db->select("*");
		$this->db->from('lsm_general_settings');
		$query = $this->db->get();
		$t =  $query->result_array();
		$settings = array();
		foreach($t as $res){
			$settings[$res["setting_key"]] = $res["setting_value"];
		}
		return $settings;
	}
	
	public function save($postvars){
		
		$report = array();
		foreach($postvars as $key=>$value){
			if(trim($value)!="")
			{
				$this->db->where("setting_key",$key);
				$this->db->delete("lsm_general_settings");
				$data = array("setting_key"=>$key, "setting_value"=>$value);
				$insert = $this->db->insert('lsm_general_settings', $data);
				if($this->db->_error_number()!=0)
				{
					array_push($report,$this->db->_error_number());
				}
			}else{
				array_push($report,1);
			}
		}
		
		if(empty($report)){
			return true;
		}else{
			return false;
		}
	}
}
?>	
