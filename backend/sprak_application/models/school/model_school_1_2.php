<?php
class Model_school_1_2 extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

   	public function get_set_by_id($id)
    {
		
		$this->db->select('set_json_data,is_active');
		$this->db->from('lsm_game_json_data');
		$this->db->where('set_id', $id);
		$query = $this->db->get();
		$t =  $query->result_array();
		$json_data = json_decode($t[0]["set_json_data"],true);
		
		$set = array();
		
		$set["related_word"][0]= $this->utilities->word_details_by_id($json_data["related_word"][0]);
		$set["related_word"][1]= $this->utilities->word_details_by_id($json_data["related_word"][1]);
		$set["related_word"][2]= $this->utilities->word_details_by_id($json_data["related_word"][2]);
		$set["sentence"]= $json_data["sentence"];
		$set["is_active"] = $t[0]["is_active"];
		return $set;
    }
	
	public function get_items($exerciseid)
    {
	    $this->db->select("*");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->order_by('set_id', "DESC");
		$query = $this->db->get();
		
		
		$a = 0;
		$array = array();
		foreach($query->result_array() as $results)
		{
			$set_data = json_decode($results["set_json_data"],true);
			//$this->utilities->printr($set_data,1);
			$array[$a]["related_word"][0]= $this->utilities->word_details_by_id($set_data["related_word"][0]);
			$array[$a]["related_word"][1]= $this->utilities->word_details_by_id($set_data["related_word"][1]);
			$array[$a]["related_word"][2]= $this->utilities->word_details_by_id($set_data["related_word"][2]);
			$array[$a]["sentence"]= $set_data["sentence"];
			$array[$a]["answer"][0]= $this->compare_words($array[$a]["related_word"][0]["word"],$set_data["sentence"][0]);
			$array[$a]["answer"][1]= $this->compare_words($array[$a]["related_word"][1]["word"],$set_data["sentence"][1]);
			$array[$a]["answer"][2]= $this->compare_words($array[$a]["related_word"][2]["word"],$set_data["sentence"][2]);
			
			$array[$a]["is_active"] = $results["is_active"];
			$array[$a]["set_id"] = $results["set_id"];
			$a++;			
		}
		return $array;
		
    }

    
	function store_set($data)
    {

		$array = array();
		
		foreach($data["sentence"] as $sentence){
			$array["sentence"][] = trim($sentence);	
		}
		$array["related_word"] = $data["related_word"];
		
		//$this->utilities->printr($array,1);
		$jsontosave = $this->utilities->json($array); 
		
		$datatostore = array("exercise_id"=>$data["exercise_id"],"set_json_data"=>$jsontosave,"is_active"=>$data["is_active"]);
		$insert = $this->db->insert('lsm_game_json_data', $datatostore);
	    return $insert;
	}

   
    function update_set($id, $data)
    {
		$array = array();
		
		foreach($data["sentence"] as $sentence){
			$array["sentence"][] = trim($sentence);	
		}
		
		$array["related_word"] = $data["related_word"];
		$jsontosave = $this->utilities->json($array);
		
		$datatostore = array("set_json_data"=>$jsontosave,"is_active"=>$data["is_active"]);
		$this->db->where('set_id', $id);
		$update = $this->db->update('lsm_game_json_data', $datatostore);
		return $update;
	}

	/*Following method compare two similar words (one complete word and another same word but with one missing character), 
    and return the missing character*/

    public function compare_words($word1,$word2){
    	$incomplete_word_arr = mb_str_split($word2);
    	$complete_word_arr = mb_str_split($word1);
    	$arrayreturn = array();
    	$keys = array_keys($incomplete_word_arr,"_");

    	foreach($keys as $key)
    	{
    		array_push($arrayreturn,$complete_word_arr[$key]);
    	} 
    	return implode('',$arrayreturn);
    }
   	/*
	Method to create JSON 
	*/
 	function create_json($exerciseid){
		$file_to_create = UPLOADS_DIR."exercises/school/school_1_2.json";
		$this->db->select("set_json_data");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->where("is_active",1);
		$query = $this->db->get();
		$t =  $query->result_array();
	
		$a = 0;
		$array = array();
		if($t)
		{
			foreach($t as $o){
				$setdata  = json_decode($o["set_json_data"],true);
				//$this->utilities->printr($r,1);
				$k=0;
				foreach($setdata["related_word"] as $r)
				{
					$details = $this->utilities->word_details_by_id($r);
					$array[$a]["image"][]=$details["imageRef"];
					$array[$a]["word"][]=$details["word"];
					$array[$a]["sound"][]=$details["audioRef"];
					$array[$a]["word_id"][]=$details["word_id"];
					$array[$a]["sentence"]=$setdata["sentence"];
					$completeWord = $details["word"];
					$incompleteWord = $setdata["sentence"][$k];
					$array[$a]["answer"][] = $this->compare_words($completeWord,$incompleteWord);

				//$array[$a]["answer"]=$setdata["answer"];
					$k++;
				}
				
				
				
				
				
				
				$a++;
			}
			//$this->utilities->printr($array,1);
			$content_type = 'application/json; charset=utf-8';
			header('Content-type: '.$content_type); 
			
			$fp = fopen($file_to_create, 'w');
			$write = fwrite($fp, $this->utilities->json($array)); 
		
			fclose($fp);
			return $write;
		}else{
			@unlink($file_to_create);
		}
	}
	
	
	//delete selected sets
	function deleteselected($postvars){
		//$this->utilities->printr($postvars,1);
		$report = array();
		foreach($postvars["sets"] as $setid)
		{
			$this->db->where('set_id', $setid);
			$this->db->delete('lsm_game_json_data');
			array_push($report,$this->db->_error_number());
		}
		
		if(!empty($report)){
			return true;
		}else{
			return false;
		}
	}
}
?>	
