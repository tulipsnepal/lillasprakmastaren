<?php
class Model_school_2_2 extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

   	public function get_set_by_id($id)
    {
		$this->db->select('set_json_data,is_active');
		$this->db->from('lsm_game_json_data');
		$this->db->where('set_id', $id);
		$query = $this->db->get();
		$t =  $query->result_array();
		$json_data = json_decode($t[0]["set_json_data"],true);
		//$this->utilities->printr($json_data,1);
		$set = array();
		
		for($k=0; $k<8; $k++)
		{
			$set["short_word"][] = $this->utilities->word_details_by_id($json_data["short_word"][$k]);
			$set["long_word"][]= $this->utilities->word_details_by_id($json_data["long_word"][$k]);
		}
		
		$set["is_active"] = $t[0]["is_active"];
		return $set;
    }
	
	public function get_items($exerciseid)
    {
	    $this->db->select("*");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->order_by('set_id', "DESC");
		$query = $this->db->get();
		
		
		$a = 0;
		$array = array();
		foreach($query->result_array() as $results)
		{
			$set_data = json_decode($results["set_json_data"],true);
			//$this->utilities->printr()
			for($k=0; $k<8; $k++)
			{
				$array[$a]["short_word"][] = $this->utilities->word_details_by_id($set_data["short_word"][$k]);
				$array[$a]["long_word"][]= $this->utilities->word_details_by_id($set_data["long_word"][$k]);
			}
			
			$array[$a]["is_active"] = $results["is_active"];
			$array[$a]["set_id"] = $results["set_id"];
			$a++;			
		}
		return $array;
		
    }

    
	function store_set($data)
    {
    	$array = array();
		
		$array["short_word"] = $data["selected_mainwords"];
		$array["long_word"] = $data["selected_options"];
		

		$jsontosave = $this->utilities->json($array); 
		
		$datatostore = array("exercise_id"=>$this->input->post("exercise_id"),"set_json_data"=>$jsontosave,"is_active"=>$this->input->post("is_active"));
		$insert = $this->db->insert('lsm_game_json_data', $datatostore);
	    return $insert;
	}

   
    function update_set($id, $data)
    {
		$array = array();
		$array["short_word"] = $data["selected_mainwords"];
		$array["long_word"] = $data["selected_options"];
		$jsontosave = $this->utilities->json($array); 
		$datatostore = array("set_json_data"=>$jsontosave,"is_active"=>$data["is_active"]);
		$this->db->where('set_id', $id);
		$update = $this->db->update('lsm_game_json_data', $datatostore);
		return $update;
	}

   	/*
	Method to create JSON 
	*/
 	function create_json($exerciseid){
		$file_to_create = UPLOADS_DIR."exercises/school/school_2_2.json";
		$this->db->select("set_json_data");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->where("is_active",1);
		$query = $this->db->get();
		$t =  $query->result_array();
		$a = 0;
		$array = array();
		if($t)
		{
			foreach($t as $o){
				$r = json_decode($o["set_json_data"],true);

				for($i=0;$i<8;$i++)
				{
						$mainword = $this->utilities->word_details_by_id($r["short_word"][$i]);
						$optionword = $this->utilities->word_details_by_id($r["long_word"][$i]);
						$array[$a]["short_word"][] = $mainword["word"];
						$array[$a]["long_word"][] = $optionword["word"];
				}
				
				
				$a++;
			}
			
			//$this->utilities->printr($array,1);
			$content_type = 'application/json; charset=utf-8';
			header('Content-type: '.$content_type); 
			
			$fp = fopen($file_to_create, 'w');
			$write = fwrite($fp, $this->utilities->json($array));
			fclose($fp);
			return $write;
		}else{
			@unlink($file_to_create);
		}
		
	}
	
	//delete selected sets
	function deleteselected($postvars){
		//$this->utilities->printr($postvars,1);
		$report = array();
		foreach($postvars["sets"] as $setid)
		{
			$this->db->where('set_id', $setid);
			$this->db->delete('lsm_game_json_data');
			array_push($report,$this->db->_error_number());
		}
		
		if(!empty($report)){
			return true;
		}else{
			return false;
		}
	}
	
}
?>	
