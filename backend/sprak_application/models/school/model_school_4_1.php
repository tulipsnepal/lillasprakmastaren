<?php
class Model_school_4_1 extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

   	public function get_set_by_id($id)
    {
		
		$this->db->select('set_json_data,is_active');
		$this->db->from('lsm_game_json_data');
		$this->db->where('set_id', $id);
		$query = $this->db->get();
		$t =  $query->result_array();
		$json_data = json_decode($t[0]["set_json_data"],true);
		
		$set = array();
		
		for($i=0;$i<count($json_data["options"]);$i++)
		{
			$set["words"][]= $this->utilities->word_details_by_id($json_data["options"][$i]);
		}
		$set["group"] = $json_data["group"];
		$set["is_active"] = $t[0]["is_active"];
		return $set;
    }
	
	public function get_items($exerciseid)
    {
	    $this->db->select("*");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->order_by('set_id', "DESC");
		$query = $this->db->get();
		
		
		$a = 0;
		$array = array();
		foreach($query->result_array() as $results)
		{
			$set_data = json_decode($results["set_json_data"],true);
			
			for($i=1;$i<=count($set_data["options"]);$i++)
			{
				$array[$a]["word_".$i]= $this->utilities->word_details_by_id($set_data["options"][$i-1]);
			}
			$array[$a]["group"]= $set_data["group"];
			$array[$a]["is_active"] = $results["is_active"];
			$array[$a]["set_id"] = $results["set_id"];
			$a++;			
		}

		return $array;
    }

    
	function store_set($data)
    {
		$array = array();
		
		$array["options"] = $data["selected_mainwords"];
		$array["group"] = $data["group"];
		//$this->utilities->printr($data,1);
		$jsontosave = $this->utilities->json($array); 
		
		$datatostore = array("exercise_id"=>$data["exercise_id"],"set_json_data"=>$jsontosave,"is_active"=>$data["is_active"]);
		$insert = $this->db->insert('lsm_game_json_data', $datatostore);
	    return $insert;
	}

   
    function update_set($id, $data)
    {
		$array = array();
		$array["options"] = $data["selected_mainwords"];
		$array["group"] = $data["group"];
		$jsontosave = $this->utilities->json($array); 
		
		$datatostore = array("set_json_data"=>$jsontosave,"is_active"=>$data["is_active"]);
		$this->db->where('set_id', $id);
		$update = $this->db->update('lsm_game_json_data', $datatostore);
		return $update;
	}

   	/*
	Method to create JSON 
	*/
 	function create_json($exerciseid){
		$file_to_create = UPLOADS_DIR."exercises/school/school_4_1.json";
		$this->db->select("set_json_data");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->where("is_active",1);
		$query = $this->db->get();
		$t =  $query->result_array();
		$jArray = array();
		if($t){
			$a = 0;
			foreach($t as $o){
				$r = json_decode($o["set_json_data"],true);
				$group = $r["group"];
				foreach($r["options"] as $rOptions)
				{
					$w  = $this->utilities->word_details_by_id($rOptions);
					$array[$group][$a]["word"][] = $w["word"];
					$array[$group][$a]["image"][] = $w["imageRef"];
					$array[$group][$a]["sound"][] = $w["audioRef"];
					$array[$group][$a]["startswith"][] = mb_substr($w["word"],0,2);
				}

				$a++;

				//REINDEXING THE ARRAY
				$i=0;
				foreach($array[$group] as $key=>$value){
					$jArray[$group][$i] = $value; 
					$i++;
				}
			}



			
			
			$content_type = 'application/json; charset=utf-8';
			header('Content-type: '.$content_type); 
			
			$fp = fopen($file_to_create, 'w');
			$write = fwrite($fp,$this->utilities->json($jArray)); 
			fclose($fp);
			return $write;
		}else{
			@unlink($file_to_create);
		}
	}
	
	//delete selected sets
	function deleteselected($postvars){
		$report = array();
		
		foreach($postvars["sets"] as $setid)
		{
			$this->db->where('set_id', $setid);
			$this->db->delete('lsm_game_json_data');
			array_push($report,$this->db->_error_number());
		}
		
		if(!empty($report)){
			return true;
		}else{
			return false;
		}
	}
	
}
?>	
