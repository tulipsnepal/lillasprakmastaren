<?php

class Admin_users_model extends CI_Model {

   
    public function get_users()
    {
	    
		$this->db->select('*');
		$this->db->from('lsm_admin_users');
		$this->db->order_by('first_name', "ASC");
		$query = $this->db->get();
		
		return $query->result_array(); 	
    }

    
	 public function get_user_by_id($id)
    {
		$this->db->select('*');
		$this->db->from('lsm_admin_users');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$t =  $query->result_array();
		return $t[0]; 
    }
	
    function count_users()
    {
		$this->db->select('*');
		$this->db->from('lsm_admin_users');
		
		$query = $this->db->get();
		return $query->num_rows();        
    }
	
   /**
    * Store the new item into the database
    * @param array $data - associative array with data to store
    * @return boolean 
    */
    function store_users($data)
    {
		$this->db->select('*');
		$this->db->from('lsm_admin_users');
		$this->db->where("user_name",$data["user_name"]);
		$query = $this->db->get();
		
		if($query->num_rows()>0){
			return false;
		}
		$insert = $this->db->insert('lsm_admin_users', $data);
	    return $insert;
	}
	
	function update_users($id, $data)
    {
		$this->db->where('id', $id);
		$this->db->update('lsm_admin_users', $data);
		$report = array();
		$report['error'] = $this->db->_error_number();
		$report['message'] = $this->db->_error_message();
		if($report !== 0){
			return true;
		}else{
			return false;
		}
	}
}

