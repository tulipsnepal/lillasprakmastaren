<?php
class Model_playground_3_1 extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

   	public function get_set_by_id($id)
    {
		
		$this->db->select('set_json_data,is_active');
		$this->db->from('lsm_game_json_data');
		$this->db->where('set_id', $id);
		$query = $this->db->get();
		$t =  $query->result_array();
		$json_data = json_decode($t[0]["set_json_data"],true);
		
		$set = array();
		
		for($i=0;$i<count($json_data["options"]);$i++)
		{
			$set["words"][]= $this->utilities->word_details_by_id($json_data["options"][$i]);
		}
		$set["category"] = $json_data["category"];
		$set["same_category"] =$json_data["same_cateory_items"];
		$set["is_active"] = $t[0]["is_active"];
		return $set;
    }
	
	public function get_items($exerciseid)
    {
	    $this->db->select("*");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->order_by('set_id', "DESC");
		$query = $this->db->get();
		
		
		$a = 0;
		$array = array();
		foreach($query->result_array() as $results)
		{
			$set_data = json_decode($results["set_json_data"],true);
			$array[$a]["same_category"]= $set_data["same_cateory_items"];
			for($i=1;$i<=count($set_data["options"]);$i++)
			{
				$w = $this->utilities->word_details_by_id($set_data["options"][$i-1]);
				$array[$a]["word_".$i]= $w;
				if(in_array($w["word_id"],$set_data["same_cateory_items"]))
				{
					$array[$a]["word_".$i]["bold_class"] = "bold";
				}else{
					$array[$a]["word_".$i]["bold_class"] = "";
				}
			}
			
			$array[$a]["category"]= $set_data["category"];
			$array[$a]["is_active"] = $results["is_active"];
			$array[$a]["set_id"] = $results["set_id"];
			$a++;			
		}
		return $array;
    }

    
	function store_set($data)
    {
		$array = array();
		
		$array["options"] = $data["selected_mainwords"];
		$array["category"] = $data["category"];
		$array["same_cateory_items"] = $data["same_category"];
		$jsontosave = $this->utilities->json($array); 
		
		$datatostore = array("exercise_id"=>$data["exercise_id"],"set_json_data"=>$jsontosave,"is_active"=>$data["is_active"]);
		$insert = $this->db->insert('lsm_game_json_data', $datatostore);
	    return $insert;
	}

   
    function update_set($id, $data)
    {
		$array = array();
		$array["options"] = $data["selected_mainwords"];
		$array["category"] = $data["category"];
		$array["same_cateory_items"] = $data["same_category"];
		$jsontosave = $this->utilities->json($array); 
		
		$datatostore = array("set_json_data"=>$jsontosave,"is_active"=>$data["is_active"]);
		$this->db->where('set_id', $id);
		$update = $this->db->update('lsm_game_json_data', $datatostore);
		return $update;
	}

   	/*
	Method to create JSON 
	*/
 	function create_json($exerciseid){
		$file_to_create = UPLOADS_DIR."exercises/playground/playground_3_1.json";
		$this->db->select("set_json_data");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->where("is_active",1);
		$query = $this->db->get();
		$t =  $query->result_array();
		$a = 0;
		if($t){
			//sort array in such a way that same element in an array doesnot come multiple times in a row, START
			$setcategories = array();
			//$this->utilities->printr($t,1);
			foreach($t as $o){
				$r = json_decode($o["set_json_data"],true);
				array_push($setcategories,$r["category"]);
			}
			
			

			$buckets = array_count_values($setcategories);
			$mobler = array_slice($t,array_search("möbler",$setcategories),$buckets["möbler"]);
			$djur = array_slice($t,array_search("djur",$setcategories),$buckets["djur"]);
			$namn = array_slice($t,array_search("namn",$setcategories),$buckets["namn"]);
			$fordon = array_slice($t,array_search("fordon",$setcategories),$buckets["fordon"]);
			$klader = array_slice($t,array_search("kläder",$setcategories),$buckets["kläder"]);
			$farger = array_slice($t,array_search("färger",$setcategories),$buckets["färger"]);
			$koksredskap = array_slice($t,array_search("köksredskap",$setcategories),$buckets["köksredskap"]);
			$maxlength = max($buckets);
			$finalt = array();

			for($k=0;$k<$maxlength;$k++){
				array_push($finalt,array_key_exists($k, $mobler)?$mobler[$k]:"");
				array_push($finalt,array_key_exists($k, $djur)?$djur[$k]:"");
				array_push($finalt,array_key_exists($k, $namn)?$namn[$k]:"");
				array_push($finalt,array_key_exists($k, $fordon)?$fordon[$k]:"");
				array_push($finalt,array_key_exists($k, $klader)?$klader[$k]:"");
				array_push($finalt,array_key_exists($k, $farger)?$farger[$k]:"");
				array_push($finalt,array_key_exists($k, $koksredskap)?$koksredskap[$k]:"");
			}

			$finalt = array_values(array_filter($finalt));

			//sort array in such a way that same element in an array doesnot come multiple times in a row, END
			
			foreach($finalt as $o){
				$r = json_decode($o["set_json_data"],true);
				
				for($i=1;$i<=count($r["options"]);$i++)
				{
					$w  = $this->utilities->word_details_by_id($r["options"][$i-1]);
					$array[$a]["word"][] = $w["word"];
				}

				for($i=1;$i<=count($r["same_cateory_items"]);$i++)
				{
					$w  = $this->utilities->word_details_by_id($r["same_cateory_items"][$i-1]);
					$array[$a]["answer"][] = $w["word"];
				}

				$array[$a]["category"][] = utf8_replace($r["category"]).".png";
				$array[$a]["category"][] = utf8_replace($r["category"]).".mp3";


				$a++;
			}
			
			$content_type = 'application/json; charset=utf-8';
			header('Content-type: '.$content_type); 
			
			$fp = fopen($file_to_create, 'w');
			$write = fwrite($fp,$this->utilities->json($array)); 
			fclose($fp);
			return $write;
		}else{
			@unlink($file_to_create);
		}
	}
	
	//delete selected sets
	function deleteselected($postvars){
		$report = array();
		
		foreach($postvars["sets"] as $setid)
		{
			$this->db->where('set_id', $setid);
			$this->db->delete('lsm_game_json_data');
			array_push($report,$this->db->_error_number());
		}
		
		if(!empty($report)){
			return true;
		}else{
			return false;
		}
	}
	
}
?>	
