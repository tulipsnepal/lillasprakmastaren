<?php
class Model_playground_3_2 extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

   	public function get_set_by_id($id)
    {
		
		$this->db->select('set_json_data,is_active');
		$this->db->from('lsm_game_json_data');
		$this->db->where('set_id', $id);
		$query = $this->db->get();
		$t =  $query->result_array();
		$json_data = json_decode($t[0]["set_json_data"],true);
		
		$set = array();
		
		for($i=0;$i<count($json_data["options"]);$i++)
		{
			$set["words"][]= $this->utilities->word_details_by_id($json_data["options"][$i]);
			$set["options_category"] = $json_data["options_category"];
		}
		$set["category"] = $json_data["categories"];
		$set["is_active"] = $t[0]["is_active"];
		return $set;
    }
	
	public function get_items($exerciseid)
    {
	    $this->db->select("*");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->order_by('set_id', "DESC");
		$query = $this->db->get();
		
		
		$a = 0;
		$array = array();
		foreach($query->result_array() as $results)
		{
			$set_data = json_decode($results["set_json_data"],true);
			for($i=1;$i<=count($set_data["options"]);$i++)
			{
				$w = $this->utilities->word_details_by_id($set_data["options"][$i-1]);
				$array[$a]["word_".$i]= $w;
				$array[$a]["category_".$i]= $set_data["options_category"][$i-1];
			}
			
			$array[$a]["category"]= $set_data["categories"];
			$array[$a]["is_active"] = $results["is_active"];
			$array[$a]["set_id"] = $results["set_id"];
			$a++;			
		}
		return $array;
    }

    
	function store_set($data)
    {
		$array = array();
		
		$array["options"] = $data["selected_mainwords"];
		$array["categories"] = $data["categories"];
		$array["options_category"] = $data["word_category"];
		$jsontosave = $this->utilities->json($array); 
		
		$datatostore = array("exercise_id"=>$data["exercise_id"],"set_json_data"=>$jsontosave,"is_active"=>$data["is_active"]);
		$insert = $this->db->insert('lsm_game_json_data', $datatostore);
	    return $insert;
	}

   
    function update_set($id, $data)
    {
		$array = array();
		$array["options"] = $data["selected_mainwords"];
		$array["categories"] = $data["categories"];
		$array["options_category"] = $data["word_category"];
		$jsontosave = $this->utilities->json($array); 
		
		$datatostore = array("set_json_data"=>$jsontosave,"is_active"=>$data["is_active"]);
		$this->db->where('set_id', $id);
		$update = $this->db->update('lsm_game_json_data', $datatostore);
		return $update;
	}

   	/*
	Method to create JSON 
	*/
 	function create_json($exerciseid){
		$file_to_create = UPLOADS_DIR."exercises/playground/playground_3_2.json";
		$this->db->select("set_json_data");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->where("is_active",1);
		$query = $this->db->get();
		$t =  $query->result_array();
		$a = 0;
		if($t){
			foreach($t as $o){
				$r = json_decode($o["set_json_data"],true);
				
				for($i=1;$i<=count($r["options"]);$i++)
				{
					$w  = $this->utilities->word_details_by_id($r["options"][$i-1]);
					$array[$a]["word"][] = $w["word"];
					$array[$a]["image"][] = $w["imageRef"];
					$array[$a]["sound"][] = $w["audioRef"];
				}

				for($i=1;$i<=count($r["options_category"]);$i++)
				{
					$array[$a]["answer"][] = $r["options_category"][$i-1];
				}

				for($i=1;$i<=count($r["categories"]);$i++)
				{
					$array[$a]["category"]["name"][] = $r["categories"][$i-1];
					$array[$a]["category"]["image"][] = utf8_replace($r["categories"][$i-1]).".png";
					$array[$a]["category"]["sound"][] = utf8_replace($r["categories"][$i-1]).".mp3";
				}


				$a++;
			}
			
			$content_type = 'application/json; charset=utf-8';
			header('Content-type: '.$content_type); 
			
			$fp = fopen($file_to_create, 'w');
			$write = fwrite($fp,$this->utilities->json($array)); 
			fclose($fp);
			return $write;
		}else{
			@unlink($file_to_create);
		}
	}
	
	//delete selected sets
	function deleteselected($postvars){
		$report = array();
		
		foreach($postvars["sets"] as $setid)
		{
			$this->db->where('set_id', $setid);
			$this->db->delete('lsm_game_json_data');
			array_push($report,$this->db->_error_number());
		}
		
		if(!empty($report)){
			return true;
		}else{
			return false;
		}
	}
	
}
?>	
