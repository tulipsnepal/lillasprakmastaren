<?php
class Model_playground_2_1 extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

   	public function get_set_by_id($id)
    {
		
		$this->db->select('set_json_data,is_active');
		$this->db->from('lsm_game_json_data');
		$this->db->where('set_id', $id);
		$query = $this->db->get();
		$t =  $query->result_array();
		$json_data = json_decode($t[0]["set_json_data"],true);
		
		$set = array();
		
		$set["exercise_sentences"] = $json_data["exercise_sentences"];
		$set["answers"] = $json_data["answers"];
		$set["is_active"] = $t[0]["is_active"];
		return $set;
    }
	
	public function get_items($exerciseid)
    {
	    $this->db->select("*");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->order_by('set_id', "DESC");
		$query = $this->db->get();
		
		
		$a = 0;
		$array = array();
		foreach($query->result_array() as $results)
		{
			$set_data = json_decode($results["set_json_data"],true);
			
			$array[$a]["exercise_sentences"]= $set_data["exercise_sentences"];
			$array[$a]["answers"] = $set_data["answers"];
			$array[$a]["is_active"] = $results["is_active"];
			$array[$a]["set_id"] = $results["set_id"];
			$a++;			
		}
		return $array;
		
    }

    
	function store_set($data)
    {

		$array = array();
		$array["exercise_sentences"] = $data["exercise_sentences"];
		$array["answers"] = $data["answers"];
		$jsontosave = $this->utilities->json($array); 
		
		$datatostore = array("exercise_id"=>$data["exercise_id"],"set_json_data"=>$jsontosave,"is_active"=>$data["is_active"]);
		$insert = $this->db->insert('lsm_game_json_data', $datatostore);
	    return $insert;
	}

   
    function update_set($id, $data)
    {
		$array = array();
		$array["exercise_sentences"] = $data["exercise_sentences"];
		$array["answers"] = $data["answers"];
		$jsontosave = $this->utilities->json($array);
		
		$datatostore = array("set_json_data"=>$jsontosave,"is_active"=>$data["is_active"]);
		$this->db->where('set_id', $id);
		$update = $this->db->update('lsm_game_json_data', $datatostore);
		return $update;
	}

   	/*
	Method to create JSON 
	*/
 	function create_json($exerciseid){
		$file_to_create = UPLOADS_DIR."exercises/playground/playground_2_1.json";
		$this->db->select("set_json_data");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->where("is_active",1);
		$query = $this->db->get();
		$t =  $query->result_array();
	
		$a = 0;
		$array = array();
		if($t)
		{
			
			foreach($t as $o){
				$r  = json_decode($o["set_json_data"],true);
				$array[$a]["exercise_sentences"] = $r["exercise_sentences"];
				$array[$a]["answers"] = $r["answers"];
				$arrayOptions = $r["answers"];
				shuffle($arrayOptions);
				$array[$a]["options"] = $arrayOptions;
				$a++;
			}
			//$this->utilities->printr($array,1);
			$content_type = 'application/json; charset=utf-8';
			header('Content-type: '.$content_type); 
			
			$fp = fopen($file_to_create, 'w');
			$write = fwrite($fp, $this->utilities->json($array)); 
		
			fclose($fp);
			return $write;
		}else{
			@unlink($file_to_create);
		}
	}
	
	
	//delete selected sets
	function deleteselected($postvars){
		//$this->utilities->printr($postvars,1);
		$report = array();
		foreach($postvars["sets"] as $setid)
		{
			$this->db->where('set_id', $setid);
			$this->db->delete('lsm_game_json_data');
			array_push($report,$this->db->_error_number());
		}
		
		if(!empty($report)){
			return true;
		}else{
			return false;
		}
	}
}
?>	
