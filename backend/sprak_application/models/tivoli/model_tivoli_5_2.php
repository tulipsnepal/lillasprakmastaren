<?php
class Model_tivoli_5_2 extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

   	public function get_set_by_id($id)
    {
		
		$this->db->select('set_json_data,is_active');
		$this->db->from('lsm_game_json_data');
		$this->db->where('set_id', $id);
		$query = $this->db->get();
		$t =  $query->result_array();
		$json_data = json_decode($t[0]["set_json_data"],true);
		
		$set = array();
		
		$set["related_word"][0]= $this->utilities->word_details_by_id($json_data["related_word"][0]);
		$set["related_word"][1]= $this->utilities->word_details_by_id($json_data["related_word"][1]);
		$set["related_word"][2]= $this->utilities->word_details_by_id($json_data["related_word"][2]);
		$set["related_word"][3]= $this->utilities->word_details_by_id($json_data["related_word"][3]);
		$set["sentence"]= $json_data["sentence"];
		$set["answer"]= $json_data["answer"];
		$set["is_active"] = $t[0]["is_active"];
		return $set;
    }
	
	public function get_items($exerciseid)
    {
	    $this->db->select("*");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->order_by('set_id', "DESC");
		$query = $this->db->get();
		
		
		$a = 0;
		$array = array();
		foreach($query->result_array() as $results)
		{
			$set_data = json_decode($results["set_json_data"],true);
			//$this->utilities->printr($set_data,1);
			$array[$a]["related_word"][0]= $this->utilities->word_details_by_id($set_data["related_word"][0]);
			$array[$a]["related_word"][1]= $this->utilities->word_details_by_id($set_data["related_word"][1]);
			$array[$a]["related_word"][2]= $this->utilities->word_details_by_id($set_data["related_word"][2]);
			$array[$a]["related_word"][3]= $this->utilities->word_details_by_id($set_data["related_word"][3]);
			$array[$a]["sentence"]= $set_data["sentence"];
			$array[$a]["answer"]= $set_data["answer"];
			
			$array[$a]["is_active"] = $results["is_active"];
			$array[$a]["set_id"] = $results["set_id"];
			$a++;			
		}
		return $array;
		
    }

    
	function store_set($data)
    {
		$array = array();
		
		$array["sentence"] = $data["sentence"];
		$array["related_word"] = $data["related_word"];
		$array["answer"] = $data["answer"];
		
		
		//$this->utilities->printr($array,1);
		$jsontosave = $this->utilities->json($array); 
		
		$datatostore = array("exercise_id"=>$data["exercise_id"],"set_json_data"=>$jsontosave,"is_active"=>$data["is_active"]);
		$insert = $this->db->insert('lsm_game_json_data', $datatostore);
	    return $insert;
	}

   
    function update_set($id, $data)
    {
		$array = array();
		$array["sentence"] = $data["sentence"];
		$array["related_word"] = $data["related_word"];
		$array["answer"] = $data["answer"];
		$jsontosave = $this->utilities->json($array);
		
		$datatostore = array("set_json_data"=>$jsontosave,"is_active"=>$data["is_active"]);
		$this->db->where('set_id', $id);
		$update = $this->db->update('lsm_game_json_data', $datatostore);
		return $update;
	}

   	/*
	Method to create JSON 
	*/
 	function create_json($exerciseid){
		$file_to_create = UPLOADS_DIR."exercises/tivoli/tivoli_5_2.json";
		$this->db->select("set_json_data");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->where("is_active",1);
		$query = $this->db->get();
		$t =  $query->result_array();
	
		$a = 0;
		$array = array();
		if($t)
		{
			foreach($t as $o){
				$setdata  = json_decode($o["set_json_data"],true);
				//$this->utilities->printr($r,1);
				foreach($setdata["related_word"] as $r)
				{
					$details = $this->utilities->word_details_by_id($r);
					$array[$a]["image"][]=$details["imageRef"];
					$array[$a]["word"][]=$details["word"];
					/*if(strpos($details["word"],"flicka")!==false)
					{
						$array[$a]["sound"][]="flicka.mp3";
					}else if(strpos($details["word"],"pojke")!==false)
					{
						$array[$a]["sound"][]="pojke.mp3";
					}else{
						$array[$a]["sound"][]=$details["audioRef"];
					}*/
					$array[$a]["sound"][]=$details["audioRef"];
					$array[$a]["word_id"][]=$details["word_id"];
				}
				
				
				$array[$a]["sentence"]=$setdata["sentence"];
				$array[$a]["answer"]=$setdata["answer"];
				
				
				
				$a++;
			}
		
			$content_type = 'application/json; charset=utf-8';
			header('Content-type: '.$content_type); 
			
			$fp = fopen($file_to_create, 'w');
			$write = fwrite($fp, $this->utilities->json($array)); 
		
			fclose($fp);
			return $write;
		}else{
			@unlink($file_to_create);
		}
	}
	
	
	//delete selected sets
	function deleteselected($postvars){
		//$this->utilities->printr($postvars,1);
		$report = array();
		foreach($postvars["sets"] as $setid)
		{
			$this->db->where('set_id', $setid);
			$this->db->delete('lsm_game_json_data');
			array_push($report,$this->db->_error_number());
		}
		
		if(!empty($report)){
			return true;
		}else{
			return false;
		}
	}
}
?>	
