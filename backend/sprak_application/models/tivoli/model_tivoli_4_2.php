<?php
class Model_tivoli_4_2 extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

   	public function get_set_by_id($id)
    {
		$this->db->select('*');
		$this->db->from('lsm_game_json_data');
		$this->db->where('set_id', $id);
		$query = $this->db->get();
		$t =  $query->result_array();
		$set["exercise_id"] = $t[0]["exercise_id"];
		$set["is_active"] = $t[0]["is_active"];
		$set["set_data"] = json_decode($t[0]["set_json_data"],true);
		return $set;
    }
	
	
    
    public function get_items($exerciseid)
    {
	    
		$this->db->select("*");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->order_by('set_id', "ASC");
		$query = $this->db->get();
		
		
		$a = 0;
		$array = array();
		foreach($query->result_array() as $results)
		{
			$array[$a]["set_arrays"] = json_decode($results["set_json_data"],true);
			$array[$a]["is_active"] = $results["is_active"];
			$array[$a]["set_id"] = $results["set_id"];
			$a++;
		}
		
		return $array;
    }

    
	function store_set($data)
    {
		$mainwordIndex = array(2,5,8);
		$selectedwords = $data["selectedoptionword"];
		$array = array();
		for($i=0;$i<count($selectedwords);$i++)
		{
			if(!in_array($i,$mainwordIndex)) //option words
			{
				$array["options"]["word"][]= $selectedwords[$i];
			}else{ //question words
				$array["questions"]["word"][]= $selectedwords[$i];
			}
			
		}
		
		for($j=0;$j<count($mainwordIndex);$j++){
			$array["answers"][$selectedwords[$mainwordIndex[$j]]]["first"] =$selectedwords[$mainwordIndex[$j]-2];
			$array["answers"][$selectedwords[$mainwordIndex[$j]]]["second"] = $selectedwords[$mainwordIndex[$j]-1];
		}
		
		$jsontosave = $this->utilities->json($array); 
		
		$datatostore = array("exercise_id"=>$this->input->post("exercise_id"),"set_json_data"=>$jsontosave,"is_active"=>$this->input->post("is_active"));
		$insert = $this->db->insert('lsm_game_json_data', $datatostore);
	    return $insert;
	}

   
    function update_set($id, $data)
    {
		$mainwordIndex = array(2,5,8);
		$selectedwords = $data["selectedoptionword"];
		$array = array();
		for($i=0;$i<count($selectedwords);$i++)
		{
			if(!in_array($i,$mainwordIndex)) //option words
			{
				$array["options"]["word"][]= $selectedwords[$i];
			}else{ //question words
				$array["questions"]["word"][]= $selectedwords[$i];
			}
			
		}
		
		for($j=0;$j<count($mainwordIndex);$j++){
			$array["answers"][$selectedwords[$mainwordIndex[$j]]]["first"] =$selectedwords[$mainwordIndex[$j]-2];
			$array["answers"][$selectedwords[$mainwordIndex[$j]]]["second"] = $selectedwords[$mainwordIndex[$j]-1];
		}
		
		$jsontosave = $this->utilities->json($array); 
		
		$datatostore = array("set_json_data"=>$jsontosave,"is_active"=>$this->input->post("is_active"));
		$this->db->where("set_id",$id);
		$insert = $this->db->update('lsm_game_json_data', $datatostore);
	    return $insert;
	}

   	/*
	Method to create JSON 
	*/
 	function create_json($exerciseid){
		$file_to_create = UPLOADS_DIR."exercises/tivoli/tivoli_4_2.json";
		$this->db->select("*");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->order_by('set_id', "ASC");
		$query = $this->db->get();
		
		
		$a = 0;
		$array = array();
		$option = array(); $question = array();
		foreach($query->result_array() as $results)
		{
			$decodedjson = json_decode($results["set_json_data"],true);
			$set_data = $decodedjson["answers"];
			
			foreach($set_data as $key=>$value)
			{
				$compositeword = $this->utilities->word_details_by_id($key);
				$firstword = $this->utilities->word_details_by_id($value["first"]);
				$secondword = $this->utilities->word_details_by_id($value["second"]);
				$option[] = $firstword;
				$option[] = $secondword;
				$array["sets"][$a]["answer"]["word"][] = $compositeword["word"];
				$array["sets"][$a]["answer"]["image"][] = $compositeword["imageRef"]; 
				$array["sets"][$a]["answer"]["sound"][] = $compositeword["audioRef"];
				
				$array["sets"][$a]["word"][] = $firstword["word"];
				$array["sets"][$a]["image"][] = $firstword["imageRef"];
				$array["sets"][$a]["sound"][] = $firstword["audioRef"];
				
				$array["sets"][$a]["word"][] = $secondword["word"];
				$array["sets"][$a]["image"][] = $secondword["imageRef"];
				$array["sets"][$a]["sound"][] = $secondword["audioRef"];
				//$array[$a]["options"]["word"] =  
			}
			
			$a++;
		}
		
		
		//$this->utilities->printr($array,1);
		
		$content_type = 'application/json; charset=utf-8';
		header('Content-type: '.$content_type); 
		
		$fp = fopen($file_to_create, 'w');
		$write = fwrite($fp,$this->utilities->json($array)); 
		fclose($fp);
		return $write;
	}
	
	//delete selected sets
	function deleteselected($postvars){
		//$this->utilities->printr($postvars,1);
		$report = array();
		foreach($postvars["sets"] as $setid)
		{
			$this->db->where('set_id', $setid);
			$this->db->delete('lsm_game_json_data');
			array_push($report,$this->db->_error_number());
		}
		
		if(!empty($report)){
			return true;
		}else{
			return false;
		}
	}
	
}
?>	
