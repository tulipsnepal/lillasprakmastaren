<?php
class Model_tivoli_3_1 extends CI_Model {

    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

   	public function get_words($exerciseid)
    {
		$this->db->select("set_json_data");
		$this->db->where("exercise_id",$exerciseid);
		$this->db->from('lsm_game_json_data');

		$query = $this->db->get();
		$t =  $query->result_array();
		$array = array();
		if($t)
		{
			$word =json_decode($t[0]["set_json_data"],true);

			$a = 0;
			$array1 = isset($word["long"])?$word["long"]:array();
			$array2 = isset($word["short"])?$word["short"]:array();
			$allwords= array_merge($array1,$array2);
			foreach($allwords as $results)
			{
				$array[$a]["word"] =$this->utilities->word_details_by_id($results);
				$a++;
			}
		}
		return $array;
    }

	public function get_items($exerciseid)
    {

		$this->db->select("set_json_data");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->order_by('set_id', "DESC");
		$query = $this->db->get();
		$t =  $query->result_array();
		$array = array();
		if($t)
		{
			$word =json_decode($t[0]["set_json_data"],true);


			if(isset($word["long"]))
			{
				$a = 0;
				foreach($word["long"] as $longword)
				{
					$lword =$this->utilities->word_details_by_id($longword);
					$array["long_word"][$a]["word"] = $lword["word"];
					$array["long_word"][$a]["imageRef"] = $lword["imageRef"];
					$array["long_word"][$a]["audioRef"] = $lword["audioRef"];
					$a++;
				}
			}

			if(isset($word["short"]))
			{
				$b = 0;
				foreach($word["short"] as $shortword)
				{
					$sword =$this->utilities->word_details_by_id($shortword);
					$array["short_word"][$b]["word"] = $sword["word"];
					$array["short_word"][$b]["imageRef"] = $sword["imageRef"];
					$b++;
				}
			}
		}
		return $array;
    }


	function store_set($data)
    {
		$array = array();
		$this->db->where("exercise_id",$data["exercise_id"]);
		$this->db->delete("lsm_game_json_data");
		if(count($data["selected_mainwords"])>0)
		{
			foreach($data["selected_mainwords"] as $wordid)
			{
				if($this->word_is_longest($wordid)==1)
				{
					$array["long"][] = $wordid;
				}else{
					$array["short"][] = $wordid;
				}
			}
			$jsontosave = $this->utilities->json($array);
			$datatostore = array("exercise_id"=>$data["exercise_id"],"set_json_data"=>$jsontosave,"is_active"=>1);
			$insert = $this->db->insert('lsm_game_json_data', $datatostore);
			return $insert;
		}else{
			return true;
		}
	}

	function word_is_longest($wordid)
	{
		$this->db->select("word");
		$this->db->from('lsm_game_words');
		$this->db->where('word_id', $wordid);
		$query = $this->db->get();
		$t =  $query->result_array();
		$word =$t[0]["word"];
		$type =  mb_strlen($t[0]["word"])<$this->utilities->settings("longest_word_min_length")?0:1;
		return $type;
	}

   	/*
	Method to create JSON
	*/
 	function create_json($exerciseid){
		$file_to_create = UPLOADS_DIR."exercises/tivoli/tivoli_3_1.json";
		$this->db->select("set_json_data");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$query = $this->db->get();
		$t =  $query->result_array();
		$wordlist =json_decode($t[0]["set_json_data"],true);

		$totalwords = count($wordlist["long"]) + count($wordlist["short"]);

		$number_of_sets = floor($totalwords/10); //get number of sets
		$arrayset = array(array(7,3),array(3,7),array(5,5),array(6,4),array(4,6)); // Combination of long,short words

		$array = array();
		if($t)
		{
			for($i=0;$i<$number_of_sets;$i++)
			{
				$randomSet = $arrayset[array_rand($arrayset)];

				$req_no_of_long_words = $randomSet[0];
				$req_no_of_short_words = $randomSet[1];

				if(($req_no_of_long_words+$req_no_of_short_words)!=10) //if sum of long and short word is not 10, then by default set both values to 5
				{
					$req_no_of_long_words = 5;
					$req_no_of_short_words = 5;
				}

				$longest_rand = array_rand(array_flip($wordlist["long"]),$req_no_of_long_words);
				$shortest_rand = array_rand(array_flip($wordlist["short"]),$req_no_of_short_words);

				$selectedwords = array_merge($longest_rand,$shortest_rand);
				shuffle($selectedwords);

				foreach($selectedwords as $selected)
				{
					$word = $this->utilities->word_details_by_id($selected);
					$array[$i]["word"][] = $word["word"];
					$array[$i]["image"][] = $word["imageRef"];
					$array[$i]["sound"][] = $word["audioRef"];
					$array[$i]["answer"][] = $this->word_is_longest($selected);
				}


			}

			$content_type = 'application/json; charset=utf-8';
			header('Content-type: '.$content_type);

			$fp = fopen($file_to_create, 'w');
			$write = fwrite($fp,$this->utilities->json($array));

			fclose($fp);
			return $write;
		}else{
			@unlink($file_to_create);
		}
	}

}
?>
