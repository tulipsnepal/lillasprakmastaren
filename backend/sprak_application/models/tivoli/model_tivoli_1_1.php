<?php
class Model_tivoli_1_1 extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

   	public function get_set_by_id($id)
    {
		$this->db->select('set_json_data,is_active');
		$this->db->from('lsm_game_json_data');
		$this->db->where('set_id', $id);
		$query = $this->db->get();
		$t =  $query->result_array();
		$json_data = json_decode($t[0]["set_json_data"],true);
		//$this->utilities->printr($json_data,1);
		$set = array();
		$set["main"][] = $this->utilities->word_details_by_id($json_data["main"]);
		$set["main"][] = $this->utilities->word_details_by_id($json_data["rhyming"]);
		$set["optional"][] = $this->utilities->word_details_by_id($json_data["options"][0]);
		$set["optional"][] = $this->utilities->word_details_by_id($json_data["options"][1]);
		$set["optional"][] = $this->utilities->word_details_by_id($json_data["options"][2]);
		$set["is_active"] = $t[0]["is_active"];
		return $set;
    }
	
	public function get_items($exerciseid)
    {
	    $this->db->select("*");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->order_by('set_id', "DESC");
		$query = $this->db->get();
		
		
		$a = 0;
		$array = array();
		foreach($query->result_array() as $results)
		{
			$set_data = json_decode($results["set_json_data"],true);
			$array[$a]["main_word"]= $this->utilities->word_details_by_id($set_data["main"]);
			$array[$a]["rhyming_word"]= $this->utilities->word_details_by_id($set_data["rhyming"]);
			$array[$a]["option_1"]= $this->utilities->word_details_by_id($set_data["options"][0]);
			$array[$a]["option_2"]= $this->utilities->word_details_by_id($set_data["options"][1]);
			$array[$a]["option_3"]= $this->utilities->word_details_by_id($set_data["options"][2]);
			$array[$a]["is_active"] = $results["is_active"];
			$array[$a]["set_id"] = $results["set_id"];
			$a++;			
		}
		return $array;
		
    }

    
	function store_set($data)
    {
		$array = array();
		
		$array["main"] = $data["selected_mainwords"][0];
		$array["rhyming"] = $data["selected_mainwords"][1];
		$array["options"] = $data["selected_options"];
		

		$jsontosave = $this->utilities->json($array); 
		
		$datatostore = array("exercise_id"=>$this->input->post("exercise_id"),"set_json_data"=>$jsontosave,"is_active"=>$this->input->post("is_active"));
		$insert = $this->db->insert('lsm_game_json_data', $datatostore);
	    return $insert;
	}

   
    function update_set($id, $data)
    {
		$array = array();
		$array["main"] = $data["selected_mainwords"][0];
		$array["rhyming"] = $data["selected_mainwords"][1];
		$array["options"] = $data["selected_options"];
		$jsontosave = $this->utilities->json($array); 
		$datatostore = array("set_json_data"=>$jsontosave,"is_active"=>$data["is_active"]);
		$this->db->where('set_id', $id);
		$update = $this->db->update('lsm_game_json_data', $datatostore);
		return $update;
	}

   	/*
	Method to create JSON 
	*/
 	function create_json($exerciseid){
		$file_to_create = UPLOADS_DIR."exercises/tivoli/tivoli_1_1.json";
		$this->db->select("set_json_data");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->where("is_active",1);
		$query = $this->db->get();
		$t =  $query->result_array();
		$a = 0;
		$array = array();
		if($t)
		{
			foreach($t as $o){
				$r = json_decode($o["set_json_data"],true);
				$mainword = $this->utilities->word_details_by_id($r["main"]);
				
				$array[$a]["word"][] = $mainword["imageRef"];
				$array[$a]["word"][] = $mainword["audioRef"];
				$word[$a][]= $this->utilities->word_details_by_id($r["rhyming"]);
				$word[$a][] = $this->utilities->word_details_by_id($r["options"][0]);
				$word[$a][] = $this->utilities->word_details_by_id($r["options"][1]);
				$word[$a][] = $this->utilities->word_details_by_id($r["options"][2]);
				
				//shuffle options
				shuffle($word[$a]);
				$answer = $this->utilities->word_details_by_id($r["rhyming"]); 
				$answerid = $answer["word_id"];
				
				for($i=0;$i<4;$i++){
					$array[$a]["image"][$i] = $word[$a][$i]["imageRef"];
					$array[$a]["sound"][$i] = $word[$a][$i]["audioRef"];
					$array[$a]["answer"][$i] = $word[$a][$i]["word_id"]==$answerid?1:0;
				}
				
				$a++;
			}
			
			$content_type = 'application/json; charset=utf-8';
			header('Content-type: '.$content_type); 
			
			$fp = fopen($file_to_create, 'w');
			$write = fwrite($fp, $this->utilities->json($array));
			fclose($fp);
			return $write;
		}else{
			@unlink($file_to_create);
		}
		
	}
	
	//delete selected sets
	function deleteselected($postvars){
		//$this->utilities->printr($postvars,1);
		$report = array();
		foreach($postvars["sets"] as $setid)
		{
			$this->db->where('set_id', $setid);
			$this->db->delete('lsm_game_json_data');
			array_push($report,$this->db->_error_number());
		}
		
		if(!empty($report)){
			return true;
		}else{
			return false;
		}
	}
	
}
?>	
