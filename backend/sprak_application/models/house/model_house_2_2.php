<?php
class Model_house_2_2 extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

   	public function get_words($exerciseid)
    {
		$this->db->select("set_json_data");
		$this->db->where("exercise_id",$exerciseid);
		$this->db->from('lsm_game_json_data');
		
		$query = $this->db->get();
		$t =  $query->result_array();
		$array = array();
		if($t)
		{
			$word =json_decode($t[0]["set_json_data"],true);
			
			$a = 0;
			
			foreach($word["selected_words"] as $results)
			{
				$array[$a]["word"] =$this->utilities->word_details_by_id($results);
				$a++;
			}
		}
		return $array;
    }
	
	public function get_items($exerciseid)
    {
	    
		$this->db->select("set_json_data,is_active,set_id");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->order_by('set_id', "DESC");
		$query = $this->db->get();
		$t =  $query->result_array();
		$array = array();
		
		if($t)
		{
			$a = 0;
			$set_data = json_decode($t[0]["set_json_data"],true);
			foreach($set_data["selected_words"] as $wordid){
				$selected = $this->utilities->word_details_by_id($wordid);
				$array["selected_words"][] = $selected;
				$a++;
			}
				
			$array["is_active"] = $t[0]["is_active"];
			$array["set_id"] = $t[0]["set_id"];
							
			
			/*
			if(isset($word["long"]))
			{	
				$a = 0;
				foreach($word["long"] as $longword)
				{
					$lword =$this->utilities->word_details_by_id($longword);
					$array["long_word"][$a]["word"] = $lword["word"];
					$array["long_word"][$a]["imageRef"] = $lword["imageRef"];
					$a++;
				}
			}
			
			if(isset($word["short"]))
			{
				$b = 0;
				foreach($word["short"] as $shortword)
				{
					$sword =$this->utilities->word_details_by_id($shortword);
					$array["short_word"][$b]["word"] = $sword["word"];
					$array["short_word"][$b]["imageRef"] = $sword["imageRef"];
					$b++;
				}
			}*/
		}
		return $array;
    }

    
	function store_set($data)
    {
		$array = array();
		$this->db->where("exercise_id",$data["exercise_id"]);
		$this->db->delete("lsm_game_json_data");
		if(count($data["selected_mainwords"])>0)
		{
			$array["selected_words"] = $data["selected_mainwords"];
			$jsontosave = $this->utilities->json($array);
			$datatostore = array("exercise_id"=>$data["exercise_id"],"set_json_data"=>$jsontosave,"is_active"=>1);
			$insert = $this->db->insert('lsm_game_json_data', $datatostore);
			return $insert;
		}else{
			return true;
		}
	}
	
	
   
   	/*
	Method to create JSON 
	*/
 	function create_json($exerciseid){
		$file_to_create = UPLOADS_DIR."exercises/house/house_2_2.json";
		$this->db->select("set_json_data");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$query = $this->db->get();
		$t =  $query->result_array();
		$wordlist =json_decode($t[0]["set_json_data"],true);
		
		$totalwords = count($wordlist["long"]) + count($wordlist["short"]);
		
		$number_of_sets = floor($totalwords/10); //get number of sets
		$arrayset = array(array(7,3),array(3,7),array(5,5),array(6,4),array(4,6)); // Combination of long,short words 
		
		$array = array();
		if($t)
		{
			$a = 0;
			$set_data = json_decode($t[0]["set_json_data"],true);
			$arrayWords = $set_data["selected_words"];
			shuffle($arrayWords);
			foreach($arrayWords as $wordid){
				$selected = $this->utilities->word_details_by_id($wordid);
				$array[$a]["image"] = $selected["imageRef"];
				$array[$a]["sound"] = $selected["audioRef"];
				$array[$a]["word"] = mb_strtoupper($selected["word"],"UTF-8");
				$array[$a]["answer"] = $selected["word"];
				$a++;
			}
			
			$content_type = 'application/json; charset=utf-8';
			header('Content-type: '.$content_type); 
			
			$fp = fopen($file_to_create, 'w');
			$write = fwrite($fp,$this->utilities->json($array)); 
			
			fclose($fp);
			return $write;
		}else{
			@unlink($file_to_create);
		}
	}
	
}
?>	
