<?php
class Model_football_3_2 extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

   	public function get_set_by_id($id)
    {
		
		$this->db->select('set_json_data,is_active');
		$this->db->from('lsm_game_json_data');
		$this->db->where('set_id', $id);
		$query = $this->db->get();
		$t =  $query->result_array();
		$json_data = json_decode($t[0]["set_json_data"],true);
		
		$set = array();
		
		$set["exercise_sentences"] = $json_data["exercise_sentences"];
		//$set["character_names"] = $json_data["character_names"];
		$set["is_active"] = $t[0]["is_active"];
		return $set;
    }
	
	public function get_items($exerciseid)
    {
	    $this->db->select("*");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->order_by('set_id', "DESC");
		$query = $this->db->get();
		
		
		$a = 0;
		$array = array();
		foreach($query->result_array() as $results)
		{
			$set_data = json_decode($results["set_json_data"],true);
			
			$array[$a]["exercise_sentences"]= $set_data["exercise_sentences"];
			//$array[$a]["character_names"] = $set_data["character_names"];
			$array[$a]["is_active"] = $results["is_active"];
			$array[$a]["set_id"] = $results["set_id"];
			$a++;			
		}
		return $array;
		
    }

    
	function store_set($data)
    {

		$array = array();
		foreach($data["exercise_sentences"] as $sentence){
			$sentence = str_replace("_"," ",$sentence);
			$sentence = str_replace(".","",$sentence);
			$sentence = preg_replace('!\s+!', ' ', $sentence);

			$array["exercise_sentences"][] = trim($sentence).".";
		}
		/*$character_names= $data["character_names"];
		$character_names = str_replace(array(", ","."),",",$character_names);
		$array["character_names"] = $character_names;*/
		$jsontosave = $this->utilities->json($array); 
		
		$datatostore = array("exercise_id"=>$data["exercise_id"],"set_json_data"=>$jsontosave,"is_active"=>$data["is_active"]);
		$insert = $this->db->insert('lsm_game_json_data', $datatostore);
	    return $insert;
	}

   
    function update_set($id, $data)
    {
		$array = array();
		foreach($data["exercise_sentences"] as $sentence){
			$sentence = str_replace("_"," ",$sentence);
			$sentence = str_replace(".","",$sentence);
			$sentence = preg_replace('!\s+!', ' ', $sentence);

			$array["exercise_sentences"][] = trim($sentence).".";
		}
	/*	$character_names= $data["character_names"];
		$character_names = str_replace(array(", ","."),",",$character_names);
		$array["character_names"] = $character_names;*/
		$jsontosave = $this->utilities->json($array);
		
		$datatostore = array("set_json_data"=>$jsontosave,"is_active"=>$data["is_active"]);
		$this->db->where('set_id', $id);
		$update = $this->db->update('lsm_game_json_data', $datatostore);
		return $update;
	}

   	/*
	Method to create JSON 
	*/
 	function create_json($exerciseid){
		$file_to_create = UPLOADS_DIR."exercises/football/football_3_2.json";
		$this->db->select("set_json_data");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->where("is_active",1);
		$query = $this->db->get();
		$t =  $query->result_array();
	
		$a = 0;
		$array = array();
		if($t)
		{
			
			foreach($t as $o){
				$r  = json_decode($o["set_json_data"],true);
				$array_exploded_words = array();
				foreach($r["exercise_sentences"] as $sentences)
				{
					$array[$a]["exercise_sentences"][] =  preg_replace('!\s+!', ' ', $sentences);

					$sentence = str_replace(".","",$sentences);
					$sentence = preg_replace('!\s+!', ' ', $sentence);
					$exploded_sentence = explode(" ",$sentence);
					$word_to_add ="";
					foreach($exploded_sentence as $words){
						if($words!="en" && $words!="ett")
						{
							$array_exploded_words[] = $word_to_add.$words;
							$word_to_add = "";
						}else{
							$word_to_add = $words." ";;
						}
					}
					$array_exploded_words[] = ".";
					shuffle($array_exploded_words);
					$array[$a]["broken_words"] = $array_exploded_words;
				}
				/*$explodedNames = explode(",",$r["character_names"]);
				foreach($explodedNames as $names){
					$array[$a]["character_names"][] = trim($names);
				}*/
				
				
				$a++;
			}
			//$this->utilities->printr($array,1);
			$content_type = 'application/json; charset=utf-8';
			header('Content-type: '.$content_type); 
			
			$fp = fopen($file_to_create, 'w');
			$write = fwrite($fp, $this->utilities->json($array)); 
		
			fclose($fp);
			return $write;
		}else{
			@unlink($file_to_create);
		}
	}
	
	
	//delete selected sets
	function deleteselected($postvars){
		//$this->utilities->printr($postvars,1);
		$report = array();
		foreach($postvars["sets"] as $setid)
		{
			$this->db->where('set_id', $setid);
			$this->db->delete('lsm_game_json_data');
			array_push($report,$this->db->_error_number());
		}
		
		if(!empty($report)){
			return true;
		}else{
			return false;
		}
	}
}
?>	
