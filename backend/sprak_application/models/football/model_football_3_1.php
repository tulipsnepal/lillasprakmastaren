<?php
class Model_football_3_1 extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

   	public function get_set_by_id($id)
    {
		$this->db->select('set_json_data,is_active');
		$this->db->from('lsm_game_json_data');
		$this->db->where('set_id', $id);
		$query = $this->db->get();
		$t =  $query->result_array();
		$json_data = json_decode($t[0]["set_json_data"],true);
		
		$set = array();
		
		for($k=0;$k<4;$k++){
			$set["names_word"][]= $this->utilities->name_details_by_id($json_data["names"][$k]);
			$set["first_word"][]= $this->utilities->word_details_by_id($json_data["first_word"][$k]);
			$set["second_word"][]= $this->utilities->word_details_by_id($json_data["second_word"][$k]);
			$set["word_article"][] = $json_data["second_word_article"][$k];
		}
		$set["is_active"] = $t[0]["is_active"];
		return $set;
    }
	
	public function get_items($exerciseid)
    {
		$this->db->select("*");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->order_by('set_id', "DESC");
		$query = $this->db->get();
		
		
		$a = 0;
		$array = array();
		foreach($query->result_array() as $results)
		{
			$set_data = json_decode($results["set_json_data"],true);
			
			for($k=0;$k<4;$k++){
				$array[$a]["names_word"][]= $this->utilities->name_details_by_id($set_data["names"][$k]);
				$array[$a]["first_word"][]= $this->utilities->word_details_by_id($set_data["first_word"][$k]);
				$array[$a]["second_word"][]= $this->utilities->word_details_by_id($set_data["second_word"][$k]);
				$array[$a]["word_article"][] = $set_data["second_word_article"][$k];
			}
			$array[$a]["is_active"] = $results["is_active"];
			$array[$a]["set_id"] = $results["set_id"];
			$a++;			
		}
		return $array;
    }

    
	function store_set($data)
    {
		$array = array();
		$array["names"] = $data["names_word"];
		$array["first_word"] = $data["first_word"];
		$array["second_word"] = $data["second_word"];
		$array["second_word_article"] = $data["word_article"];
		
		//$this->utilities->printr($data,1);
		$jsontosave = $this->utilities->json($array); 
		
		$datatostore = array("exercise_id"=>$data["exercise_id"],"set_json_data"=>$jsontosave,"is_active"=>$data["is_active"]);
		$insert = $this->db->insert('lsm_game_json_data', $datatostore);
	    return $insert;
	}

   
    function update_set($id, $data)
    {
		$array = array();
		$array["names"] = $data["names_word"];
		$array["first_word"] = $data["first_word"];
		$array["second_word"] = $data["second_word"];
		$array["second_word_article"] = $data["word_article"];

		$jsontosave = $this->utilities->json($array); 
		
		$datatostore = array("set_json_data"=>$jsontosave,"is_active"=>$data["is_active"]);
		$this->db->where('set_id', $id);
		$update = $this->db->update('lsm_game_json_data', $datatostore);
		return $update;
	}

   	/*
	Method to create JSON 
	*/
 	function create_json($exerciseid){
		$file_to_create = UPLOADS_DIR."exercises/football/football_3_1.json";
		$this->db->select("set_json_data");
		$this->db->from('lsm_game_json_data');
		$this->db->where('exercise_id', $exerciseid);
		$this->db->where("is_active",1);
		$query = $this->db->get();
		$t =  $query->result_array();
		$a = 0;
		
		if($t)
		{
			foreach($t as $o){
				$wordsArray = array();
				$imageArrays = array();
				$partArrays = array();

				$r  = json_decode($o["set_json_data"],true);

				//names
				foreach($r["names"] as $name){
					$char_name = $this->utilities->name_details_by_id($name);
					array_push($wordsArray,$char_name["word"]);
					array_push($partArrays,1);
					array_push($imageArrays,$char_name["imageRef"]);
				}

				//first word
				foreach($r["first_word"] as $first_word){
					$part_one_word = $this->utilities->word_details_by_id($first_word);
					array_push($wordsArray,$part_one_word["word"]);
					array_push($partArrays,2);
					array_push($imageArrays,$part_one_word["imageRef"]);
				}

				//second word
				for($k=0;$k<4;$k++)
				{
					$part_two_word = $this->utilities->word_details_by_id($r["second_word"][$k]);
					$article = $r["second_word_article"][$k];
					array_push($wordsArray,$article." ".$part_two_word["word"].".");
					array_push($partArrays,3);
					array_push($imageArrays,$part_two_word["imageRef"]);

				}
				
				$array[$a]["exercise_words"] = $wordsArray;
				$array[$a]["images"]=$imageArrays;
				$array[$a]["parts"]=$partArrays;
				
				$a++;
			}
			
			$content_type = 'application/json; charset=utf-8';
			header('Content-type: '.$content_type); 
			
			$fp = fopen($file_to_create, 'w');
			$write = fwrite($fp, $this->utilities->json($array)); 
			
			fclose($fp);
			return $write;
		}else{
			@unlink($file_to_create);
		}
	}
	
	//delete selected sets
	function deleteselected($postvars){
		$report = array();
		foreach($postvars["sets"] as $setid)
		{
			$this->db->where('set_id', $setid);
			$this->db->delete('lsm_game_json_data');
			array_push($report,$this->db->_error_number());
		}
		
		if(!empty($report)){
			return true;
		}else{
			return false;
		}
	}
}
?>	
