<?php
class Words_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

    /**
    * Get product by his is
    * @param int $product_id 
    * @return array
    */
    public function get_word_by_id($id)
    {
		$this->db->select('*');
		$this->db->from('lsm_game_words');
		$this->db->where('word_id', $id);
		$query = $this->db->get();
		$t =  $query->result_array();
		return $t[0]; 
    }
/*Method to clean up entry of missing audios in a word table*/
	function clean_broken_audios(){
		$this->db->select("word_id,audioRef");
		$this->db->from('lsm_game_words');
		$this->db->where("audioRef !=","");
		$query = $this->db->get();
		foreach($query->result_array() as $rs)
		{
			if(!is_file(GLOBAL_AUDIO_DIR.$rs["audioRef"].".mp3"))
			{
				$data = array("audioRef"=>"");
				$this->db->where('word_id', $rs["word_id"]);
				$this->db->update('lsm_game_words', $data);
			}
		}
		
	}
    
	
    public function get_words($search_string=null, $filter_by=null, $order_type='Asc', $limit_start, $limit_end,$show="all")
    {
	    $this->db->select('*');
		$this->db->from('lsm_game_words');
		
		if($search_string && trim($search_string)!=""){
			$this->db->like('word', $search_string);
		}

		if($filter_by && trim($filter_by)!="-1"){
			$where = "FIND_IN_SET('".urldecode($filter_by)."', tags)";  
			$this->db->where($where);
		}
		if($show=="noaudio"){
			$this->db->where("audioRef","");
		}
		
		$this->db->order_by("word",$order_type);


		$this->db->limit($limit_start, $limit_end);
		//$this->db->limit('4', '4');
		

		$query = $this->db->get();
		/*echo "SHOW = ".$show."<br>";*/
		//echo $this->db->last_query(); exit;
		
		return $query->result_array();
    }

	
    function count_words($show="all",$search_string=null, $filter_by=null)
    {
		
		$this->db->select('*');
		$this->db->from('lsm_game_words');
		
		if($search_string && trim($search_string)!=""){
			$this->db->like('word', $search_string);
		}

		if($filter_by && trim($filter_by)!="-1"){
			$where = "FIND_IN_SET('".urldecode($filter_by)."', tags)";  
			$this->db->where($where);
		}
		if($show=="noaudio"){
			$this->db->where("audioRef","");
		}
		
		
		$this->db->order_by('word', 'Asc');
		$query = $this->db->get();
		
		return $query->num_rows();
    }
	
	function get_tags_by_ids($ids)
	{
		$q = $this->db->query("SELECT GROUP_CONCAT(tag_name) AS tags FROM lsm_word_tags WHERE FIND_IN_SET(tag_id,'".$ids."')");
		$row = $q->row();
		return $row->tags;
	}
	
	function get_ids_by_tags($tags)
	{
		$q = $this->db->query("SELECT GROUP_CONCAT(tag_id) AS tags FROM lsm_word_tags WHERE FIND_IN_SET(tag_name,'".$tags."')");
		$row = $q->row();
		return $row->tags;
	}
	
    /**
    * Store the new item into the database
    * @param array $data - associative array with data to store
    * @return boolean 
    */
    function store_word($data)
    {
		$insert = $this->db->insert('lsm_game_words', $data);
	    return $insert;
	}

    /**
    * Update product
    * @param array $data - associative array with data to store
    * @return boolean
    */
    function update_word($id, $data)
    {
		$this->db->where('word_id', $id);
		$this->db->update('lsm_game_words', $data);
		//echo $this->db->last_query();exit;
		$report = array();
		$report['error'] = $this->db->_error_number();
		$report['message'] = $this->db->_error_message();
		if($report !== 0){
			return true;
		}else{
			return false;
		}
	}

    /**
    * Delete item
    * @param int $id - item id
    * @return boolean
    */
	function delete_word($id){
		$data = array("word_status"=>0);
		$this->db->where('word_id', $id);
		$this->db->update('lsm_game_words', $data);
	}
	
	 /**
    * Undelete item
    * @param int $id - item id
    * @return boolean
    */
	function undelete_word($id){
		$data = array("word_status"=>1);
		$this->db->where('word_id', $id);
		$this->db->update('lsm_game_words', $data);
	}
 	
	/**
	* Fetch all images from a given folder
	* @Param string - the folder path
	* @Return array or string - if images exists returns array of the filename, otherwise string
	*/
	public function get_images_in_folder($folder,$selectedimg=""){
		$img = array();
		
		if(is_dir($folder))
		{
			$dirarray = array_diff(scandir($folder), array('..', '.'));
			$dirarray =  array_values($dirarray);
			foreach($dirarray as $files){
				$pinfo = pathinfo($files);
				if(strtolower($pinfo["extension"])=="png") //return only png images
				{
					array_push($img,$files);
				}
			}
			
			
			//in edit mode always show selected image at first
			if($selectedimg!=""){
				usort($img, function ($a, $b) use ($selectedimg) {
					if ($a != $selectedimg && $b == $selectedimg) {
						return 1;
					} elseif ($a == $selectedimg && $b != $selectedimg) {
						return -1;
					} else {
						return 0;
					}
				});
			}
			return $img;
		}else{
			return "Images Not Available";
		}
	}
	
	/**
	* Fetch all audios from a given folder
	* @Param string - the folder path
	* @Return array or string - if audios exists returns array of the filename, otherwise string
	*/
	public function get_audios_in_folder($folder,$selectedaudio=""){
		$audio = array();
		
		if(is_dir($folder))
		{
			$dirarray = array_diff(scandir($folder), array('..', '.'));
			$dirarray =  array_values($dirarray);
			foreach($dirarray as $files){
				$pinfo = pathinfo($files);
				
				if(strtolower($pinfo["extension"])=="mp3") //return only mp3 audios
				{
					array_push($audio,$files);
				}
			}
			
			//in edit mode always show selected audio at first
			if($selectedaudio!=""){
				usort($audio, function ($a, $b) use ($selectedaudio) {
					if ($a != $selectedaudio && $b == $selectedaudio) {
						return 1;
					} elseif ($a == $selectedaudio && $b != $selectedaudio) {
						return -1;
					} else {
						return 0;
					}
				});
			}
			return $audio;
		}else{
			return "Audio Not Available";
		}
	}
	
	/*Method to link tags with words*/
	function link_tags($postdata){
		$report = array();
		
		foreach($postdata as $key=>$value)
		{
			$tags = implode(",",$value);
			$data = array("tags"=>$tags);
			$this->db->where('word_id', $key);
			$this->db->update('lsm_game_words', $data);
			if($this->db->_error_number()!=0)
			{
				array_push($report,$this->db->_error_number());
			}
		}
		
		if(empty($report)){
			return true;
		}else{
			return false;
		}
	}
	
	function save_tags($postvar){
		$report = array();
		$selectedtags = $postvar["addnewtags"];
		
		foreach($postvar["selected_tags"] as $wordid)
		{
			$this->db->select('tags');
			$this->db->from('lsm_game_words');
			$this->db->where('word_id', $wordid);
			$query = $this->db->get();
			$t =  $query->result_array();
			$existingtags = explode(",",$t[0]["tags"]);
			$diff = array_merge($existingtags,$selectedtags);
			
			$newtags = implode(",",array_unique($diff));
				$data = array("tags"=>$newtags);
				$this->db->where('word_id', $wordid);
				$this->db->update('lsm_game_words', $data);
				if($this->db->_error_number()!=0)
				{
					array_push($report,$this->db->_error_number());
				}
				//array_push($report,$newtags);
			
		}
		//exit;
		//echo "<pre>".print_r($report,true)."</pre>"; exit;
		if(empty($report)){
			return true;
		}else{
			return false;
		}
	}
	
	function bulkaction($postvars){
		$report = array();
		$wordstodelete = $postvars["words_delete"];
		foreach($wordstodelete as $del)
		{	
			//SELECT IMAGE AND AUDIO TO DELETE AS WELL
			if($postvars["bulkaction"]==2)
			{
				$this->db->select('imageRef,audioRef');
				$this->db->from('lsm_game_words');
				$this->db->where('word_id', $del);
				$query = $this->db->get();
				$t =  $query->result_array();
				$image = $t[0]["imageRef"];
				$audio = $t[0]["audioRef"];
				
				//NOW DELETE WORD
				$this->db->where("word_id",$del);
				$this->db->delete('lsm_game_words');
				
				if($this->db->_error_number()!=0)
				{
					array_push($report,$this->db->_error_number());
				}else{
					//WORD DELETE SUCCESSFUL, SO DELETE ASSOCIATED IMAGE AND AUDIO AS WELL
					@unlink(GLOBAL_IMG_DIR.$image.".png");
					@unlink(GLOBAL_AUDIO_DIR.$audio.".mp3");
				}
			}else{
				$this->db->where("word_id",$del);
				$this->db->update("lsm_game_words",array("word_status"=>$postvars["bulkaction"]));
				if($this->db->_error_number()!=0)
				{
					array_push($report,$this->db->_error_number());
				}
			}
		}
		if(empty($report)){
			return true;
		}else{
			return false;
		}
	}
	
	public function get_orphaned_audios(){
		$this->load->helper('directory');
       	$map = directory_map(GLOBAL_AUDIO_DIR, 1);
		array_multisort($map);
		$arrayFile = array();
		$a = 0;
        foreach($map as $file){
			$pinfo = pathinfo($file);
			$this->db->select("audioRef");
			$this->db->from('lsm_game_words');
			$this->db->where('audioRef',$pinfo["filename"]);
			$this->db->where("audioRef !=","");
			$query = $this->db->get();
			
			$t =  $query->result_array();
			
			
			if(!count($t)>0){
				$arrayFile[$a]["filename"] = $file;
				$display_name = to_utf8($file);
				$arrayFile[$a]["displayname"] = $display_name;
				$p = pathinfo($display_name);
				$q = $this->db->query("SELECT GROUP_CONCAT(word) AS similar_words FROM lsm_game_words WHERE  SOUNDEX(word) = SOUNDEX('".$p["filename"]."')");
				$row = $q->row();
				$arrayFile[$a]["similar_word"] = $row->similar_words;
				$a++;
			}
		}
		return $arrayFile;
	}
	
	public function delete_audios($data)
	{
		$a = 0;
		foreach($data as $delaudio)
		{
			$del = @unlink(GLOBAL_AUDIO_DIR.$delaudio);
			if($del){
				$a++;
			}
		}
		return $a;
	}

	/*
	** Upload words in bulk which doesnot have images
	*/
	public function upload_words_only($data){
		$exploded_words = explode("##",$data["bulk_no_image_words"]);
		foreach($exploded_words as $word)
		{
			$word = trim(preg_replace('/\s+/', ' ', $word)); //remove multiple spaces into one in a word/phrase
			//insert only if the word doesnot exist
			$this->db->select('*');
			$this->db->from('lsm_game_words');
			$this->db->where("word",$word);
			$query = $this->db->get();
			$row = $query->result_array();
			$count =  $query->num_rows();
			if(!$count>0)
			{
				$data_to_store = array(
					'word' => $word,
					'has_image' => 0,
					'word_status' => 1
				);
				$this->db->insert('lsm_game_words', $data_to_store);
			}
		}
		return true;
	}
}
?>	
