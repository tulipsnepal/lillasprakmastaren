<?php
class Exercise_model extends CI_Model {

    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

   	public function get_exercise_by_id($id)
    {
		$this->db->select('*');
		$this->db->from('lsm_exercises');
		$this->db->where('exercise_id', $id);
		$query = $this->db->get();
		$t =  $query->result_array();
		return $t[0];
    }

    public function create_json(){
    	$file_to_create = UPLOADS_DIR."exercises/global_help.json";
    	$this->db->select('description,help_text_identifier,exercise_identifier');
		$this->db->from('lsm_exercises');
		$this->db->order_by('main_exercise', "ASC");
		$query = $this->db->get();
		$array = array();
		foreach($query->result_array() as $results)
		{
			$array[$results["help_text_identifier"]]["help_text"] = $results["description"];
			$array[$results["help_text_identifier"]]["title"] = $results["exercise_identifier"];
		}

		//$this->utilities->printr($array,1);

		$content_type = 'application/json; charset=utf-8';
		header('Content-type: '.$content_type);

		$fp = fopen($file_to_create, 'w');
		$write = fwrite($fp,$this->utilities->json($array));
		fclose($fp);
		return $write;
    }


    public function get_exercises()
    {

		$this->db->select('*');
		$this->db->from('lsm_exercises');
		$this->db->order_by('main_exercise', "ASC");
		$this->db->where('status', 1);
		$query = $this->db->get();

		return $query->result_array();
    }


	function store_exercise($data)
    {
		$insert = $this->db->insert('lsm_exercises', $data);
	    return $insert;
	}


    function update_exercise($id, $data)
    {
		$this->db->where('exercise_id', $id);
		$this->db->update('lsm_exercises', $data);
		$report = array();
		$report['error'] = $this->db->_error_number();
		$report['message'] = $this->db->_error_message();
		if($report !== 0){
			return true;
		}else{
			return false;
		}
	}




}
?>
