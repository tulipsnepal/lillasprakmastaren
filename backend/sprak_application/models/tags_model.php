<?php
class Tags_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

  	public function fetch_tags()
	{
		$this->db->select("*");
		$this->db->from('lsm_word_tags');
		$query = $this->db->get();
		$t =  $query->result_array();
		return $t;
	}
	
	public function get_linked_tags()
	{
			$query = $this->db->query("SELECT tags FROM lsm_game_words WHERE tags!='' and tags is not null");
			$arrayTagIDs = array();
			foreach ($query->result() as $row)
			{
				$explode_comma_separated_tags = explode(",",$row->tags);
				for($j=0;$j<count($explode_comma_separated_tags);$j++)
				{
					array_push($arrayTagIDs,$explode_comma_separated_tags[$j]);
				}
			}
			
			$ret_tags = array_unique($arrayTagIDs);
			return $ret_tags;
	}
	
	public function get_tag_by_id($id)
    {
		$this->db->select('tag_name,is_active');
		$this->db->from('lsm_word_tags');
		$this->db->where('tag_id', $id);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		$t =  $query->result_array();
		return $t[0];
    }
	
	
	function store_set($data)
    {
		$report = array();
		//user might enter comma separated tags to enter multiple tags, lets explode the entry to loop through each tag.
		$tagsarray = explode(",",$data["tag_name"]); 
		for($i=0;$i<count($tagsarray);$i++)
		{
			$datatostore = array("tag_name"=>trim($tagsarray[$i]),"is_active"=>$data["is_active"]);
			$insert = $this->db->insert('lsm_word_tags', $datatostore);
			array_push($report,$this->db->_error_number());
		}
		if(!empty($report)){
			return true;
		}else{
			return false;
		}
	}
	
	
   
    function update_set($id, $data)
    {
		$datatostore = array("tag_name"=>$data["tag_name"],"is_active"=>$data["is_active"]);
		$this->db->where('tag_id', $id);
		$update = $this->db->update('lsm_word_tags', $datatostore);
		return $update;
	}
	
	
	//delete selected tags
	function bulkaction($postvars){
		$report = array();
		foreach($postvars["selected_tags"] as $tagid)
		{
			if($postvars["bulkaction"]==2)
			{
				$this->db->where('tag_id', $tagid);
				$this->db->delete('lsm_word_tags');
			}else{
				$this->db->where("tag_id",$tagid);
				$this->db->update("lsm_word_tags",array("is_active"=>$postvars["bulkaction"]));
			}
			array_push($report,$this->db->_error_number());
		}
		
		if(!empty($report)){
			return true;
		}else{
			return false;
		}
	}
}
?>	
