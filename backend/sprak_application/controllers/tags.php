<?php
class Tags extends CI_Controller {
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
		
        $this->load->model('tags_model');

        if(!$this->session->userdata('is_logged_in')){
            redirect('login');
        }
    }
 
 	
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index()
    {
		$data["tags"] = $this->tags_model->fetch_tags();
		$data["used_tags"] = $this->tags_model->get_linked_tags();
		$data['main_content'] = 'tags/list';
        $this->load->view('includes/template', $data);
    }//index
	
	
	public function add()
    {
		
		//if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
			
			//form validation
			$this->form_validation->set_rules('tag_name', 'Tag Name', 'required|is_unique[lsm_word_tags.tag_name]');
			$this->form_validation->set_rules('is_active', 'Set Status', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
			
			
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
				$data_to_store = $this->input->post();
               
                if($this->tags_model->store_set($data_to_store)){
                    $this->session->set_flashdata('flash_message', "Records saved successfully");
                }else{
                     $this->session->set_flashdata('flash_message', "not_done");
                }
				redirect("tags");
            }

        }
        //fetch manufactures data to populate the select field
        
        //load the view
		$data["action"] = "Add";
		$data["actioning"] 	= "Adding";
		$data["action_done"] = "inserted";
        $data['main_content'] = 'tags/add';
		$this->load->view('includes/template', $data);  
    }  
	

    /**
    * Update item by his id
    * @return void
    */
    public function update()
    {
        //product id 
        $id = $this->uri->segment(3);
		
         //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

			//form validation
			$this->form_validation->set_rules('tag_name', 'Tag Name', 'required');
			$this->form_validation->set_rules('is_active', 'Set Status', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
				$data_to_store = $this->input->post();
                if($this->tags_model->update_set($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', "Records updated  successfully");
                }else{
                    $this->session->set_flashdata('flash_message', "not_done");
                }
                redirect('tags');

            }

        }
        //fetch manufactures data to populate the select field
        
        //load the view
		    
		$data['tags'] = $this->tags_model->get_tag_by_id($id);
		$data["action"] = "Update";
		$data["actioning"] 	= "Updating";
		$data["action_done"] = "updated";
        $data['main_content'] = 'tags/add';
		$this->load->view('includes/template', $data);;    

    }//update
	
	//method to delete selected sets
	public function bulkaction(){
		$result = $this->tags_model->bulkaction($this->input->post());
		if($result){
			$action = ($this->input->post("bulkaction")==2)?"deleted":($this->input->post("bulkaction")==1?"made active":"made inactive");
			$this->session->set_flashdata('flash_message', "Selected record $action successfully.");
		}else{
			$this->session->set_flashdata('flash_message', "not_done");
		}
		redirect('tags');
	}
	
	
	public function save()
	{
		//echo $this->utilities->printr($this->input->post(),1);
		if($this->tags_model->save($this->input->post())){
			 $this->session->set_flashdata('flash_message', "done");
		}else{
			 $this->session->set_flashdata('flash_message', "not_done");
		}
		redirect("settings");
	}
	
	

}