<?php
class Exercise extends CI_Controller {

    /**
    * name of the folder responsible for the views 
    * which are manipulated by this controller
    * @constant string
    */
    const VIEW_FOLDER = 'exercise';
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('exercise_model');
		if(!$this->session->userdata('is_logged_in')){
            redirect('login');
        }
    }


    /*Method to fetch exercise description and create JSON. This is shown in the help box during the game*/
    public function createJSON(){
    	$createjson = $this->exercise_model->create_json();
		
		//$this->utilities->printr($createjson,1);
		if($createjson){ 
			$this->session->set_flashdata('flash_message', "JSON file created successfully.");
		}else{
			$this->session->set_flashdata('flash_message', "not_done");
		}
		redirect('exercise');
    }
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index()
    {
		$data['order'] = 'id';

		$data['exercises'] = $this->exercise_model->get_exercises();        
		
		$data['main_content'] = 'exercise/list';
        $this->load->view('includes/template', $data);  

    }//index

	public function main(){
		$this->index();
	}
	
    public function add()
    {
		//if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

			//form validation 
			$this->form_validation->set_rules('main_exercise', 'Main Exercise', 'required');
            $this->form_validation->set_rules('sub_exercise', 'Sub Exercise', 'required');
			$this->form_validation->set_rules('help_text_identifier', 'Help Text Identifier', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
				$screenshot = "";
				if(strlen($_FILES["exercise_screenshot"]["name"])>0){
					$config['upload_path'] = EXTRA_IMG_DIR;
					$config['allowed_types'] = 'jpg|jpeg|gif|png';
					$config['max_size'] = '6048';
					$config['remove_spaces'] = true;
					$config['file_name'] = iconv("utf-8", "cp1252", $_FILES["word_image"]["name"]);
					$config['overwrite'] = TRUE;
					$this->load->library('upload');
					$this->upload->initialize($config);
					if(!$this->upload->do_upload("exercise_screenshot"))//Check if upload is unsuccessful
					{
						$upload_errors = $this->upload->display_errors('', '');
						$this->session->set_flashdata('errors', 'Error: '.$upload_errors);   
						redirect('exercise/add');  
					}else{
						$screenshot = $_FILES["exercise_screenshot"]["name"];
					}
				}
				
				$data_to_store = array(
                    'main_exercise' => $this->input->post('main_exercise'),
					'sub_exercise' => $this->input->post('sub_exercise'),
					'exercise_identifier' => $this->input->post('exercise_identifier'),
					'help_text_identifier' => $this->input->post('help_text_identifier'),
					'exercise_screenshot' => $screenshot,
					'description' => $this->input->post('description'),
					'url_link' => $this->input->post('url_link')
                );
                //if the insert has returned true then we show the flash message
				//ini_set("display_errors",1); error_reporting(E_ALL);
                if($this->exercise_model->store_exercise($data_to_store)){
                	 $this->exercise_model->create_json();
                     $this->session->set_flashdata('flash_message', "done");
                }else{
                     $this->session->set_flashdata('flash_message', "not_done");
                }
				redirect("exercise");
            }

        }
        //fetch manufactures data to populate the select field
        
        //load the view
		$data["action"] = "Add";
		$data["actioning"] 	= "Adding";
		$data["action_done"] = "inserted";
        $data['main_content'] = 'exercise/add';
        $this->load->view('includes/template', $data);  
    }       

    /**
    * Update item by his id
    * @return void
    */
    public function update()
    {
        //product id 
        $id = $this->uri->segment(3);

  
         //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

			
            //form validation
			$this->form_validation->set_rules('main_exercise', 'Main Exercise', 'required');
            $this->form_validation->set_rules('sub_exercise', 'Sub Exercise', 'required');
			$this->form_validation->set_rules('help_text_identifier', 'Help Text Identifier', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
				$screenshot = "";
				if(strlen($_FILES["exercise_screenshot"]["name"])>0){
					$config['upload_path'] = EXTRA_IMG_DIR;
					$config['allowed_types'] = 'jpg|jpeg|gif|png';
					$config['max_size'] = '6048';
					$config['remove_spaces'] = true;
					$config['file_name'] = iconv("utf-8", "cp1252", $_FILES["word_image"]["name"]);
					$config['overwrite'] = TRUE;
					$this->load->library('upload');
					$this->upload->initialize($config);
					if(!$this->upload->do_upload("exercise_screenshot"))//Check if upload is unsuccessful
					{
						$upload_errors = $this->upload->display_errors('', '');
						$this->session->set_flashdata('errors', 'Error: '.$upload_errors);   
						redirect('exercise/update/'.$id);  
					}else{
						$screenshot = $_FILES["exercise_screenshot"]["name"];
					}
				}else{
					$screenshot = $this->input->post('hid_exercise_screenshot');
				}
				
				$data_to_store = array(
                    'main_exercise' => $this->input->post('main_exercise'),
					'sub_exercise' => $this->input->post('sub_exercise'),
					'exercise_identifier' => $this->input->post('exercise_identifier'),
					'help_text_identifier' => $this->input->post('help_text_identifier'),
					'exercise_screenshot' => $screenshot,
					'description' => $this->input->post('description'),
					'url_link' => $this->input->post('url_link')
                );
				                
                //if the insert has returned true then we show the flash message
				//ini_set("display_errors",1); error_reporting(E_ALL);
                 if($this->exercise_model->update_exercise($id, $data_to_store) == TRUE){
                 	$this->exercise_model->create_json();
                    $this->session->set_flashdata('flash_message', "done");
                }else{
                    $this->session->set_flashdata('flash_message', "not_done");
                }
                redirect('exercise');

            }

        }
        //fetch manufactures data to populate the select field
        
        //load the view
		$data["action"] = "Update";
		$data["actioning"] = "Updating";
		$data["action_done"] = "updated";
		$data['exercises'] = $this->exercise_model->get_exercise_by_id($id);
		
		$data['main_content'] = 'exercise/add';
        $this->load->view('includes/template', $data);       

    }//update

}