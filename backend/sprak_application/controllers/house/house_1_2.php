<?php
class House_1_2 extends CI_Controller {

   
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('house/model_house_1_2',"model_house");
		if(!$this->session->userdata('is_logged_in')){
            redirect('login');
        }
    }
	
	
	/*
	Method to convert Uppercase alphabets to lower
	*/
	public function ajaxtolower()
	{
		if($this->input->post())
		{
			 $UCAlphabets = $this->input->post("uc_alphabets");
			$UCArrayAlphas = explode(",",$UCAlphabets);
			$LCArrayAlphas = array();
			foreach($UCArrayAlphas as $uc_alpha){
				$LCArrayAlphas[] = mb_strtolower($uc_alpha,"UTF-8");
			}
			echo implode(",",$LCArrayAlphas);
			exit;
		}
	}
	
	
	/*
	Method to create JSON of the Tivoli rhyme that is to be used by game
	*/
	public function json($exerciseid)
	{
		

		$createjson = $this->model_house->create_json($exerciseid);
		//$this->utilities->printr($createjson,1);
		if($createjson){ 
			$this->session->set_flashdata('flash_message', "JSON file created successfully.");
		}else{
			$this->session->set_flashdata('flash_message', "not_done");
		}
		redirect('house/house_1_2');
	}
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index()
    {
		$data['order'] = 'id';
		$data["exercise_desc"] = $this->utilities->get_exercise_desc(2);
		$data['set'] = $this->model_house->get_items($data["exercise_desc"][2]);        
		
		$data['main_content'] = 'house/house_1_2/list';
        $this->load->view('includes/template', $data);  

    }//index

	public function main(){
		$this->index();
	}
	
    public function add()
    {
		$data["exercise_desc"] = $this->utilities->get_exercise_desc(2);
		//if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
			
			//form validation
			$this->form_validation->set_rules('alphabet_uppercase', 'Option Sentences', 'required');
			$this->form_validation->set_rules('alphabet_lowercase', 'Correct Sentence', 'required');
			$this->form_validation->set_rules('is_active', 'Set Status', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
			
			
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
				$data_to_store = $this->input->post();
                //if the insert has returned true then we show the flash message
				//ini_set("display_errors",1); error_reporting(E_ALL);
                if($this->model_house->store_set($data_to_store)){
                     $this->model_house->create_json($data["exercise_desc"][2]);
                    $this->session->set_flashdata('flash_message', "Records saved successfully");
                }else{
                     $this->session->set_flashdata('flash_message', "not_done");
                }
				redirect("house/house_1_2");
            }

        }
        //fetch manufactures data to populate the select field
        
        //load the view
		
		$data["action"] = "Add";
		$data["actioning"] 	= "Adding";
		$data["action_done"] = "inserted";
        $data['main_content'] = 'house/house_1_2/add';
		
        $this->load->view('includes/template', $data);  
    }  
	

    /**
    * Update item by his id
    * @return void
    */
    public function update()
    {
        //product id 
        $id = $this->uri->segment(4);
		$data["exercise_desc"] = $this->utilities->get_exercise_desc(2);

  
         //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

			//form validation
			$this->form_validation->set_rules('alphabet_uppercase', 'Option Sentences', 'required');
			$this->form_validation->set_rules('alphabet_lowercase', 'Correct Sentence', 'required');
			$this->form_validation->set_rules('is_active', 'Set Status', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
				$data_to_store = $this->input->post();         
                //if the insert has returned true then we show the flash message
				//ini_set("display_errors",1); error_reporting(E_ALL);
                 if($this->model_house->update_set($id, $data_to_store) == TRUE){
                   $this->model_house->create_json($data["exercise_desc"][2]);
                    $this->session->set_flashdata('flash_message', "Records saved successfully");
                }else{
                    $this->session->set_flashdata('flash_message', "not_done");
                }
                redirect('house/house_1_2');

            }

        }
        //fetch manufactures data to populate the select field
        
        //load the view
		    
		$data['set'] = $this->model_house->get_set_by_id($id);
		$data["action"] = "Update";
		$data["actioning"] 	= "Updating";
		$data["action_done"] = "updated";
        $data['main_content'] = 'house/house_1_2/add';
		$this->load->view('includes/template', $data);;    

    }//update
	
	//method to delete selected sets
	public function deleteselected(){
		$result = $this->model_house->deleteselected($this->input->post());
		//$this->utilities->printr($createjson,1);
		if($result){ 
			$exerciseid = $this->utilities->get_exercise_desc(2);
			$this->model_house->create_json($exerciseid[2]);
			$this->session->set_flashdata('flash_message', "Selected record deleted successfully.");
		}else{
			$this->session->set_flashdata('flash_message', "not_done");
		}
		redirect('house/house_1_2');
	}
}