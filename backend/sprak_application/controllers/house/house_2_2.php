<?php
class House_2_2 extends CI_Controller {

   
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('house/model_house_2_2',"model_house");
		if(!$this->session->userdata('is_logged_in')){
            redirect('login');
        }
    }
	
	/*
	method to load word checkboxes via ajax
	*/
	public function loadWords($type="all",$keywords="A"){
		$keywords = urldecode($keywords);
		$results = $this->utilities->list_all_words($type,$keywords,"col-xs-25");
		echo $results;
		exit;
	}
	
	/*
	Method to create JSON of the House rhyme that is to be used by game
	*/
	public function json($exerciseid)
	{
		
		$createjson = $this->model_house->create_json($exerciseid);
		//$this->utilities->printr($createjson,1);
		if($createjson){ 
			$this->session->set_flashdata('flash_message', "JSON file created successfully.");
		}else{
			$this->session->set_flashdata('flash_message', "not_done");
		}
		redirect('house/house_2_2');
	}
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index()
    {
		$data['order'] = 'id';
		$data["exercise_desc"] = $this->utilities->get_exercise_desc(2);
		$data["set"] = $this->model_house->get_items($data["exercise_desc"][2]);
		$data['main_content'] = 'house/house_2_2/list';
        $this->load->view('includes/template', $data);  

    }//index

	public function main(){
		$this->index();
	}
	
    public function add()
    {
		$data["exercise_desc"] = $this->utilities->get_exercise_desc(2);
		//if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
			//form validation
			$this->form_validation->set_rules('selected_mainwords[]', 'Words', 'callback__check2boxes');
			$this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
			
			
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
				$data_to_store = $this->input->post();
                //if the insert has returned true then we show the flash message
				//ini_set("display_errors",1); error_reporting(E_ALL);
                if($this->model_house->store_set($data_to_store)){
                     $this->model_house->create_json($data["exercise_desc"][2]);
                    $this->session->set_flashdata('flash_message', "Records saved successfully");
                }else{
                     $this->session->set_flashdata('flash_message', "not_done");
                }
				redirect("house/house_2_2");
            }

        }
        //fetch manufactures data to populate the select field
        
        //load the view
		$data["set"] = $this->model_house->get_words($data["exercise_desc"][2]);
		$data["tags"] = $this->utilities->fetch_tags();
		//$data["words"] = $this->utilities->fetch_all_words();
		$data["action"] = "Add";
		$data["actioning"] 	= "Adding";
		$data["action_done"] = "inserted";
        $data['main_content'] = 'house/house_2_2/add';
		$data["gen_alphabets"] = $this->utilities->generate_alphabets("loadcharwords",true,true);
        $this->load->view('includes/template', $data);  
    }  
	
	/*Method to check if checkbox are checked*/
	public function _check2boxes($str)
	{
		if(!count($this->input->post("selected_mainwords"))>0)
		{
			$this->form_validation->set_message('_check2boxes', 'You must select the words for the set.');
			return FALSE;
		}else{
			return TRUE;
		}
	}     


}