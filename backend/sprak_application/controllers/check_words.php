<?php
class check_words extends CI_Controller 
{
	public function __construct(){
		parent::__construct();
		echo "Missing words <br><br>";
		$this->load->database();
		$array= array('Ada',
'Adam',
'Ali',
'Amira',
'Ann',
'Anton',
'Bea',
'Cesar',
'David',
'Dina',
'Dora',
'Elsa',
'Eva',
'Fabian',
'Fatima',
'Fia',
'Hanna',
'Hassan',
'Ida',
'Ines',
'Jan',
'Jennifer',
'Jesper',
'Jim',
'Josef',
'Karin',
'Kia',
'Kim',
'Lea',
'Leila',
'Leo',
'Liam',
'Lisa',
'Maria',
'Mia',
'Moa',
'Måns',
'Nona',
'Nora',
'Ola',
'Oskar',
'Pia',
'Pål',
'Pär',
'Rafael',
'Rod',
'Rosy',
'Rune',
'Sam',
'Sara',
'Soran',
'Sune',
'Tanja',
'Tim',
'Tina',
'Tom',
'Tore',
'Tyra',
'Ulf',
'Viktor');

		$array = array_unique($array);
		$array = array_values($array);
		//$this->utilities->printr($array);
		for($i=0;$i<count($array);$i++)
		{

			$this->db->select("word");
			$this->db->from('lsm_character_names');
			$this->db->where('word', trim($array[$i]));
			$query = $this->db->get();
			$t =  $query->result_array();
			if(!$t)
			{
				echo $array[$i]."<br>";
			}
		}
	}
}
?>