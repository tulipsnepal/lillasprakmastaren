<?php
class Admin_users extends CI_Controller {
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
		
        $this->load->model('admin_users_model');

        if(!$this->session->userdata('is_logged_in')){
            redirect('login');
        }
    }
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index()
    {

        //all the posts sent by the view
        $search_string = $this->input->post('search_string');        
        $order = $this->input->post('order'); 
        $order_type = $this->input->post('order_type'); 
		
		 //limit end
        $page = $this->uri->segment(2);
		$page = $page>0?$page:0;
		
        //pagination settings
		$config['uri_segment'] = 2;
        $config['per_page'] = RECORDS_PER_PAGES;
        $config['base_url'] = base_url().'words';
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 20;
        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';

       

        //math to get the initial record to be select in the database
        $limit_end = ($page * $config['per_page']) - $config['per_page'];
        if ($limit_end < 0){
            $limit_end = 0;
        } 

        //if order type was changed
        if($order_type){
            $filter_session_data['order_type'] = $order_type;
        }
        else{
            //we have something stored in the session? 
            if($this->session->userdata('order_type')){
                $order_type = $this->session->userdata('order_type');    
            }else{
                //if we have nothing inside session, so it's the default "Asc"
                $order_type = 'Asc';    
            }
        }
        //make the data type var avaible to our view
        $data['order_type_selected'] = $order_type;        


		//pre selected options
		$data['order'] = 'id';

		//fetch sql data into arrays
		$data['count_users']= $this->admin_users_model->count_users();
		$data['users'] = $this->admin_users_model->get_users();        
		$config['total_rows'] = $data['count_users'];

        //!isset($manufacture_id) && !isset($search_string) && !isset($order)

        //initializate the panination helper 
		
      

	
        //load the view
		
		
		
        $data['main_content'] = 'users/list';
        $this->load->view('includes/template', $data);  

    }//index

	
	
    public function add()
    {
		
	
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

			
            //form validation
			$this->form_validation->set_rules('first_name', 'First Name', 'required');
            $this->form_validation->set_rules('first_name', 'Last Name', 'required');
			$this->form_validation->set_rules('email_address', 'Email Address', 'required|valid_email');
			$this->form_validation->set_rules('user_name', 'User Name', 'trim|required');
			$this->form_validation->set_rules('pass_word', 'Password', 'trim|required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
				
                $data_to_store = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'email_addres' => $this->input->post('email_address'),
					'user_name' => $this->input->post('user_name'),
					'pass_word' => md5($this->input->post('pass_word')),
                    'user_status' => $this->input->post("user_status")
                );
                //if the insert has returned true then we show the flash message
				//ini_set("display_errors",1); error_reporting(E_ALL);
                if($this->admin_users_model->store_users($data_to_store)){
                     $this->session->set_flashdata('flash_message', "done");
					 redirect("users");
                }else{
                     $this->session->set_flashdata('flash_message', "not_done");
					  $this->session->set_flashdata('errors', "Username already exist. Specify different one.");
					 redirect("users");
                }
				

            }

        }
        //fetch manufactures data to populate the select field
        
        //load the view
		$data["action"] = "Add";
		$data["actioning"] = "Adding";
		$data["action_done"] = "inserted";
		
        $data['main_content'] = 'users/add';
        $this->load->view('includes/template', $data);  
    }       

    /**
    * Update item by his id
    * @return void
    */
    public function update()
    {
        //product id 
        $id = $this->uri->segment(3);
		
  
          //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

			
            //form validation
			$this->form_validation->set_rules('first_name', 'First Name', 'required');
            $this->form_validation->set_rules('first_name', 'Last Name', 'required');
			$this->form_validation->set_rules('email_address', 'Email Address', 'required|valid_email');
			$this->form_validation->set_rules('user_name', 'User Name', 'trim|required');
			$this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
				if(trim($this->input->post('pass_word'))=="")
				{
					$data_to_store = array(
						'first_name' => $this->input->post('first_name'),
						'last_name' => $this->input->post('last_name'),
						'email_addres' => $this->input->post('email_address'),
						'user_status' => $this->input->post("user_status")
					);

				}else{
					$data_to_store = array(
						'first_name' => $this->input->post('first_name'),
						'last_name' => $this->input->post('last_name'),
						'email_addres' => $this->input->post('email_address'),
						'pass_word' => md5($this->input->post('pass_word')),
						'user_status' => $this->input->post("user_status")
					);
				}
                //if the insert has returned true then we show the flash message
				//ini_set("display_errors",1); error_reporting(E_ALL);
                if($this->admin_users_model->update_users($id,$data_to_store)){
                     $this->session->set_flashdata('flash_message', "done");
                }else{
                     $this->session->set_flashdata('flash_message', "not_done");
                }
				 redirect("users");
				

            }

        }
        //fetch manufactures data to populate the select field
        
        //load the view
		$data["action"] = "Update";
		$data["actioning"] = "Updating";
		$data["action_done"] = "updated";
		$data['users'] = $this->admin_users_model->get_user_by_id($id);
		
	    $data['main_content'] = 'users/add';
        $this->load->view('includes/template', $data);       

    }//update

    /**
    * Delete product by his id
    * @return void
    */
    public function delete()
    {
        $id = $this->uri->segment(4);
        $this->admin_users_model->delete_product($id);
        redirect('words');
    }//edit

}