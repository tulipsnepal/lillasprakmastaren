<?php
class Football_1_2 extends CI_Controller {

   
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('football/model_football_1_2',"model_football");
		if(!$this->session->userdata('is_logged_in')){
            redirect('login');
        }
    }
	
	/*
	method to load word checkboxes via ajax
	*/
	public function loadWords($type="all",$keywords="A"){
		$keywords = urldecode($keywords);
		$results = $this->utilities->list_all_words($type,$keywords,"col-xs-25");
		echo $results;
		exit;
	}
	
	/*
	Method to create JSON of the Football rhyme that is to be used by game
	*/
	public function json($exerciseid)
	{
		

		$createjson = $this->model_football->create_json($exerciseid);
		//$this->utilities->printr($createjson,1);
		if($createjson){ 
			$this->session->set_flashdata('flash_message', "JSON file created successfully.");
		}else{
			$this->session->set_flashdata('flash_message', "not_done");
		}
		redirect('football/football_1_2');
	}
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index()
    {
		$data['order'] = 'id';
		$data["exercise_desc"] =  $this->utilities->get_exercise_desc(2);
		$data["set"] = $this->model_football->get_items($data["exercise_desc"][2]);        
		
		$data['main_content'] = 'football/football_1_2/list';
        $this->load->view('includes/template', $data);  

    }//index

	public function main(){
		$this->index();
	}
	
    public function add()
    {
		$data["exercise_desc"] =  $this->utilities->get_exercise_desc(2);
		//if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
			//form validation
			$this->form_validation->set_rules('selected_mainwords[]', 'Words', 'required|callback__check2boxes[selected_mainwords]');
			$this->form_validation->set_rules('option_sentences[]', 'Exercise sentences', 'required|callback__check2boxes[option_sentences]');
			$this->form_validation->set_rules('is_active', 'Set Status', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
			
			
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
				$data_to_store = $this->input->post();
                //if the insert has returned true then we show the flash message
				//ini_set("display_errors",1); error_reporting(E_ALL);
                if($this->model_football->store_set($data_to_store)){
                    $this->model_football->create_json($data["exercise_desc"][2]);
                    $this->session->set_flashdata('flash_message', "Records saved successfully");
                }else{
                     $this->session->set_flashdata('flash_message', "not_done");
                }
				redirect("football/football_1_2");
            }

        }
     
        
        //load the view
		$data["set"] = "";
		
		$data["tags"] = $this->utilities->fetch_tags();
		//$data["words"] = $this->utilities->fetch_all_words();
		$data["action"] = "Add";
		$data["actioning"] 	= "Adding";
		$data["action_done"] = "inserted";
        $data['main_content'] = 'football/football_1_2/add';
		$data["gen_alphabets"] = $this->utilities->generate_alphabets("loadcharwords",true,true);
        $this->load->view('includes/template', $data);  
    }  
	
	/*Method to check if checkbox are checked*/
	public function _check2boxes($str,$t)
	{
		$reqcount = 3;
		//echo "COUNT = ".count($this->input->post($t)).", T = ".$t; exit;
		if(count($this->input->post($t))<$reqcount)
		{
			if($t=="selected_mainwords")
			{
				$this->form_validation->set_message('_check2boxes', 'You must select 3 words');
			}else{
				$this->form_validation->set_message('_check2boxes', 'You must specify 3 exercise sentences.');
			}
			return FALSE;
		}else{
			return TRUE;
		}
	}     

    /**
    * Update item by his id
    * @return void
    */
    public function update()
    {
        //product id 
        $id = $this->uri->segment(4);
		$data["exercise_desc"] =  $this->utilities->get_exercise_desc(2);
  
         //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

			//form validation
			$this->form_validation->set_rules('selected_mainwords[]', 'Words', 'required|callback__check2boxes[selected_mainwords]');
			$this->form_validation->set_rules('option_sentences[]', 'Exercise sentences', 'required|callback__check2boxes[option_sentences]');
			$this->form_validation->set_rules('is_active', 'Set Status', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
				$data_to_store = $this->input->post();
				                
                //if the insert has returned true then we show the flash message
				//ini_set("display_errors",1); error_reporting(E_ALL);
                 if($this->model_football->update_set($id, $data_to_store) == TRUE){
                   $this->model_football->create_json($data["exercise_desc"][2]);
                    $this->session->set_flashdata('flash_message', "Records saved successfully");
                }else{
                    $this->session->set_flashdata('flash_message', "not_done");
                }
                redirect('football/football_1_2');

            }

        }
        //fetch manufactures data to populate the select field
        
        //load the view
		    
		$data["set"] = $this->model_football->get_set_by_id($id);
		$data["tags"] = $this->utilities->fetch_tags();
		//$data["words"] = $this->utilities->fetch_all_words();
		$data["action"] = "Update";
		$data["actioning"] 	= "Updating";
		$data["action_done"] = "updated";
        $data['main_content'] = 'football/football_1_2/add';
		$data["gen_alphabets"] = $this->utilities->generate_alphabets("loadcharwords",true,true);
        $this->load->view('includes/template', $data);;    

    }//update
	
	//method to delete selected sets
	public function deleteselected(){
		$result = $this->model_football->deleteselected($this->input->post());
		//$this->utilities->printr($createjson,1);
		if($result){
			$exerciseid = $this->utilities->get_exercise_desc(2);
			$this->model_football->create_json($exerciseid[2]); 
			$this->session->set_flashdata('flash_message', "Selected record deleted successfully.");
		}else{
			$this->session->set_flashdata('flash_message', "not_done");
		}
		redirect('football/football_1_2');
	}

}