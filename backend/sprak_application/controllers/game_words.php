<?php
class Game_words extends CI_Controller {
 
 	public $used_words = "";
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
		$this->used_words = $this->utilities->createIdsFromJSON();
        $this->load->model('words_model');

        if(!$this->session->userdata('is_logged_in')){
            redirect('login');
        }
    }
 
 	/*
	Method to load medias
	*/
 	public function medialoader($type="image")
	{
		$searchstr = "A";
		$searchcriteria="startingwith";
		if(isset($_POST["searchfor"]) && trim($_POST["searchfor"])!="")
		{
			$searchstr = $this->input->post("searchfor");
		}
		
		if(isset($_POST["criteria"]) && trim($_POST["criteria"])!="")
		{
			$searchcriteria = $this->input->post("criteria");
		}
		
		$results = $this->utilities->list_medias($type,$searchstr,$searchcriteria);
		
		$data["results"] = $results;
		$data["type"] = $type;
		$data["category"] = $type;
		$data["searchstr"] = $searchstr;
		$data["url"] = $_POST["url"];
		$data["target"] = $_POST["target"];
		
		$this->load->view("ajax_medias",$data);
	}
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index($show="all")
    {
		//all the posts sent by the view
		
        $search_string = $this->input->post('search_string');        
        $filter_by = $this->input->post('filter_by'); 
        $order_type = $this->input->post('order_type'); 
		if($_POST){
			if(trim($search_string)=="" && $filter_by=="-1"){
				$this->session->set_userdata(array("order_type"=>$order_type));
				redirect("words");
			}
		}
		 //limit end
        $page = $this->uri->segment(3);
		$page = $page>0?$page:0;
		
        //pagination settings
		$config['uri_segment'] = 3;
        $config['per_page'] = RECORDS_PER_PAGES;
        $config['base_url'] = base_url().'words/'.$show;
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 10;
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_link'] = '&gt;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		
		$config['prev_link'] = '&lt;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		
        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';

       

        //math to get the initial record to be select in the database
        $limit_end = ($page * $config['per_page']) - $config['per_page'];
        if ($limit_end < 0){
            $limit_end = 0;
        } 

        //if order type was changed
	//	echo "ORDER TYPE = ".$this->utilities->printr('order_type',1);
        if($this->input->post('order_type')!=""){
            $filter_session_data['order_type'] = $order_type;
        }
        else{
            //we have something stored in the session? 
            if($this->session->userdata('order_type')){
                $order_type = $this->session->userdata('order_type');    
            }else{
                //if we have nothing inside session, so it's the default "Asc"
                $order_type = 'ASC';    
            }
        }
		
		 //if filter was changed
        if($filter_by){
            $filter_session_data['filter_by'] = $filter_by;
        }
        else{
            //we have something stored in the session? 
            if($this->session->userdata('filter_by')){
                $filter_by = $this->session->userdata('filter_by');    
            }else{
                //if we have nothing inside session, so it's the default "Asc"
                $filter_by = '';    
            }
        }
        //make the data type var avaible to our view
        $data['order_type_selected'] = $order_type; 
		$data["filter_selected"] = $filter_by;


        //we must avoid a page reload with the previous session data
        //if any filter post was sent, then it's the first time we load the content
        //in this case we clean the session filter data
        //if any filter post was sent but we are in some page, we must load the session data

        //filtered && || paginated
        if($search_string !== false && $filter_by !== false || $this->uri->segment(3) == true){ 
		
          	/*if($search_string){
                $filter_session_data['search_string_selected'] = $search_string;
            }else{
				$search_string = $this->session->userdata('search_string_selected');
            }*/
			$filter_session_data['search_string_selected'] = $search_string;
			 
            $data['search_string_selected'] = $search_string;

            if($filter_by){
                $filter_session_data['filter_by'] = $filter_by;
            }
            else{
                $filter_by = $this->session->userdata('filter_by');
            }
			
			if($order_type){
                $filter_session_data['order_type'] = $order_type;
            }
            else{
                $order_type = $this->session->userdata('order_type');
            }
			
            $data['filter_by'] = $filter_by;
			$data['order_type'] = $order_type;

            //save session data into the session
			if(isset($filter_session_data)){
            	$this->session->set_userdata($filter_session_data);
			}

            //fetch manufacturers data into arrays
          
            $data['count_words']= $this->words_model->count_words($show,$search_string, $filter_by,$show);
            $config['total_rows'] = $data['count_words'];

            //fetch sql data into arrays
            if($search_string){
                if($filter_by){
                    $data['words'] = $this->words_model->get_words($search_string, $filter_by, $order_type, $config['per_page'],$limit_end,$show);        
                }else{
                    $data['words'] = $this->words_model->get_words($search_string, '', $order_type, $config['per_page'],$limit_end,$show);           
                }
            }else{
                if($filter_by){
                    $data['words'] = $this->words_model->get_words('', $filter_by, $order_type, $config['per_page'],$limit_end,$show);        
                }else{
                    $data['words'] = $this->words_model->get_words('', '', $order_type, $config['per_page'],$limit_end,$show);        
                }
            }

        }else{

            //clean filter data inside section
            $filter_session_data['search_string_selected'] = null;
            $filter_session_data['filter_by'] = null;
            $filter_session_data['order_type'] = null;
			
            $this->session->set_userdata($filter_session_data);

            //pre selected options
            $data['search_string_selected'] = '';
            $data["filter_by"] = "";
			
            //fetch sql data into arrays
            $data['count_words']= $this->words_model->count_words($show);
            $data['words'] = $this->words_model->get_words('', '', $order_type, $config['per_page'],$limit_end,$show); 
			       
            $config['total_rows'] = $data['count_words'];

        }//!isset($manufacture_id) && !isset($search_string) && !isset($order)

        //initializate the panination helper 
		
        $this->pagination->initialize($config);   

	
        //load the view
		
		$data["show"] = $show;
		$data["tags"] = $this->utilities->fetch_tags();
        $data['main_content'] = 'words/list';
        $this->load->view('includes/template', $data);  

    }//index

	
	public function clean_audios(){
		$this->words_model->clean_broken_audios();
		redirect("words/noaudio");
	}
	
	
	public function linktags(){
		
		if($this->words_model->link_tags($this->input->post("selectedtags"))){
			 $this->session->set_flashdata('flash_message', "done");
		}else{
			 $this->session->set_flashdata('flash_message', "not_done");
		}
		redirect("words/all");
	}
	
	
	public function savetags()
	{
	
		if($this->words_model->save_tags($this->input->post())){
			 $this->session->set_flashdata('flash_message', "done");
		}else{
			 $this->session->set_flashdata('flash_message', "not_done");
		}
		redirect("words/all");
	}
	
	/*
	method to load word checkboxes via ajax
	*/
	public function loadWords($type="all",$keywords="A"){
		$keywords = urldecode($keywords);
		$results = $this->utilities->list_all_words($type,$keywords,"col-xs-25");
		echo $results;
		exit;
	}
	
	public function createtags(){
		$data["gen_alphabets"] = $this->utilities->generate_alphabets("loadcharwords",true,true);
		$data["tags"] = $this->utilities->fetch_tags();
		$this->load->view("createtags",$data);
	}
	
    public function add()
    {
		
		$this->load->library("utilities");
		$data["tags"] = $this->utilities->fetch_tags();
		
		//if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

			//form validation
			$this->form_validation->set_rules('hid_selected_image_file', 'Image', 'required');
            $this->form_validation->set_rules('word', 'Word/Phrase', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
				$imagefilename = $this->input->post("hid_selected_image_file");
				$impathinfo = pathinfo($imagefilename);
				
				//SOUND NOUN
				$audiofilename = $this->input->post("hid_selected_audio_noun_file");
				$audiopathinfo = pathinfo($audiofilename);
				
				$data_to_store = array(
                    'word' => $this->input->post('word'),
					'tags' => implode(",",$this->input->post("selectedtags")),
                    'imageRef' => utf8_replace($impathinfo["filename"]),
                    'audioRef' => utf8_replace($audiopathinfo["filename"]),
					'word_status' => 1
                );
                //if the insert has returned true then we show the flash message
				//ini_set("display_errors",1); error_reporting(E_ALL);
                if($this->words_model->store_word($data_to_store)){
                     $this->session->set_flashdata('flash_message', "done");
                }else{
                     $this->session->set_flashdata('flash_message', "not_done");
                }
				redirect("words/all");
            }

        }
        //fetch manufactures data to populate the select field
        
        //load the view
		$data["action"] = "Add";
		$data["actioning"] = "Adding";
		$data["action_done"] = "inserted";
		$data['main_content'] = 'words/add';
        $this->load->view('includes/template', $data);  
    }       

    /**
    * Update item by his id
    * @return void
    */
    public function update()
    {
        //product id 
		//$this->utilities->printr($this->input->post(),1);
        $id = $this->uri->segment(3);
		$this->load->library("utilities");
		$data["tags"] = $this->utilities->fetch_tags();
  		
         //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

			
            //form validation
			$this->form_validation->set_rules('hid_selected_image_file', 'Image', 'required');
            $this->form_validation->set_rules('word', 'Word/Phrase', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
				$imagefilename = $this->input->post("hid_selected_image_file");
				$impathinfo = pathinfo($imagefilename);
				
				
				
				//SOUND NOUN
				$audiofilename = $this->input->post("hid_selected_audio_noun_file");
				$audiopathinfo = pathinfo($audiofilename);
				
			    $data_to_store = array(
                    'word' => $this->input->post('word'),
					'tags' => implode(",",$this->input->post("selectedtags")),
                    'imageRef' => utf8_replace($impathinfo["filename"]),
                    'audioRef' => utf8_replace($audiopathinfo["filename"]),
					'word_status' => 1
                );
				
				//echo "<pre>".print_r($data_to_store,true)."</pre>"; exit;
                
                //if the insert has returned true then we show the flash message
				//ini_set("display_errors",1); error_reporting(E_ALL);
                if($this->words_model->update_word($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', "done");
                }else{
                    $this->session->set_flashdata('flash_message', "not_done");
                }
                redirect('words');

            }

        }
        //fetch manufactures data to populate the select field
        
        //load the view
		$data["action"] = "Update";
		$data["actioning"] = "Updating";
		$data["action_done"] = "updated";
		$data['words'] = $this->words_model->get_word_by_id($id);
		$data["selected_tags"] = $this->words_model->get_tags_by_ids($data['words']["tags"]);
		$data['main_content'] = 'words/add';
        $this->load->view('includes/template', $data);       

    }//update

    /**
    * Delete
    * @return void
    */
    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->words_model->delete_word($id);
        redirect('words');
    }//edit
	
	public function undelete()
    {
        $id = $this->uri->segment(3);
        $this->words_model->undelete_word($id);
        redirect('words');
    }
	
	public function bulkaction(){
		
		if($this->words_model->bulkaction($this->input->post()) == TRUE){
			$action = ($this->input->post("bulkaction")==2)?"deleted":($this->input->post("bulkaction")==1?"made active":"made inactive");
			$this->session->set_flashdata('flash_message', "Selected record(s) $action successfully.");
		}else{
			$this->session->set_flashdata('flash_message', "Error in deleting selected records.");
		}
		redirect('words');
	}
	
	public function delete_audios(){
		$ret = $this->words_model->delete_audios($this->input->post("audio_delete"));
		if($ret>0){
			$this->session->set_flashdata('flash_message', "Selected <b>$ret</b> audio(s) deleted successfully.");
		}else{
			$this->session->set_flashdata('flash_message', "Error in deleting selected audio(s).");
		}
		redirect('words/orphaned_audios');
	}
	
	/*method to list audios that is not linked with any words*/
	public function orphaned_audios(){
		$data["orphaned_audio"] = 1;
		$data["show"] = "orphaned_audios";
		$data["orphaned_audio_list"]=$this->words_model->get_orphaned_audios();
		$data['main_content'] = 'words/list';
        $this->load->view('includes/template', $data);  
	}

    public function renameAudios($directory="capital",$extension="mp3"){
        $alphabet_path = GLOBAL_AUDIO_DIR."alphabets/".$directory."/";
        foreach (glob($alphabet_path."*.".$extension) as $filename) {
            $destfilename = str_replace("_","",utf8_encode($filename));
            $convertedfilename = utf8_replace($destfilename);
            echo $convertedfilename."<br>";
            rename($filename,$convertedfilename);
        }
    }

}