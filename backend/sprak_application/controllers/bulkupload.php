<?php
class Bulkupload extends CI_Controller {
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
		
        if(!$this->session->userdata('is_logged_in')){
            redirect('login');
        }
		
		
    }
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index()
    {
		$data['main_content'] = 'bulkupload/main';
        $this->load->view('includes/template', $data);  

    }//index

	
	
    public function startupload($filetype)
    {
		if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
			$MAX_FILES_SIZE 	= null;
			$ALLOW_EXTENSIONS 	= null;
			$UPLOAD_PATH 		= null;
			$OVERRIDE 			= false;
			$ALLOW_DELETE		= true;
			
			if(isset($MAX_FILES_SIZE) && $MAX_FILES_SIZE) 		$this->bulkuploader->setMaxFileSize($MAX_FILES_SIZE);
			if(isset($ALLOW_EXTENSIONS) && $ALLOW_EXTENSIONS) 	$this->bulkuploader->setAllowExt($ALLOW_EXTENSIONS);
			if(isset($UPLOAD_PATH) && $UPLOAD_PATH) 			$this->bulkuploader->setUploadPath($UPLOAD_PATH);
			
			//register email send on file complete
		//	if(isset($EMAIL_TO) && $EMAIL_TO) 					$this->bulkuploader->setEmail($EMAIL_TO, $EMAIL_FROM);//the email will be send after file upload
			
			//register a callback function on file complete
			//if(isset($FINISH_FUNCTION) && $FINISH_FUNCTION) 	$this->bulkuploader->onFinish($FINISH_FUNCTION);//set name of external function to be called on finish upload
			$DENY_EXT = array('php','php3', 'php4', 'php5', 'phtml', 'exe', 'pl', 'cgi', 'html', 'htm', 'js', 'asp', 'aspx', 'bat', 'sh', 'cmd');
			$this->load->model('bulkupload_model',"bulkuploader");
			$this->bulkuploader->initialize($DENY_EXT,TRUE,$filetype);
			//check request, this check if file already exits only, depends from javascript part requests
			if(isset($_REQUEST['ax-check-file']))
			{
				$this->bulkuploader->xHeader();
				echo $this->bulkuploader->_checkFileExists() ? 'yes': 'no';
			}
			elseif( isset($_REQUEST['ax-delete-file']) && $ALLOW_DELETE)
			{
				$this->bulkuploader->xHeader();
				echo $this->bulkuploader->deleteFile();
			}
			else
			{
				$this->bulkuploader->uploadFile();
			}
		
		}else{
			redirect("bulkupload");
		}
    }


    /**
    * Upload words which has no image. 
    * @return void
    */
    public function upload_no_image_words(){
    	if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
        	$this->load->model('words_model',"wordsuploader");
        	$return = $this->wordsuploader->upload_words_only($this->input->post());
        	if($return==true){
        		$this->session->set_flashdata('flash_message', "Words uploaded successfully");
        	}else{
                $this->session->set_flashdata('flash_message', "not_done");
            }
            redirect("bulkupload");
        }
    }
}