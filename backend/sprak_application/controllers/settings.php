<?php
class Settings extends CI_Controller {
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
		
        $this->load->model('settings_model');

        if(!$this->session->userdata('is_logged_in')){
            redirect('login');
        }
    }
 
 	
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index()
    {
		$data["settings"] = $this->settings_model->fetch_settings();
		$data['main_content'] = 'settings';
        $this->load->view('includes/template', $data);  

    }//index

	
	
	
	
	
	public function save()
	{
		//echo $this->utilities->printr($this->input->post(),1);
		if($this->settings_model->save($this->input->post())){
			 $this->session->set_flashdata('flash_message', "done");
		}else{
			 $this->session->set_flashdata('flash_message', "not_done");
		}
		redirect("settings");
	}
	
	

}