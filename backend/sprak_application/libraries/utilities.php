<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Utilities{

	protected $ci="";


	function __construct(){
		$this->ci =& get_instance();
		$this->ci->load->database();
	}

	/*
	method to fetch all words
	*/
	function fetch_all_words()
	{
		$this->ci->db->select('word_id,word,imageRef');
		$this->ci->db->from('lsm_game_words');
		$this->ci->db->where('word_status =',"1");
		$query = $this->ci->db->get();
		$t =  $query->result_array();
		return $t;
	}

	/*
	method to convert messed of unicode characters  for php version <=5.3
	*/
	function json($array)
	{
		 //FLAG JSON_UNESCAPED_UNICODE not supported in php version prior to 5.4
		if (version_compare(phpversion(), '5.4.0', '<')) {
			$str = json_encode($array);
			$str = preg_replace_callback('/\\\\u([0-9a-f]{4})/i',function ($matches) {
					$sym = mb_convert_encoding(
							pack('H*', $matches[1]),
							'UTF-8',
							'UTF-16'
							);
					return $sym;
				},
				$str
			);
		}else{
			$str = json_encode($array,JSON_UNESCAPED_UNICODE);
		}

		return $str;
	}

	/*
	method to fetch tags from words table
	*/
	function fetch_tags()
	{
		/*$this->ci->db->select('tags');
		$this->ci->db->from('lsm_game_words');
		$this->ci->db->where('tags !=',"");
		$query = $this->ci->db->get();
		$t =  $query->result_array();

		$strtags = "";
		for($i=0;$i<count($t);$i++)
		{
			$strtags .=trim($t[$i]["tags"]).",";
		}
		$strtags = implode(',', array_keys(array_flip(explode(',', $strtags))));
		$strtags = rtrim($strtags,",");
		return explode(",",$strtags);*/
		$this->ci->db->select('tag_id,tag_name');
		$this->ci->db->from('lsm_word_tags');
		$this->ci->db->where('is_active',"1");
		$query = $this->ci->db->get();
		$t =  $query->result_array();
		return $t;
	}

	/*method to print array-for debugging purpose only*/
	function printr($array,$exit=0){
		$ret = "<pre>".print_r($array,true)."</pre>";
		echo  $ret;
		if($exit>0)
		{
			exit;
		}
	}

	/*method to fetch exercises for menu*/
	function fetch_exercise_menu(){
		$arrayForThreeGroups = array("house");
		$this->ci->db->select("main_exercise,GROUP_CONCAT(CONCAT_WS('^',sub_exercise,url_link,exercise_identifier)) AS sub_exercise",FALSE);
		$this->ci->db->from("lsm_exercises");
		$this->ci->db->group_by("main_exercise");
		$this->ci->db->where('status',"1");
		$query = $this->ci->db->get();
		$t =  $query->result_array();
		$mainmenu = "";
		$lastgroup = "";
		for($i=0;$i<count($t);$i++)
		{
			$mainmenu .='<li class="dropdown-submenu">
                          <a tabindex="-1" href="javascript:void(0)">'.$t[$i]["main_exercise"].'</a>';

			$subex = explode(",",$t[$i]["sub_exercise"]);
			$mainmenu .='<ul class="dropdown-menu">';

            for($j=0;$j<count($subex);$j++){
				$exp = explode("^",$subex[$j]);
				if(trim($exp[2])!="")
				{
					$htext  = " - ".$exp[2];
				}else{
					$htext = "";
				}


				if(trim($exp[1])=="")
				{
					$mainmenu .="<li><a href='javascript:void(0)'  style='color:#999' onclick='alert(\"The static data is enough for this exercise. So this exercise does not require page to create exercise data\")'>".$exp[0].$htext."</a></li>";
				}else{
					$mainmenu .='<li><a href="'.base_url().strtolower($t[$i]["main_exercise"])."/".$exp[1].'">'.$exp[0].$htext.'</a></li>';
				}

				$gr = in_array(strtolower($t[$i]["main_exercise"]),$arrayForThreeGroups)?3:2;

				if(($j+1)%$gr==0)
				{
					$mainmenu .="<li class='divider'>Test</li>";
				}
			}
			$mainmenu .='</ul></li>';

		}

		return $mainmenu;
	}



	function list_all_words($type,$keyword,$liclass,$inputtype="checkbox"){
		$this->ci->db->select('word_id,word,imageRef');
		$this->ci->db->where('word_status =',"1");
		$this->ci->db->where('has_image =',"1");

		if($type=="search" && trim($keyword)=="A")
		{
			$type="alpha";
		}
		if($type=="tags")
		{
			$where = "FIND_IN_SET('".urldecode($keyword)."', tags)";
			$this->ci->db->where($where);
		}elseif($type=="startingwith"){
			$groupletter = explode("-",$keyword);
			$this->ci->db->like('word',$groupletter[0],"after");
			$this->ci->db->or_like('word',$groupletter[1],"after");
		}else{
			$operand =($type=="search")?"both":"after";
			$this->ci->db->like('word',mb_strtolower($keyword),$operand);
		}

		$this->ci->db->from('lsm_game_words');
		$this->ci->db->order_by("word","asc");
		$query = $this->ci->db->get();

		$t =  $query->result_array();

		$li_items = "";

		if($t)
		{
			foreach($t as $w){
				$extra = "";
				$test = stripos($w["imageRef"],"verb_");
				if($test!==false){
					$extra = " (Verb)";
				}

				$li_items .="<li class='".$liclass."'><label>";

				if($inputtype=="radio")
				{
					$name = "name='rdo_selected_word'";
				}else{
					$name = "";
				}

				$li_items .="<input type='".$inputtype."' class='checkbox' value='".$w["word_id"]."' data-imgsrc='".$w["imageRef"]."' ".$name." /> ".$w["word"].$extra;
				$li_items .="</label></li>";
			}
		}else{
			$li_items = "Records not available for search key <b>".$keyword."</b>";
		}
		return $li_items;
	}


	function list_all_words_no_image($type,$keyword,$liclass,$inputtype="checkbox"){
		$this->ci->db->select('word_id,word');
		$this->ci->db->where('word_status =',"1");

		if($type=="search" && trim($keyword)=="A")
		{
			$type="alpha";
		}
		if($type=="tags")
		{
			$where = "FIND_IN_SET('".urldecode($keyword)."', tags)";
			$this->ci->db->where($where);
		}elseif($type=="startingwith"){
			$groupletter = explode("-",$keyword);
			$this->ci->db->like('word',$groupletter[0],"after");
			$this->ci->db->or_like('word',$groupletter[1],"after");
		}else{
			$operand =($type=="search")?"both":"after";
			$this->ci->db->like('word',mb_strtolower($keyword),$operand);
		}

		$this->ci->db->from('lsm_game_words');
		$this->ci->db->order_by("word","asc");
		$query = $this->ci->db->get();

		$t =  $query->result_array();

		$li_items = "";

		if($t)
		{
			foreach($t as $w){
				$li_items .="<li class='".$liclass."'><label>";
				if($inputtype=="radio")
				{
					$name = "name='rdo_selected_word'";
				}else{
					$name = "";
				}
				$li_items .="<input type='".$inputtype."' class='checkbox' value='".$w["word_id"]."' ".$name." /> ".$w["word"];
				$li_items .="</label></li>";

			}
		}else{
			$li_items = "Records not available for search key <b>".$keyword."</b>";
		}
		return $li_items;
	}


	function list_all_persons(){
		$this->ci->db->select('word_id,word,imageRef');
		$this->ci->db->where('word_status =',"1");
		$liclass = "col-xs-25";


		$this->ci->db->from('lsm_character_names');
		$this->ci->db->order_by("word","asc");
		$query = $this->ci->db->get();

		$t =  $query->result_array();

		$li_items = "";

		if($t)
		{
			foreach($t as $w){
				$li_items .="<li class='".$liclass."'><label>";

				$li_items .="<input type='checkbox' class='checkbox mainwords-names' value='".$w["word_id"]."' data-imgsrc='".$w["imageRef"]."' /> ".$w["word"];
				$li_items .="</label></li>";

			}
		}else{
			$li_items = "Records not available for search key <b>".$keyword."</b>";
		}
		return $li_items;
	}


	//fetch word and related details from main word table by word id
	function word_details_by_id($id)
	{
		$this->ci->db->select("word_id,word,imageRef,audioRef");
		$this->ci->db->from('lsm_game_words');
		$this->ci->db->where("word_id",$id);
		$query = $this->ci->db->get();
		$t =  $query->result_array();
		$a =  $t[0];
		array_walk($a,function(&$value,$key){
			if($key=="imageRef"){
				$value = trim($value)!=""?$value.".png":"";
			}elseif($key=="audioRef"){
				if(trim($value)!="" && is_file(GLOBAL_AUDIO_DIR.$value.".mp3"))
				{
					$value = $value.".mp3";
				}else if(trim($value)!="" && is_file(GLOBAL_AUDIO_DIR.strtolower($value).".mp3"))
				{
					$value = strtolower($value).".mp3";
				}else{
					$value = "_nosound.mp3";
				}
			}
		});
		return $a;
	}

	//fetch word and related details from main word table by word id
	function name_details_by_id($id)
	{
		$this->ci->db->select("word_id,word,imageRef,audioRef");
		$this->ci->db->from('lsm_character_names');
		$this->ci->db->where("word_id",$id);
		$query = $this->ci->db->get();
		$t =  $query->result_array();
		$a =  $t[0];
		array_walk($a,function(&$value,$key){
			if($key=="imageRef"){
				$value = trim($value)!=""?$value.".png":"";
			}elseif($key=="audioRef"){
				if(trim($value)!="" && is_file(GLOBAL_AUDIO_DIR."names/".$value.".mp3"))
				{
					$value = $value.".mp3";
				}else{
					$value = "_nosound.mp3";
				}
			}
		});
		return $a;
	}

	/*Method to fetch media files from folder*/
	function list_medias($type,$searchstr="A",$criteria="startingwith"){
		$retfiles = array();
		$this->ci->db->select("file_name_with_ext");
		$this->ci->db->from('lsm_all_medias');
		$this->ci->db->where("file_type",$type);

		$percentsignon = ($criteria=="containing")?"both":"after";

		$this->ci->db->like("file_name",$searchstr,$percentsignon);
		$this->ci->db->order_by("file_name","asc");

		$query = $this->ci->db->get();

		return $query->result();
	}

	/*Method that generates alphabets for easy nav*/
	function generate_alphabets($selector="loadcharwords",$showsearch=false,$showfilter=false)
	{
		$txtboxwidth = $showfilter==true?"135px":"200px";

		$retvar = '<div class="atoz" style="padding-bottom:15px;padding-left:30px">
                    <div class="pagination" style="float:left">
                        <ul>';

                        foreach(range("A","Z") as $char)
                        {
							$retvar .='<li data-char="'.$char.'"><a href="javascript:void(0)" class="'.$selector.'">'.$char.'</a></li>';
                        }

                        $retvar .='  <li data-char="Ä"><a href="javascript:void(0)"  class="'.$selector.'">Ä</a></li>
                         <li data-char="Å"><a href="javascript:void(0)"  class="'.$selector.'">Å</a></li>
                         <li data-char="Ö"><a href="javascript:void(0)"  class="'.$selector.'">Ö</a></li>
                        </ul>
                    </div>';
			if($showsearch==true)
			{
				$retvar .='<div class="ajaxinput"><input type="text" placeholder="Search Keyword" size="20" class="searchstring" style="width:'.$txtboxwidth.'" /><input type="button" value="Go"  class="btn btn-primary '.$selector.'_search" data-searchfor="Ä" /></div>';
			}
			if($showfilter==true)
			{
				$tags = $this->fetch_tags();
				$retvar .="<div class='ajaxfilter'>
					<select class='".$selector."_filter selectpicker span2' data-style='btn-info' style='width:160px;'>
					<option value=''>-- Filter by Tags --</option>
					";
				foreach($tags as $tag)
				{
					$retvar .='<option value="'.$tag["tag_id"].'">'.$tag["tag_name"].'</option>';
				}
				$retvar .='</select></div>';
			}
            $retvar .='<div style="clear:both"></div>
                </div>';
		return $retvar;
	}


	/*Method to get general settings by key*/
	function settings($key){
		if(trim($key)!="")
		{
			$this->ci->db->select('setting_value');
			$this->ci->db->from('lsm_general_settings');
			$this->ci->db->where('setting_key',$key);
			$query = $this->ci->db->get();
			$t =  $query->result_array();
			return $t[0]["setting_value"];
		}
	}

	/*This method will retrieve all json data, decodes it, remove all non numeric elements, and converts multidimensional array to single dimensional
	for example
	$sourcearray = array("key"=>234,"answer"=>"yes",array("data"=>678,"data2"=>789)) TO
	$destArray = array(234,678,789);
	*/
	function createIdsFromJSON()
	{
		$this->ci->db->select('set_json_data');
		$this->ci->db->from('lsm_game_json_data');
		//$this->ci->db->where('set_id',19);
		$query = $this->ci->db->get();
		$t =  $query->result_array();
		$arr = array();
		foreach($t as $json)
		{
			$decodeJSON = json_decode($json["set_json_data"],true);
			$output = array();
			array_walk_recursive($decodeJSON, function ($current) use (&$output) {
				$output[] = $current;
			});
			$arraynum = array_filter($output,"is_numeric");
			$arr[] = array_values($arraynum);
		}
		$retarray = array();
		array_walk_recursive($arr, function ($current) use (&$retarray) {
			$retarray[] = $current;
		});
		return $retarray;
	}

	/*Method to get exercise related stuff*/
	function get_exercise_desc($segment){
		$exid = $this->ci->uri->segment($segment);
		$this->ci->db->select("exercise_identifier,exercise_screenshot,exercise_id");
		$this->ci->db->from('lsm_exercises');
		$this->ci->db->where('url_link', $exid);
		$query = $this->ci->db->get();
		$t =  $query->result_array();
		$array =array($t[0]["exercise_identifier"],$t[0]["exercise_screenshot"],$t[0]["exercise_id"]);
		return $array;
	}
}