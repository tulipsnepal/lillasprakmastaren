<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

if($_SERVER['HTTP_HOST']=="dev.websearchpro.net") {

	$url = 'http'.(isset($_SERVER['HTTPS'])?'s':'').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	$pos = strpos($url,"lm_test");
	if($pos===false){
		$subdir = "lm";
	}else{
		$subdir = "lm_test";
	}
	define("BASE_URL","http://dev.websearchpro.net/".$subdir."/backend");
	define("BASE_URL_CLIENT","http://dev.websearchpro.net/".$subdir);
	define("GLOBAL_IMG_DIR",$_SERVER["DOCUMENT_ROOT"]."/".$subdir."/uploads/images/");
	define("EXTRA_IMG_DIR",$_SERVER["DOCUMENT_ROOT"]."/".$subdir."/uploads/extra_images/");
	define("GLOBAL_AUDIO_DIR",$_SERVER["DOCUMENT_ROOT"]."/".$subdir."/uploads/audios/");

	//for saving JSON filss
	define("UPLOADS_DIR",$_SERVER["DOCUMENT_ROOT"]."/".$subdir."/uploads/");

}elseif($_SERVER['HTTP_HOST']=="lillasprakmastaren.strawberryhill.se") {

	define("BASE_URL","http://lillasprakmastaren.strawberryhill.se/backend");
	define("BASE_URL_CLIENT","http://lillasprakmastaren.strawberryhill.se");
	define("GLOBAL_IMG_DIR",$_SERVER["DOCUMENT_ROOT"]."/uploads/images/");
	define("EXTRA_IMG_DIR",$_SERVER["DOCUMENT_ROOT"]."/uploads/extra_images/");
	define("GLOBAL_AUDIO_DIR",$_SERVER["DOCUMENT_ROOT"]."/uploads/audios/");

	//for saving JSON filss
	define("UPLOADS_DIR",$_SERVER["DOCUMENT_ROOT"]."/uploads/");

}elseif($_SERVER['HTTP_HOST']=="lillasprakmastaren.se") {
	define("BASE_URL","http://lillasprakmastaren.se/backend");
	define("BASE_URL_CLIENT","http://lillasprakmastaren.se");
	define("GLOBAL_IMG_DIR",$_SERVER["DOCUMENT_ROOT"]."/uploads/images/");
	define("EXTRA_IMG_DIR",$_SERVER["DOCUMENT_ROOT"]."/uploads/extra_images/");
	define("GLOBAL_AUDIO_DIR",$_SERVER["DOCUMENT_ROOT"]."/uploads/audios/");

	//for saving JSON filss
	define("UPLOADS_DIR",$_SERVER["DOCUMENT_ROOT"]."/uploads/");

}else{

	define("BASE_URL","http://www.lsm.com/backend");
	define("BASE_URL_CLIENT","http://www.lsm.com");
	define("GLOBAL_IMG_DIR",$_SERVER["DOCUMENT_ROOT"]."/uploads/images/");
	define("EXTRA_IMG_DIR",$_SERVER["DOCUMENT_ROOT"]."/uploads/extra_images/");
	define("GLOBAL_AUDIO_DIR",$_SERVER["DOCUMENT_ROOT"]."/uploads/audios/");

	//json files path
	define("UPLOADS_DIR",$_SERVER["DOCUMENT_ROOT"]."/uploads/");

}

define("SITENAME","Lilla Språkmästaren");
define("RECORDS_PER_PAGES",30);

define("GLOBAL_IMG_URL",BASE_URL_CLIENT."/uploads/images/");
define("GLOBAL_AUDIO_URL",BASE_URL_CLIENT."/uploads/audios/");

define("EXTRA_IMAGE_URL",BASE_URL_CLIENT."/uploads/extra_images/");

define("CONST_SUCCESS_MSG","<strong>Congratulations!</strong> Records saved successfully. ");
define("CONST_ERROR_MSG","<strong>Error!</strong> Something went wrong, records not saved. Please check the details and try again.");


/* End of file constants.php */
/* Location: ./application/config/constants.php */