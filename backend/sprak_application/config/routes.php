<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
$route['default_controller'] = 'user/index';
$route['404_override'] = '';

/*admin*/

$route['signup'] = 'user/signup';
$route['create_member'] = 'user/create_member';
$route['login'] = 'user/index';
$route['logout'] = 'user/logout';
$route['login/validate_credentials'] = 'user/validate_credentials';

$route["exercise"] = "exercise/index";
$route["exercise/add"] = "exercise/add";
$route["exercise/update"] = "exercise/update";
$route['exercise/update/(:any)'] = 'exercise/update/$1';
$route['exercise/(:any)'] = 'exercise/index/$1'; //$1 = page number


$route["users"] = "admin_users/index";
$route["users/add"] = "admin_users/add";
$route["users/update"] = "admin_users/update";
$route['users/update/(:any)'] = 'admin_users/update/$1';
$route['users/(:any)'] = 'admin_users/index/$1'; //$1 = page number

//Route for game words
$route['words'] = 'game_words/index';
$route['words/clean_audios'] = 'game_words/clean_audios';
$route['words/delete_audios'] = 'game_words/delete_audios';
$route['words/orphaned_audios'] = 'game_words/orphaned_audios';
$route['words/add'] = 'game_words/add';
$route['words/linktags'] = 'game_words/linktags';
$route['words/bulkaction'] = 'game_words/bulkaction';
$route['words/savetags'] = 'game_words/savetags';
$route['words/createtags'] = 'game_words/createtags';
$route['words/medialoader'] = 'game_words/medialoader';
$route['words/medialoader/(:any)'] = 'game_words/medialoader/$1';
$route['words/update'] = 'game_words/update';
$route['words/renameAudios'] = 'game_words/renameAudios';
$route['words/renameAudios/capital/mp3'] = 'game_words/renameAudios/capital/mp3';
$route['words/renameAudios/capital/ogg'] = 'game_words/renameAudios/capital/ogg';
$route['words/renameAudios/small/mp3'] = 'game_words/renameAudios/small/mp3';
$route['words/renameAudios/small/ogg'] = 'game_words/renameAudios/small/ogg';
$route['words/update/(:any)'] = 'game_words/update/$1';
$route['words/delete/(:any)'] = 'game_words/delete/$1';
$route['words/undelete/(:any)'] = 'game_words/undelete/$1';
$route['words/(:any)'] = 'game_words/index/$1'; //$1 = page number

$route['bulkupload'] = 'bulkupload/index';
$route['bulkupload/orphaned_audios'] = 'bulkupload/orphaned_audios';


/* End of file routes.php */
/* Location: ./application/config/routes.php */