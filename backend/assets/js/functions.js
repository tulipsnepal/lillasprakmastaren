// JavaScript Document
$(function() {

	//WHEN KEY IS PRESSED ON THE TEXTBOX THEN TRIGGER THE SEARCH FUNCTIONALITY (AVAILABLE ONLY WHEN THE SORT ALPHABET BLOCK IS USED)
	$(".searchstring").on("keypress", function(e) {
		if (e.keyCode == "13") {
			e.preventDefault();
			$(this).next("input[type=button]").trigger("click");
		}
	});

	//check uncheck all checkboxes in the exercise list page
	if ($("#checkallsets").length > 0) {
		$("#checkallsets").click(function() {
			if ($(this).prop("checked")) {
				$(".exercise_sets").prop("checked", true);
			} else {
				$(".exercise_sets").prop("checked", false);
			}
		});
	}

	if ($("#submitlist").length > 0) {
		$("#submitlist").click(function(e) {
			e.preventDefault();
			if ($(".exercise_sets:checked").length == 0) {
				alert("At least select something to delete");
				return false;
			} else {
				if (confirm("Are you sure you want to delete selected set(s)?")) {
					$("#frmSetList").submit();
				}
				return false;
			}
		})
	}

	$('.showAudio tr').each(function() {
		$(this).find('td a.fancybox').each(function() {
			//do your stuff, you can use $(this) to get current cell
			var audiosrc = ajax_client_url + 'uploads/audios/' + $(this).data('audio');
			// console.log(audiosrc);
			if (audiosrc.indexOf('_nosound') < 0) {
				$(this).parent().append('<span style="float:left;"><img style="padding-right: 10px;height:16px;" class="playAudioTag" data-src="' + audiosrc + '" src="' + ajax_base_url + 'assets/img/sound.png"/></span>');
			};
		})
	})

	$(document.body).on("click", ".playAudioTag", function() {

		$('.showAudio').append('<div class="audiotagholder"></div>');
		var audioSrc = $(this).data("src");
		$(".audiotagholder").empty().html("<audio class='audioFile' id='html5audio' src='" + audioSrc + "' ></audio>");
		document.getElementById("html5audio").play();
	});

});

//FUNCTION TO LOAD ALL THE WORDS AND ITS RELATED INFORMATION.
function loadWords() {

	var updatecontents = "";

	if (arguments.length == 2) {
		var url = GlobalAjaxUrl + '/loadWords/' + arguments[0] + "/" + arguments[1];
	} else if (arguments.length == 3) {
		var url = GlobalAjaxUrl + '/loadWords/' + arguments[0] + "/" + arguments[1] + "/" + arguments[2];
	} else {
		var url = GlobalAjaxUrl + '/loadWords';
	}
	$.ajax({
		url: url,
		type: "POST",
		beforeSend: function() {
			$(".pvw_loader").show();
		},
		success: function(data) {

			$(".main-col").html(data); //POPULATE THE .main-col DIV WITH THE CHECKBOXES/RADIO-BUTTONS RETURNED FROM AN AJAX

			//ADD CLASS "mainwords" TO EACH CHECKBOXES/RADIO-BUTTONS WE JUST ADDED ABOVE
			$(".main-col").find("li").find("label").find("input").each(function() {
				$(this).attr("name", "main[]").addClass("mainwords");
			});


			//PRE SELECT THE WORDS LINKED WITH THE TAGS WHEN ADDING NEW TAGS FROM THE WORD LIST
			if ($("#selected_tags").length > 0 && $("#selected_tags").children('option').length > 0) {
				$(".mainwords").each(function() {
					$lengthsel = $('#selected_tags option[value=' + $(this).val() + ']').length;

					if ($lengthsel > 0) {
						$(this).prop("checked", true);
						$(this).parent("label").addClass("btn btn-success no-padding");
					}
				});
			}

			//PRE SELECT ALL THE CHECKBOXES WHOSE VALUES EXIST IN THE "#selected_mainwords" LISTBOX (THE LISTBOX IS POPULATED FROM DATABASE WHILE UPDATING THE RECORD)

			if ($("#selected_mainwords").length > 0 && $("#selected_mainwords").children('option').length > 0) {
				$(".mainwords").each(function() {
					$lengthsel = $('#selected_mainwords option[value=' + $(this).val() + ']').length;

					if ($lengthsel > 0) {
						$(this).prop("checked", true);
						$(this).parent("label").addClass("btn btn-success no-padding");
					}
				});
			}

			//PRE SELECT THE CHECKBOXES ON THE BASIS OF DATA FROM HIDDEN FIELD. APPLICABLE ONLY IF THE PAGE CONTAINS RESPECTIVE HIDDEN FIELD
			if ($("#selected_word").length > 0 && $("#selected_word").children('option').length > 0 && $("#selected_word").is("select")) {
				$(".mainwords").each(function() {
					if ($("#selected_word").val() == $(this).val()) {

						$(this).prop("checked", true);
						$(this).parent("label").addClass("btn btn-success no-padding");
					}
				});
			} else if ($("#selected_word").length > 0 && $.trim($("#selected_word").val()) != "") {

				$(".mainwords").each(function() {
					if ($("#selected_word").val() == $(this).val()) {

						$(this).prop("checked", true);
						$(this).parent("label").addClass("btn btn-success no-padding");
					}
				});
			}


			if ($(".option-col").length > 0) //for tivoli 1-1 we need this because we've have two separate blocks of words to choose from
			{
				$(".option-col").html(data);
				$(".option-col").find("li").find("label").find("input").each(function() {
					$(this).attr("name", "option[]").addClass("optionalwords");
				});

				$(".optionalwords").each(function() {
					$lengthsel = $('#selected_options option[value=' + $(this).val() + ']').length;
					if ($lengthsel > 0) {
						$(this).prop("checked", true);
						$(this).parent("label").addClass("btn btn-danger no-padding");
					}
				});
			}

			$(".pvw_loader").hide();

			$(".words-image").on("load", function() {
				$(this).parent().find(".image-loader").hide();
			})
		}
	});
	$(".searchstring").val("");
	if (arguments[0] != "tags") {
		$(".loadcharwords_filter").val("");
		$('.selectpicker').selectpicker('refresh');
	}
}


//FUNCTION TO LOAD ALL THE WORDS AND ITS RELATED INFORMATION.
function loadWordsAll() {

	var updatecontents = "";

	if (arguments.length == 2) {
		var url = GlobalAjaxUrl + '/loadWordsAll/' + arguments[0] + "/" + arguments[1];
	} else if (arguments.length == 3) {
		var url = GlobalAjaxUrl + '/loadWordsAll/' + arguments[0] + "/" + arguments[1] + "/" + arguments[2];
	} else {
		var url = GlobalAjaxUrl + '/loadWordsAll';
	}
	$.ajax({
		url: url,
		type: "POST",
		beforeSend: function() {
			$(".pvw_loader").show();
		},
		success: function(data) {

			$(".main-col").html(data); //POPULATE THE .main-col DIV WITH THE CHECKBOXES/RADIO-BUTTONS RETURNED FROM AN AJAX

			//ADD CLASS "mainwords" TO EACH CHECKBOXES/RADIO-BUTTONS WE JUST ADDED ABOVE
			$(".main-col").find("li").find("label").find("input").each(function() {
				$(this).attr("name", "main[]").addClass("mainwords");
			});


			//PRE SELECT THE WORDS LINKED WITH THE TAGS WHEN ADDING NEW TAGS FROM THE WORD LIST
			if ($("#selected_tags").length > 0 && $("#selected_tags").children('option').length > 0) {
				$(".mainwords").each(function() {
					$lengthsel = $('#selected_tags option[value=' + $(this).val() + ']').length;

					if ($lengthsel > 0) {
						$(this).prop("checked", true);
						$(this).parent("label").addClass("btn btn-success no-padding");
					}
				});
			}

			//PRE SELECT ALL THE CHECKBOXES WHOSE VALUES EXIST IN THE "#selected_mainwords" LISTBOX (THE LISTBOX IS POPULATED FROM DATABASE WHILE UPDATING THE RECORD)

			if ($("#selected_mainwords").length > 0 && $("#selected_mainwords").children('option').length > 0) {
				$(".mainwords").each(function() {
					$lengthsel = $('#selected_mainwords option[value=' + $(this).val() + ']').length;

					if ($lengthsel > 0) {
						$(this).prop("checked", true);
						$(this).parent("label").addClass("btn btn-success no-padding");
					}
				});
			}

			//PRE SELECT THE CHECKBOXES ON THE BASIS OF DATA FROM HIDDEN FIELD. APPLICABLE ONLY IF THE PAGE CONTAINS RESPECTIVE HIDDEN FIELD
			if ($("#selected_word").length > 0 && $("#selected_word").children('option').length > 0 && $("#selected_word").is("select")) {
				$(".mainwords").each(function() {
					if ($("#selected_word").val() == $(this).val()) {

						$(this).prop("checked", true);
						$(this).parent("label").addClass("btn btn-success no-padding");
					}
				});
			} else if ($("#selected_word").length > 0 && $.trim($("#selected_word").val()) != "") {

				$(".mainwords").each(function() {
					if ($("#selected_word").val() == $(this).val()) {

						$(this).prop("checked", true);
						$(this).parent("label").addClass("btn btn-success no-padding");
					}
				});
			}


			if ($(".option-col").length > 0) //for tivoli 1-1 we need this because we've have two separate blocks of words to choose from
			{
				$(".option-col").html(data);
				$(".option-col").find("li").find("label").find("input").each(function() {
					$(this).attr("name", "option[]").addClass("optionalwords");
				});

				$(".optionalwords").each(function() {
					$lengthsel = $('#selected_options option[value=' + $(this).val() + ']').length;
					if ($lengthsel > 0) {
						$(this).prop("checked", true);
						$(this).parent("label").addClass("btn btn-danger no-padding");
					}
				});
			}

			$(".pvw_loader").hide();

			$(".words-image").on("load", function() {
				$(this).parent().find(".image-loader").hide();
			})
		}
	});
	$(".searchstring").val("");
	if (arguments[0] != "tags") {
		$(".loadcharwords_filter").val("");
		$('.selectpicker').selectpicker('refresh');
	}
}



function loadWordsTwoInstances(holder, type, keyword, loadWhat) {
	var updatecontents = "";
	//$(".tivoli-main-words-img").empty().hide();
	var loadWhat = typeof loadWhat !== 'undefined' ? loadWhat : "with_images";
	if (loadWhat == "with_images") {
		var url = GlobalAjaxUrl + '/loadWords/' + type + "/" + keyword;
	} else {
		var url = GlobalAjaxUrl + '/loadWordsAll/' + type + "/" + keyword;
	}
	$.ajax({
		url: url,
		type: "POST",
		beforeSend: function() {
			$(".pvw_loader").show();
		},
		success: function(data) {
			$(".main-col-" + holder).html(data);

			$(".main-col-" + holder).find("li").find("label").find("input").each(function() {
				$(this).attr("name", "selectedwords_" + holder + "[]").addClass("mainwords-" + holder);
			});

			$(".pvw_loader").hide();

			//select and select the checkboxes in word list if they have already been selected(useful incase of update)

			$(".main-col-" + holder).find("li").find("label").find(".mainwords-" + holder).each(function() {
				if ($("#" + holder + "_word").val() == $(this).val()) {
					$(this).prop("checked", true);
					$(this).parent("label").addClass("btn btn-success no-padding");
				} else {

					if ($("#selected_options").length > 0) {
						$(".mainwords-second").each(function() {
							$lengthsel = $('#selected_options option[value=' + $(this).val() + ']').length;

							if ($lengthsel > 0) {
								$(this).prop("checked", true);
								$(this).parent("label").addClass("btn btn-success no-padding");
							}
						});
					}
				}
			});
		}
	});
	$(".searchstring").val("");
	if (arguments[1] != "tags") {
		$(".loadcharwords_" + holder + "_filter").val("");
		if ($(".selectpicker").hasClass("loadcharwords_" + holder + "_filter")) {
			$('.selectpicker').selectpicker('refresh');
		}
	}
}

function hasDuplicates(array) {
	var valuesSoFar = {};
	for (var i = 0; i < array.length; ++i) {
		var value = array[i];
		if (Object.prototype.hasOwnProperty.call(valuesSoFar, value)) {
			return true;
		}
		valuesSoFar[value] = true;
	}
	return false;
}